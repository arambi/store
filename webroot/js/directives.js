; (function () {
  function shopAddAttribute($timeout) {
    return {
      restrict: 'A',
      scope: '=',
      require: 'ngModel',
      link: function (scope, element, attrs, ngModel) {
        element.click(function () {
          scope.content[scope.field.key].push(scope.$eval(ngModel.$viewValue));
          $timeout(function () {
            scope.$apply();
          })
          return false;
        })
      }
    }
  }


  function shopPrice($timeout) {

    function round(value) {
      return Math.round(value * 100) / 100;
    }

    return {
      restrict: 'A',
      scope: {
        price: '=shopPriceRef',
        tax: '=shopPriceTax',
        priceWithTaxes: '=priceWithTaxes'
      },
      require: 'ngModel',
      link: function (scope, element, attrs, ngModel) {

        var changePrice = true;

        var changePriceWithTaxes = true;

        var unbindPrice = scope.$watch('price', function (value) {
          if (!value || value == 0 || value == null) {
            return;
          }
          if (!changePrice) {
            changePrice = true;
            return;
          }
          var taxes = round((value * scope.tax) / 100);
          var total = round(parseFloat(value) + parseFloat(taxes));
          changePriceWithTaxes = false;
          scope.priceWithTaxes = total;
        })


        var unbindPriceWithTaxes = scope.$watch('priceWithTaxes', function (value) {
          if (!value || value == 0 || value == null) {
            return;
          }

          if (!changePriceWithTaxes) {
            changePriceWithTaxes = true;
            return;
          }
          var tax = round(parseFloat((round(scope.tax / 100)) + 1));
          var total = round(value / tax);
          changePrice = false;
          scope.price = total;
        })
      }
    }
  }


  function cfShopAttributesPhoto() {
    return {
      restrict: 'A',
      scope: '=',
      link: function (scope, element, attrs) {
        console.log(scope.parentContent)
      }
    }
  }

  function cfAttributesUnique($timeout) {
    return {
      restrict: 'A',
      scope: '=',
      link: function (scope, element, attrs) {
        scope.arrayUnique = function (elements) {
          var collec = [];
          var ids = [];

          for (var i = 0; i < elements.length; i++) {
            for (var x = 0; x < elements[i].attributes.length; x++) {
              if (ids.indexOf(elements[i].attributes[x].id) !== -1) {
                elements[i].attributes[x].show = false;
              } else {
                ids.push(elements[i].attributes[x].id);
                elements[i].attributes[x].show = true;
              }
            }
          }

          return elements;
        }
      }
    }
  }

  function shopProductCombi($timeout) {
    return {
      restrict: 'A',
      scope: false,
      link: function (scope, element, attrs) {
        scope.beforeSelectColors = function (result) {
          var val = scope.$eval(attrs.scope + '.products_colors');
          if (val == null) {
            val = [result.originalObject];
          } else {
            val.push(result.originalObject);
          }

          $timeout(function () {
            scope.$apply();
          })

          console.log(val);
          element.val('');
        }

        scope.beforeSelectProducts = function (result) {
          var val = scope.$eval(attrs.scope + '.products');
          if (val == null) {
            val = [result.originalObject];
          } else {
            val.push(result.originalObject);
          }

          $timeout(function () {
            scope.$apply();
          })

          console.log(val);
          element.val('');
        }
      }
    }
  }

  function cfStatsButton($timeout, $http, $location) {
    return {
      restrict: 'A',
      scope: '=',
      link: function (scope, element, attrs) {
        $timeout(function () {
          window.dispatchEvent(new Event('resize'));
        });

        element.click(function () {
          var form = scope.data.crudConfig.stats.form;
          form.date_start = moment(form.date_start).format('YYYY-MM-DD');
          form.date_end = moment(form.date_end).format('YYYY-MM-DD');
          var location = $location.path('/admin/store/orders/statistics').search(form);
          window.location = '#' + location.url();

        });
      }
    }
  }

  function storeRefundInvoice($timeout, $http, $location) {
    return {
      restrict: 'A',
      scope: '=',
      link: function (scope, element, attrs) {
        $timeout(function () {
          window.dispatchEvent(new Event('resize'));
        });

        element.click(function () {
          swal({
            title: "¿Generar factura de devolución?",
            text: "Selecciona la opción que deseas",
            icon: "warning",
            buttons: {
              cancel: "Cancelar",
              shipping: {
                text: "Incluir gastos de envío",
                value: "shipping",
              },
              noShipping: {
                text: "Sin gastos de envío",
                value: "noShipping",
              },
              concept: {
                text: "Concepto libre",
                value: "concept",
              },
            },
          })
            .then(function (value) {
              if (!value) {
                return;
              }

              var order_id = scope.$eval(attrs.storeRefundInvoice);

              if (value == 'concept') {
                window.location.href = '#admin/store/invoice_refunds/create?order_id=' +  order_id;
              } else {
                window.location.href = '#admin/store/invoices/negative/' +  order_id + '/' + value;
              }

            })
        });
      }
    }
  }

  function nav($timeout) {
    return {
      restrict: 'C',
      scope: '=',
      link: function (scope, element, attrs) {
        $(element).find('li').click(function () {
          $timeout(function () {
            window.dispatchEvent(new Event('resize'));
          });
        })
      }
    }
  }
  /**
   *
   * Pass all functions into module
   */
  angular
    .module('admin')
    .directive('shopAddAttribute', shopAddAttribute)
    .directive('shopPrice', shopPrice)
    .directive('cfShopAttributesPhoto', cfShopAttributesPhoto)
    .directive('cfAttributesUnique', cfAttributesUnique)
    .directive('shopProductCombi', shopProductCombi)
    .directive('cfStatsButton', cfStatsButton)
    .directive('storeRefundInvoice', storeRefundInvoice)
    .directive('nav', nav)

    ;
})()

