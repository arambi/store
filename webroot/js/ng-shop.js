
(function ($) {
  angular.module( 'ngShop', [
     'ui.slider',
     'angularModalService',
     'ngSanitize'     
  ])
  .config(function( $locationProvider){
    
  })
  .run(function( $rootScope, $http, ModalService){
    $rootScope.showProduct = function( product){
      // $rootScope._product = product;
      ModalService.showModal({
        templateUrl: "product_show.html",
        controller: "shopProductShow",
        inputs: {
          _product: product
        }
      }).then(function(modal) {
        modal.element.modal();
      });
      return false;
    }

    $rootScope.arraySize = function(obj) {
        var size = 0, key;
        for (key in obj) {
            if (obj.hasOwnProperty(key)) size++;
        }
        return size;
    };



    function parseQuery(qstr) {
        var query = {};
        var a = qstr.substr(1).split('&');
        for (var i = 0; i < a.length; i++) {
            var b = a[i].split('=');
            query[decodeURIComponent(b[0])] = decodeURIComponent(b[1] || '');
        }
        return query;
    }

    function serializeQuery(obj) {
      var str = [];
      for(var p in obj)
        if( p !== ''){
          if (obj.hasOwnProperty(p)) {
            str.push(encodeURIComponent(p) + "=" + encodeURIComponent(obj[p]));
          }
        }
          
      return str.join("&");
    }

    function setParamUrl( key, href){
      var parser = document.createElement('a');
      var url = $("#search-current-url").attr( 'href');
      parser.href = url;
      var query = parseQuery( parser.search);
      var parts = href.split( '/');
      query[key] = parts[parts.length - 1];
      var finalUrl = parser.pathname + '?' + serializeQuery( query);
      $("#search-current-url").attr( 'href', finalUrl);
      return finalUrl;
    }

    var minvalue = $( "#slider-range").data( 'minvalue');
    var maxvalue = $( "#slider-range").data( 'maxvalue');
    $( "#slider-range" ).slider({
      range: true,
      min: 0,
      max: 500,
      values: [ minvalue, maxvalue],
      change: function( event, ui ) {
        var $content = $("#content-for-layout");
        var parser = document.createElement('a');
        var url = $("#search-current-url").attr( 'href');
        parser.href = url;
        var query = parseQuery( parser.search);
        var url = window.location.href + '?price_min=' + ui.values[0] + '&price_max=' + ui.values[0];
        query.price_min = ui.values[0];
        query.price_max = ui.values[1];
        var urlRequest = parser.pathname + '?' + serializeQuery( query);
        $("#search-current-url").attr( 'href', urlRequest);
        // window.history.pushState( 'shop', false, '/' + urlRequest);
        $.ajax({
          url: urlRequest,
          beforeSend: function(){
            $content.loadingimagethingy( 'enable');
          },
          success: function( data) {
            $content.loadingimagethingy( 'disable');
            $content.html( data);
            $("#reload-search").show();
          }
        })
      },
      slide: function( event, ui ) {
        $("#slider-min").text( ui.values [0] + '€');
        $("#slider-max").text( ui.values [1] + '€');
        $("#price-min").text( ui.values [0]);
        $("#price-max").text( ui.values [1]);
      }
    });
    var $slider = $( "#slider-range" );

    $("#slider-min").text( $slider.slider( 'values', 0) + '€');
    $("#slider-max").text( $slider.slider( 'values', 1) + '€');
    $("#price-min").text( $slider.slider( 'values', 0));
    $("#price-max").text( $slider.slider( 'values', 1));


    $('.tag-search').click(function( e){
      e.preventDefault();
      var urlRequest = setParamUrl( 'tag', $(this).attr( 'href'));
      var $content = $("#content-for-layout");
      $content.loadingimagethingy( 'enable');

      $(this).parent().parent().find('a').removeClass( 'active');
      $(this).addClass( 'active');
      $.ajax({
        url: urlRequest,
        beforeSend: function(){
          $content.loadingimagethingy( 'enable');
        },
        success: function( data) {
          $content.loadingimagethingy( 'disable');
          $content.html( data);
          $("#reload-search").show();

          // window.history.pushState( 'shop', false, '/' + urlRequest);
        }
      });
      return false;
    })


    $('#fullsearch').click(function( e){
      var parser = document.createElement('a');
      var url = $("#search-current-url").attr( 'href');
      parser.href = url;
      var query = parseQuery( parser.search);
      query['full'] = $(this).is( ':checked');
      var urlRequest = parser.pathname + '?' + serializeQuery( query);
      $("#search-current-url").attr( 'href', urlRequest);

      var $content = $("#content-for-layout");
      $content.loadingimagethingy( 'enable');

      $(this).parent().parent().find('a').removeClass( 'active');
      $(this).addClass( 'active');
      $.ajax({
        url: urlRequest,
        beforeSend: function(){
          $content.loadingimagethingy( 'enable');
        },
        success: function( data) {
          $content.loadingimagethingy( 'disable');
          $content.html( data);
          $("#reload-search").show();

          // window.history.pushState( 'shop', false, '/' + urlRequest);
        }
      });
    })


    $('.color-search').click(function( e){
      e.preventDefault();
      var urlRequest = setParamUrl( 'color', $(this).attr( 'href'));
      var $content = $("#content-for-layout");
      $content.loadingimagethingy( 'enable');

      $(this).parent().parent().find('a').removeClass( 'active');
      $(this).addClass( 'active');
      $.ajax({
        url: urlRequest,
        beforeSend: function(){
          $content.loadingimagethingy( 'enable');
        },
        success: function( data) {
          $content.loadingimagethingy( 'disable');
          $content.html( data);
          // window.history.pushState( 'shop', false, urlRequest);
          $("#reload-search").show();
        }
      });
      return false;
    })
    /*
    $http.get( '/shop/orders/order.json')
      .success(function(result){
        $rootScope.order = result.order;    
      })
    */
  })

  .controller( 'shopCategories', function( $window, $scope, $http, ModalService){
    
  })

  .controller( 'shopProductShow', function( $scope, $rootScope, _product, $timeout, $element){
    $scope._product = _product;
    $scope.attrsColorCombination = _product.jsInfo.colorCombination;
    $scope.attrsCombination = _product.jsInfo.combination;
    $scope.attrsPhotos = _product.jsInfo.photos;
    $scope.product.salt = _product.salt;
    $scope.close = function(){
      $element.modal( 'hide');
    }
    angular.element( 'body').bind( 'closeModal', function(){
      $element.modal( 'hide');
    })  
    $scope.shopShowAttr = function( attr){
      if( !$scope.currentAttributes){
        return false;
      }

      return $scope.currentAttributes.indexOf( attr) != -1;
    }
    $timeout(function(){
      $scope.$apply()
    });
  })
  ;



  function shopAddProduct( $http, $timeout, $rootScope){
    return {
        restrict: 'A',
        scope: '=',
        link: function( scope, element, attrs) {
          var closeButton = function(){
            if( attrs.shopAddProduct == 'parent') {
              scope.$parent.itemAdded = false;
            } else {
              scope.itemAdded = false;
            }
          }

          if( attrs.shopAddProduct == 'parent') {
            scope.$parent.closeItem = closeButton;
          } else {
            scope.closeItem = closeButton;
          }


          element.click( function(){
            $http.post( '/shop/orders/additem.json', {
              product: scope.product
            }).success(function( data){
              $('body').trigger( 'disable');
              if( !data.result.errors){
                if( attrs.shopAddProduct == 'parent') {
                  scope.$parent.itemAdded = true;
                  scope.$parent.item = data.result.item;
                  scope.$parent.itemError = false;
                } else {
                  scope.itemAdded = true;
                  scope.item = data.result.item;
                  scope.itemError = false;
                }
                
                $rootScope.order = data.result.order;
                $timeout( function(){
                  scope.$apply();
                })
                angular.element( 'body').trigger( 'closeModal');
              } else {
                if( attrs.shopAddProduct == 'parent') {
                  scope.$parent.itemError = data.result.errors.join("\n");
                  $timeout(function(){
                    scope.$parent.itemError = false;
                  }, 3000)
                } else {  
                  scope.itemError = data.result.errors.join("\n");
                  $timeout(function(){
                    scope.itemError = false;
                  }, 3000)
                } 
                
              }
                
            })
          })
        }
    };
  }

  function shopDeleteItem( $http, $timeout, $rootScope){
    return {
        restrict: 'A',
        scope: '=',
        link: function( scope, element, attrs) {
          element.click( function(){

            $http.post( '/shop/orders/deleteitem.json', {
              salt: attrs.shopDeleteItem
            }).success(function( data){
              // element.parent().parent().remove();
              $rootScope.order = data.order;
              $('body').trigger( 'disable');
            })
          })
        }
    };
  }

  function shopItemQuantity( $http, $timeout, $rootScope){
    return {
        restrict: 'A',
        scope: '=',
        link: function( scope, element, attrs) {
          element.click( function(){
            $http.post( '/shop/orders/quantity.json', {
              salt: scope.item.salt,
              action: attrs.shopItemQuantity
            }).success(function( data){
              $rootScope.order = data.order;
              $('body').trigger( 'disable');

              if( data.errors){
                var $scope = scope.$parent;
                $scope.itemError = data.errors.join("\n");
                $timeout(function(){
                  $scope.itemError = false;
                }, 3000)
              }
            })
          })
        }
    };
  }

  function shopSelectCountry( $http, $compile, $timeout){
    return {
        restrict: 'A',
        scope: '=',
        link: function( scope, element, attrs) {
          element.bind( 'change', function(){
            $http.post( '/shop/orders/states.json', {
              country_id: element.val().replace( 'string:', '')
            }).success(function( data){
              var options = '<option value=""> -- </option>';
              angular.forEach( data.states, function( value, key){
                options += '<option value="' + key + '">' + value + '</option>';
              });

              angular.element( '#' + attrs.shopSelectCountry + '-addresses-state-id').html( options);
              
            })
          })
        }
    };
  }

  function shopSelectAddress( $http, $compile, $timeout){
    function change( attrs, element) {
      $(attrs.shopSelectAddress).find( '.shop-address').hide();
      $(attrs.shopSelectAddress).find( '#shop-address-' + element.val()).show();
    }
    return {
        restrict: 'A',
        scope: '=',
        link: function( scope, element, attrs) {
          change( attrs, element);
          element.bind( 'change', function(){
            change( attrs, element);
          })
        }
    };
  }

  function shopAddFav( $http, $timeout){
    return {
        restrict: 'A',
        scope: '=',
        link: function( scope, element, attrs) {
          element.click( function(){
            $http.post( '/shop/favs/add.json', {
              product: attrs.shopAddFav
            }).success(function( data){
              if( !data.result.errors){
                
              } else {

              } 
            })
          })
        }
    };
  }

  function shopRemoveFav( $http, $timeout){
    return {
        restrict: 'A',
        scope: '=',
        link: function( scope, element, attrs) {
          element.click( function(){
            $http.post( '/shop/favs/delete.json', {
              product: attrs.shopRemoveFav
            }).success(function( data){
              if( attrs.ref){
                angular.element( attrs.ref).remove();
              }
              if( !data.result.errors){
                
              } else {

              }
                
            })
          })
        }
    };
  }

  function shopColorAttribute( $timeout, $rootScope){
    return {
        restrict: 'A',
        scope: '=',
        require: 'ngModel',
        link: function( scope, element, attrs, ngModel) {
          if( !scope.shopShowAttr){
            scope.shopShowAttr = function( attr){
              if( !scope.currentAttributes){
                return false;
              }
              return scope.currentAttributes.indexOf( attr) != -1;
            }
          }

          element.click( function(){
            $timeout( function(){
              scope.currentAttributes = scope.attrsColorCombination[ngModel.$viewValue];
              scope.currentColor = ngModel.$viewValue;
              $rootScope.currentAttributes = scope.attrsColorCombination[ngModel.$viewValue];
              scope.$apply();
              angular.element( 'select').trigger( 'changeColor');
            })
          })
        }
    };
  }

  function shopPreventAttrEmpty( $timeout){
    return {
        restrict: 'A',
        scope: '=',
        require: 'ngModel',
        link: function( scope, element, attrs, ngModel) {
          element.bind( 'changeColor', function( oldValue, newValue){
            $timeout( function(){
              element.trigger( 'change');
              if( scope.currentAttributes.indexOf( parseInt(element.val())) == -1) {
                element.trigger( 'change');
              }
            })
          })
        }
    };
  }

/**
 * Vigila el cambio de atributos para desactivar/activar el botón de "Añadir producto" y para cambiar la foto
 */
  function shopWatchAttributes( $timeout, $rootScope){
    return {
        restrict: 'A',
        scope: '=',
        link: function( scope, element, attrs) {
          scope.$watchCollection( 'product.attributes', function(){
            // Photo
            angular.forEach( scope.product.attributes, function( value, key){
              // if( $rootScope.attrsPhotos[value]){
              if( scope.attrsPhotos[value]){
                // scope.mainPhoto = $rootScope.attrsPhotos[value];
                scope.mainPhoto = scope.attrsPhotos[value];
                $timeout( function(){
                  scope.$apply();
                })
              }
            });

            // Activar/Desactivar producto
            if( !scope.product.attributes && (scope.attrsColorCombination.length > 0 || scope.attrsCombination.length > 0)){
              scope.buttonInactive = true;
              $timeout( function(){
                scope.$apply();
              })
              return;
            }

            var attrs = scope.attrsCombination;

            for( var i = 0; i < attrs.length; i++) {
              var has = 0;
              angular.forEach( scope.product.attributes, function( value, key){
                if( attrs[i].indexOf( parseInt( value)) != -1){
                  has++;
                }
              });

              if( has == attrs[i].length){
                scope.buttonInactive = false;
                scope.productAttribute = scope._product.product_attributes[i];
                $timeout( function(){
                  scope.$apply();
                })
                return;
              }
            }

            if( (scope.attrsColorCombination.length > 0 || scope.attrsCombination.length > 0)){
              scope.buttonInactive = true;
            } else {
              scope.buttonInactive = false;
            }
            $timeout( function(){
              scope.$apply();
            })

            return;
          })
        }
    };
  }


  function shopOrderPoints( $http, $rootScope){
    return {
        restrict: 'A',
        scope: '=',
        require: 'ngModel',
        link: function( scope, element, attrs, ngModel) {
          scope.pointsOptions = {
            stop: function( event, ui) { 
              $('body').loadingimagethingy( 'enable');
              $http.post( '/shop/orders/addpoints.json', {
                points: ngModel.$viewValue
              }).success(function( data){
                if( data.order.salt){
                  $rootScope.order = data.order;
                }
                $('body').loadingimagethingy( 'disable');
              })
            }
          }
        }
    };
  }

  function shopOrderForm( $http){
    return {
        restrict: 'A',
        scope: '=',
        link: function( scope, element, attrs) {
          scope.$watchCollection( 'cart', function( oldValue, newValue){
            if( scope.cart){
              $('body').loadingimagethingy( 'enable');
              $http.post( '/shop/orders/update.json', {
                Orders: scope.cart
              }).success(function( data){
                scope.order = data.order;
                $('body').loadingimagethingy( 'disable');
              })
            }
          })
        }
    };
  }

  function shopProduct( $timeout){
    return {
      restrict: 'A',
      scope: '=',
      link: function( scope, element, attrs){
      }
    }
  }

  function shopTrigger( $timeout){
    return {
      restrict: 'A',
      scope: '=',
      link: function( scope, element, attrs){
        $timeout( function(){
          element.trigger( attrs.shopTrigger);
        })
      }
    }
  }


  function rawdata( $timeout, $rootScope, $parse) {
    return {
      restrict: 'A',
      scope: '=',
      link: function(scope, element, attrs) { 
          var data = (element[0].innerHTML);
          var variable = $parse( attrs.rawdata);
          variable.assign( $rootScope, JSON.parse(data));
          $timeout( function(){
            $rootScope.$apply();
          })
      }
    }
  }

  function spinner(){
    return {
      restrict: 'A',
      scope: '=',
      link: function( scope, element, attrs) {
        element.click(function(){
          $(attrs.spinner).loadingimagethingy( 'enable');
          $(attrs.spinner).bind( 'disable', function(){
            $(attrs.spinner).loadingimagethingy( 'disable');
          })
        })
      }
    }
  }





  function tagSearch( $http, $location, $window, $location){
    
    return {
      restrict: 'A',
      scope: '=',
      link: function( scope, element, attrs){
        element.click(function( e){
          e.preventDefault();
          var urlRequest = setParamUrl( 'tag', attrs);
          var $content = $("#content-for-layout");
          $content.loadingimagethingy( 'enable');
          $(element).parent().parent().find('a').removeClass( 'active');
          $(element).addClass( 'active');
          $http.get( '/' + urlRequest).success(function( data){
            $content.loadingimagethingy( 'disable');
            $content.html( data);
            // $window.history.pushState( '', false, '/' + urlRequest);
            angular.element($window).triggerHandler('popstate');
            $("#advanced-search").show();
          });

          return false;
        })
      }
    }
  }

  function colorSearch( $http, $location, $window, $log){
    
    return {
      restrict: 'AC',
      scope: '=',
      link: function( scope, element, attrs){
        element.click(function( e){
          e.preventDefault();
          var urlRequest = setParamUrl( 'color',attrs);
          var $content = $("#content-for-layout");
          $content.loadingimagethingy( 'enable');

          $(element).parent().parent().find('a').removeClass( 'active');
          $(element).addClass( 'active');
          $http.get(  urlRequest)
            .success(function( data){
              $content.loadingimagethingy( 'disable');
              $content.html( data);
              // window.history.pushState( '', false, '/' + urlRequest);
              angular.element($window).triggerHandler('popstate');
            })
            .error( function( response){
              $log.error( response)
            });
          $("#advanced-search").show();
          return false;
        })
      }
    }
  }


  function mailto(){
    
    return {
      restrict: 'A',
      scope: '=',
      link: function( scope, element, attrs){
        element.attr( 'href', 'mailto:?&subject=' + attrs.mailto + '&body=' + attrs.mailto + " \n\n" + $('title').html() + ". \n\n" + window.location.href);
      }
    }
  }

  function submitCart( $http, $timeout, $rootScope){
    function sendData( element, scope){
      $('body').loadingimagethingy( 'enable');
      
      $http.post( element.attr( 'action') + '.json', {
        order: $rootScope.order,
        user: $rootScope.user,
        address_delivery: $rootScope.address_delivery,
        address_invoice: $rootScope.address_invoice
      }).success( function( data){
        $('body').loadingimagethingy( 'disable');
        $rootScope.order = data.order;
        $rootScope.errors = data.errors;
        $timeout(function(){
          $rootScope.$apply();
        })
      })
    }

    return {
      restrict: 'A',
      scope: '=',
      link: function( scope, element, attrs){
        $timeout(function(){
          $(element).find('input, select, textarea, .form-password').each(function(key){
            $(this).change(function(){
              sendData( element, scope);
            })
          });
        })
      }
    }
  }

  function applySelect( $timeout){
    return {
      restrict: 'A',
      scope: '=',
      require: 'ngModel',
      link: function( scope, element, attrs, ngModel){
        var value = scope.$eval( attrs.applySelect);
        if( value) {
          ngModel.$setViewValue( value.toString());
        }
      }
    }
  }



  angular
      .module( 'ngShop')
      .directive( 'shopAddProduct', shopAddProduct)
      .directive( 'shopSelectCountry', shopSelectCountry)
      .directive( 'shopSelectAddress', shopSelectAddress)
      .directive( 'shopDeleteItem', shopDeleteItem)
      .directive( 'shopAddFav', shopAddFav)
      .directive( 'shopRemoveFav', shopRemoveFav)
      .directive( 'shopColorAttribute', shopColorAttribute)
      .directive( 'shopPreventAttrEmpty', shopPreventAttrEmpty)
      .directive( 'shopItemQuantity', shopItemQuantity)
      .directive( 'shopOrderPoints', shopOrderPoints)
      .directive( 'shopOrderForm', shopOrderForm)
      .directive( 'shopWatchAttributes', shopWatchAttributes)
      .directive( 'shopProduct', shopProduct)
      .directive( 'shopTrigger', shopTrigger)
      .directive( 'rawdata', rawdata)
      .directive( 'spinner', spinner)
      .directive( 'tagSearch', tagSearch)
      // .directive( 'colorSearch', colorSearch)
      .directive( 'mailto', mailto)
      .directive( 'submitCart', submitCart)
      .directive( 'applySelect', applySelect)

  ;
})(jQuery);