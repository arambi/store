<?php
namespace Store\View\Cell;

use Cake\View\Cell;
use Store\Config\StoreConfig;

/**
 * Novelties cell
 */
class NoveltiesCell extends Cell
{

  /**
   * List of valid options that can be passed into this
   * cell's constructor.
   *
   * @var array
   */
  protected $_validCellOptions = [];

  /**
   * Default display method.
   *
   * @return void
   */
  public function display( $block)
  {
    $contents = StoreConfig::getProductsModel()
      ->find( 'novelties')
      ->limit( $block->settings->limit)
      ->toArray();

    $this->set( compact( 'block', 'contents'));
  }
}
