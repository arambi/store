<?php
namespace Store\View\Cell;

use Cake\View\Cell;
use Cake\ORM\TableRegistry;
/**
 * ShopCollection cell
 */
class ShopCollectionCell extends Cell
{

    /**
     * List of valid options that can be passed into this
     * cell's constructor.
     *
     * @var array
     */
    protected $_validCellOptions = [];

    /**
     * Default display method.
     *
     * @return void
     */
    public function display( $block)
    {
      $collection = TableRegistry::get( 'Store.Collections')->find()
        ->where([
            'Collections.id' => $block->parent_id
          ])
        ->first();

      $this->set( compact( 'collection', 'block'));
    }
}
