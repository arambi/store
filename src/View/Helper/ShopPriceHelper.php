<?php
namespace Store\View\Helper;

use Cake\View\Helper;
use Cake\View\View;

/**
 * ShopPrice helper
 */
class ShopPriceHelper extends Helper
{

  public $helpers = [ 'Html', 'Number'];


  public function currency( $value)
  {
    return $this->Number->currency( $value, 'EUR');
  }

}
