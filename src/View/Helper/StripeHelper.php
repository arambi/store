<?php
namespace Store\View\Helper;

use Stripe\Stripe;
use Cake\View\View;
use Stripe\Customer;
use Cake\View\Helper;
use Cake\Core\Configure;
use Cake\ORM\TableRegistry;
use Store\Error\StripeErrors;

/**
 * Stripe helper
 */
class StripeHelper extends Helper 
{
  public $helpers = array( 'Html', 'Form', 'User.Auth');
  
  public $events = [
    'onsubmit' => '',
    'onerror' => '',
    'onsuccess' => ''
  ];

  public function getUserCards()
  {
    $keys = $this->getKeys();
    Stripe::setApiKey( $keys ['secret']);    
    
    if( !$this->Auth->user())
    {
      return;
    }

    $cards = TableRegistry::get( 'Payment.StripeCards')->find()
      ->where([
        'StripeCards.user_id' => $this->Auth->user( 'id') 
      ]);
    
    return $cards;
  }

  public function getDefaultCard( $customer)
  {
    if( $customer->default_card)
    {
      foreach( $customer->cards->data as $card)
      {
        if( $card->id == $customer->default_card)
        {
          return $card;
        }
      }
    }
  }
  
  
  public function getKeys()
  {
    $prefix = Configure::read( 'App.payEnvironment') == 'development' ? 'test_' : 'live_';

    $payment = TableRegistry::get( 'Store.PaymentMethods')->find()
      ->where([
        'PaymentMethods.processor' => 'stripe'
      ])
      ->first();
    
    
    if( !$payment || empty( $payment->settings->{$prefix . 'public'}) || empty( $payment->settings->{$prefix . 'secret'}))
    {
      throw new \RuntimeException(
          'No está configurado Stripe'
      );
    }

    return [
      'public' => $payment->settings->{$prefix . 'public'},
      'secret' => $payment->settings->{$prefix . 'secret'},
    ];
  }

  public function createIntent( $order, $model)
  {
    if (empty($order->total)) {
      return;
    }
    
    \Stripe\Stripe::setApiKey( $this->getKeys()['secret']);

    $data = [
      'amount' => round( $order->total * 100),
      'currency' => 'eur',
      'metadata' => [
        'Pedido' => $order->id
      ]
    ];

    if( $this->Auth->user())
    {
      $user = TableRegistry::get( 'User.Users')->find()
        ->where([
          'Users.id' => $this->Auth->user( 'id')
        ])
        ->first();      

      if( $user->stripe_customer_id)
      {
        $data ['customer'] = $user->stripe_customer_id;
      }
    }


    $customer_id = $this->Auth->user( 'stripe_customer_id');
    
    $CardStripeOrders = TableRegistry::get( 'Payment.CardStripeOrders');
    
    $entity = $CardStripeOrders->find()
    ->where([
      'order_id' => $order->id,
      'model' => $model,
      ])
      ->first();
      
    if( !$entity)
    {
      $entity = $CardStripeOrders->newEntity([
        'order_id' => $order->id,
        'model' => $model
        ]);
    }

    if( !empty( $entity->intent_id))
    {
      try 
      {
        $intent = \Stripe\PaymentIntent::retrieve( $entity->intent_id);
      } 
      catch (\Throwable $th) 
      {
        
      }
    }

    if( !isset( $intent))
    {
      $intent = \Stripe\PaymentIntent::create( $data);
    }
    
    $entity->set( 'intent_id', $intent->id);
    $CardStripeOrders->save( $entity);
    return $intent->client_secret;
  }
}
