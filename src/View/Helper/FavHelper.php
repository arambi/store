<?php
namespace Store\View\Helper;

use Cake\View\Helper;
use Cake\View\View;

/**
 * Fav helper
 */
class FavHelper extends Helper
{

  private $hasForm = false;
  
  public $helpers = ['Html', 'Form', 'Cofree.Buffer', 'User.Auth'];  

  public function beforeLayout(Event $event, $layoutFile)
	{
		if( $this->hasForm)
		{
			$this->Buffer->start();
			$script = <<<EOF
			<script type="text/javascript">
			(function($) {
				$('[fav-item]').click(function(e){
					e.stopPropagation();
					var _el = $(this);
					$.post('/' + document.locale + '/store/favs/add.json', {
						id: _el.attr( 'fav-item'),
						model: _el.attr( 'fav-model')
					})
					.done(function( data){
						if( data.success){
							_el.parents('.c-fav-form').html('<span class="c-fav-form__success">'+ data.success +'</span>');
						} else {
							_el.parents('.c-fav-form').html('<span class="c-fav-form__error">'+ data.error +'</span>');
						}
					})
					.error(function(){

					})
					;
				})
			}(jQuery));
			</script>
EOF;
			echo $script;
			$this->Buffer->end();
		}
  }
  
  public function form( $entity)
	{
    if( !$this->Auth->user())
    {
      return false;
    }

		$this->hasForm = true;

	  $html = '<span class="c-fav-text" fav-model="'. $entity->getSource() . '" fav-item="'. $entity->id .'">';

		return '<div class="c-fav-form">'. $html .'</div>';
	}
}
