<?php
namespace Store\View\Helper;

use Cake\View\Helper;
use Cake\View\View;
use Cake\ORM\TableRegistry;
use Cake\Routing\Router;
use Cake\Collection\Collection;

/**
 * ShopNav helper
 */
class ShopNavHelper extends Helper
{

  public $helpers = ['Html', 'Url', 'User.Auth', 'Section.Nav'];

  


/**
 * Devuelve el menú de navegación del usuario
 * Dependiendo de si está logueado o no, devuelve menús distintos
 * 
 * @param  array  $li_attrs Atributos HTML para las etiquetas li
 * @return string HTML
 */
  public function user( $li_attrs = [])
  {
    $links = [];

    if( $this->Auth->isLogged())
    {
      $links [] = $this->Html->tag( 'span', $this->Auth->user( 'name'));

      $url = [
        'plugin' => 'Store',
        'controller' => 'Users',
        'action' => 'index'
      ];

      $links [] = $this->Nav->sectionLink( $url, [], __d( 'user', 'Mis datos'));

      $url = [
        'plugin' => 'User',
        'controller' => 'Users',
        'action' => 'logout'
      ];

      $links [] = $this->Nav->sectionLink( $url, [], __d( 'user', 'Salir'));
    }
    else
    {
      $url = [
        'plugin' => 'User',
        'controller' => 'Users',
        'action' => 'login'
      ];

      $links [] = $this->Nav->sectionLink( $url, [], __d( 'user', 'Entrada'));

      $url = [
        'plugin' => 'User',
        'controller' => 'Users',
        'action' => 'register'
      ];

      // $links [] = $this->Nav->sectionLink( $url, [], __d( 'user', 'Registro'));
    }

    return $links;
  }

/**
 * Navegación de los menús del proceso de compra
 * 
 * @return string HTML
 */
  public function cartNav()
  {
    $urls = [
      'cart' => [
        'plugin' => 'Store',
        'controller' => 'Orders',
        'action' => 'cart',
      ],
      'login' => [
        [
          'controller' => 'Users',
          'controller' => 'Users',
          'action' => 'login',
        ],
        [
          'controller' => 'Users',
          'controller' => 'Users',
          'action' => 'register',
        ]
      ],
      'address' => [
        [
          'plugin' => 'Store',
          'controller' => 'Orders',
          'action' => 'address',
        ],
        [
          'plugin' => 'Store',
          'controller' => 'Addresses',
          'action' => 'add',
        ]
      ],
      'checkout' => [
        'plugin' => 'Store',
        'controller' => 'Orders',
        'action' => 'checkout',
      ],
      'payment' => [
        'plugin' => 'Store',
        'controller' => 'Orders',
        'action' => 'payment',
      ],
    ];

    $labels = [
      'cart' => __d( 'app', 'Resumen'),
      'login' => __d( 'app', 'Iniciar sesión'),
      'address' => __d( 'app', 'Dirección'),
      'checkout' => __d( 'app', 'Confirmar pedido'),
      'payment' => __d( 'app', 'Pago'),
    ];

    if( $this->Auth->isLogged())
    {
      unset( $urls ['login']);
    }

    $out = [];

    $passed_current = false;

    foreach( $urls as $key => $url)
    {
      $is_current = $this->isCurrentCartNav( $url);

      if( $is_current)
      {
        $passed_current = true;
      }

      $class = $is_current ? 'current' : false;
      $_url = isset( $url ['controller']) ? $url : $url [0];

      if( !$passed_current)
      {
        $link = $this->Html->link( $labels [$key], $this->Nav->url( $_url));
        $class .= ' passed';
      }
      else
      {
        $link = $this->Html->tag( 'span', $labels [$key]);

      }

      
      $out [] = $this->Html->tag( 'li', $link, [
        'class' => $class
      ]);
    }

    return $this->Html->tag( 'ul', implode( "\n", $out));
  }

/**
 * Ayudante de cartNav()
 * Indica si la URL pasada es actual
 * 
 * @param  array  $urls
 * @return boolean
 */
  public function isCurrentCartNav( $urls)
  {
    $params = $this->request->params;

    if( isset( $urls ['controller']))
    {
      $urls = [$urls];
    }

    foreach( $urls as $url)
    {
      if( $params ['controller'] == $url ['controller'] && $params ['action'] == $url ['action'])
      {
        return true;
      }
    }

    return false;
  }


  public function loginRedirectFromCart()
  {
    if( !$this->request->session()->check( 'Auth.redirect'))
    {
      return false;
    }

    $redirect = $this->request->session()->read( 'Auth.redirect');
    $url = Router::parse( $redirect);

    return $url ['controller'] == 'Orders';
  }


  public function userNav( $labels = [], $extra = [])
  {
    $urls = [
      'profile' => [
        'plugin' => 'Store',
        'controller' => 'Users',
        'action' => 'edit',
      ],
      'orders' => [
        'plugin' => 'Store',
        'controller' => 'Users',
        'action' => 'orders',
      ],
      'returns' => [
        [
          'controller' => 'Shop',
          'controller' => 'Users',
          'action' => 'returns',
        ],
        [
          'controller' => 'Shop',
          'controller' => 'Returneds',
          'action' => 'add',
        ]
      ],
      'addresses' => [
        'plugin' => 'Store',
        'controller' => 'Users',
        'action' => 'addresses',
      ],
      'favs' => [
        'plugin' => 'Store',
        'controller' => 'Favs',
        'action' => 'index',
      ],
    ];

    $urls = array_merge( $extra, $urls);

    if( empty( $labels))
    {
      $labels = [
        'profile' => __d( 'app', 'Información personal'),
        'orders' => __d( 'app', 'Pedidos'),
        'returns' => __d( 'app', 'Solicitud de devolución'),
        'addresses' => __d( 'app', 'Mis direcciones'),
        'favs' => __d( 'app', 'Mi lista de deseos'),
      ];
    }
      
    $out = [];
    
    foreach( $labels as $key => $label)
    {
      if( !isset( $labels [$key]))
      {
        continue;
      }

      $url = $urls [$key];
      
      $is_current = $this->isCurrentCartNav( $url);

      $class = $is_current ? 'current' : false;
      $_url = isset( $url ['controller']) ? $url : $url [0];

      $link = $this->Html->link( $labels [$key], $this->Nav->url( $_url), [
        'escape' => false
      ]);

      $out [] = $this->Html->tag( 'li', $link, [
        'class' => $class
      ]);
    }

    return $this->Html->tag( 'ul', implode( "\n", $out));
  }


  public function promosForUser()
  {
    $promos = TableRegistry::get( 'Store.Promos')->activesForUser( $this->Auth->user( 'group_id'));

    $out = [];

    foreach( $promos as $promo)
    {
      $out [] = $this->promoText( $promo);
    }

    return $out; 
  }

  public function promoText( $promo)
  {
    $types = [
      'global' => __d( 'app', 'en todos los productos'),
      'categories' => __d( 'app', 'en los productos de las categorías {0}', [
        implode( ', ', (new Collection( $promo->categories_shops))->extract( 'title')->toArray())
      ]),
      'products' => __d( 'app', 'para algunos productos')
    ];

    $type = $types [$promo->promo_type];

    $value = $promo->discount_type == 'fixed' ? $promo->value .' €' : $promo->value . '%';

    $text = __d( 'app', 'Tienes activo un descuento extra {0} de {1} por ser del grupo {2} hasta el {3}', [
      $type,
      $value,
      $this->Auth->user( 'group.name'),
      date( 'd/m/Y', $promo->finish_on->toUnixString())
    ]);


    return $text;
  }
}
