<?php
namespace Store\View\Helper;

use Cake\View\Helper;
use Cake\View\View;
use Cake\Collection\Collection;

/**
 * Attributes helper
 * Utilidades para el uso de atributos en el frontend
 */

class AttributesHelper extends Helper
{
  public $helpers = ['Html', 'Form'];


}
