<?php

namespace Store\View\Helper;

use Cake\View\Helper;
use Cake\View\View;
use Cake\Collection\Collection;
use Cake\ORM\TableRegistry;
use Store\Config\StoreConfig;
use Cake\Utility\Text;

/**
 * ShopProduct helper
 */
class ShopProductHelper extends Helper
{

    public $helpers = ['Html', 'Number', 'Url', 'Form', 'Store.ShopPrice', 'Upload.Uploads', 'User.Auth'];

    private $product;

    private $favsIds = [];

    private $favsIdsInit = false;

    private function setFavsIds()
    {
        $ids = TableRegistry::getTableLocator()->get('Store.Favs')->find()
            ->where([
                'Favs.user_id' => $this->Auth->user('id')
            ])
            ->extract('product_id')
            ->toArray();

        $this->favsIds = $ids;
    }

    public function hasFav($product_id)
    {
        if (!$this->Auth->user()) {
            return false;
        }

        if (!$this->favsIdsInit) {
            $this->setFavsIds();
        }

        return in_array($product_id, $this->favsIds);
    }

    public function setProduct($product)
    {
        $this->product = $product;
        return $this;
    }

    public function quantity()
    {
        return $this->Form->input('Product', [
            'type' => 'select',
            'options' => array_combine(range(1, 10), range(1, 10)),
            'label' => __d('app', 'Cantidad'),
            'empty' => false,
            'ng-model' => 'product.quantity',
            'ng-init' => "product.quantity = '1'"
        ]);
    }

    public function addButton($product, $title = false, $attrs = [])
    {
        $_defaults = [
            'class' => 'btn',
            'shop-add-product' => '',
            'escape' => false,
            'product' => $product->salt,
            'ng-disabled' => 'buttonInactive'
        ];

        $out = [];
        $attrs = array_merge($_defaults, $attrs);

        if (!$title) {
            $title = __d('app', 'Añadir a cesta');
        }

        $out[] = '<input class="shop-add" spinner="body" type="hidden" ng-model="product.salt" ng-init="product.salt=\'' . $product->salt . '\'" />';
        $out[] = $this->Html->tag('button', $title, $attrs);
        return implode("\n", $out);
    }

    public function tags($product)
    {
        $out = [];

        foreach ($product->tags as $tag) {
            $link = $this->Html->link($tag->title, [
                'plugin' => 'Store',
                'controller' => 'Products',
                'action' => 'tag',
                'slug' => $tag->slug
            ]);

            $out[] = $this->Html->tag('li', $link);
        }

        return implode("\n", $out);
    }

    public function link($category, $product)
    {
        return $this->Html->link($product->title, $this->url($category, $product));
    }

    public function url($category, $product)
    {
        if (is_array($category)) {
            $category = $category[0];
        }

        $params = [
            'plugin' => 'shop',
            'controller' => 'products',
            'action' => 'view',
            'slug' => $product->slug
        ];


        $url = $this->Url->build($params);

        if (isset($product->color) && isset($product->color->attribute_id)) {
            $url .= '?attribute_id=' . $product->color->attribute_id;
        }

        return $url;
    }

    public function imageIndex($product, $attrs = [], $view = 'view')
    {
        if (empty($product->photos)) {
            return null;
        }



        // Si es un producto con un product_attribute concreto se devuelve la foto asociada a ese atributo
        if (isset($product->color) && isset($product->color->attribute_id)) {
            $candidates = [];
            $photoMain = false;

            foreach ($product->photos as $photo) {
                if (!isset($photo->attributes)) {
                    continue;
                }

                $attributes = $photo->attributes;
                $attributes = (array)$attributes;
                $id = $product->color->attribute_id;

                if ((is_array($photo->attributes) && isset($photo->attributes[$id]) && $photo->attributes[$id]) ||
                    (is_object($photo->attributes) && isset($photo->attributes->$id) && $photo->attributes->$id)
                ) {
                    $candidates[] = $photo;
                }
            }
            if (!empty($candidates)) {
                if (count($candidates) == 1) {
                    $photoMain = $candidates[0];
                }

                foreach ($candidates as $candidate) {
                    if (isset($candidate->main) && $candidate->main) {
                        $photoMain = $candidate;
                    }
                }

                if (!$photoMain) {
                    $photoMain = $candidates[0];
                }
            }

            if ($photoMain) {
                if ($view == 'view') {
                    $_attrs = [
                        'ng-src' => '{{mainPhoto}}',
                        'ng-init' => "mainPhoto = '" . $photoMain->paths->square . "'",
                        'escape' => false,
                        'alt' => $photo->alt ? $photo->alt : $product->title,
                        'title' => $photo->title ? $photo->title : $product->title,
                    ];
                } else {
                    $_attrs = [
                        'src' => $photoMain->paths->square,
                        'alt' => $photo->alt ? $photo->alt : $product->title,
                        'title' => $photo->title ? $photo->title : $product->title,
                    ];
                }

                $attrs = $attrs + $_attrs;
                return '<img ' . $this->Html->templater()->formatAttributes($attrs) . ' />';
            }
        }

        foreach ($product->photos as $photo) {
            if (isset($photo->main) && $photo->main) {
                if ($view == 'view') {
                    $_attrs = [
                        'ng-src' => '{{mainPhoto}}',
                        'ng-init' => "mainPhoto = '" . $photo->paths->square . "'",
                        'escape' => false,
                        'alt' => $photo->alt ? $photo->alt : $product->title,
                        'title' => $photo->title ? $photo->title : $product->title
                    ];
                } else {
                    $_attrs = [
                        'src' => $photo->paths->square,
                        'alt' => $photo->alt ? $photo->alt : $product->title,
                        'title' => $photo->title ? $photo->title : $product->title,
                    ];
                }

                $attrs = $_attrs + $attrs;
                return '<img ' . $this->Html->templater()->formatAttributes($attrs) . ' />';
            }
        }
    }

    /**
     * Devuelve el precio/precios de un producto y de sus product_attributes
     * 
     * @param  Store\Model\Entity\Product    $product
     * @return string HTML
     */
    public function price($product, $isView = false)
    {
        $out = [];
        // Precio del producto
        $price = [];

        if (!empty($product->offer_price)) {
            $price[] = '<small style="text-decoration: line-through" class="shop-old-price">' . $product->original_price_human . '</small>';
        }

        $price[] = $product->price_human;

        $out[] = $this->Html->tag('span', implode("\n", $price), [
            'ng-show' => $isView ? '!productAttribute' : 'true'
        ]);
        // Precios de los product_attributes

        if ($isView && !empty($product->product_attributes)) {
            foreach ($product->product_attributes as $pa) {
                $price = [];
                if (!empty($pa->offer_price)) {
                    $price[] = '<small style="text-decoration: line-through" class="shop-old-price">' . $pa->original_price_human . '</small>';
                }

                $price[] = $pa->price_human;

                $out[] = $this->Html->tag('span', implode("\n", $price), [
                    'ng-show' => $isView ? 'productAttribute.id == ' . $pa->id : false,
                    'class' => 'ng-hide'
                ]);
            }
        }

        return implode("\n", $out);
    }

    public function title($product)
    {
        return $product->title;
    }


    public function favButton($product, $options = [])
    {
        if (!$this->Auth->isLogged()) {
            return;
        }

        $_options = [
            'class' => '',
            'content' => __d('app', 'Añadir a favorito'),
        ];

        $options = $options + $_options;
        $return = '<a href shop-add-fav="' . $product->salt . '" class="' . $options['class'] . '">' . $options['content'] . '</a>';

        return $return;
    }

    public function deleteFav($product, $options = [])
    {
        if (!$this->Auth->isLogged()) {
            return;
        }

        $_options = [
            'class' => '',
            'content' => __d('app', 'Borrar favorito'),
            'ref' => false
        ];

        $return = '<a href shop-remove-fav="' . $product->salt . '" ref="' . $options['ref'] . '" class="' . $options['class'] . '">' . $options['content'] . '</a>';

        return $return;
    }

    public function attributes($product, $options = [], $cname = false)
    {
        $options = array_merge([
            'show_prices' => false,
            'title' => false,
            'prices_format' => '<span>:title</span> <span>:price</span>'
        ], $options);

        if ($cname && !is_array($cname)) {
            $cname = [$cname];
        }

        $groups = StoreConfig::getProductsModel()->attributeGroupsFromProduct($product);
        $out = [];

        foreach ($groups as $group) {
            if ($cname && !in_array($group->cname, $cname)) {
                continue;
            }

            $_options = $this->group($group, $product, $options);

            if (!empty($_options)) {
                $out[] = '<div class="c-product-attribute">' . $_options . '</div>';
            }
        }

        return implode("\n", $out);
    }


    /**
     * Devuelve una lista <li> de un conjunto de atributos dado un grupo de atributos
     * 
     * @param  Store\Model\Entity\AttributeGroup $group 
     * @return string HTML
     */
    public function group($group, $product, $options)
    {
        $method = '__' . $group->group_type;

        return $this->$method($group, $product, $options);
    }

    /**
     * Ayudante de $this->group() para atributos de tipo color
     * 
     * @param  Store\Model\Entity\AttributeGroup $group
     * @return string HTML
     */
    private function __color($group, $product, $options)
    {
        $out = [];
        $title = $options['title'] ? $options['title'] : $group->web_title;
        $out[] = $this->Html->tag('h4', $title);
        $li = [];

        if (empty($group->attributes)) {
            return '';
        }
        
        foreach ($group->attributes as $key => $attribute) {
            $attribute_query_id = $this->request->query('attribute_id');
            $has = ($key == 0 && !$attribute_query_id) || ($attribute_query_id && $attribute_query_id == $attribute->id);
            $checked = count($group->attributes) == 1 ? 'checked="checked"' : '';
            $li[] = '<label><input ' . $checked . ' name="attributes[' . $group->id . ']" style="display: none" type="radio" value="' . $attribute->id . '" /><span class="c-attribute__color" style="background-color: '
                . $attribute->color .
                '"></span></label>';
        }

        $out[] = $this->Html->tag('div', implode("\n", $li), ['class' => 'c-product-attribute__colorgroup']);

        return implode("\n", $out);
    }

    /**
     * Ayudante de $this->group() para atributos de tipo selector
     * 
     * @param  Store\Model\Entity\AttributeGroup $group
     * @return string HTML
     */
    private function __select($group, $product, $options)
    {
        $out = [];

        $title = $options['title'] ? $options['title'] : $group->web_title;
        $out[] = $this->Html->tag('h4', $title);

        $options = (new Collection($group->attributes))->combine('id', 'title')->toArray();

        $opts = [];

        if (empty($options)) {
            return '';
        }

        foreach ($options as $value => $text) {
            $opts[] = [
                'value' => $value,
                'text' => $text,
            ];
        }
        $select = $this->Form->input($group->id, [
            'label' => false,
            'type' => 'select',
            'options' => $opts,
            'name' => 'attributes[' . $group->id . ']',
            'empty' => '--',
        ]);

        $out[] = $this->Html->tag('div', $select, ['class' => 'c-product-attribute__select']);
        return implode("\n", $out);
    }

    private function __radio($group, $product, $options)
    {
        
        $out = [];
        $title = $options['title'] ? $options['title'] : $group->web_title;

        $out[] = $this->Html->tag('h4', $title);

        $_options = (new Collection($group->attributes))->combine('id', 'title')->toArray();

        if (empty($_options)) {
            return '';
        }
        
        $opts = [];
        $li = [];

        foreach ($_options as $value => $text) {
            if ($options['show_prices']) {
                $prices = $product->pricesOfAttribute($value);
                $text = Text::insert($options['prices_format'], [
                    'title' => $text,
                    'price' => $prices
                ]);
            }

            $li[] = '<label><input name="attributes[' . $group->id . ']" type="radio" value="' . $value . '" /><span>' . $text . '</span></span></label>';
        }

        $out[] = $this->Html->tag('div', implode("\n", $li), ['class' => 'c-product-attribute__radio']);
        return implode("\n", $out);
    }
}
