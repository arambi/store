<?php

namespace Store\View\Helper;

use Cake\View\Helper;
use Cake\View\View;
use Cake\Collection\Collection;
use Cake\ORM\TableRegistry;
use Website\Lib\Website;
use Cofree\Lib\RemoteLocation;
use Cake\Core\Configure;
use Store\Cart\Cart;

/**
 * Cart helper
 */
class CartHelper extends Helper
{
    public $helpers = ['Upload.Uploads', 'User.Auth'];

    public function itemImage($item, $attrs = [])
    {
        if (!empty($item->photo)) {
            return $this->Uploads->image($item->photo, 'square', $attrs);
        }
    }

    public function taxRate($value)
    {
        return $value . '% ' . 'IVA';
    }

    public function cart()
    {
    }

    public function payments($element = 'Store.cart/payment_methods')
    {
        $payments = TableRegistry::getTableLocator()->get('Store.PaymentMethods')->getValids($this->Auth->user());
        return $this->_View->element($element, ['payments' => $payments]);
    }

    public function carriers($element = 'Store.cart/carriers')
    {
        $carriers = TableRegistry::getTableLocator()->get('Store.Carriers')->getValids();
        return $this->_View->element($element, ['carriers' => $carriers]);
    }

    public function userPoints($order)
    {
        $points = TableRegistry::getTableLocator()->get('Store.Points')->totalAvailables($this->Auth->user('id'), $order);
        return $points;
    }

    public function maxUserPointsAvailable($order, $points)
    {
        $value = Website::get('settings.store_points_down_value');
        $total = $order->subtotal_fixed + $order->taxes_fixed;
        $max = round($total / $value, 0, PHP_ROUND_HALF_DOWN);

        if (($max * $value) > $total) {
            $max = $max - 1;
        }

        if ($max > $points) {
            return $points;
        }

        return $max;
    }

    public function orderBrandAddress($order)
    {
    }

    public function cartError($key)
    {
        return '<div class="error-message hide u-hide" cart-error="' . $key . '"></div>';
    }

    public function countries()
    {
        $order = Cart::getOrder();

        $countries = TableRegistry::getTableLocator()->get('Store.Countries')
            ->find('list');

        if ($order && $order->has_portable_products) {
            $countries->where([
                'active' => true,
            ]);
        } else {
            $countries->where([
                'OR' => [
                    'active' => true,
                    'active_no_carrier' => true,
                ]
            ]);
        }

        $countries->order([
            'Countries.with_position' => 'desc',
            'Countries.position',
            'Countries.title'
        ]);

        return $countries->all();
    }

    public function states($country_id = null)
    {
        if ($country_id === null) {
            $location = new RemoteLocation();
            $country = false;
            if (property_exists($location, 'country_id')) {
                $country = TableRegistry::getTableLocator()->get('Store.Countries')->getByCode($location->country_code);
            }

            if ($country) {
                $country_id = $country->id;
            }
        }

        if ($country_id) {
            $states = TableRegistry::getTableLocator()->get('Store.States')->find('list')
                ->order([
                    'States.title'
                ])
                ->where([
                    'States.country_id' => $country_id,
                    'States.active' => true,
                ])->all();

            return $states;
        }

        return [];
    }

    public function userHasAddress($type)
    {
        $address = TableRegistry::getTableLocator()->get('Store.Addresses')->find()
            ->where([
                'type' => $type,
                'user_id' => $this->Auth->user('id')
            ])
            ->first();

        return (bool)$address;
    }
}
