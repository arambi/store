<?php 

namespace Store\Traits;

trait MigrationsStoreTrait
{
  public function productsTable( $table)
  {
    if( !$table->hasColumn( 'date_restrict'))
    {
      $table
          ->addColumn( 'date_restrict', 'boolean', ['null' => false, 'default' => 0]);
    }

    if( !$table->hasColumn( 'start_on'))
    {
      $table
          ->addColumn( 'start_on', 'date', ['null' => true, 'default' => null]);
    }

    if( !$table->hasColumn( 'finish_on'))
    {
      $table
          ->addColumn( 'finish_on', 'date', ['null' => true, 'default' => null]);
    }

    if( !$table->hasColumn( 'published'))
    {
      $table
          ->addColumn( 'published', 'boolean', ['null' => false, 'default' => 0])
          ->addIndex( ['published']);
    }

    if( !$table->hasColumn( 'store_brand_id'))
    {
      $table
          ->addColumn( 'store_brand_id', 'integer', ['null' => true, 'default' => null])
          ->addIndex( ['store_brand_id']);
    }

    if( !$table->hasColumn( 'store_tax_id'))
    {
      $table
          ->addColumn( 'store_tax_id', 'integer', ['null' => true, 'default' => null])
          ->addIndex( ['store_tax_id']);
    }
    
    if( !$table->hasColumn( 'store_price'))
    {
      $table
          ->addColumn( 'store_price', 'float', ['precision' => 20, 'scale' => 6, 'null' => true, 'default' => 0])
          ->addIndex( ['store_price']);
    }

    if( !$table->hasColumn( 'store_weight'))
    {
      $table->addColumn( 'store_weight', 'float', ['null' => true, 'default' => NULL]);
    }

    if( !$table->hasColumn( 'store_minimal_quantity'))
    {
      $table->addColumn( 'store_minimal_quantity', 'integer', ['null' => true, 'default' => NULL]);
    }

    if( !$table->hasColumn( 'store_reference'))
    {
      $table->addColumn( 'store_reference', 'string', ['null' => true, 'default' => NULL, 'limit' => 32]);
    }

    if( !$table->hasColumn( 'store_ean13'))
    {
      $table->addColumn( 'store_ean13', 'string', ['null' => true, 'default' => NULL, 'limit' => 13]);
    }

    if( !$table->hasColumn( 'store_upc'))
    {
      $table->addColumn( 'store_upc', 'string', ['null' => true, 'default' => NULL, 'limit' => 12]);
    }

    if( !$table->hasColumn( 'store_quantity'))
    {
      $table
        ->addColumn( 'store_quantity', 'integer', ['null' => true, 'default' => NULL, 'limit' => 10])
        ->addIndex( ['store_quantity']);
    }

    if( !$table->hasColumn( 'store_available_date'))
    {
      $table->addColumn( 'store_available_date', 'date', ['null' => true, 'default' => null]);
    }

    if( !$table->hasColumn( 'store_novelty'))
    {
      $table->addColumn( 'store_novelty', 'boolean', ['null' => true, 'default' => null]);
    }

    if( !$table->hasColumn( 'salt'))
    {
      $table->addColumn( 'salt', 'string', ['default' => NULL, 'null' => true]);
    }

    if( !$table->hasColumn( 'delay_days'))
    {
      $table->addColumn( 'delay_days', 'integer', ['null' => true, 'default' => 0]);
    }
    
    if( !$table->hasColumn( 'only_with_attributes'))
    {
      $table->addColumn( 'only_with_attributes', 'boolean', ['null' => false, 'default' => 0]);
    }

    if( !$table->hasColumn( 'without_stock'))
    {
      $table->addColumn( 'without_stock', 'boolean', ['null' => false, 'default' => 0]);
    }

    $table->save();
  }
}