<?php 
namespace Store\Traits;

use Cake\ORM\TableRegistry;
use Cake\ORM\Query;

trait ProductsTableTrait
{
  public function setAssociations()
  {
    // Associations
    $this->hasMany( 'ProductAttributes', [
      'className' => 'Store.ProductAttributes',
      'foreignKey' => 'product_id',
      'conditions' => [
        'ProductAttributes.store_price IS NOT NULL'
      ]
    ]);

    $this->belongsTo( 'Taxes', [
      'className' => 'Store.Taxes',
      'foreignKey' => 'store_tax_id',
    ]);

    $this->belongsTo( 'Brands', [
      'className' => 'Store.Brands',
      'foreignKey' => 'store_brand_id',
    ]);

    $this->belongsToMany( 'RelatedProducts', [
      'joinTable' => 'store_relateds',
      'through' => 'Store.Relateds',
      'foreignKey' => 'product_id',
      'targetForeignKey' => 'related_id',
      'className' => 'Store.ProductRelateds',
    ]);

    
  }

  public function setBehaviors()
  {

  }


  public function setAttributesPrice( $query)
  {
    $prices = [];

    $query->formatResults( function( $results) use ($prices){

      return $results->map(function( $row) use ($prices){
          $isSeted = false;

          if( isset( $row->product_attributes))
          {
            foreach( $row->product_attributes as $pa)
            {
              $pa->tax = $row->tax;

              if( empty( $pa->shop_price))
              {
                $pa->shop_price = $row->shop_price;
              }
              else
              {
                $prices [] = $pa->shop_price;
              }
            }
          }

          if( !$isSeted && !empty( $prices))
          {
            $row->shop_price = min( $prices);
          }
          return $row;
        });
    }, $query::APPEND);

    return $query;
  }







  public function attributeGroupsFromProduct( $product)
  {
    $collection = collection( $product->product_attributes)
      ->match([
        'published' => true,
        // 'quantity > 0'
      ]);


    $attributes = $collection->extract( 'attributes')->toArray();
    $attribute_ids = [];

    foreach( $attributes as $attribute)
    {
      $collection = collection( $attribute);
      $attribute_ids = array_merge( $attribute_ids, $collection->extract( 'id')->toArray());
    }
    
    $groups = TableRegistry::get( 'Store.AttributeGroups')->find()
      ->contain(['Attributes'])
      ->toArray();


    foreach( $groups as $group)
    {
      foreach( $group->attributes as $key =>$attribute)
      {
        if( !in_array( $attribute->id, $attribute_ids))
        {
          unset( $group->attributes [$key]);
        }
      }
    }

    return $groups;
  }

  public function findNovelties( Query $query)
  {
    return $query->where([
      $this->alias() . '.store_novelty' => true
    ]);
  }
}