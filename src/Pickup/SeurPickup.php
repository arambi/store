<?php

namespace Store\Pickup;

use Cake\ORM\TableRegistry;

class SeurPickup
{
    private $__endPoint = 'https://ws.seur.com/WSEcatalogoPublicos/servlet/XFireServlet/WSServiciosWebPublicos?wsdl';

    private $__connectOptions = [
        'connection_timeout' => 7,
        'default_socket_timeout' => 7,
        'trace' => 1,
    ];

    private $__user;

    private $__password;

    public function __construct($user, $password)
    {
        $this->__user = $user;
        $this->__password = $password;
    }

    private function getClient()
    {
        $client = new \SoapClient($this->__endPoint, $this->__connectOptions);
        return $client;
    }

    public function getPickups($postcode)
    {
        $client = $this->getClient();
        $postcode_state = substr($postcode, 0, 2);

        $state = TableRegistry::getTableLocator()->get('Store.States')
            ->find()
            ->where([
                'States.postcode' => $postcode_state,
                'States.active' => true
            ])
            ->first();
        
        if (!$state) {
            return [];
        }

        $body = <<<XML
    <CAMPOS>
      <CODIGO_POSTAL>$postcode</CODIGO_POSTAL> 
      <USUARIO>{$this->__user}</USUARIO> 
      <PASSWORD>{$this->__password}</PASSWORD> 
    </CAMPOS>
XML;

        $data = [
            'in0' => $body
        ];

        $response = $client->puntosDeVentaStr($data);

        \Cake\Log\Log::debug('*** REQUEST:\n'.$client->__getLastRequest());

        $xml = simplexml_load_string(utf8_decode($response->out));
        return $this->formatResults($xml);
    }

    private function formatResults($xml)
    {
        $results = [];
        $num = (int)$xml->attributes()->NUM[0];

        for ($i = 1; $i <= $num; $i++) {
            $name = 'REG' . $i;
            $timetable = $this->getHorario($xml->$name, (string)$xml->$name->NOM_CENTRO_SEUR);

            if ($timetable && $xml->$name->COD_TIPO_CENTRO != 'K') {
                $results[] = [
                    'company' => (string)$xml->$name->NOM_CENTRO_SEUR,
                    'address' => (string)$xml->$name->COD_TIPO_VIA . '. ' . (string)$xml->$name->NOM_CORTO . ', ' . (string)$xml->$name->NUM_VIA,
                    'codCentro' => (string)$xml->$name->COD_CENTRO_SEUR,
                    'codCentroSeur' => (string)$xml->$name->COD_UNIDAD_ADMIN_CT,
                    'city' => (string)$xml->$name->NOM_POBLACION,
                    'postcode' => (string)$xml->$name->CODIGO_POSTAL,
                    'phone' => (string)$xml->$name->TELEFONO_1,
                    'gMapDir' => (string)$xml->$name->COD_TIPO_VIA . '. ' . $xml->$name->NOM_CORTO . ', ' . $xml->$name->NUM_VIA . ', ' . $xml->$name->NOM_POBLACION,
                    'position' => array('lat' => (float)$xml->$name->LATITUD, 'lng' => (float)$xml->$name->LONGITUD),
                    'timetable' => $timetable
                ];
            }
        }

        return $results;
    }

    private function getHorario($xml, $name)
    {
        if (!isset($xml->HORARIO_EXT->DIA_SEMANA)) {
            return;
        }

        $days = [];
        foreach ($xml->HORARIO_EXT->DIA_SEMANA as $day) {
            $days[(string)$day->DIA] = (string)$day->FRANJA;
        }

        $last_horario = false;
        $last_letter = false;
        $last_letter_marked = false;

        $horaries = [];

        foreach (['L', 'M', 'X', 'J', 'V', 'S', 'D'] as $key => $letter) {
            if (isset($days[$letter])) {
                $horary = $days[$letter];

                if ($horary != $last_horario) {
                    if (isset($horaries[$last_letter_marked]) && $last_letter_marked != $last_letter) {
                        $horaries[$last_letter_marked . '-' . $last_letter] = $horaries[$last_letter_marked];
                        unset($horaries[$last_letter_marked]);
                    }

                    $horaries[$letter] = $horary;
                    $last_horario = $horary;
                    $last_letter_marked  = $letter;
                } elseif ($key == 6) {
                    $horaries[$last_letter_marked . '-' . $last_letter] = $horaries[$last_letter_marked];
                    unset($horaries[$last_letter_marked]);
                }

                $last_letter = $letter;
            }
        }

        $return = [];
        $i = 0;

        foreach ($horaries as $key => $horary) {
            $return[] = [
                'key' => $i,
                'day' => [$key],
                'horary' => $horary,
                'valid' => true,
                'marked' => false
            ];

            $i++;
        }

        foreach ($return as $key => $data) {
            foreach ($return as $_key => $_data) {
                if ($_data['marked'] || !$_data['valid']) {
                    continue;
                }

                if (!empty($_data['horary']) && $_data['horary'] == $data['horary'] && $data['key'] != $_data['key']) {
                    $return[$key]['day'][] = $_data['day'][0];
                    $return[$key]['horary'] = $data['horary'];
                    $return[$_key]['valid'] = false;
                    $return[$key]['marked'] = true;
                }
            }
        }

        foreach ($return as $key => &$data) {
            if (!$data['valid']) {
                unset($return[$key]);
            }

            unset($data['marked']);
            unset($data['valid']);
            unset($data['key']);
        }

        return $this->arrangeFranjas($return);
    }

    public function arrangeFranjas($horaries)
    {
        foreach ($horaries as &$horary) {
            $horary['horary'] = str_replace(' - : a : - ', ' - ', $horary['horary']);
            $horary['horary'] = str_replace('a', __d('app', 'y'), $horary['horary']);

            $days = [
                'L' => __d('app', 'L'),
                'M' => __d('app', 'M'),
                'X' => __d('app', 'X'),
                'J' => __d('app', 'J'),
                'V' => __d('app', 'V'),
                'S' => __d('app', 'S'),
                'D' => __d('app', 'D'),
            ];

            foreach ($days as $letter => $translate) {
                $horary['day'] = str_replace($letter, $translate, $horary['day']);
            }
        }

        return $horaries;
    }
}
