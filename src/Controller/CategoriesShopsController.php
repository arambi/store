<?php
namespace Store\Controller;

use Store\Controller\AppController;
use Cake\Event\Event;
use Cake\Cache\Cache;

/**
 * CategoriesShops Controller
 *
 * @property \Store\Model\Table\CategoriesShopsTable $CategoriesShops
 */
class CategoriesShopsController extends AppController
{
  public function initialize() 
  {
    parent::initialize();
    $this->loadComponent( 'RequestHandler');
    $this->loadComponent( 'Store.ShopSearch');
    $this->loadComponent( 'Manager.Cache');
    $this->Auth->allow();
    $this->loadModel( 'Store.Products');
    $this->paginate = [
      'limit' => 18,
    ];
  }

  public function beforeFilter(Event $event)
  {
    parent::beforeFilter( $event);

    if( $this->request->is( ['ajax', 'json']))
    {
      $this->viewBuilder()->layout( 'ajax');
    }
  }

  public function category()
  {
    $content = $this->Products->CategoriesShops->find( 'slug', ['slug' => $this->request->params ['slug']])
      ->first();

    $this->paginate = [
      'Products' => [
        'limit' => 18,
      ]
    ];

    $query = $this->Products->findProducts();

    if( !$this->request->query( 'full'))
    {
      $query->innerJoinWith( 'CategoriesShops', function( $q)  use ($content){
          return $q->where(['CategoriesShops.id' => $content->id]);
      });
    }
      
    $this->ShopSearch->search( $query);
    // $products = $this->Cache->paginate( $query, $this->request->param ('slug'));
    $products = $this->paginate( $query);

    $products = $products->toArray();
    shuffle( $products);
    $this->set( compact( 'content', 'products'));
    $this->set( ['_serialize' => ['products']]);
  }

}
