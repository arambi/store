<?php
namespace Store\Controller;

use Store\Controller\AppController;

/**
 * OrderUserActions Controller
 *
 * @property \Store\Model\Table\OrderUserActionsTable $OrderUserActions
 */
class OrderUserActionsController extends AppController
{
  
  public function initialize() 
  {
    parent::initialize();
    $this->loadComponent( 'RequestHandler');
    $this->Auth->allow();
  }

  public function index()
  {
    $query = $this->OrderUserActions
      ->find( 'front')
    ;

    $contents = $this->paginate( $query);
    $this->set( compact( 'contents'));
  }

  public function view()
  {
    $content = $this->OrderUserActions
      ->find( 'front')
      ->first();
    
    if( !$content)
    {
      $this->Section->notFound();
    }

    $this->set( compact( 'content'));
  }
}
