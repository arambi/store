<?php
namespace Store\Controller\Component;

use ArrayObject;
use Cake\Log\Log;
use Cake\ORM\Entity;
use Store\Cart\Cart;
use Cake\Event\Event;
use Cake\Core\Configure;
use Website\Lib\Website;
use Store\Cart\ItemDraft;
use Cake\ORM\TableRegistry;
use Store\Payment\Checkout;
use Store\Config\StoreConfig;
use Cake\Controller\Component;
use Cake\Event\EventDispatcherTrait;
use Cake\Controller\ComponentRegistry;

/**
 * Cart component
 */
class CartComponent extends Component
{

  use EventDispatcherTrait;

  /**
   * Default configuration.
   *
   * @var array
   */
  protected $_defaultConfig = [];

/**
 * Components
 * @var array
 */
  public $components = ['Auth'];

/**
 * Propiedad para el objeto OrdersTable
 * @var Table
 */
  public $Orders;

  public $order = false;

/**
 * Key de la sesión del cart
 * @var string
 */
  public $orderSessionKey = 'Store.orderSalt';

/**
 * El país actual
 * @var string|null
 */
  public $defaultCountry = null;

/**
 * Initialize
 * 
 * @param  array  $config 
 * @return
 */
  public function initialize( array $config)
  {
    $request = $this->getController()->getRequest();
    
    if( $request->prefix == 'admin')
    {
      return;
    }

    $this->Orders = TableRegistry::getTableLocator()->get( 'Store.Orders');
    $this->Products = StoreConfig::getProductsModel();
    $this->Controller = $this->_registry->getController();
    Cart::setAuthComponent( $this->Auth);
    Cart::setSession( $request->getSession());
  }

  public function beforeFilter( Event $event)
  {
    if( $this->getController()->getRequest()->prefix == 'admin')
    {
      return;
    }

    $this->Controller->loadModel( 'Store.Promos');
  } 

  public function beforeRender( Event $event)
  {
    $request = $this->getController()->getRequest();

    if( $request->getSession()->check( $this->orderSessionKey))
    {
      $order = $this->order( true);

      if( $order)
      {
        if( $request->is(['patch', 'post', 'put']) && $request->getData( 'Orders'))
        {
          $data = $request->getData( 'Orders');

          
          if( !empty( $order->user_id))
          {
            $data ['user_id'] = $order->user_id;
          }

          $data = new ArrayObject($data);

          $event = new Event('Store.Controller.Orders.beforePatchEntity', $this, [
            'order' => $order,
            'data' => $data,
          ]);

          $this->getEventManager()->dispatch($event);
          
          $this->Orders->patchEntity( $order, (array)$data, [
            'validate' => 'checkout'
          ]);
        }
        else
        {
          $this->Orders->patchEntity( $order, $order->toArray(), [
            'validate' => 'checkout'
          ]);
        }

        $event = new Event( 'Store.Controller.Orders.beforeUpdate', $this, [
          'order' => $order,
        ]);

        $this->getEventManager()->dispatch( $event);

        if( !$this->Orders->save( $order)) {
        }

        $cart = $this->order()->for_cart;
        $this->_registry->getController()->set( 'cart', $cart);
      }
      else
      {
        $this->_registry->getController()->set( 'cart', null);
      }
    }
  }

/**
 * Get/Set
 *
 * Devuelve el país actual o lo setea
 * 
 * @param  string|null $iso_code El código ISO del paí´s
 * @return string           
 */
  public function country( $iso_code = null)
  {
    if( $iso_code)
    {
      $this->defaultCountry = $iso_code;
    }

    return $this->defaultCountry;
  }

/**
 * Indica si existe una orden en sesión
 * 
 * @return boolean 
 */
  public function hasOrder()
  {
    return !empty( $this->order) || $this->getController()->getRequest()->getSession()->check( $this->orderSessionKey);
  }

/**
 * Toma el order actual y si no hay, lo manda crear
 * 
 * @return Store\Model\Entity\Order Order
 */
  public function order( $renew = false)
  {
    $request = $this->getController()->getRequest();
    
    if( $request->prefix == 'admin')
    {
      return;
    }
    
    if( !$renew && $this->order)
    {
      return $this->order;
    }

    $this->order = $this->Orders->getOrder([
      'Orders.salt' => $request->getSession()->read( $this->orderSessionKey),
      'Orders.status IS NULL'
    ]);

    if( !empty( $this->order) && empty( $this->order->user_id) && $this->user( 'id'))
    {
      $this->order->user_id = $this->user( 'id');
      $this->Orders->save( $this->order, ['associated' => false]);
    }

    if( !empty( $this->order))
    {
      Cart::setOrder( $this->order);
    }
    
    return $this->order;
  }

  public function writeSession( Entity $order)
  {
    $this->getController()->getRequest()->getSession()->write( $this->orderSessionKey, $order->salt);
  }

/**
 * Borra la sesión actual
 */
  public function clearSession()
  {
    $this->getController()->getRequest()->getSession()->delete( $this->orderSessionKey);
    $this->order = null;
  }


/**
 * Devuelve el usuario actual
 * Devuelve un array con los datos del usuario o el valor del $key dado
 * 
 * @param  string|null $key El key del usuario a devolver
 * @return string|array
 */
  public function user( $key = null)
  {
    $user = $this->getController()->getRequest()->getSession()->read( 'Auth.User');

    if( !$user)
    {
      return null;
    }

    if( !$key)
    {
      return $user;
    }

    return $user [$key];
  }

  public function address( $type = 'delivery')
  {
    $order = $this->order();
    $user = $this->user();

    $addressProperty = 'address_'. $type;
    // $location = new RemoteLocation();
  }

/**
 * Añade un item al order actual
 * 
 * @param array $data
 * @return Order Entity
 */
  public function addItem( $data)
  {    
    $request = $this->getController()->getRequest();
    $isCreated = false;
    
    if( !$request->getSession()->check( $this->orderSessionKey) || !$this->order())
    {
      $user_id = $this->user( 'id');

      $PaymentMethods = TableRegistry::getTableLocator()->get( 'Store.PaymentMethods');
      $payments = $PaymentMethods->getValids( $this->Auth->user());
     
      if( $payments->count() == 1)
      {
        $payment_method_id = $payments->first()->id; 
      }
      else
      {
        $payment_method_id = null;
      }

      $order_defaults = array_merge( [
        'user_id' => $user_id,
        'carrier_id' => $this->Orders->Carriers->byDefaultId(),
        'payment_method_id' => $payment_method_id,
        'same_address' => 1,
        'save_adr_delivery' => 1,

      ], StoreConfig::getConfig( 'orderDefaults'));
      
      $created = $this->Orders->createOrder( $order_defaults);
      $isCreated = true;
      $this->writeSession( $created);
    }

    $draft = new ItemDraft( $data, $this->order());

    if( $draft->isValid())
    {
      $item = $this->Orders->addItem( $this->order()->id, $draft, isset( $created));
      $item->build();
      $item->set( 'price_real', $item->realPrice( $item->price, $item->tax_rate));

      $event = new Event( 'Store.Model.Order.afterAddItem', $this, [
        'item' => $item,
        $isCreated
      ]);
      
      $this->getEventManager()->dispatch( $event);
      return [
        'order' => $this->order( true)->for_cart,
        'item' => $item->toArray(),
        'errors' => !empty( $draft->errors()) ? $draft->errors() : false,
        'valid' => true
      ];
    }
    else
    {
      return [
        'errors' => $draft->errors(),
        'valid' => false
      ];
    }    
  }

/**
 * Modifica la cantidad de un line_item
 *
 * @param string $id LineItem::id
 * @param string $action 'add' | 'remove'
 * @param mixed boolean|integer $quantity
 * @return void
 */
  public function itemQuantity( $id, $action, $quantity = 1)
  {
    $item = $this->Orders->LineItems->find()
      ->where([
        'LineItems.id' => $id,
        'LineItems.order_id' => $this->order()->id
      ])
      ->contain([
        'Products' => function( $q){
          return $q->find( 'products')
            ->contain('Taxes');
        },
        'ProductAttributes' => [
          'Attributes'
        ],
      ])
      ->first();
    
    if( !$item)
    {
      return false;
    }

    if( $action == 'add')
    {
      if( !empty( $item->product_attribute))
      {
        foreach( $item->product_attribute->attributes as $attr)
        {
          $attributes [] = [
            'group_id' => $attr->attribute_group_id,
            'attribute_id' => $attr->id
          ];
        }
      }
      else
      {
        $attributes = null;
      }

      $draft = new ItemDraft([
        'salt' => $item->product->salt,
        'attributes' => $attributes,
        'quantity' => $quantity
      ], $this->order());
      
      if( $draft->isValid())
      {
        $quantity_final = $item->quantity + $quantity;
      }
      else
      {
        return [
          'draft' => $draft,
          'item' => false,
          'order' => $this->order(),
          'errors' => $draft->errors()
        ];
      }
    }
    elseif( $action == 'remove')
    {
      $quantity_final = $item->quantity - $quantity;
    }

    if( $quantity_final == 0)
    {
      // Event
      $event = new Event( 'Store.Model.Order.afterDeleteItem', $this, [
        'item' => $item,
      ]);
      
      $this->getEventManager()->dispatch( $event);
      $this->Orders->LineItems->delete( $item);
    }
    else
    {
      $item->set( 'quantity', $quantity_final);
      $this->Orders->LineItems->save( $item);
      $item->build();
      $item->set( 'price_real', $item->realPrice( $item->price, $item->tax_rate));
      // Event
      $event = new Event( 'Store.Model.Order.afterQuantityItem', $this, [
        'item' => $item,
      ]);
      
      $this->getEventManager()->dispatch( $event);
    }
      
    if( isset( $draft))
    {
      return [
        'draft' => $draft,
        'item' => $item
      ];
    }

    return [
      'item' => $item,
      'draft' => false
    ];
  }

/**
 * Añade puntos al pedido actual
 * 
 * @param integer $points
 */
  public function addPoints( $points)
  {
    $availables = $this->Orders->PointsUp->totalAvailables( $this->Auth->user( 'id'), $this->order());

    // No hay puntos suficientes para el usuario
    if( $points > $availables)
    {
      return false;
    }

    $id = null;
    
    // El pedido actual ya tiene puntos
    if( !empty( $this->order()->points_down))
    {
      $id = $this->order()->points_down->id;
    }

    $value = Website::get( 'settings.store_points_down_value');

    $discount = $points * $value;

    $total = ($this->order()->subtotal + $this->order()->taxes);

    if( $discount > $total)
    {
      // $points = round( $points - (($discount - $total) / $value));

    }

    $this->Orders->PointsUp->usePoints( $points, $this->Auth->user( 'id'), $this->order(), $id);
    return $this->order( true);
  }

/**
 * Elimina el item de la cesta de la compra
 *
 * @param integer $id Lineitem::id
 * @return void
 */
  public function deleteItem( $id)
  {
    $item = $this->Orders->deleteItem( $id, $this->order());
    $item->set( 'price_real', $item->realPrice( $item->price, $item->tax_rate));
    $event = new Event( 'Store.Model.Order.afterDeleteItem', $this, [
      'item' => $item,
    ]);
    
    $this->getEventManager()->dispatch( $event);

    return [
      'order' => $this->order( true)->for_cart,
      'item' => $item
    ];
  }

/**
 * Realiza el procesamiento de un pago, llamando a Store\Payment\Checkout
 * 
 * @return \Store\Payment\PaymentResponse
 */
  public function payment( $order)
  {
    $request = $this->getController()->getRequest();
    $response = Checkout::proccess( $order, $request);
    $response->set( 'order_id', $order->id);    
    TableRegistry::getTableLocator()->get( 'Store.Payments')->savePayment( $response->toArray());

    if( $response->success())
    {
      $order = $this->Orders->finalize( $order, $response->status);
      
      if( Website::get( 'settings.store_has_points') && !empty( $order->user_id))
      {
        $this->Orders->PointsUp->addPointsFromOrder( $order);
      }

      $this->clearSession();
    }
    
    return $response;
  }

  public function isValidForAddress( $order)
  {
    return !empty( $order->line_items);
  }

  public function isValidForCheckout( $order)
  {
    return !is_null( $order) && !(empty( $order->address_delivery) || empty( $order->address_invoice) || !$order->has_portable_products);
  }

  public function isValidForPayment( $order)
  {
    return $order->total == 0 || !empty( $order->payment_method);
  }

}
