<?php
namespace Store\Controller\Component;

use Cake\Controller\Component;
use Cake\Controller\ComponentRegistry;

/**
 * ShopSearch component
 */
class ShopSearchComponent extends Component
{

/**
 * Modifica el query dependiendo de los valores que haya en el queryString del URL
 * 
 * @param  Cake\ORM\Query $query
 * @return  Cake\ORM\Query
 */
  public function search( $query)
  {
    if( $this->request->query( 'price_min'))
    {
      $query->where([
        'Products.store_price >'. $this->request->query( 'price_min')
      ]);
    }

    if( $this->request->query( 'price_max'))
    {
      $query->where([
        'Products.store_price <'. $this->request->query( 'price_max')
      ]);
    }

    if( $this->request->query( 'tag'))
    {
      $query->innerJoinWith( 'Tags', function( $q) {
            return $q->where(['Tags.slug' => $this->request->query( 'tag')]);
        });
    }

    if( $this->request->query( 'category'))
    {
      $query->innerJoinWith( 'CategoriesShops', function( $q) {
            return $q->where(['CategoriesShops.id' => $this->request->query( 'category')]);
        });
    }

    if( $this->request->query( 'q'))
    {
      $query->find( 'fulltext', ['text' => $this->request->query('q')]);
      // $text = $this->request->query( 'q');
      // $query->andWhere(function( $exp, $query) use( $text){
      //   return $exp->or_([
      //     'Products_title_translation.content LIKE' => '%'. $text . '%',
      //     'Products_subtitle_translation.content LIKE' => '%'. $text . '%',
      //     'Products_summary_translation.content LIKE' => '%'. $text . '%',
      //     'Products_body_translation.content LIKE' => '%'. $text . '%',
      //   ]);
      // });
    }

    if( $this->request->query( 'color'))
    {
      $query->where([
        'ProductsColors.attribute_id' => $this->request->query( 'color'),
      ]);
    }

    return $query;
  }
}
