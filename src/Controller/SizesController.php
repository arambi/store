<?php
namespace Store\Controller;

use Store\Controller\AppController;

/**
 * Sizes Controller
 *
 * @property \Store\Model\Table\SizesTable $Sizes
 */
class SizesController extends AppController
{
  public function initialize() 
  {
    parent::initialize();
    $this->Auth->allow();
  }

  public function view( $id)
  {
    $this->viewBuilder()->layout( 'ajax');
    $content = $this->Sizes->find()
      ->where(['Sizes.id' => $id])
      ->first();

    $this->set( compact( 'content'));
  }
}
