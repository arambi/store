<?php
namespace Store\Controller;

use Store\Controller\AppController;
use Cofree\Lib\RemoteLocation;
use Cake\Core\Configure;
/**
 * Addresses Controller
 *
 * @property \Store\Model\Table\AddressesTable $Addresses
 */
class AddressesController extends AppController
{

  public function initialize()
  {
    parent::initialize();
    $this->loadComponent( 'RequestHandler');
    $this->loadComponent( 'Store.Cart');
  }

  public function add()
  {
    $this->__sessionRefererSave();

    $this->__setCountries();
    $address = $this->Addresses->newEntity();

    if( $this->request->is(['patch', 'post', 'put']))
    {
      $this->request->data ['Addresses']['country_id'] = str_replace( 'string:', '', $this->request->data ['Addresses']['country_id']);
      $address = $this->Addresses->patchEntity( $address, $this->request->data);
      $address->set( 'user_id', $this->Auth->user( 'id'));

      if( $this->Addresses->save( $address))
      { 
        $redirect = $this->__sessionRefererRead();
        $this->request->session()->delete( 'AddressEdit');
        return $this->redirect( $redirect);
      }
      else
      {
        $this->__setCountries();
        
        if( $this->request->data( 'Addresses.country_id'))
        {
          $states = $this->Addresses->States->find('list')->where([
              'States.country_id' => $this->request->data( 'Addresses.country_id')
          ])->all();

          $this->set( compact( 'states'));
        }
      }
    }
    else
    {
      $location = new RemoteLocation();

      $country = $this->Addresses->Countries->getByCode( $location->country_code);

      if( $country)
      {
        $address->set( 'country_id', $country->id);
        $states = $this->Addresses->States->find('list')->where([
            'States.country_id' => $country->id
        ])->all();

        $this->set( compact( 'states'));
      }
    }

    $this->set( compact( 'address'));
  }

/**
 * Edición de una dirección
 * 
 * @return void
 */
  public function edit()
  {
    $this->__sessionRefererSave();

    $address = $this->Addresses->findBySalt( $this->request->param( 'salt'))
      ->contain([
        'Countries',
        'States'
      ])
      ->first();

    if( !$address)
    {
      throw new \Cake\Network\Exception\NotFoundException( __( 'Página no encontrada'));
    }

    if( $this->request->is( ['patch', 'post', 'put']))
    {
      $this->Addresses->patchEntity( $address, $this->request->data);

      if( $this->Addresses->save( $address))
      {
        return $this->redirect( $this->__sessionRefererRead());
      }
    }
    else
    {
      
    }

    $this->set( compact( 'address'));

    if( $address->country)
    {
      $address->set( 'country_id', $address->country->id);
      $states = $this->Addresses->States->find('list')->where([
          'States.country_id' => $address->country->id
      ])->all();

      $this->set( compact( 'states'));  
    }

    $this->__setCountries();
  }

  public function delete( $salt = null)
  {
    $address = $this->Addresses->find()
      ->where([
          'Addresses.salt' => $salt,
          'Addresses.user_id' => $this->Auth->user( 'id')
        ])
      ->first();

    if( !$address)
    {
      throw new \Cake\Network\Exception\NotFoundException( __( 'Página no encontrada'));
    }

    $this->Addresses->delete( $address);
    return $this->redirect( $this->request->referer());
  }

  private function __setCountries()
  {
    $countries = $this->Addresses->Countries
      ->find( 'list')
      ->where([
        'active' => true
      ])
    ;

    if( Configure::read( 'I18n.behavior') == 'I18n.I18nTranslate')
    {
      $countries->order([
        'Countries.title'
      ]);
    }
    else
    {
      $countries->order([
        'Countries_title_translation__content'
      ]);
    }

    $this->set( 'countries', $countries->all());
  }
/**
 * Guarda la URL referer para que después de la edición de la dirección se le reenvie al usuario
 * Los usuarios llegan a la edición de dirección desde distintos lugares
 * 
 * @return void
 */
  private function __sessionRefererSave()
  {
    if( !$this->request->session()->check( 'AddressEdit'))
    {
      $this->request->session()->write( 'AddressEdit', $this->request->referer());
    }
  }

/**
 * Devuelve la URL referer que ha sido guardada cuando el usuario llegó a la edición de una dirección
 * 
 * @return void
 */
  private function __sessionRefererRead()
  {
    if( $this->request->session()->check( 'AddressEdit'))
    {
      $url = $this->request->session()->read( 'AddressEdit');
      $this->request->session()->delete( 'AddressEdit');
      return $url;
    }

    return $this->Section->url([
      'plugin' => 'Store',
      'controller' => 'Users',
      'action' => 'index'
    ]);
  }
}
