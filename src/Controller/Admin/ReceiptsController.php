<?php
namespace Store\Controller\Admin;

use Manager\Controller\CrudControllerTrait;
use Store\Controller\AppController;

/**
 * Receipts Controller
 *
 * @property \Store\Model\Table\ReceiptsTable $Receipts
 */
class ReceiptsController extends AppController
{
    use CrudControllerTrait;
}
