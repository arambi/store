<?php
namespace Store\Controller\Admin;

use Store\Controller\AppController;
use Manager\Controller\CrudControllerTrait;

/**
 * ProductRelateds Controller
 *
 * @property \Store\Model\Table\ProductRelatedsTable $ProductRelateds
 */
class ProductRelatedsController extends AppController
{
  use CrudControllerTrait;
}
