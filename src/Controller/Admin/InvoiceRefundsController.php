<?php

namespace Store\Controller\Admin;

use Manager\Controller\CrudControllerTrait;
use Store\Controller\AppController;

/**
 * InvoiceRefunds Controller
 *
 * @property \Store\Model\Table\InvoiceRefundsTable $InvoiceRefunds
 */
class InvoiceRefundsController extends AppController
{
    use CrudControllerTrait;

    public function create()
    {
        $data = $this->request->getData();
        $this->CrudTool->setAction('create');

        $content = $this->Table->getNewEntity($data);


        if (!empty($this->request->query)) {
            foreach ($this->request->query as $key => $value) {
                $content->set($key, $value);
            }
        }

        if ($this->request->is('post')) {
            $this->getTableLocator()->get('Store.InvoiceRefunds')
                ->createFromConcept($data['order_id'], $data['concept'], $data['price'], $data['tax_rate']);

            $this->CrudTool->addSerialized([
                'redirect' => '/admin/store/invoice_refunds'
            ]);
        } else {
            $this->Table->crud->setContent($this->Table->emptyEntityProperties($content));
        }
    }
}
