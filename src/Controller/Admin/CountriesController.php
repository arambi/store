<?php
namespace Store\Controller\Admin;

use Store\Controller\AppController;
use Manager\Controller\CrudControllerTrait;

/**
 * Countries Controller
 *
 * @property \Store\Model\Table\CountriesTable $Countries
 */
class CountriesController extends AppController
{
  use CrudControllerTrait;
}
