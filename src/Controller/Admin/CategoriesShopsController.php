<?php
namespace Store\Controller\Admin;

use Store\Controller\AppController;
use Manager\Controller\CrudControllerTrait;

/**
 * CategoriesShop Controller
 *
 * @property \Taxonomy\Model\Table\CategoriesShopTable $CategoriesShop
 */
class CategoriesShopsController extends AppController
{
  use CrudControllerTrait;

  public function index()
  {
    $this->CrudTool->addSerialized( [
      'contents' => $this->CategoriesShops->tree()
    ]);
  }
}
