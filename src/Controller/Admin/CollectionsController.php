<?php
namespace Store\Controller\Admin;

use Manager\Controller\CrudControllerTrait;
use Store\Controller\AppController;

/**
 * Collections Controller
 *
 * @property \Store\Model\Table\CollectionsTable $Collections
 */
class CollectionsController extends AppController
{
    use CrudControllerTrait;

    protected function _beforeUpdateFind( $query)
    {
      $query->contain([
        'ProductsColors.Products',
        'Products'
      ]);
      $query->formatResults( function( $results){
         return $results->map(function( $row){
            foreach( $row ['products'] as &$product)
            {
              foreach( (array)$product ['photos'] as $photo)
              {
                if( $photo->main)
                {
                  $product['photo_main'] = $photo;
                  break;
                }
              }
            }
            
            return $row;
         });
      });
    }
}
