<?php
namespace Store\Controller\Admin;

use Store\Controller\AppController;
use Manager\Controller\CrudControllerTrait;

/**
 * Taxes Controller
 *
 * @property \Store\Model\Table\TaxesTable $Taxes
 */
class TaxesController extends AppController
{
  use CrudControllerTrait;
}
