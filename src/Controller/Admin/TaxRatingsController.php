<?php
namespace Store\Controller\Admin;

use Manager\Controller\CrudControllerTrait;
use Store\Controller\AppController;

/**
 * TaxRatings Controller
 *
 * @property \Store\Model\Table\TaxRatingsTable $TaxRatings
 */
class TaxRatingsController extends AppController
{
    use CrudControllerTrait;
}
