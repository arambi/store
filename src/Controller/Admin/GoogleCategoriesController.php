<?php
namespace Store\Controller\Admin;

use Store\Controller\AppController;
use Manager\Controller\CrudControllerTrait;

/**
 * GoogleCategories Controller
 *
 * @property \Store\Model\Table\GoogleCategoriesTable $GoogleCategories
 *
 * @method \Store\Model\Entity\GoogleCategory[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class GoogleCategoriesController extends AppController
{
    use CrudControllerTrait;

    public function autocomplete()
    {
        $contents = $this->Table->find()->where([
            'GoogleCategories.path LIKE' => '%' . $this->request->query['q'] . '%'
        ])->all();

        $this->CrudTool->addSerialized([
            'results' => $contents->toArray()
        ]);
    }
}
