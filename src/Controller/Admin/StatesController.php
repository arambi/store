<?php
namespace Store\Controller\Admin;

use Store\Controller\AppController;
use Manager\Controller\CrudControllerTrait;

/**
 * States Controller
 *
 * @property \Store\Model\Table\StatesTable $States
 */
class StatesController extends AppController
{
  use CrudControllerTrait;
}
