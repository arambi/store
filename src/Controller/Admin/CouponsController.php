<?php

namespace Store\Controller\Admin;

use Manager\Controller\CrudControllerTrait;
use Store\Controller\AppController;

/**
 * Coupons Controller
 *
 * @property \Store\Model\Table\CouponsTable $Coupons
 */
class CouponsController extends AppController
{
    use CrudControllerTrait;

    protected function _index($query)
    {
        $query->where([
            'Coupons.is_giftcard' => false
        ]);
    }
}
