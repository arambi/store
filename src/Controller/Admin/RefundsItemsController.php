<?php
namespace Store\Controller\Admin;

use Manager\Controller\CrudControllerTrait;
use Store\Controller\AppController;

/**
 * RefundsItems Controller
 *
 * @property \Store\Model\Table\RefundsItemsTable $RefundsItems
 */
class RefundsItemsController extends AppController
{
    use CrudControllerTrait;
}
