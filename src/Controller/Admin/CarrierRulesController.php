<?php

namespace Store\Controller\Admin;

use Manager\Controller\CrudControllerTrait;
use Store\Controller\AppController;

/**
 * CarrierRules Controller
 *
 * @property \Store\Model\Table\CarrierRulesTable $CarrierRules
 */
class CarrierRulesController extends AppController
{
    use CrudControllerTrait {
        index as indexCrud;
    }


    public function index($carrier_id = null)
    {
        $this->indexCrud();
    }

    protected function _index($query)
    {
        if ($carrier_id = $this->request->getParam('pass.0')) {
            $query->where([
                'CarrierRules.carrier_id' => $carrier_id,
            ]);

            $this->CarrierRules->crud->set('carrier_id', $carrier_id);
        }
    }

    protected function _beforeCreate(&$data)
    {
        if ($carrier_id = $this->request->getQuery('carrier_id')) {
            $data['carrier_id'] = $carrier_id;

            $this->set([
                'parentContent' => $this->getTableLocator()->get('Store.Carriers')->find()
                    ->where([
                        'Carriers.id' => $carrier_id,
                    ])
                    ->contain('Taxes')
                    ->first()
            ]);

            $this->CarrierRules->crud->set('carrier_id', $carrier_id);
        }
    }
}
