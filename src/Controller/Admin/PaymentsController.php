<?php
namespace Store\Controller\Admin;

use Manager\Controller\CrudControllerTrait;
use Store\Controller\AppController;

/**
 * Payments Controller
 *
 * @property \Store\Model\Table\PaymentsTable $Payments
 */
class PaymentsController extends AppController
{
    use CrudControllerTrait;
}
