<?php
namespace Store\Controller\Admin;

use Manager\Controller\CrudControllerTrait;
use Store\Controller\AppController;
use Cake\Collection\Collection;
use Cake\Mailer\MailerAwareTrait;

/**
 * PointsAdds Controller
 *
 * @property \Store\Model\Table\PointsAddsTable $PointsAdds
 */
class PointsAddsController extends AppController
{
  use CrudControllerTrait;
  use MailerAwareTrait;

  protected function _beforeCreate()
  {
    $this->_beforeUpdate();
  }

  protected function _beforeUpdate()
  {
    if( $this->request->is( ['patch', 'post', 'put']))
    {
      $json = [
        'countries' => $this->request->data( 'countries'),
        'states' => $this->request->data( 'states'),
        'groups' => $this->request->data( 'groups'),
        'points' => $this->request->data( 'points'),
        'userstop' => $this->request->data( 'userstop'),
        'usersfrom' => $this->request->data( 'usersfrom'),
        'usersto' => $this->request->data( 'usersto'),
      ];

      $this->request->data ['data'] = $json;
      $this->request->data ['user_id'] = $this->Auth->user( 'id');
    }
  }


  protected function _beforeUpdateFind( $query, $content)
  {
    if( $this->request->is( ['patch', 'post', 'put'])) 
    {
      $this->loadModel( 'Store.PointsAddsSends');
      $users = $this->PointsAddsSends->findUsers( $content);
      $this->PointsAddsSends->add( $users, $content->id);
    }
  }


  public function search()
  {
    $this->CrudTool->serializeAction( false);
    $this->loadModel( 'Store.PointsAddsSends');

    $users = $this->PointsAddsSends->find()
      ->where([
        'PointsAddsSends.send_id' => $this->request->data( 'id'),
      ])
      ->where(function ($exp, $query) {
        return $exp
          ->or_([
            'Users.name LIKE' => '%'. $this->request->data( 'query') .'%',
            'Users.email LIKE' => '%'. $this->request->data( 'query') .'%',
          ]);
      })
      ->contain([
        'Users'
      ]);

    $this->set( compact( 'users'));
    $this->set( '_serialize', true);
  }

  public function preview( $id)
  {
    $this->CrudTool->serializeAction( false);

    $content = $this->Table->get( $id);

    if( !filter_var( $this->request->data( 'email'), FILTER_VALIDATE_EMAIL)) 
    {
      $this->set([
        'text' => 'El email no es válido',
        'type' => 'error'
      ]);
    }
    else
    {
      $this->loadModel( 'Store.PointsAddsSends');
      $send = $this->PointsAddsSends->newEntity([
        'email' => $this->request->data( 'email')
      ]);

      $send->points_add = $content;

      $this->getMailer( 'Store.PointsSend')->send( 'sending', [
          $send,
      ]);

      $this->set([
        'text' => 'El email ha sido enviado',
        'type' => 'success'
      ]);
    }
  
    $this->set( '_serialize', true);
  }
}
