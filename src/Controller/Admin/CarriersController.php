<?php
namespace Store\Controller\Admin;

use Manager\Controller\CrudControllerTrait;
use Store\Controller\AppController;

/**
 * Carriers Controller
 *
 * @property \Store\Model\Table\CarriersTable $Carriers
 */
class CarriersController extends AppController
{
    use CrudControllerTrait;

    protected function _beforeUpdateFind($query)
    {
        $this->Carriers->CarrierRules->hasOne( 'Zone', [
          'className' => 'store_carrier_rule_zones',
          'foreignKey' => 'carrier_rule_id',
        ]); 

        $this->Carriers->CarrierRules->Zone->belongsTo( 'Zones', [
            'className' => 'Store.Zones',
            'foreignKey' => 'zone_id',
          ]); 

        // $query->contain([
        //     'CarrierRules' => function($q) {
        //         return $q
        //             ->group([
        //                 'CarrierRules.id'
        //             ])
        //             ->contain([
        //                 'Zones',
        //                 'Zone'
        //             ])
        //             ->order([
        //                 'Zone.zone_id'
        //             ]);
        //     }
        // ]);

    }
}
