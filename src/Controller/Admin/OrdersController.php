<?php

namespace Store\Controller\Admin;

use Store\Cart\Cart;
use Cake\Event\Event;
use Cake\Utility\Hash;
use Cake\Core\Configure;
use Cake\Database\Query;
use Cake\Routing\Router;
use Website\Lib\Website;
use Cake\ORM\TableRegistry;
use Store\Config\StoreConfig;
use Store\Controller\AppController;
use Manager\Controller\CrudControllerTrait;

/**
 * Orders Controller
 *
 * @property \Store\Model\Table\OrdersTable $Orders
 */
class OrdersController extends AppController
{
    use CrudControllerTrait;

    protected function _index($query)
    {
        $query->find('order', [
            'noPayment' => true
        ]);

        $query->group('Orders.id');
        $query->where([
            'Orders.status IS NOT NULL'
        ]);

        $query->order([
            'Orders.order_date' => 'desc'
        ]);

        $event = new Event('Store.Controller.Orders.index', $this, ['query' => $query]);
        $this->getEventManager()->dispatch($event);
    }

    public function unfinish()
    {
        $this->Orders->hasOne('Payments', [
            'className' => 'Payment.Payments',
            'foreignKey' => 'order_id',
        ]);

        $query = $this->Orders->find()
            ->where([
                'Orders.status IS NULL',
                'Payments.id IS NOT NULL'
            ])
            ->group([
                'Orders.id'
            ])
            ->contain([
                'Payments'
            ])
            ->order([
                'Orders.modified' => 'desc'
            ]);

        $this->Filter->query($query);

        $this->paginate['limit'] = $this->Table->crud->limit() ? $this->Table->crud->limit() : 20;

        if ($this->request->params['_ext'] === 'csv') {
            $this->exportCSV($query);
        } else {
            $this->CrudTool->addSerialized([
                'contents' => $this->paginate($query),
            ]);
        }
    }

    public function abandoned()
    {
        // $this->Orders->hasOne('Payments', [
        //     'className' => 'Payment.Payments',
        //     'foreignKey' => 'order_id',
        // ]);

        $query = $this->Orders->find()
            ->where([
                'Orders.status IS NULL',
                'Orders.payment_method_id IS NULL'
            ])
            ->group([
                'Orders.id'
            ])
            // ->contain([
            //     'Payments'
            // ])
            ->order([
                'Orders.modified' => 'desc'
            ]);


        $this->Filter->query($query);

        $this->paginate['limit'] = $this->Table->crud->limit() ? $this->Table->crud->limit() : 20;

        if ($this->request->params['_ext'] === 'csv') {
            $this->exportCSV($query);
        } else {
            $this->CrudTool->addSerialized([
                'contents' => $this->paginate($query),
            ]);
        }
    }

    public function view($id = false)
    {
        if (StoreConfig::getConfig('adminWithoutTaxes')) {
            $site = Website::get();
            $settings = $site->settings;
            $settings->store_price_with_taxes = false;
        }

        $id = $id ? $id : ($this->request->getData('id') ? $this->request->getData('id') : false);

        $this->CrudTool->addSerialized([
            'statuses' => $this->Table->statuses
        ]);

        foreach (['state_delivery', 'country_delivery', 'state_invoice', 'state_invoice'] as $field) {
            $this->setRequest($this->request->withoutData($field));
        }

        if ($this->request->is(['patch', 'post', 'put'])) {
            $this->update($id);
        }
        $this->CrudTool->setAction('view');
        $this->Table->crud->addAssociations(['Products']);

        $query = $this->Table->find('order')
            ->where([$this->Table->alias() . '.id' => $id])
            ->formatResults(function ($result) {
                return $result->map(function ($row) {
                    $row->set('invoice_url_original', $row->invoice_url_original);
                    return $row;
                });
            })
            ->contain([
                'Users.Groups'
            ]);

        $event = new Event('Store.Controller.Orders.view', $this, ['query' => $query]);
        $this->getEventManager()->dispatch($event);

        $array = $query->first();
        $this->Table->crud->setContent($array);
    }

    public function finalize()
    {
        Cart::setSession($this->getRequest()->getSession());

        $order = $this->Orders->find('order')->where([
            'Orders.salt' => $this->getRequest()->getData('salt')
        ])->first();

        $event = new Event('Store.Controller.Orders.adminFinalize', $this, [
            $order
        ]);

        $this->getEventManager()->dispatch($event);
        $this->Orders->finalize($order, 'accepted');
    }

    public function cart($id = false)
    {
        if (StoreConfig::getConfig('adminWithoutTaxes')) {
            $site = Website::get();
            $settings = $site->settings;
            $settings->store_price_with_taxes = false;
        }

        $id = $id ? $id : ($this->request->getData('id') ? $this->request->getData('id') : false);

        $this->CrudTool->setAction('cart');
        $this->Table->crud->addAssociations(['Products']);

        $this->Orders->hasMany('Payments', [
            'className' => 'Payment.Payments',
            'foreignKey' => 'order_id',
        ]);

        $array = $this->Orders->find('order')
            ->where([$this->Orders->getAlias() . '.id' => $id])
            ->contain([
                'Users.Groups',
                'Payments',
            ])
            ->formatResults(function ($results) {
                return $results->map(function ($row) {
                    foreach ($row->payments as $payment) {
                        $payment->set('response_code', $payment->response_code);
                    }

                    return $row;
                });
            })
            ->first();

        $this->Table->crud->setContent($array);
    }

    public function abandoned_cart($id = false)
    {
        if (StoreConfig::getConfig('adminWithoutTaxes')) {
            $site = Website::get();
            $settings = $site->settings;
            $settings->store_price_with_taxes = false;
        }

        $id = $id ? $id : ($this->request->getData('id') ? $this->request->getData('id') : false);

        $this->CrudTool->setAction('abandoned_cart');
        $this->Table->crud->addAssociations(['Products']);

        $array = $this->Orders->find('order')
            ->where([$this->Orders->getAlias() . '.id' => $id])
            ->contain([
                'Users.Groups',
            ])
            ->first();

        // \Cake\Log\Log::debug($array);

        $this->Table->crud->setContent($array);
    }

    public function states()
    {
        $this->loadModel('Store.States');
        $states = $this->States->find('list')
            ->order([
                'States.title'
            ])
            ->where([
                'States.country_id' => (int)$this->request->getData('country_id')
            ]);

        $options = [];

        foreach ($states as $key => $value) {
            $options[] = [
                'key' => $key,
                'value' => $value
            ];
        }

        $this->set([
            'states' => $options,
            '_serialize' => ['states']
        ]);
    }

    public function change_status()
    {
        $this->CrudTool->serializeAction(false);

        $status = $this->Orders->addStatus($this->request->getData('salt'), $this->request->getData('status'), $this->Auth->user('id'));

        if ($status) {
            $order = $this->Orders->find('order')
                ->where([
                    'Orders.salt' => $this->request->getData('salt')
                ])->first();

            $this->set([
                'status_human' => $order->status_human,
                'result' => $order->order_histories,
                'order' => $order,
                '_serialize' => ['result', 'status_human', 'order'],
            ]);
        } else {
            $this->set([
                'result' => false,
                '_serialize' => ['result'],
            ]);
        }
    }

    public function invoice($salt)
    {
        $cart = $this->Orders->find('order')
            ->where([
                'Orders.salt' => $salt,
            ])->first();

        $this->set(compact('cart'));
    }

    public function statistics()
    {
        $this->loadComponent('Chart.Stats');
        $this->loadComponent('Chart.Chart');

        $charts = [];

        $this->Orders->crud->addConfig([
            'stats' => [
                'charts' => [
                    'orders' => $this->__chartOrders(),
                    'carts' =>  $this->__chartCarts(),
                    'totals' => $this->__chartTotals(),
                    'views' => $this->__productViews(),
                    'items' => $this->__lineItems(),
                ],
                'tables' => [
                    'products' => $this->__topProducts(),
                    'product_attributes' => $this->__topProductAttributes(),
                    'product_views' => $this->__topProductsViews(),
                ],
                'form' => [
                    'date_start' => $this->Stats->dateStart(),
                    'date_end' => $this->Stats->dateEnd(),
                    'group_id' => $this->request->getQuery('group_id'),
                ],
                'groups' => $this->__setGroups()
            ]
        ]);
    }

    public function remove_taxes()
    {
        $order = $this->Orders->find()
            ->contain('LineItems')
            ->where([
                'Orders.status IS NOT NULL',
                'Orders.id' => $this->request->getData('order_id')
            ])
            ->first();

        if (!$order) {
            return;
        }

        foreach ($order->line_items as $item) {
            $item->set([
                'tax_rate' => 0,
            ]);

            $this->Orders->LineItems->query()->update()
                ->set([
                    'tax_rate' => 0,
                ])
                ->where([
                    'id' => $item->id
                ])
                ->execute();
        }

        $order->set([
            'shipping_handling_tax' => 0,
            'taxes' => 0,
        ]);


        $this->Orders->setTotal($order);

        $this->Orders->query()->update()
            ->set([
                'shipping_handling_tax' => 0,
                'taxes' => 0,
                'total' => $order->total
            ])
            ->where([
                'id' => $order->id
            ])
            ->execute();
    }

    private function __chartOrders()
    {
        $line = $this->__createChart(__d('admin', 'Pedidos'), 'areaspline');
        $query = $this->__getStatsOrderQuery();

        $this->Stats
            ->conditions('Orders.order_date', $query)
            ->group('Orders.order_date', $query)
            ->select('Orders.order_date', $query);

        $query->select(['number' => $query->func()->count('*')]);

        $entries = $this->Stats->build($query);

        $line->addSerie(array(
            'name' => __d('admin', 'Pedidos'),
            'data' => array_map('intval', Hash::extract($entries, '{n}.number'))
        ), 'ref');

        return $line->options;
    }

    private function __chartCarts()
    {
        $line = $this->__createChart(__d('admin', 'Pedidos / Carritos'), 'areaspline');

        $query = $this->__getStatsOrderQuery();
        $this->Stats
            ->conditions('Orders.created', $query)
            ->group('Orders.created', $query)
            ->select('Orders.created', $query);

        $query->select(['number' => $query->func()->count('*')]);
        $entries = $this->Stats->build($query);
        $line->addSerie(array(
            'name' => __d('admin', 'Carritos'),
            'data' => array_map('intval', Hash::extract($entries, '{n}.number'))
        ), 'ref');

        $query = $this->__getStatsOrderQuery();

        $this->Stats
            ->conditions('Orders.created', $query)
            ->group('Orders.created', $query)
            ->select('Orders.created', $query);

        $query->where([
            'Orders.status IS NOT NULL'
        ]);

        $query->select(['number' => $query->func()->count('*')]);
        $entries = $this->Stats->build($query);
        $line->addSerie(array(
            'name' => __d('admin', 'Pedidos'),
            'data' => array_map('intval', Hash::extract($entries, '{n}.number'))
        ), 'ref');

        return $line->options;
    }

    private function __chartTotals()
    {
        $line = $this->__createChart(__d('admin', 'Volumen de negocio'), 'areaspline');
        $query = $this->__getStatsOrderQuery();

        $this->Stats
            ->conditions('Orders.order_date', $query)
            ->group('Orders.order_date', $query)
            ->select('Orders.order_date', $query);

        $query->select(['number' => $query->func()->sum('Orders.total')]);

        $entries = $this->Stats->build($query);
        $line->addSerie(array(
            'name' => __d('admin', '€'),
            'data' => array_map('intval', Hash::extract($entries, '{n}.number'))
        ), 'ref');

        return $line->options;
    }

    private function __productViews()
    {
        $this->loadModel('Store.ProductViews');
        $this->loadModel('Store.CategoriesShops');
        $line = $this->__createChart(__d('admin', 'Productos vistos'), 'spline');
        $query = $this->ProductViews->find();

        $this->Stats
            ->conditions('ProductViews.created', $query)
            ->group('ProductViews.created', $query)
            ->select('ProductViews.created', $query);

        $query->select(['number' => $query->func()->count('*')]);

        $entries = $this->Stats->build($query);
        $line->addSerie(array(
            'name' => __d('admin', 'Todos'),
            'data' => array_map('intval', Hash::extract($entries, '{n}.number'))
        ), 'ref');

        $categories = $this->CategoriesShops->find();

        foreach ($categories as $category) {
            $query = $this->ProductViews->find();

            $this->Stats
                ->conditions('ProductViews.created', $query)
                ->group('ProductViews.created', $query)
                ->select('ProductViews.created', $query);

            $query->select(['number' => $query->func()->count('*')]);

            $product_ids = $this->ProductViews->Products->find()
                ->innerJoinWith('CategoriesShops', function ($q) use ($category) {
                    return $q->where(['CategoriesShops.id' => $category->id]);
                });

            $ids = $product_ids->extract('id')->toArray();

            if (!empty($ids)) {
                $query->where([
                    'ProductViews.product_id IN' => $product_ids->extract('id')->toArray()
                ]);
            }

            $entries = $this->Stats->build($query);
            $line->addSerie(array(
                'name' => $category->title,
                'data' => array_map('intval', Hash::extract($entries, '{n}.number'))
            ), 'ref');
        }

        return $line->options;
    }


    private function __lineItems()
    {
        $this->loadModel('Store.LineItems');
        $this->LineItems->belongsTo('Orders', [
            'className' => 'Store.Orders',
            'foreignKey' => 'order_id'
        ]);

        $line = $this->__createChart(__d('admin', 'Productos comprados'), 'areaspline');
        $query = $this->LineItems->find();
        $query->contain(['Orders']);
        $this->Stats
            ->conditions('Orders.order_date', $query)
            ->group('Orders.order_date', $query)
            ->select('Orders.order_date', $query);

        $query->where([
            'Orders.status IS NOT NULL'
        ]);

        $query->select(['number' => $query->func()->sum('LineItems.quantity')]);

        $entries = $this->Stats->build($query);
        $line->addSerie(array(
            'name' => __d('admin', 'Compras'),
            'data' => array_map('intval', Hash::extract($entries, '{n}.number'))
        ), 'ref');

        return $line->options;
    }

    private function __createChart($title, $type)
    {
        $line = $this->Chart->create('Datos', '#datos', array(
            'options' => [
                'chart' => [
                    'type' => $type,
                    'zoomType' => 'x',
                ],
            ],
            'title' => array(
                'text' => $title
            ),
            'axisTitle' => array(
                'y' => $title
            ),
            'categories' => array(
                'x' => $this->Stats->getLegendValues()
            ),
            'plotOptions' => [
                'areaspline' => [
                    'fillOpacity' => 0.5
                ]
            ],
        ));

        return $line;
    }

    private function __topProducts()
    {
        $this->loadModel('Store.LineItems');

        $query = $this->LineItems->find()
            ->group(['product_id'])
            ->order(['number' => 'desc'])
            ->contain([
                'Orders'
            ])
            ->where(['Orders.status IS NOT NULL'])
            ->limit(20);

        $query->select([
            'product_id',
            'number' => $query->func()->count('*')
        ]);

        $data = $query->toArray();

        foreach ($data as $item) {
            $item->product = $this->LineItems->Products->get($item->product_id);
        }

        return $data;
    }

    private function __topProductAttributes()
    {
        $this->loadModel('Store.LineItems');

        $query = $this->LineItems->find()
            ->group(['product_attribute_id'])
            ->contain([
                'Orders'
            ])
            ->where(['Orders.status IS NOT NULL'])
            ->order(['number' => 'desc'])
            ->limit(20);

        $query->select([
            'product_attribute_id',
            'number' => $query->func()->count('*')
        ]);
        $data = $query->toArray();

        foreach ($data as $item) {
            $product = $this->LineItems->ProductAttributes
                ->find()
                ->where([
                    'ProductAttributes.id' => $item->product_attribute_id
                ])
                ->contain([
                    'Products',
                    'Attributes'
                ])
                ->first();

            if ($product) {
                $product->set('title', $product->title);
                $item->product = $product;
            }
        }

        return $data;
    }

    private function __topProductsViews()
    {
        $this->loadModel('Store.ProductViews');

        $query = $this->ProductViews->find()
            ->group(['product_id'])
            ->order(['number' => 'desc'])
            ->limit(20);

        $query->select([
            'product_id',
            'number' => $query->func()->count('*')
        ]);
        $data = $query->toArray();

        foreach ($data as $key => $item) {
            if ($item->product_id) {
                $item->product = $this->ProductViews->Products->get($item->product_id);
            } else {
                unset($data[$key]);
            }
        }

        return $data;
    }

    private function __getStatsOrderQuery()
    {
        $query = $this->Orders->find();

        if ($this->request->getQquery('group_id')) {
            $query->contain(['Users']);
            $query->where([
                'Users.group_id' => $this->request->getQuery('group_id')
            ]);
        }

        return $query;
    }



    private function __setGroups()
    {
        $this->loadModel('User.Groups');
        $groups = $this->Groups->find('list')->toArray();
        $groups[null] = __d('admin', '-- Todos --');
        return $groups;
    }
}
