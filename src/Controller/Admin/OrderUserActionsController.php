<?php
namespace Store\Controller\Admin;

use Manager\Controller\CrudControllerTrait;
use Store\Controller\AppController;

/**
 * OrderUserActions Controller
 *
 * @property \Store\Model\Table\OrderUserActionsTable $OrderUserActions
 */
class OrderUserActionsController extends AppController
{
    use CrudControllerTrait;
}
