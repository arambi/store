<?php
namespace Store\Controller\Admin;

use Manager\Controller\CrudControllerTrait;
use Store\Controller\AppController;

/**
 * Returneds Controller
 *
 * @property \Store\Model\Table\ReturnedsTable $Returneds
 */
class ReturnedsController extends AppController
{
    use CrudControllerTrait;
}
