<?php

namespace Store\Controller\Admin;

use Store\Controller\AppController;
use Manager\Controller\CrudControllerTrait;
use Cake\Network\Request;
use Cake\Network\Response;
use Website\Lib\Website;
use Cake\ORM\TableRegistry;
use Cake\Routing\Router;
use Store\Config\StoreConfig;

/**
 * Users Controller
 *
 * @property \Store\Model\Table\UsersTable $Users
 */
class UsersController extends AppController
{
    use CrudControllerTrait;


    public function __construct(Request $request = null, Response $response = null, $name = null, $eventManager = null)
    {
        parent::__construct($request, $response, $name, $eventManager);
        $this->loadModel('User.Users');
    }

    public function _index($query)
    {
        $query
            ->order([
                'Users.created' => 'desc'
            ])
            ->formatResults(function ($result) {
                return $result->map(function ($row) {
                    $count = TableRegistry::getTableLocator()->get('Store.Orders')->find('completed')
                        ->where([
                            'Orders.user_id' => $row->id,
                        ])->count();

                    $row->set('orders_count', (string)$count);
                    return $row;
                });
            });

        $this->Table->crud->adminUrl('/admin/store/users/');
        $this->Table->crud
            ->addFields([
                'birthday' => 'Fecha de nacimiento',
                'orders_count' => [
                    'type' => 'info',
                    'label' => 'Nº de pedidos'
                ],
            ])
            ->addIndex('index', [
                'fields' => [
                    'orders_count',
                    'name',
                    'email',
                    'group',
                    'status',
                    'birthday',
                    'store_no_taxes' => 'No se le cobra impuestos',
                    'created' => __d('app', 'Alta')
                ],
                'actionLink' => 'view',
                'filters' => [
                    'lastbuy'
                ]
            ])

            ->addFilters([
                'lastbuy' => [
                    'label' => __d('admin', 'Que hayan comprado entre'),
                    'type' => 'date',
                    'filter' => function ($table, $query, $params) {
                        $table->hasOne('Orders', [
                            'className' => 'Store.Orders',
                            'foreignKey' => 'user_id',
                            'conditions' => [
                                'Orders.status IS NOT NULL'
                            ]
                        ]);

                        $query->contain(['Orders']);

                        if (!empty($params['from'])) {
                            $query->where([
                                'Orders.order_date >=' => $params['from']
                            ]);
                        }

                        if (!empty($params['to'])) {
                            $query->where([
                                'Orders.order_date <=' => $params['to']
                            ]);
                        }
                    }
                ]
            ]);

        $this->Table->crud->getView('index')
            ->set('exportCsv', true)
            ->set('csvFields', [
                'id' => 'ID',
                'name' => 'Nombre',
                'email' => 'Email',
                'birthday' => 'Fecha de nacimiento'
            ]);
    }

    public function view($id = false)
    {
        $this->Users->hasMany('Orders', [
            'className' => 'Store.Orders',
            'conditions' => [
                'Orders.status IS NOT NULL'
            ]
        ]);

        $this->Users->hasMany('ProductViews', [
            'className' => 'Store.ProductViews',
            'finder' => 'ranking'
        ]);

        // Tienda con puntos
        if (Website::get('settings.store_has_points')) {
            TableRegistry::getTableLocator()->get('Store.Points')->bindUser();
            $this->Table->crud->setContent([
                'total_points' => TableRegistry::getTableLocator()->get('Store.Points')->totalAvailables($id)
            ]);
        }

        $id = $id ? $id : ($this->request->getData('id') ? $this->request->getData('id') : false);

        $this->__setFields();
        $this->update($id);
        $this->CrudTool->setAction('view');
    }

    protected function _beforeUpdateFind($query, $content)
    {
        $query->contain([
            'Groups',
            'AddressDelivery',
            'AddressInvoice',
            'Orders' => [
                'LineItems'
            ],
            'PaymentMethods',
            'ProductViews' => [
                'Products' => function ($q) {
                    if (method_exists($this->Table->ProductViews->Products->target(), 'containOrder')) {
                        $this->Table->ProductViews->Products->target()->containOrder($q);
                    }

                    if (method_exists($this->Table->ProductViews->Products->target(), 'selectOrder')) {
                        $this->Table->ProductViews->Products->target()->selectOrder($q);
                    }

                    return $q;
                },
            ]
        ]);

        if (Website::get('settings.store_has_points')) {
            $query->contain([
                'PointsUp.Administrators',
                'PointsDown.Administrators',
            ]);
        }
    }

    private function __setFields()
    {
        $fields = [
            'firstname' => !StoreConfig::getConfig('onlyFirstname') ? __d('admin', 'Nombre') : __d('admin', 'Nombre y apellidos'),
        ];

        if (!StoreConfig::getConfig('onlyFirstname')) {
            $fields += [
                'lastname' => __d('admin', 'Apellidos'),
            ];
        }

        $fields += [
            'company' => __d('admin', 'Empresa'),
            'address' => __d('admin', 'Dirección'),
            'city' => __d('admin', 'Población'),
            'phone' => __d('admin', 'Teléfono'),
            'postcode' => __d('admin', 'Código postal'),
            'vat_number' => __d('admin', 'NIF/CIF'),
            'email' => __d('admin', 'Email'),
            'store_no_taxes' => 'No se le cobra impuestos',
        ];

        foreach ($fields as $key => $name) {
            foreach (['address_delivery', 'address_invoice'] as $type) {
                $this->Users->crud->addFields([
                    $type . '.' . $key => [
                        'label' => $name,
                        'type' => 'string',
                        'hasOne' => true
                    ],
                ]);
            }
        }

        $this->Users->crud->addFields([
            'store_comments' => [
                'label' => __d('admin', 'Observaciones'),
                'type' => 'text'
            ],
            'store_no_taxes' => [
                'label' => 'No se le cobra impuestos'
            ],
            'address_delivery.country_id' => [
                'label' => __d('admin', 'País'),
                'type' => 'select',
                'hasOne' => true,
                'options' => function ($crud) {
                    return TableRegistry::getTableLocator()->get('Store.Countries')
                        ->find('list')
                        ->where([
                            'active' => true
                        ]);
                },
                'change' => "\$http.post( '/admin/store/orders/states.json', {country_id: element.val()}).success(function( r){ scope.crudConfig.view.fields['address_delivery.state_id'].options = r.states})"
            ],
            'address_invoice.country_id' => [
                'label' => __d('admin', 'País'),
                'type' => 'select',
                'hasOne' => true,
                'options' => function ($crud) {
                    return TableRegistry::getTableLocator()->get('Store.Countries')
                        ->find('list')
                        ->where([
                            'active' => true
                        ]);
                },
                'change' => "\$http.post( '/admin/store/orders/states.json', {country_id: element.val()}).success(function( r){ scope.crudConfig.view.fields['address_invoice.state_id'].options = r.states})"
            ],
            'address_delivery.state_id' => [
                'label' => __d('admin', 'Provincia'),
                'type' => 'select',
                'hasOne' => true,
                'options' => function ($crud) {
                    return TableRegistry::getTableLocator()->get('Store.States')
                        ->find('list')
                        ->where([
                            'country_id' => @$crud->getContent()['address_delivery']['country_id'],
                            'active' => true
                        ]);
                },
            ],
            'address_invoice.state_id' => [
                'label' => __d('admin', 'Provincia'),
                'type' => 'select',
                'hasOne' => true,
                'options' => function ($crud) {
                    return TableRegistry::getTableLocator()->get('Store.States')
                        ->find('list')
                        ->where([
                            'country_id' => @$crud->getContent()['address_invoice']['country_id'],
                            'active' => true
                        ]);
                },
            ],
        ]);

        $this->Users->crud
            ->addFields([
                'email' => 'Email',
                'payment_methods' => [
                    'type' => 'checkboxes',
                    'label' => __d('admin', 'Métodos de pago'),
                    'options' => function ($crud) {
                        return $this->Users->PaymentMethods->find()
                            ->where([
                                'PaymentMethods.active' => true
                            ])
                            ->hydrate(true)
                            ->toArray();
                    }
                ]
            ])
            ->addView('view', [
                'template' => 'Store/user',
                'urlSubmit' => Router::url([
                    'plugin' => 'Store',
                    'controller' => 'Users',
                    'action' => 'view'
                ]),
                'actionButtons' => ['index' => [
                    'label' => 'Listado',
                    'url' => '#admin/store/users'
                ]],
                'columns' => [
                    [
                        'cols' => 8,
                        'box' => [
                            [
                                'elements' => [
                                    'address_invoice.firstname'
                                ]
                            ],
                        ],
                    ]
                ],
            ])
            ->addAssociations(['PaymentMethods', 'AddressDelivery', 'AddressInvoice']);
    }
}
