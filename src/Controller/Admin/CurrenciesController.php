<?php
namespace Store\Controller\Admin;

use Store\Controller\AppController;
use Manager\Controller\CrudControllerTrait;

/**
 * Currencies Controller
 *
 * @property \Store\Model\Table\CurrenciesTable $Currencies
 */
class CurrenciesController extends AppController
{
  use CrudControllerTrait;
}
