<?php
namespace Store\Controller\Admin;

use Store\Controller\AppController;
use Manager\Controller\CrudControllerTrait;

/**
 * Zones Controller
 *
 * @property \Store\Model\Table\ZonesTable $Zones
 */
class ZonesController extends AppController
{
  use CrudControllerTrait;
}
