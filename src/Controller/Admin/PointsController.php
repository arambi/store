<?php
namespace Store\Controller\Admin;

use Manager\Controller\CrudControllerTrait;
use Store\Controller\AppController;
use Cake\Mailer\MailerAwareTrait;

/**
 * Points Controller
 *
 * @property \Store\Model\Table\PointsTable $Points
 */
class PointsController extends AppController
{
  use CrudControllerTrait;
  use MailerAwareTrait;

  public function addup()
  {
    $this->CrudTool->serializeAction( false);

    $user = $this->Points->Users->find()
      ->where(['Users.salt' => $this->request->data( 'salt')])
      ->first();

    if( $user)
    {
      $add = $this->Points->addPoints([
        'points' => $this->request->data( 'points'),
        'user_id' => $user->id,
        'administrator_id' => $this->Auth->user( 'id')
      ]);
    }
    else
    {
      $add = false;
    }

    if( $add)
    {
      $this->Points->bindUser();
      $user = $this->Points->Users->find( 'content')
        ->where( [ 'Users.id' => $user->id])
        ->first();

      // Envio de email
      $this->getMailer( 'Store.Shop')->send( 'user_add_points', [
          $user,
          $this->request->data( 'points'),
          $this->Points->totalAvailables( $user->id)
      ]);

      $this->set([
        'result' => $user->points_up,
        '_serialize' => ['result'],
      ]);
    }
    else
    {
      $this->set([
        'result' => false,
        '_serialize' => ['result'],
      ]);
    }
  }

  public function add()
  {
    
  }
}
