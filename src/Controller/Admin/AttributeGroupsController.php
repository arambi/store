<?php
namespace Store\Controller\Admin;

use Store\Controller\AppController;
use Manager\Controller\CrudControllerTrait;

/**
 * AttributeGroups Controller
 *
 * @property \Store\Model\Table\AttributeGroupsTable $AttributeGroups
 */
class AttributeGroupsController extends AppController
{
  use CrudControllerTrait;
}
