<?php
namespace Store\Controller\Admin;

use Manager\Controller\CrudControllerTrait;
use Store\Controller\AppController;
use Cake\Core\Configure;

/**
 * ProductsColors Controller
 *
 * @property \Store\Model\Table\ProductsColorsTable $ProductsColors
 */
class ProductsColorsController extends AppController
{
    use CrudControllerTrait;

  public function autocomplete()
  {
    $this->loadModel( 'Store.Products');
    $this->Products->hasOne( 'ProductsColors', [
      'className' => 'Store.ProductsColors',
      'foreignKey' => 'product_id'
    ]);

    if( $this->request->query( 'model') == 'products_colors')
    {
      $query = $this->ProductsColors->find( 'colors')
        ->where([
          'Products.published' => true
        ]);


      if( Configure::read( 'I18n.behavior') == 'I18n.I18nTranslate')
      {
        $query->where([
          'ProductsTranslation.title LIKE' => '%'.$this->request->query ['q'].'%',
        ]);
      }
      else
      {
        $query->where([
          'Products_title_translation.content LIKE' => '%'.$this->request->query ['q'].'%',
        ]);
      }
      
      $contents = $query->toArray();
    }
    else
    {
      $query = $this->Products->find()
        ->contain([
          'ProductsColors'
        ])
        ->select([
          'id',
          'photos',
          'title'
        ])
        ->where([
          'ProductsColors.id IS NULL',
          'Products.published' => true
        ]);

      if( Configure::read( 'I18n.behavior') == 'I18n.I18nTranslate')
      {
        $query->where([
          'ProductsTranslation.title LIKE' => '%'.$this->request->query ['q'].'%',
        ]);
      }
      else
      {
        $query->where([
          'Products_title_translation.content LIKE' => '%'.$this->request->query ['q'].'%',
        ]);
      }

      $contents = $query->toArray();

      foreach( $contents as $content)
      {
        foreach( (array)$content->photos as $photo)
        {
          if( $photo->main)
          {
            $content->set( 'photo_main', $photo->toArray());
            $content->dirty( 'photo_main');
            break;
          }
        }

        if( !$content->photo_main)
        {
          $content->set( 'photo_main', [
            'paths' => [
              'square'
            ]
          ]);
        }

        $content->unsetProperty( 'photos');
      }
    }

    $this->CrudTool->addSerialized([
      'results' => $contents
    ]);
  }
}
