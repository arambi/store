<?php
namespace Store\Controller\Admin;

use Manager\Controller\CrudControllerTrait;
use Store\Controller\AppController;

/**
 * Invoices Controller
 *
 * @property \Store\Model\Table\InvoicesTable $Invoices
 */
class InvoicesController extends AppController
{
    use CrudControllerTrait;

    public function negative($order_id, $type)
    {
        $this->getTableLocator()->get('Store.InvoiceRefunds')
            ->createFromOrder($order_id, $type);

        $this->CrudTool->addSerialized([
            'redirect' => '/admin/store/invoice_refunds'
        ]);
    }

    
}
