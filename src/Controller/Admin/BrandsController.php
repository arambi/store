<?php
namespace Store\Controller\Admin;

use Manager\Controller\CrudControllerTrait;
use Store\Controller\AppController;

/**
 * Brands Controller
 *
 * @property \Store\Model\Table\BrandsTable $Brands
 */
class BrandsController extends AppController
{
    use CrudControllerTrait;
}
