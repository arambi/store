<?php
namespace Store\Controller\Admin;

use Manager\Controller\CrudControllerTrait;
use Store\Controller\AppController;

/**
 * OrderUserSessions Controller
 *
 * @property \Store\Model\Table\OrderUserSessionsTable $OrderUserSessions
 */
class OrderUserSessionsController extends AppController
{
    use CrudControllerTrait;
}
