<?php
namespace Store\Controller\Admin;

use Manager\Controller\CrudControllerTrait;
use Store\Controller\AppController;

/**
 * CarrierFestives Controller
 *
 * @property \Store\Model\Table\CarrierFestivesTable $CarrierFestives
 */
class CarrierFestivesController extends AppController
{
    use CrudControllerTrait;
}
