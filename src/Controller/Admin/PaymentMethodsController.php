<?php
namespace Store\Controller\Admin;

use Store\Controller\AppController;
use Manager\Controller\CrudControllerTrait;

/**
 * PaymentMethods Controller
 *
 * @property \Store\Model\Table\PaymentMethodsTable $PaymentMethods
 */
class PaymentMethodsController extends AppController
{
  use CrudControllerTrait;
}
