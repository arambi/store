<?php

namespace Store\Controller\Admin;

use Cake\ORM\TableRegistry;
use Cake\Utility\Inflector;
use Store\Config\StoreConfig;
use Letter\Mailer\MailerAwareTrait;
use Manager\Controller\CrudControllerTrait;
use Store\Controller\AppController;

/**
 * Refunds Controller
 *
 * @property \Store\Model\Table\LineItemsTable $LineItems
 * @property \Store\Model\Table\RefundsTable $Refunds
 *
 */
class RefundsController extends AppController
{
    use CrudControllerTrait;
    use CrudControllerTrait {
        field as crudField;
    }

    use MailerAwareTrait;

    private function changeQuery($query)
    {
        $query
            ->where([
                'Refunds.status IS NOT NULL'
            ])
            ->formatResults(function ($result) {
                return $result->map(function ($item) {
                    $item->set('status_human', $item->status_human);
                    $item->set('price_human', $item->price_human);
                    $item->set('order_link', '<a target="_blank" href="/admin#/admin/store/orders/view/' . $item->order_id . '">' . $item->order_id . '</a>');
                    $item->set('date', $item->modified->format('d/m/Y'));

                    if (!empty($item->product)) {
                        $item->set('title_link', '<a target="_blank" href="' . $item->product->link() . '">' . $item->title . '</a>');
                    }

                    return $item;
                });
            });

        $ProductsTable = StoreConfig::getProductsModel();

        if ($ProductsTable->hasBehavior('Deletable')) {
            $query->contain([
                'Products' => function ($q) {
                    return $q->find('regardless');
                }
            ]);
        } else {
            $query->contain('Products');
        }

        if (StoreConfig::getConfig('withAttributes')) {
            $query->contain('ProductAttributes.Attributes');
        }
    }

    public function _index($query)
    {
        // $this->changeQuery($query);
    }

    public function _beforeUpdateFind($query)
    {
        $query->formatResults(function($result) {
            return $result->map(function($row) {
                $order = $this->Refunds->Orders->findById($row['order_id'])->first();
                $row['delivery_address'] = $order->pretty_delivery_address;
                $row['invoice_address'] = $order->pretty_invoice_address;
                return $row;
            });
        });
    }

    public function _afterSave($content)
    {
        $map = [
            StoreConfig::REFUND_STATUS_REVIEW => StoreConfig::REFUND_STATUS_PROCCESS,
            StoreConfig::REFUND_STATUS_RECEIVED => StoreConfig::REFUND_STATUS_FINALIZED,
        ];

        foreach ($map as $current => $next) {
            if ($content->status == $current) {
                $this->Refunds->addStatus($content->id, $next);
            }
        }

        if ($this->request->getData('generate_invoice')) {
            $this->Refunds->createInvoiceRefund($content, (bool)$this->request->getData('shipping_coast'));
        }
    }

    public function field($field, $id, $locale = false)
    {
        $entity = $this->crudField($field, $id, $locale);

        if ($field == 'status') {
            $this->sendOrderEmail($entity);
        }
    }

    private function sendOrderEmail($entity)
    {
        $item = TableRegistry::getTableLocator()->get('Store.LineItems')->find()
            ->where([
                'LineItems.id' => $entity->id
            ])
            ->first();

        $order = TableRegistry::getTableLocator()->get('Store.Orders')->find()
            ->where([
                'Orders.id' => $item->order_id
            ])
            ->contain([
                'Users'
            ])
            ->first();

        $user = TableRegistry::getTableLocator()->get('User.Users')->find()
            ->where(['Users.id' => $order->user_id])
            ->first();

        $locale = $user ? $user->locale : ($order->locale ? $order->locale : false);

        // Envio de email
        $key = 'item' . Inflector::camelize($entity->status);

        $this->getMailer('Store.Orders')->send($key, [
            $entity,
            $order,
        ]);
    }
}
