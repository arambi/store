<?php
namespace Store\Controller\Admin;

use Store\Controller\AppController;
use Manager\Controller\CrudControllerTrait;

/**
 * Attributes Controller
 *
 * @property \Store\Model\Table\AttributesTable $Attributes
 */
class AttributesController extends AppController
{
  use CrudControllerTrait;
}
