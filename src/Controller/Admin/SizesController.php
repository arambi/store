<?php
namespace Store\Controller\Admin;

use Manager\Controller\CrudControllerTrait;
use Store\Controller\AppController;

/**
 * Sizes Controller
 *
 * @property \Store\Model\Table\SizesTable $Sizes
 */
class SizesController extends AppController
{
    use CrudControllerTrait;
}
