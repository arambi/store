<?php
namespace Store\Controller\Admin;

use Manager\Controller\CrudControllerTrait;
use Store\Controller\AppController;

/**
 * Favs Controller
 *
 * @property \Store\Model\Table\FavsTable $Favs
 */
class FavsController extends AppController
{
    use CrudControllerTrait;

  protected function _index( $query)
  {

    $query
      ->where([
        'Products.id IS NOT NULL'
      ])
      ->formatResults( function( $result){
        return $result->map( function( $row){
          return $row;
        });
      })
      ->select([
        'total' => 'COUNT(*)',
        'Favs.id',
        'Products.title',
        'Products.id',
      ])
      ->order([
        'total' => 'desc'
      ])
      ->group([
        'Products.id'
      ]);
  }
}
