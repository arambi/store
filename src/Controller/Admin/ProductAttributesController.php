<?php
namespace Store\Controller\Admin;

use Store\Controller\AppController;
use Manager\Controller\CrudControllerTrait;

/**
 * ProductAttributes Controller
 *
 * @property \Store\Model\Table\ProductAttributesTable $ProductAttributes
 */
class ProductAttributesController extends AppController
{
  use CrudControllerTrait;

  public function autocomplete()
  {
    $this->loadModel( 'Store.Products');

    $query = $this->Products->find();
    $query->contain([
        'ProductAttributes' => [
          'Products',
          'Attributes' => [
            'AttributeGroups'
          ]
        ],
    ]);
    
    $query->where([
      'Products.title LIKE' => '%'.$this->request->query ['q'].'%'
    ]);
    /*
    $query->innerJoinWith( 'Attributes', function( $q){
        return $q->where([
          'Attributes.title LIKE' => '%'.$this->request->query ['q'].'%'
        ]);
    });
    */
   
    $contents = $query->all();

    $results = [];

    foreach( $contents as $product)
    {
      foreach( $product->product_attributes as $product_attribute)
      {
        $results [] = $product_attribute;
      }
    }

    $this->CrudTool->addSerialized([
      'results' => $results
    ]);
  }

}
