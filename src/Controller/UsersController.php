<?php

namespace Store\Controller;

use ArrayObject;
use I18n\Lib\Lang;
use Cake\Event\Event;
use Section\Crumbs\Crumbs;
use Cake\ORM\TableRegistry;
use Section\Routing\RouteData;
use Store\Controller\AppController;
use Cake\Http\Exception\NotFoundException;
use Cake\Http\Response;
use Cake\Http\ServerRequest;

/**
 * Users Controller
 *
 * @property \Store\Model\Table\UsersTable $Users
 */
class UsersController extends AppController
{

    public function __construct(ServerRequest $request = null, Response $response = null, $name = null, $eventManager = null)
    {
        parent::__construct($request, $response, $name, $eventManager);
        $this->loadModel('User.Users');
    }

    public function initialize()
    {
        parent::initialize();
        $this->loadComponent('Store.Cart');
        $this->Auth->allow([
            'order'
        ]);
    }

    public function index()
    {
    }

    public function edit()
    {
        if (empty($this->Auth->user('id'))) {
            throw new NotFoundException(__('Página no encontrada'));
        }

        $user = $this->Users->find()
            ->where(['Users.id' => $this->Auth->user('id')])
            ->first();

        if ($this->request->is(['patch', 'post', 'put'])) {
            $this->Users->patchEntity($user, $this->request->data, ['validate' => 'edit']);

            if (empty($user->errors) && empty($this->request->data['Users']['current_password'])) {
                $user->unsetProperty('password')->unsetProperty('password2');
            }

            if ($this->Users->save($user)) {
                $event = new Event('User.Controller.Users.afterUpdate', $this, [$user]);
                $this->getEventManager()->dispatch($event);

                $user = $this->Users->find('auth')
                    ->where(['Users.id' => $user->id])
                    ->contain('Groups')
                    ->first()->toArray();

                $user = new ArrayObject($user);
                $event = new Event('Store.Controller.Users.afterUpdate', $this, [$user]);
                $this->getEventManager()->dispatch($event);
                $user = (array)$user;
                $this->Auth->setUser($user);
                $this->Flash->success(__d('app', 'La información se ha guardado correctamente'));


                $this->redirect($this->Section->url([
                    'plugin' => 'Store',
                    'controller' => 'Users',
                    'action' => 'edit'
                ]));
            } else {
                $this->Flash->error(__d('app', '¡OOOPS! Debe de haber un error en los datos introducidos. Revísalos por favor.'), 'alert/error');
            }
        }


        $this->set('currencies', TableRegistry::getTableLocator()->get('Store.Currencies')->find('list'));
        $this->set('languages', Lang::combine('iso3', 'name'));
        $this->set(compact('user'));
    }

    public function addresses($type = null)
    {
        if ($type) {
            return $this->_address($type);
        }

        $this->loadModel('Store.Addresses');
        $user = $this->Users->find()
            ->where([
                'Users.id' => $this->Auth->user('id'),
            ])
            ->contain([
                'AddressDelivery',
                'AddressInvoice',
            ])
            ->first();

        if ($this->request->is(['patch', 'post', 'put'])) {
            $this->Users->patchEntity($user, $this->request->data);
            $user->address_delivery->set('user_id', $this->Auth->user('id'));
            $user->address_invoice->set('user_id', $this->Auth->user('id'));
            $user->address_delivery->set('type', 'delivery');
            $user->address_invoice->set('type', 'invoice');

            if ($this->Addresses->save($user->address_delivery) && $this->Addresses->save($user->address_invoice)) {
                $event = new Event('User.Controller.Users.afterUpdate', $this, [$user]);
                $this->getEventManager()->dispatch($event);
                $this->Flash->success(__d('app', 'Los datos han sido guardados'));

                if ($this->Cart->order()) {
                    $this->Cart->order()->setAddresses($user);
                    $this->loadModel('Store.Orders')->save($this->Cart->order());
                }

                $this->redirect($this->Section->url([
                    'plugin' => 'Store',
                    'controller' => 'Users',
                    'action' => 'addresses'
                ]) . ($type ? '/' . $type : ''));
            }
        }

        if ($user->address_delivery) {
            $delivery = $user->address_delivery;
        } else {
            $address = $this->Addresses->newEntity([
                'user_id' => $this->Auth->user('id'),
                'type' => 'invoice'
            ]);

            $user->set('address_delivery', $address);
        }

        if ($user->address_invoice) {
            $invoice = $user->address_invoice;
        } else {
            $address = $this->Addresses->newEntity([
                'user_id' => $this->Auth->user('id'),
                'type' => 'invoice'
            ]);

            $user->set('address_invoice', $address);
        }

        $this->set(compact('user'));
    }


    private function _address($type = null)
    {
        $referer = $this->request->getSession()->read('UserAddressReferer');

        if (!$referer || strpos($referer, $this->request->here) !== false) {
            $referer = $this->referer();
            $this->request->getSession()->write('UserAddressReferer', $referer);
        }


        $this->loadModel('Store.Addresses');
        $user = $this->Users->find()
            ->where([
                'Users.id' => $this->Auth->user('id'),
            ])
            ->contain([
                'Address' . ucfirst($type),
            ])
            ->first();

        $address_key = 'address_' . $type;

        if ($this->request->is(['patch', 'post', 'put'])) {
            $this->Users->patchEntity($user, $this->request->data);
            $user->$address_key->set('user_id', $this->Auth->user('id'));
            $user->$address_key->set('type', $type);

            if ($this->Addresses->save($user->$address_key)) {
                $event = new Event('User.Controller.Users.afterUpdate', $this, [$user]);
                $this->getEventManager()->dispatch($event);

                $this->Flash->success(__d('app', 'Los datos han sido guardados'));

                if ($this->Cart->order()) {
                    $this->Cart->order()->setAddresses($user);
                    $this->loadModel('Store.Orders')->save($this->Cart->order());
                }

                $referer = $this->request->getSession()->read('UserAddressReferer');
                $this->request->getSession()->delete('UserAddressReferer');
                $this->redirect($referer);
            }
        }

        if ($user->address_delivery) {
            $delivery = $user->address_delivery;
        } else {
            $address = $this->Addresses->newEntity([
                'user_id' => $this->Auth->user('id'),
                'type' => 'invoice'
            ]);

            $user->set('address_delivery', $address);
        }

        if ($user->address_invoice) {
            $invoice = $user->address_invoice;
        } else {
            $address = $this->Addresses->newEntity([
                'user_id' => $this->Auth->user('id'),
                'type' => 'invoice'
            ]);

            $user->set('address_invoice', $address);
        }

        $this->set(compact('user'));

        return $this->render('address');
    }

    public function favs()
    {
    }

    public function returns()
    {
    }

    public function returns_form()
    {
    }

    public function orders()
    {
        $this->loadModel('Store.Orders');

        $query = $this->Orders->find('order')
            ->where([
                'Orders.user_id' => $this->Auth->user('id'),
                'Orders.status IS NOT NULL',
            ])
            ->group([
                'Orders.id'
            ])
            ->order(['Orders.order_date' => 'desc']);

        $orders = $this->paginate($query);

        $this->set(compact('orders'));
    }

    public function order()
    {
        $this->loadModel('Store.Orders');
        $content = $this->Orders->find('order')
            ->where([
                'Orders.salt' => $this->request->getParam('salt'),
                'Orders.status IS NOT NULL',
            ])
            ->contain([
                'OrderHistories'
            ])
            ->first();

        if (!$content) {
            $this->Section->notFound('No encontrado el order');
        }

        $this->set(compact('content'));
    }
}
