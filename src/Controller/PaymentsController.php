<?php
namespace Store\Controller;

use Store\Controller\AppController;

/**
 * Payments Controller
 *
 * @property \Store\Model\Table\PaymentsTable $Payments
 */
class PaymentsController extends AppController
{
  public function index()
  {
    $payments = $this->loadModel( 'Store.PaymentMethods')->getValids( $this->Auth->user());
    $this->set( compact( 'payments'));

    if( $this->request->is( 'post'))
    {
      $success = true;
      $data = $this->request->getData();
      $this->loadModel( 'User.Users')->query()->update()
        ->where([
          'id' => $this->Auth->user( 'id')
        ])
        ->set([
          'payment_id' => $data ['payment_id']
        ])
        ->execute();
      
      if( $success)
      {
        $this->Flash->success( __d( 'app', 'La información se ha guardado correctamente'));
      }
    }

    $user = $this->loadModel( 'User.Users')->find()
      ->where([
        'Users.id' => $this->Auth->user( 'id')
      ])
      ->first();
        
    $this->set( compact( 'user'));
  }

}
