<?php
namespace Store\Controller;

use Store\Controller\AppController;
use Store\Config\StoreConfig;

/**
 * Favs Controller
 *
 * @property \Store\Model\Table\FavsTable $Favs
 */
class FavsController extends AppController
{
  public function initialize()
  {
    parent::initialize();
    $this->loadComponent( 'RequestHandler');
  }

  public function add()
  {
    if( $this->request->is( 'post'))
    {
      $this->loadModel( 'Store.Favs');
      $result = $this->Favs->add( $this->request->data( 'product'), $this->Auth->user( 'id'));
      
      if( !array_key_exists( 'error', $result))
      {
        $this->set([
          'success' => __d( 'app', '¡Añadido!')
        ]);
      }
      else
      {
        $this->set([
          'error' => $result ['error'] . '. <a href="'. $this->Section->url([
            'plugin' => 'Store',
            'controller' => 'Favs',
            'action' => 'index'
          ])  .'">'. __d( 'app', 'Ir a mi lista de deseos') .'</a>'
        ]);
      }
  
      $this->set( '_serialize', true);
    }
  }

  public function index()
  {
    $table = StoreConfig::getProductsModel();

    $table->hasOne( 'Favs', [
      'className' => 'Store.Favs',
      'foreignKey' => 'product_id',
    ]);

    $contents = $table->find( 'front')
      ->where([
        'Favs.user_id' => $this->Auth->user( 'id')
      ])
      ->contain([
        'Favs',
      ])
      ->all();

    $this->set( compact( 'contents'));
  }

  public function delete()
  {
    if( $this->request->is( 'post'))
    {
      $this->loadModel( 'Store.Favs');
      $result = $this->Favs->remove( $this->request->data( 'product'), $this->Auth->user( 'id'));
      $count = $this->Favs->find()
      ->where([
        'Favs.user_id' => $this->Auth->user( 'id')
      ])
      ->count();

      $this->set([
        'result' => $result,
        'count' => $count,
        '_serialize' => ['result', 'count']
      ]);
    }
  }

}
