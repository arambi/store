<?php
namespace Store\Controller;

use Store\Controller\AppController;

/**
 * Returneds Controller
 *
 * @property \Store\Model\Table\ReturnedsTable $Returneds
 */
class ReturnedsController extends AppController
{
  public function add()
  {
    if( $this->request->query( 'success'))
    {
      return $this->render( 'success');
    }
 
    $returned = $this->Returneds->newEntity();

    $returned->set( 'user_id', $this->Auth->user( 'id'));

    if( $this->request->is( ['patch', 'post', 'put']))
    {
      $this->Returneds->patchEntity( $returned, $this->request->data);

      if( $this->Returneds->save( $returned))
      {
        $this->redirect( $this->Section->url([
          '?' => [
            'success' => true
          ]
        ]));
      }
    }

    $this->set( compact( 'returned'));
  }

  
}
