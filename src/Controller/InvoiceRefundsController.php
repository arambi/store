<?php

namespace Store\Controller;

use Store\Controller\AppController;

/**
 * InvoiceRefunds Controller
 *
 * @property \Store\Model\Table\InvoiceRefundsTable $InvoiceRefunds
 */
class InvoiceRefundsController extends AppController
{

	public function initialize()
	{
		parent::initialize();
		$this->loadComponent('RequestHandler');
		$this->Auth->allow();
	}

	public function view()
	{
		$invoice = $this->InvoiceRefunds
			->find()
			->contain([
				'Orders',
				'InvoiceRefundLineItems.LineItems.Products',
			])
			->where([
				'InvoiceRefunds.salt' => $this->request->getQuery('salt')
			])
			->first();

		if (!$invoice) {
			$this->Section->notFound();
		}

		$this->set(compact('invoice'));
	}
}
