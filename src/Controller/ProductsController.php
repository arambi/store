<?php
namespace Store\Controller;

use Store\Controller\AppController;
use Cake\Event\Event;
use Cake\ORM\TableRegistry;
use Section\Crumbs\Crumbs;
use Cake\Collection\Collection;
use Cake\Routing\Router;
use Cake\Cache\Cache;

/**
 * Products Controller
 *
 * @property \Store\Model\Table\ProductsTable $Products
 */
class ProductsController extends AppController
{
  public function initialize() 
  {
    parent::initialize();
    $this->loadComponent( 'RequestHandler');
    $this->loadComponent( 'Store.ShopSearch');
    $this->loadComponent( 'Manager.Cache');
    $this->Auth->allow();
    $this->paginate = [
      'limit' => 18,
    ];
  }

  public function beforeFilter(Event $event)
  {
    parent::beforeFilter( $event);

    if( $this->request->is( 'ajax'))
    {
      $this->viewBuilder()->layout( 'ajax');
    }
  }
  /**
   * Index method
   *
   * @return void
   */
  public function index()
  {
    $this->set( 'products', $this->paginate($this->Products));
    $this->set( '_serialize', ['products']);
  }

  public function categories()
  {
    
  }
  
  /**
   * View method
   *
   * @param string|null $id Product id.
   * @return void
   * @throws \Cake\Network\Exception\NotFoundException When record not found.
   */
  public function view()
  {
    if( !$this->Auth->user())
    {
      $cacheKey = 'Products.view.'. $this->request->params ['slug'];
      $product = Cache::read( $cacheKey);

      if( !$product)
      {
        $product = $product = $this->findProduct();
        Cache::write( $cacheKey, $product);
      }
    }
    else
    {
      $product = $this->findProduct();
    }


    $this->set( compact( 'product', 'last_views'));

    // Migas de pan
    if( isset( $product->categories_shops [0]))
    {
      $category = $product->categories_shops [0];
      Crumbs::add( $category->title, [
        'plugin' => 'Store',
        'controller' => 'CategoriesShops',
        'action' => 'category',
        'slug' => $category->slug
      ]);
    }
  }

  private function findProduct()
  {
    $query =  $this->Products->find( 'slug', ['slug' => $this->request->params ['slug']])
      ->contain([
        'ProductAttributes' => function( $q){
          $q->where( ['ProductAttributes.published' => true])->contain(['Attributes']);
          return $q;
        },
        'Tags' => function( $query){
          return $query->formatResults (function( $results){
            return $results->map(function( $row){
              $row->view_url = \Cake\Routing\Router::url([
                  'plugin' => 'Store',
                  'controller' => 'Products',
                  'action' => 'tag',
                  'slug' => $row->slug
              ]);
              return $row;
            });
          });

        },
        'Taxes',
        'Brands',
        'CategoriesShops' => [
          'Sizes'
        ],
        'ProductsColors'
      ]);
    
    $this->Products->findRelateds( $query);

    // De Cofree.PublisherBehavior
    $this->Products->publishedQuery( $query)->fromToQuery( $query);
    $this->Products->setAttributesPrice( $query);
    $this->Products->setInfo( $query);
    $product = $query->first();

    if( !$product)
    {
      throw new \Cake\Network\Exception\NotFoundException( __( 'Página no encontrada'));
    }

    
    $this->loadModel( 'Store.ProductViews');
    $this->ProductViews->add( $this->request->session()->id(), $this->Auth->user( 'id'), $product->id);

    $exceptions = (new Collection( $product->relateds))->extract( 'id')->toArray() + [$product->id];
    $products_by_tags = $this->Products->byTags( $product->tags, $exceptions);

    if( is_object( $product->relateds))
    {
      // $product->relateds = array_merge( $product->relateds->toArray(), $products_by_tags);
      $product->relateds = array_merge( $product->relateds->toArray());
    }

    return $product;
  }

  private function findLastsViews()
  {
    $last_views = $this->Products->lastViewProducts( $this->Auth->user( 'id'), $this->request->session()->id())->toArray();
    $this->Products->injectInfo( $last_views);
    return $last_views;
  }

  public function search()
  {
    $query = $this->Products->findProducts();
    $this->ShopSearch->search( $query);

    $products = $this->paginate( $query);

    $this->set( compact( 'products'));
    $this->set( ['_serialize' => ['products']]);
  }

  public function tag()
  {
    $tag = $this->Products->Tags->find()
      ->where(['Tags.slug' => $this->request->params ['slug']])
      ->first();

    $query = $this->Products->findProducts()
      ->innerJoinWith( 'Tags', function( $q)  use ($tag){
          return $q->where(['Tags.id' => $tag->id]);
      });

    $products = $this->paginate( $query);

    $this->set( compact( 'tag', 'products'));
  }

  public function color()
  {
    $color = $this->Products->ProductAttributes->Attributes->find()
      ->where([
        'Attributes.id' => $this->request->params ['slug'],
        'AttributeGroups.group_type' => 'color'
      ])
      ->contain(['AttributeGroups'])
      ->first();

    if( !$color)
    {
      throw new \Cake\Network\Exception\NotFoundException( __( 'Página no encontrada'));
    }

    $product_attributes = $this->Products->ProductAttributes->find( 'list')
      ->innerJoinWith( 'Attributes', function( $q)  use ($color){
          return $q->where(['Attributes.id' => $color->id]);
      })
      ->select(['ProductAttributes.id'])->toArray();

    $query = $this->Products->find()
      ->innerJoinWith( 'ProductAttributes', function( $q) use ($color, $product_attributes){
          return $q->where(['ProductAttributes.id' => array_values( $product_attributes)]);
      });


    $query->contain([
        'CategoriesShops',
    ]);

    $products = $this->paginate( $query);

    $this->set( compact( 'color', 'products'));

  }

  public function novelties()
  {
    $query = $this->Products->findProducts()
      ->where([
        'Products.store_novelty' => true
      ]);

    $products = $this->paginate( $query);

    $this->set( compact( 'products'));
    $this->set( ['_serialize' => ['products']]);
  }

  public function gmc()
  {
    $this->viewBuilder()->layout( 'ajax');
    
    $limit = 50;
    $offset = 0;

    $out = Cache::read( 'Products.gmc');

    if( !$out)
    {
      $out = [];

      while( true)
      {
        $query = $this->Products->find()
          ->limit( $limit)
          ->offset( $offset)
          ->order([
            'Products.id'
          ])
          ->contain([
            'ProductAttributes' => [
              'Attributes',
              'Products'
            ],
            'Brands',
            'CategoriesShops',
            'Taxes'
        ]);

        $this->Products->publishedQuery( $query)->fromToQuery( $query);

        $products = $query->all();

        if( $products->count() == 0)
        {
          break;
        }

        foreach( $products as $product)
        {
          if( empty( $product->product_attributes))
          {
            if( $product->quantity < 1)
            {
              continue;
            }

            $price = round( $product->realPrice( $product->price, $product->tax), 2);

            if( $product->photo_main)
            {
              $photo = Router::url( $product->photo_main->paths->org, true);
            }
            else
            {
              $photo = false;
            }

            $row = [
              $product->id,
              $product->title,
              str_replace( "\t", '', str_replace( "\n", ". ", strip_tags( $product->summary))),
              $price,
              Router::url([
                  'plugin' => 'Store',
                  'controller' => 'Products',
                  'action' => 'view',
                  'slug' => $product->slug
              ], true),
              $pa->quantity > 0 ? 'In stock' : 'Pre-order',
              $photo,
              '167',
              isset( $product->categories_shops[0]) ? $product->categories_shops[0]->title : '',
              isset( $product->brand->title) ? $product->brand->title : '',
              !empty( $product->store_ean13) ? $product->store_ean13 : '',
              'new'
            ];

            if( $price > 0)
            {
              $out [] = implode( "\t", $row);
            }
          }

          foreach( $product->product_attributes as $pa)
          {
            if( !$pa->has( 'product') || $pa->quantity < 1)
            {
              continue;
            }

            foreach( $pa->attributes as $attribute)
            {
              if( !empty( $attribute->color))
              {
                $pa->product->set( 'color', new \Store\Model\Entity\ProductsColor([
                  'attribute_id' => $attribute->id
                ]));
              }
            }

            $photo = $pa->product->color_photo;

            if( $photo && isset( $photo->paths->org))
            {
              $photo = Router::url( $photo->paths->org, true);
            }
            elseif( isset( $pa->product->photos [0]) && isset( $pa->product->photos [0]->paths->org))
            {
              $photo = Router::url( $pa->product->photos [0]->paths->org, true);
            }
            else
            {
              $photo = false;
            }
              
            $title = (new Collection( $pa->attributes))->extract( 'title')->toArray();

            if( !empty( $pa->store_price) && $photo)
            {
              foreach( $pa->attributes as $attr)
              {
                if( $attr->attribute_group->is_color_group)
                {
                  $attribute_id = $attr->id;
                }
              }

              $price = round( $pa->realPrice( $pa->price, $product->tax), 2);
              
              $row = [
                $pa->product->id .'-'.$pa->id,
                $pa->product->title . ' - ' . implode( ' - ', $title),
                str_replace( "\t", '', str_replace( "\n", ". ", strip_tags( $pa->product->summary))),
                str_replace( ',', '.', $price),
                Router::url([
                    'plugin' => 'Store',
                    'controller' => 'Products',
                    'action' => 'view',
                    'slug' => $pa->product->slug
                ], true) . '?attribute_id=' . $attribute_id,
                $pa->quantity > 0 ? 'In stock' : 'Pre-order',
                $photo,
                '167',
                isset( $product->categories_shops[0]) ? $product->categories_shops[0]->title : '',
                isset( $product->brand->title) ? $product->brand->title : '',
                !empty( $pa->store_ean13) ? $pa->store_ean13 : '',
                'new'
              ];

              if( $price > 0)
              {
                $out [] = implode( "\t", $row);
              }

            }
              
          }
        }

        $offset += $limit;
      }

      Cache::write( 'Products.gmc', $out);
    }
      

    $headers = [
      'id',
      'title',
      'description',
      'price',
      'link',
      'availability',
      'image_link',
      'google_product_category',
      'product type',
      'brand',
      'gtin',
      'condition',
    ];

    $this->set( compact( 'out', 'headers'));
  }
}
