<?php
namespace Store\Controller;

use Store\Controller\AppController;

/**
 * CarrierFestives Controller
 *
 * @property \Store\Model\Table\CarrierFestivesTable $CarrierFestives
 */
class CarrierFestivesController extends AppController
{
  
  public function initialize() 
  {
    parent::initialize();
    $this->loadComponent( 'RequestHandler');
    $this->Auth->allow();
  }

  public function index()
  {
    $query = $this->CarrierFestives
      ->find( 'front')
    ;

    $contents = $this->paginate( $query);
    $this->set( compact( 'contents'));
  }

  public function view()
  {
    $content = $this->CarrierFestives
      ->find( 'front')
      ->first();
    
    if( !$content)
    {
      $this->Section->notFound();
    }

    $this->set( compact( 'content'));
  }
}
