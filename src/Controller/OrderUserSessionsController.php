<?php
namespace Store\Controller;

use Store\Controller\AppController;

/**
 * OrderUserSessions Controller
 *
 * @property \Store\Model\Table\OrderUserSessionsTable $OrderUserSessions
 */
class OrderUserSessionsController extends AppController
{
  
  public function initialize() 
  {
    parent::initialize();
    $this->loadComponent( 'RequestHandler');
    $this->Auth->allow();
  }

  public function index()
  {
    $query = $this->OrderUserSessions
      ->find( 'front')
    ;

    $contents = $this->paginate( $query);
    $this->set( compact( 'contents'));
  }

  public function view()
  {
    $content = $this->OrderUserSessions
      ->find( 'front')
      ->first();
    
    if( !$content)
    {
      $this->Section->notFound();
    }

    $this->set( compact( 'content'));
  }
}
