<?php
namespace Store\Controller;

use Cake\Core\Configure;
use Store\Pickup\SeurPickup;
use Store\Controller\AppController;

/**
 * Seur Controller
 *
 *
 * @method \Store\Model\Entity\Seur[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class SeurController extends AppController
{
  public function initialize() 
  {
    parent::initialize();
    $this->Auth->allow();
    $this->loadComponent( 'RequestHandler');
  }

  /**
   * Index method
   *
   * @return \Cake\Http\Response|null
   */
  public function index()
  {
    $this->viewBuilder()->setLayout( 'ajax');
    $pickup = new SeurPickup( Configure::read( 'Seur.user'), Configure::read( 'Seur.password'));
    $response = $pickup->getPickups( $this->request->getQuery( 'postcode'));
    $this->set( 'pickups', $response);
  }
}
