<?php
namespace Store\Controller;

use Store\Controller\AppController;

/**
 * TaxRatings Controller
 *
 * @property \Store\Model\Table\TaxRatingsTable $TaxRatings
 */
class TaxRatingsController extends AppController
{
  
  public function initialize() 
  {
    parent::initialize();
    $this->loadComponent( 'RequestHandler');
    $this->Auth->allow();
  }

  public function index()
  {
    $query = $this->TaxRatings
      ->find( 'front')
    ;

    $contents = $this->paginate( $query);
    $this->set( compact( 'contents'));
  }

  public function view()
  {
    $content = $this->TaxRatings
      ->find( 'front')
      ->first();
    
    if( !$content)
    {
      $this->Section->notFound();
    }

    $this->set( compact( 'content'));
  }
}
