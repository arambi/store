<?php

namespace Store\Controller;

use ArrayObject;
use Cake\Log\Log;
use Store\Cart\Cart;
use Cake\Event\Event;
use Cake\Utility\Text;
use Cake\Core\Configure;
use Cake\Routing\Router;
use Website\Lib\Website;
use Cake\Utility\Security;
use Cake\Utility\Inflector;
use Store\Payment\Checkout;
use Store\Config\StoreConfig;
use Letter\Mailer\MailerAwareTrait;
use Store\Controller\AppController;
use Cake\Http\Exception\NotFoundException;
use Cake\ORM\TableRegistry;

/**
 * Orders Controller
 *
 * @property \Store\Model\Table\OrdersTable $Orders
 */
class OrdersController extends AppController
{
    use MailerAwareTrait;

    public $errors;

    public function initialize()
    {
        parent::initialize();
        $this->loadComponent('RequestHandler');
        $this->loadComponent('Store.Cart');
        $this->Auth->allow([
            'additem',
            'deleteitem',
            'cart',
            'express',
            'states',
            'order',
            'quantity',
            'update',
            'checkout',
            'payment',
            'success',
            'error',
            'update',
            'invoice',
            'coupon',
            'couponremove',
            'view',
            'delete',
            'refundLineItem',
            'refund',
            'refundShippingCost',
            'invoiceDownload',
            'deliveryNoteDownload',
        ]);
    }

    private function addErrors($errors, $key)
    {
        foreach ($errors as $field => $error) {
            $this->errors[$key][$field] = current($error);
        }
    }

    public function order()
    {
        $this->set([
            'order' => $this->Cart->order(),
            '_serialize' => ['order']
        ]);
    }

    /**
     * Añade un item a la cesta de la compra
     *
     * @return
     */
    public function addItem()
    {
        if ($this->request->is('post')) {
            $result = $this->Cart->addItem($this->request->getData('product'));
            $order = $this->Cart->order(true);

            $event = new Event('Store.Controller.Orders.afterAddItem', $this, [
                $result,
                $order,
            ]);

            $this->getEventManager()->dispatch($event);

            $this->set([
                'result' => $result,
                '_serialize' => ['result']
            ]);
        }
    }


    public function quantity()
    {
        $result = $this->Cart->itemQuantity(
            $this->request->getData('id'),
            $this->request->getData('action'),
            ($this->request->getData('quantity') ? $this->request->getData('quantity') : 1)
        );

        $draft = $result['draft'];
        $item = $result['item'];

        $order = $this->Cart->order();

        if ($draft && !empty($draft->errors())) {
            $this->set([
                'errors' => $draft->errors()
            ]);
        }

        $event = new Event('Store.Controller.Orders.afterQuantityItem', $this, [
            $result,
        ]);

        $this->getEventManager()->dispatch($event);

        $this->set(compact('order', 'item'));
        $this->set([
            '_serialize' => true
        ]);
    }

    public function deleteitem()
    {
        if ($this->request->is('post')) {
            $result = $this->Cart->deleteItem($this->request->getData('id'));

            $event = new Event('Store.Controller.Orders.afterDeleteItem', $this, [
                $result,
            ]);

            $this->getEventManager()->dispatch($event);

            $this->set([
                'order' => $result['order'],
                'item' => $result['item'],
                '_serialize' => ['order', 'item']
            ]);
        }
    }

    public function cart()
    {
        $order = $this->Cart->order();
        $this->set(compact('order'));

        // Event
        $event = new Event('Store.Controller.Orders.afterCart', $this, [
            'order' => $order,
        ]);

        $this->getEventManager()->dispatch($event);
    }

    public function express()
    {
        // Event
        $event = new Event('Store.Controller.Orders.beforeCart', $this, []);
        $this->getEventManager()->dispatch($event);

        $this->errors = [];
        $order = $this->Cart->order(true);

        if (!$order || count($order->line_items) == 0) {
            $this->render('empty');
        }

        // Event
        $event = new Event('Store.Controller.Orders.afterCart', $this, [
            'order' => $order,
        ]);

        $this->getEventManager()->dispatch($event);
        $this->set(compact('order'));
    }

    protected function _addresses()
    {
        $order = $this->Cart->order();

        if (!$this->request->is(['patch', 'post', 'put'])) {
            if (!$this->Auth->user()) {
                if (!empty($order->address_delivery_id)) {
                    $address_delivery = $this->Orders->AddressDelivery->get($order->address_delivery_id);
                    $this->set(compact('address_delivery'));
                } else {
                    $address_delivery = $this->Orders->AddressDelivery->newEntity();
                    $address_delivery->set('country_id', 6);
                    $this->set(compact('address_delivery'));
                }

                if (!empty($order->address_invoice_id)) {
                    $address_invoice = $this->Orders->AddressDelivery->get($order->address_invoice_id);
                    $this->set(compact('address_invoice'));
                } else {
                    $address_invoice = $this->Orders->AddressDelivery->newEntity();
                    $address_invoice->set('country_id', 6);
                    $this->set(compact('address_invoice'));
                }
            }
        }
    }

    public function address()
    {
        $this->loadModel('Store.Addresses');

        // Comprobación de que puede estar en este paso
        $order = $this->Cart->order();

        if (!$this->Cart->isValidForAddress($order)) {
            return $this->redirect($this->Section->url([
                'plugin' => 'Store',
                'controller' => 'Orders',
                'action' => 'cart'
            ]));
        }
        //

        $list = $this->Addresses->find()->where([
            'Addresses.user_id' => $this->Auth->user('id')
        ]);

        if ($list->count() == 0) {
            $this->request->getSession()->write('AddressEdit', $this->Section->url([
                'plugin' => 'Store',
                'controller' => 'Orders',
                'action' => 'address'
            ]));

            return $this->redirect($this->Section->url([
                'plugin' => 'Store',
                'controller' => 'Addresses',
                'action' => 'add'
            ]));
        }

        $addresses = $this->Addresses->find()
            ->where([
                'Addresses.user_id' => $this->Auth->user('id')
            ])
            ->contain([
                'Countries',
                'States'
            ]);

        $this->set(compact('list', 'addresses'));

        if ($this->request->is(['patch', 'post', 'put'])) {
            $this->Orders->patchEntity($order, $this->request->data);

            if ($this->Orders->saveOrder($order)) {
                return $this->redirect($this->Section->url([
                    'plugin' => 'Store',
                    'controller' => 'Orders',
                    'action' => 'checkout'
                ]));
            } else {
            }
        }

        $this->set(compact('order'));
    }

    public function checkout()
    {
        // Comprobación de que puede estar en este paso
        $order = $this->Cart->order();

        if (!$this->Cart->isValidForCheckout($order)) {
            return $this->redirect($this->Section->url([
                'plugin' => 'Store',
                'controller' => 'Orders',
                'action' => 'address'
            ]));
        }

        if ($this->request->is(['patch', 'post', 'put'])) {
            $this->Orders->patchEntity($order, $this->request->data);

            if ($this->Orders->saveOrder($order)) {
                return $this->redirect($this->Section->url([
                    'plugin' => 'Store',
                    'controller' => 'Orders',
                    'action' => 'payment'
                ]));
            } else {
            }
        }

        // Event
        $event = new Event('Store.Controller.Orders.afterCheckout', $this, [
            'order' => $order,
        ]);
        $this->getEventManager()->dispatch($event);

        $this->set('order', $order);
    }


    public function payment()
    {
        if (Configure::read('AppEvents.register')) {
            $this->request->getSession()->write('AppEvents.register', Configure::read('AppEvents.register'));
        }

        $order = $this->Cart->order(true);

        if (!$order) {
            return $this->render('empty');
        }

        $this->set(compact('order'));
        $response = $this->Cart->payment($order);

        if (!$this->Cart->isValidForPayment($order)) {
            $this->redirect($this->Section->url([
                'plugin' => 'Store',
                'controller' => 'Orders',
                'action' => 'express'
            ]));
        }

        if ($response->hasRedirect()) {
            return $this->redirect($response->redirect);
        }

        if ($response->success()) {
            return $this->redirect($this->Section->url([
                'plugin' => 'Store',
                'controller' => 'Orders',
                'action' => 'success'
            ]) . '?cart=' . $order->salt . '&timestamp=' . strtotime('+10 minute'));
        } else {
            if ($response->redirectToError()) {
                return $this->redirect($this->Section->url([
                    'plugin' => 'Store',
                    'controller' => 'Orders',
                    'action' => 'error'
                ]));
            } else {
                return $this->redirect($this->Section->url([
                    'plugin' => 'Store',
                    'controller' => 'Orders',
                    'action' => 'express'
                ]));
            }
        }
    }

    public function success()
    {
        $content = $this->Orders
            ->find('order')
            ->where([
                'Orders.salt' => $this->request->getQuery('cart')
            ])
            ->first();


        if (!$content) {
            throw new NotFoundException(__('Página no encontrada'));
        }

        // Logueo del usuario
        if ((StoreConfig::getConfig('registerMandatory') || $content->want_account) && !$this->Auth->user()) {
            $user = $this->Orders->Users->find('auth')
                ->where(['Users.id' => $content->user_id])
                ->first();

            if ($user) {
                $user = $user->toArray();
                $this->Auth->setUser($user);

                return $this->redirect($this->Section->url([
                    'plugin' => 'Store',
                    'controller' => 'Orders',
                    'action' => 'success'
                ]) . '?cart=' . $content->salt . '&timestamp=' . strtotime('+10 minute'));
            }
        }

        $event = new Event('Store.Controller.Orders.orderSuccess', $this, [
            'order' => $content,
        ]);

        $this->getEventManager()->dispatch($event);

        $this->set(compact('content'));
    }

    public function error()
    {
    }

    public function update()
    {
        if ($this->request->is(['patch', 'post', 'put'])) {
            $order = $this->Cart->order(true);
            $data = $this->request->getData('Orders');

            if (!empty($order->user_id)) {
                $data['user_id'] = $order->user_id;
            }

            $data = new ArrayObject($data);

            $event = new Event('Store.Controller.Orders.beforePatchEntity', $this, [
                'order' => $order,
                'data' => $data,
            ]);

            $this->getEventManager()->dispatch($event);

            $this->Orders->patchEntity($order, (array)$data, [
                'validate' => 'checkout'
            ]);

            Checkout::update($order, $this->getRequest());

            $event = new Event('Store.Controller.Orders.beforeUpdate', $this, [
                'order' => $order,
            ]);

            $this->getEventManager()->dispatch($event);
            \Cake\Log\Log::debug($order->is_valid ? 'true' : 'false');

            if ($this->request->getQuery('submit') && $order->is_valid && empty($order->user_id) && $user = $this->Orders->createUser($order)) {
                $this->Auth->setUser($user->toArray());
                $this->request->getSession()->write('AppEvents.register', $user->toArray());
                $this->Orders->query()->update()
                    ->set('user_id', $user->id)
                    ->where([
                        'Orders.id' => $order->id
                    ])
                    ->execute();

                $order = $this->Orders->getOrder([
                    'Orders.id' => $order->id
                ]);

                $event = new Event('Store.Controller.Orders.beforeUpdateSubmit', $this, [
                    'order' => $order,
                ]);

                $this->getEventManager()->dispatch($event);
                $this->Orders->save($order);
                Cart::resetStateDeliveryId();
                $this->update();
            }

            if (!$order->is_valid) {
                $set = [
                    'error' => [
                        'title' => __d('app', 'Hay errores en el pedido'),
                        'text' => __d('app', 'Por favor, revisa los campos e inténtalo de nuevo')
                    ],
                ];

                if (!empty($user)) {
                    $set['redirect'] = $this->Section->url([
                        'plugin' => 'Store',
                        'controller' => 'Orders',
                        'action' => 'express'
                    ]);
                }

                // Error de que no se ha encontrado una tarifa de envío lanza un evento
                if ($this->request->getQuery('submit') && $order->getError('carrier_rule_id')) {
                    // Event
                    $event = new Event('Store.Model.Carriers.noCarrier', $this, [
                        'order' => $order,
                    ]);

                    $this->getEventManager()->dispatch($event);
                }

                $this->set($set);
            }

            $order->unsetProperty(array_keys($order->getErrors()));
            $order->unsetErrors();
            $order->unsetProperty('payment_method');

            $event = new Event('Store.Controller.Orders.beforeUpdateSubmit', $this, [
                'order' => $order,
            ]);

            $this->getEventManager()->dispatch($event);

            if (!$this->Orders->save($order)) {
            } else {
                Cart::resetStateDeliveryId();

                foreach ($order->line_items as $item) {
                    $item->title;
                    $item->unsetProperty('photo');
                    $this->Orders->LineItems->save($item, ['associated' => false]);
                }

                $this->Orders->saveFields($order, $data);
            }
        }


        $this->set([
            '_serialize' => true
        ]);
    }

    public function view()
    {
        $this->loadModel('Store.Orders');
        $content = $this->Orders->find('order')
            ->where([
                'Orders.salt' => $this->request->getParam('salt'),
                'Orders.status IS NOT NULL',
            ])
            ->contain([
                'OrderHistories'
            ])
            ->first();

        if (!$content) {
            $this->Section->notFound('No encontrado el order');
        }

        $this->set([
            'content' => $content,
            'cart' => $content
        ]);
    }

    public function invoice()
    {
        $order = $this->Orders->find('order')
            ->where([
                'Orders.salt' => str_replace('.pdf', '', $this->request->getParam('salt')),
            ])
            ->first();

        if (!$order) {
            throw new NotFoundException(__('Invoice no encontrada'));
        }


        $this->redirect($order->invoice_url);
    }

    public function invoiceDownload()
    {
        $order = $this->Orders->find('order')
            ->where([
                'Orders.salt' => str_replace('.pdf', '', $this->request->getQuery('salt')),
            ])->first();

        if (!$order) {
            throw new NotFoundException(__('Invoice no encontrada'));
        }

        $this->set(compact('order'));
        return $this->render('invoice');
    }

    public function deliveryNoteDownload()
    {
        $order = $this->Orders->find('order')
            ->where([
                'Orders.salt' => str_replace('.pdf', '', $this->request->getQuery('salt')),
            ])->first();

        if (!$order) {
            throw new NotFoundException(__('Invoice no encontrada'));
        }

        $this->set(compact('order'));
        return $this->render('invoice');
    }


    /**
     * Añade o intenta añadir un cupón al pedido
     *
     * @return void
     */
    public function coupon()
    {
        $order = $this->Cart->order(true);
        $result = $this->loadModel('Store.Coupons')->addToOrder($this->request->getData('code'), $order, $this->Auth->user('id'));
        $this->Cart->order(true);
        $this->set($result);
        $this->set('_serialize', true);
    }

    /**
     * Elimna un cupón del pedido
     *
     * @return void
     */
    public function couponremove()
    {
        $order = $this->Cart->order(true);
        $this->loadModel('Store.Coupons')->removeFromOrder($order->id);
        $this->Cart->order(true);
        $this->set('_serialize', true);
    }



    public function addpoints()
    {
        $order = $this->Cart->addPoints($this->request->getData('points'));
        $this->set([
            '_serialize' => true
        ]);
    }

    public function delete()
    {
        $this->Cart->clearSession();
        $this->Flash->success(__d('app', 'El carro de la compra ha sido eliminado.'));
        return $this->redirect('/');
    }

    public function states()
    {
        $this->loadModel('Store.States');
        $states = $this->States->find()
            ->order([
                'States.title'
            ])
            ->where([
                'States.country_id' => (int)$this->request->getData('country_id'),
                'States.active' => true
            ])
            ->toArray();

        $keys = array_keys($states);
        $keys = array_map(function ($k) {
            return $k + 1;
        }, $keys);

        $states = array_combine($keys, $states);

        $states[0] = [
            'id' => '',
            'title' => __d('app', '-- Provincia --') . ' *'
        ];

        $this->set([
            'states' => $states,
            '_serialize' => ['states']
        ]);
    }

    public function refund()
    {
        $this->set('_serialize', true);

        foreach ($this->request->getData('items') as $item) {
            if (@$item['refunded'] === 'true') {
                $this->getTableLocator()->get('Store.Refunds')->addItem($item['id'], $item['quantity']);
            }
        }

        $this->set('success', true);
    }

    public function refundShippingCost()
    {
        /** @var \Store\Model\Entity\Order $order */
        $order = $this->Orders->find('order')
            ->where([
                'Orders.salt' => $this->request->getData('order'),
            ])
            ->first();

        $listeners = $this->getEventManager()->listeners('Store.Controller.Orders.refundShippingCost');

        if (!empty($listeners)) {
            $event = new Event('Store.Controller.Orders.refundShippingCost', $this, [
                $order,
                $this->request->getData(),
            ]);

            $this->getEventManager()->dispatch($event);
            return;
        }

        $cost = 0;

        foreach ($this->request->getData('items') as $item) {
            if (@$item['refunded'] === 'true') {
                $line_item = $this->Orders->LineItems->findById($item['id'])->first();

                if ($line_item) {
                    $rule = $this->Orders->Carriers->getOrderRule($order, $line_item->subtotal, $line_item->weight * $item['quantity'], null, true);

                    if ($rule) {
                        $cost += $rule->price;
                    }
                }
            }
        }

        $cost = $order->setPrice($order->realPrice($cost, $order->shipping_handling_tax_rate));
        $this->set('_serialize', true);
        $this->set(compact('cost'));
    }

    public function refundLineItem()
    {
        $this->set('_serialize', true);

        $line_item = $this->Orders->LineItems->find()
            ->where([
                'LineItems.salt' => $this->request->getData('item'),
            ])
            ->first();

        $quantity = $this->request->getData('quantity') ?? $line_item->quantity;

        TableRegistry::getTableLocator()->get('Store.Refunds')->addItem($line_item->id, $quantity);

        $this->set([
            'success' => true
        ]);
    }
}
