<?php
namespace Store\Controller;

use Store\Controller\AppController;

/**
 * Promos Controller
 *
 * @property \Store\Model\Table\PromosTable $Promos
 */
class PromosController extends AppController
{

  public function initialize() 
  {
    parent::initialize();
    $this->loadComponent( 'RequestHandler');
    $this->loadComponent( 'Store.ShopSearch');
    $this->loadComponent( 'Paginator');

    $this->Auth->allow();
    $this->paginate = [
      'limit' => 9,
    ];

  }

  public function index()
  {
    $this->loadModel( 'Store.Promos');
    $this->loadModel( 'Store.Products');
    $promos = $this->Promos->actives([
      'Promos.promo_type IN' => ['categories', 'products']
    ]);

    $ids = [];

    $query = $this->Products->findProducts();
    
    foreach( $promos as $promo)
    {
      $isAllow = $this->Promos->checkUser( $promo, $this->Auth->user());

      if( $isAllow->allow)
      {
        $_ids = $this->Promos->conditionsForSearchProducts( $promo, $query);
        $ids = array_merge_recursive( $ids, (array)$_ids);
      }
    }

    if( empty( $promos->toArray()))
    {
      $query->where(['Products.id' => -1]);
    }

    $or = [];

    if( !empty( $ids ['product_color_ids']))
    {
      $or ['ProductsColors.id IN'] = $ids ['product_color_ids'];
    }

    if( !empty( $ids ['product_ids']))
    {
      $or ['Products.id IN'] = $ids ['product_ids'];
    }


    if( !empty( $or))
    {
      $query->where( [
          'OR' => $or
      ]);
    }
    else
    {
      $query->where(['Products.id' => -1]);
    }

    $products = $this->paginate( $query);
    $this->set( compact( 'products'));
  }
}
