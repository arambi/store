<?php
namespace Store\Controller;

use Store\Controller\AppController;
use Cake\Collection\Collection;

/**
 * Collections Controller
 *
 * @property \Store\Model\Table\CollectionsTable $Collections
 */
class CollectionsController extends AppController
{

  public function initialize() 
  {
    parent::initialize();
    $this->Auth->allow();
    $this->loadModel( 'Store.Products');
  }

  public function view()
  {
    $query =  $this->Collections->find( 'slug', ['slug' => $this->request->params ['slug']])
      ->contain([
        'ProductsColors',
        'Products' => function( $q){
          return $q->find( 'products');
        }
      ]);

    // De Cofree.PublisherBehavior
    $this->Collections->publishedQuery( $query)->fromToQuery( $query);
    $content = $query->first();
    $product_colors_ids = (new Collection( $content->products_colors))->extract( 'id');
    
    $product_colors_ids = $product_colors_ids->toArray();

    if( !empty( $product_colors_ids))
    {
      $products = $this->Products->find( 'products')
        ->where([
          'ProductsColors.id IN' => $product_colors_ids
        ])
        ->toArray();
    }
    else
    {
      $products = [];
    }      
    
    $products = array_merge( $products, $content->products);
      

    $this->set( compact( 'content', 'products'));
  }
}
