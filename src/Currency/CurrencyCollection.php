<?php

namespace Store\Currency;

use Cake\Utility\Inflector;

class CurrencyCollection
{
    static protected $_collection = [];

    static protected $_default = 'eur';

    protected static function _getNamespace(string $code): string
    {
        return '\\Store\\Currency\\' . Inflector::camelize($code);
    }

    public static function getCurrency($code = null): CurrencyInterface
    {
        if (!$code) {
            $code = static::$_default;
        }

        return static::_getObject($code);
    }

    protected static function _getObject($code): CurrencyInterface
    {
        if (!isset(static::$_collection[$code])) {
            $className = static::_getNamespace($code);
            static::$_collection[$code] = new $className();
        }

        return static::$_collection[$code];
    }

    public static function setDefault(string $code): void
    {
        static::$_default = $code;
    }

    public static function getDefault(): string
    {
        return static::$_default;
    }
    
}