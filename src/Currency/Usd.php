<?php 

namespace Store\Currency;

class Usd implements CurrencyInterface
{
    public function getName(): string
    {
        return 'Dollar';
    }

    public function getSymbol(): string
    {
        return '$';
    }

    public function getCode(): string
    {
        return 'USD';
    }

    public function getFormat($value)
    {
        $price = number_format($value, 2, "</n>.<d>", ",");
        $price = '<c>'. $this->getSymbol() . '</c><n>' . $price . '</d>';
        return $price;
    }

    public function getNumberFormat($value): string
    {
        return number_format($value, 2, ".", ",");
    }

    public function getProductKey(): string
    {
        return 'store_price_usd';
    }
}