<?php 

namespace Store\Currency;

class Eur implements CurrencyInterface
{
    public function getName(): string
    {
        return 'Euro';
    }

    public function getSymbol(): string
    {
        return '€';
    }

    public function getCode(): string
    {
        return 'EUR';
    }

    public function getFormat($value)
    {
        if ($value === null) {
            return null;
        }

        $price = number_format($value, 2, "</n>,<d>", ".");
        $price = '<n>' . $price . '</d> <c>'. $this->getSymbol() . '</c>';
        return $price;
    }

    public function getNumberFormat($value): string
    {
        return number_format($value, 2, ",", ".");
    }

    public function getProductKey(): string
    {
        return 'store_price';
    }
}