<?php 

namespace Store\Currency;

interface CurrencyInterface
{
    public function getName(): string;

    public function getSymbol(): string;

    public function getCode(): string;

    public function getFormat($price);

    public function getProductKey(): string;

    public function getNumberFormat($value): string;


}