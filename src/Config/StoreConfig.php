<?php

namespace Store\Config;

use Website\Lib\Website;

class StoreConfig
{
    const ORDER_STATUS_PENDING = 'pending';
    const ORDER_STATUS_ACCEPTED = 'accepted';
    const ORDER_STATUS_ENVOY = 'envoy';
    const ORDER_STATUS_CANCELED = 'canceled';
    const ORDER_STATUS_RECEIVED = 'received';
    const ORDER_STATUS_WAITING_PAY = 'waiting_pay';
    const ORDER_STATUS_RETURNED_PENDENT = 'returned_pendent';
    const ORDER_STATUS_RETURNED = 'returned';
    
    const ITEM_STATUS_REFUNDED_REQUEST = 'refunded_request';
    const ITEM_STATUS_REFUNDED_WORKING = 'refunded_working'; // Envío de mensajería
    const ITEM_STATUS_REFUNDED_SEND = 'refunded_send';
    const ITEM_STATUS_REFUNDED_RECEIVED = 'refunded_received';
    const ITEM_STATUS_REFUNDED_ACCEPT = 'refunded_accept';
    const ITEM_STATUS_REFUNDED_CANCELED = 'refunded_canceled';
    const ITEM_STATUS_REFUNDED_REFUSED = 'refunded_refused';

    const REFUND_STATUS_REVIEW = 'refund_review';
    const REFUND_STATUS_PROCCESS = 'refund_proccess';
    const REFUND_STATUS_SENT = 'refund_sent';
    const REFUND_STATUS_RECEIVED = 'refund_received';
    const REFUND_STATUS_FINALIZED = 'refund_finalized';
    const REFUND_STATUS_REFUSED = 'refunded_refused';

    const DISCOUNT_STRATEGY_WITH_TAXES = 1;
    const DISCOUNT_STRATEGY_WITHOUT_TAXES = 2;
    
    /**
     * El objeto model del producto
     *
     * @var Cake\ORM\Table
     */
    protected static $_productsModel;

    protected static $_productsModelCallback;

    protected static $_customStatuses = [];

    protected static $_defaults = [
        'withAttributes' => false,
        'invoiceOnly' => false,
        'itemTitle' => false,
        'itemFields' => [],
        'orderDefaults' => [],
        'notShowCarrier' => false,
        'registerMandatory' => false,
        'dontSendStatus' => [],
        'dontChangeStatusFromAdmin' => false,
        'invoiceEmailElement' => 'Store.invoice_mail',
        'noCarrierMessage' => false,
        'adminWithoutTaxes' => false,
        'mailClientWithoutTaxes' => false,
        'onlyFirstname' => false,
        'sendMailWhenNewUser' => true,
        'withPoints' => false,
        'withInvoices' => true,
        'sendStatusToAdmin' => false,
        'allGroupAttributesMandatory' => false,
        'onlyInvoiceVat' => false,
        'showPricesInEdition' => true,
        'statuses' => [
            'pending',
            'accepted',
            'envoy',
            'canceled',
            'received',
            'waiting_pay',
            'returned_pendent',
            'returned',
        ],

        // Dirección principal en el pedido
        'orderMainAddress' => 'delivery',

        // Promos
        'promoTypeCart' => false,
        'promoTypeShipping' => false,

        // Cupones
        'couponDrifts' => false,
        'couponGroups' => false,
        'couponStates' => false,
        'couponStrategy' => StoreConfig::DISCOUNT_STRATEGY_WITH_TAXES,

        // Marketing
        'mailchimpMarketing' => false,
        'googleMarketing' => false,

        // Tarjeta regalo
        'giftCard' => false,
        'giftCardExpireMonths' => 12,

        'hasInvoiceRefund' => true,

        'hasInvoiceNoTaxes' => false,

        'adminEditCarrier' => false,
    ];

    protected static $_config = [];

    protected static $_hooks = [];

    public static function itemStatusTitle($status)
    {
        $texts = [
            self::ITEM_STATUS_REFUNDED_REQUEST => __d( 'app', 'Devolución solicitada'),
            self::ITEM_STATUS_REFUNDED_CANCELED => __d( 'app', 'Devolución rechazada'),
            self::ITEM_STATUS_REFUNDED_WORKING => __d( 'app', 'Devolución en proceso'),
            self::ITEM_STATUS_REFUNDED_SEND => __d( 'app', 'Devolución enviada'),
            self::ITEM_STATUS_REFUNDED_RECEIVED => __d( 'app', 'Devolución recibida'),
            self::ITEM_STATUS_REFUNDED_ACCEPT => __d( 'app', 'Devolución aceptada'),
            self::ITEM_STATUS_REFUNDED_REFUSED => __d( 'app', 'Devolución suspendida'),
        ];

        if (!empty($texts[$status])) {
            return $texts[$status];
        }
    }

    public static function refundStatusTitle($status)
    {
        $texts = [
            self::REFUND_STATUS_REVIEW => __d( 'app', 'Devolución en revisión'),
            self::REFUND_STATUS_PROCCESS => __d( 'app', 'Devolución en proceso'),
            self::REFUND_STATUS_SENT => __d( 'app', 'Devolución enviada'),
            self::REFUND_STATUS_RECEIVED => __d( 'app', 'Devolución recibida'),
            self::REFUND_STATUS_FINALIZED => __d( 'app', 'Devolución finalizada'),
            self::REFUND_STATUS_REFUSED => __d( 'app', 'Devolución suspendida'),
        ];

        if (!empty($texts[$status])) {
            return $texts[$status];
        }
    }

    protected static function _defaultHook($name)
    {
        $defaults = [
            'itemTitle' => function ($item) {
                return $item->title;
            },
            'invoiceTitle' => function ($item) {
                return $item->title;
            },
            'itemDescription' => function ($item) {
                return null;
            },
            'validateCheckout' => false,
            'invoicePrefix' => function ($invoice) {
                if (!empty($invoice)) {
                    return $invoice->invoice_num;
                }
            },
            'receiptPrefix' => function ($invoice) {
                return $invoice->invoice_num;
            },
            'checkZeroShipping' => function ($order) {
                return true;
            },
        ];

        if (!empty($defaults[$name])) {
            return $defaults[$name];
        }
    }

    public static function setHook($name, $callback = null)
    {
        if (is_array($name)) {
            foreach ($name as $key => $callback) {
                self::$_hooks[$key] = $callback;
            }
        } else {
            self::$_hooks[$name] = $callback;
        }
    }

    public static function getHook($name, $args = null)
    {
        if (!array_key_exists($name, self::$_hooks)) {
            $callback = self::_defaultHook($name);
        } else {
            $callback = self::$_hooks[$name];
        }

        if (is_array($args) && is_callable($callback)) {
            return call_user_func_array($callback, $args);
        }

        return $callback;
    }

    public static function getStatuses()
    {
        $statuses = [
            'pending' => __d('app', 'Pendiente de pago'),
            'accepted' => __d('app', 'Pago aceptado'),
            'envoy' => __d('app', 'Enviado'),
            'received' => __d('app', 'Entregado'),
            'waiting_returned' => __d('app', 'Devolución solicitada'),
            'canceled' => __d('app', 'Cancelado'),
            'waiting_pay' => __d('app', 'Esperando confirmación bancaria'),
            'returned_pendent' => __d('app', 'Pendiente de devolución/cambio'),
            'returned' => __d('app', 'Devuelto'),
        ];

        if (!empty(static::$_customStatuses)) {
            return static::$_customStatuses;
        }

        return $statuses;
    }

    public static function setStatuses($statuses)
    {
        static::$_customStatuses = $statuses;
    }



    public static function setProductsModel($callback, $execute = false, $force = false)
    {
        if (!empty(static::$_productsModel) && !$force) {
            return;
        }

        static::$_productsModelCallback = $callback;

        if ($execute) {
            static::$_productsModel = $callback();
        }
    }

    public static function getProductsModel()
    {
        if (static::$_productsModel === null) {
            $callback = static::$_productsModelCallback;
            static::$_productsModel = $callback();
        }

        return static::$_productsModel;
    }

    public static function setConfig(array $config)
    {
        static::$_config = $config;

        foreach (static::$_defaults as $key => $value) {
            if (!isset(static::$_config[$key])) {
                static::$_config[$key] = $value;
            }
        }
    }

    public static function setConfigParam(string $key, $value)
    {
        static::$_config[$key] = $value;
    }

    public static function getConfig($key = null)
    {
        if ($key === null) {
            return static::$_config;
        }

        if (!array_key_exists($key, static::$_config)) {
            return null;
        }

        return static::$_config[$key];
    }
}
