<?php

namespace Store\Event;

use Cake\Log\Log;
use Cake\Core\Configure;
use Cake\Routing\Router;
use Cake\ORM\TableRegistry;
use Store\Config\StoreConfig;
use Mailchimp\Queue\MailchimpQueue;
use Mailchimp\Traits\MailchimpTrait;
use Cake\Event\EventListenerInterface;

class MailchimpListener implements EventListenerInterface
{
    use MailchimpTrait;

    const STORE_ID = 'store';

    public function implementedEvents()
    {
        return [
            'Store.Model.Order.afterAddItem' => 'afterAddItem',
            'Store.Model.Order.afterQuantityItem' => 'afterAddItem',
            'Store.Model.Order.afterDeleteItem' => 'afterDeleteItem',
            'User.Controller.Users.afterRegister' => 'afterRegister',
            'User.Controller.Users.afterRegisterFromOrder' => 'afterRegisterOrder',
            'Store.Model.Order.finalizeOrder' => 'finalizeOrder',
            'Store.Model.Product.afterSave' => 'productAfterSave',
            'Store.Model.Product.afterDelete' => 'productAfterDelete',
        ];
    }

    private function getItem($id)
    {
        /** @var LineItem $item */
        $item = TableRegistry::getTableLocator()->get('Store.LineItems')->find()->where([
            'LineItems.id' => $id
        ])->contain([
            'Products' => function ($q) {
                return $q->find('products')
                    ->contain('Taxes');
            },
            'ProductAttributes' => [
                'Attributes'
            ],
        ])->first();

        if ($item) {
            $item->build();
        }

        return $item;
    }

    private function getOrder($id)
    {
        $order = TableRegistry::getTableLocator()->get('Store.Orders')->find()
            ->where([
                'id' => $id
            ])
            ->first();

        return $order;
    }

    public function afterAddItem($event, $item, $new_order = false)
    {
        $item = $this->getItem($item->id);
        $order = $this->getOrder($item->order_id);

        if (empty($order->user_id)) {
            return;
        }

        if ($new_order) {
            $data = [
                'id' => (string)$order->id,
                'customer' => [
                    'id' => (string)$order->user_id
                ],
                'currency_code' => 'EUR',
                'order_total' => (float)str_replace(',', '.', $order->total),
                'tax_total' => (float)str_replace(',', '.', $order->taxes),
                'lines' => [
                    $this->itemData($item)
                ]
            ];

            MailchimpQueue::add('/ecommerce/stores/' . self::STORE_ID . '/carts', 'post', $data);
        } else if ($item->quantity > 1) {
            MailchimpQueue::add('/ecommerce/stores/' . self::STORE_ID . '/carts/' . $item->order_id . '/lines/' . $item->id, 'patch', $this->itemData($item));
        } else {
            MailchimpQueue::add('/ecommerce/stores/' . self::STORE_ID . '/carts/' . $item->order_id . '/lines', 'post', $this->itemData($item));
        }
        
        
    }

    public function productAfterSave($event, $entity)
    {
        \Cake\Log\Log::debug( 'Procesando Mailchimp '. $entity->id);
        $product = StoreConfig::getProductsModel()->find()
            ->where([
                'id' => $entity->id
            ])
            ->first();

        $data = [
            'id' => (string)$product->id,
            'title' => $product->title,
            'url' => Router::url($product->link(), true),
            'description' => $product->mc_description,
            'type' => $product->mc_type,
            'image_url' => Router::url($product->mc_image, true),
            'variants' => [
                [
                    'price' => $product->realPrice($product->price, $product->tax),
                    'id' => (string)$product->id,
                    'title' => $product->title,
                ]
            ]
        ];


        $mailchimp_product = $this->mc()->get( '/ecommerce/stores/'. self::STORE_ID .'/products/'. $product->id);
        
        if (isset($mailchimp_product['status']) && $mailchimp_product['status'] == 404) {
            MailchimpQueue::add('/ecommerce/stores/' . self::STORE_ID . '/products', 'post', $data);
        } else {
            MailchimpQueue::add('/ecommerce/stores/' . self::STORE_ID . '/products/' . $product->id, 'patch', $data);
        }
    }

    public function productAfterDelete($event, $product)
    {
        MailchimpQueue::add('/ecommerce/stores/' . self::STORE_ID . '/products/' . $product->id, 'delete');
    }

    public function afterDeleteItem($event, $item)
    {
        $item = $this->getItem($item->id);

        if ($item) {
            $order = $this->getOrder($item->order_id);

            if (empty($order->user_id)) {
                return;
            }

            MailchimpQueue::add('/ecommerce/stores/' . self::STORE_ID . '/carts/' . $item->order_id . '/lines/' . $item->id, 'delete');
        }
    }

    private function itemData($item)
    {
        $data = [
            'id' => (string)$item->id,
            'product_id' => (string)$item->product_id,
            'quantity' => $item->quantity,
            'price' => $item->realPrice($item->subtotal, $item->tax_rate),
        ];

        if (!empty($item->product_attribute_id)) {
            $data['product_variant_id'] = (string)$item->product_attribute_id;
        } else {
            $data['product_variant_id'] = (string)$item->product_id;
        }

        return $data;
    }

    public function afterRegister($event, $user)
    {
        $controller = $event->getSubject();

        if (!method_exists($controller, 'getRequest')) {
            return;
        }

        $controller_data = $controller->getRequest()->getData();

        $data = [
            'id' => (string)$user->id,
            'email_address' => $user->email,
            'opt_in_status' => true,
            'first_name' => $user->name,
        ];

        if (isset($controller_data['Address'])) {
            $data['last_name'] = !empty($controller_data['Address']['lastname']) ? $controller_data['Address']['lastname'] : ' - ';
            $data['address'] = [
                'address1' => $controller_data['Address']['address'],
                'country' => $this->getCountryName($controller_data['Address']['country_id']),
                'province' => $this->getStateName($controller_data['Address']['state_id']),
                'city' =>  $controller_data['Address']['city'],
                'postal_code' =>  $controller_data['Address']['postcode'],
            ];
        }

        MailchimpQueue::add('/ecommerce/stores/' . self::STORE_ID . '/customers', 'post', $data);
    }

    public function afterRegisterOrder($event, $user, $order)
    {
        $data = [
            'id' => (string)$user->id,
            'email_address' => $user->email,
            'opt_in_status' => true,
            'first_name' => $user->name,
        ];

        MailchimpQueue::add('/ecommerce/stores/' . self::STORE_ID . '/customers', 'post', $data);
    }

    private function getCountryName($id)
    {
        return TableRegistry::getTableLocator()->get('Store.Countries')
            ->find()
            ->where([
                'Countries.id' => $id
            ])
            ->first()
            ->title;
    }

    private function getStateName($id)
    {
        return TableRegistry::getTableLocator()->get('Store.States')
            ->find()
            ->where([
                'States.id' => $id
            ])
            ->first()
            ->title;
    }

    public function finalizeOrder($event, $order)
    {
        $lines = [];

        foreach ($order->line_items as $item) {
            $lines[] = $this->itemData($item);
        }

        $data = [
            'id' => (string)$order->id,
            'currency_code' => 'EUR',
            'order_total' => (float)str_replace(',', '.', $order->total),
            'tax_total' => (float)str_replace(',', '.', $order->taxes),
            'discount_total' => (float)str_replace(',', '.', $order->discount_price),
            'shipping_total' => (float)str_replace(',', '.', $order->shipping_handling),
            'order_url' => Router::url($order->order_url, true),
            // 'cancelled_at_foreign' => '',
            'processed_at_foreign' => $order->order_date->format('c'),
            'updated_at_foreign' => $order->modified->format('c'),
            'shipping_address' => $this->getAddress($order, 'delivery'),
            'billing_address' => $this->getAddress($order, 'invoice'),
            'lines' => $lines,
        ];

        if ($financial_status = $this->getFinancialStatus($order->status)) {
            $data['financial_status'] = $financial_status;
        }

        if ($fulfillment_status = $this->getFulfillmentStatus($order->status)) {
            $data['fulfillment_status'] = $fulfillment_status;
        }

        if (!empty($order->coupon_discount) && $coupon = $this->getCouponCode($order->id)) {
            $data['promos'] = [
                'code' => $coupon->code,
                'amount_discounted' => $order->coupon_discount,
                'type' => 'fixed'
            ];
        }

        
        
        if (!empty($order->user_id)) {
            $data['customer'] = [
                'id' => (string)$order->user_id
            ];
        } else {
            $address = [
                'address1' => $order->adr_invoice_address,
                'city' => $order->adr_invoice_city,
                'province' => $order->state_invoice->title,
                'postal_code' => $order->adr_invoice_postcode,
                'country' => $order->country_invoice->title,
            ];

            $data['customer'] = [
                'id' => (string)$order->id . '_order',
                'email_address' => !empty($order->adr_invoice_email) ? $order->adr_invoice_email : $order->adr_delivery_email,
                'first_name' => $order->adr_invoice_firstname,
                'last_name' => !empty($order->adr_invoice_lastname) ? $order->adr_invoice_lastname : ' - ',
                'address' => $address,
                'opt_in_status' => true
            ];

            if ($company = $order->adr_invoice_company) {
                $data['company'] = $company;
            }
        }

        if ($campaign_id = Configure::read('Mailchimp.campaign_id')) {
            $data ['campaign_id'] = $campaign_id;
        }

        if ($email_user = Configure::read('Mailchimp.campaign_member_email')) {
            $data ['customer']['email_address'] = $email_user;
        }

        MailchimpQueue::add('/ecommerce/stores/' . self::STORE_ID . '/orders', 'post', $data);
    }

    private function getAddress($order, $type)
    {
        $address = [
            'name' => $order->{'adr_' . $type . '_firstname'} . ' ' . $order->{'adr_' . $type . '_lastname'},
            'address1' => $order->{'adr_' . $type . '_address'},
            'city' => $order->{'adr_' . $type . '_city'},
            'province' => $order->{'state_' . $type}->title,
            'postal_code' => $order->{'adr_' . $type . '_postcode'},
            'country' => $order->{'country_' . $type}->title,
            'phone' => $order->{'adr_' . $type . '_phone'},
        ];

        if ($company = $order->{'adr_' . $type . '_company'}) {
            $address['company'] = $company;
        }

        return $address;
    }

    private function getCouponCode($order_id)
    {
        $coupon_order = TableRegistry::getTableLocator()->get('Store.CouponsOrders')->find()
            ->where([
                'order_id' => $order_id
            ])
            ->first();

        if ($coupon_order) {
            $coupon = TableRegistry::getTableLocator()->get('Store.Coupons')->find()
                ->where([
                    'id' => $coupon_order->coupon_id
                ])
                ->first();

            if ($coupon) {
                return $coupon;
            }
        }
    }

    private function getFinancialStatus($status)
    {
        $combine = [
            'accepted' => 'paid',
            'pending' => 'pending',
            'returned' => 'refunded',
            'canceled' => 'cancelled',
            'envoy' => 'paid',
        ];

        if (array_key_exists($status, $combine)) {
            return $combine[$status];
        }
    }

    private function getFulfillmentStatus($status)
    {
        $combine = [
            'envoy' => 'shipped',
        ];

        if (array_key_exists($status, $combine)) {
            return $combine[$status];
        }
    }
}
