<?php 

namespace Store\Event;

use Cake\Event\EventListenerInterface;
use Cake\ORM\TableRegistry;

class OrdersListener implements EventListenerInterface
{
  public function implementedEvents()
  {
    return [
      'User.Controller.Users.afterLogin' => 'afterLogin',
      'User.Controller.Users.afterLogout' => 'afterLogout',
    ];
  }

  public function afterLogin( $event, $user)
  {
    $controller = $event->subject();

    if( !isset( $controller->Cart))
    {
      $controller->loadComponent( 'Store.Cart');
    }
    
    $has = $controller->Cart->hasOrder();

    $OrdersTable = TableRegistry::get( 'Store.Orders');

    if( !$has)
    {
      $order = $OrdersTable->getLastUserOrder( $user ['id']);

      if( $order)
      {
        $controller->Cart->writeSession( $order);
      }
    }
    else
    {
      $order = $controller->Cart->order();

      if( $order instanceof \Store\Model\Entity\Order)
      {
        if( empty( $order->user_id))
        {
          $order->user_id = $user ['id'];
        }
        
        $OrdersTable->setAddressesToNewOrder( $order);
        $OrdersTable->save( $order);
        // Borra el Stripe Payment
        try {
          TableRegistry::get( 'Payment.CardStripeOrders')->deleteAll([
            'order_id' => $order->id,
            'model' => 'Store.Orders'
          ]);
        } catch (\Throwable $th) {
        }
      }

      
    }    
  }

  public function afterLogout( $event, $user)
  {
    $controller = $event->subject();

    if( !isset( $controller->Cart))
    {
      $controller->loadComponent( 'Store.Cart');
    }
    
    $controller->Cart->clearSession();
  }

} 