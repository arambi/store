<?php

namespace Store\Event;

use Cake\ORM\Query;
use Cake\ORM\Table;
use Cake\Core\Plugin;
use Cake\Routing\Router;
use Website\Lib\Website;
use Cake\ORM\TableRegistry;
use Store\Config\StoreConfig;
use Cake\Event\EventListenerInterface;

class ShopListener implements EventListenerInterface
{
    public function implementedEvents()
    {
        return [
            'Manager.View.Sites.beforeBuild' => 'websiteBuild',
            'Manager.Behavior.Crudable.Users.initialize' => 'initializeUsers',
            'Store.Model.Order.afterSetSubtotal' => 'afterSetSubtotal',
            'User.Controller.Users.beforeLogin' => 'beforeLogin',
            'User.Model.Users.findAuth' => 'findAuth',
            'Payment.Controller.RedsysReference.getOrderId' => 'redsysReferenceOrder',
            'Payment.Controller.RedsysRest.getOrderId' => 'redsysReferenceOrder',
        ];
    }

    public function redsysReferenceOrder($event, $obOrder, $payment)
    {
        $order = TableRegistry::getTableLocator()->get('Store.Orders')->findById($payment->order_id)->first();
        $obOrder->card_id = $order->card_id;
    }

    /**
     * Se encarga de modificar el crud de website
     * Le añade una pestaña nueva para meterle las preferencias de la tienda
     * 
     * @param  Cake\Event\event $event
     * @param  Table $table
     */
    public function websiteBuild($event, $table)
    {
        $view = $event->subject();

        $table->crud->addFields([
            'settings.store_name' => [
                'label' => __d('admin', 'Nombre'),
                'type' => 'string'
            ],
            'settings.store_cif' => [
                'label' => __d('admin', 'CIF'),
                'type' => 'string'
            ],
            'settings.store_address' => [
                'label' => __d('admin', 'Dirección'),
                'type' => 'string'
            ],
            'settings.store_postcode' => [
                'label' => __d('admin', 'Código postal'),
                'type' => 'string'
            ],
            'settings.store_city' => [
                'label' => __d('admin', 'Ciudad'),
                'type' => 'string'
            ],
            'settings.store_state' => [
                'label' => __d('admin', 'Provincia'),
                'type' => 'string'
            ],
            'settings.store_country' => [
                'label' => __d('admin', 'País'),
                'type' => 'string'
            ],
            'settings.store_price_with_taxes' => [
                'label' => __d('admin', 'Mostrar precios con impuestos'),
                'type' => 'boolean',
                'help' => __d('admin', 'Marca esta opción para que la tienda muestre los precios con descuentos'),
            ],
            'settings.store_show_delivery_days' => [
                'label' => __d('admin', 'Mostrar plazo de entrega en mails'),
                'type' => 'boolean',
                'help' => __d('admin', 'Marca esta opción para que en los emails que se envían al usuario aparezca el plazo de entrega'),
            ],
            'settings.store_has_points' => [
                'label' => __d('admin', 'Tienda con puntos'),
                'type' => 'boolean',
                'help' => __d('admin', 'Marca esta opción para que la tienda tenga un sistema de puntos de descuento'),
            ],
            'settings.store_points_down_value' => [
                'label' => __d('admin', 'Valor del cambio de los puntos'),
                'help' => __d('admin', 'Valor de los puntos en la moneda principal de la tienda'),
                'type' => 'numeric',
                'show' => 'content.settings.store_has_points == true',
            ],
            'settings.store_points_up_value' => [
                'label' => __d('admin', 'Valor de consecución de puntos'),
                'help' => __d('admin', 'Porcentaje de consecución de los puntos sobre el total del pedido'),
                'type' => 'numeric',
                'show' => 'content.settings.store_has_points == true',
            ],
            'settings.store_points_charge_days' => [
                'label' => __d('admin', 'Días cuando se cargarán los puntos en el usuario'),
                'help' => __d('admin', 'Días que pasan desde que el usuario ha hecho el pedido hasta que se cargan los puntos en su cuenta'),
                'type' => 'select',
                'options' => [1 => 1, 2 => 2, 3 => 3, 4 => 4, 5 => 5],
                'show' => 'content.settings.store_has_points == true',
            ],
            'settings.store_invoice_prefix' => [
                'label' => __d('admin', 'Prefijo para las facturas'),
                'help' => __d('admin', 'Prefijo que se adjuntará a un número para las facturas'),
                'type' => 'string',
            ],
            'settings.store_invoice_zeros' => [
                'label' => __d('admin', 'Número de cifras para el número de factura'),
                'help' => __d('admin', 'Número de cifras para el número de factura'),
                'type' => 'select',
                'options' => [1 => 1, 2 => 2, 3 => 3, 4 => 4, 5 => 5, 6 => 6, 7 => 7, 8 => 8, 9 => 9],
            ],
            'settings.store_invoice_first' => [
                'label' => __d('admin', 'Primer número'),
                'help' => __d('admin', 'Si no hay ninguna factura añadida para el prefijo señalado, éste será el primer número'),
                'type' => 'numeric',
            ],
            'settings.store_invoice_refund_prefix' => [
                'label' => __d('admin', 'Prefijo para las facturas de devolución'),
                'help' => __d('admin', 'Prefijo que se adjuntará a un número para las facturas de devolución'),
                'type' => 'string',
            ],
            'settings.store_invoice_refund_zeros' => [
                'label' => __d('admin', 'Número de cifras para el número de factura de devolución'),
                'help' => __d('admin', 'Número de cifras para el número de factura de devolución'),
                'type' => 'select',
                'options' => [1 => 1, 2 => 2, 3 => 3, 4 => 4, 5 => 5, 6 => 6, 7 => 7, 8 => 8, 9 => 9],
            ],
            'settings.store_invoice_refund_first' => [
                'label' => __d('admin', 'Primer número de la factura de devolución'),
                'help' => __d('admin', 'Si no hay ninguna factura añadida para el prefijo señalado, éste será el primer número'),
                'type' => 'numeric',
            ],
            'settings.store_invoice_pdf_name' => [
                'label' => __d('admin', 'Nombre del archivo de la factura a descargar'),
                'help' => __d('admin', 'Nombre que saldrá en el fichero que se descarga de la factura'),
                'type' => 'string',
            ],
            'settings.store_receipt_prefix' => [
                'label' => __d('admin', 'Prefijo para los recibos'),
                'help' => __d('admin', 'Prefijo que se adjuntará a un número para los recibos'),
                'type' => 'string',
            ],
            'settings.store_receipt_zeros' => [
                'label' => __d('admin', 'Número de cifras para el número de recibo'),
                'help' => __d('admin', 'Número de cifras para el número de recibo'),
                'type' => 'select',
                'options' => [1 => 1, 2 => 2, 3 => 3, 4 => 4, 5 => 5, 6 => 6, 7 => 7, 8 => 8, 9 => 9],
            ],
            'settings.store_receipt_first' => [
                'label' => __d('admin', 'Primer número'),
                'help' => __d('admin', 'Si no hay ningún recibo añadido para el prefijo señalado, éste será el primer número'),
                'type' => 'numeric',
            ],
            'settings.store_gift_wrapping_price' => [
                'label' => __d('admin', 'Precio de envoltura de regalo'),
                'help' => __d('admin', 'Precio que se cobrará a los clientes por la envoltura de regalo'),
                'type' => 'numeric',
            ],
            'settings.store_gift_wrapping_tax_id' => [
                'label' => __d('admin', 'Impuesto para la envoltura de regalo'),
                'type' => 'select',
                'options' => function ($crud) {
                    return TableRegistry::get('Store.Taxes')->find('list');
                }
            ],
            'settings.store_mandatory_fields_firstname' => [
                'label' => __d('admin', 'Nombre'),
                'type' => 'boolean',
            ],
            'settings.store_mandatory_fields_lastname' => [
                'label' => __d('admin', 'Apellidos'),
                'type' => 'boolean',
            ],
            'settings.store_mandatory_fields_address' => [
                'label' => __d('admin', 'Calle / Plaza'),
                'type' => 'boolean',
            ],
            'settings.store_mandatory_fields_phone' => [
                'label' => __d('admin', 'Teléfono'),
                'type' => 'boolean',
            ],
            'settings.store_mandatory_fields_postcode' => [
                'label' => __d('admin', 'Código postal'),
                'type' => 'boolean',
            ],
            'settings.store_mandatory_fields_vat_number' => [
                'label' => __d('admin', 'CIF'),
                'type' => 'boolean',
            ],
            'settings.store_mandatory_fields_email_confirmation' => [
                'label' => __d('admin', 'Confirmación de email'),
                'type' => 'boolean',
            ],
            'settings.store_invoice_when_status' => [
                'label' => __d('admin', 'La factura o recibo se crea cuando el estado cambia a'),
                'type' => 'select',
                'options' => [
                    'accepted' => __d('admin', 'Pago aceptado'),
                    'envoy' => __d('admin', 'Enviado'),
                ]
            ],
            'settings.store_order_email' => [
                'label' => __d('admin', 'Email para pedidos'),
                'help' => __d('admin', 'Dirección de email a la que se enviará una notificación cuando se hagan nuevos pedidos. Se pueden poner varios separadados por comas'),
                'type' => 'string',
                'options' => [
                    'accepted' => __d('admin', 'Pago aceptado'),
                    'envoy' => __d('admin', 'Enviado'),
                ]
            ],
            'settings.store_order_email_replay' => [
                'label' => __d('admin', 'Email de respuesta para pedidos'),
                'help' => __d('admin', 'Dirección de email a la que podrá responder el usuario cuando recibe un pedido o una notificación de cambio de pedido. Se pueden poner varios separadados por comas'),
                'type' => 'string',
                'options' => [
                    'accepted' => __d('admin', 'Pago aceptado'),
                    'envoy' => __d('admin', 'Enviado'),
                ]
            ],
            'settings.store_giftcard_shipping' => [
                'label' => __d('admin', 'Coste de envío de la tarjeta regalo'),
                'help' => __d('admin', 'Coste de envío para la tarjeta regalo enviada en un sobre por correo'),
                'type' => 'string',
            ],
            'settings.store_days_limit_refund' => [
                'label' => __d('admin', 'Máximo de días para poder devolver un producto'),
                'help' => __d('admin', 'Máximo de días para poder devolver un producto'),
                'type' => 'string',
            ],
        ]);

        if (Plugin::isLoaded('Mailchimp')) {
            $table->crud->addFields([
                'settings.store_mailchimp' => [
                    'type' => 'boolean',
                    'label' => __d('admin', 'Guardar usuarios en Mailchimp'),
                ],
                'settings.store_mailchimp_list' => [
                    'type' => 'select',
                    'options' => function ($crud) {
                        try {
                            $mc = new \DrewM\MailChimp\MailChimp(\Website\Lib\Website::get('settings.mailchimp'));
                            $lists = $mc->get('lists');
                            $options = array('-- Ninguna --');
                            $_options = collection($lists['lists'])->combine('id', 'name')->toArray();
                            return array_merge($options, $_options);
                        } catch (\Throwable $th) {
                            return [];
                        }
                    },
                    'change' => 'scope.send( scope.data.crudConfig.view.submit, scope.data.content, false)',
                    'label' => __d('admin', 'Lista de Mailchimp para los usuarios registrados'),
                    'show' => 'content.settings.store_mailchimp && content.settings.mailchimp'
                ],
            ]);

            $mc_fields = [
                'settings.store_mc_firstname' => __d('admin', 'Campo para nombre'),
                'settings.store_mc_lastname' => __d('admin', 'Campo para apellido'),
                'settings.store_mc_vat_number' => __d('admin', 'Campo para CIF'),
                'settings.store_mc_address' => __d('admin', 'Campo para dirección'),
                'settings.store_mc_city' => __d('admin', 'Campo para población'),
                'settings.store_mc_postcode' => __d('admin', 'Campo para código postal'),
                'settings.store_mc_country' => __d('admin', 'Campo para país'),
                'settings.store_mc_state' => __d('admin', 'Campo para provincia'),
                'settings.store_mc_phone' => __d('admin', 'Campo para teléfono'),
            ];

            foreach ($mc_fields as $key => $label) {
                $table->crud->addFields([
                    $key => [
                        'label' => $label,
                        'type' => 'select',
                        'options' => function ($crud) {
                            try {
                                $content = $crud->getContent();
                                if (empty($content['settings']['store_mailchimp_list'])) {
                                    return [];
                                }

                                $merge_fields = \Mailchimp\Store\MailchimpStore::getMergeFields($content['settings']['store_mailchimp_list']);
                                $fields = [
                                    __d('app', '-- Ninguno --'),
                                ];
                                return array_merge($fields, $merge_fields);
                            } catch (\Throwable $th) {
                                return [];
                            }
                        },
                        'show' => 'data.content.settings.store_mailchimp && content.settings.store_mailchimp_list && content.settings.mailchimp',
                    ],
                ]);
            }
        }

        $elements = [
            [
                'title' => __d('admin', 'Datos de facturación'),
                'elements' => [
                    'settings.store_name',
                    'settings.store_cif',
                    'settings.store_address',
                    'settings.store_postcode',
                    'settings.store_city',
                    'settings.store_state',
                    'settings.store_country',
                ]
            ],
            [
                'title' => __d('admin', 'Facturas y recibos'),
                'elements' => [
                    'settings.store_invoice_when_status',
                    'settings.store_invoice_pdf_name',
                    'settings.store_invoice_prefix',
                    'settings.store_invoice_zeros',
                    'settings.store_invoice_first',
                    'settings.store_invoice_refund_prefix',
                    'settings.store_invoice_refund_zeros',
                    'settings.store_invoice_refund_first',
                ]
            ],

            [
                'title' => __d('admin', 'Notificaciones'),
                'elements' => [
                    'settings.store_order_email',
                    'settings.store_order_email_replay',
                ]
            ],
            [
                'title' => __d('admin', 'Campos obligatorios en el pedido'),
                'elements' => [
                    'settings.store_mandatory_fields_firstname',
                    'settings.store_mandatory_fields_lastname',
                    'settings.store_mandatory_fields_address',
                    'settings.store_mandatory_fields_phone',
                    'settings.store_mandatory_fields_postcode',
                    'settings.store_mandatory_fields_vat_number',
                    'settings.store_mandatory_fields_email_confirmation',
                ]
            ],
            [
                'title' => __d('admin', 'Otros'),
                'elements' => [
                    'settings.store_price_with_taxes',
                    'settings.store_gift_wrapping_price',
                    'settings.store_gift_wrapping_tax_id',
                    'settings.store_show_delivery_days',
                    'settings.store_days_limit_refund',
                ]
            ],
        ];

        if (StoreConfig::getConfig('withPoints')) {
            $elements[] = [
                'title' => __d('admin', 'Puntos de usuario'),
                'elements' => [
                    'settings.store_has_points',
                    'settings.store_points_down_value',
                    'settings.store_points_up_value',
                    'settings.store_points_charge_days',
                ]
            ];
        }

        if (StoreConfig::getConfig('giftCard')) {
            $elements[] = [
                'title' => __d('admin', 'Puntos de usuario'),
                'elements' => [
                    'settings.store_giftcard_shipping',
                ]
            ];
        }

        if (Plugin::isLoaded('Mailchimp')) {
            $mc_elements = [
                'settings.store_mailchimp',
                'settings.store_mailchimp_list',
            ];

            foreach ($mc_fields as $key => $label) {
                $mc_elements[] = $key;
            }


            $elements[] = [
                'title' => __d('admin', 'Mailchimp'),
                'elements' => $mc_elements
            ];
        }

        $view->addColumn('update', [
            'title' => __d('admin', 'Tienda'),
            'box' => $elements
        ]);
    }


    public function initializeUsers($event, $table)
    {
        $table->belongsToMany('PaymentMethods', [
            'className' => 'Store.PaymentMethods',
            'foreignKey' => 'user_id',
            'targetForeignKey' => 'payment_method_id',
            'joinTable' => 'store_users_payment_methods',
        ]);

        $table->hasOne('AddressDelivery', [
            'className' => 'Store.Addresses',
            'foreignKey' => 'user_id',
            'conditions' => [
                'AddressDelivery.type' => 'delivery'
            ]
        ]);

        $table->hasOne('AddressInvoice', [
            'className' => 'Store.Addresses',
            'foreignKey' => 'user_id',
            'conditions' => [
                'AddressInvoice.type' => 'invoice'
            ]
        ]);
    }

    public function afterSetSubtotal($event, $order)
    {
        $OrdersTable = $event->subject();

        if (empty($order->points_down)) {
            return;
        }

        $discount = $order->points_down->discount;

        if (+ ($discount) > ($order->line_items_totals)) {
            $diff = $discount - ($order->line_items_totals);
            $change_value = Website::get('settings.store_points_down_value');
            $new_points = $order->points_down->points - round($diff / Website::get('settings.store_points_down_value'));
            $order->points_down = $OrdersTable->PointsDown->usePoints($new_points, $order->user_id, $order, $order->points_down->id);
            $discount = $order->points_down->discount;
        }

        $order->points_discount_tax_rate = 21;
        $order->points_discount_tax = $discount - ($discount / (1 + ($order->points_discount_tax_rate / 100)));
        $order->points_discount = $discount - $order->points_discount_tax;
        $order->points_discount_tax = - ($order->points_discount_tax);
        $order->points_discount = - ($order->points_discount);
    }

    public function beforeLogin($event)
    {
        $controller = $event->subject();

        if ($controller->request->query('cart')) {
            $controller->request->session()->write('Auth.redirectUrl', $controller->Section->url([
                'plugin' => 'Store',
                'controller' => 'Orders',
                'action' => 'express'
            ]));
        }
    }

    /**
     * Añade al usuario que se va a loguear el contain para PaymentMethods
     * Para que se sepa qué métodos de pago tiene el usuario adjudicados, en el caso que los tenga
     *
     * @param Event $event
     * @param Query $query
     * @return void
     */
    public function findAuth($event, Query $query)
    {
        $query->contain('PaymentMethods');
    }
}
