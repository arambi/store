<?php

namespace Store\Event;

use Browser;
use Cake\Log\Log;
use Cake\Routing\Router;
use Cake\ORM\TableRegistry;
use Store\Config\StoreConfig;
use Mailchimp\Queue\MailchimpQueue;
use Cake\Event\EventListenerInterface;

class MonitorListener implements EventListenerInterface
{

    public function implementedEvents()
    {
        return [
            'Store.Controller.Orders.afterAddItem' => 'afterAddItem',
            'Store.Controller.Orders.afterQuantityItem' => 'afterAddItem',
            'Store.Controller.Orders.afterDeleteItem' => 'afterDeleteItem',
            'Store.Controller.Orders.beforeUpdate' => 'beforeUpdateSubmit',
            'Store.Controller.Orders.orderSuccess' => 'orderSuccess',
        ];
    }

    private function getSessionId($event)
    {
        $controller = $event->getSubject();
        $request = $controller->getRequest();
        $session_id = $request->getSession()->id();
        $OrderSessions = TableRegistry::get('Store.OrderUserSessions');

        $session = $OrderSessions->find()
            ->where([
                'session_id' => $session_id
            ])
            ->first();

        if (!$session) {
            $data = $this->getSessionData( $event);
            $data ['session_id'] = $session_id;
            $session = $OrderSessions->newEntity( $data);
            $OrderSessions->save( $session);
        }

        return $session->id;
    }

    private function getSessionData($event)
    {
        $controller = $event->getSubject();
        $request = $controller->getRequest();
        $browser = new Browser();

        $data = [
            'user_id' => $controller->Auth->user('id'),
            'ip' => $request->clientIp(),
            'browser' => $browser->getBrowser() . ' ' . $browser->getVersion(),
            'platform' => $browser->getPlatform(),
        ];

        return $data;
    }

    private function getData($event)
    {
        $controller = $event->getSubject();
        $request = $controller->getRequest();

        $data = [
            'session_id' => $this->getSessionId($event),
            'post' => $request->getData(),
            'url' => env('REQUEST_URI'),
            'action' => strtolower($request->controller) . '_' . strtolower($request->action),
        ];

        return $data;
    }

    private function saveData($data)
    {
        $OrderActions = TableRegistry::get('Store.OrderUserActions');
        $entity = $OrderActions->newEntity($data);
        return $OrderActions->save($entity);
    }

    public function afterAddItem($event, $result)
    {
        $data = $this->getData($event);
        $response = [];

        if (!empty($result['item'])) {
            $response['title'] = $result['item']['title'];
            $response['product_id'] = $result['item']['product_id'];
            $response['quantity'] = $result['item']['quantity'];
            $response['order_id'] = $result['item']['order_id'];
        }

        $data['response'] = $response;
        $this->saveData($data);
    }

    public function afterDeleteItem($event, $result)
    {
        $data = $this->getData($event);
        $response = [];
        
        if ($result['item']) {
            $response['title'] = $result['item']['title'];
            $response['product_id'] = $result['item']['product_id'];
            $response['quantity'] = $result['item']['quantity'];
            $response['order_id'] = $result['item']['order_id'];
        }

        $data['response'] = $response;
        $this->saveData($data);
    }

    public function beforeUpdateSubmit($event, $order)
    {
        $controller = $event->getSubject();

        if (!($controller instanceof \Cake\Controller\Controller)) {
            return;
        }
        
        $data = $this->getData($event);
        $cart = (array)$order->for_cart;
        $response = $cart;

        if (!empty( $cart ['errors'])) {
            $data['errors'] = $cart ['errors'];
        }
        
        $response ['order_id'] = $order->id;
        $data['response'] = $response;
        $this->saveData($data);
    }

    public function orderSuccess($event, $order)
    {
        $data = $this->getData($event);
        $response = [
            'order_id' => $order->id
        ];

        $data['response'] = $response;
        $this->saveData($data);
    }
}
