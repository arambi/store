<?php

namespace Store\Mrw;

use Migrations\Table;
use Cake\Core\Configure;
use Store\Mrw\MrwClient;
use Website\Lib\Website;
use klareNNNs\MRW\Client;
use Cake\ORM\TableRegistry;
use klareNNNs\MRW\Entity\AuthHeader;
use klareNNNs\MRW\Entity\ServiceData;
use klareNNNs\MRW\Entity\ShippingUser;
use klareNNNs\MRW\Entity\ShippingAddress;

class Mrw
{
    private $order;

    private $MrwDeliveries;

    private $dirFiles = WWW_ROOT . 'files/mrw/';

    private $reverseAddress = false;

    private $delivery;

    public function __construct()
    {
        $this->MrwDeliveries = TableRegistry::getTableLocator()->get('Store.MrwDeliveries');
    }

    public function setReverseAddress()
    {
        $this->reverseAddress = true;
    }

    public function getConfig($key)
    {
        return Configure::read("MrwData.$key");
    }

    public function getHook($key, $default)
    {
        $func = Configure::read("MrwHooks.$key");

        if ($func) {
            return $func($this->order);
        }

        return $default;
    }

    public function getClient()
    {
        $soap = new \SoapClient($this->getConfig('urlSend'), array('trace' => 1, "exceptions" => 0));
        $auth = new AuthHeader(
            $this->getConfig('franchise'),
            $this->getConfig('subscriber'),
            $this->getConfig('department'),
            $this->getConfig('user'),
            $this->getConfig('password')
        );

        return new MrwClient($soap, $auth);
    }

    public function send($order, $delivery)
    {
        $this->delivery = $delivery;
        $this->order = $order;
        $apiClient = $this->getClient();

        if (!$this->reverseAddress) {
            $delivery = $apiClient->createTransaction(
                $this->getServiceData(),
                $this->getShippingAddress(),
                $this->getShippingUser(),
                $this->getPickupAddress(),
                $this->getPickupUser(),
            );
        } else {
            $delivery = $apiClient->createTransaction(
                $this->getServiceData(),
                $this->getPickupAddress(),
                $this->getPickupUser(),
                $this->getShippingAddress(),
                $this->getShippingUser(),
            );
        }
        
        $this->saveResponse($delivery);
        return $delivery;
    }

    public function saveResponse($delivery)
    {
        $entity = $this->delivery;
        $entity->set([
            'request_number' => $delivery->getRequestNumber(),
            'shipping_number' => $delivery->getShippingNumber(),
            'state' => $delivery->getState(),
            'message' => $delivery->getMessage(),
            'proccesed' => true
        ]);

        $filename = $this->getTicketFile($entity);

        if (!empty($filename)) {
            $entity->set('ticket_file', $filename);
            $this->MrwDeliveries->save($entity);
        }
    }

    public function getTicketFile($mrw_delivery)
    {
        if ($mrw_delivery->state == '0') {
            return;
        }

        $apiClient = $this->getClient();
        $ticket = $apiClient->getTicketFile($mrw_delivery->shipping_number);
        $filename = $mrw_delivery->shipping_number . '.pdf';
        file_put_contents($this->dirFiles . $filename, $ticket->GetEtiquetaEnvioResult->EtiquetaFile);
        return $filename;
    }

    public function getServiceData()
    {
        $dateTime = new \DateTime();
        $date = $dateTime->format('d/m/Y');
        $reference = '';
        $onFranchise = $this->getHook('onFranchise', 'N');
        $serviceCode = $this->getHook('serviceCode', '0800');
        $serviceDescription = '';
        $items = '';
        $numberOfItems = '1';
        $weight = str_replace('.', ',', $this->order->weight() / 1000);
        $saturdayDelivery = $this->getHook('saturdayDelivery', 'N');
        $return = 'N';
        $refund = 'N';
        $refundAmount = '';
        $notificationsMail = $this->getHook('notificationsMail', Website::get('email'));
        $notificationsSMS = $this->getHook('notificationsSMS', $this->order->adr_delivery_phone);

        $serviceData = new ServiceData(
            $date,
            $reference,
            $onFranchise,
            $serviceCode,
            $serviceDescription,
            $items,
            $numberOfItems,
            $weight,
            $saturdayDelivery,
            $return,
            $refund,
            $refundAmount,
            $notificationsMail,
            $notificationsSMS
        );

        return $serviceData;
    }

    public function getPickupAddress()
    {
        return $this->getHook('pickupAddress', null);
    }

    public function getPickupUser()
    {
        return $this->getHook('pickupUser', null);
    }

    public function getShippingAddress()
    {
        $addressCode = '';
        $viaType = '';
        $via = $this->order->adr_delivery_address;
        $number = '';
        $other = '';
        $postalCode = $this->order->adr_delivery_postcode;
        $city = $this->order->adr_delivery_city;
        $countryCode = $this->order->country_delivery->iso_code;
        $countryCode = 'ESP';

        $shippingAddress = new ShippingAddress(
            $addressCode,
            $viaType,
            $via,
            $number,
            $other,
            $postalCode,
            $city,
            $countryCode
        );

        return $shippingAddress;
    }

    public function getShippingUser()
    {
        $nif = '';
        $name =  $this->order->adr_delivery_firstname . ' ' . $this->order->adr_delivery_lastname;
        $telephone = $this->order->adr_delivery_phone;
        $contact = $this->order->adr_delivery_firstname;
        $atentionTo = $this->order->adr_delivery_firstname;
        $observations = '';

        $shippingUser = new ShippingUser($nif, $name, $telephone, $contact, $atentionTo, $observations);
        return $shippingUser;
    }

    public function getStatus($order_id)
    {
        $url = 'https://trackingservice.mrw.es/TrackingService.svc?wsdl';
        Configure::write('MrwData.urlSend', $url);
        $apiClient = $this->getClient();
        $status = $apiClient->getStatus($order_id, $this->getConfig('user'), $this->getConfig('password'));
        return $status;
    }
}
