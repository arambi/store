<?php 

namespace Store\Mrw;

use klareNNNs\MRW\Client;
use klareNNNs\MRW\Entity\AuthHeader;
use klareNNNs\MRW\Services\SoapHeaderFactory;
use klareNNNs\MRW\Services\SoapTicketRequestFactory;
use SoapClient;

class MrwClient extends Client
{
    const STATUS_METHOD = 'GetEnvios';


    public function getStatus($orderId, $user, $pass)
    {
        $this->client->__setSoapHeaders([SoapHeaderFactory::create($this->authHeader)]);
        $request = $this->statusFactory($orderId, $user, $pass);
        $response = $this->client->__soapCall(self::STATUS_METHOD, $request);
        return $response;
    }

    public function statusFactory($orderId, $user, $pass)
    {
        return [
            'GetEnvios' => [
                'login' => $user,
                'pass' => $pass,
                'tipoFiltro' => '0',
                'tipoInformacion' => '0',
                'codigoIdioma' => 3082,
                'valorFiltroDesde' => $orderId,
                'valorFiltroHasta' => $orderId,
            ]
        ];
    }
}