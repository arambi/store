<?php
namespace Store\Shell;

use Cake\Console\Shell;
use Cake\ORM\TableRegistry;
use Cake\Core\Plugin;

/**
 * Dumper shell command.
 */
class DumperShell extends Shell
{

  /**
   * main() method.
   *
   * @return bool|int Success or error code.
   */
  public function main() 
  {
    $this->createTaxes();
    $this->createCountries();
    $this->createZones();
    $this->createCarriers();
  }

  private function __getData( $file)
  {
    $path = Plugin::path( 'Store') . 'src' .DS. 'Model' .DS. 'Data' .DS;
    $content = file_get_contents( $path . $file .'.json');
    return json_decode( $content, true);
  }

  public function createCurrencies()
  {
    $Currencies = TableRegistry::get( 'Store.Currencies');
    $data = $this->__getData( 'currencies');

    foreach( $data ['currencies'] as $record)
    {
      if( !$Currencies->exists(['iso_code' => $record ['iso_code']]))
      {
        $entity = $Currencies->getNewEntity( $record);
        $Currencies->save( $entity);
      }
    }
  }

  public function createCountries()
  {
    $Countries = TableRegistry::get( 'Store.Countries');
    $Countries->getFromRest();

    foreach( ['ES'] as $code)
    {
      $country = $Countries->find()
        ->where([
          'Countries.iso_code' => $code,
        ])
        ->order([
          'Countries.title'
        ])
        ->first();
        
      $country->set( 'active', true);
      $Countries->save( $country);
    }
  }

  public function createZones()
  {
    $Zones = TableRegistry::get( 'Store.Zones');
    $data = $this->__getData( 'zones');

    foreach( $data ['zones'] as $record)
    {
      $entity = $Zones->newEntity( $record);
      $Zones->save( $entity);
    }
  }


  public function createTaxes()
  {
    $Taxes = TableRegistry::get( 'Store.Taxes');
    $data = $this->__getData( 'taxes');

    foreach( $data ['taxes'] as $record)
    {
      $entity = $Taxes->newEntity( $record);
      $Taxes->save( $entity);
    }
  }

  public function createCarriers()
  {
    $Carriers = TableRegistry::get( 'Store.Carriers');
    $data = $this->__getData( 'carriers');

    foreach( $data ['carriers'] as $record)
    {
      $entity = $Carriers->newEntity( $record);
      $Carriers->save( $entity);
    }
  }
}
