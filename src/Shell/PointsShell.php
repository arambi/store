<?php
namespace Store\Shell;

use Cake\Console\Shell;
use Cake\ORM\TableRegistry;
use Website\Lib\Website;

/**
 * Points shell command.
 */
class PointsShell extends Shell
{

/**
 * Crea los puntos para un usuario después de haber pasado los días indicados en settings.store_points_charge_days.
 *
 * @example  bin/cake points add
 */
  public function add()
  {
    $this->Sites = TableRegistry::get( 'Website.Sites');
    $this->Orders = TableRegistry::get( 'Store.Orders');
    $this->Points = TableRegistry::get( 'Store.Points');

    $this->Sites->setSite();
    $this->Orders->hasOne( 'Points', [
      'className' => 'Store.Points',
      'foreignKey' => 'order_id',
      'conditions' => [
        'Points.action' => 'up'
      ]
    ]);

    $days = Website::get( 'settings.store_points_charge_days');

    $orders = $this->Orders->findCompleted()
      ->where([
        // 'DATE(Orders.order_date) = DATE_SUB(CURDATE(), INTERVAL '. $days .' DAY)',
        'Points.id IS NULL'
      ])
      ->contain([
        'Points'
      ])
      ->all();

    foreach( $orders as $order)
    {
      $this->Points->addPointsFromOrder( $order);
    }
  }
}
