<?php

namespace Store\Shell;

use Cake\Console\Shell;
use Cake\Http\Client;
use Store\Config\StoreConfig;

/**
 * CorreosTrack shell command.
 */
class CorreosTrackShell extends Shell
{
    private $url = 'https://api1.correos.es/digital-services/searchengines/api/v1/';

    private $orderStatusMap = [
        'Admitido' => 'envoy',
        'Entregado' => 'received',
    ];

    private $itemStatusMap = [
        'Admitido' => StoreConfig::ITEM_STATUS_REFUNDED_SEND,
        'Entregado' => StoreConfig::ITEM_STATUS_REFUNDED_RECEIVED,
        'En reparto' => StoreConfig::ITEM_STATUS_REFUNDED_RECEIVED,
    ];

    public function getOptionParser()
    {
        $parser = parent::getOptionParser();
        return $parser;
    }

    public function orders()
    {
        $table = $this->getTableLocator()->get('Store.Orders');

        $orders = $table->find()
            ->where([
                'Orders.correos_track_number IS NOT NULL',
                'Orders.status IN' => ['envoy'],
                'Orders.modified >' => date('Y-m-d', strtotime( '-15 day')) 
            ]);

        $client = new Client();
        foreach ($orders as $order) {
            $response = $client->get("{$this->url}?text={$order->correos_track_number}&language=ES&searchType=envio");
            $data = $response->getJson();
            if (!isset($data['shipment'][0]['events'])) {
                continue;
            }

            $last_event = end($data['shipment'][0]['events']);
            if (array_key_exists($last_event['summaryText'], $this->orderStatusMap)) {
                $table->query()->update()
                    ->where([
                        'id' => $order->id,
                    ])
                    ->set([
                        'correos_track_data' => $data
                    ])
                    ->execute();

                $table->addStatus($order->salt, $this->orderStatusMap[$last_event['summaryText']]);
            }
        }
    }


    public function items()
    {
        $table = $this->getTableLocator()->get('Store.LineItems');

        $items = $table->find()
            ->where([
                'LineItems.correos_track_number IS NOT NULL',
                'LineItems.status IN' => [StoreConfig::ITEM_STATUS_REFUNDED_REQUEST, StoreConfig::ITEM_STATUS_REFUNDED_WORKING, StoreConfig::ITEM_STATUS_REFUNDED_SEND]
            ]);

        $client = new Client();

        foreach ($items as $item) {
            $response = $client->get("{$this->url}?text={$item->correos_track_number}&language=ES&searchType=envio");
            $data = $response->getJson();

            if (!isset($data['shipment'][0]['events'])) {
                continue;
            }

            $last_event = end($data['shipment'][0]['events']);

            if (array_key_exists($last_event['summaryText'], $this->itemStatusMap)) {
                // $table->query()->update()
                //     ->where([
                //         'id' => $item->id,
                //     ])
                //     ->set([
                //         'correos_track_data' => $data
                //     ])
                //     ->execute();

                $table->addStatus($item->id, $this->itemStatusMap[$last_event['summaryText']]);
            }
        }
    }
}
