<?php

namespace Store\Shell;

use I18n\Lib\Lang;
use Cake\Core\Plugin;
use Cake\Console\Shell;

/**
 * Countries shell command.
 */
class CountriesShell extends Shell
{
    /**
     * Manage the available sub-commands along with their arguments and help
     *
     * @see http://book.cakephp.org/3.0/en/console-and-shells.html#configuring-options-and-generating-help
     *
     * @return \Cake\Console\ConsoleOptionParser
     */
    public function getOptionParser()
    {
        $parser = parent::getOptionParser();

        return $parser;
    }

    /**
     * main() method.
     *
     * @return bool|int|null Success or error code.
     */
    public function main()
    {
        $this->out($this->OptionParser->help());
    }

    public function generate()
    {
        $Countries = $this->getTableLocator()->get('Store.Countries');
        $langs = Lang::get();
        $path = Plugin::path('Store') . 'src' . DS . 'Model' . DS . 'Data' . DS . 'countries.csv';
        $csv = file_get_contents($path);
        $states = [];
        $rows = explode("\n", $csv);

        foreach ($rows as $row) {
            $cols = explode(",", $row);
            $cols = array_map(function ($value) {
                return str_replace('"', '', $value);
            }, $cols);

            if (!isset($cols[1])) {
                continue;
            }

            $title = $cols[1];
            $code = $cols[3];

            $country = $Countries->find()
                ->where([
                    'Countries.iso_code' => $code
                ])
                ->first();

            if ($country) {
                continue;
            }

            $data = [
                'title' => $title,
                'iso_code' => $code,
                'active' => 0
            ];

            foreach ($langs as $lang) {
                foreach ($cols as $col) {
                    if (substr($col, 0, 3) == $lang->iso2 . ':') {
                        $data['_translations'][$lang->iso3]['title'] = str_replace($lang->iso2 . ':', '', $col);
                    }
                }

                if (empty($data['_translations'][$lang->iso3]['title'])) {
                    $data['_translations'][$lang->iso3]['title'] = $title;
                }
            }

            $country = $Countries->newEntity($data);
            $Countries->save($country);
            $Countries->saveStates($country);
        }
    }

    public function generateStates()
    {
        $Countries = $this->getTableLocator()->get('Store.Countries');
        $iso_code = $this->in('Indica el código del país dos letras');
        $iso_code = strtoupper($iso_code);

        $country = $Countries->find()
            ->where([
                'iso_code' => $iso_code
            ])
            ->first();

        if (!$country) {
            $this->abort('No existe el país');
        }

        $Countries->saveStates($country);
    }
}
