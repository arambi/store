<?php

namespace Store\Shell;

use Cake\Console\Shell;
use Cake\ORM\TableRegistry;
use Store\Config\StoreConfig;

/**
 * RestoreProductsCount shell command.
 */
class RestoreProductsCountShell extends Shell
{
    public function getOptionParser()
    {
        $parser = parent::getOptionParser();

        return $parser;
    }

    public function main()
    {
        $this->updatePrice();
        $this->updateSales();
    }

    public function updatePrice()
    {
        $ProductsTable = StoreConfig::getProductsModel();
        
        foreach ($ProductsTable->find() as $entity) {
            if (!empty($entity->store_price_offer)) {
                $price = $entity->store_price_offer;
            } else {
                $price = $entity->store_price;
            }

            $ProductsTable->query()->update()
                ->set([
                    'store_price_order' => $price
                ])
                ->where([
                    'id' => $entity->id
                ])
                ->execute();
        }
    }

    public function updateSales()
    {
        $OrdersTable = TableRegistry::getTableLocator()->get('Store.Orders');

        foreach ($OrdersTable->find('completed')->contain('LineItems') as $order) {
            $OrdersTable->updateProductSales($order);
        }
    }

    public function updatePVP()
    {
        $ProductsTable = StoreConfig::getProductsModel();
        
        foreach ($ProductsTable->find()->contain('Taxes') as $entity) {
            $ProductsTable->setPricePVP($entity);
            $ProductsTable->query()->update()
                ->set([
                    'store_price_pvp' => $entity->store_price_pvp
                ])
                ->where([
                    'id' => $entity->id
                ])
                ->execute();
        }
    }

    public function updateStock()
    {
        $ProductsTable = StoreConfig::getProductsModel();
        
        foreach ($ProductsTable->find() as $entity) {
            $ProductsTable->updateStock($entity->id);
        }
    }

    public function updatePriceOfferGlobal()
    {
        $ProductsTable = StoreConfig::getProductsModel();
        
        foreach ($ProductsTable->find() as $entity) {
            $ProductsTable->updatePriceOfferGlobal($entity->id);
        }
    }
}
