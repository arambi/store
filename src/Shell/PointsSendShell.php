<?php
namespace Store\Shell;

use Cake\Console\Shell;
use Cake\ORM\TableRegistry;
use Section\Routing\SectionsRoutes;
use Cake\Collection\Collection;

/**
 * PointsSend shell command.
 */
class PointsSendShell extends Shell
{

  /**
   * main() method.
   *
   * @example bin/cake points_send send
   */
  public function send() 
  {
    $SectionsTable = TableRegistry::get( 'Section.Sections');
    $Sites = TableRegistry::get( 'Website.Sites');
    $Sites->setSite();

    $sections = $SectionsTable->find( 'threaded')->where( ['Sections.published' => 1])->order( 'position')->toArray();
    SectionsRoutes::nestedRoutes( $sections);

    $this->loadModel( 'Store.PointsAdds');
    $this->loadModel( 'Store.PointsAddsSends');

    $actions = $this->PointsAdds->findActives();
    
    foreach( $actions as $action)
    {
      $users = $this->PointsAddsSends->findUsers( $action);
      $this->PointsAddsSends->add( $users, $action->id);
    }

    $sends = $this->PointsAddsSends->find()
      ->where([
        'PointsAddsSends.send_on IS NULL',
        'PointsAdds.active' => true,
        'PointsAdds.send_on <' => date( 'Y-m-d'),
        'PointsAdds.finish_on >' => date( 'Y-m-d'),
      ])
      ->contain([
        'Users',
        'PointsAdds'
      ])
      ->limit( 20);

    foreach( $sends as $send)
    {
      $this->PointsAddsSends->send( $send);
      $this->out( 'Enviado a '. $send->email .' (Envío '. $send->points_add->id . ' - a usuario ' . $send->user_id .')');
      sleep( 5);
    }
  }
}
