<?php

namespace Store\Shell;

use Cake\Console\Shell;
use Cake\Routing\Router;
use Store\Config\StoreConfig;
use Mailchimp\Traits\MailchimpTrait;

class MailchimpCommerceShell extends Shell
{
    use MailchimpTrait;

    const STORE_ID = 'store';

    public function main()
    {
    }

    public function addStore()
    {
        $list_id = $this->in('Introduce el ID de lista');

        if (empty($list_id)) {
            $this->error('Es necesario introducir un id de lista');
        }

        $name = $this->in('Introduce el nombre de la tienda');

        if (empty($name)) {
            $this->error('Es necesario introducir el nombre de la tienda');
        }

        $id = $this->in('Introduce el id de la tienda', null, 'store');

        if (empty($id)) {
            $this->error('Es necesario introducir el id de la tienda');
        }

        try {
            $response = $this->mc()->post('/ecommerce/stores', [
                'id' => $id,
                'list_id' => $list_id,
                'name' => $name,
                'currency_code' => 'EUR',
            ]);

            if (array_key_exists('status', $response)) {
                print_r($response);
            }
        } catch (\Throwable $th) {
            print_r($th);
        }
    }

    public function deleteStore()
    {
        $response = $this->mc()->get('/ecommerce/stores');

        if (empty($response['stores'])) {
            $this->error('No hay ninguna tienda');
        }

        $stores = [];

        foreach ($response['stores'] as $key => $store) {
            $stores[$key + 1] = $store['id'];
            $this->out($key + 1 . '. ' . $store['name']);
        }

        $store_key = $this->in('Selecciona una tienda: ');

        if (!array_key_exists($store_key, $stores)) {
            $this->error('La tienda seleccionada no existe');
        }

        $response = $this->mc()->delete('/ecommerce/stores/' . $stores[$store_key]);

        _d($response);
    }

    public function getStoreId()
    {
        $response = $this->mc()->get('/ecommerce/stores');
        print_r($response);
    }

    public function addProducts()
    {
        $table = StoreConfig::getProductsModel();
        $query = $table->find('front')->contain('Taxes');

        $limit = 50;
        $offset = 0;

        while (true) {
            $_query = clone $query;
            $_query
                ->limit($limit)
                ->offset($offset)
                ->order($table->getAlias() . '.' . $table->getPrimaryKey());

            $products = $_query->all();

            foreach ($products as $product) {
                if (empty($product->store_price)) {
                    continue;
                }

                $data = [
                    'id' => (string)$product->id,
                    'title' => $product->title,
                    'url' => Router::url($product->link(), true),
                    'description' => $product->mc_description,
                    'type' => $product->mc_type,
                    'image_url' => Router::url($product->mc_image, true),
                    'variants' => [
                        [
                            'price' => $product->realPrice($product->price, $product->tax),
                            'id' => (string)$product->id,
                            'title' => $product->title,
                        ]
                    ]
                ];

                try {
                    $response = $this->mc()->post('/ecommerce/stores/' . self::STORE_ID . '/products', $data);

                    if (empty($response['status'])) {
                        $this->out('Añadido ' . $product->id);
                    } else {
                        // $this->out('ERROR ' . $product->id);
                        // print_r($response);
                        try {
                            $response = $this->mc()->patch('/ecommerce/stores/' . self::STORE_ID . '/products/' . (string)$product->id, $data);
    
                            if (empty($response['status'])) {
                                $this->out('Actualizado ' . $product->id);
                            } else {
                                $this->out('ERROR ' . $product->id);
                                print_r($response);
                            }
    
                        } catch (\Throwable $th) {
                            //throw $th;
                        }
                    }
                } catch (\Throwable $th) {
                    print_r($th);
                }

            }
            
            $offset += $limit;
    
            if ($products->count() < $limit) {
                break;
            }
        }
    }

    public function addUsers()
    {
        $users = $this->loadModel('User.Users')->find();

        $this->loadModel('Store.Addresses');

        foreach ($users as $user) {
            if (empty($user->email)) {
                continue;
            }

            $data = [
                'id' => (string)$user->id,
                'email_address' => $user->email,
                'opt_in_status' => true,
                'first_name' => $user->name,
            ];

            $address = $this->Addresses->find()
                ->where([
                    'type' => 'invoice',
                    'user_id' => $user->id
                ])
                ->first();

            if ($address) {
                $data['last_name'] = (string)$address->lastname;
                $data['address'] = [
                    'address1' => (string)$address->address,
                    'country' => (string)$this->getCountryName($address->country_id),
                    'province' => (string)$this->getStateName($address->state_id),
                    'city' =>  (string)$address->city,
                    'postal_code' => (string)$address->postcode,
                ];
            }

            try {
                $response = $this->mc()->post('/ecommerce/stores/' . self::STORE_ID . '/customers', $data);

                if (empty($response['status'])) {
                    $this->out('Añadido ' . $user->id);
                } else {
                    $this->out('ERROR ' . $user->id);
                    print_r($response);
                }
            } catch (\Throwable $th) {
                _d($th);
            }
        }
    }

    private function getCountryName($id)
    {
        $country = $this->loadModel('Store.Countries')
            ->find()
            ->where([
                'Countries.id' => $id
            ])
            ->first();

        if ($country) {
            return $country->title;
        }
    }

    private function getStateName($id)
    {
        if (empty($id)) {
            return '';
        }

        return $this->loadModel('Store.States')
            ->find()
            ->where([
                'States.id' => $id
            ])
            ->first()
            ->title;
    }


    public function getOrders()
    {
        $response = $this->mc()->get('/ecommerce/stores/' . self::STORE_ID . '/orders');
    }
}
