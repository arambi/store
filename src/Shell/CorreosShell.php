<?php

namespace Store\Shell;

use Cake\Console\Shell;
use Cake\Core\Configure;
use CorreosSdk\Factories\Address;
use CorreosSdk\Factories\Shipment;
use CorreosSdk\Factories\PackageSize;
use CorreosSdk\Factories\ProductList;
use CorreosSdk\Factories\Identification;
use CorreosSdk\Factories\SendingContent;
use CorreosSdk\Factories\SendingInsides;
use CorreosSdk\Factories\ProductDescription;
use CorreosSdk\CorreosConnector\CorreosConfig;
use CorreosSdk\Factories\SenderUnitedIdentity;
use CorreosSdk\Factories\ReceiverUnitedIdentity;
use CorreosSdk\CorreosConnector\CorreosConnector;

/**
 * Correos shell command.
 */
class CorreosShell extends Shell
{
    /**
     * Manage the available sub-commands along with their arguments and help
     *
     * @see http://book.cakephp.org/3.0/en/console-and-shells.html#configuring-options-and-generating-help
     *
     * @return \Cake\Console\ConsoleOptionParser
     */
    public function getOptionParser()
    {
        $parser = parent::getOptionParser();
        return $parser;
    }

    /**
     * main() method.
     *
     * @return bool|int|null Success or error code.
     */
    public function main()
    {
        $entities = $this->getEntities();

        foreach ($entities as $entity) {
            $this->send($entity);
        }
    }

    private function send($entity)
    {
        $connector = $this->getConnector($entity);
        $shipment = new Shipment($this->getReceiver($entity), $this->getSendingContent($entity));
        $createdInvoice = $connector->createShipment($shipment);
        $response = $createdInvoice->getResponse();
        $this->updateEntity($response, $entity);
        $this->writeTicket($connector, $response);
    }

    private function updateEntity($response, $entity)
    {
        $entity->set([
            'sended' => true,
            'sended_at' => date('Y-m-d H:i:s'),
            'fecha_respuesta' => $this->changeDate($response->FechaRespuesta),
            'cod_expedicion' => $response->CodExpedicion,
            'referencia_expedicion' => $response->ReferenciaExpedicion,
            'resultado' => $response->Resultado,
            'cod_envio' => $response->Bulto->CodEnvio,
            'cod_manifiesto' => $response->Bulto->CodManifiesto,
        ]);

        $this->getTableLocator()->get('Store.CorreosPreregistro')->save($entity);
        $this->getTableLocator()->get('Store.LineItems')->query()->update()
            ->where([
                'id' => $entity->line_item_id,
            ])
            ->set([
                'correos_track_number' => $response->Bulto->CodEnvio,
            ])
            ->execute();
    }

    private function changeDate($date)
    {
        [$day, $hour] = explode(' ', $date);
        preg_match('/([0-9]{1,2})\-([0-9]{1,2})\-([0-9]{2,4})/', $day, $match);
        $new = $match[3] . "-" . $match[2] . "-" . $match[1];
        return "$new $hour";
    }

    private function getEntities()
    {
        $entities = $this->getTableLocator()->get('Store.CorreosPreregistro')
            ->find()
            ->where([
                'sended' => false,
            ]);

        return $entities;
    }

    private function getConnector($entity)
    {
        $senderAddress = new Address(
            $entity->sender_city_name,
            $entity->sender_street_name,
            $entity->sender_province_name,
            $entity->sender_street_number
        );

        $senderIdentification = new Identification(
            $entity->sender_name
        );

        $senderUnitedIdentity = new SenderUnitedIdentity(
            $senderAddress,
            $senderIdentification,
            $entity->sender_postcode,
            $entity->sender_phone,
            $entity->sender_email,
            $entity->sender_zip,
            $entity->sender_country,
        );

        return new CorreosConnector($this->getCorreosConfig(), $senderUnitedIdentity);
    }

    private function getReceiver($entity)
    {
        $receiverAddress = new Address(
            $entity->receiver_city_name,
            $entity->receiver_street_name,
            $entity->receiver_province_name,
            $entity->receiver_street_number
        );

        $receiverIdentity = new Identification(
            $entity->receiver_name
        );

        return new ReceiverUnitedIdentity(
            $receiverAddress,
            $receiverIdentity,
            $entity->receiver_postcode,
            $entity->receiver_country,
            $entity->receiver_email,
            $entity->receiver_postcode,
            $entity->receiver_phone,
        );
    }

    private function getProductList($entity)
    {
        $item = $this->getLineItem($entity);
        $product = new ProductDescription(
            $item->quantity,
            $item->product_id,
            $item->weight,
            ($item->price + $item->taxes) * 100,
            null,
            'ES'
        );

        $productList = new ProductList();
        $productList->addProduct($product);
        return $productList;
    }

    private function getInsides($entity)
    {
        $productList = $this->getProductList($entity);
        return new SendingInsides(
            SendingInsides::GOODS_CONTENT_TYPE, // GOODS,
            SendingInsides::YES_CHOICE, // Y
            SendingInsides::YES_CHOICE,
            $productList,
            SendingInsides::NO_CHOICE
        );
    }

    private function getPackageSize($entity)
    {
        $item = $this->getLineItem($entity);

        return new PackageSize(
            15,
            15,
            15,
            $item->weight,
        );
    }

    private function getSendingContent($entity)
    {
        $sendingContent = new SendingContent(
            'S0148',
            // SendingContent::RETURN_DELIVERY_INSTRUCTION, // PAQ LIGHT INTERNATIONAL(I)
            SendingContent::POSTAGE_PAID_PAYMENT_TYPE,
            SendingContent::STANDARD_DELIVERY_MODE,
            $this->getPackageSize($entity),
            $this->getInsides($entity)
        );

        $sendingContent->setCustomerShipmentCode("Order {$entity->id}");
        return $sendingContent;
    }

    private function getCorreosConfig()
    {
        $config = Configure::read('Correos.credentials');

        return new CorreosConfig(
            $config['login'],
            $config['password'],
            $config['client_code'],
            Configure::read('Correos.env')
        );
    }

    public function getLineItem($entity)
    {
        return $this->getTableLocator()->get('Store.LineItems')
            ->find()
            ->contain('Products')
            ->where([
                'LineItems.id' => $entity->line_item_id,
            ])
            ->first();
    }

    private function writeTicket($connector, $response)
    {
        $trackNumber = $response->getBulto()->CodEnvio;
        $createdShipmentDateTime = $response->getFechaRespuesta();
        $labelPdfByteCode = $connector->printLabel($trackNumber, new \DateTime($createdShipmentDateTime));
        file_put_contents(WWW_ROOT . '/files/correos/' . $trackNumber .'.pdf', $labelPdfByteCode);
    }
}
