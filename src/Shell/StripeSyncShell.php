<?php

namespace Store\Shell;

use Cake\Console\Shell;
use Cake\Core\Configure;
use Cake\ORM\TableRegistry;

/**
 * StripeSync shell command.
 */
class StripeSyncShell extends Shell
{
    /**
     * Manage the available sub-commands along with their arguments and help
     *
     * @see http://book.cakephp.org/3.0/en/console-and-shells.html#configuring-options-and-generating-help
     *
     * @property \Store\Model\Table\OrdersTable $Orders
     * 
     * @return \Cake\Console\ConsoleOptionParser
     */
    public function getOptionParser()
    {
        $parser = parent::getOptionParser();

        return $parser;
    }

    public function getKeys()
    {
        $prefix = Configure::read('App.payEnvironment') == 'development' ? 'test_' : 'live_';

        $payment = $this->getTableLocator()->get('Store.PaymentMethods')->find()
            ->where([
                'PaymentMethods.processor' => 'stripe'
            ])
            ->first();


        if (!$payment || empty($payment->settings->{$prefix . 'public'}) || empty($payment->settings->{$prefix . 'secret'})) {
            throw new \RuntimeException(
                'No está configurado Stripe'
            );
        }

        return [
            'public' => $payment->settings->{$prefix . 'public'},
            'secret' => $payment->settings->{$prefix . 'secret'},
        ];
    }


    public function main()
    {
        $this->Orders = $this->getTableLocator()->get('Store.Orders');
        $charges = $this->getCharges();

        foreach ($charges as $charge) {
            if ($order = $this->getOrder($charge)) {
                $this->out("Finalizando pedido {$order->id}");
                $this->Orders->finalize($order, 'accepted');
            }
        }
    }

    private function getOrder($charge)
    {
        if ($charge->metadata->Pedido) {
            return $this->Orders
                ->find('order')
                ->where([
                    'Orders.id' => (int)$charge->metadata->Pedido,
                    'Orders.total' => ($charge->amount_captured / 100),
                    'Orders.status IS NULL'
                ])
                ->first();
        }
    }

    private function getCharges()
    {
        $stripe = new \Stripe\StripeClient(
            $this->getKeys()['secret']
        );

        $charges = $stripe->charges->all([
            'limit' => 20
        ]);
        
        return array_filter($charges->data, function($charge) {
            return $charge->status == 'succeeded';
        });
    }
}
