<?php
namespace Store\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;
use Store\Model\Entity\AttributeGroup;
use Cake\Core\Configure;

/**
 * AttributeGroups Model
 */
class AttributeGroupsTable extends Table
{
  /**
   * Initialize method
   *
   * @param array $config The configuration for the Table.
   * @return void
   */
  public function initialize( array $config)
  {
    $this->table( 'store_attribute_groups');
    $this->displayField( 'title');
    $this->primaryKey( 'id');
    
    // Associations
    $this->hasMany( 'Attributes', [
      'className' => 'Store.Attributes',
      'foreignKey' => 'attribute_group_id'
    ]);

    // Behaviors
    $this->addBehavior( 'Timestamp');
    $this->addBehavior( Configure::read( 'I18n.behavior'), [
      'fields' => ['title', 'store_title'],
      'translationTable' => 'Store.ShopI18n',
    ]);
    $this->addBehavior( 'Manager.Crudable');
    $this->addBehavior( 'Cofree.Saltable');


    // CRUD
    $this->crud->associations( ['Attributes']);
    $this->crud
      ->addFields([
        'title' => [
          'label' => __d( 'admin', 'Nombre'),
          'help' => __d( 'admin', 'Nombre del grupo de atributos como referencia en el administrador')
        ],
        'store_title' => [
          'label' => __d( 'admin', 'Nombre en la tienda'),
          'help' => __d( 'admin', 'Nombre del grupo de atributos tal y como se mostrará en la tienda')
        ],
        'cname' => [
          'label' => __d( 'admin', 'Clave interna'),
          'show' => 'data.administrator.superadmin'
        ],
        'group_type' => [
          'label' => __d( 'admin', 'Tipo de atributo'),
          'type' => 'select',
          'options' => [
            'select' => __d( 'admin', 'Lista desplegable'),
            'radio' => __d( 'admin', 'Botones de selección'),
            'color' => __d( 'admin', 'Color o textura'),
          ],
          'help' => __d( 'admin', 'La forma en que los valores de los atributos serán presentados a los clientes en la página de producto.')
        ],
        'has_product_photo' => [
          'label' => __d( 'admin', 'Producto con foto'),
          'type' => 'boolean',
          'help'=> __d( 'admin', 'Indica que el producto que contenga este atributo podrá tener una foto para este determinado atributo')
        ],
        'attributes' => [
          'label' => __d( 'admin', 'Valores'),
          'type' => 'hasMany'
        ]
      ])
      ->addIndex( 'index', [
        'fields' => [
          'title',
          'group_type',
        ],
        'actionButtons' => ['create'],
        'saveButton' => false,
      ])
      ->setName( [
        'singular' => __d( 'admin', 'Atributo de producto'),
        'plural' => __d( 'admin', 'Atributos de producto'),
      ])
      ->addView( 'create', [
        'columns' => [
          [
            'box' => [
              [
                'elements' => [
                  'title',
                  'store_title',
                  'cname',
                  'group_type',
                  'has_product_photo',
                  'attributes'
                ]
              ]
            ]
          ],
        ],
        'actionButtons' => ['index', 'create'],
      ], ['update'])
      ->order([
        'AttributeGroups.title'
      ])
      ;
  } 

/**
 * Devuelve los atributos junto con las opciones (AttributeGroups junto con los Attributes)
 *   
 * @return array
 */
  public function getOptions()
  {
    $contents = $this->find()
      ->contain( ['Attributes' => [
          'fields' => [
            'Attributes.id', 
            'Attributes.title', 
            'Attributes.attribute_group_id',
          ]
      ]])
      ->select( ['AttributeGroups.id', 'AttributeGroups.title'])
      ->toArray();

    return json_decode( json_encode( $contents), true);
  }

  // /**
  //  * Default validation rules.
  //  *
  //  * @param \Cake\Validation\Validator $validator Validator instance.
  //  * @return \Cake\Validation\Validator
  //  */
  // public function validationDefault(Validator $validator)
  // {
  //   $validator
  //     ->add('id', 'valid', ['rule' => 'numeric'])
  //     ->allowEmpty('id', 'create')
  //     ->requirePresence('title', 'create')
  //     ->notEmpty('title')
  //     ->requirePresence('group_type', 'create')
  //     ->notEmpty('group_type');

  //   return $validator;
  // }
}
