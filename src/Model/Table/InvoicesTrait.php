<?php

namespace Store\Model\Table;


/**
 * Métodos prácticos para facturas y recibos
 */

trait InvoicesTrait
{

    protected function _setCrud()
    {
        $this->crud->associations(['Orders', 'LineItems']);
        $this->crud
            ->addFields([
                'order_id' => __d('admin', 'Id de pedido'),
                'invoice_num' => [
                    'label' => 'Número',
                    'type' => 'info',
                ],
                'invoice_date' => [
                    'label' => 'Fecha',
                    'type' => 'string',
                ],
                'total_taxes_human' => [
                    'label' => 'Impuestos',
                    'type' => 'string',
                ],
                'total_human' => [
                    'label' => 'Total',
                    'type' => 'string',
                ],
                'name' => [
                    'label' => 'Nombre',
                    'type' => 'string',
                ],
                'invoice_url' => [
                    'label' => '',
                    'type' => 'info',
                    'templateIndex' => 'Store.indexes/invoice_pdf'
                ],
            ])
            ->addIndex('index', [
                'fields' => [
                    'order_id',
                    'invoice_num',
                    'invoice_date',
                    'name',
                    'total_taxes_human',
                    'total_human',
                    'invoice_url',
                ],
                'actionButtons' => [],
                'saveButton' => false,
                'editLink' => false,
                'deleteLink' => false
            ])
            ->order([
                $this->getAlias() . '.created' => 'desc'
            ]);
    }

    /**
     * Devuelve el siguiente número para la factura o recibo
     * 
     * @param  string $prefix 
     * @param  integer $first  El primer número a poner, en el caso de que no haya ninguno
     * @return integer
     */
    public function getNextNumber($prefix, $first = null)
    {
        $entity = $this->find()
            ->where([
                'prefix' => $prefix
            ])
            ->order([
                'num' => 'desc'
            ])
            ->first();
            
        if (!$entity) {
            if (empty($first)) {
                $number = 1;
            } else {
                $number = $first;
            }
        } else {
            $num = $entity->num;

            if (!is_numeric($num)) {
                $num = 0;
            }

            $number = ($num + 1);
        }

        return $number;
    }
}
