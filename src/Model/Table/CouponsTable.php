<?php

namespace Store\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\Table;
use Store\Cart\Cart;
use Cake\Event\Event;
use Cake\ORM\RulesChecker;
use Cake\ORM\TableRegistry;
use Cake\Event\EventManager;
use Store\Config\StoreConfig;
use Cake\Validation\Validator;
use Store\Model\Entity\PriceTrait;


class CouponsTable extends Table
{
    use PriceTrait;


    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('store_coupons');
        $this->setDisplayField('title');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');

        // Behaviors
        $this->addBehavior('Manager.Crudable');
        $this->addBehavior('Cofree.Contentable');


        $this->belongsToMany('Products', [
            'joinTable' => 'store_coupons_products',
            'foreignKey' => 'coupon_id',
            'targetForeignKey' => 'product_id',
            'className' => StoreConfig::getProductsModel()->registryAlias(),
            'targetTable' => StoreConfig::getProductsModel(),
        ]);

        $this->belongsToMany('Gifts', [
            'joinTable' => 'store_coupons_gifts',
            'foreignKey' => 'coupon_id',
            'targetForeignKey' => 'product_id',
            'className' => StoreConfig::getProductsModel()->registryAlias(),
        ]);

        $this->belongsToMany('Groups', [
            'joinTable' => 'store_coupons_groups',
            'foreignKey' => 'coupon_id',
            'targetForeignKey' => 'group_id',
            'className' => 'User.Groups',
        ]);

        $this->belongsToMany('States', [
            'joinTable' => 'store_coupons_states',
            'foreignKey' => 'coupon_id',
            'targetForeignKey' => 'state_id',
            'className' => 'Store.States',
        ]);



        // CRUD Config
        //Escribe aquí las asociaciones vinculadas con el model a la hora de editar (si las hubiera)
        $this->crud->associations(['Products', 'Gifts', 'Groups', 'States']);

        $this->crud
            ->addFields([
                'title' => __d('admin', 'Título'),
                'code' => __d('admin', 'Codigo'),
                'unique' => __d('admin', 'De un único uso'),
                'type' => [
                    'label' => __d('admin', 'Tipo'),
                    'options' => [
                        'percent' => 'Porcentaje',
                        'fixed' => 'Fijo',
                        'free_delivery' => 'Portes gratis',
                    ],
                    'type' => 'select'
                ],
                'value' => [
                    'label' =>  __d('admin', 'Valor'),
                    'show' => 'content.type != "free_delivery"',
                ],
                'active' => [
                    'label' => __d('admin', 'Activo'),
                    'show' => 'content.type != "free_delivery"'
                ],
                'products' => [
                    'type' => 'autocomplete',
                    'label' => __d('admin', 'Productos'),
                    'help' => __d('admin', 'Solo para unos productos concretos.'),
                    'show' => 'content.type != "free_delivery"'
                ],
                'gifts' => [
                    'type' => 'autocomplete',
                    'label' => __d('admin', 'Regalos'),
                    'show' => 'content.has_gifts'
                ],
                'groups' => [
                    'type' => 'select_multiple',
                    'label' => __d('admin', 'Grupos de usuarios'),
                    'help' => __d('admin', 'Indica los grupos de usuario que podrán optar a este cupón')
                ],
                'with_price_min' => __d('admin', 'Solo para pedidos con un mínimo'),
                'has_gifts' => __d('admin', 'Con regalos'),
                'price_min' => [
                    'label' => __d('admin', 'Precio mínimo'),
                    'show' => 'content.with_price_min'
                ],
                'start_on' => __d('admin', 'Comienzo'),
                'finish_on' => __d('admin', 'Fin'),
                'states' => [
                    'type' => 'autocomplete',
                    'label' => __d('admin', 'Provincias'),
                    'help' => __d('admin', 'Escribe las provincias que quieres que estén asociadas a esta oferta.')
                ],
                'states_exclude' => [
                    'label' => 'Excluir las provincias seleccionados',
                    'show' => "content.states.length > 0"
                ],
            ])
            ->addIndex('index', [
                'fields' => [
                    'title',
                    'code',
                    'type',
                    'value',
                    'active',
                    'start_on',
                    'finish_on',
                ],
                'actionButtons' => ['create'],
                'saveButton' => false,
            ])
            ->setName([
                'singular' => __d('admin', 'Cupones de descuento'),
                'plural' => __d('admin', 'Cupones de descuento'),
            ])
            ->addView('create', [
                'columns' => [
                    [
                        'cols' => 8,
                        'box' => $this->getBox()
                    ]
                ],
                'actionButtons' => ['create', 'index']
            ], ['update']);
    }

    private function getBox()
    {
        $out = [
            [
                'elements' => $this->getElements()
            ]
        ];

        if (StoreConfig::getConfig('couponDrifts')) {
            $out[] = [
                'title' => __d('admin', 'Regalos'),
                'elements' => [
                    'has_gifts',
                    'gifts',
                ]
            ];
        }

        if (StoreConfig::getConfig('couponStates')) {
            $out[] = [
                'title' => __d('admin', 'Restricción por zona geográfica'),
                'elements' => [
                    'states',
                    'states_exclude',
                ]
            ];
        }

        $out[] = [
            'title' => __d('admin', 'Fechas'),
            'elements' => [
                'start_on',
                'finish_on',
            ]
        ];

        return $out;
    }

    private function getElements()
    {
        $out = [
            'unique',
            'title',
            'code',
            'type',
            'with_price_min',
            'price_min',
            'value',
            'active',
            'products',
        ];

        if (StoreConfig::getConfig('couponGroups')) {
            $out[] = 'groups';
        }

        return $out;
    }

    public function findValid(Query $query)
    {
        $query->where([
            'Coupons.active' => true,
            'Coupons.start_on <= ' => date('Y-m-d'),
            'Coupons.finish_on >= ' => date('Y-m-d'),
        ]);


        return $query;
    }

    /**
     * Añade un cupón a un pedido
     *
     * @param string $text
     * @param integer $order_id
     * @param integer $user_id
     * @return mixed
     */
    public function addToOrder($text, $order, $user_id = null)
    {
        $query = $this->find('valid')
            ->where([
                'Coupons.code' => $text
            ])
            ->contain([
                'Groups',
                'States'
            ]);

        $event = new Event('Store.Model.Coupons.beforeFind', $this, [$query]);
        EventManager::instance()->dispatch($event);

        $coupon = $query->first();

        if (!$coupon) {
            return [
                'error' => __d('app', 'El cupón introducido no existe')
            ];
        }
        
        if ($coupon->unique) {
            $CouponsOrders = TableRegistry::getTableLocator()->get('Store.CouponsOrders');
            $CouponsOrders->belongsTo('Orders', [
                'className' => 'Store.Orders',
                'foreignKey' => 'order_id',
            ]);

            $has = $CouponsOrders->find()
                ->contain([
                    'Orders'
                ])
                ->where([
                    'Orders.status IS NOT NULL',
                    'CouponsOrders.coupon_id' => $coupon->id,
                ])
                ->first();

            if ($has) {
                $coupon = null;

                return [
                    'error' => __d('app', 'El cupón introducido ya ha sido utilizado')
                ];
            }
        }

        

        if (!$this->checkForGroup($coupon, $order)) {
            return [
                'error' => __d('app', 'El cupón introducido no es válido')
            ];
        }

        $isAllow = new \stdClass();
        $isAllow->allow = true;

        $event = new Event('Store.Model.Coupons.check', $this, [$isAllow, $coupon, $user_id]);
        EventManager::instance()->dispatch($event);

        if (!$isAllow->allow) {
            return [
                'error' => __d('app', 'El cupón introducido no es válido')
            ];
        }

        if (!$this->checkStates($coupon, $order)) {
            return [
                'error' => __d('app', 'El cupón introducido no es válido')
            ];
        }

        // Comprueba si está siendo usado para este pedido
        $used_for_order = $this->checkUsedForOrder($coupon->id, $order->id);

        if ($used_for_order) {
            return [
                'error' => __d('app', 'Este cupón ya está siendo utilizado para este pedido')
            ];
        }

        // Comprueba si ha sido usado por el mismo usuario otra vez
        if ($user_id) {
            $used = $this->checkUsedForUser($coupon->id, $user_id);

            if ($used) {
                return [
                    'error' => __d('app', 'Ya has usado este cupón anteriormente')
                ];
            }
        }

        if (!$this->checkPriceMin($coupon, $order)) {
            return [
                'error' => __d('app', 'El pedido debe ser como mínimo de {0}', [
                    strip_tags($this->setPrice($coupon->price_min))
                ])
            ];
        }



        $CouponsOrders = TableRegistry::getTableLocator()->get('Store.CouponsOrders');

        $data = [
            'user_id' => $user_id,
            'coupon_id' => $coupon->id,
            'order_id' => $order->id
        ];

        $entity = $CouponsOrders->newEntity($data);
        $CouponsOrders->save($entity);

        return [
            'success' => __d('app', 'El cupón ha sido añadido correctamente')
        ];
    }


    public function checkCoupon($coupon, $order, $user_id)
    {
        if (!$this->checkForGroup($coupon, $order) || !$this->checkStates($coupon, $order) || !$this->checkPriceMin($coupon, $order)) {
            return false;
        }

        // Comprueba si ha sido usado por el mismo usuario otra vez
        if ($user_id) {
            $used = $this->checkUsedForUser($coupon->id, $user_id);

            if ($used) {
                return false;
            }
        }

        return true;
    }



    /**
     * Comprueba si el cupón ha sido usado anteriomente por el usuario
     *
     * @param integer $coupon_id
     * @param integer $user_id
     * @return boolean
     */
    public function checkUsedForUser($coupon_id, $user_id)
    {
        $used = TableRegistry::getTableLocator()->get('Store.CouponsOrders')->find()
            ->where([
                'CouponsOrders.coupon_id' => $coupon_id,
                'CouponsOrders.user_id' => $user_id,
                'Orders.status IS NOT NULL'
            ])
            ->contain([
                'Orders'
            ])
            ->first();

        return (bool)$used;
    }

    private function checkStates($coupon, $order)
    {
        if (empty($coupon->states)) {
            return true;
        }

        $state_delivery_id = Cart::getStateDeliveryId();

        if ($state_delivery_id) {
            $state_id = $state_delivery_id;
        } elseif (!empty($order->user_id)) {
            $address = TableRegistry::getTableLocator()->get('Store.Addresses')->find()
                ->where([
                    'Addresses.user_id' => $order->user_id,
                    'Addresses.type' => 'delivery'
                ])
                ->first();

            if ($address) {
                $state_id = $address->state_id;
            }
        }

        if (empty($state_id)) {
            return false;
        }

        $state_ids = collection($coupon->states)->extract('id')->toArray();

        if (!$coupon->states_exclude && in_array($state_id, $state_ids)) {
            return true;
        }

        if ($coupon->states_exclude && !in_array($state_id, $state_ids)) {
            return true;
        }

        return false;
    }

    /**
     * Comprueba si el cupón ha sido ya usado para este pedido dado
     *
     * @param integer $coupon_id
     * @param integer $order_id
     * @return boolean
     */
    public function checkUsedForOrder($coupon_id, $order_id)
    {
        $used = TableRegistry::getTableLocator()->get('Store.CouponsOrders')->find()
            ->where([
                'CouponsOrders.coupon_id' => $coupon_id,
                'CouponsOrders.order_id' => $order_id,
            ])
            ->first();

        return (bool)$used;
    }

    public function checkForGroup($coupon, $order)
    {
        if (empty($coupon->groups)) {
            return true;
        }

        if (empty($order->user_id)) {
            return false;
        }

        $group_ids = collection($coupon->groups)->extract('id')->toArray();

        $user = TableRegistry::getTableLocator()->get('User.Users')->find()
            ->where([
                'Users.id' => $order->user_id
            ])
            ->first();

        if (!$user) {
            return false;
        }

        return in_array($user->group_id, $group_ids);
    }

    public function removeFromOrder($order_id)
    {
        TableRegistry::getTableLocator()->get('Store.CouponsOrders')->deleteAll([
            'order_id' => $order_id
        ]);
    }

    public function checkPriceMin($coupon, $order)
    {
        if (!$coupon->with_price_min) {
            return true;
        }

        return $coupon->price_min < $order->subtotal_num;
    }
}
