<?php
namespace Store\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;
use Store\Model\Entity\ProductView;
use Store\Config\StoreConfig;

/**
 * ProductViews Model
 */
class ProductViewsTable extends Table
{

  protected $_defaultConfig = [
    'implementedFinders' => [
      'ranking' => 'findRanking',
    ],
  ];

  public function initialize(array $config)
  {
    parent::initialize($config);

    $this->table('store_product_views');
    $this->displayField('id');
    $this->primaryKey('id');
    $this->addBehavior('Timestamp');

    $this->belongsTo( 'Products', [
      'className' => StoreConfig::getProductsModel()->registryAlias(),
      'foreignKey' => 'product_id'
    ]);
  }

  public function add( $session_id, $user_id, $product_id)
  {
    $data = [
      'session_id' => $session_id,
      'user_id' => $user_id,
      'product_id' => $product_id
    ];

    $entity = $this->newEntity( $data);
    $this->save( $entity);
  }

/**
 * Finder para devolver los productos más vistos
 *   
 * @param  Cake\ORM\Query  $Query
 * @param  array  $options
 * @return Cake\ORM\Query
 */
  public function findRanking( Query $query, array $options)
  {
    $query->contain([
      'Products'
    ]);
    $query->group(['ProductViews.product_id']);
    $query->select([
        'total' => 'COUNT(ProductViews.product_id)', 
        'ProductViews.id', 
        'ProductViews.user_id',
        'ProductViews.product_id',
    ]);
    
    $query->limit( 10);
    $query->order(['total' => 'desc']);

    return $query;
  }

  public function validationDefault(Validator $validator)
  {
    return $validator;
  }


}
