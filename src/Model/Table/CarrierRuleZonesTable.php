<?php
namespace Store\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;
use Store\Model\Entity\CarrierRuleZone;

/**
 * CarrierRuleZones Model
 */
class CarrierRuleZonesTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        $this->table( 'store_carrier_rule_zones');
        $this->displayField( 'id');
        $this->primaryKey( 'id');
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->add('id', 'valid', ['rule' => 'numeric'])
            ->allowEmpty('id', 'create')
            ->add('carrier_rule_id', 'valid', ['rule' => 'numeric'])
            ->allowEmpty('carrier_rule_id')
            ->add('zone_id', 'valid', ['rule' => 'numeric'])
            ->allowEmpty('zone_id');

        return $validator;
    }
}
