<?php
namespace Store\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * InvoiceRefundLineItems Model
 *
 * @property \Store\Model\Table\InvoiceRefundsTable&\Cake\ORM\Association\BelongsTo $InvoiceRefunds
 * @property \Store\Model\Table\LineItemsTable&\Cake\ORM\Association\BelongsTo $LineItems
 *
 * @method \Store\Model\Entity\InvoiceRefundLineItem get($primaryKey, $options = [])
 * @method \Store\Model\Entity\InvoiceRefundLineItem newEntity($data = null, array $options = [])
 * @method \Store\Model\Entity\InvoiceRefundLineItem[] newEntities(array $data, array $options = [])
 * @method \Store\Model\Entity\InvoiceRefundLineItem|false save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \Store\Model\Entity\InvoiceRefundLineItem saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \Store\Model\Entity\InvoiceRefundLineItem patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \Store\Model\Entity\InvoiceRefundLineItem[] patchEntities($entities, array $data, array $options = [])
 * @method \Store\Model\Entity\InvoiceRefundLineItem findOrCreate($search, callable $callback = null, $options = [])
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class InvoiceRefundLineItemsTable extends Table
{
    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('store_invoice_refund_line_items');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');

        $this->belongsTo('InvoiceRefunds', [
            'foreignKey' => 'invoice_refunds_id',
            'className' => 'Store.InvoiceRefunds',
        ]);
        $this->belongsTo('LineItems', [
            'foreignKey' => 'line_item_id',
            'className' => 'Store.LineItems',
        ]);
    }

   
}
