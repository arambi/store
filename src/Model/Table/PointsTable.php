<?php
namespace Store\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;
use Cake\ORM\TableRegistry;
use Store\Model\Entity\Point;
use Website\Lib\Website;
use Cake\Mailer\MailerAwareTrait;

/**
 * Points Model
 */
class PointsTable extends Table
{
  use MailerAwareTrait;

  /**
   * Initialize method
   *
   * @param array $config The configuration for the Table.
   * @return void
   */
  public function initialize(array $config)
  {
    parent::initialize($config);

    $this->table( 'store_points');
    $this->displayField( 'id');
    $this->primaryKey( 'id');
    
    // Associations
    $this->belongsTo( 'Users', [
      'className' => 'User.Users',
      'foreignKey' => 'user_id'
    ]);

    $this->belongsTo( 'Orders', [
      'className' => 'Store.Orders',
      'foreignKey' => 'order_id'
    ]);
    
    $this->belongsTo( 'Administrators', [
      'className' => 'User.Users',
      'foreignKey' => 'administrator_id'
    ]);

    // Behaviors
    $this->addBehavior( 'Timestamp');
    $this->addBehavior( 'Manager.Crudable');
    $this->addBehavior( 'Cofree.Saltable');

    // CRUD Config
    //Escribe aquí las asociaciones vinculadas con el model a la hora de editar (si las hubiera)
    // $this->crud->associations([]);

    $this->crud
      ->addIndex( 'index', [
        'fields' => [
          'user_id',
          'order_id',
          'points',
    
        ],
        'actionButtons' => ['create'],
        'saveButton' => false,
      ])
      ->setName( [
        'singular' => __d( 'admin', 'Puntos de descuento'),
        'plural' => __d( 'admin', 'Puntos de descuento'),
      ])
      ->addView( 'add', [
        'columns' => [
          [
            'cols' => 8,
            'box' => [
              [
                'elements' => [
                  'points' => [
                    'label' => __d( 'admin', 'Número de puntos'),
                    'type' => 'numeric',
                    'spin' => true,
                    'range' => [1, 100]
                  ],
                  'groups' => [
                    'label' => __d( 'admin', 'Grupos de usuarios'),
                    'type' => 'select_multiple',
                    'options' => function( $crud){
                      return TableRegistry::get( 'User.Groups')->find()->toArray();
                    }
                  ],
                  'countries' => [
                    'label' => __d( 'admin', 'Países'),
                    'type' => 'select_multiple',
                    'options' => function( $crud){
                      return TableRegistry::get( 'Store.Countries')->find()->toArray();
                    }
                  ],
                  'states' => [
                    'label' => __d( 'admin', 'Provincias'),
                    'type' => 'select_multiple',
                    'options' => function( $crud){
                      return TableRegistry::get( 'Store.States')->find()->toArray();
                    }
                  ],
                  'userstop' => [
                    'label' => __d( 'admin', 'Usuarios que hayan comprado más de (€)'),
                    'type' => 'numeric',
                  ],
                  'usersfrom' => [
                    'label' => __d( 'admin', 'Usuarios que se hayan registrado desde la fecha'),
                    'type' => 'date',
                  ],
                  'usersto' => [
                    'label' => __d( 'admin', 'Usuarios que se hayan registrado hasta la fecha'),
                    'type' => 'date',
                  ]
                ]
              ]
            ]
          ]
        ],
        'template' => 'Manager/update',
        'actionButtons' => ['create', 'index']
      ])
      ->setContent([
          'points' => null,
          'groups' => [],
          'countries' => [],
          'states' => []
        ])
      ;
    
  }


  public function bindUser()
  {
    $this->Users->hasMany( 'PointsUp', [
      'className' => 'Store.Points',
      'foreignKey' => 'user_id',
      'conditions' => [
        'PointsUp.action' => 'up' 
      ],
      'sort' => [
        'PointsUp.created' => 'desc'
      ]
    ]);

    $this->Users->hasMany( 'PointsDown', [
      'className' => 'Store.Points',
      'foreignKey' => 'user_id',
      'conditions' => [
        'PointsDown.action' => 'down' 
      ],
      'finder' => 'down',
      'sort' => [
        'PointsDown.created' => 'desc'
      ]
    ]);

    $this->Orders->associations()->remove( 'PointsDown');
    $this->Orders->associations()->remove( 'PointsUp');
    $this->Users->crud->addAssociations( ['PointsUp', 'PointsDown', 'Administrators']);
  }

  public function findDown( Query $query, array $options)
  {
    return $query
      ->where(['Orders.status IS NOT NULL'])
      ->contain( 'Orders');
  }
/**
 * Añade puntos a un usuario
 * 
 * @param integer $points el número de puntos a añadir
 * @param integer $user_id          El id del usuario
 * @param integer|null $administrator_id El id del administrador
 */
  public function addPoints( $data)
  {
    $_defaults = [
      'administrator_id' => null,
      'order_id' => null,
      'action' => 'up'
    ];

    $data = array_merge( $_defaults, $data);

    $entity = $this->newEntity( $data);
    return $this->save( $entity);
  }

  public function addPointsFromOrder( $order)
  {
    $this->addPoints([
      'points' => $this->calculePointsFromOrder( $order),
      'order_id' => $order->id,
      'user_id' => $order->user_id
    ]);

    if( isset( $_SERVER ['PHP_SELF']) && strpos( $_SERVER ['PHP_SELF'], 'phpunit') !== false)
    {
      return true;
    }
    
    // Envio de email
    $this->getMailer( 'Store.Shop')->send( 'user_add_points_order', [
        $order->user,
        $this->calculePointsFromOrder( $order),
        $this->totalAvailables( $order->user_id)
    ]);
  }

  public function calculePointsFromOrder( $order)
  {
    $value = Website::get( 'settings.store_points_up_value');
    $total = $order->subtotal - $order->points_discount - $order->points_discount_tax;
    return round( $total * $value);
  }

  public function usePoints( $points, $user_id, $order, $id = null)
  {
    $availables = $this->totalAvailables( $user_id, $order);

    if( $points > $availables)
    {
      return false;
    }

    $data = [
      'user_id' => $user_id,
      'order_id' => $order->id,
      'points' => $points,
      'action' => 'down' 
    ];

    if( $id && $entity = $this->get( $id))
    {
      $this->patchEntity( $entity, $data);
    }
    else
    {
      $entity = $this->newEntity( $data);
    }

    $change_value = Website::get( 'settings.store_points_down_value');
    $discount = $entity->points * $change_value;

    $entity->set( 'discount', $discount);
    return $this->save( $entity);
  }

  public function total( $user_id, $action, $order = false)
  {
    $query = $this->find()
      ->where([
        $this->alias() .'.user_id' => $user_id,
        $this->alias() .'.action' => $action,
      ])
      
      ->select( ['total' => 'SUM('. $this->alias() .'.points)']);

    if( $action == 'down')
    {
      $query
        ->where([
          'Orders.status IS NOT NULL',
        ])
        ->contain([
          'Orders'
        ]);
    }

    $query->where([
      $this->alias() .'.created > ' => (date( 'Y') - 1) .'-12-31',
    ]);

    if( $order)
    {
      $query->where( ['NOT' => [$this->alias() . '.order_id' => $order->id]]);
    }
    $total = $query->first();
    return $total->total;
  }


  public function totalAvailables( $user_id, $order = false)
  {
    if( $user_id)
    {
      $down = $this->total( $user_id, 'down', $order);
      $up = $this->total( $user_id, 'up');
  
      return $up - (int)$down;
    }
  }
}
