<?php
namespace Store\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;
use Store\Model\Entity\Attribute;
use Cake\Event\Event;
use Cake\Core\Configure;

/**
 * Attributes Model
 */
class AttributesTable extends Table
{
  /**
   * Initialize method
   *
   * @param array $config The configuration for the Table.
   * @return void
   */
  public function initialize( array $config)
  {
    $this->table( 'store_attributes');
    $this->displayField( 'title');
    $this->primaryKey( 'id');



    // Behaviors
    $this->addBehavior( 'Timestamp');
    $this->addBehavior( Configure::read( 'I18n.behavior'), [
      'fields' => ['title'],
      'translationTable' => 'Store.ShopI18n',
    ]);
    $this->addBehavior( 'Manager.Crudable');
    $this->addBehavior( 'Cofree.Saltable');
    $this->addBehavior( 'Cofree.Sortable');


    // CRUD
    $this->crud
      ->addFields([
        'title' => [
          'label' => __d( 'admin', 'Nombre'),
          'help' => __d( 'admin', 'Nombre del atributo')
        ],
        'color' => [
          'label' => __d( 'admin', 'Color'),
          'help' => __d( 'admin', 'Color del atributo'),
          'show' => "parentContent.group_type == 'color'",
          'type' => 'colorpicker'
        ],
      ])
      ->addIndex( 'index', [
        'fields' => [
          'title',
          'color',
        ],
        'actionButtons' => ['create'],
        'saveButton' => false,
      ])
      ->setName( [
        'singular' => __d( 'admin', 'Valores de atributo de producto'),
        'plural' => __d( 'admin', 'Valores de atributos'),
      ])
      ->addView( 'create', [
        'columns' => [
          [
            'box' => [
              [
                'elements' => [
                  'title',
                  'color',
                ]
              ]
            ]
          ],
        ],
        'actionButtons' => ['index'],
      ], ['update'])
      
      ;
  }

  public function beforeFind( Event $event, Query $query)
  {
    $query->formatResults(function ($results) {
      return $this->_rowMapper( $results);
    }, $query::PREPEND);
  }

  protected function _rowMapper( $results)
  {
    return $results->map(function( $result){
      if( is_object( $result))
      {
        $result->set( 'attribute_group', $result->attribute_group);
        return $result;
      }
      
    });
  }


}
