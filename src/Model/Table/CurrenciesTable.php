<?php
namespace Store\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;
use Store\Model\Entity\Currency;

/**
 * Currencies Model
 */
class CurrenciesTable extends Table
{
  /**
   * Initialize method
   *
   * @param array $config The configuration for the Table.
   * @return void
   */
  public function initialize(array $config)
  {
    $this->table( 'store_currencies');
    $this->displayField( 'title');
    $this->primaryKey( 'id');
    

    // Behaviors
    $this->addBehavior( 'Timestamp');
    $this->addBehavior( 'Manager.Crudable');
    $this->addBehavior( 'Cofree.Saltable');

    // CRUD
    $this->crud
      ->addFields([
        'active' => __d( 'admin', 'Activo'),
        'title' => __d( 'admin', 'Nombre de la divisa'),
        'iso_code' => [
          'label' => __d( 'admin', 'Código ISO'),
          'help' => __d( 'admin', 'Código ISO (ej. USD para Dólar, EUR para Euros, etc.)')
        ],
        'iso_code_num' => [
          'label' => __d( 'admin', 'Código numérico ISO'),
          'help' => __d( 'admin', 'Código numérico ISO (ej: 840 para el Dolar, 978 para el Euro)')
        ],
        'sign' => [
          'label' => __d( 'admin', 'Símbolo'),
          'help' => __d( 'admin', 'Símbolo de la moneda (€, $)')
        ],
        'conversion_rate' => [
          'label' => __d( 'admin', 'Tipo de cambio'),
          'help' => __d( 'admin', 'Las tasas de cambio se calculan a partir de la unidad de moneda por defecto de su tienda. Por ejemplo, si la moneda base es el euro y la moneda elegida es el dólar, el tipo de "1,20" (1€ = $1,20).')
        ],
        'format' => [
          'label' => __d( 'admin', 'Formato de moneda'),
          'help' => __d( 'admin', 'Se aplica a todos los precios (por ejemplo, 1.240,15€)'),
          'type' => 'select',
          'options' => [
              '' => __d( 'admin', '--- Selección --'),
              '0 000,00X' => '0 000,00X' .' '. __d( 'admin', '(Tal como con euros)'),
              'X0,000.00' => 'X0,000.00' .' '. __d( 'admin', '(Tal como con los dólares)'),
              'X0.000,00' => 'X0.000,00',
              '0,000.00X' => '0,000.00X',
              'X0\'000.00' => 'X0\'000.00'                                                                                    
          ]
        ],
        'decimals' => [
          'label' => __d( 'admin', 'Decimales'),
          'help' => __d( 'admin', 'Mostrar decimales en los precios')
        ],
        'blank' => [
          'label' => __d( 'admin', 'Espaciado'),
          'help' => __d( 'admin', 'Incluya un espacio entre el símbolo y el precio (por ejemplo, 1.240,15€ -> 1.240,15€)')
        ]
      ])
      ->addIndex( 'index', [
        'fields' => [
          'title',
          'active',
          'iso_code'     
        ],
        'actionButtons' => ['create'],
        'saveButton' => false,
      ])
      ->setName( [
        'singular' => __d( 'admin', 'Moneda'),
        'plural' => __d( 'admin', 'Monedas'),
      ])
      ->addView( 'create', [
        'columns' => [
          [
            'box' => [
              [
                'elements' => [
                  'title',
                  'active',
                  'iso_code',
                  'iso_code_num',
                  'sign',
                  'conversion_rate',
                  'format',
                  'decimals',
                  'blank'
                ]
              ]
            ]
          ],
        ],
        'actionButtons' => ['index'],
      ], ['update'])
      ;
  }

  /**
   * Default validation rules.
   *
   * @param \Cake\Validation\Validator $validator Validator instance.
   * @return \Cake\Validation\Validator
   */
  public function validationDefault(Validator $validator)
  {
      $validator
          ->notEmpty( 'title', __d( 'admin', 'El título no puede estar vacío'))
          ->notEmpty( 'iso_code', __d( 'admin', 'Este campo no puede estar vacío'))
          ->notEmpty( 'iso_code_num', __d( 'admin', 'Este campo no puede estar vacío'))
          ->notEmpty( 'sign', __d( 'admin', 'Este campo no puede estar vacío'))
          ->notEmpty( 'format', __d( 'admin', 'Este campo no puede estar vacío'))
          ->notEmpty( 'conversion_rate', __d( 'admin', 'Este campo no puede estar vacío'));

      return $validator;
  }
}
