<?php

namespace Store\Model\Table;

use ArrayObject;
use Cake\ORM\Query;
use Cake\ORM\Table;
use Cake\Event\Event;
use Website\Lib\Website;
use Cake\ORM\RulesChecker;
use Cake\ORM\TableRegistry;
use Cake\Utility\Inflector;
use Store\Config\StoreConfig;
use Cake\Validation\Validator;
use Store\Model\Entity\LineItem;
use Letter\Mailer\MailerAwareTrait;

/**
 * LineItems Model
 */
class LineItemsTable extends Table
{
    use MailerAwareTrait;

    private $mailersMap = [
        StoreConfig::ITEM_STATUS_REFUNDED_REQUEST => [
            'user',
            'admin',
        ],
        StoreConfig::ITEM_STATUS_REFUNDED_WORKING => [
            'user',
            'admin',
        ],
        StoreConfig::ITEM_STATUS_REFUNDED_SEND => [
            'user',
            'admin',
        ],
        StoreConfig::ITEM_STATUS_REFUNDED_RECEIVED => [
            'user',
            'admin',
        ],
        StoreConfig::ITEM_STATUS_REFUNDED_ACCEPT => [
            'user',
            'admin',
        ],
        StoreConfig::ITEM_STATUS_REFUNDED_CANCELED => [
            'user',
            'admin',
        ],
        StoreConfig::ITEM_STATUS_REFUNDED_REFUSED => [
            'user',
            'admin',
        ],
    ];

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        $this->setTable('store_line_items');
        $this->setDisplayField('title');
        $this->setPrimaryKey('id');
        $this->addBehavior('Timestamp');
        $this->addBehavior('Cofree.Saltable');
        $this->belongsTo('Products', [
            'className' => StoreConfig::getProductsModel()->getAlias(),
            'targetTable' => StoreConfig::getProductsModel(),
            'foreignKey' => 'product_id',
        ]);

        $this->belongsTo('ProductAttributes', [
            'className' => 'Store.ProductAttributes',
            'foreignKey' => 'product_attribute_id',
        ]);

        $this->belongsTo('Orders', [
            'className' => 'Store.Orders',
            'foreignKey' => 'order_id',
        ]);

        $this->hasMany('LineItemHistories', [
            'className' => 'Store.LineItemHistories',
            'foreignKey' => 'line_item_id',
            'sort' => [
                'LineItemHistories.created' => 'desc'
            ]
        ]);
    }

    public function addStatus($id, $status, $user_id = null)
    {
        $item = $this->find()
            ->contain([
                'Orders'
            ])
            ->where([
                'LineItems.id' => $id
            ])
            ->first();

        $user = $this->Orders->Users->find()
            ->where(['Users.id' => $item->order->user_id])
            ->first();

        if (!$item) {
            return false;
        }

        $last = $this->getLastStatus($item->id);

        if (!$last || $last->status != $status) {
            $data = [
                'line_item_id' => $item->id,
                'status' => $status,
                'user_id' => $user_id
            ];

            $entity = $this->LineItemHistories->newEntity($data);

            if ($this->LineItemHistories->save($entity)) {
                $this->query()->update()
                    ->set('status', $status)
                    ->where(['id' => $item->id])
                    ->execute();

                $event = new Event('Store.Model.LineItems.addStatus', $this, [
                    $item,
                    $status
                ]);
        
                $this->getEventManager()->dispatch($event);

                $this->sendMailStatus($status, $item, $user);
                return true;
            }
        }
    }

    public function sendMailStatus($status, $item, $user)
    {
        $dontsends = StoreConfig::getConfig('dontSendStatus');

        if (in_array($status, $dontsends)) {
            return;
        }

        if (in_array('user', $this->mailersMap[$status])) {
            $locale = $user ? $user->locale : ($item->order->locale ? $item->order->locale : false);
            $key = 'item' . Inflector::camelize($status);
            $email = $user ? $user->email : $item->order->adr_invoice_email;
            $this->getMailer('Store.Orders')->send($key, [
                $item,
                $email,
            ], [], $locale);
        }

        if (in_array('admin', $this->mailersMap[$status])) {
            $email_admin = Website::get('settings.store_order_email');

            if (empty($email_admin)) {
                $email_admin = Website::get('settings.users_reply_email');
            }

            $emails = new ArrayObject(explode(',', $email_admin));

            $event = new Event('Store.Model.LineItems.itemsEmailsMailer', $this, [
                $emails,
                $item,
                $status
            ]);
    
            $this->getEventManager()->dispatch($event);

            foreach ($emails as $email) {
                $key = 'item' . Inflector::camelize($status) . 'Admin';
                $this->getMailer('Store.Orders')->send($key, [
                    $item,
                    $email,
                ], [], $locale);
            }
        }
    }

    public function getLastStatus($id)
    {
        $item = $this->find()
            ->where([
                'LineItems.id' => $id
            ])
            ->contain([
                'LineItemHistories' => function ($q) {
                    return $q
                        ->limit(1);
                }
            ])
            ->first();

        if ($item && isset($item->line_item_histories[0])) {
            return $item->line_item_histories[0];
        }

        return false;
    }

    public function createInvoiceRefund($item, $shipping_coast = false)
    {
        if (!StoreConfig::getConfig('withInvoices')) {
            return;
        }

        TableRegistry::getTableLocator()->get('Store.InvoiceRefunds')->createFromItem($item, $shipping_coast);
    }
}
