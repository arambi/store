<?php
namespace Store\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * CarrierFestives Model
 *
 * @method \Store\Model\Entity\CarrierFestive get($primaryKey, $options = [])
 * @method \Store\Model\Entity\CarrierFestive newEntity($data = null, array $options = [])
 * @method \Store\Model\Entity\CarrierFestive[] newEntities(array $data, array $options = [])
 * @method \Store\Model\Entity\CarrierFestive|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \Store\Model\Entity\CarrierFestive patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \Store\Model\Entity\CarrierFestive[] patchEntities($entities, array $data, array $options = [])
 * @method \Store\Model\Entity\CarrierFestive findOrCreate($search, callable $callback = null, $options = [])
 */
class CarrierFestivesTable extends Table
{

/**
 * Initialize method
 *
 * @param array $config The configuration for the Table.
 * @return void
 */
public function initialize(array $config)
{
    parent::initialize($config);

    $this->setTable('store_carrier_festives');
    $this->setDisplayField('id');
    $this->setPrimaryKey('id');
    
    // Behaviors
    $this->addBehavior( 'Manager.Crudable');

    // CRUD Config
    //Escribe aquí las asociaciones vinculadas con el model a la hora de editar (si las hubiera)
    // $this->crud->associations([]);

    $this->crud
      ->addFields([
        'day' => __d( 'admin', 'Día'),
        
      ])
      ->addIndex( 'index', [
        'fields' => [
          'day',
        ],
        'actionButtons' => ['create'],
        'saveButton' => false,
      ])
      ->setName( [
        'singular' => __d( 'admin', 'Festivos'),
        'plural' => __d( 'admin', 'Festivos'),
      ])
      ->addView( 'create', [
        'columns' => [
          [
            'cols' => 8,
            'box' => [
              [
                'elements' => [
                'day',
                ]
              ]
            ]
          ]
        ],
        'actionButtons' => []
      ], ['update'])
      ;
  
  }

  public function findFront( Query $query)
  {
    return $query
    ;
  }

}
