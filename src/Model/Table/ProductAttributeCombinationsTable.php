<?php
namespace Store\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;
use Store\Model\Entity\ProductAttributeCombination;
use Cake\Datasource\EntityInterface;
use Cake\Event\Event;
use ArrayObject;

/**
 * ProductAttributeCombinations Model
 */
class ProductAttributeCombinationsTable extends Table
{

  /**
   * Initialize method
   *
   * @param array $config The configuration for the Table.
   * @return void
   */
  public function initialize(array $config)
  {
    $this->table('store_product_attribute_combinations');
    $this->displayField('id');
    $this->primaryKey('id');

    $this->belongsTo( 'Attributes', [
        'className' => 'Store.Attributes',
        'foreignKey' => 'attribute_id'
    ]);
  }



  /**
   * Default validation rules.
   *
   * @param \Cake\Validation\Validator $validator Validator instance.
   * @return \Cake\Validation\Validator
   */
  public function validationDefault(Validator $validator)
  {
    $validator
        ->add('id', 'valid', ['rule' => 'numeric'])
        ->allowEmpty('id', 'create')
        ->add('attribute_id', 'valid', ['rule' => 'numeric'])
        ->allowEmpty('attribute_id')
        ->add('product_attribute_id', 'valid', ['rule' => 'numeric'])
        ->allowEmpty('product_attribute_id');

    return $validator;
  }
}
