<?php

namespace Store\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\Table;
use Cake\Event\Event;
use Website\Lib\Website;
use Cake\ORM\RulesChecker;
use Cake\ORM\TableRegistry;
use Cake\Validation\Validator;
use Letter\Mailer\MailerAwareTrait;
use Store\Config\StoreConfig;
use Store\Model\Table\InvoicesTrait;

/**
 * InvoiceRefunds Model
 *
 * @method \Store\Model\Entity\InvoiceRefund get($primaryKey, $options = [])
 * @method \Store\Model\Entity\InvoiceRefund newEntity($data = null, array $options = [])
 * @method \Store\Model\Entity\InvoiceRefund[] newEntities(array $data, array $options = [])
 * @method \Store\Model\Entity\InvoiceRefund|false save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \Store\Model\Entity\InvoiceRefund saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \Store\Model\Entity\InvoiceRefund patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \Store\Model\Entity\InvoiceRefund[] patchEntities($entities, array $data, array $options = [])
 * @method \Store\Model\Entity\InvoiceRefund findOrCreate($search, callable $callback = null, $options = [])
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class InvoiceRefundsTable extends Table
{
    use InvoicesTrait;
    use MailerAwareTrait;

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('store_invoice_refunds');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');
        $this->addBehavior('Cofree.Saltable');

        // Behaviors
        $this->addBehavior('Manager.Crudable');

        $this->belongsTo('Orders', [
            'className' => 'Store.Orders',
            'foreignKey' => 'order_id ',
        ]);

        $this->hasMany('InvoiceRefundLineItems', [
            'className' => 'Store.InvoiceRefundLineItems',
            'foreignKey' => 'invoice_refund_id',
        ]);

        // CRUD Config
        //Escribe aquí las asociaciones vinculadas con el model a la hora de editar (si las hubiera)
        $this->crud->associations(['Orders', 'InvoiceRefundLineItems']);

        $this->_setCrud();

        $this->crud
            ->addFields([
                'concept' => [
                    'label' => 'Concepto',
                    'type' => 'string',
                ],
                'price' => [
                    'label' => 'Precio',
                    'type' => 'numeric',
                ],
                'tax_rate' => [
                    'label' => 'Impuesto (porcentaje)',
                    'type' => 'numeric',
                ]
            ])
            ->addView('create', [
                'columns' => [
                    [
                        'cols' => 8,
                        'title' => 'General',
                        'box' => [
                            [
                                'elements' => [
                                    'concept',
                                    'price',
                                    'tax_rate',
                                ]
                            ],

                        ],
                    ],
                ]
            ]);

        $this->crud->setName([
            'singular' => __d('admin', 'Facturas de devolución'),
            'plural' => __d('admin', 'Facturas de devolución'),
        ]);

        $this->crud->getView('index')
            ->set('exportCsv', true)
            ->set('csvFields', [
                'order_id' => 'ID de pedido',
                'invoice_number' => [
                    'label' => 'Número',
                    'type' => 'info'
                ],
                // 'invoice_date' => 'Fecha',
                'adr_invoice_firstname' => 'Nombre',
                'adr_invoice_lastname' => 'Apellidos',
                'adr_invoice_company' => 'Empresa',
                'adr_invoice_vat_number' => 'CIF',
                'adr_invoice_address' => 'Dirección',
                'adr_invoice_postcode' => 'CP',
                'adr_invoice_city' => 'Población',
                'total_without_taxes_human_no_tags' => 'Base Imponible',
                'total_taxes_human_no_tags' => 'IVA',
                'total_human_no_tags' => 'Total',

            ]);
    }

    public function findFront(Query $query)
    {
        return $query;
    }

    public function createFromOrder($order_id, $type)
    {
        $order = TableRegistry::getTableLocator()->get('Store.Orders')
            ->find('order')
            ->where([
                'Orders.id' => $order_id
            ])
            ->first();

        $prefix = Website::get('settings.store_invoice_refund_prefix');
        $first = Website::get('settings.store_invoice_refund_first');
        $number = $this->getNextNumber($prefix, $first);

        $data = [
            'order_id' => $order->id,
            'num' => $number,
            'currency' => $order->currency,
            // 'invoice_date' => date('Y-m-d'),
            'invoice_refund_line_items' => []
        ];

        $items = [];

        foreach ($order->line_items as $item) {
            $items[] = [
                'line_item_id' => $item->id,
                'quantity' => $item->quantity,
                'price' => -$item->price,
                'tax_rate' => $item->tax_rate,
                'currency' => $item->currency,
            ];
        }

        $data['invoice_refund_line_items'] = $items;

        if (!empty($prefix)) {
            $data['prefix'] = $prefix;
        }

        if ($type == 'shipping') {
            $data['shipping_handling'] = -$order->shipping_handling;
            $data['shipping_handling_tax_rate'] = $order->shipping_handling_tax_rate;
            $data['shipping_handling_tax'] = $this->Orders->Carriers->Taxes->calculate($data['shipping_handling_tax_rate'], $data['shipping_handling']);
        }


        $entity = $this->newEntity($data);

        $this->getEventManager()->dispatch(new Event('Store.Model.InvoicesRefunds.create', $this, [
            $entity,
            $order,
        ]));

        $this->save($entity);
        return $entity;
    }

    public function createFromConcept($order_id, $concept, $value, $tax_rate)
    {
        $order = TableRegistry::getTableLocator()->get('Store.Orders')
            ->find('order')
            ->where([
                'Orders.id' => $order_id
            ])
            ->first();

        $prefix = Website::get('settings.store_invoice_refund_prefix');
        $first = Website::get('settings.store_invoice_refund_first');
        $number = $this->getNextNumber($prefix, $first);

        $data = [
            'order_id' => $order->id,
            'num' => $number,
            // 'invoice_date' => date('Y-m-d'),
            'invoice_refund_line_items' => [
                [
                    'concept' => $concept,
                    'quantity' => 1,
                    'price' => (float)$value * -1,
                    'tax_rate' => $tax_rate
                ]
            ]
        ];

        $entity = $this->newEntity($data);
        $this->save($entity);
        return $entity;
    }

    public function createFromItem($item, $shipping_coast = false)
    {
        $prefix = Website::get('settings.store_invoice_refund_prefix');
        $first = Website::get('settings.store_invoice_refund_first');
        $number = $this->getNextNumber($prefix, $first);

        $data = [
            'order_id' => $item->order->id,
            'num' => $number,
            // 'invoice_date' => date('Y-m-d'),
            'invoice_refund_line_items' => [
                [
                    'line_item_id' => $item->id,
                    'quantity' => $item->quantity,
                    'price' => -$item->price,
                    'tax_rate' => $item->tax_rate,
                ]
            ]
        ];

        if (!empty($prefix)) {
            $data['prefix'] = $prefix;
        }

        if ($shipping_coast) {
            $data['shipping_handling'] = $this->getShippingPrice($item) * -1;
            $data['shipping_handling_tax_rate'] = $item->order->carrier->tax ? $item->order->carrier->tax->value : 0;
            $data['shipping_handling_tax'] = $this->Orders->Carriers->Taxes->calculate($data['shipping_handling_tax_rate'], $data['shipping_handling']) * -1;
        }

        $entity = $this->newEntity($data);
        $this->save($entity);

        $entity = $this->find()
            ->where([
                'InvoiceRefunds.id' => $entity->id,
            ])
            ->contain([
                'Orders.Users',
            ])
            ->first();

        $this->getMailer('Store.Orders')->send('refundRefundedInvoice', [
            $entity,
            $item,
            trim($entity->order->order_email)
        ]);

        return $entity;
    }

    public function createFromRefund($item, $shipping_coast = false)
    {
        $prefix = Website::get('settings.store_invoice_refund_prefix');
        $first = Website::get('settings.store_invoice_refund_first');
        $number = $this->getNextNumber($prefix, $first);

        $data = [
            'order_id' => $item->order->id,
            'num' => $number,
            // 'invoice_date' => date('Y-m-d'),
        ];

        $items = [];

        foreach ($item->refunds_items as $_item) {
            if ($_item->status == StoreConfig::ITEM_STATUS_REFUNDED_ACCEPT && !$_item->refuse) {
                $items[] = [
                    'line_item_id' => $_item->line_item->id,
                    'quantity' => $_item->line_item->quantity,
                    'price' => -round($_item->line_item->price, 2),
                    'tax_rate' => $_item->line_item->tax_rate,
                ];
            }
        }

        $data ['invoice_refund_line_items'] = $items;

        if (!empty($prefix)) {
            $data['prefix'] = $prefix;
        }

        if ($shipping_coast) {
            $data['shipping_handling'] = $this->getShippingPrice($item);
            $data['shipping_handling_tax_rate'] = $item->order->carrier->tax ? $item->order->carrier->tax->value : 0;
            $data['shipping_handling_tax'] = $this->Orders->Carriers->Taxes->calculate($data['shipping_handling_tax_rate'], $data['shipping_handling']);
        }

        $entity = $this->newEntity($data);
        $this->save($entity);

        $entity = $this->find()
            ->where([
                'InvoiceRefunds.id' => $entity->id,
            ])
            ->contain([
                'Orders.Users',
            ])
            ->first();

        $this->getMailer('Store.Orders')->send('refundRefundedInvoice', [
            $entity,
            $item,
            trim($entity->order->order_email)
        ]);

        return $entity;
    }


    public function getShippingPrice($item)
    {
        $order = $this->Orders->find('order')
            ->where([
                'Orders.id' => $item->order_id,
            ])
            ->first();

        $rule = $this->Orders->Carriers->getOrderRule($order, $item->subtotal, $item->weight, null, true);

        if ($rule) {
            return $rule->price;
        }
    }

    public function getNextNumber($prefix, $first = null)
    {
        $entity = $this->find()
            ->where([
                // 'prefix' => $prefix
            ])
            ->order([
                'num' => 'desc'
            ])
            ->first();

        if (!$entity) {
            if (empty($first)) {
                $number = 1;
            } else {
                $number = $first;
            }
        } else {
            $num = $entity->num;

            if (!is_numeric($num)) {
                $num = 0;
            }

            $number = ($num + 1);
        }

        return $number;
    }
}
