<?php
namespace Store\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\Table;
use Cake\ORM\RulesChecker;
use Cake\Validation\Validator;
use Cake\Database\Schema\TableSchema;

/**
 * OrderUserActions Model
 *
 * @method \Store\Model\Entity\OrderUserAction get($primaryKey, $options = [])
 * @method \Store\Model\Entity\OrderUserAction newEntity($data = null, array $options = [])
 * @method \Store\Model\Entity\OrderUserAction[] newEntities(array $data, array $options = [])
 * @method \Store\Model\Entity\OrderUserAction|false save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \Store\Model\Entity\OrderUserAction saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \Store\Model\Entity\OrderUserAction patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \Store\Model\Entity\OrderUserAction[] patchEntities($entities, array $data, array $options = [])
 * @method \Store\Model\Entity\OrderUserAction findOrCreate($search, callable $callback = null, $options = [])
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class OrderUserActionsTable extends Table
{
    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('store_order_user_actions');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');
        
      // Behaviors
      $this->addBehavior( 'Manager.Crudable');

        // CRUD Config
        //Escribe aquí las asociaciones vinculadas con el model a la hora de editar (si las hubiera)
        // $this->crud->associations([]);

        $this->crud
          ->addFields([
            'session_id' => __d( 'admin', 'Session Id'),
            
          ])
          ->addIndex( 'index', [
            'fields' => [
              'session_id',
        
            ],
            'actionButtons' => ['create'],
            'saveButton' => false,
          ])
          ->setName( [
            'singular' => __d( 'admin', 'Acciones de usuario'),
            'plural' => __d( 'admin', 'Acciones de usuario'),
          ])
          ->addView( 'create', [
            'columns' => [
              [
                'cols' => 8,
                'box' => [
                  [
                    'elements' => [
                    'session_id',
                    ]
                  ]
                ]
              ]
            ],
            'actionButtons' => ['create', 'index']
          ], ['update'])
          ;
      
      }

  protected function _initializeSchema(TableSchema $schema)
  {
      $schema->setColumnType('errors', 'json');
      $schema->setColumnType('post', 'json');
      $schema->setColumnType('response', 'json');
      return $schema;
  }
  
  public function findFront( Query $query)
  {
    return $query
    ;
  }

}
