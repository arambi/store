<?php
namespace Store\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;
use Store\Model\Entity\CollectionsProduct;

/**
 * CollectionsProducts Model
 *
 * @property \Cake\ORM\Association\BelongsTo $Collecions
 * @property \Cake\ORM\Association\BelongsTo $ProductAttributes
 */
class CollectionsProductsTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->table('store_collections_products');
        $this->displayField('id');
        $this->primaryKey('id');

        $this->belongsTo('Collections', [
            'foreignKey' => 'collecion_id',
            'className' => 'Store.Collections'
        ]);


        $this->belongsTo('ProductAttributes', [
            'foreignKey' => 'product_id',
            'className' => 'Store.ProductAttributes'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->add('id', 'valid', ['rule' => 'numeric'])
            ->allowEmpty('id', 'create');

        return $validator;
    }

}
