<?php
namespace Store\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;
use Store\Model\Entity\Receipt;
use Website\Lib\Website;
use Store\Model\Table\InvoicesTrait;

/**
 * Receipts Model
 */
class ReceiptsTable extends Table
{ 
  use InvoicesTrait;

  public function initialize(array $config)
  {
    parent::initialize($config);
    
    $this->table('store_receipts');
    $this->displayField('id');
    $this->primaryKey('id');

    $this->addBehavior('Timestamp');
    
    // Behaviors
    $this->addBehavior( 'Manager.Crudable');
    $this->addBehavior( 'Cofree.Saltable');

    $this->belongsTo( 'Orders', [
      'className' => 'Store.Orders',
      'foreignKey' => 'order_id',
    ]);

    $this->_setCrud();

    $this->crud->setName( [
        'singular' => __d( 'admin', 'Recibos'),
        'plural' => __d( 'admin', 'Recibos'),
      ])
    ;
      
  }

  public function create( $order, $number = null)
  {
    if( $number === null)
    {
      $prefix = Website::get( 'settings.store_receipt_prefix');
      $first = Website::get( 'settings.store_receipt_first');
      $number = $this->getNextNumber( $prefix, $first);
    }

    $data = [
      'order_id' => $order->id,
      'num' => $number,
      'prefix' => $prefix,
    ];

    if( !empty( $prefix))
    {
      $data ['prefix'] = $prefix;
    }

    \Cake\Log\Log::debug( $data);
    $entity = $this->findByOrderId( $order->id)->first();

    if( $entity)
    {
      $this->patchEntity( $entity, $data);
    }
    else
    {
      $entity = $this->newEntity( $data);
    }
    

    return $this->save( $entity);    
  }

}
