<?php

namespace Store\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\ORM\TableRegistry;
use Cake\Validation\Validator;
use Store\Model\Entity\Order;

class CouponsOrdersTable extends Table
{

    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('store_coupons_orders');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');

        $this->belongsTo('Coupons', [
            'foreignKey' => 'coupon_id',
            'className' => 'Store.Coupons'
        ]);
        $this->belongsTo('Users', [
            'foreignKey' => 'user_id',
            'className' => 'Store.Users'
        ]);
        $this->belongsTo('Orders', [
            'foreignKey' => 'order_id',
            'className' => 'Store.Orders'
        ]);
    }

    public function setForOrder(Order $order)
    {
        $coupon_order = $this->find()
            ->where([
                'order_id' => $order->id,
            ])
            ->contain([
                'Coupons' => [
                    'Products',
                    'Gifts'
                ]
            ])
            ->first();

        if (!$coupon_order) {
            $order->set('coupon_discount', 0);
            $this->removeGifts($order);
            return;
        }

        if (!$this->Coupons->checkCoupon($coupon_order->coupon, $order, $order->user_id)) {
            $this->delete($coupon_order);
            $order->set('coupon_discount', 0);
            $this->removeGifts($order);
            return;
        }

        $order->set('coupon_discount', $coupon_order->coupon->discount($order));

        $this->setGifts($order, $coupon_order->coupon);
    }

    public function removeGifts($order)
    {
        $LineItems = TableRegistry::get('Store.LineItems');
        $items = $LineItems->find()
            ->where([
                'order_id' => $order->id,
                'is_gift' => true
            ]);

        foreach ($items as $item) {
            $LineItems->delete($item);
        }
    }

    public function setGifts(Order $order, $coupon)
    {
        if (!$coupon->has_gifts) {
            $this->removeGifts($order);
            return;
        }

        $LineItems = TableRegistry::get('Store.LineItems');

        foreach ($coupon->gifts as $product) {
            $item = $LineItems->find()
                ->where([
                    'order_id' => $order->id,
                    'product_id' => $product->id
                ])
                ->first();

            if (!$item) {
                $item = $LineItems->newEntity([
                    'order_id' => $order->id,
                    'product_id' => $product->id,
                    'is_gift' => true,
                    'price' => 0,
                    'quantity' => 1
                ]);

                $LineItems->save($item);
            }
        }
    }


    public function updateCoupon($order)
    {
        $coupon_order = $this->find()
            ->where([
                'order_id' => $order->id,
            ])
            ->contain([
                'Coupons'
            ])
            ->first();

        if (!$coupon_order) {
            return;
        }

        if ($coupon_order->coupon->is_giftcard) {
            $this->Coupons->query()->update()
                ->set([
                    'active' => false
                ])
                ->where([
                    'id' => $coupon_order->coupon->id
                ])
                ->execute();
        }
    }
}
