<?php

namespace Store\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\TableRegistry;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;
use Store\Model\Entity\Promo;
use Cake\Event\Event;
use Cake\Event\EventManager;
use Cake\Network\Session;
use Cake\Collection\Collection;
use Cake\Core\Configure;
use Store\Cart\Cart;
use Store\Config\StoreConfig;


/**
 * Promos Model
 */
class PromosTable extends Table
{

    public $currentPromos = false;

    public $user = false;

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        EventManager::instance()->attach($this);

        parent::initialize($config);

        $this->table('store_promos');
        $this->displayField('title');
        $this->primaryKey('id');
        $this->addBehavior('Timestamp');

        // Behaviors
        $this->addBehavior('Manager.Crudable');
        $this->addBehavior('Cofree.Saltable');
        $this->addBehavior(Configure::read('I18n.behavior'), [
            'fields' => ['title'],
            'translationTable' => 'Store.ShopI18n',
        ]);


        $this->belongsToMany('Groups', [
            'joinTable' => 'store_promos_groups',
            'through' => 'Store.PromosGroups',
            'foreignKey' => 'promo_id',
            'targetForeignKey' => 'group_id',
            'className' => 'User.Groups',
        ]);


        $this->belongsToMany('Products', [
            'joinTable' => 'store_promos_products',
            'through' => 'Store.PromosProducts',
            'foreignKey' => 'promo_id',
            'targetForeignKey' => 'product_id',
            'className' => StoreConfig::getProductsModel()->registryAlias(),
        ]);

        $this->belongsToMany('Countries', [
            'joinTable' => 'store_promos_countries',
            'foreignKey' => 'promo_id',
            'targetForeignKey' => 'country_id',
            'className' => 'Store.Countries',
        ]);

        $this->belongsToMany('States', [
            'joinTable' => 'store_promos_states',
            'foreignKey' => 'promo_id',
            'targetForeignKey' => 'state_id',
            'className' => 'Store.States',
        ]);

        // CRUD Config
        //Escribe aquí las asociaciones vinculadas con el model a la hora de editar (si las hubiera)
        $this->crud->associations(['Products', 'Groups', 'States']);

        $this->crud
            ->addFields([
                'title' => [
                    'label' => __d('admin', 'Título'),
                    'help' => __d('admin', 'Título interno de la promoción')
                ],
                'start_on' => [
                    'label' => __d('admin', 'Comienzo'),
                    'help' => __d('admin', 'Fecha de comienzo de la promoción')
                ],
                'finish_on' => [
                    'label' => __d('admin', 'Final'),
                    'help' => __d('admin', 'Fecha de finalización de la promoción')
                ],
                'active' => [
                    'label' => __d('admin', 'Activo'),
                    'type' => 'boolean',
                    'help' => __d('admin', 'Indica que la promoción está en activo')
                ],
                'discount_type' => [
                    'label' => __d('admin', 'Tipo de descuento'),
                    'type' => 'select',
                    'options' => [
                        'fixed' => __d('admin', 'Valor fijo'),
                        'percent' => __d('admin', 'Porcentaje'),
                    ],
                    'show' => 'content.promo_type != "free_delivery"',
                    'help' => __d('admin', 'Selecciona <valor fijo> para un descuento fijo sobre el producto y <porcentaje> para un porcentaje de descuento aplicado al producto')
                ],
                'value' => [
                    'label' => __d('admin', 'Valor'),
                    'show' => 'content.promo_type != "free_delivery"',
                ],
                'groups' => [
                    'type' => 'select_multiple',
                    'label' => __d('admin', 'Grupos de usuarios'),
                    'help' => __d('admin', 'Indica los grupos de usuario que podrán optar a las ofertas de esta promoción')
                ],
                'groups_exclude' => [
                    'label' => 'Excluir los grupos seleccionados',
                    'show' => "content.groups.length > 0"
                ],
                'promo_type' => [
                    'label' => __d('admin', 'Tipo de promocion'),
                    'type' => 'select',
                    'options' => $this->getPromoTypes(),
                    'help' => __d('admin', 'Indica a qué productos irá destinada la oferta.')
                ],
                'with_price_min' => [
                    'label' => __d('admin', 'Solo para pedidos con un mínimo'),
                    'show' => 'content.promo_type == "cart" || content.promo_type == "free_delivery"'
                ],
                'price_min' => [
                    'label' => __d('admin', 'Precio mínimo'),
                    'show' => 'content.with_price_min && (content.promo_type == "cart" || content.promo_type == "free_delivery")'
                ],
                'products' => [
                    'type' => 'autocomplete',
                    'label' => __d('admin', 'Productos'),
                    'show' => "content.promo_type == 'products'",
                    'help' => __d('admin', 'Escribe los productos que quieres que estén asociados a esta promoción.'),
                ],
                'products_exclude' => [
                    'label' => 'Excluir los productos seleccionados',
                    'show' => "content.promo_type == 'products' && content.products.length > 0"
                ],
                'countries' => [
                    'type' => 'select_multiple',
                    'label' => __d('admin', 'Países'),
                    'help' => __d('admin', 'Escribe los países que quieres que estén asociados a esta oferta.')
                ],
                'states' => [
                    'type' => 'autocomplete',
                    'label' => __d('admin', 'Provincias'),
                    'help' => __d('admin', 'Escribe las provincias que quieres que estén asociadas a esta oferta.')
                ],
                'states_exclude' => [
                    'label' => 'Excluir las provincias seleccionados',
                    'show' => "content.states.length > 0"
                ],
            ])
            ->addIndex('index', [
                'fields' => [
                    'title',
                    'start_on',
                    'finish_on',
                    'active',
                    'promo_type',

                ],
                'actionButtons' => ['create'],
                'saveButton' => false,
            ])
            ->setName([
                'singular' => __d('admin', 'Promociones'),
                'plural' => __d('admin', 'Promociones'),
            ])
            ->addView('create', [
                'columns' => [
                    [
                        'key' => 'general',
                        'cols' => 8,
                        'box' => [
                            [
                                'elements' => [
                                    'title',
                                    'start_on',
                                    'finish_on',
                                    'active',
                                    'promo_type',
                                    'discount_type',
                                    'value',
                                    'with_price_min',
                                    'price_min',
                                ]
                            ],
                            [
                                'title' => __d('admin', 'Restricción por tipo de usuario'),
                                'elements' => [
                                    'groups',
                                    'groups_exclude',
                                ]
                            ],
                            [
                                'title' => __d('admin', 'Restricción por producto'),
                                'elements' => [
                                    'products',
                                    'products_exclude',
                                ]
                            ],
                            [
                                'title' => __d('admin', 'Restricción por zona geográfica'),
                                'elements' => [
                                    // 'countries',
                                    'states',
                                    'states_exclude',
                                ]
                            ]
                        ]
                    ]
                ],
                'actionButtons' => ['create', 'index']
            ], ['update']);

        $this->crud->order(['Promos.finish_on' => 'desc']);
    }

    private function getPromoTypes()
    {
        $out = [
            'global' => __d('admin', 'Para todos los productos'),
            'products' => __d('admin', 'Para productos concretos'),
        ];

        if (StoreConfig::getConfig('promoTypeCart')) {
            $out['cart'] = __d('admin', 'Para el carro de la compra');
        }

        if (StoreConfig::getConfig('promoTypeShipping')) {
            $out['free_delivery'] = __d('admin', 'Portes gratis');
        }

        return $out;
    }

    /**
     * Toma las promociones activas
     * 
     * @return array
     */
    public function user()
    {
        if ($this->user === false) {
            if (isset($_SESSION['Auth']['User'])) {
                $this->user = $_SESSION['Auth']['User'];
                $this->addAddressDelivery();
            }

            if (Configure::read('OrderUser')) {
                $this->user = Configure::read('OrderUser');
                $this->addAddressDelivery();
            }
        }

        return $this->user;
    }

    private function addAddressDelivery()
    {
        if (!$this->user) {
            return;
        }

        $address = TableRegistry::get('Store.Addresses')->find()
            ->where([
                'Addresses.user_id' => $this->user['id'],
                'Addresses.type' => 'delivery'
            ])
            ->first();

        if ($address) {
            $this->user['address_delivery'] = $address->toArray();
        }
    }

    public function actives($conditions = [])
    {
        if ($this->currentPromos === false) {
            $this->hasMany('PromosProducts', [
                'className' => 'Store.PromosProducts',
                'foreignKey' => 'promo_id',
            ]);

            $this->hasMany('PromosGroups', [
                'className' => 'Store.PromosGroups',
                'foreignKey' => 'promo_id',
            ]);

            $query = $this->find()
                ->where([
                    'Promos.start_on <=' => date('Y-m-d'),
                    'Promos.finish_on >='  => date('Y-m-d'),
                    'Promos.active' => true,
                ])
                ->contain([
                    'PromosGroups',
                    'PromosProducts',
                    'States'
                ]);

            $event = new Event('Store.Model.Promos.beforeFind', $this, [$query]);
            EventManager::instance()->dispatch($event);

            if (!empty($conditions)) {
                $query->where($conditions);
            }

            $promos = $query->all();

            $this->currentPromos = new Collection($promos);
        }

        return $this->currentPromos;
    }

    public function activesForUser($group_id)
    {
        $this->hasOne('PromosGroups', [
            'className' => 'Store.PromosGroups',
            'foreignKey' => 'promo_id',
        ]);


        $promos = $this->find()
            ->where([
                'Promos.start_on <= NOW()',
                'Promos.finish_on >= NOW()',
                'Promos.active' => true,
                'PromosGroups.group_id' => $group_id,
            ])
            ->contain([
                'PromosGroups',
                'States'
            ])
            ->all();

        return $promos;
    }

    /**
     * Comprueba los productos que tienen promoción
     * 
     * @param  Event $event
     * @param  Store\Model\entityProduct $row
     * @return [type]        [description]
     */
    public function afterFindProduct($event, $row)
    {
        // Promociones activas
        $promos = $this->actives();
        // Usuario actual
        $user = $this->user();

        $this->onProduct($row, $promos, $user);

        if (isset($row->product_attributes)) {
            foreach ($row->product_attributes as $pa) {
                $this->onProduct($pa, $promos, $user, true);
            }
        }
    }

    public function onProduct($row, $promos, $user, $isProductAttribute = false)
    {
        // El array donde se guardarán las promos que vayan asociadas al producto
        $discounts = new \ArrayObject;
        // Itera cada promo
        // Guarda en el array discounts, los descuentos aplicados para el producto
        $promos->each(function ($promo) use ($row, $user, $discounts, $isProductAttribute) {
            if (!in_array($promo->promo_type, ['global', 'products'])) {
                return;
            }

            $isAllow = $this->checkUser($promo, $user);

            if (!$isAllow->allow) {
                return;
            }

            $method = 'check' . ucfirst($promo->promo_type);

            if ($method == 'checkProducts' && $isProductAttribute) {
                $method = 'checkProductAttributes';
            }

            $discount = $this->$method($promo, $row, $user);
            
            // Si la promo no es false, se añade al array
            if ($discount) {
                $discounts[] = $discount;
            } else {
            }
        });

        $discounts = (array)$discounts;


        if (empty($discounts)) {
            return;
        }

        // Toma la mejor promoción de todas las que se han producido
        $amount = $this->getGoodPromo($discounts, $row);

        if (!is_object($row)) {
            return $row;
        }

        // Setea el precio de oferta
        $row->set('offer_price', $row->store_price - $amount);

        if (!empty($row->store_price)) {
            $row->set('offer_percent', round((100 * $amount) / $row->store_price));
        }

        if (!empty($row->product_attributes)) {
            foreach ($row->product_attributes as $pa) {
                // Setea el precio de oferta
                $pa->set('offer_price', $pa->store_price - $amount);

                if (!empty($pa->store_price)) {
                    $pa->set('offer_percent', round((100 * $amount) / $pa->store_price));
                }

                $pa->set('discount', $amount);
            }
        }


        $row->set('discount', $amount);
        return $row;
    }

    public function getGoodPromo($promos, $row)
    {
        $results = [];

        foreach ($promos as $promo) {
            $results[$promo->id] = $this->applyDiscount($promo, $row);
        }

        sort($results);
        $results = array_reverse($results);
        return $results[0];
    }

    public function applyDiscount($promo, $row)
    {
        if ($promo->discount_type == 'fixed') {
            $tax = $row->tax->value;
            $price = $row->realPrice($row->store_price, $tax);
            $discount_price = $price - $promo->value;
            $tax_value = $discount_price / (1 + $tax / 100);
            return $row->store_price - $tax_value;
        }

        if (!is_object($row)) {
            return;
        }

        $value = ($row->store_price * $promo->value) / 100;
        return $value;
    }

    public function checkGlobal($promo, $row, $user)
    {
        $hasPromo = new \stdClass();
        $hasPromo->allow = true;
        $event = new Event('Store.Model.Promos.checkGlobal', $this, [$promo, $row, $user, $hasPromo]);
        EventManager::instance()->dispatch($event);

        if (!empty($hasPromo->allow)) {
            return $promo;
        }

        return false;
    }

    public function checkUser($promo, $user)
    {
        $isAllow = new \stdClass();
        $isAllow->allow = true;

        if (!empty($promo->promos_groups)) {
            if (!$user) {
                $isAllow->allow = false;
            }

            $group_ids = collection($promo->promos_groups)->extract('group_id')->toArray();

            if ($promo->groups_exclude) {
                if (in_array($user['group_id'], $group_ids)) {
                    $isAllow->allow = false;
                }
            } else {
                if (!in_array($user['group_id'], $group_ids)) {
                    $isAllow->allow = false;
                }
            }
        }

        // Provincias
        $this->checkStates($promo, $isAllow);

        $event = new Event('Store.Model.Promos.checkUser', $this, [$isAllow, $promo, $user]);
        EventManager::instance()->dispatch($event);
        return $isAllow;
    }

    private function checkStates($promo, $isAllow)
    {
        if (empty($promo->states)) {
            return;
        }

        $state_delivery_id = Cart::getStateDeliveryId();

        if ($state_delivery_id) {
            $state_id = $state_delivery_id;
        } elseif (!empty($this->user['address_delivery'])) {
            $state_id = $this->user['address_delivery']['state_id'];
        }

        if (empty($state_id)) {
            $isAllow->allow = false;
            return;
        }

        $state_ids = collection($promo->states)->extract('id')->toArray();
        if ($promo->states_exclude && in_array($state_id, $state_ids)) {
            $isAllow->allow = false;
        }

        if (!$promo->states_exclude && !in_array($state_id, $state_ids)) {
            $isAllow->allow = false;
        }
    }


    /**
     * Chequea que el producto pasado esté dentro de la oferta pasada
     * 
     * @param  Store\Model\Entity\Promo        $promo
     * @param  Store\Model\Entity\Product      $row
     * @param  User\Model\Entity\User         $user
     * @return
     */
    public function checkProducts($promo, $row, $user)
    {
        if (!is_object($row)) {
            return false;
        }

        $product_ids = (collection($promo->promos_products))->extract('product_id')->toArray();


        if (!$promo->products_exclude && in_array($row->id, $product_ids)) {
            return $promo;
        }

        if ($promo->products_exclude && !in_array($row->id, $product_ids)) {
            return $promo;
        }

        return false;
    }

    /**
     * Chequea que el producto-atributo pasado esté dentro de la oferta pasada
     * 
     * @param  Store\Model\Entity\Promo              $promo
     * @param  Store\Model\Entity\ProductAttribute   $row
     * @param  User\Model\Entity\User               $user
     * @return
     */
    public function checkProductAttributes($promo, $row, $user)
    {
        // $attribute_ids = (new Collection( $promo->products_colors))->extract( 'attribute_id')->toArray();

        // Itera los atributos del product_attribute
        foreach ($row->attributes as $attr) {
            // Itera el product_colors de la oferta

        }

        return false;
    }


    public function conditionsForSearchProducts($promo, $query)
    {
        $method = 'conditions' . ucfirst($promo->promo_type);
        return $this->$method($promo, $query);
    }

    public function conditionsUser($promo, $query)
    {
    }


    public function conditionsGlobal($promo, $query)
    {
        $event = new Event('Store.Model.Promos.conditionsGlobal', $this, [$promo, $query]);
        EventManager::instance()->dispatch($event);
        return $query;
    }

    public function conditionsProducts($promo, $query)
    {
        $product_ids = $products_colors_ids = [];

        foreach ($promo->products_colors as $product) {
            $products_colors_ids[] = $product->id;
        }

        foreach ($promo->promos_products_products as $product) {
            $product_ids[] = $product->product_id;
        }

        return [
            'product_ids' => $product_ids
        ];
    }

    public function beforeSetTotal($event, $order)
    {
        $order->set('promo_discount', 0);
        $this->checkForCart($order);
        $this->checkForDelivery($order);
    }

    private function checkForCart($order)
    {
        $promos = $this->actives();
        $user = $this->user();
        $valids = new \ArrayObject;
        $promos->each(function ($promo) use ($order, $user, $valids) {
            if ($promo->promo_type != 'cart') {
                return;
            }

            $isAllow = $this->checkUser($promo, $user);

            if (!$isAllow->allow) {
                return;
            }

            $amount = $order->subtotal + $order->line_items_taxes;

            if ($promo->with_price_min && $amount < $promo->price_min) {
                return;
            }

            if ($promo->discount_type == 'percent') {
                $discount = ($amount * $promo->value) / 100;
            } else {
                $discount = $promo->value;
            }

            $promo->set('promo_discount', $discount);
            $valids[] = $promo;
        });

        $valids = (array)$valids;

        if (count($valids) == 0) {
            return;
        }

        $promo = null;

        foreach ($valids as $_promo) {
            if (!$promo || $_promo->promo_discount > $promo->promo_discount) {
                $promo = $_promo;
            }
        }

        if (!$promo) {
            return;
        }

        $order->set('promo_id', $promo->id);
        $order->set('promo_type', $promo->promo_type);
        $order->set('promo_discount', $order->promo_discount + $promo->promo_discount);
    }

    private function checkForDelivery($order)
    {
        $promos = $this->actives();
        $user = $this->user();
        $valids = new \ArrayObject;
        $promos->each(function ($promo) use ($order, $user, $valids) {
            if ($promo->promo_type != 'free_delivery') {
                return;
            }

            $isAllow = $this->checkUser($promo, $user);

            if (!$isAllow->allow) {
                return;
            }

            $amount = $order->subtotal + $order->line_items_taxes;

            if ($promo->with_price_min && $amount < $promo->price_min) {
                return;
            }

            $promo->set('promo_discount', $order->shipping_handling + $order->shipping_handling_tax);
            $valids[] = $promo;
        });

        $valids = (array)$valids;

        if (count($valids) == 0) {
            return;
        }

        $promo = null;

        foreach ($valids as $_promo) {
            if (!$promo || $_promo->promo_discount > $promo->promo_discount) {
                $promo = $_promo;
            }
        }

        if (!$promo) {
            return;
        }

        $order->set('promo_id', $promo->id);
        $order->set('promo_type', $promo->promo_type);
        $order->set('promo_discount', $order->promo_discount + $promo->promo_discount);
    }

    public function implementedEvents()
    {
        $eventMap = [
            'Store.Model.Products.afterFind' => 'afterFindProduct',
            'Store.Model.Order.beforeSetTotal' => 'beforeSetTotal'
        ];

        return $eventMap;
    }
}
