<?php

namespace Store\Model\Table;

use ArrayObject;
use I18n\Lib\Lang;
use Cake\ORM\Query;
use Cake\ORM\Table;
use Store\Cart\Cart;
use Cake\Event\Event;
use Website\Lib\Website;
use Store\Cart\ItemDraft;
use Cake\ORM\RulesChecker;
use Cake\ORM\TableRegistry;
use Cake\Utility\Inflector;
use Store\Config\StoreConfig;
use Store\Model\Entity\Order;
use Cake\Validation\Validator;
use Cake\Validation\Validation;
use Letter\Mailer\MailerAwareTrait;
use Cake\Datasource\EntityInterface;
use Cake\Database\Schema\TableSchema;
use Store\Currency\CurrencyCollection;

/**
 * Orders Model
 */
class OrdersTable extends Table
{
    use MailerAwareTrait;

    public $statuses;

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        $this->setTable('store_orders');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        // Behaviors
        $this->addBehavior('Timestamp');
        $this->addBehavior('Manager.Crudable');
        $this->addBehavior('Cofree.Saltable');

        $this->belongsTo('Users', [
            'className' => 'User.Users',
            'foreignKey' => 'user_id'
        ]);

        $this->belongsTo('Carriers', [
            'className' => 'Store.Carriers',
            'foreignKey' => 'carrier_id'
        ]);

        $this->belongsTo('PaymentMethods', [
            'className' => 'Store.PaymentMethods',
            'foreignKey' => 'payment_method_id',
            'propertyName' => 'payment_method'
        ]);

        $this->belongsTo('CountryDelivery', [
            'className' => 'Store.Countries',
            'foreignKey' => 'adr_delivery_country_id',
        ]);

        $this->belongsTo('CountryInvoice', [
            'className' => 'Store.Countries',
            'foreignKey' => 'adr_invoice_country_id',
        ]);

        $this->belongsTo('StateDelivery', [
            'className' => 'Store.States',
            'foreignKey' => 'adr_delivery_state_id',
        ]);

        $this->belongsTo('StateInvoice', [
            'className' => 'Store.States',
            'foreignKey' => 'adr_invoice_state_id',
        ]);



        $this->hasMany('LineItems', [
            'className' => 'Store.LineItems',
            'foreignKey' => 'order_id'
        ]);

        $this->hasMany('OrderMessages', [
            'className' => 'Store.OrderMessages',
            'foreignKey' => 'order_id'
        ]);

        $this->hasMany('OrderHistories', [
            'className' => 'Store.OrderHistories',
            'foreignKey' => 'order_id',
            'sort' => [
                'OrderHistories.created' => 'desc'
            ]
        ]);

        $this->hasOne('PointsDown', [
            'className' => 'Store.Points',
            'foreignKey' => 'order_id',
            'conditions' => [
                'PointsDown.action' => 'down'
            ],
        ]);

        $this->hasOne('PointsUp', [
            'className' => 'Store.Points',
            'foreignKey' => 'order_id',
            'conditions' => [
                'PointsUp.action' => 'up'
            ],
        ]);

        $this->hasOne('Invoices', [
            'className' => 'Store.Invoices',
            'foreignKey' => 'order_id',
        ]);

        $this->hasOne('Receipts', [
            'className' => 'Store.Receipts',
            'foreignKey' => 'order_id',
        ]);

        $this->hasOne('Payments', [
            'className' => 'Payment.Payments',
            'foreignKey' => 'order_id',
            'conditions' => [
                'Payments.model' => 'Store.Orders',
                'Payments.success' => 1
            ]
        ]);

        $this->setStatuses();

        $this->crud->associations([
            'CountryInvoice',
            'CountryDelivery',
            'StateDelivery',
            'StateInvoice',
            'Users',
            'Carriers',
            'PointsDown',
            'PaymentMethods',
            'LineItems',
            'OrderMessages',
            'OrderHistories',
            'Invoices',
            'Receipts'
        ]);

        $this->crud->addJsFiles([
            '/store/js/directives.js',
        ]);

        // Index fields
        $index_fields = [
            'id',
            'order_date',
            'user_id',
            'adr_invoice_firstname'
        ];

        if (!StoreConfig::getConfig('onlyFirstname')) {
            $index_fields[] = 'adr_invoice_lastname';
        }

        $index_fields[] = 'discount_price_human';
        $index_fields[] = 'shipping_handling_human';
        $index_fields[] = 'order_date';
        $index_fields[] = 'total_human';
        $index_fields[] = 'items_descriptions';
        $index_fields[] = 'status';
        $index_fields = new ArrayObject($index_fields);

        $event = new Event('Store.Model.Order.indexFields', $this, [
            $index_fields,
        ]);

        $this->getEventManager()->dispatch($event);
        // $index_fields = (array)$index_fields;

        $unfinish_index = $index_fields;
        unset($unfinish_index[array_search('order_date', (array)$unfinish_index)]);
        $unfinish_index['created'] = [
            'class' => 'index-small',
            'label' => 'Fecha',
            'templateIndex' => 'Manager.indexes/datetime'
        ];

        $abandoned_index = $index_fields;
        unset($abandoned_index[array_search('order_date', (array)$abandoned_index)]);
        $abandoned_index['created'] = [
            'class' => 'index-small',
            'label' => 'Fecha',
            'templateIndex' => 'Manager.indexes/datetime'
        ];

        $this->crud
            ->addFields([
                'id' => 'ID',
                'user_id' => 'ID Cliente',
                'user' => [
                    'label' => __d('admin', 'Usuario'),
                    'type' => 'belongsTo'
                ],
                'discount_price_human' => [
                    'label' => __d('admin', 'Descuento'),
                    'templateSearch' => 'Manager.searches/empty',
                    'type' => 'string'
                ],
                'order_date' => [
                    'class' => 'index-small',
                    'label' => 'Fecha',
                    'templateIndex' => 'Manager.indexes/datetime'
                ],
                'shipping_handling_human' => [
                    'label' => __d('admin', 'Envío'),
                    'templateSearch' => 'Manager.searches/empty',
                    'type' => 'string',
                ],
                'items_descriptions' => [
                    'label' => __d('admin', 'Prods'),
                    'type' => 'string'
                ],
                'status' => [
                    'templateIndex' => 'Store.indexes/status',
                    'templateSearch' => 'Store.searches/status',
                    'type' => 'select',
                    'options' => $this->statuses,
                    'label' => 'Estado'
                ],
                'total_human' => [
                    'label' => __d('admin', 'Total'),
                    'type' => 'string'
                ],
                'total_items' => [
                    'label' => __d('admin', 'Productos'),
                    'type' => 'string'
                ],
                'status_human' => [
                    'label' => __d('admin', 'Estado'),
                    'type' => 'string'
                ],
                'order_date_human' => [
                    'label' => __d('admin', 'Fecha'),
                    'type' => 'string'
                ],
                'comments' => __d('admin', 'Comentarios del usuario'),
                'store_comments' => __d('admin', 'Observaciones'),
                'adr_invoice_firstname' => !StoreConfig::getConfig('onlyFirstname') ? __d('admin', 'Nombre') : __d('admin', 'Nombre y apellidos'),
                'adr_delivery_firstname' => !StoreConfig::getConfig('onlyFirstname') ? __d('admin', 'Nombre') : __d('admin', 'Nombre y apellidos'),
                'adr_invoice_lastname' => __d('admin', 'Apellidos'),
                'adr_delivery_lastname' => __d('admin', 'Apellidos'),
                'adr_invoice_vat_number' => __d('admin', 'NIF/CIF'),
                'adr_delivery_vat_number' => __d('admin', 'NIF/CIF'),
                'adr_invoice_postcode' => __d('admin', 'Código postal'),
                'adr_delivery_postcode' => __d('admin', 'Código postal'),
                'adr_invoice_company' => __d('admin', 'Empresa'),
                'adr_delivery_company' => __d('admin', 'Empresa'),
                'adr_invoice_email' => __d('admin', 'Email'),
                'adr_invoice_phone' => __d('admin', 'Teléfono'),
                'adr_delivery_phone' => __d('admin', 'Teléfono'),
                'adr_invoice_city' => __d('admin', 'Población'),
                'adr_delivery_city' => __d('admin', 'Población'),
                'adr_invoice_address' => __d('admin', 'Dirección'),
                'adr_delivery_address' => __d('admin', 'Dirección'),
                'adr_invoice_country_id' => [
                    'label' => __d('admin', 'País'),
                    'type' => 'select',
                    'options' => function ($crud) {
                        return TableRegistry::getTableLocator()->get('Store.Countries')
                            ->find('list')
                            ->where([
                                'active' => true
                            ]);
                    },
                    'change' => "\$http.post( '/admin/store/orders/states.json', {country_id: element.val()}).success(function( r){ scope.crudConfig.view.fields.adr_invoice_state_id.options = r.states})"
                ],
                'adr_delivery_country_id' => [
                    'label' => __d('admin', 'País'),
                    'type' => 'select',
                    'options' => function ($crud) {
                        return TableRegistry::getTableLocator()->get('Store.Countries')
                            ->find('list')
                            ->where([
                                'active' => true
                            ]);
                    },
                    'change' => "\$http.post( '/admin/store/orders/states.json', {country_id: element.val()}).success(function( r){ scope.crudConfig.view.fields.adr_delivery_state_id.options = r.states})"
                ],
                'adr_invoice_state_id' => [
                    'label' => __d('admin', 'Provincia'),
                    'type' => 'select',
                    'options' => function ($crud) {
                        return TableRegistry::getTableLocator()->get('Store.States')
                            ->find('list')
                            ->where([
                                'country_id' => $crud->getContent()['adr_invoice_country_id'],
                                'active' => true
                            ]);
                    },
                ],
                'adr_delivery_state_id' => [
                    'label' => __d('admin', 'Provincia'),
                    'type' => 'select',
                    'options' => function ($crud) {
                        return TableRegistry::getTableLocator()->get('Store.States')
                            ->find('list')
                            ->where([
                                'country_id' => $crud->getContent()['adr_delivery_country_id'],
                                'active' => true
                            ]);
                    },
                ],
                'carrier' => 'Transporte'
            ])
            ->addIndex('index', [
                'fields' => $index_fields,
                'actionButtons' => [],
                'saveButton' => false,
                'actionLink' => 'view',
                'filters' => [
                    'items_descriptions'
                ]
            ])
            ->addIndex('unfinish', [
                'template' => 'Manager/index',
                'fields' => $unfinish_index,
                'actionButtons' => [],
                'saveButton' => false,
                'actionLink' => 'cart',
            ])
            ->addIndex('abandoned', [
                'template' => 'Manager/index',
                'fields' => $abandoned_index,
                'actionButtons' => [],
                'saveButton' => false,
                'actionLink' => 'abandoned_cart',
            ])
            ->setName([
                'singular' => __d('admin', 'Pedido'),
                'plural' => __d('admin', 'Pedidos'),
            ])
            ->addView('view', [
                'template' => 'Store/order',
                'columns' => [
                    [
                        'cols' => 8,
                        'box' => [
                            [
                                'elements' => []
                            ],
                        ],
                    ]
                ],
                'actionButtons' => ['index']
            ])
            ->addView('cart', [
                'template' => 'Store/order',
                'saveButton' => false,
                'columns' => [
                    [
                        'cols' => 8,
                        'box' => [
                            [
                                'elements' => []
                            ],
                        ],
                    ]
                ],
                'actionButtons' => ['index' => [
                    'url' => '#admin/store/orders/unfinish',
                    'label' => 'Listado'
                ]]
            ])
            ->addView('abandoned_cart', [
                'template' => 'Store/order',
                'saveButton' => false,
                'columns' => [
                    [
                        'cols' => 8,
                        'box' => [
                            [
                                'elements' => []
                            ],
                        ],
                    ]
                ],
                'actionButtons' => ['index' => [
                    'url' => '#admin/store/orders/abandoned',
                    'label' => 'Listado'
                ]]
            ])
            ->addView('statistics', [
                'template' => 'Store/statistics',
                'title' => __d('admin', 'Estadísticas'),
                'saveButton' => false,
                'columns' => [
                    [
                        'cols' => 8,
                        'box' => [
                            [
                                'elements' => []
                            ],
                        ],
                    ]
                ]
            ])
            ->addFilters([
                'items_descriptions' => [
                    'filter' =>  function ($table, $query, $param) {
                        $this->hasOne( 'Item', [
                          'className' => 'Store.LineItems',
                          'foreignKey' => 'order_id',
                        ]);

                        $query->group(['Orders.id']);
                        $query->contain('Item');
                        $query->where([
                            'Item.title LIKE' => "%$param%"
                        ]);
                    },
                    'type' => 'string',
                    'label' => 'Productos'
                ],
            ])
            ->order([
                'Orders.order_date' => 'desc'
            ]);

        $this->crud->getView('index')
            ->set('exportCsv', true)
            ->set('csvFields', [
                'id' => 'ID',
                'order_date' => 'Fecha',
                'adr_invoice_firstname' => 'Nombre',
                'adr_invoice_lastname' => 'Apellidos',
                'order_email' => 'Email',
                'count_items' => 'Nº productos',
                'items_descriptions' => 'Productos',
                'total_without_taxes_human_no_tags' => 'Base Imponible',
                'total_taxes_human_no_tags' => 'Impuestos',
                'total_human_no_tags' => 'Total',
            ]);
    }

    public function beforeSave(Event $event, EntityInterface $entity, ArrayObject $options)
    {
        $event = new Event('Store.Model.Order.beforeSave', $this, [
            $entity,
        ]);

        $this->getEventManager()->dispatch($event);
    }

    public function saveFields($order, $data)
    {
        $fields = [
            'adr_delivery_country_id',
            'adr_delivery_state_id',
            'adr_invoice_country_id',
            'adr_invoice_state_id',
        ];

        $query = $this->query();

        foreach ($fields as $field) {
            if (!empty($data[$field])) {
                $query->update()
                    ->set([$field => $data[$field]])
                    ->where(['id' => $order->id])
                    ->execute();
            }
        }
    }

    public function setStatuses()
    {
        $statuses = StoreConfig::getStatuses();
        $store_statuses = StoreConfig::getConfig('statuses');
        $this->statuses = array_filter($statuses, function ($key) use ($store_statuses) {
            return in_array($key, $store_statuses);
        }, ARRAY_FILTER_USE_KEY);
    }

    protected function _initializeSchema(TableSchema $schema)
    {
        $schema->setColumnType('same_address', 'integer');
        $schema->setColumnType('correos_track_data', 'json');
        $schema->setColumnType('metadata', 'json');
        return $schema;
    }

    /**
     * Devuelve un order dado un salt
     * 
     * @param  string
     * @return Order
     */
    public function getOrder($conditions, $build_items_price = true)
    {
        $query = $this->find();
        $query->contain([
            'LineItems' => function ($q) {
                $q->contain([
                    'Products' => function ($q) {
                        $q->find('products');
                        $q->getRepository()->setAlias('Products');
                        $q->contain([
                            'Taxes',
                        ]);

                        if (StoreConfig::getConfig('withAttributes')) {
                            $q->contain([
                                'ProductAttributes.Attributes',
                            ]);

                            $this->LineItems->Products->setAttributesPrice($q);
                        }


                        if (method_exists($this->LineItems->Products->target(), 'containOrder')) {
                            $this->LineItems->Products->target()->containOrder($q);
                        }

                        return $q;
                    }
                ]);

                if (StoreConfig::getConfig('withAttributes')) {
                    $q->contain([
                        'ProductAttributes' => [
                            'Attributes'
                        ],
                    ]);
                }

                return $q;
            },
            'Carriers' => [
                'Taxes'
            ],
            'PaymentMethods' => [
                'Taxes'
            ],
            'Users',
            'OrderMessages',
            'OrderHistories',
            'StateInvoice',
            'StateDelivery',
            'CountryInvoice',
            'CountryDelivery',
            'PointsDown',
            'PointsUp',
            'Payments',
        ]);

        $query->where($conditions);
        $query->order([
            'Orders.modified' => 'desc'
        ]);

        $event = new Event('Store.Model.Order.getOrder', $this, [
            $query,
        ]);

        $this->getEventManager()->dispatch($event);

        $order = $query->first();

        if ($order) {
            $this->build($order, $build_items_price);
        }

        return $order;
    }


    public function findOrder(Query $query, array $options = [])
    {
        $query->contain([
            'LineItems' => function ($q) {
                $q->contain([
                    'Products' => function ($q) {
                        $q->find('products');
                        $q->getRepository()->setAlias('Products');
                        $q->contain([
                            'Taxes',
                        ]);

                        $ProductsTable = StoreConfig::getProductsModel();

                        if ($ProductsTable->hasBehavior('Deletable')) {
                            $q->find('regardless');
                        }

                        if (StoreConfig::getConfig('withAttributes')) {
                            $q->contain([
                                'ProductAttributes.Attributes',
                            ]);

                            $this->LineItems->Products->setAttributesPrice($q);
                        }


                        if (method_exists($this->LineItems->Products->target(), 'containOrder')) {
                            $this->LineItems->Products->target()->containOrder($q);
                        }

                        return $q;
                    }
                ]);

                if (StoreConfig::getConfig('withAttributes')) {
                    $q->contain([
                        'ProductAttributes' => [
                            'Attributes'
                        ],
                    ]);
                }

                return $q;
            },
            'Carriers' => [
                'Taxes'
            ],
            'PaymentMethods' => [
                'Taxes'
            ],
            'OrderMessages',
            'OrderHistories',
            'StateInvoice',
            'StateDelivery',
            'CountryInvoice',
            'CountryDelivery',
            'PointsDown',
            'PointsUp',
            'Invoices',
            'Receipts',
            'Users' => [
                'Groups'
            ]
        ]);

        if (!array_key_exists('noPayment', $options)) {
            $query->contain('Payments');
        }

        $event = new Event('Store.Model.Order.findOrder', $this, [
            $query,
        ]);

        $this->getEventManager()->dispatch($event);


        return $query;
    }


    public function findCompleted()
    {
        return $this->find('order')
            ->where([
                'Orders.status IN' => [
                    'accepted',
                    'envoy',
                    'received',
                ]
            ]);
    }

    public function findAddresses(Query $query, array $options)
    {
        $user_id = $options['user_id'];

        $query
            ->where([
                'Orders.user_id' => $user_id,
                // 'YEAR(`order_date`) = YEAR(DATE_SUB(CURDATE(), INTERVAL 1 YEAR))'
            ])
            ->contain([
                'StateInvoice',
                'CountryInvoice',
                'StateDelivery',
                'CountryDelivery',
            ]);

        $query->formatResults(function ($result) {
            $__addresses = [];
            return $result->map(function ($row) use ($__addresses) {
                $addresses = [];

                if (!in_array($row->adr_invoice_address, $__addresses)) {
                    $__addresses[] = $row->adr_invoice_address;
                    $addresses[] = $row->pretty_invoice_address;
                }

                if (!in_array($row->adr_delivery_address, $__addresses)) {
                    $__addresses[] = $row->adr_delivery_address;
                    $addresses[] = $row->pretty_delivery_address;
                }

                if (!empty($addresses)) {
                    $row->set('addresses', $addresses);
                    return $row;
                }
            });
        });

        return $query;
    }


    public function getAddresses($user_id, $callback = null)
    {
        $query = $this->find('addresses', ['user_id' => $user_id]);
        $return = [];

        if ($callback) {
            $callback($query);
        }

        foreach ($query as $content) {
            foreach ($content->addresses as $address) {
                if (!in_array($address, $return)) {
                    $return[] = $address;
                }
            }
        }

        return $return;
    }



    public function createOrder($data = [])
    {
        $order = $this->newEntity($data);
        $order->set('locale', Lang::current('iso3'));
        $order->set('currency', CurrencyCollection::getDefault());
        $this->setAddressesToNewOrder($order);

        // Event
        $event = new Event('Store.Model.Order.createOrder', $this, [
            'order' => $order,
        ]);
        $this->getEventManager()->dispatch($event);

        $this->save($order);

        // Event
        $event = new Event('Store.Model.Order.afterCreateOrder', $this, [
            'order' => $order,
        ]);

        $this->getEventManager()->dispatch($event);
        return $order;
    }

    public function setAddressesToNewOrder($order)
    {
        if (empty($order->user_id)) {
            return;
        }

        foreach (['invoice', 'delivery'] as $type) {
            $address = TableRegistry::getTableLocator()->get('Store.Addresses')->find()
                ->where([
                    'Addresses.user_id' => $order->user_id,
                    'Addresses.type' => $type
                ])
                ->first();

            if ($address) {
                $this->copyAddressToOrder($address, $type, $order, true, true);
            }
        }
    }

    public function copyAddressToOrder($address, $type, $order, $force = false, $save = false)
    {
        $user = TableRegistry::get('User.Users')->find()
            ->where([
                'Users.id' => $order->id,
            ])
            ->first();


        $fields = [
            'firstname',
            'lastname',
            'company',
            'address',
            'country_id',
            'state_id',
            'city',
            'phone',
            'postcode',
            'email',
        ];

        foreach ($fields as $field) {
            $prop = 'adr_' . $type . '_' . $field;

            if (empty($order->$prop) || $force) {
                $order->set($prop, $address->$field);

                if ($save && $this->hasField($prop)) {
                    $this->query()->update()
                        ->set([
                            $prop => $address->$field
                        ])
                        ->where([
                            'id' => $order->id
                        ])
                        ->execute();
                }
            }
        }
    }



    public function saveOrder($order)
    {
        return $this->save($order, ['associated' => false]);
    }

    /**
     * Construye un order
     * Coloca los datos de los items y los totales
     * 
     * @param  Order $order
     * @return void
     */
    public function build($order, $build_items_price = true)
    {
        $event = new Event('Store.Model.Order.beforeBuild', $this, [
            'order' => $order,
        ]);
        $this->getEventManager()->dispatch($event);
        Cart::setOrder($order);
        $this->buildItems($order->line_items, $build_items_price);
        $this->buildTotals($order);
        $this->buildDeliveryDate($order);
        Cart::setOrder($order);

        // Event
        $event = new Event('Store.Model.Order.afterBuild', $this, [
            'order' => $order,
        ]);
        $this->getEventManager()->dispatch($event);
    }

    /**
     * Construye la información de los items cuando todavía no se ha hecho el pedido
     * 
     * @param  array $items
     * @return void
     */
    public function buildItems(array $items, $build_items_price = true)
    {
        foreach ($items as $item) {
            $item->build($build_items_price);
        }
    }

    /**
     * Construye los totales del pedido
     * 
     * @param  Order $order
     * @return void
     */
    public function buildTotals($order)
    {
        Cart::setOrder($order);
        $this->setSubtotal($order);
        $this->Carriers->buildOrder($order);
        $this->setCouponDiscount($order);
        $this->setGift($order);
        $this->setTaxes($order);
        $this->setPaymentMethodCost($order);
        $this->setTotal($order);
        $this->observePaymentMethod($order);
    }

    public function observePaymentMethod($order)
    {
        if ($order->total == 0) {
            $order->set('payment_method_id', null);
        }
    }

    public function setPaymentMethodCost($order)
    {
        if (empty($order->payment_method)) {
            $order->set('payment_price', 0);
            return;
        }

        if (!$order->payment_method->pay_for_used) {
            $order->set('payment_price', 0);
            return;
        }

        $amount = $order->subtotal + $order->line_items_taxes - $order->promo_discount - $order->coupon_discount;

        if ($order->payment_method->cost_type == 'percent') {
            $cost = $amount * $order->payment_method->price / 100;
            $order->set('payment_price', $cost);
        }
    }

    /**
     * Calcula la fecha de entrega, tienedo en cuenta el transportista
     * 
     * @param  \Store\Model\Entity\Order $order
     */
    public function buildDeliveryDate($order)
    {
        $delivery_date_start = date('Y-m-d');
        $festives = $this->Carriers->getFestives($order);

        if (!$order->delivery_days) {
            $order->set('delivery_date_start', null);
            $order->set('delivery_date', null);
        }

        if (!$order->delivery_date_custom) {
            if (!empty($order->carrier->hour)) {
                if (time() > strtotime($order->carrier->hour) && !$this->Carriers->isDayFestive($delivery_date_start, $festives)) {
                    $delivery_date_start = date('Y-m-d', strtotime('+1 day'));
                }
            }

            while ($this->Carriers->isDayFestive($delivery_date_start, $festives)) {
                $delivery_date_start = date('Y-m-d', strtotime($delivery_date_start . ' +1 day'));
            }

            $days = $order->delivery_days;
            $delivery_date = $delivery_date_start;

            if ($days) {
                for ($i = 1; $i <= $days; $i++) {
                    $delivery_date = date('Y-m-d', strtotime($delivery_date . ' +1 day'));

                    if ($order->carrier->delivery_days_type == 'business_days') {
                        while ($this->Carriers->isDayFestive($delivery_date, $festives)) {
                            $delivery_date = date('Y-m-d', strtotime($delivery_date . ' +1 day'));
                        }
                    }
                }

                $order->set('delivery_date_start', $delivery_date_start);
                $order->set('delivery_date', $delivery_date);
            }
        } else {
            // $order->set( 'express_delivery', false);
            $order->delivery_date = $order->delivery_date_input;
        }

        if ($order->delivery_days === false) {
            $order->delivery_days = null;
        }


        // Event
        $event = new Event('Store.Model.Order.buildDeliveryDate', $this, [
            'order' => $order,
        ]);

        $this->getEventManager()->dispatch($event);
    }


    public function preventDeliveryDate($order)
    {
        $delivery_date = $this->calculateDeliveryDate($order, $order->delivery_date);
        $order->delivery_date = $delivery_date;
    }

    public function calculateDeliveryDate($order, $delivery_date)
    {
        $festives = $this->Carriers->getFestives($order);

        if ($order->carrier->delivery_days_type == 'business_days') {
            while ($this->Carriers->isDayFestive($delivery_date, $festives)) {
                $delivery_date = date('Y-m-d', strtotime($delivery_date . ' +1 day'));
            }

            return date('Y-m-d', strtotime($delivery_date));
        }
    }

    /**
     * Setea el subtotal del pedido, es decir, el total de los items
     * 
     * @param Order $order
     */
    public function setSubtotal($order)
    {
        $subtotal = 0;
        $discount = 0;

        foreach ($order->line_items as $item) {
            $subtotal += $item->subtotal;
            $discount += $item->discount;
        }

        if ($subtotal < 0) {
            $subtotal = 0;
        }

        $order->set('subtotal', $subtotal);
        $order->set('subtotal_fixed', $subtotal);
        $order->set('discount_price', $discount);

        // Event
        $event = new Event('Store.Model.Order.afterSetSubtotal', $this, [
            'order' => $order,
        ]);

        $this->getEventManager()->dispatch($event);

        return $order;
    }

    /**
     * Setea el total del pedido
     * 
     * @param Order $order
     */
    public function setTotal($order)
    {
        // Event
        $event = new Event('Store.Model.Order.beforeSetTotal', $this, [
            'order' => $order,
        ]);

        $this->getEventManager()->dispatch($event);

        $this->__calcultateTotal($order);

        // Event
        $event = new Event('Store.Model.Order.afterSetTotal', $this, [
            'order' => $order,
        ]);

        $this->getEventManager()->dispatch($event);
    }

    private function __calcultateTotal($order)
    {
        $total =  $order->subtotal +
            $order->shipping_handling +
            $order->shipping_handling_tax +
            $order->payment_price +
            $order->payment_price_tax +
            $order->gift_wrapping_price +
            $order->gift_wrapping_taxes +
            $order->points_discount_tax +
            $order->points_discount -
            $order->promo_discount
            + $order->taxes;

        $total -= $order->coupon_discount;

        if ($total < 0) {
            $total = 0;
        }

        $order->set('total', $total);
    }

    /**
     * Setea los impuestos de los items
     * 
     * @param Order $order
     */
    public function setTaxes($order)
    {
        $taxes = 0;

        foreach ($order->line_items as $item) {
            $taxes += round($item->taxes_total, 2);
        }

        $order->set('taxes', $taxes);
        $order->set('taxes_fixed', $taxes);
    }


    public function setGift($order)
    {
        if ($order->grif_wrap) {
            $price = Website::get('settings.store_gift_wrapping_price');
            $order->set('gift_wrapping_price', $price);
            $tax = TableRegistry::getTableLocator()->get('Store.Taxes')->get(Website::get('settings.store_gift_wrapping_tax_id'));
            $taxes = ($price * $tax->rate) / 100;
            $order->set('gift_wrapping_taxes_rate', $tax->rate);
            $order->set('gift_wrapping_taxes', $taxes);
        } else {
            $order->set('gift_wrapping_price', 0);
            $order->set('gift_wrapping_taxes', 0);
        }
    }


    public function setCouponDiscount($order)
    {
        TableRegistry::getTableLocator()->get('Store.CouponsOrders')->setForOrder($order);
    }

    /**
     * Finaliza un pedido
     * 
     * @param  \Store\Model\Entity\Order $order 
     * @param  string $status
     * @return \Store\Model\Entity\Order
     */
    public function finalize(Order $order, $status, $set_country = true)
    {
        $order->set('status', $status);
        $order->set('order_date', date('Y-m-d H:i:s'));

        // Copia las direcciones en el registro de order
        $this->copyAddresses($order);

        // Crea el usuario (si se ha pedido)
        $this->createUser($order);

        // Guarda
        $this->save($order, ['associated' => false]);
        $this->updateStock($order);

        foreach ($order->line_items as $item) {
            $item->title;
            $item->unsetProperty('photo');
            $this->LineItems->save($item, ['associated' => false]);
        }

        // Actualiza las direcciones del usuario
        TableRegistry::getTableLocator()->get('Store.Addresses')->saveFromOrder($order);

        // Renueva el registro
        $order = $this->find('order')->where(['Orders.id' => $order->id])->first();

        // Event
        $event = new Event('Store.Model.Order.finalizeOrder', $this, [
            'order' => $order,
        ]);

        $this->getEventManager()->dispatch($event);

        $user = $this->Users->find()
            ->where(['Users.id' => $order->user_id])
            ->first();

        $email_admin = Website::get('settings.store_order_email');

        if (empty($email_admin)) {
            $email_admin = Website::get('settings.users_reply_email');
        }

        $this->addStatus($order->salt, $status);

        if (StoreConfig::getConfig('adminWithoutTaxes')) {
            $site = Website::get();
            $settings = $site->settings;
            $settings->store_price_with_taxes = false;
            $order = $this->find('order')->where(['Orders.id' => $order->id])->first();
        }

        foreach (explode(',', $email_admin) as $email) {
            $this->getMailer('Store.Orders')->send('newOrder', [
                $order,
                $user,
                trim($email)
            ]);
        }

        if ($set_country) {
            Cart::setInvoiceCountryId($order->adr_invoice_country_id);
        }

        // $this->updateProductSales($order);
        $this->createGiftCoupon($order);
        $this->updateCoupon($order);

        // Event
        $event = new Event('Store.Model.Order.afterFinalizeOrder', $this, [
            'order' => $order,
        ]);

        $this->getEventManager()->dispatch($event);

        return $order;
    }

    public function createGiftCoupon($order)
    {
        foreach ($order->line_items as $item) {
            if ($item->is_giftcard) {
                $CouponsTable = TableRegistry::getTableLocator()->get('Store.Coupons');
                $coupon = $CouponsTable->newEntity([
                    'is_giftcard' => true,
                    'active' => true,
                    'value' => $item->realPrice($item->price, $item->tax_rate),
                    'type' => 'fixed',
                    'title' => 'Gift ' . $item->id,
                    'code' => $item->giftcard_code,
                    'start_on' => date('Y-m-d'),
                    'finish_on' => date('Y-m-d', strtotime('+ ' . StoreConfig::getConfig('giftCardExpireMonths') . ' month')),
                ]);

                $CouponsTable->save($coupon);

                if ($item->giftcard_method == 'email') {
                    $this->getMailer('Store.Orders')->send('giftcard', [
                        $order,
                        $item,
                        trim($item->giftcard_email)
                    ]);
                }
            }
        }
    }

    public function updateCoupon($order)
    {
        TableRegistry::getTableLocator()->get('Store.CouponsOrders')->updateCoupon($order);
    }

    public function updateProductSales($order)
    {
        $ProductsTable = StoreConfig::getProductsModel();

        foreach ($order->line_items as $item) {
            $product = $ProductsTable->find()
                ->where([
                    $ProductsTable->getAlias() . '.id' => $item->product_id
                ])
                ->first();

            if ($product) {
                $ProductsTable->query()->update()
                    ->set([
                        'store_sales_count' => $product->store_sales_count + $item->quantity
                    ])
                    ->where([
                        'id' => $product->id
                    ])
                    ->execute();
            }
        }
    }

    /**
     * Crea un usuario si desde el pedido el usuario a solicitado registro
     *
     * @param Order $order
     * @return void
     */
    public function createUser(Order $order)
    {
        if (!empty($order->user_id)) {
            return;
        }

        if (StoreConfig::getConfig('registerMandatory') || $order->want_account) {
            $data = [
                'name' => $order->adr_invoice_firstname . ' ' . $order->adr_invoice_lastname,
                'email' => $order->adr_invoice_email,
                'password' => $order->password,
                'password2' => $order->password2,
                'registered' => true
            ];

            if (empty(trim($data['name']))) {
                $data['name'] = $order->adr_delivery_firstname . ' ' . $order->adr_delivery_lastname;
            }

            $user = $this->Users->newEntity($data, ['validate' => 'register']);
            $this->Users->prepareRegister($user);

            $event = new Event('User.Controller.Users.beforeRegisterFromOrder', $this, ['user' => $user, 'order' => $order]);
            $this->getEventManager()->dispatch($event);

            if ($this->Users->save($user)) {
                // Actualiza el order con el user_id
                $this->query()
                    ->update()
                    ->set([
                        'user_id' => $user->id,
                        'password' => null,
                        'password2' => null
                    ])
                    ->where(['id' => $order->id])
                    ->execute();

                // Evento de registro
                $event = new Event('User.Controller.Users.afterRegisterFromOrder', $this, ['user' => $user, 'order' => $order]);
                $this->getEventManager()->dispatch($event);

                $order->set('user_id', $user->id);

                // Generación del enlace de activación
                $link = $this->Users->generateSendLink();

                // Evento de registro
                $event = new Event('User.Controller.Users.afterRegister', $this, [$user]);
                $this->getEventManager()->dispatch($event);

                if (array_key_exists('SCRIPT_NAME', $_SERVER) && strpos($_SERVER['SCRIPT_NAME'], 'phpunit') === false) {
                    // Envio del correo electrónico
                    $this->getMailer('User.User')->send('register', [
                        $user,
                        $link
                    ]);

                    if (StoreConfig::getConfig('sendMailWhenNewUser')) {
                        $this->getMailer('User.User')->send('newUser', [
                            $user
                        ]);
                    }
                }

                return $user;
            } else {
            }
        }
    }

    /**
     * Actualiza el stock, poniendo las cantidades correctamente después de la realización de un pedido
     *
     * @param Store\Model\Entity\Order $order
     * @return void
     */
    public function updateStock(Order $order)
    {
        foreach ($order->line_items as $item) {
            if (!empty($item->product_attribute)) {
                $Model = $this->LineItems->Products->ProductAttributes;
                $pa = $Model->findById($item->product_attribute->id)->first();
                $pa->quantity = $pa->quantity - $item->quantity;
                $Model->save($pa);
            } elseif (isset($item->product)) {
                $product = $this->LineItems->Products->findById($item->product->id)->first();

                $listeners = $this->getEventManager()->listeners('Store.Model.Orders.updateStockProduct');

                if (!empty($listeners)) {
                    $event = new Event('Store.Model.Orders.updateStockProduct', $this, [
                        $order,
                        $item,
                        $product
                    ]);

                    $this->getEventManager()->dispatch($event);
                    continue;
                }

                $product->store_quantity = $product->store_quantity - $item->quantity;
                $product->unsetProperty('photo');
                $product->unsetProperty('photos');
                $this->LineItems->Products->save($product, ['associated' => []]);
            }
        }
    }

    /**
     * Copia las direcciones a la tabla orders
     * @param  Store\Model\Entity\Order  $order [description]
     * @return null
     */
    public function copyAddresses(Order $order)
    {
        if ($order->same_address == 1) {
            $fields = [
                'firstname',
                'lastname',
                'company',
                'address',
                'country_id',
                'state_id',
                'city',
                'phone',
                'postcode',
            ];

            $order_main_address = StoreConfig::getConfig('orderMainAddress');
            $order_secondary_address = $order_main_address == 'invoice' ? 'delivery' : 'invoice';

            foreach ($fields as $field) {
                $prop = 'adr_' . $order_secondary_address . '_' . $field;
                $prop2 = 'adr_' . $order_main_address . '_' . $field;
                $order->set($prop, $order->$prop2);
            }

            if (!empty($order->adr_delivery_vat_number) && empty($order->adr_invoice_vat_number)) {
                $order->set('adr_invoice_vat_number', $order->adr_delivery_vat_number);
            }
        }

        // Copia los datos de la tienda
        $combinations = [
            'adr_store_company' => 'settings.store_name',
            'adr_store_cif' => 'settings.store_cif',
            'adr_store_address' => 'settings.store_address',
            'adr_store_postcode' => 'settings.store_postcode',
            'adr_store_city' => 'settings.store_city',
            'adr_store_state' => 'settings.store_state',
            'adr_store_country' => 'settings.store_country',
        ];

        foreach ($combinations as $order_field => $site_field) {
            $order->$order_field = \Website\Lib\Website::get($site_field);
        }

        $order->adr_store_phone = \Website\Lib\Website::get('settings.phone');
    }

    /**
     * Añade un item al order dado como primer argumento
     * 
     */
    public function addItem($order_id, $draft, $new_order = false)
    {
        $data = [
            'order_id' => $order_id,
            'product_id' => $draft->product()->id
        ];

        $product_attribute_id = $draft->productAttributesId();

        if ($product_attribute_id) {
            $data['product_attribute_id'] = $product_attribute_id;
        }

        // Busca si ya hay un item igual añadido a la cesta
        $item = $this->LineItems->find()
            ->where($data)
            ->first();

        $data['quantity'] = $draft->quantity();

        // Si el item ya existe con las propiedades dadas, añade una cantidad
        if ($item && $draft->product()->type != 'giftcard') {
            $item->set('quantity', ($item->quantity + $draft->quantity()));
        } else {
            $item = $this->LineItems->newEntity($data);

            $event = new Event('Store.Model.Order.beforeAddItem', $this, [
                $item,
                $draft
            ]);

            $this->getEventManager()->dispatch($event);
        }

        if ($item && $draft->product()->type == 'giftcard') {
            $item->set('is_giftcard', true);
            $item->set('giftcard_code', $this->generateGiftItemCode());
        }

        if ($draft->extras()) {
            foreach ($draft->extras() as $key => $value) {
                $item->set($key, $value);
            }
        }

        $item->set('currency', CurrencyCollection::getDefault());

        $saved = $this->LineItems->save($item);

        return $this->LineItems->find()
            ->where([
                'LineItems.id' => $saved->id
            ])
            ->contain([
                'Products' => function ($q) {
                    return $q->find('products')
                        ->contain('Taxes');
                },
                'ProductAttributes' => [
                    'Attributes'
                ],
            ])
            ->first();
    }

    private function generateGiftItemCode()
    {
        $chars = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ";
        $code = "";
        for ($i = 0; $i < 6; $i++) {
            $code .= $chars[mt_rand(0, strlen($chars) - 1)];
        }

        if ($this->LineItems->exists([
            'giftcard_code' => $code
        ])) {
            return $this->generateGiftItemCode();
        }

        return $code;
    }


    /**
     * Borra un item de la lista de pedido pasada
     * 
     * @param  string $id  El id del item
     * @param  Store\Table\Entity\Order $order
     * @return Store\Table\Entity\LineItem | false
     */
    public function deleteItem($id, $order)
    {
        $item = $this->LineItems->find()
            ->where([
                'LineItems.id' => $id,
                'LineItems.order_id' => $order->id
            ])
            ->contain([
                'Products' => function ($q) {
                    return $q->find('products')
                        ->contain('Taxes');
                },
                'ProductAttributes' => [
                    'Attributes'
                ],
            ])
            ->first();

        if ($item) {
            $older = clone $item;
            $this->LineItems->delete($item);
            $older->build();
            $event = new Event('Store.Model.Order.afterDeleteItem', $this, [
                'item' => $older,
            ]);

            $this->getEventManager()->dispatch($event);
            return $older;
        }

        return false;
    }

    /**
     * Devuelve el último estado del pedido dado un id
     * 
     * @param  integer $id 
     * @return Store\Table\Entity\OrderHistory | false
     */
    public function getLastStatus($id)
    {
        $order = $this->find()
            ->where([
                'Orders.id' => $id
            ])
            ->contain([
                'OrderHistories' => function ($q) {
                    return $q
                        ->limit(1);
                }
            ])
            ->first();

        if ($order && isset($order->order_histories[0])) {
            return $order->order_histories[0];
        }

        return false;
    }



    /**
     * Añade un estado al pedido
     * 
     * @param string $salt El salt del pedido
     * @param string $status El nuevo estado
     * @param integer $user_id El id del usuario administrador que lo ha cambiado
     */
    public function addStatus($salt, $status, $user_id = null)
    {
        $order = $this->find('order')
            ->where([
                'Orders.salt' => $salt
            ])
            ->first();

        $user = $this->Users->find()
            ->where(['Users.id' => $order->user_id])
            ->first();

        if (!$order) {
            return false;
        }

        $last = $this->getLastStatus($order->id);

        $event = new Event('Store.Model.Order.addStatus', $this, [
            $order,
            $status
        ]);

        $this->getEventManager()->dispatch($event);

        if (!$last || $last->status != $status) {
            $data = [
                'order_id' => $order->id,
                'status' => $status,
                'user_id' => $user_id
            ];

            $entity = $this->OrderHistories->newEntity($data);

            if ($this->OrderHistories->save($entity)) {
                $this->query()->update()
                    ->set('status', $status)
                    ->where(['id' => $order->id])
                    ->execute();

                if ($order->total > 0) {
                    $this->createInvoice($order, $status);
                }


                if (StoreConfig::getConfig('mailClientWithoutTaxes')) {
                    $site = Website::get();
                    $settings = $site->settings;
                    $settings->store_price_with_taxes = false;
                    $order = $this->find('order')->where(['Orders.id' => $order->id])->first();
                }

                $order->set('status', $status);
                $dontsends = StoreConfig::getConfig('dontSendStatus');

                if (!in_array($status, $dontsends)) {
                    $locale = $user ? $user->locale : ($order->locale ? $order->locale : false);

                    // Envio de email
                    $this->getMailer('Store.Orders')->send('status' . Inflector::camelize($status), [
                        $order,
                        $user
                    ], [], $locale);
                }
                return true;
            }
        }
    }


    public function createInvoice($order, $status)
    {
        if (!StoreConfig::getConfig('withInvoices') || Website::get('settings.store_invoice_when_status') != $status) {
            return;
        }

        if ($order->has_invoice || StoreConfig::getConfig('invoiceOnly') == 'invoice') {
            $this->Invoices->create($order);
        } else {
            $this->Receipts->create($order);
        }
    }

    /**
     * Toma el último pedido completo del usuario
     * 
     * @param  integer $user_id
     */
    public function getLastUserShopping($user_id)
    {
        $order = $this->find()
            ->where([
                'Orders.user_id' => $user_id,
                'Orders.status IS NOT NULL',
            ])
            ->order([
                'Orders.created' => 'desc'
            ])
            ->first();

        return $order;
    }

    /**
     * Toma el último pedido que ha hecho un usuario en las últimas 24 horas
     * Sirve para recuperar un carro que ha sido guardado con anterioridad
     * 
     * @param  integer $user_id
     * @return Store\Model\Entity\Order
     */
    public function getLastUserOrder($user_id)
    {
        $order = $this->getOrder([
            'Orders.user_id' => $user_id,
            'Orders.modified >= now() - INTERVAL 1 DAY',
            'Orders.status IS NULL',
        ]);

        $lastorder = $this->getLastUserShopping($user_id);

        return $order;
    }


    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationCheckout(Validator $validator)
    {
        $customValidator = StoreConfig::getHook('validateCheckout');

        if (is_callable($customValidator)) {
            return $customValidator($validator);
        }

        $order_main_address = StoreConfig::getConfig('orderMainAddress');
        $order_secondary_address = $order_main_address == 'invoice' ? 'delivery' : 'invoice';

        $validator
            ->add('adr_invoice_email', 'validUserEmail', [
                'rule' => 'isValidUserEmail',
                'message' => __d('app', 'Es necesario indicar un email correcto'),
                'provider' => 'table',
            ])
            ->add('adr_invoice_email', 'validUserUniqueEmail', [
                'rule' => 'isValidUserUniqueEmail',
                'message' => __d('app', 'El email ya está siendo utilizado por otro usuario'),
                'provider' => 'table',
            ])
            ->allowEmptyString('adr_invoice_email', null, function ($context) {
                return !empty($context['data']['user_id']);
            })
            ->add('adr_' . $order_main_address . '_city', 'userDataEmpty', [
                'rule' => 'isUserDataEmpty',
                'message' => __d('app', 'Es necesario indicar una población'),
                'provider' => 'table',
            ])
            ->add('adr_' . $order_main_address . '_country_id', 'notEmpty', [
                'rule' => function ($value, $context) {
                    if (empty($value)) {
                        return false;
                    }

                    return true;
                },
                'message' => __d('app', 'Es necesario indicar un país'),
            ])
            ->add('adr_' . $order_main_address . '_state_id', 'notEmpty', [
                'rule' => function ($value, $context) {
                    if (empty($value)) {
                        return false;
                    }

                    return true;
                },
                'message' => __d('app', 'Es necesario indicar una provincia'),
            ]);

        if (Website::get('settings.store_mandatory_fields_firstname')) {
            $validator->add('adr_' . $order_main_address . '_firstname', 'userDataEmpty', [
                'rule' => 'isInvoiceDataEmpty',
                'message' => __d('app', 'Es necesario indicar el nombre'),
                'provider' => 'table',
            ]);
        }

        $validator
            ->add('adr_' . $order_secondary_address . '_firstname', 'invoiceDataEmpty', [
                'rule' => 'isInvoiceDataEmpty',
                'message' => __d('app', 'Es necesario indicar el nombre'),
                'provider' => 'table',
            ])
            ->allowEmpty('adr_' . $order_secondary_address . '_firstname', function ($context) {
                return $context['data']['same_address'] == 1;
            });


        if (!StoreConfig::getConfig('onlyFirstname') && Website::get('settings.store_mandatory_fields_lastname')) {
            $validator->add('adr_' . $order_main_address . '_lastname', 'userDataEmpty', [
                'rule' => 'isUserDataEmpty',
                'message' => __d('app', 'Es necesario indicar los apellidos'),
                'provider' => 'table',
            ]);
        }

        if (!StoreConfig::getConfig('onlyFirstname')) {
            $validator
                ->add('adr_' . $order_secondary_address . '_lastname', 'invoiceDataEmpty', [
                    'rule' => 'isInvoiceDataEmpty',
                    'message' => __d('app', 'Es necesario indicar los apellidos'),
                    'provider' => 'table',
                ])
                ->allowEmpty('adr_' . $order_secondary_address . '_lastname', function ($context) {
                    return $context['data']['same_address'] == 1;
                });
        }

        if (StoreConfig::getConfig('onlyFirstname') && Website::get('settings.store_mandatory_fields_firstname')) {
            $validator->add('adr_' . $order_main_address . '_firstname', 'custom', [
                'rule' => function ($value, $context) {
                    $words = explode(' ', trim($value));
                    return count($words) > 1;
                },
                'message' => __d('app', 'Es necesario indicar nombre y apellidos'),
            ]);
        }

        if (Website::get('settings.store_mandatory_fields_phone')) {
            $validator
                ->add('adr_' . $order_main_address . '_phone', 'userDataEmpty', [
                    'rule' => 'isInvoiceDataEmpty',
                    'message' => __d('app', 'Es necesario indicar un teléfono'),
                    'provider' => 'table',
                ])
                ->allowEmpty('adr_' . $order_secondary_address . '_phone', function ($context) {
                    return $context['data']['same_address'] == 1;
                });

            $validator->add('adr_' . $order_main_address . '_phone', 'validPhone', [
                'rule' => 'isValidPhone',
                'message' => __d('app', 'Es necesario indicar un teléfono válido'),
                'provider' => 'table',
            ]);
        }

        if (Website::get('settings.store_mandatory_fields_email_confirmation')) {
            $validator->add('adr_invoice_email_confirmation', 'custom', [
                'rule' => function ($value, $context) {
                    return $context['data']['adr_invoice_email'] == $context['data']['adr_invoice_email_confirmation'];
                },
                'message' => __d('app', 'Es necesario que el email coincida'),
            ]);
        }

        if (Website::get('settings.store_mandatory_fields_address')) {
            $validator->add('adr_' . $order_main_address . '_address', 'userDataEmpty', [
                'rule' => 'isUserDataEmpty',
                'message' => __d('app', 'Es necesario indicar una dirección'),
                'provider' => 'table',
            ]);
        }

        if (Website::get('settings.store_mandatory_fields_postcode')) {
            $validator->add('adr_' . $order_main_address . '_postcode', 'userDataEmpty', [
                'rule' => 'isUserDataEmpty',
                'message' => __d('app', 'Es necesario indicar un código postal'),
                'provider' => 'table',
            ]);
        }

        if (Website::get('settings.store_mandatory_fields_vat_number')) {
            if (StoreConfig::getConfig('onlyInvoiceVat')) {
                $validator
                    ->add('adr_invoice_vat_number', 'userDataEmpty', [
                        'rule' => 'isUserDataEmpty',
                        'message' => __d('app', 'Es necesario indicar un CIF/NIF'),
                        'provider' => 'table',
                    ])
                    ->add('adr_invoice_vat_number', 'validVat', [
                        'rule' => 'isValidVat',
                        'message' => __d('app', 'Es necesario indicar un CIF/NIF válido'),
                        'provider' => 'table',
                    ])
                    ->allowEmpty('adr_delivery_vat_number', function ($context) {
                        return $context['data']['same_address'] == 1;
                    });
            } else {
                $validator
                    ->add('adr_' . $order_main_address . '_vat_number', 'userDataEmpty', [
                        'rule' => 'isUserDataEmpty',
                        'message' => __d('app', 'Es necesario indicar un CIF/NIF'),
                        'provider' => 'table',
                    ])
                    ->add('adr_' . $order_secondary_address . '_vat_number', 'validVat', [
                        'rule' => 'isValidVat',
                        'message' => __d('app', 'Es necesario indicar un CIF/NIF válido'),
                        'provider' => 'table',
                    ])
                    ->add('adr_' . $order_main_address . '_vat_number', 'validVat', [
                        'rule' => 'isValidVat',
                        'message' => __d('app', 'Es necesario indicar un CIF/NIF válido'),
                        'provider' => 'table',
                    ])
                    ->allowEmpty('adr_' . $order_secondary_address . '_vat_number', function ($context) {
                        return $context['data']['same_address'] == 1;
                    });
            }
        } else {
            $validator
                ->allowEmpty('adr_' . $order_secondary_address . '_vat_number', function ($context) {
                    return $context['data']['same_address'] == 1;
                })
                ->allowEmpty('adr_' . $order_main_address . '_vat_number', function ($context) {
                    return $context['data']['same_address'] == 1;
                });
        }

        $validator->add('adr_' . $order_secondary_address . '_address', 'invoiceDataEmpty', [
            'rule' => 'isInvoiceDataEmpty',
            'message' => __d('app', 'Es necesario indicar una dirección'),
            'provider' => 'table',
        ])
            ->allowEmpty('adr_' . $order_secondary_address . '_address', function ($context) {
                return $context['data']['same_address'] == 1;
            })
            ->add('adr_' . $order_secondary_address . '_city', 'invoiceDataEmpty', [
                'rule' => 'isInvoiceDataEmpty',
                'message' => __d('app', 'Es necesario indicar una población'),
                'provider' => 'table',
            ])
            ->allowEmpty('adr_' . $order_secondary_address . '_city', function ($context) {
                return $context['data']['same_address'] == 1;
            })
            ->add('adr_' . $order_secondary_address . '_country_id', 'invoiceDataEmpty', [
                'rule' => 'isInvoiceDataEmpty',
                'message' => __d('app', 'Es necesario indicar un país'),
                'provider' => 'table',
            ])
            ->allowEmpty('adr_' . $order_secondary_address . '_country_id', function ($context) {
                return $context['data']['same_address'] == 1;
            })
            ->add('adr_' . $order_secondary_address . '_state_id', 'invoiceDataEmpty', [
                'rule' => 'isInvoiceDataEmpty',
                'message' => __d('app', 'Es necesario indicar una provincia'),
                'provider' => 'table',
            ])
            ->allowEmpty('adr_' . $order_secondary_address . '_state_id', function ($context) {
                return $context['data']['same_address'] == 1;
            })
            ->add('adr_' . $order_secondary_address . '_postcode', 'invoiceDataEmpty', [
                'rule' => 'isInvoiceDataEmpty',
                'message' => __d('app', 'Es necesario indicar un código postal'),
                'provider' => 'table',
            ])
            ->allowEmpty('adr_' . $order_secondary_address . '_postcode', function ($context) {
                return $context['data']['same_address'] == 1;
            })
            ->add('payment_method_id', 'hasPayment', [
                'rule' => 'hasPayment',
                'message' => __d('app', 'Es necesario indicar un método de pago'),
                'provider' => 'table',
            ])
            ->allowEmpty('password', function ($context) {
                if (!empty($context['data']['user_id'])) {
                    return true;
                }

                return !StoreConfig::getConfig('registerMandatory') && empty($context['data']['want_account']);
            })
            ->allowEmpty('password2', function ($context) {
                if (!empty($context['data']['user_id'])) {
                    return true;
                }

                return !StoreConfig::getConfig('registerMandatory') && empty($context['data']['want_account']);
            })
            // ->add('password', 'length', [
            //     'rule' => ['minLength', 5],
            //     'message' => __d('app', 'Las contraseñas han de tener como mínimo 5 carácteres'),
            // ])
            ->add('password', 'custom', [
                'rule' => function ($value, array $context) {
                    if (!StoreConfig::getConfig('registerMandatory') && empty($context['data']['want_account'])) {
                        return true;
                    }

                    if (empty($value)) {
                        return false;
                    }

                    return $this->Users->validatePassword($value, $context);
                },
                'message' => __d('app', 'La contraseña debe estar formada por, como mínimo, por una letra y un número'),
            ])
            ->add('password2', 'custom', [
                'rule' => function ($value, $context) {

                    if (empty($context['data']['password']) || empty($context['data']['want_account'])) {
                        return true;
                    }

                    if ($value !== $context['data']['password']) {
                        return false;
                    }

                    return true;
                },
                'message' => __d('app', 'Las contraseñas han de ser iguales'),
            ]);

        $event = new Event('Store.Model.Orders.validationCheckout', $this, [$validator]);
        $this->getEventManager()->dispatch($event);

        return $validator;
    }

    public function hasPayment($value, array $context)
    {
        if (isset($context['data']['total']) && $context['data']['total'] == 0) {
            return true;
        }

        return !empty($value);
    }

    public function isUserDataEmpty($value, array $context)
    {
        if (!empty($context['data']['user_id'])) {
            return true;
        }

        return Validation::notBlank($value);
    }

    public function isInvoiceDataEmpty($value, array $context)
    {
        if ($context['data']['same_address'] == 1) {
            return true;
        }

        return Validation::notBlank($value);
    }

    public function isValidUserEmail($value, array $context)
    {
        if (!empty($context['data']['user_id'])) {
            return true;
        }

        return Validation::email(trim($value));
    }

    public function isValidUserUniqueEmail($value, array $context)
    {
        if (!empty($context['data']['user_id'])) {
            return true;
        }

        if (StoreConfig::getConfig('registerMandatory') || !empty($context['data']['want_account'])) {
            $context['data']['registered'] = true;
            return $this->Users->validateUniqueEmail($value, $context);
        }

        return true;
    }

    public function isValidPhone($value, array $context)
    {
        $value = preg_replace('/\s+/', '', $value);
        $a = preg_match('/^[0-9\(\)\-\+]+$/', str_replace(' ', '', $value));
        preg_match_all('!\d+!', $value, $matches);

        $numbers = '';

        foreach ((array)$matches[0] as $_nums) {
            $numbers .= $_nums;
        }

        return strlen($numbers) >= 9 && (bool)$a;
    }

    public function emptyPaymentError($id)
    {
        $this->query()->update()
            ->set([
                'payment_error_code' => '',
            ])
            ->where([
                'id' => $id
            ])
            ->execute();
    }

    public function isValidVat($value, array $context)
    {
        if (StoreConfig::getConfig('onlyInvoiceVat')) {
            $order_main_address = 'invoice';
            $order_secondary_address = 'delivery';
        } else {
            $order_main_address = StoreConfig::getConfig('orderMainAddress');
            $order_secondary_address = $order_main_address == 'invoice' ? 'delivery' : 'invoice';

            if ($context['field'] == 'adr_' . $order_secondary_address . '_vat_number' && $context['data']['same_address'] == 1) {
                return true;
            }
        }
        if ($context['field'] == 'adr_' . $order_main_address . '_vat_number') {
            $key = 'adr_' . $order_main_address . '_country_id';
        } else {
            $key = 'adr_' . $order_secondary_address . '_country_id';
        }

        if (array_key_exists($key, $context['data'])) {
            $country = TableRegistry::getTableLocator()->get('Store.Countries')->getISO($context['data'][$key]);
            if ($country && in_array($country, ['ES'])) {
                if ($value == '00000000T') {
                    return false;
                }

                $validator = new \Skilla\ValidatorCifNifNie\Validator(new \Skilla\ValidatorCifNifNie\Generator());
                try {
                    $value = trim($value);
                    return $validator->validate(strtoupper($value));
                } catch (\Exception $e) {
                    return false;
                }
            }
        }

        return true;
    }
}
