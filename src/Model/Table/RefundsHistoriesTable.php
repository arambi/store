<?php
namespace Store\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * RefundsHistories Model
 *
 * @property \Store\Model\Table\UsersTable&\Cake\ORM\Association\BelongsTo $Users
 * @property \Store\Model\Table\RefundsTable&\Cake\ORM\Association\BelongsTo $Refunds
 *
 * @method \Store\Model\Entity\RefundsHistory get($primaryKey, $options = [])
 * @method \Store\Model\Entity\RefundsHistory newEntity($data = null, array $options = [])
 * @method \Store\Model\Entity\RefundsHistory[] newEntities(array $data, array $options = [])
 * @method \Store\Model\Entity\RefundsHistory|false save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \Store\Model\Entity\RefundsHistory saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \Store\Model\Entity\RefundsHistory patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \Store\Model\Entity\RefundsHistory[] patchEntities($entities, array $data, array $options = [])
 * @method \Store\Model\Entity\RefundsHistory findOrCreate($search, callable $callback = null, $options = [])
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class RefundsHistoriesTable extends Table
{
    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('store_refunds_histories');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');

        $this->belongsTo('Users', [
            'foreignKey' => 'user_id',
            'className' => 'Store.Users',
        ]);
        
        $this->belongsTo('Refunds', [
            'foreignKey' => 'refund_id',
            'className' => 'Store.Refunds',
        ]);
    }

}
