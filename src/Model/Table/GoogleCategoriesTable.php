<?php
namespace Store\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * GoogleCategories Model
 *
 * @method \Store\Model\Entity\GoogleCategory get($primaryKey, $options = [])
 * @method \Store\Model\Entity\GoogleCategory newEntity($data = null, array $options = [])
 * @method \Store\Model\Entity\GoogleCategory[] newEntities(array $data, array $options = [])
 * @method \Store\Model\Entity\GoogleCategory|false save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \Store\Model\Entity\GoogleCategory saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \Store\Model\Entity\GoogleCategory patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \Store\Model\Entity\GoogleCategory[] patchEntities($entities, array $data, array $options = [])
 * @method \Store\Model\Entity\GoogleCategory findOrCreate($search, callable $callback = null, $options = [])
 */
class GoogleCategoriesTable extends Table
{
    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('store_google_categories');
        $this->setDisplayField('title');
        $this->setPrimaryKey('id');

        $this->addBehavior('Manager.Crudable');

        $this->crud
            ->addFields([
                'title' => __d('admin', 'Título'),
                'path' => __d('admin', 'Título'),
            ])
            ->addIndex('index', [
                'fields' => [
                    'title',
                    'path',
                ],
                'actionButtons' => ['create'],
                'saveButton' => false,
            ])
            ->setName([
                'singular' => __d('admin', 'Categorías de Google'),
                'plural' => __d('admin', 'Categorías de Google'),
            ])
            ->addView('create', [
                'columns' => [
                    [
                        'cols' => 8,
                        'box' => [
                            [
                                'elements' => [
                                    'title',
                                    'path',
                                ]
                            ]
                        ]
                    ]
                ],
                'actionButtons' => []
            ], ['update']);
    }

    
}
