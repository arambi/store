<?php

namespace Store\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;
use Store\Model\Entity\Country;
use Cake\Utility\Xml;
use Cake\Core\Plugin;
use Cake\ORM\TableRegistry;
use Cake\Core\Configure;
use Cake\Http\Client;
use I18n\Lib\Lang;
use ArrayObject;
use Cake\Datasource\EntityInterface;
use Cake\Event\Event;

/**
 * Countries Model
 */
class CountriesTable extends Table
{
    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        $this->setTable('store_countries');
        $this->setDisplayField('title');
        $this->setPrimaryKey('id');


        // Associations
        $this->belongsTo('Zones', [
            'className' => 'Store.Zones'
        ]);

        $this->hasMany('States', [
            'className' => 'Store.States',
            'foreignKey' => 'country_id',
        ]);

        // Behaviors
        $this->addBehavior('Timestamp');
        $this->addBehavior(Configure::read('I18n.behavior'), [
            'fields' => ['title'],
            'translationTable' => 'Store.ShopI18n',
        ]);

        $this->addBehavior('Manager.Crudable');
        $this->addBehavior('Cofree.Saltable');
        $this->addBehavior('Cofree.BooleanUnique', [
            'fields' => ['by_default']
        ]);

        // CRUD
        $this->crud->associations(['Zones', 'States']);

        $this->crud
            ->addFields([
                'active' => __d('admin', 'Activo'),
                'active_no_carrier' => __d('admin', 'Activo para pedidos sin transporte'),
                'by_default' => __d('admin', 'Por defecto'),
                'with_position' => 'Con posición privilegiada',
                'position' => [
                    'type' => 'numeric',
                    'label' => 'Posición',
                    'show' => 'content.with_position'
                ],
                'title' => __d('admin', 'Nombre'),
                'iso_code' => __d('admin', 'ISO Code'),
                'no_taxes' => __d('admin', 'Libre de impuestos'),
                'zone' => [
                    'label' => __d('admin', 'Zona'),
                    'type' => 'BelongsTo'
                ],
                'states' => [
                    'type' => 'hasMany',
                    'label' => 'Provincias'
                ]
            ])
            ->addIndex('index', [
                'fields' => [
                    'title',
                    'with_position',
                    'position',
                    'iso_code',
                    'active',
                    'active_no_carrier',
                    'by_default',
                    'no_taxes',
                    'zone',
                ],
                'actionButtons' => ['create'],
                'saveButton' => false,
            ])
            ->setName([
                'singular' => __d('admin', 'País'),
                'plural' => __d('admin', 'Países'),
            ])
            ->addView('create', [
                'columns' => [
                    [
                        'title' => __d('admin', 'Datos del producto'),
                        'box' => [
                            [
                                'elements' => [
                                    'title',
                                    'iso_code',
                                    'active',
                                    'active_no_carrier',
                                    'with_position',
                                    'position',
                                    'by_default',
                                    'no_taxes',
                                    'zone',
                                    'states',
                                ]
                            ]
                        ]
                    ],
                ],
                'actionButtons' => ['create', 'index'],
            ], ['update']);
    }


    public function afterSave(Event $event, EntityInterface $entity)
    {
        if ($entity->isNew() && $entity->get('active') === true) {
            $this->saveStates($entity);
        }
    }

    public function saveStates($country)
    {
        $path = Plugin::path('Store') . 'src' . DS . 'Model' . DS . 'Data' . DS . 'states.csv';
        $csv = file_get_contents($path);
        $states = [];
        $rows = explode("\n", $csv);

        foreach ($rows as $row) {
            $cols = explode(",", $row);
            $cols = array_map(function ($value) {
                return str_replace('"', '', $value);
            }, $cols);

            if ($cols[0] == $country->iso_code && !empty($cols[2])) {
                $states[] = [
                    'country_id' => $country->id,
                    'iso_code' => $cols[1],
                    'title' => $cols[2]
                ];
            }
        }

        if (empty($states)) {
            return;
        }

        foreach ($states as $data) {
            $state = $this->States->find()
                ->where([
                    'country_id' => $data['country_id'],
                    'iso_code' => $data['iso_code']
                ])
                ->first();

            if (!$state) {
                $entity = $this->States->newEntity($data);
                $entity->set('active', true);
                $this->States->save($entity);
            }
        }
    }

    public function getByCode($code)
    {
        return $this->find()
            ->where([
                'Countries.iso_code' => $code,
                'Countries.active' => true,
            ])
            ->order([
                'Countries.title'
            ])
            ->first();
    }


    public function getISO($id)
    {
        $entity = $this->find()
            ->where([
                'id' => $id
            ])
            ->first();

        if (!$entity) {
            return false;
        }

        if (empty($entity->iso_code)) {
            return false;
        }

        return $entity->iso_code;
    }

    public function getDefault()
    {
        return $this->find()
            ->where([
                'Countries.by_default' => true
            ])
            ->first();
    }

    public function getFromRest()
    {
        $http = new Client();
        $results = $http->get('https://restcountries.eu/rest/v2/all', ['type' => 'json'])
            ->body('json_decode');


        foreach ($results as $result) {
            $country = $this->getByCode($result->alpha2Code);

            if (!$country) {
                $data = [
                    '_translations' => $this->_getTranslations($result),
                    'iso_code' => $result->alpha2Code,
                    'call_prefix' => $result->callingCodes[0],
                ];

                $country = $this->newEntity($data);
                $this->save($country);
            }
        }
    }

    private function _getTranslations($result)
    {
        $locales = Lang::combine('iso2', 'iso3');
        $data = [];
        $translations = (array)$result->translations;
        $translations['en'] = $result->name;

        foreach ($locales as $iso2 => $iso3) {
            if (array_key_exists($iso2, $translations)) {
                $data[$iso3]['title'] = $translations[$iso2];
            }
        }

        return $data;
    }
}
