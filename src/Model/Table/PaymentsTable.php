<?php
namespace Store\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;
use Store\Model\Entity\Payment;

/**
 * Payments Model
 *
 * Contiene los pagos o intentos de pago realizados
 */
class PaymentsTable extends Table
{

  public function initialize(array $config)
  {
    parent::initialize($config);

    $this->table('store_payments');
    $this->displayField('id');
    $this->primaryKey('id');
    
    // Behaviors
    $this->addBehavior( 'Timestamp');
    $this->addBehavior( 'Manager.Crudable');
    $this->addBehavior( 'Cofree.Saltable');

    $this->addBehavior( 'Cofree.Jsonable', [
      'fields' => ['data']
    ]);
  }

/**
 * Guarda un pago
 * 
 * @param  array $data 
 * @return Entity|false
 */
  public function savePayment( $data)
  {
    $entity = $this->newEntity( $data);
    return $this->save( $entity);
  }
}
