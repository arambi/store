<?php
namespace Store\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * CorreosPreregistro Model
 *
 * @method \Store\Model\Entity\CorreosPreregistro get($primaryKey, $options = [])
 * @method \Store\Model\Entity\CorreosPreregistro newEntity($data = null, array $options = [])
 * @method \Store\Model\Entity\CorreosPreregistro[] newEntities(array $data, array $options = [])
 * @method \Store\Model\Entity\CorreosPreregistro|false save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \Store\Model\Entity\CorreosPreregistro saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \Store\Model\Entity\CorreosPreregistro patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \Store\Model\Entity\CorreosPreregistro[] patchEntities($entities, array $data, array $options = [])
 * @method \Store\Model\Entity\CorreosPreregistro findOrCreate($search, callable $callback = null, $options = [])
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class CorreosPreregistroTable extends Table
{
    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('store_correos_preregistro');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');
        $this->addBehavior('Timestamp');
    }
}
