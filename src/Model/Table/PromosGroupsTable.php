<?php
namespace Store\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;
use Store\Model\Entity\PromosGroup;

/**
 * PromosGroups Model
 *
 * @property \Cake\ORM\Association\BelongsTo $Promos
 * @property \Cake\ORM\Association\BelongsTo $Groups
 */
class PromosGroupsTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->table('store_promos_groups');
        $this->displayField('id');
        $this->primaryKey('id');
        $this->belongsTo('Promos', [
            'foreignKey' => 'promo_id',
            'className' => 'Store.Promos'
        ]);
        $this->belongsTo('Groups', [
            'foreignKey' => 'group_id',
            'className' => 'Store.Groups'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->add('id', 'valid', ['rule' => 'numeric'])
            ->allowEmpty('id', 'create');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['promo_id'], 'Promos'));
        $rules->add($rules->existsIn(['group_id'], 'Groups'));
        return $rules;
    }
}
