<?php
namespace Store\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;
use Store\Model\Entity\PromosProductAttribute;

/**
 * PromosProductAttributes Model
 *
 * @property \Cake\ORM\Association\BelongsTo $Promos
 * @property \Cake\ORM\Association\BelongsTo $ProductAttributes
 */
class PromosProductAttributesTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->table('store_promos_product_attributes');
        $this->displayField('id');
        $this->primaryKey('id');
        $this->belongsTo('Promos', [
            'foreignKey' => 'promo_id',
            'className' => 'Store.Promos'
        ]);
        $this->belongsTo('ProductAttributes', [
            'foreignKey' => 'product_attribute_id',
            'className' => 'Store.ProductAttributes'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->add('id', 'valid', ['rule' => 'numeric'])
            ->allowEmpty('id', 'create');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['promo_id'], 'Promos'));
        $rules->add($rules->existsIn(['product_attribute_id'], 'ProductAttributes'));
        return $rules;
    }
}
