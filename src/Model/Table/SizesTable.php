<?php
namespace Store\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;
use Store\Model\Entity\Size;

/**
 * Sizes Model
 */
class SizesTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->table('store_sizes');
        $this->displayField('title');
        $this->primaryKey('id');
        
        // Behaviors
        $this->addBehavior( 'Manager.Crudable');
        $this->addBehavior( 'Block.Blockable', [
          'blocks' => [
            'text', 'gallery'
          ],
          'defaults' => [
            'text'
          ]
        ]);
        $this->addBehavior( 'Cofree.Saltable');

        // CRUD Config
        //Escribe aquí las asociaciones vinculadas con el model a la hora de editar (si las hubiera)
        // $this->crud->associations([]);

        $this->crud
          ->addFields([
            'title' => __d( 'admin', 'Título'),
            
          ])
          ->addIndex( 'index', [
            'fields' => [
              'title',
        
            ],
            'actionButtons' => ['create'],
            'saveButton' => false,
          ])
          ->setName( [
            'singular' => __d( 'admin', 'Tallas'),
            'plural' => __d( 'admin', 'Tallas'),
          ])
          ->addView( 'create', [
            'columns' => [
              [
                'cols' => 8,
                'box' => [
                  [
                    'elements' => [
                    'title',
                      'blocks'
                    ]
                  ]
                ]
              ]
            ],
            'actionButtons' => ['create', 'index']
          ], ['update'])
          ;
      
}
}
