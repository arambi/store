<?php
namespace Store\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * MrwStatuses Model
 *
 * @method \Store\Model\Entity\MrwStatus get($primaryKey, $options = [])
 * @method \Store\Model\Entity\MrwStatus newEntity($data = null, array $options = [])
 * @method \Store\Model\Entity\MrwStatus[] newEntities(array $data, array $options = [])
 * @method \Store\Model\Entity\MrwStatus|false save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \Store\Model\Entity\MrwStatus saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \Store\Model\Entity\MrwStatus patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \Store\Model\Entity\MrwStatus[] patchEntities($entities, array $data, array $options = [])
 * @method \Store\Model\Entity\MrwStatus findOrCreate($search, callable $callback = null, $options = [])
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class MrwStatusesTable extends Table
{
    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('store_mrw_statuses');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');
        $this->addBehavior('Timestamp');
    }

}
