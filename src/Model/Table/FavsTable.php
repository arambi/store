<?php
namespace Store\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;
use Store\Model\Entity\Fav;
use Store\Config\StoreConfig;

/**
 * Favs Model
 */
class FavsTable extends Table
{

  /**
   * Initialize method
   *
   * @param array $config The configuration for the Table.
   * @return void
   */
  public function initialize(array $config)
  {
    parent::initialize($config);

    $this->table( 'store_favs');
    $this->displayField( 'id');
    $this->primaryKey( 'id');
    $this->addBehavior( 'Timestamp');
    $this->addBehavior( 'Manager.Crudable');
    $this->addBehavior( 'Cofree.Saltable');
    
    $this->belongsTo( 'Products', [
      'className' => StoreConfig::getProductsModel()->registryAlias(),
    ]);    

    $this->belongsTo( 'Users', [
      'className' => 'User.Users'
    ]);    

    // CRUD
    $this->crud->associations( ['Products']);

    $this->crud
      ->addFields([
        'title' => [
          'label' => __d( 'admin', 'Producto'),
          'type' => 'info'
        ],
        'total' => [
          'label' => __d( 'admin', 'Total'),
          'type' => 'info'
        ],
      ])
      ->addIndex( 'index', [
        'fields' => [
          'title',
          'total',
        ],
        'actionButtons' => [],
        'saveButton' => false,
        'deleteLink' => false,
        'noSearch' => true,
        'editLink' => false,
      ])
      ->setName( [
        'singular' => __d( 'admin', 'Favoritos'),
        'plural' => __d( 'admin', 'Favoritos'),
      ])
      ->addView( 'create', [
        'columns' => [
          [
            'title' => __d( 'admin', 'Datos del producto'),
            'box' => [
              [
                'elements' => [
                  'title',
                ]
              ]
            ]
          ],
        ],
        'actionButtons' => ['create', 'index'],
      ], ['update'])
      
      ;
    
  }

/**
 * Añade un producto a favoritos y retorna un array con una clave 'error' y otra 'success', con los mensajes para el usuario
 * 
 * @param string $salt 
 * @param integer $user_id
 */
  public function add( $salt, $user_id)
  {
    $product = $this->Products->find()
      ->where(['Products.salt' => $salt])
      ->first();

    if( !$product)
    {
      return ['error' => __d( 'app', 'El producto no existe')];
    }

    $data = [
      'product_id' => $product->id,
      'user_id' => $user_id
    ];


    if( $this->exists( $data))
    {
      return ['error' => __d( 'app', 'Añadido')];
    }

    $entity = $this->newEntity( $data);

    if( $this->save( $entity))
    {
      return ['success' => __d( 'app', '¡Añadido¡')];
    }

    return ['error' => __d( 'app', 'Ha habido un error al añadir a favoritos')];
  }


  public function remove( $salt, $user_id)
  {
    $product = $this->Products->find()
      ->where(['Products.salt' => $salt])
      ->first();

    if( !$product)
    {
      return ['error' => __d( 'app', 'El producto no existe')];
    }

    $data = [
      'product_id' => $product->id,
      'user_id' => $user_id
    ];

    $entity = $this->find()->where([$data])->first();

    if( !$entity)
    {
      return ['error' => __d( 'app', 'El producto no estaba añadido a favoritos')];
    }

    if( $this->delete( $entity))
    {
      return ['success' => __d( 'app', 'El producto ha sido eliminado de favoritos')];
    }

    return ['error' => __d( 'app', 'Ha habido un error al eliminar el producto de favoritos')];
  }
}
