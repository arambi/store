<?php

namespace Store\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;
use Store\Model\Entity\State;

/**
 * States Model
 */
class StatesTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        $this->setTable('store_states');
        $this->setDisplayField('title');
        $this->setPrimaryKey('id');
        $this->addBehavior('Timestamp');

        // Associations
        $this->belongsTo('Zones', [
            'className' => 'Store.Zones'
        ]);

        $this->belongsTo('Countries', [
            'className' => 'Store.Countries'
        ]);

        $this->addBehavior('Manager.Crudable');

        // CRUD
        $this->crud->associations(['Zones', 'Countries']);

        $this->crud
            ->addFields([
                'active' => __d('admin', 'Activo'),
                'title' => __d('admin', 'Nombre'),
                'postcode' => [
                    'label' => __d('admin', 'Código postal'),
                    'help' => 'Los 2 primeros números del código postal de la provincia'
                ],
                'zone' => [
                    'label' => __d('admin', 'Zona'),
                    'type' => 'BelongsTo'
                ],
                'country' => [
                    'label' => __d('admin', 'País'),
                    'type' => 'BelongsTo'
                ],
                'no_taxes' => __d('admin', 'Libre de impuestos'),
            ])
            ->addIndex('index', [
                'fields' => [
                    'title',
                    'active',
                    'zone',
                    'postcode',
                    'no_taxes',
                ],
                'actionButtons' => ['create'],
                'saveButton' => false,
            ])
            ->setName([
                'singular' => __d('admin', 'Provincia'),
                'plural' => __d('admin', 'Provincias'),
            ])
            ->addView('create', [
                'columns' => [
                    [
                        'title' => __d('admin', 'Datos del producto'),
                        'box' => [
                            [
                                'elements' => [
                                    'title',
                                    'active',
                                    'no_taxes',
                                    'zone',
                                    'country',
                                    'postcode'
                                ]
                            ]
                        ]
                    ],
                ],
                'actionButtons' => ['create', 'index'],
            ], ['update']);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        return $validator;
        $validator
            ->add('id', 'valid', ['rule' => 'numeric'])
            ->allowEmpty('id', 'create')
            ->requirePresence('title', 'create')
            ->notEmpty('title')
            ->add('country_id', 'valid', ['rule' => 'numeric'])
            ->allowEmpty('country_id')
            ->add('zone_id', 'valid', ['rule' => 'numeric'])
            ->allowEmpty('zone_id')
            ->allowEmpty('iso_code')
            ->add('active', 'valid', ['rule' => 'boolean'])
            ->requirePresence('active', 'create')
            ->notEmpty('active');

        return $validator;
    }
}
