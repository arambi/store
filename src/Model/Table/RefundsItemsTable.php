<?php

namespace Store\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\Table;
use Cake\ORM\RulesChecker;
use Store\Config\StoreConfig;
use Cake\Validation\Validator;

/**
 * RefundsItems Model
 *
 * @method \Store\Model\Entity\RefundsItem get($primaryKey, $options = [])
 * @method \Store\Model\Entity\RefundsItem newEntity($data = null, array $options = [])
 * @method \Store\Model\Entity\RefundsItem[] newEntities(array $data, array $options = [])
 * @method \Store\Model\Entity\RefundsItem|false save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \Store\Model\Entity\RefundsItem saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \Store\Model\Entity\RefundsItem patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \Store\Model\Entity\RefundsItem[] patchEntities($entities, array $data, array $options = [])
 * @method \Store\Model\Entity\RefundsItem findOrCreate($search, callable $callback = null, $options = [])
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class RefundsItemsTable extends Table
{
    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('store_refunds_items');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');

        // Behaviors
        $this->addBehavior('Manager.Crudable');

        $this->belongsTo('LineItems', [
            'className' => 'Store.LineItems',
            'foreignKey' => 'line_item_id',
        ]);

        $this->belongsTo('Refunds', [
            'className' => 'Store.Refunds',
            'foreignKey' => 'refund_id',
        ]);

        // CRUD Config
        //Escribe aquí las asociaciones vinculadas con el model a la hora de editar (si las hubiera)
        // $this->crud->associations([]);

        $this->crud
            ->addFields([
                'quantity' => __d('admin', 'Cantidad'),
                'status' => [
                    'label' => '',
                    'type' => 'select',
                    'options' => [
                        StoreConfig::ITEM_STATUS_REFUNDED_REQUEST => StoreConfig::itemStatusTitle(StoreConfig::ITEM_STATUS_REFUNDED_REQUEST),
                        StoreConfig::ITEM_STATUS_REFUNDED_CANCELED => StoreConfig::itemStatusTitle(StoreConfig::ITEM_STATUS_REFUNDED_CANCELED),
                        StoreConfig::ITEM_STATUS_REFUNDED_WORKING  => StoreConfig::itemStatusTitle(StoreConfig::ITEM_STATUS_REFUNDED_WORKING),
                        StoreConfig::ITEM_STATUS_REFUNDED_SEND => StoreConfig::itemStatusTitle(StoreConfig::ITEM_STATUS_REFUNDED_SEND),
                        StoreConfig::ITEM_STATUS_REFUNDED_RECEIVED => StoreConfig::itemStatusTitle(StoreConfig::ITEM_STATUS_REFUNDED_RECEIVED),
                        StoreConfig::ITEM_STATUS_REFUNDED_ACCEPT => StoreConfig::itemStatusTitle(StoreConfig::ITEM_STATUS_REFUNDED_ACCEPT),
                        StoreConfig::ITEM_STATUS_REFUNDED_REFUSED => StoreConfig::itemStatusTitle(StoreConfig::ITEM_STATUS_REFUNDED_REFUSED),
                    ]
                ],
            ])
            ->addIndex('index', [
                'fields' => [
                    'status',
                    'quantity',
                ],
                'actionButtons' => ['create'],
                'saveButton' => false,
            ])
            ->setName([
                'singular' => __d('admin', 'Productos de devolución'),
                'plural' => __d('admin', 'Productos de devolución'),
            ])
            ->addView('create', [
                'columns' => [
                    [
                        'cols' => 8,
                        'box' => [
                            [
                                'elements' => [
                                    'status',
                                    'quantity',
                                ]
                            ]
                        ]
                    ]
                ],
                'actionButtons' => ['create', 'index']
            ], ['update']);
    }

    public function findFront(Query $query)
    {
        return $query;
    }
}
