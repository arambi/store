<?php
namespace Store\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * MrwDeliveries Model
 *
 * @property \Store\Model\Table\OrdersTable&\Cake\ORM\Association\BelongsTo $Orders
 *
 * @method \Store\Model\Entity\MrwDelivery get($primaryKey, $options = [])
 * @method \Store\Model\Entity\MrwDelivery newEntity($data = null, array $options = [])
 * @method \Store\Model\Entity\MrwDelivery[] newEntities(array $data, array $options = [])
 * @method \Store\Model\Entity\MrwDelivery|false save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \Store\Model\Entity\MrwDelivery saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \Store\Model\Entity\MrwDelivery patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \Store\Model\Entity\MrwDelivery[] patchEntities($entities, array $data, array $options = [])
 * @method \Store\Model\Entity\MrwDelivery findOrCreate($search, callable $callback = null, $options = [])
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class MrwDeliveriesTable extends Table
{
    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('store_mrw_deliveries');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');
        $this->addBehavior('Timestamp');
    }

    public function findPendents(Query $query)
    {
        return $query->where([
            'MrwDeliveries.proccesed' => false
        ]);
    }
    
    public function add($content_id, $model, $reverse_address = false)
    {
        $entity = $this->newEntity([
            'content_id' => $content_id,
            'model' => $model,
            'reverse_address' => $reverse_address,
        ]);

        $this->save($entity);
    }
}
