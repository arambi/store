<?php
namespace Store\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\ORM\TableRegistry;
use Cake\Validation\Validator;
use Store\Model\Entity\PointsAddsSend;
use Cake\Mailer\MailerAwareTrait;
use Cake\Collection\Collection;

/**
 * PointsAddsSends Model
 *
 * @property \Cake\ORM\Association\BelongsTo $Sends
 * @property \Cake\ORM\Association\BelongsTo $Users
 */
class PointsAddsSendsTable extends Table
{
  use MailerAwareTrait;
  
  /**
   * Initialize method
   *
   * @param array $config The configuration for the Table.
   * @return void
   */
  public function initialize(array $config)
  {
    parent::initialize($config);

    $this->table('store_points_adds_sends');
    $this->displayField('id');
    $this->primaryKey('id');

    $this->addBehavior('Timestamp');

    $this->belongsTo('PointsAdds', [
        'foreignKey' => 'send_id',
        'className' => 'Store.PointsAdds'
    ]);
    $this->belongsTo('Users', [
        'foreignKey' => 'user_id',
        'joinType' => 'INNER',
        'className' => 'Store.Users'
    ]);
  }

  public function findUsers( $content)
  {
    $query = $this->Users->find();

    if( !empty( $content->groups))
    {
      $group_ids = (new Collection( $content->groups))->extract( 'id')->toArray();
      $query->where([
        'Users.group_id IN' => $group_ids
      ]);
    }

    if( !empty( $content->countries) || !empty( $content->states))
    {
      $this->Users->hasOne( 'Addresses', [
        'className' => 'Store.Addresses'
      ]);

      $query->contain([
        'Addresses'
      ]);
    }

    if( !empty( $content->countries))
    {
      $country_ids = (new Collection( $content->countries))->extract( 'id')->toArray();
      
      $query->where([
        'Addresses.country_id IN' => $country_ids
      ]);
    }

    if( !empty( $content->states))
    {
      $state_ids = (new Collection( $content->states))->extract( 'id')->toArray();
      
      $query->where([
        'Addresses.state_id IN' => $state_ids
      ]);
    }

    if( !empty( $content->usersfrom))
    {
      $query->where([
        'Users.created >=' => $content->usersfrom
      ]);
    }

    if( !empty( $content->usersto))
    {
      $query->where([
        'Users.created <=' => $content->usersto
      ]);
    }

    if( !empty( $content->userstop))
    {
      $this->Users->hasOne( 'Orders', [
        'className' => 'Store.Orders'
      ]);

      $q = TableRegistry::get( 'Store.Orders')->find()
        ->group(['Orders.user_id'])
        ->contain( 'Users')
        ->select( 'Users.id');

      $q->select(['number' => $q->func()->count( '*')]);
      $q->having([
        'number > ' . $content->userstop
      ]);

      $ids = $q->extract( 'user.id')->toArray();

      unset( $ids [0]);

      if( !empty( $ids))
      {
        $query->andWhere([
          'Users.id IN' => $ids
        ]);
      }
    }

    return $query;
  }

  public function add( $users, $point_add_id)
  {
    $Users = TableRegistry::get( 'User.Users');

    // Borramos los usuarios que han sido guardados anteriormente y ahora no cumplen las condiciones
    // Lo haremos al final
    $guards_ids = $this->find()
      ->where([
        'PointsAddsSends.send_id' => $point_add_id
      ])
      ->select( ['user_id'])
      ->extract( 'user_id')->toArray();

    $new_ids = $users->extract( 'id')->toArray();
    $diff = array_diff( $guards_ids, $new_ids);


    foreach( $users as $key => $user)
    {
      $has = $this->find()
        ->where([
          'PointsAddsSends.user_id' => $user->id,
          'PointsAddsSends.send_id' => $point_add_id
      ])->first();

      if( $has)
      {
        continue;
      }

      $data = [
        'user_id' => $user->id,
        'send_id' => $point_add_id,
        'email' => $user->email,
      ];

      $entity = $this->newEntity( $data);
      $this->save( $entity);
    }

    // Borramos los usuarios que han sido guardados anteriormente y ahora no cumplen las condiciones
    $this->deleteAll([
      'user_id IN' => $diff,
      'send_id' => $point_add_id
    ]);
  }

  public function send( $send)
  {
    $this->getMailer( 'Store.PointsSend')->send( 'sending', [
        $send,
    ]);
    $send->set( 'send_on', date( 'Y-m-d H:i:s'));
    $this->save( $send);
    $this->Points = TableRegistry::get( 'Store.Points');
    $this->Points->addPoints([
      'administrator_id' => $send->points_add->user_id,
      'action' => 'up',
      'points' => $send->points_add->data->points,
      'user_id' => $send->user_id
    ]);
  }

  /**
   * Default validation rules.
   *
   * @param \Cake\Validation\Validator $validator Validator instance.
   * @return \Cake\Validation\Validator
   */
  public function validationDefault(Validator $validator)
  {
      $validator
          ->add('id', 'valid', ['rule' => 'numeric'])
          ->allowEmpty('id', 'create');

      $validator
          ->add('send_on', 'valid', ['rule' => 'datetime']);

      $validator
          ->add('email', 'valid', ['rule' => 'email'])
          ->allowEmpty('email');

      return $validator;
  }

}
