<?php

namespace Store\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\Table;
use Cake\Event\Event;
use Cake\ORM\RulesChecker;
use Store\Config\StoreConfig;
use Cake\Validation\Validator;
use Store\Model\Entity\Address;

/**
 * Addresses Model
 */
class AddressesTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        $this->table('store_addresses');
        $this->displayField('alias');
        $this->primaryKey('id');

        // Behaviors
        $this->addBehavior('Timestamp');
        $this->addBehavior('Manager.Crudable');
        $this->addBehavior('Cofree.Saltable');

        $this->belongsTo('Countries', [
            'className' => 'Store.Countries',
            'foreignKey' => 'country_id'
        ]);

        $this->belongsTo('States', [
            'className' => 'Store.States',
            'foreignKey' => 'state_id'
        ]);

        $this->crud
            ->addFields([
                'firstname' => [
                    'label' => __d('admin', 'Nombre'),
                ],
            ])
            ->setName([
                'singular' => __d('admin', 'Dirección'),
                'plural' => __d('admin', 'Direcciones'),
            ]);
    }

    public function byUser($user_id)
    {
        $query = $this->find()
            ->where([
                'Addresses.user_id' => $user_id
            ])
            ->contain([
                'Countries',
                'States'
            ]);

        return $query;
    }

    public function saveFromOrder($order)
    {
        if (empty($order->user_id)) {
            return;
        }

        $fields = [
            'firstname',
            'lastname',
            'company',
            'address',
            'country_id',
            'state_id',
            'city',
            'phone',
            'postcode',
            'vat_number',
            'email',
        ];

        foreach (['invoice', 'delivery'] as $type) {
            $saveObj = new \stdClass();
            $saveObj->save = true;

            $event = new Event('Store.Model.Addresses.saveFromOrder', $this, [
                $order,
                $type,
                $saveObj,
            ]);
    
            $this->getEventManager()->dispatch($event);

            if (!$saveObj->save) {
                continue;
            }
            
            if ($order->{'save_adr_' . $type} || StoreConfig::getConfig('registerMandatory') || $order->want_account) {
                $entity = $this->find()
                    ->where([
                        'Addresses.user_id' => $order->user_id,
                        'type' => $type
                    ])
                    ->first();

                if (!$entity) {
                    $entity = $this->newEntity([
                        'type' => $type,
                        'user_id' => $order->user_id
                    ]);
                }

                foreach ($fields as $field) {
                    $column = 'adr_' . $type . '_' . $field;
                    $entity->set($field, $order->get($column));
                }

                $this->save($entity);
            }
        }
    }

    /**
     * Devuelve la primera dirección encontrada para un id de usuario
     * 
     * @param  integer $user_id
     * @return integer | null
     */
    public function getUserAddressId($user_id)
    {
        $address = $this->find()
            ->where([
                $this->alias() . '.user_id' => $user_id
            ])
            ->first();

        if ($address) {
            return $address->id;
        }

        return null;
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function _validationDefault(Validator $validator)
    {
        $validator
            ->add('id', 'valid', ['rule' => 'numeric'])
            ->add('country_id', 'valid', ['rule' => 'numeric'])
            ->add('state_id', 'valid', ['rule' => 'numeric'])
            // ->notEmpty( 'firstname')
            // ->notEmpty( 'lastname')
            ->notEmpty('address1')
            ->notEmpty('postcode')
            ->add('postcode', 'valid', ['rule' => 'numeric', 'message' => __d('app', 'El campo debe de ser un número')])
            ->notEmpty('city')
            // ->notEmpty( 'alias')
            // ->notEmpty( 'phone')
        ;

        return $validator;
    }
}
