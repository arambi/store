<?php
namespace Store\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;
use Store\Model\Entity\Brand;
use Cake\Core\Configure;

/**
 * Brands Model
 */
class BrandsTable extends Table
{

  /**
   * Initialize method
   *
   * @param array $config The configuration for the Table.
   * @return void
   */
  public function initialize(array $config)
  {
      $this->table( 'store_brands');
      $this->displayField( 'title');
      $this->primaryKey( 'id');

      // Behaviors
      $this->addBehavior( 'Manager.Crudable');
      $this->addBehavior( 'Cofree.Sluggable');
      $this->addBehavior( 'Cofree.Saltable');
      $this->addBehavior( 'Upload.UploadJsonable', [
        'fields' => [
          'photo',
        ]
      ]);
      $this->addBehavior( Configure::read( 'I18n.behavior'), [
        'fields' => ['body'],
        'translationTable' => 'Store.ShopI18n',
      ]);

      // CRUD Config
      //Escribe aquí las asociaciones vinculadas con el model a la hora de editar (si las hubiera)
      // $this->crud->associations([]);

      $this->crud
        ->addFields([
          'published' => __d( 'admin', 'Publicado'),
          'title' => 'Título',
          'slug' => 'Enlace',
          'body' => 'Cuerpo',
          'logo' => [
            'type' => 'upload',
            'label' => 'Logotipo',
            'config' => [
              'type' => 'post',
              'size' => 'thm'
            ]
          ],
          
        ])
        ->addIndex( 'index', [
          'fields' => [
            'title',
          ],
          'actionButtons' => ['create'],
          'saveButton' => false,
        ])
        ->setName( [
          'singular' => __d( 'admin', 'Marcas'),
          'plural' => __d( 'admin', 'Marca'),
        ])
        ->addView( 'create', [
          'columns' => [
            [
              'cols' => 8,
              'box' => [
                [
                  'elements' => [
                    'title',
                    'published',
                    'slug',
                    'logo',
                    'body'
                  ]
                ]
              ]
            ]
          ]
        ], ['update'])
        ;
      
  }
}
