<?php

namespace Store\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * OrderUserSessions Model
 *
 * @method \Store\Model\Entity\OrderUserSession get($primaryKey, $options = [])
 * @method \Store\Model\Entity\OrderUserSession newEntity($data = null, array $options = [])
 * @method \Store\Model\Entity\OrderUserSession[] newEntities(array $data, array $options = [])
 * @method \Store\Model\Entity\OrderUserSession|false save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \Store\Model\Entity\OrderUserSession saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \Store\Model\Entity\OrderUserSession patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \Store\Model\Entity\OrderUserSession[] patchEntities($entities, array $data, array $options = [])
 * @method \Store\Model\Entity\OrderUserSession findOrCreate($search, callable $callback = null, $options = [])
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class OrderUserSessionsTable extends Table
{
    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('store_order_user_sessions');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');

        // Behaviors
        $this->addBehavior('Manager.Crudable');

        $this->hasMany( 'OrderUserActions', [
          'className' => 'Store.OrderUserActions',
          'foreignKey' => 'session_id',
          'sort' => [
              'OrderUserActions.created'
          ]
        ]);

        // CRUD Config
        //Escribe aquí las asociaciones vinculadas con el model a la hora de editar (si las hubiera)
        $this->crud->associations(['OrderUserActions']);

        $this->crud
            ->addFields([
                'session_id' => [
                    'type' => 'info',
                    'label' => 'ID Sesión'
                ],
                'platform' => [
                    'type' => 'info',
                    'label' => 'Sistema'
                ],
                'browser' => [
                    'type' => 'info',
                    'label' => 'Navegador'
                ],
                'ip_map' => [
                    'type' => 'info',
                    'label' => 'IP'
                ],
                'user' => [
                    'type' => 'info',
                    'label' => 'Usuario',
                    'template' => 'Store.fields.session_user',
                    'templateIndex' => 'Store.indexes.session_user'
                ],
                'order_last_status' => [
                    'type' => 'info',
                    'label' => 'Última acción',
                ],
                'created' => [
                    'type' => 'info',
                    'label' => 'Fecha de creación'
                ],
                'actions' => [
                    'type' => 'info',
                    'template' => 'Store.fields.session_actions'
                ],
                'has_errors' => [
                    'type' => 'info',
                    'label' => 'Con errores'
                ]
            ])
            ->addIndex('index', [
                'fields' => [
                    'platform',
                    'browser',
                    'ip_map',
                    'user',
                    'order_last_status',
                    'has_errors',
                    'created',
                ],
                'actionButtons' => [],
                'saveButton' => false,
            ])
            ->setName([
                'singular' => __d('admin', 'Sesiones de compra'),
                'plural' => __d('admin', 'Sesiones de compra'),
            ])
            ->addView('create', [
                'columns' => [
                    [
                        'cols' => 8,
                        'box' => [
                            [
                                'elements' => [
                                    'platform',
                                    'browser',
                                    'ip_map',
                                    'user',
                                    'order_last_status',
                                    'created',
                                    'actions',
                                ]
                            ]
                        ]
                    ]
                ],
                'actionButtons' => ['index']
            ], ['update'])
            ->order([
                'OrderUserSessions.created' => 'desc'
            ]);
    }

    public function findFront(Query $query)
    {
        return $query;
    }
}
