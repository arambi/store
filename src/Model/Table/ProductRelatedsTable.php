<?php
namespace Store\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;
use Store\Model\Entity\ProductRelated;
use Cake\Core\Configure;


/**
 * ProductRelateds Model
 *
 */
class ProductRelatedsTable extends Table
{

  /**
   * Initialize method
   *
   * @param array $config The configuration for the Table.
   * @return void
   */
  public function initialize(array $config)
  {
    $this->table('store_products');
    $this->displayField('title');
    $this->primaryKey('id');
    $this->addBehavior('Timestamp');

    // Behaviors
    $this->addBehavior( 'Manager.Crudable');
    $this->addBehavior( 'Cofree.Contentable', [
      'type' => 'Products'
    ]);

    $this->addBehavior( Configure::read( 'I18n.behavior'), [
      'fields' => ['title', 'body'],
      'referenceName' => 'Products',
      'translationTable' => 'Store.ShopI18n',
    ]);
  }

  public function attachBehaviors()
  {
    $this->addBehavior( 'Slug.Sluggable');
    

    $this->addBehavior( 'Taxonomy.Taxonomic', [
      'Store.CategoriesShops' => [
        'associationType' => 'belongsToMany',
      ],
      'Tags' => [
        'associationType' => 'belongsToMany',
        'type' => 'taggable'
      ]
    ]);
  }

  /**
   * Default validation rules.
   *
   * @param \Cake\Validation\Validator $validator Validator instance.
   * @return \Cake\Validation\Validator
   */
  public function validationDefault(Validator $validator)
  {
      return $validator;
  }

}
