<?php
namespace Store\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;
use Store\Model\Entity\Collection;
use Cake\Core\Configure;
use Store\Config\StoreConfig;

/**
 * Collections Model
 */
class CollectionsTable extends Table
{

  /**
   * Initialize method
   *
   * @param array $config The configuration for the Table.
   * @return void
   */
  public function initialize(array $config)
  {
      parent::initialize($config);

      $this->table('store_collections');
      $this->displayField('title');
      $this->primaryKey('id');

      $this->addBehavior('Timestamp');
      
      // Behaviors
      $this->addBehavior( 'Manager.Crudable');
      $this->addBehavior( 'Slug.Sluggable');
      $this->addBehavior( 'Cofree.Saltable');
      $this->addBehavior( 'Upload.UploadJsonable', [
        'fields' => [
          'photo',
        ]
      ]);

      $this->addBehavior( Configure::read( 'I18n.behavior'), [
        'fields' => ['title', 'body']
      ]);
      $this->addBehavior( 'Cofree.Publisher');


      $this->belongsToMany( 'Products', [
        'joinTable' => 'store_collections_products_products',
        'foreignKey' => 'collection_id',
        'targetForeignKey' => 'product_id',
        'className' => StoreConfig::getProductsModel()->registryAlias(),
      ]);
      


      // CRUD Config
      //Escribe aquí las asociaciones vinculadas con el model a la hora de editar (si las hubiera)
      $this->crud->associations( ['Products', 'Attributes', 'AttributeGroups']);

      $this->crud
        ->addFields([
          'title' => __d( 'admin', 'Título'),
          'products_colors' => [
            'type' => 'autocomplete',
            'label' => __d( 'admin', 'Productos/colores'),
            'help' => __d( 'admin', 'Escribe los productos que quieres que estén asociados a esta colección.'),
            'template' => 'Shop/fields/products_colors'
          ],
          'products' => [
            'type' => 'autocomplete',
            'label' => __d( 'admin', 'Productos'),
            'help' => __d( 'admin', 'Escribe los productos que quieres que estén asociados a esta colección.'),
            'template' => 'Shop/fields/products_colors_products'
          ],
          'published' => __d( 'admin', 'Publicado'),
          'date_restrict' => [
            'label' => __d( 'admin', 'Publicado durante un tiempo'),
          ],
          'start_on' => [
            'label' => __d( 'admin', 'Publicado desde'),
            'show' => 'content.date_restrict == 1'
          ],
          'finish_on' => [
            'label' => __d( 'admin', 'Publicado hasta'),
            'show' => 'content.date_restrict == 1'
          ],
          'photo' => [
            'type' => 'upload',
            'label' => 'Foto',
            'config' => [
              'type' => 'post',
              'size' => 'thm'
            ]
          ],
          'body' => [
            'label' => __d( 'admin', 'Cuerpo de texto'),
            'type' => 'ckeditor'
          ]
        ])
        ->addIndex( 'index', [
          'fields' => [
            'title',
            'published',
            'start_on',
            'finish_on',
          ],
          'actionButtons' => ['create'],
          'saveButton' => false,
        ])
        ->setName( [
          'singular' => __d( 'admin', 'Colecciones'),
          'plural' => __d( 'admin', 'Colecciones'),
        ])
        ->addView( 'create', [
          'columns' => [
            [
              'title' => 'General',
              'cols' => 8,
              'box' => [
                [
                  'elements' => [
                    'title',
                    'slugs' => [
                      'after' => function( $crud){
                        $url = \Cake\Routing\Router::url([
                          'prefix' => false,
                          'plugin' => 'Store',
                          'controller' => 'Collections',
                          'action' => 'index'
                        ], true);
                        return '<a target="_blank" href="'.  $url .'/{{ data.content.slugs[0].slug }}"><i class="fa fa-link"></i></a> '. $url .'/{{ data.content.slugs[0].slug }}';
                      }
                    ],
                    'published',
                    'date_restrict',
                    'start_on',
                    'finish_on',
                    'photo',
                    'products_colors',
                    'products',
                    'body'
                  ]
                ]
              ]
            ]
          ],
          'actionButtons' => ['create', 'index']
        ], ['update'])
      ;
    
    $this->crud->order([
      'Collections.start_on' => 'desc'
    ]);
    
    $this->addBehavior( 'Seo.Seo');

  }
}
