<?php
namespace Store\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;
use Store\Model\Entity\PaymentMethod;
use Cake\Core\Plugin;
use Cake\Filesystem\Folder;
use Cake\Event\Event;
use I18n\Lib\Lang;
/**
 * PaymentMethods Model
 */
class PaymentMethodsTable extends Table
{

  /**
   * Initialize method
   *
   * @param array $config The configuration for the Table.
   * @return void
   */
  public function initialize(array $config)
  {
    $this->table('store_payment_methods');
    $this->displayField('title');
    $this->primaryKey('id');

    $this->addBehavior( 'Manager.Crudable');
    $this->addBehavior( 'Cofree.Saltable');
    $this->addBehavior( 'Cofree.Sortable');
    $this->addBehavior( 'Cofree.BooleanUnique', [
        'fields' => [
            'by_default'
        ]
    ]);

    $this->addBehavior( 'Cofree.Jsonable', [
      'fields' => ['settings']
    ]);

    $this->addBehavior( 'I18n.I18nTranslate', [
      'fields' => ['title', 'subtitle', 'title_success', 'text_success']
    ]);

    $this->belongsTo( 'Taxes', [
      'className' => 'Store.Taxes',
      'foreignKey' => 'tax_id'
    ]);
    

    $this->crud
      ->addFields([
        'active' => __d( 'admin', 'Activo'),
        'title' => __d( 'admin', 'Nombre'),
        'subtitle' => __d( 'admin', 'Descripción'),
        'processor' => [
          'type' => 'select',
          'label' => __d( 'admin', 'Procesador de pago'),
          'options' => $this->paymentProcessors()
        ],
        'pay_for_used' => __d( 'admin', 'Conlleva un coste'),
        'cost_type' => [
          'type' => 'select',
          'label' => __d( 'admin', 'Método de cobro'),
          'options' => [
            'percent' => 'Porcentaje',
            'fixed' => 'Valor fijo'
          ],
          'show' => 'content.pay_for_used'
        ],
        'price' => [
          'label' => __d( 'admin', 'Valor/Precio'),
          'show' => 'content.pay_for_used'
        ],
        'settings' => [
          'type' => 'text',
          'label' => __d( 'admin', 'Configuración'),
          'template' => 'Store/fields/payment_settings',
          'options' => $this->paymentProcessorsSettings()
        ],
        'title_success' => __d( 'admin', 'Título de la página de finalización de compra'),
        'text_success' => __d( 'admin', 'Texto de la página de finalización de compra'),
        'for_users' => [
          'label' => __d( 'admin', 'Solo para algunos usuarios'),
          'help' => __d( 'admin', 'Al marcar esta opción, este método de pago estará disponible solo para los usuarios a los que así se indique')
        ]
      ])
      ->addIndex( 'index', [
        'fields' => [
          'title',
          'active',
          'processor',
          'for_users'
        ],
        'actionButtons' => ['create'],
        'saveButton' => false,
      ])
      ->setName( [
        'singular' => __d( 'admin', 'Método de pago'),
        'plural' => __d( 'admin', 'Métodos de pago'),
      ])
      ->addView( 'create', [
        'columns' => [
          [
            'cols' => 8,
            'box' => [
              [
                'elements' => [
                  'title',
                  'subtitle',
                  'active',
                  'processor',
                  'pay_for_used',
                  'cost_type',
                  'price',
                  'settings',
                  'for_users',
                  'title_success',
                  'text_success'
                ]
              ],
            ],
          ]
        ],
        'actionButtons' => ['create', 'index'],
      ], ['update'])
      ;
  }

  public function getValids( $user)
  {
    $query = $this->find();

    if( !empty( $user ['payment_methods']))
    {
      $query->where([
        $this->alias() .'.active' => true,
        'OR' => [
          'PaymentMethods.for_users' => false,
          [
            'PaymentMethods.id IN' => collection( $user ['payment_methods'])->extract( 'id')->toArray(),
            'PaymentMethods.for_users' => true,
          ]
        ]
      ]);
    }
    else
    {
      $query->where([
        $this->alias() .'.active' => true,
        $this->alias() .'.for_users' => false
      ]);
    }
    
    $event = new Event( 'Store.Model.PaymentMethods.valids', $this, [$query, $user]);
    $this->eventManager()->dispatch( $event);

    return $query;
  }

  public function paymentProcessors()
  {
    $processors = $this->processors();

    foreach( $processors as $name)
    {
      $className = '\Store\\Payment\\Processor\\'. $name .'Payment';

      $title = isset($className::$name [Lang::current( 'iso3')]) ? $className::$name [Lang::current( 'iso3')] : $className::$name ['spa'];
      $return [strtolower( $name)] = $title;
    }

    return $return;
  }

  private function processors()
  {
    $path = Plugin::path( 'Store') .'src' .DS. 'Payment' .DS. 'Processor';

    $folder = new Folder( $path);
    $dir = $folder->read()[1];

     $return = [];

    foreach( $dir as $filename)
    {
      $name = str_replace( 'Payment.php', '', $filename);
      $return [] = $name;
    }

    return $return;
  }

  public function paymentProcessorsSettings()
  {
    $processors = $this->processors();

    $out = [];

    foreach( $processors as $name)
    {
      $className = '\Store\\Payment\\Processor\\'. $name .'Payment';
      $out [] = [
        'name' => strtolower( $name),
        'keys' => $className::settings()
      ];
    }

    return $out;
  }

  /**
   * Default validation rules.
   *
   * @param \Cake\Validation\Validator $validator Validator instance.
   * @return \Cake\Validation\Validator
   */
  public function validationDefault(Validator $validator)
  {

      return $validator;
  }
}
