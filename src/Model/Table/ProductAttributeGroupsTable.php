<?php
namespace Store\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * ProductAttributeGroups Model
 *
 * @property \Store\Model\Table\ProductsTable|\Cake\ORM\Association\BelongsTo $Products
 * @property \Store\Model\Table\AttributeGroupsTable|\Cake\ORM\Association\BelongsTo $AttributeGroups
 *
 * @method \Store\Model\Entity\ProductAttributeGroup get($primaryKey, $options = [])
 * @method \Store\Model\Entity\ProductAttributeGroup newEntity($data = null, array $options = [])
 * @method \Store\Model\Entity\ProductAttributeGroup[] newEntities(array $data, array $options = [])
 * @method \Store\Model\Entity\ProductAttributeGroup|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \Store\Model\Entity\ProductAttributeGroup patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \Store\Model\Entity\ProductAttributeGroup[] patchEntities($entities, array $data, array $options = [])
 * @method \Store\Model\Entity\ProductAttributeGroup findOrCreate($search, callable $callback = null, $options = [])
 */
class ProductAttributeGroupsTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('store_product_attribute_groups');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');
       
    }

}
