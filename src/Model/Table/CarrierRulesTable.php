<?php

namespace Store\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;
use Store\Model\Entity\CarrierRule;

/**
 * CarrierRules Model
 */
class CarrierRulesTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        $this->setTable('store_carrier_rules');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        // Behaviors
        $this->addBehavior('Manager.Crudable');
        $this->addBehavior('Cofree.Saltable');

        $this->belongsTo('Carriers', [
            'className' => 'Store.Carriers',
            'foreignKey' => 'carrier_id'
        ]);

        // Associations
        $this->belongsToMany('Zones', [
            'className' => 'Store.Zones',
            'foreignKey' => 'carrier_rule_id',
            'targetForeignKey' => 'zone_id',
            'joinTable' => 'store_carrier_rule_zones',
        ]);

        // CRUD Config
        //Escribe aquí las asociaciones vinculadas con el model a la hora de editar (si las hubiera)
        $this->crud->associations(['Zones', 'Carriers', 'Taxes']);

        $this->crud->addJsFiles([
            '/store/js/directives.js',
        ]);
        
        $this->crud
            ->addFields([
                'carrier_id' => [
                    'type' => 'select',
                    'label' => 'Transporte',
                    'options' => function ($crud) {
                        return $this->Carriers->find('list');
                    }
                ],
                'type' => [
                    'type' => 'select',
                    'label' => __d('admin', 'Calcular precio por'),
                    'options' => [
                        'weight' => __d('admin', 'Peso (grs)'),
                        'price' => __d('admin', 'Precio')
                    ]
                ],
                'by_default' => [
                    'label' => __d('admin', 'Por defecto'),
                    'type' => 'boolean',
                    'help' => __d('admin', 'Regla por defecto para este tipo')
                ],
                'is_interval' => [
                    'label' => __d('admin', 'Aplicar cada N unidades'),
                    'type' => 'boolean',
                    'help' => __d('admin', 'Se aplicará el precio cada N unidades, por ejemplo, cada 1 kilo se sumará el precio indicado')
                ],
                'interval' => [
                    'label' => __d('admin', 'Cada'),
                    'show' => 'content.is_interval'
                ],
                'min' => [
                    'label' => __d('admin', 'Mínimo'),
                    'show' => '!content.is_interval'
                ],
                'max' => [
                    'label' => __d('admin', 'Máximo'),
                    'show' => '!content.is_interval'
                ],
                'from' => [
                    'label' => __d('admin', 'A partir de'),
                    'show' => 'content.is_interval'
                ],
                'price' => [
                    'label' => __d('admin', 'Precio'),
                    'template' => 'Store.fields.price_rules',
                    'show' => '!content.is_interval'
                ],
                'price_from' => [
                    'label' => __d('admin', 'Precio inicial'),
                    'template' => 'Store.fields.price_rules_from',
                    'show' => 'content.is_interval'
                ],
                'price_interval' => [
                    'label' => __d('admin', 'Precio por unidad'),
                    'template' => 'Store.fields.price_rules_interval',
                    'show' => 'content.is_interval'
                ],
                'is_reduction' => [
                    'label' => __d('admin', 'Tarifa reducida'),
                    'help' => __d('admin', 'Indica esta opción si la tarifa es reducida'),
                ],
                'deadline' => [
                    'label' => __d('admin', 'Días de entrega'),
                    'type' => 'numeric',
                    'range' => [1, 15],
                ],
                'delivery_type' => [
                    'type' => 'select',
                    'label' => __d('admin', 'Tipo de envío'),
                    'options' => [
                        'normal' => __d('admin', 'Normal'),
                        'express' => __d('admin', 'Express')
                    ]
                ],
                'zones' => [
                    'type' => 'select_multiple',
                    'label' => __d('admin', 'Zonas'),
                ],
            ])
            ->addIndex('index', [
                'fields' => [
                    'delivery_type',
                    'by_default',
                    'type',
                    'min',
                    'max',
                    'is_interval',
                    'from',
                    'interval',
                    'price',
                    'is_reduction',
                    'deadline',
                    'zones',
                ],
                'filters' => [
                    'carrier_id'
                ],
                'actionButtons' => [
                    function ($crud) {
                        if ($crud->get('carrier_id')) {
                            return [
                                'label' => 'Nuevo',
                                'action' => 'create',
                                'url' => '#admin/store/carrier_rules/create?carrier_id=' . $crud->get('carrier_id')
                            ];
                        }
                    }
                ],
                'actionsBox' => [
                    [
                        'method' => 'delete',
                        'title' => 'Borrar'
                    ]
                ],
                'saveButton' => false,
            ])
            ->setName([
                'singular' => __d('admin', 'Reglas de pago'),
                'plural' => __d('admin', 'Reglas de pago'),
            ])
            ->addView('create', [
                'columns' => [
                    [
                        'cols' => 8,
                        'box' => [
                            [
                                'elements' => [
                                    'delivery_type',
                                    'by_default',
                                    'type',
                                    'is_interval',
                                    'from',
                                    'interval',
                                    'min',
                                    'max',
                                    'price',
                                    'price_from',
                                    'price_interval',
                                    'is_reduction',
                                    'deadline',
                                    'zones'
                                ]
                            ]
                        ]
                    ]
                ],
                'actionButtons' => [
                    function ($crud) {
                        if ($crud->get('carrier_id')) {
                            return [
                                'label' => 'Listado',
                                'action' => 'index',
                                'url' => '#admin/store/carrier_rules/index/' . $crud->get('carrier_id')
                            ];
                        }
                    }
                ],
            ], ['update'])
            ->addFilters([
                'carrier_id' => [
                    'filter' =>  function ($table, $query, $param) {
                        $query->where([
                            'CarrierRules.carrier_id' => $param
                        ]);
                    },
                    'type' => 'info',
                    'label' => 'Agencia'
                ],
            ])
            ->defaults([
                'delivery_type' => 'normal',
                'type' => 'weight',
            ]);
    }
}
