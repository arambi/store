<?php
namespace Store\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;
use Store\Model\Entity\CategoriesShop;
use Slug\Model\Entity\SlugTrait;
use Cake\Core\Configure;

/**
 * Category Model
 */
class CategoriesShopsTable extends Table
{
  /**
   * Initialize method
   *
   * @param array $config The configuration for the Table.
   * @return void
   */
  public function initialize(array $config)
  {
    $this->table( 'taxonomy_terms');
    $this->displayField( 'title');
    $this->primaryKey( 'id');

    // Associations
    $this->belongsTo( 'Sizes', [
      'className' => 'Store.Sizes',
      'foreignKey' => 'related_id'
    ]);

    // Behaviors
    $this->addBehavior( 'Timestamp');
    $this->addBehavior( Configure::read( 'I18n.behavior'), [
      'fields' => ['title', 'body'],
    ]);

    $this->addBehavior( 'Cofree.Saltable');
    $this->addBehavior( 'Cofree.Contentable');
    $this->addBehavior( 'Manager.Crudable');
    $this->addBehavior( 'Slug.Sluggable');

    $this->addBehavior( 'Upload.UploadJsonable', [
      'fields' => [
        'photo'
      ]
    ]);

    $this->crud->associations( ['Sizes']);
    
    // CRUD Config
    $this->crud
      ->addFields([
        'title' => __d( 'admin', 'Nombre'),
        'slug' => [
          'label' => __d( 'admin', 'URL amigable'),
          'help' => __d( 'admin', ' Sólo está permitidos los caracteres alfanuméricos, los guiones medio (-) y bajo (_).')
        ],
        'published' => __d( 'admin', 'Publicado'),
        'size' => [
          'label' => __d( 'admin', 'Talla'),
        ],
        'body' => [
          'label' => __d( 'admin', 'Entradilla'),
        ],
        'photo' => [
          'type' => 'upload',
          'label' => 'Foto',
          'config' => [
            'type' => 'post',
            'size' => 'thm'
          ]
        ],
      ])->addIndex( 'index', [
        'fields' => [
          'title',
          'published'
        ],
        'actionButtons' => ['create'],
        'template' => 'Manager/tree',
      ])->setName( [
        'singular' => __d( 'admin', 'Categoría'),
        'plural' => __d( 'admin', 'Categorías'),
      ])->addView( 'create', [
        'columns' => [
          [
            'title' => 'General',
            'cols' => 8,
            'box' => [
              [
                'elements' => [
                  'title',
                  'slugs',
                  'published',
                  'size',
                  'photo',
                  'body',
                ]
              ]
            ]
          ]
        ],
        'actionButtons' => ['index', 'create'],
      ], ['update']);

      $this->addBehavior( 'Seo.Seo');
  }


}
