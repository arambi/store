<?php
namespace Store\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;
use Store\Model\Entity\PromosProduct;

/**
 * PromosProducts Model
 *
 * @property \Cake\ORM\Association\BelongsTo $Promos
 * @property \Cake\ORM\Association\BelongsTo $Products
 */
class PromosProductsTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->table( 'store_promos_products');
        $this->displayField( 'id');
        $this->primaryKey( 'id');

        /*
        $this->belongsTo( 'Promos', [
            'foreignKey' => 'promo_id',
            'className' => 'Store.Promos'
        ]);
        */
    /*
        $this->belongsTo( 'ProductsColors', [
            'foreignKey' => 'product_id',
            'className' => 'Store.ProductsColors'
        ]);*/
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {   
        return $validator;
        $validator
            ->add('id', 'valid', ['rule' => 'numeric'])
            ->allowEmpty('id', 'create');

        return $validator;
    }


}
