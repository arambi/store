<?php
namespace Store\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Taxes Model
 *
 * @method \Store\Model\Entity\Tax get($primaryKey, $options = [])
 * @method \Store\Model\Entity\Tax newEntity($data = null, array $options = [])
 * @method \Store\Model\Entity\Tax[] newEntities(array $data, array $options = [])
 * @method \Store\Model\Entity\Tax|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \Store\Model\Entity\Tax patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \Store\Model\Entity\Tax[] patchEntities($entities, array $data, array $options = [])
 * @method \Store\Model\Entity\Tax findOrCreate($search, callable $callback = null)
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class TaxesTable extends Table
{

  /**
   * Initialize method
   *
   * @param array $config The configuration for the Table.
   * @return void
   */
  public function initialize(array $config)
  {
    parent::initialize($config);

    $this->table( 'store_taxes');
    $this->displayField('title');
    $this->primaryKey('id');

    $this->addBehavior('Timestamp');
    
    // Behaviors
    $this->addBehavior( 'Manager.Crudable');
    $this->addBehavior( 'Cofree.Saltable');
    $this->addBehavior( 'I18n.I18nTranslate', [
      'fields' => ['title']
    ]);

    $this->hasMany( 'TaxRatings', [
      'className' => 'Store.TaxRatings',
      'foreignKey' => 'tax_id',
    ]);

    // CRUD Config
    //Escribe aquí las asociaciones vinculadas con el model a la hora de editar (si las hubiera)
    $this->crud->associations(['TaxRatings', 'Countries']);

    $this->crud
      ->addFields([
        'active' => __d( 'admin', 'Activo'),
        'title' => [
          'label' => __d( 'admin', 'Nombre'),
          'help' => __d( 'admin', 'Nombre de la tasa de impuestos que se mostrará en los carritos y en las facturas (ejemplo "VAT). - Caracteres inválidos <>;=#{}')
        ],
        'value' => [
          'label' => __d( 'admin', 'Tasa'),
          'help' => __d( 'admin', 'Formato: XX.XX o XX.XXX (por ejemplo: 19.60 o 13.925) - Caracteres inválidos <>;=#{}')
        ],
        'tax_ratings' => [
          'label' => __d( 'admin', 'Zonas'),
          'type' => 'hasMany'
        ]
      ])
      ->addIndex( 'index', [
        'fields' => [
          'title',
          'value',
          'active'       
        ],
        'actionButtons' => ['create'],
        'saveButton' => false,
      ])
      ->setName( [
        'singular' => __d( 'admin', 'Impuesto'),
        'plural' => __d( 'admin', 'Impuestos'),
      ])
      ->addView( 'create', [
        'columns' => [
          [
            'box' => [
              [
                'elements' => [
                  'title',
                  'value',
                  'active',
                  'tax_ratings'
                ]
              ]
            ]
          ],
        ],
        'actionButtons' => ['index'],
      ], ['update'])
      
      ;

    
  }

  public function calculate( $rate, $total)
  {
    return round( ($rate * $total) / 100, 2);
  }
}
