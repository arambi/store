<?php
namespace Store\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;
use Cake\ORM\Entity;
use Cake\Event\Event;
use Store\Model\Entity\CategorieShopsRelation;

/**
 * Categorings Model
 */
class CategoriesShopsRelationsTable extends Table
{

  /**
   * Initialize method
   *
   * @param array $config The configuration for the Table.
   * @return void
   */
  public function initialize(array $config)
  {
      $this->table( 'taxonomy_relationships');
      $this->displayField( 'id');
      $this->primaryKey( 'id');
      $this->addBehavior( 'Timestamp');
  }

/**
 * Pone en la tabla relacional, el nombre del model que se relaciona
 *   
 * @param  Event  $event  
 * @param  Entity $entity 
 * @return void         
 */
  public function beforeSave( Event $event, Entity $entity)
  {
    $associations = $event->subject()->associations();

    foreach( $associations->keys() as $key)
    {
      if( $associations->get( $key)->table() == 'taxonomy_terms')
      {
        $entity->model = $associations->get( $key)->alias();
      }
    }
  }

  public function beforeFind( Event $event, Query $query)
  {
    $query->where( [$this->alias() . '.model' => 'CategoriesShops']);
  }

  /**
   * Default validation rules.
   *
   * @param \Cake\Validation\Validator $validator Validator instance.
   * @return \Cake\Validation\Validator
   */
  public function validationDefault(Validator $validator)
  {
      $validator
          ->add('id', 'valid', ['rule' => 'numeric'])
          ->allowEmpty('id', 'create')
          ->add('content_id', 'valid', ['rule' => 'numeric'])
          ->requirePresence('content_id', 'create')
          ->notEmpty('content_id')
          ->add('term_id', 'valid', ['rule' => 'numeric'])
          ->requirePresence('term_id', 'create')
          ->notEmpty('term_id')
          ->allowEmpty('model');

      return $validator;
  }
}
