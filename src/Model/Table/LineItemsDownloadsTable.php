<?php
namespace Store\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * LineItemsDownloads Model
 *
 * @property \Store\Model\Table\LineItemsTable&\Cake\ORM\Association\BelongsTo $LineItems
 *
 * @method \Store\Model\Entity\LineItemsDownload get($primaryKey, $options = [])
 * @method \Store\Model\Entity\LineItemsDownload newEntity($data = null, array $options = [])
 * @method \Store\Model\Entity\LineItemsDownload[] newEntities(array $data, array $options = [])
 * @method \Store\Model\Entity\LineItemsDownload|false save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \Store\Model\Entity\LineItemsDownload saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \Store\Model\Entity\LineItemsDownload patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \Store\Model\Entity\LineItemsDownload[] patchEntities($entities, array $data, array $options = [])
 * @method \Store\Model\Entity\LineItemsDownload findOrCreate($search, callable $callback = null, $options = [])
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class LineItemsDownloadsTable extends Table
{
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('store_line_items_downloads');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');
        $this->addBehavior('Timestamp');

        $this->belongsTo('LineItems', [
            'foreignKey' => 'line_item_id',
            'className' => 'Store.LineItems',
        ]);
    }

    public function create($item_id)
    {
        $this->save($this->newEntity([
            'line_item_id' => $item_id,
        ]));
    }

}
