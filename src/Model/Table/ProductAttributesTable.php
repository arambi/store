<?php
namespace Store\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;
use Store\Model\Entity\ProductAttribute;
use Cake\ORM\TableRegistry;
use Store\Config\StoreConfig;

/**
 * ProductAttributes Model
 */
class ProductAttributesTable extends Table
{
  /**
   * Initialize method
   *
   * @param array $config The configuration for the Table.
   * @return void
   */
  public function initialize(array $config)
  {
    $this->table( 'store_product_attributes');
    $this->displayField( 'id');
    $this->primaryKey( 'id');


    // Associations
    $this->belongsTo( 'Products', [
      'className' => StoreConfig::getProductsModel()->registryAlias(),
      'foreignKey' => 'product_id'     
    ]);

    $this->belongsToMany( 'Attributes', [
      'joinTable' => 'store_product_attribute_combinations',
      'through' => 'Store.ProductAttributeCombinations',
      'foreignKey' => 'product_attribute_id',
      'targetForeignKey' => 'attribute_id',
      'className' => 'Store.Attributes',
      // 'saveStrategy' => 'replace'
    ]);

    // Behaviors
    $this->addBehavior( 'Timestamp');
    $this->addBehavior( 'Manager.Crudable');
    $this->addBehavior( 'Cofree.Saltable');

    // CRUD Config
    $this->crud->associations( ['Attributes']);

    $this->crud
      ->addFields([
        'store_reference' => [
          'label' => __d( 'admin', 'Código de referencia'),
          'help' => __d( 'admin', 'Referencia interna para este producto')
        ],
        'store_ean13' => [
          'label' => __d( 'admin', 'EAN-13 o código de barra JAN'),
          'help' => __d( 'admin', 'Este tipo de código de producto es específico de Europa y Japón, pero es ampliamente utilizado a nivel internacional. Es más utilizado que el código UPC: todos los productos con un EAN serán aceptados en Norteamérica.')
        ],
        'attributes' => [
          'label' => __d( 'admin', 'Combinaciones'),
          'template' => 'Store.fields.attributes',
          'options' => function( $crud){
            return TableRegistry::get( 'Store.AttributeGroups')->getOptions(); 
          }
        ],
        'published' => __d( 'admin', 'Publicado'),
        'store_price' => [
          'label' => __d( 'admin', 'Precio sin I.V.A.'),
          'template' => 'Store.fields.price_attribute',
          'help' => __d( 'app', 'Introduce el precio con o sin impuestos. El precio es calculado automáticamente. Para introducir decimales, utiliza el punto')
        ],
        'quantity' => [
          'label' => __d( 'admin', 'Stock disponible'),
          'help' => __d( 'admin', 'Número de artículos disponibles'),
          'show' => '!data.content.without_stock'
        ],
      ])
      ->addIndex( 'index', [
        'fields' => [
          'store_price',
          'published',
          'attributes',
          'quantity'
        ],
        'actionButtons' => ['create'],
        'saveButton' => false,
      ])
      ->setName( [
        'singular' => __d( 'admin', 'Combinación de producto'),
        'plural' => __d( 'admin', 'Combinaciones de producto'),
      ])
      ->addView( 'create', [
        'columns' => [
          [
            'box' => [
              [
                'elements' => [
                  'published',
                  'attributes',
                  'quantity',
                  'store_price',
                  'store_reference',
                  'store_ean13',
                  
                ]
              ]
            ]
          ]
        ],
      ], ['update'])
      ->defaults([
        'published' => true
      ])
      ;
  }


  /**
   * Default validation rules.
   *
   * @param \Cake\Validation\Validator $validator Validator instance.
   * @return \Cake\Validation\Validator
   */
  public function validationDefault(Validator $validator)
  {
    return $validator;
    $validator
        ->add('store_price', 'valid', ['rule' => 'numeric'])
        ->add('weight', 'valid', ['rule' => 'numeric'])
        ->allowEmpty('weight')
        ->add('minimal_quantity', 'valid', ['rule' => 'numeric'])
        ->allowEmpty('minimal_quantity')
        ->allowEmpty('reference')
        ->allowEmpty('ean13')
        ->allowEmpty('upc')
        ->add('quantity', 'valid', ['rule' => 'numeric'])
        ->allowEmpty('quantity')
        ->add('available_date', 'valid', ['rule' => 'date'])
        ->requirePresence('available_date', 'create')
        ->notEmpty('available_date');

    return $validator;
  }
}
