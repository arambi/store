<?php
namespace Store\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;
use Store\Model\Entity\Erp;

/**
 * Erp Model
 *
 * @property \Cake\ORM\Association\BelongsTo $Erps
 * @property \Cake\ORM\Association\BelongsTo $Stores
 */
class ErpTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->table('store_erp');
        $this->displayField('id');
        $this->primaryKey('id');
    }


    public function findRelative( $erp_id, $model)
    {
      return $this->find()
      ->where([
        'model' => $model,
        'erp_id' => $erp_id
      ])->first();
    }
}
