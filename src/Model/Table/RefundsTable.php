<?php

namespace Store\Model\Table;

use ArrayObject;
use Cake\ORM\Query;
use Cake\ORM\Table;
use Cake\Event\Event;
use Website\Lib\Website;
use Cake\ORM\RulesChecker;
use Cake\ORM\TableRegistry;
use Cake\Utility\Inflector;
use Store\Config\StoreConfig;
use Cake\Validation\Validator;
use Letter\Mailer\MailerAwareTrait;

/**
 * Refunds Model
 *
 * @method \Store\Model\Entity\Refund get($primaryKey, $options = [])
 * @method \Store\Model\Entity\Refund newEntity($data = null, array $options = [])
 * @method \Store\Model\Entity\Refund[] newEntities(array $data, array $options = [])
 * @method \Store\Model\Entity\Refund|false save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \Store\Model\Entity\Refund saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \Store\Model\Entity\Refund patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \Store\Model\Entity\Refund[] patchEntities($entities, array $data, array $options = [])
 * @method \Store\Model\Entity\Refund findOrCreate($search, callable $callback = null, $options = [])
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class RefundsTable extends Table
{
    use MailerAwareTrait;

    private $mailersMap = [
        StoreConfig::REFUND_STATUS_REVIEW => [
            'user',
            'admin',
        ],
        StoreConfig::REFUND_STATUS_PROCCESS => [
            'user',
            'admin',
        ],
        StoreConfig::REFUND_STATUS_SENT => [
            'user',
            'admin',
        ],
        StoreConfig::REFUND_STATUS_RECEIVED => [
            'user',
            'admin',
        ],
        StoreConfig::REFUND_STATUS_FINALIZED => [
            'user',
            'admin',
        ],
    ];


    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('store_refunds');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');

        // Behaviors
        $this->addBehavior('Manager.Crudable');

        $this->belongsTo('Orders', [
            'className' => 'Store.Orders',
            'foreignKey' => 'order_id',
        ]);

        $this->hasMany('RefundsItems', [
            'className' => 'Store.RefundsItems',
            'foreignKey' => 'refund_id',
        ]);

        $this->hasMany('RefundsHistories', [
            'className' => 'Store.RefundsHistories',
            'foreignKey' => 'refund_id',
        ]);

        // CRUD Config
        //Escribe aquí las asociaciones vinculadas con el model a la hora de editar (si las hubiera)
        $this->crud->associations(['RefundsItems', 'LineItems']);

        $this->crud
            ->setName([
                'singular' => __d('admin', 'Devoluciones'),
                'plural' => __d('admin', 'Devoluciones'),
            ])
            ->addFields([
                'id' => 'id',
                'order' => [
                    'label' =>  'Pedido',
                    'type' => 'info',
                    'template' => 'Store.fields/refund_order_info'
                ],
                'generate_invoice' => [
                    'label' => 'Generar factura al guardar',
                    'type' => 'boolean',
                    'show' => 'content.status == "' . StoreConfig::REFUND_STATUS_FINALIZED . '" && content.total > 0'
                ],
                'shipping_coast' => [
                    'label' => 'Con gastos de envío',
                    'type' => 'boolean',
                    'show' => 'content.generate_invoice'
                ],
                'items' => [
                    'type' => 'info',
                    'template' => 'Store.fields/refund_items'
                ],
                'refunds_items' => [
                    'type' => 'hasMany',
                    'label' => false
                ],
                'refunds_save_button' => [
                    'type' => 'info',
                    'template' => 'Store.fields/refund_save_button',
                    'show' => 'content.status == "' . StoreConfig::REFUND_STATUS_REVIEW . '" || content.status == "' . StoreConfig::REFUND_STATUS_RECEIVED . '"'
                ],
                'status_human' => [
                    'type' => 'info',
                    'template' => 'Store.fields/refund_status',
                    'label' => 'Estado',
                ],
                'status' => [
                    'label' => 'Estado',
                    'type' => 'select',
                    'options' => [
                        StoreConfig::REFUND_STATUS_REVIEW => StoreConfig::refundStatusTitle(StoreConfig::REFUND_STATUS_REVIEW),
                        StoreConfig::REFUND_STATUS_PROCCESS => StoreConfig::refundStatusTitle(StoreConfig::REFUND_STATUS_PROCCESS),
                        StoreConfig::REFUND_STATUS_SENT => StoreConfig::refundStatusTitle(StoreConfig::REFUND_STATUS_SENT),
                        StoreConfig::REFUND_STATUS_RECEIVED => StoreConfig::refundStatusTitle(StoreConfig::REFUND_STATUS_RECEIVED),
                        StoreConfig::REFUND_STATUS_FINALIZED => StoreConfig::refundStatusTitle(StoreConfig::REFUND_STATUS_FINALIZED),
                    ]
                ],
            ])
            ->addIndex('index', [
                'template' => 'Manager/index',
                'deleteLink' => false,
                'fields' => [
                    'order_id' => 'Pedido',
                    'status_human' => [
                        'type' => 'info',
                    ],
                    'created' => 'Fecha',
                ],
                'actionButtons' => [],
            ])
            ->addView('update', [
                'saveButton' => true,
                'columns' => [
                    [
                        'cols' => 8,
                        'box' => [
                            [
                                'elements' => [
                                    'order',
                                    'items',
                                    'status_human',
                                    'refunds_save_button',
                                ]
                            ],
                            [
                                'show' => 'content.status == "' . StoreConfig::REFUND_STATUS_FINALIZED . '" && content.total > 0',
                                'title' => 'Factura de devolución',
                                'elements' => [
                                    'generate_invoice',
                                    'shipping_coast',
                                ]
                            ]
                        ]
                    ]
                ],
                'actionButtons' => ['index']
            ])
            ->order([
                'Refunds.modified' => 'desc'
            ]);
    }

    public function beforeFind(Event $event, Query $query)
    {
        $event = new Event('Store.Model.Refunds.beforeFind', $this, [
            $query,
        ]);

        $this->getEventManager()->dispatch($event);
    }

    public function addItem($item_id, $quantity)
    {
        $item = TableRegistry::getTableLocator()->get('Store.LineItems')->find()
            ->where([
                'LineItems.id' => $item_id
            ])
            ->first();

        if (!$item) {
            return;
        }

        $listeners = $this->getEventManager()->listeners('Store.Model.Refunds.addItem');

        if (!empty($listeners)) {
            $event = new Event('Store.Model.Refunds.addItem', $this, [
                $item,
                $quantity
            ]);

            $this->getEventManager()->dispatch($event);
            return;
        }

        $refund = $this->find()
            ->where([
                'Refunds.order_id' => $item->order_id,
            ])
            ->first();

        if (!$refund) {
            $refund = $this->newEntity([
                'order_id' => $item->order_id,
                'status' => StoreConfig::REFUND_STATUS_REVIEW,
                'refunds_items' => [[
                    'line_item_id' => $item->id,
                    'quantity' => $quantity,
                    'to_refund' => true,
                ]]
            ]);

            $this->save($refund);
            $this->addStatus($refund->id, StoreConfig::REFUND_STATUS_REVIEW);
        } else {
            $refund_item = $this->RefundsItems->newEntity([
                'refund_id' => $refund->id,
                'line_item_id' => $item->id,
                'quantity' => $quantity,
                'status' => StoreConfig::ITEM_STATUS_REFUNDED_REQUEST,
            ]);

            $this->RefundsItems->save($refund_item);
        }
    }

    public function addStatus($id, $status, $user_id = null)
    {
        $refund = $this->find()
            ->contain([
                'Orders'
            ])
            ->where([
                'Refunds.id' => $id
            ])
            ->first();

        if (!$refund) {
            return false;
        }

        $this->changeItemsStatus($refund->id, $status);

        $user = $this->Orders->Users->find()
            ->where(['Users.id' => $refund->order->user_id])
            ->first();

        $last = $this->getLastStatus($refund->id);

        if (!$last || $last->status != $status) {
            $data = [
                'refund_id' => $refund->id,
                'status' => $status,
                'user_id' => $user_id
            ];

            $entity = $this->RefundsHistories->newEntity($data);

            if ($this->RefundsHistories->save($entity)) {
                $this->query()->update()
                    ->set('status', $status)
                    ->where(['id' => $refund->id])
                    ->execute();

                $event = new Event('Store.Model.Refunds.addStatus', $this, [
                    $refund,
                    $status
                ]);

                $this->getEventManager()->dispatch($event);
                $this->sendMailStatus($status, $refund, $user);
                return true;
            }
        }
    }


    public function changeItemsStatus($refund_id, $status)
    {
        $map = [
            StoreConfig::REFUND_STATUS_REVIEW => [
                0 => StoreConfig::ITEM_STATUS_REFUNDED_REQUEST,
                1 => StoreConfig::ITEM_STATUS_REFUNDED_REQUEST
            ],
            StoreConfig::REFUND_STATUS_PROCCESS => [
                0 => StoreConfig::ITEM_STATUS_REFUNDED_WORKING,
                1 => StoreConfig::ITEM_STATUS_REFUNDED_CANCELED
            ],
            StoreConfig::REFUND_STATUS_SENT => [
                0 => StoreConfig::ITEM_STATUS_REFUNDED_SEND
            ],
            StoreConfig::REFUND_STATUS_RECEIVED => [
                0 => StoreConfig::ITEM_STATUS_REFUNDED_RECEIVED,
            ],
            StoreConfig::REFUND_STATUS_FINALIZED => [
                0 => StoreConfig::ITEM_STATUS_REFUNDED_ACCEPT,
                1 => function ($item) {
                    if ($item->status != StoreConfig::ITEM_STATUS_REFUNDED_CANCELED) {
                        $item->set('status', StoreConfig::ITEM_STATUS_REFUNDED_REFUSED);
                    }
                }
            ],
        ];

        $items = $this->RefundsItems->findByRefundId($refund_id);

        foreach ($items as $item) {
            $key = (int)$item->refuse;

            if (isset($map[$status]) && isset($map[$status][$key])) {
                if (is_callable($map[$status][$key])) {
                    $func = $map[$status][$key];
                    $func($item);
                } else {
                    $item->set('status', $map[$status][$key]);
                }

                $this->RefundsItems->save($item);
            }
        }
    }

    public function sendMailStatus($status, $refund, $user)
    {
        $dontsends = StoreConfig::getConfig('dontSendStatus');

        if (in_array($status, $dontsends)) {
            return;
        }

        if (in_array('user', $this->mailersMap[$status])) {
            $locale = $user ? $user->locale : ($refund->order->locale ? $refund->order->locale : false);
            $key = 'refund' . Inflector::camelize($status);
            $email = $user ? $user->email : $refund->order->adr_invoice_email;
            $this->getMailer('Store.Orders')->send($key, [
                $refund,
                $email,
            ], [], $locale);
        }

        if (in_array('admin', $this->mailersMap[$status])) {
            $email_admin = Website::get('settings.store_order_email');

            if (empty($email_admin)) {
                $email_admin = Website::get('settings.users_reply_email');
            }

            $emails = new ArrayObject(explode(',', $email_admin));

            $event = new Event('Store.Model.LineItems.itemsEmailsMailer', $this, [
                $emails,
                $refund,
                $status
            ]);

            $this->getEventManager()->dispatch($event);

            foreach ($emails as $email) {
                $key = 'refund' . Inflector::camelize($status) . 'Admin';
                $this->getMailer('Store.Orders')->send($key, [
                    $refund,
                    $email,
                ], [], $locale);
            }
        }
    }

    public function getLastStatus($id)
    {
        $refund = $this->find()
            ->where([
                'Refunds.id' => $id
            ])
            ->contain([
                'RefundsHistories' => function ($q) {
                    return $q
                        ->limit(1);
                }
            ])
            ->first();

        if ($refund && isset($refund->refunds_histories[0])) {
            return $refund->refunds_histories[0];
        }

        return false;
    }

    public function createInvoiceRefund($item, $shipping_coast = false)
    {
        if (!StoreConfig::getConfig('withInvoices')) {
            return;
        }

        TableRegistry::getTableLocator()->get('Store.InvoiceRefunds')->createFromRefund($item, $shipping_coast);
    }
}
