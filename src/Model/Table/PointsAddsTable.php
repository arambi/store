<?php
namespace Store\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;
use Store\Model\Entity\PointsAdd;
use Cake\ORM\TableRegistry;
use Cake\Event\Event;
use Cake\Collection\Collection;
use Cake\Core\Configure;

/**
 * PointsAdds Model
 */
class PointsAddsTable extends Table
{

  /**
   * Initialize method
   *
   * @param array $config The configuration for the Table.
   * @return void
   */
  public function initialize(array $config)
  {
    parent::initialize($config);

    $this->table('store_points_adds');
    $this->displayField('title');
    $this->primaryKey('id');

    $this->addBehavior('Timestamp');
    
    // Behaviors
    $this->addBehavior( 'Manager.Crudable');
    $this->addBehavior( 'Cofree.Saltable');
    $this->addBehavior( Configure::read( 'I18n.behavior'), [
      'fields' => ['title','subject','body']
    ]);

    $this->addBehavior( 'Cofree.Jsonable', [
      'fields' => ['data']
    ]);

    // CRUD Config
    //Escribe aquí las asociaciones vinculadas con el model a la hora de editar (si las hubiera)
    $this->crud->associations([]);

    $this->crud
      ->addFields([
        'title' => __d( 'admin', 'Título'),
        'subject' => __d( 'admin', 'Tema'),
        'variables' => [
          'type' => 'string',
          'template' => 'Letter.fields.variables'
        ],
        'active' => [
          'label' => 'Activo',
          'type' => 'boolean'
        ],
        'send_on' => [
          'label' => 'Enviar no antes de',
          'type' => 'datetime'
        ],
        'finish_on' => [
          'label' => 'No enviar después de',
          'type' => 'datetime'
        ],
        'body' => __d( 'admin', 'Cuerpo de texto'),
        'send' => [
          'label' => __d( 'admin', 'Enviar al guardar'),
          'type' => 'boolean',
        ],
        'points' => [
          'label' => __d( 'admin', 'Número de puntos'),
          'type' => 'numeric',
          'spin' => true,
          'range' => [1, 100000]
        ],
        'groups' => [
          'label' => __d( 'admin', 'Grupos de usuarios'),
          'type' => 'select_multiple',
          'options' => function( $crud){
            $groups = TableRegistry::get( 'User.Groups')->find()->toArray();
            $content = $crud->getContent();
            
            if( !empty( $content) && isset( $content ['groups']))
            {
              $_content = $content ['groups'];
              $this->setTicked( $_content, $groups);
            }
              
            return $groups;
          },
        ],
        'countries' => [
          'label' => __d( 'admin', 'Países'),
          'type' => 'select_multiple',
          'options' => function( $crud){
            $countries = TableRegistry::get( 'Store.Countries')->find()->toArray();
            $content = isset( $crud->getContent()['countries']) ? $crud->getContent()['countries'] : [];
            $this->setTicked( $content, $countries);
            return $countries;
          },
        ],
        'states' => [
          'label' => __d( 'admin', 'Provincias'),
          'type' => 'select_multiple',
          'options' => function( $crud){
            $states = TableRegistry::get( 'Store.States')->find()->toArray();
            $content = isset( $crud->getContent()['states']) ? $crud->getContent()['states'] : [];
            $this->setTicked( $content, $states);
            return $states;
          },
        ],
        'userstop' => [
          'label' => __d( 'admin', 'Usuarios que hayan hecho un número de compras'),
          'type' => 'numeric',
        ],
        'usersfrom' => [
          'label' => __d( 'admin', 'Usuarios que se hayan registrado desde la fecha'),
          'type' => 'date',
        ],
        'usersto' => [
          'label' => __d( 'admin', 'Usuarios que se hayan registrado hasta la fecha'),
          'type' => 'date',
        ],
        'preview' => [
          'type' => 'string',
          'template' => 'Store.fields/preview_addpoints'
        ],
      ])
      ->addIndex( 'index', [
        'fields' => [
          'title',
          'subject',
          'send_on',
          'finish_on',
          'active',
          'groups'
        ],
        'actionButtons' => ['create'],
        'saveButton' => false,
      ])
      ->setName( [
        'singular' => __d( 'admin', 'Anadir puntos'),
        'plural' => __d( 'admin', 'Anadir puntos'),
      ])
      ->addView( 'create', [
        'columns' => [
          [
            'cols' => 8,
            'box' => [
              [
                'elements' => [
                  'preview',
                  'active',
                  'send_on',
                  'finish_on',
                  'title',
                  'subject',
                  'variables',
                  'body',
                  'points',
                  'groups',
                  'countries',
                  'states',
                  'userstop',
                  'usersfrom',
                  'usersto',
                ]
              ],
              [
                'title' => __d( 'admin', 'Usuarios'),
                'elements' => [
                  'search' => [
                    'type' => 'string',
                    'template' => 'Store.fields/points_sends_search'
                  ],
                ]
              ]
            ],
          ]
        ],
        'actionButtons' => ['create', 'index']
      ], ['update'])

      ->setContent([
        'vars' => [
          'points' => 'Número de puntos',
          'name' => 'Nombre del usuario',
        ]
      ])
      ;
      
  }

  public function setTicked( $content, $rows)
  {
    $ids = (new Collection($content))->extract( 'id')->toArray();

    foreach( $rows as $row)
    {
      if( in_array( $row->id, $ids))
      {
        $row->ticked = true;
      }
    }
  }

  public function beforeFind( Event $event, Query $query, $options)
  {
    $query->formatResults( function( $results) {
        return $this->_rowMapper( $results);
    }, $query::PREPEND);
  }

  protected function _rowMapper( $results)
  {
    return $results->map( function ($row) {
      
      if( !empty( $row->data))
      {
        $data = json_decode( $row->data, true);

        foreach( $data as $key => $value)
        {
          $row->set( $key, $value);
        }
      }

      return $row;
    });
  }

  public function findActives()
  {
    $query = $this->find()
      ->where([
        'PointsAdds.active' => true,
        'PointsAdds.send_on <' => date( 'Y-m-d'),
        'PointsAdds.finish_on >' => date( 'Y-m-d'),
      ]);

    return $query;
  }

}
