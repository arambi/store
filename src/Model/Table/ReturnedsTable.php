<?php
namespace Store\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;
use Store\Model\Entity\Returned;

/**
 * Returneds Model
 */
class ReturnedsTable extends Table
{

  /**
   * Initialize method
   *
   * @param array $config The configuration for the Table.
   * @return void
   */
  public function initialize(array $config)
  {
    parent::initialize($config);

    $this->table('store_returneds');
    $this->displayField('id');
    $this->primaryKey('id');

    
    // Associations
    $this->belongsTo( 'Users', [
      'foreignKey' => 'user_id',
      'className' => 'User.Users'
    ]);

    // Behaviors
    $this->addBehavior( 'Timestamp');
    $this->addBehavior( 'Manager.Crudable');
    $this->addBehavior( 'Cofree.Saltable');

    // CRUD Config
    //Escribe aquí las asociaciones vinculadas con el model a la hora de editar (si las hubiera)
    $this->crud->associations(['Users']);

    $this->crud
      ->addFields([
        'ordernumber' => __d( 'admin', 'Pedido'),
        'reason' => __d( 'admin', 'Motivo'),
        'comments' => __d( 'admin', 'Comentarios'),
        'user' => __d( 'admin', 'Usuario')
      ])
      ->addIndex( 'index', [
        'fields' => [
          'user',
          'ordernumber',
          'reason',
        ],
        'actionButtons' => ['create'],
        'saveButton' => false,
      ])
      ->setName( [
        'singular' => __d( 'admin', 'Devoluciones'),
        'plural' => __d( 'admin', 'Devoluciones'),
      ])
      ->addView( 'create', [
        'columns' => [
          [
            'cols' => 8,
            'box' => [
              [
                'elements' => [
                  'user' => [
                    'template' => 'Store.fields.returned_user'
                  ],
                  'ordernumber',
                  'reason',
                  'comments',
                ]
              ]
            ]
          ]
        ],
        'actionButtons' => ['create', 'index']
      ], ['update'])
      ;
    

    $this->reasons = [
      'change_size' => __d( 'app', 'Cambio de talla'),
      'total' => __d( 'app', 'Devolución total'),
      'partial' => __d( 'app', 'Devolución parcial'),
      'otros' => __d( 'app', 'Otros'),
    ];

    $this->statuses = [
      'new' => __d( 'app', 'Nuevo'),
      'proccessed' => __d( 'app', 'Procesado')
    ];
  }

  public function validationDefault( Validator $validator) 
  {
    $validator
      ->notEmpty( 'ordernumber', __d( 'app', 'El número de pedido es obligatorio'))
      ->notEmpty( 'reason', __d( 'app', 'Es necesario indicar una razón'));

    return $validator;
  }

}
