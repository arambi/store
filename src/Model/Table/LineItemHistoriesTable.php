<?php
namespace Store\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * LineItemHistories Model
 *
 * @property \Store\Model\Table\UsersTable&\Cake\ORM\Association\BelongsTo $Users
 * @property \Store\Model\Table\LineItemsTable&\Cake\ORM\Association\BelongsTo $LineItems
 *
 * @method \Store\Model\Entity\LineItemHistory get($primaryKey, $options = [])
 * @method \Store\Model\Entity\LineItemHistory newEntity($data = null, array $options = [])
 * @method \Store\Model\Entity\LineItemHistory[] newEntities(array $data, array $options = [])
 * @method \Store\Model\Entity\LineItemHistory|false save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \Store\Model\Entity\LineItemHistory saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \Store\Model\Entity\LineItemHistory patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \Store\Model\Entity\LineItemHistory[] patchEntities($entities, array $data, array $options = [])
 * @method \Store\Model\Entity\LineItemHistory findOrCreate($search, callable $callback = null, $options = [])
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class LineItemHistoriesTable extends Table
{
    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('store_line_items_histories');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');

        $this->belongsTo('Users', [
            'foreignKey' => 'user_id',
            'className' => 'Store.Users',
        ]);
        $this->belongsTo('LineItems', [
            'foreignKey' => 'line_item_id',
            'className' => 'Store.LineItems',
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmptyString('id', null, 'create');

        $validator
            ->scalar('status')
            ->maxLength('status', 32)
            ->allowEmptyString('status');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['user_id'], 'Users'));
        $rules->add($rules->existsIn(['line_item_id'], 'LineItems'));

        return $rules;
    }
}
