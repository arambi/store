<?php

namespace Store\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\Table;
use Cake\Event\Event;
use Website\Lib\Website;
use Cake\ORM\RulesChecker;
use Cake\Validation\Validator;
use Store\Model\Entity\Invoice;
use Store\Model\Table\InvoicesTrait;

/**
 * Invoices Model
 */
class InvoicesTable extends Table
{
    use InvoicesTrait;

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('store_invoices');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');

        // Behaviors
        $this->addBehavior('Manager.Crudable');
        $this->addBehavior('Cofree.Saltable');

        $this->belongsTo('Orders', [
            'className' => 'Store.Orders',
            'foreignKey' => 'order_id',
        ]);

        $this->_setCrud();

        $this->crud->addJsFiles([
            '/store/js/directives.js?'. time(),
        ]);

        $this->crud->setName([
            'singular' => __d('admin', 'Facturas'),
            'plural' => __d('admin', 'Facturas'),
        ]);

        $csv_fields = [
            'order_id' => 'ID de pedido',
            'invoice_full_number' => 'Número',
            'invoice_date' => 'Fecha',
            'adr_invoice_firstname' => 'Nombre',
            'adr_invoice_lastname' => 'Apellidos',
            'adr_invoice_company' => 'Empresa',
            'adr_invoice_vat_number' => 'CIF',
            'adr_invoice_address' => 'Dirección',
            'adr_invoice_postcode' => 'CP',
            'adr_invoice_city' => 'Población',
            'adr_invoice_state_title' => 'Provincia',
            'adr_invoice_country_title' => 'País',
            'shipping_handling_human' => 'Gastos de transporte',
            'total_without_taxes_human_no_tags' => 'Base Imponible',
            'total_taxes_human_no_tags' => 'IVA',
            'total_human_no_tags' => 'Total',
        ];

        $csv_fields = new \ArrayObject($csv_fields);

        $this->getEventManager()->dispatch(new Event('Store.Model.Invoices.csvFields', $this, [
            $csv_fields,
        ]));

        $this->crud->getView('index')
            ->set('exportCsv', true)
            ->set('csvFields', (array)$csv_fields);
    }

    public function create($order, $number = null)
    {
        if ($number == null) {
            $prefix = Website::get('settings.store_invoice_prefix');
            $first = Website::get('settings.store_invoice_first');
            $number = $this->getNextNumber($prefix, $first);
        }

        $data = [
            'order_id' => $order->id,
            'num' => $number,
            'invoice_date' => date('Y-m-d'),
        ];

        if (!empty($prefix)) {
            $data['prefix'] = $prefix;
        }

        $entity = $this->findByOrderId($order->id)->first();

        if (!$entity) {
            $entity = $this->newEntity($data);

            $this->getEventManager()->dispatch(new Event('Store.Model.Invoices.create', $this, [
                $entity,
                $order,
            ]));

            return $this->save($entity);
        }
    }
}
