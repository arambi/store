<?php
namespace Store\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;
use Store\Config\StoreConfig;

/**
 * PromosProductsProducts Model
 *
 * @property \Cake\ORM\Association\BelongsTo $Promos
 * @property \Cake\ORM\Association\BelongsTo $Products
 *
 * @method \Store\Model\Entity\PromosProductsProduct get($primaryKey, $options = [])
 * @method \Store\Model\Entity\PromosProductsProduct newEntity($data = null, array $options = [])
 * @method \Store\Model\Entity\PromosProductsProduct[] newEntities(array $data, array $options = [])
 * @method \Store\Model\Entity\PromosProductsProduct|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \Store\Model\Entity\PromosProductsProduct patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \Store\Model\Entity\PromosProductsProduct[] patchEntities($entities, array $data, array $options = [])
 * @method \Store\Model\Entity\PromosProductsProduct findOrCreate($search, callable $callback = null)
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class PromosProductsProductsTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->table('store_promos_products_products');
        $this->displayField('id');
        $this->primaryKey('id');

        $this->addBehavior('Timestamp');

        $this->belongsTo('Promos', [
            'foreignKey' => 'promo_id',
            'className' => 'Store.Promos'
        ]);
        $this->belongsTo('Products', [
            'foreignKey' => 'product_id',
            'className' => StoreConfig::getProductsModel()->registryAlias(),
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['promo_id'], 'Promos'));
        $rules->add($rules->existsIn(['product_id'], 'Products'));

        return $rules;
    }
}
