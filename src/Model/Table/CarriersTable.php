<?php

namespace Store\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\Table;
use Store\Cart\Cart;
use Cake\Event\Event;
use Cake\ORM\RulesChecker;
use Cake\ORM\TableRegistry;
use Cake\Event\EventManager;
use Store\Config\StoreConfig;
use Cake\Validation\Validator;
use Store\Model\Entity\Carrier;

/**
 * Carriers Model
 */
class CarriersTable extends Table
{

    public $types = array('weight', 'price');

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        $this->setTable('store_carriers');
        $this->setDisplayField('title');
        $this->setPrimaryKey('id');

        // Behaviors
        $this->addBehavior('Manager.Crudable');
        $this->addBehavior('Cofree.Saltable');
        $this->addBehavior('Cofree.BooleanUnique', [
            'fields' => [
                'by_default'
            ]
        ]);


        $this->belongsTo('Taxes', [
            'className' => 'Store.Taxes',
            'foreignKey' => 'tax_id'
        ]);


        $this->hasMany('CarrierRules', [
            'foreignKey' => 'carrier_id',
            'className' => 'Store.CarrierRules',
        ]);

        $this->hasMany('CarrierFestives', [
            'foreignKey' => 'carrier_id',
            'className' => 'Store.CarrierFestives',
            'conditions' => [
                'CarrierFestives.day >=' => date('Y-m-d')
            ]
        ]);

        // CRUD Config
        //Escribe aquí las asociaciones vinculadas con el model a la hora de editar (si las hubiera)
        $this->crud->associations([ 'Taxes','CarrierFestives']);
        // $this->crud->associations(['CarrierRules', 'Taxes', 'Zones', 'CarrierFestives']);
        $this->crud->addJsFiles([
            '/store/js/directives.js',
        ]);
        $this->crud
            ->addFields([
                'active' => __d('admin', 'Activo'),
                'title' => 'Título',
                'is_free' => __d('admin', 'Envío gratuito'),
                'tax' => [
                    'label' => __d('admin', 'Tipo de impuesto'),
                    'show' => '!content.is_free'
                ],
                'insurance' => __d('admin', 'Cobrar seguro de transporte'),
                'insurance_type' => [
                    'label' => __d('admin', 'Tipo de precio'),
                    'type' => 'select',
                    'options' => [
                        'fixed' => __d('admin', 'Precio fijo'),
                        'percent' => __d('admin', 'Porcentaje')
                    ],
                    'show' => 'content.insurance == 1'
                ],
                'insurance_value' => [
                    'label' => __d('admin', 'Valor'),
                    'show' => 'content.insurance == 1'
                ],
                'carrier_festives' => [
                    'label' => __d('admin', 'Días festivos'),
                    'type' => 'hasMany',
                    'show' => '!content.is_free'
                ],
                // 'carrier_rules' => [
                //     'label' => __d('admin', 'Reglas de pago'),
                //     'type' => 'hasMany',
                //     'show' => '!content.is_free'
                // ],
                'carrier_rules_link' => [
                    'label' => 'Reglas de pago',
                    'type' => 'info',
                    'template' => 'Store.fields.carrier_rules_link',
                ],
                'delivery_days' => [
                    'label' => __d('admin', 'Días de entrega'),
                    'type' => 'numeric',
                    'range' => [1, 15],
                ],
                'hour' => [
                    'label' => __d('admin', 'Hora de corte'),
                    'type' => 'select',
                    'options' => [
                        '08:00' => '08:00',
                        '08:30' => '08:30',
                        '09:00' => '09:00',
                        '09:30' => '09:30',
                        '10:00' => '10:00',
                        '10:30' => '10:30',
                        '11:00' => '11:00',
                        '11:30' => '11:30',
                        '12:00' => '12:00',
                        '12:30' => '12:30',
                        '13:00' => '13:00',
                        '13:30' => '13:30',
                        '14:00' => '14:00',
                        '14:30' => '14:30',
                        '15:00' => '15:00',
                        '15:30' => '15:30',
                        '16:00' => '16:00',
                        '16:30' => '16:30',
                        '17:00' => '17:00',
                        '17:30' => '17:30',
                        '18:00' => '18:00',
                        '18:30' => '18:30',
                        '19:00' => '19:00',
                        '19:30' => '19:30',
                        '20:00' => '20:00',
                        '20:30' => '20:30',
                        '21:00' => '21:00',
                        '21:30' => '21:30',
                        '22:00' => '22:00',
                        '22:30' => '22:30',
                    ],
                    'show' => '!content.is_free'
                ],
                'delivery_days_type' => [
                    'label' => __d('admin', 'Contando'),
                    'type' => 'select',
                    'options' => [
                        'business_days' => __d('admin', 'Días laborales'),
                        'all_days' => __d('admin', 'Todos los días')
                    ],
                    'show' => '!content.is_free'
                ],
            ])
            ->addIndex('index', [
                'fields' => [
                    'title',
                    'active',
                    'tax'
                ],
                'actionButtons' => ['create'],
                'saveButton' => false,
            ])
            ->setName([
                'singular' => __d('admin', 'Transporte'),
                'plural' => __d('admin', 'Transporte'),
            ])
            ->addView('create', [
                'columns' => [
                    [
                        'cols' => 8,
                        'title' => 'General',
                        'box' => [
                            [
                                'elements' => [
                                    'title',
                                    'active',
                                    'tax',
                                    'is_free',
                                ]
                            ],
                            [
                                'title' => __d('admin', 'Seguro de transporte'),
                                'elements' => [
                                    'insurance',
                                    'insurance_type',
                                    'insurance_value'
                                ]
                            ],
                            [
                                'title' => __d('admin', 'Entrega'),
                                'elements' => [
                                    // 'delivery_days',
                                    'delivery_days_type',
                                    'hour',
                                ]
                            ],

                            [
                                'elements' => [
                                    'carrier_rules_link',
                                ]
                            ]
                        ],
                    ],
                    [
                        'cols' => 8,
                        'title' => 'Días festivos',
                        'box' => [
                            [
                                'elements' => [
                                    'carrier_festives'
                                ]
                            ]
                        ]
                    ]
                ]
            ], ['update']);
    }

    public function getValids()
    {
        $query = $this->find()
            ->where([
                'Carriers.active' => true
            ]);

        return $query;
    }

    public function hasExpress($order)
    {
        $zone_id = $order->delivery_zone_id;
        $query = $this->CarrierRules->find()
            ->contain(['Carriers'])
            ->where([
                'Carriers.active' => true,
                'Carriers.id' => $order->carrier_id
            ])
            ->where(['CarrierRules.delivery_type' => 'express'])
            ->matching('Zones', function ($q) use ($zone_id) {
                return $q->where(['Zones.id' => $zone_id]);
            });

        return $query->count() > 0;
    }

    public function buildOrder($order)
    {
        if ($order->custom_shipping) {
            if (!Cart::hasTaxes($order)) {
                $tax_value = 0;
                $order->no_taxes = true;
                $order->set('shipping_handling_tax_rate', $tax_value);
                $order->set('shipping_handling_tax', $this->Taxes->calculate($order->shipping_handling_tax_rate, $order->shipping_handling));
                $this->setForFree($order);

                $event = new Event('Store.Model.Carriers.afterHandlingCost', $this, [
                    $order,
                ]);

                $this->getEventManager()->dispatch($event);
            } else {
                $order->set('shipping_handling_tax', $this->Taxes->calculate($order->shipping_handling_tax_rate, $order->shipping_handling));
            }

            return;
        }

        if ($order->no_carrier) {
            $order->set('carrier_rule_id', null);
            $order->set('carrier_rule_express', null);
            $order->set('carrier_rule_normal', null);
            $order->set('shipping_handling', 0);
            $order->set('delivery_days', false);
            return;
        }

        $this->setQuota($order);

        if (!Cart::hasTaxes($order)) {
            $tax_value = 0;
            $order->no_taxes = true;
        } else {
            $tax_value = $order->carrier->tax ? $order->carrier->tax->value : 0;
        }

        $order->set('shipping_handling_tax_rate', $tax_value);
        $order->set('shipping_handling_tax', $this->Taxes->calculate($order->shipping_handling_tax_rate, $order->shipping_handling));
        $this->setForFree($order);

        $event = new Event('Store.Model.Carriers.afterHandlingCost', $this, [
            $order,
        ]);

        $this->getEventManager()->dispatch($event);
    }

    public function setForFree($order)
    {
        $free_rule_price = $this->getFreeRulePrice($order);
        $order->set('shipping_handling_free_price', $order->setPrice($free_rule_price));

        if (empty($order->shipping_handling)) {
            return;
        }

        if ($free_rule_price) {
            $subtotal = ($order->subtotal + $order->points_discount);

            if (StoreConfig::getConfig('couponDiscountApplyToCarrier')) {
                $subtotal = $subtotal - $order->coupon_discount;
            }

            if ($free_rule_price > $subtotal) {
                $price = $free_rule_price - $order->subtotal_num;

                if (StoreConfig::getConfig('couponDiscountApplyToCarrier')) {
                    $price = $price + $order->coupon_discount;
                }

                $order->set('shipping_handling_left_for_free', ($order->setPrice($free_rule_price - $order->subtotal_num + $order->coupon_discount)));
            }
        }
    }

    public function getRulesQuery($order, $type = null, $no_reduction = false)
    {
        $zero = StoreConfig::getHook('checkZeroShipping', [$order]);
        $conditions = [
            'Carriers.active' => true,
            'Carriers.id' => $order->carrier_id,
        ];

        if (!$zero || $no_reduction) {
            $conditions['CarrierRules.is_reduction'] = false;
        }

        $zone_id = $order->delivery_zone_id;

        $query = $this->CarrierRules->find()
            ->contain(['Carriers'])
            ->where($conditions)
            ->matching('Zones', function ($q) use ($zone_id) {
                return $q->where(['Zones.id' => $zone_id]);
            });

        if ($type == 'express' || ($order->express_delivery == 1 && $type === null && $this->hasExpress($order))) {
            $query->where(['CarrierRules.delivery_type' => 'express']);
        } else {
            $query->where(['CarrierRules.delivery_type' => 'normal']);
        }

        return $query;
    }


    public function getOrderRule($order, $price, $weight, $type = null, $no_reduction = false)
    {
        $orderRule = $this->getRulesQuery($order, $type)
            ->where(['CarrierRules.by_default' => 1])
            ->first();

        if (!empty($order->carrier->id)) {
            $orderRule = null;
            $rules = $this->getRulesQuery($order, $type, $no_reduction)
                ->all();

            $total = [];
            $total['price'] =  $price;
            $total['weight'] = $weight;

            foreach ($rules as $rule) {
                foreach ($this->types as $type) {
                    if (($rule->type == $type)) {
                        $condition = false;

                        // Se dan los dos valores
                        if (!$rule->is_interval && !empty($rule->min) && !empty($rule->max)) {
                            $condition = $total[$type] >= $rule->min && $total[$type] <= $rule->max;
                        }

                        // Hay minimo pero el máximo se omite
                        if (!$rule->is_interval && !empty($rule->min) && empty($rule->max)) {
                            $condition = $total[$type] >= $rule->min;
                        }

                        // Hay máximo pero el mínimo se omite
                        if (!$rule->is_interval && empty($rule->min) && !empty($rule->max)) {
                            $condition = $total[$type] <= $rule->max;
                        }

                        // Hay un interval
                        if ($rule->is_interval && !empty($rule->from) && !empty($rule->interval)) {
                            $condition = $total[$type] >= $rule->from;
                        }

                        if ($condition) {
                            $orderRule = $rule;
                        }
                    }
                }
            }
        }

        return $orderRule;
    }


    public function setQuota($order)
    {
        $total = $order->total_for_quota;
        $orderRule = $this->getOrderRule($order, $total, $order->weight());

        if ($order->carrier->is_free || !$order->has_portable_products) {
            $order->set('carrier_rule_id', null);
            $order->set('carrier_rule_express', null);
            $order->set('carrier_rule_normal', null);
            $order->set('shipping_handling', 0);
            $order->set('delivery_days', false);
            return $order;
        }

        if (!$orderRule) {
            // Event
            $hasCarrierRule = new \stdClass();
            $hasCarrierRule->has = false;
            $hasCarrierRule->rule = null;

            $event = new Event('Store.Model.Carriers.noCarrierRule', $this, [
                $order,
                $hasCarrierRule
            ]);

            $this->getEventManager()->dispatch($event);

            if (!$hasCarrierRule->has) {

                if (!StoreConfig::getConfig('notShowCarrier')) {
                    $order->set('carrier_rule_id', null);
                    $order->set('carrier_rule_express', null);
                    $order->set('carrier_rule_normal', null);
                    $order->set('shipping_handling', null);
                    $order->set('delivery_days', false);
    
                    if (StoreConfig::getConfig('noCarrierMessage')) {
                        $message = StoreConfig::getConfig('noCarrierMessage');
                    } else {
                        $message = __d('app', 'El pedido no puede hacerse porque no hay una tarifa de transporte para el precio o peso y la zona indicada.');
                    }
    
                    $order->setError('carrier_rule_id', [$message]);
                }
    
                return $order;
            } else {
                if ($hasCarrierRule->rule) {
                    $orderRule = $hasCarrierRule->rule;
                    $order->set('carrier_rule_id', $hasCarrierRule->rule->id);
                }
            }
            
        } else {
            $order->set('carrier_rule_id', $orderRule->id);
        }


        // Precio del transporte
        $order->set('shipping_handling', $this->calculateRulePrice($order, $orderRule));

        $event = new Event('Store.Model.Carriers.afterRule', $this, [
            $order,
            $orderRule
        ]);

        $this->getEventManager()->dispatch($event);

        $this->insurance($order);

        // Días de entrega
        $order->set('delivery_days', $orderRule->deadline);


        $this->delayProducts($order);

        if ($order->express_delivery == 1) {
            $order->set('carrier_rule_express', $orderRule);
            $orderRule = $this->getOrderRule($order, $total, $order->weight(), 'normal');
            $order->set('carrier_rule_normal', $orderRule);
        } else {
            $order->set('carrier_rule_normal', $orderRule);
            $orderRule = $this->getOrderRule($order, $total, $order->weight(), 'express');
            $order->set('carrier_rule_express', $orderRule);
        }



        return $order;
    }

    public function calculateRulePrice($order, $rule)
    {
        if (!$rule->is_interval) {
            return $rule->price;
        }

        $weight = $order->weight();
        $diff = $weight - $rule->from;
        $unites = round($diff / $rule->interval);
        return $rule->price_from + ($unites * $rule->price_interval);
    }

    public function insurance($order)
    {
        $carrier = $order->carrier;

        if (!$carrier->insurance) {
            return;
        }

        $price = $order->shipping_handling;

        if ($carrier->insurance_type == 'fixed') {
            $price += $carrier->insurance_value;
        } else {
            $price += ($carrier->insurance_value * $price) / 100;
        }

        $order->set('shipping_handling', $price);
    }

    public function getFreeRulePrice($order)
    {
        $query = $orderRule = $this->getRulesQuery($order)
            ->where([
                'CarrierRules.type' => 'price',
                'max' => 0
            ]);

        if ($order->express_delivery) {
            $query->where([
                'CarrierRules.delivery_type' => 'express'
            ]);
        } else {
            $query->where([
                'CarrierRules.delivery_type' => 'normal'
            ]);
        }

        $rule = $query->first();

        if ($rule) {
            return $rule->min;
        }
    }

    public function delayProducts($order)
    {
        $delay_days = 0;

        foreach ($order->line_items as $item) {
            if (!empty($item->product->delay_days) && $item->product->delay_days > $delay_days) {
                $delay_days = $item->product->delay_days;
            }
        }

        $delivery_days = $order->delivery_days;
        $delivery_days += $delay_days;
        $order->set('delivery_days', $delivery_days);

        if ($delay_days > 0) {
            $order->set('delayDaysProduct', $delay_days);
        }
    }

    public function byDefaultId()
    {
        $carrier = $this->find()
            ->where([
                'by_default' => 1
            ])
            ->first();

        if ($carrier) {
            return $carrier->id;
        } else {
            $carrier = $this->find()
                ->first();

            if ($carrier) {
                return $carrier->id;
            }
        }

        return null;
    }

    public function getFestives($order)
    {
        if (empty($order->carrier)) {
            return [];
        }

        $days = [];

        $contents = $this->CarrierFestives->find()
            ->where([
                'CarrierFestives.carrier_id' => $order->carrier->id
            ])
            ->extract('day');

        foreach ($contents as $content) {
            $days[] = $content->format('Y-m-d');
        }

        return $days;
    }

    public function observeFestives($order)
    {
        if (empty($order->carrier)) {
            return;
        }

        $days = $this->CarrierFestives->find()
            ->where([
                'CarrierFestives.carrier_id' => $order->carrier->id
            ])
            ->extract('day')
            ->toArray();

        if (count($days) == 0) {
            return;
        }

        $days = array_map(function ($value) {
            return $value->format('Y-m-d');
        }, $days);

        $i = 0;

        while (1 == 1) {
            $date = date('Y-m-d', strtotime('+ ' . $i . ' day'));

            if (in_array($date, $days)) {
                $order->delivery_date = date('Y-m-d', strtotime($order->delivery_date . ' + 1 day'));
                TableRegistry::getTableLocator()->get('Store.Orders')->preventDeliveryDate($order);
            }

            if (strtotime($date) >= strtotime($order->delivery_date)) {
                break;
            }

            $i++;
        }
    }

    public function isDayFestive($day, $festives)
    {
        $dayweek = date("N", strtotime($day));
        return in_array($day, $festives) || in_array($dayweek, [6, 7]);
    }

    public function getWorkingDays($startDate, $endDate, $holidays = [])
    {
        // do strtotime calculations just once
        $endDate = strtotime($endDate);
        $startDate = strtotime($startDate);


        //The total number of days between the two dates. We compute the no. of seconds and divide it to 60*60*24
        //We add one to inlude both dates in the interval.
        $days = ($endDate - $startDate) / 86400 + 1;

        $no_full_weeks = floor($days / 7);
        $no_remaining_days = fmod($days, 7);

        //It will return 1 if it's Monday,.. ,7 for Sunday
        $the_first_day_of_week = date("N", $startDate);
        $the_last_day_of_week = date("N", $endDate);

        //---->The two can be equal in leap years when february has 29 days, the equal sign is added here
        //In the first case the whole interval is within a week, in the second case the interval falls in two weeks.
        if ($the_first_day_of_week <= $the_last_day_of_week) {
            if ($the_first_day_of_week <= 6 && 6 <= $the_last_day_of_week) $no_remaining_days--;
            if ($the_first_day_of_week <= 7 && 7 <= $the_last_day_of_week) $no_remaining_days--;
        } else {
            // (edit by Tokes to fix an edge case where the start day was a Sunday
            // and the end day was NOT a Saturday)

            // the day of the week for start is later than the day of the week for end
            if ($the_first_day_of_week == 7) {
                // if the start date is a Sunday, then we definitely subtract 1 day
                $no_remaining_days--;

                if ($the_last_day_of_week == 6) {
                    // if the end date is a Saturday, then we subtract another day
                    $no_remaining_days--;
                }
            } else {
                // the start date was a Saturday (or earlier), and the end date was (Mon..Fri)
                // so we skip an entire weekend and subtract 2 days
                $no_remaining_days -= 2;
            }
        }

        //The no. of business days is: (number of weeks between the two dates) * (5 working days) + the remainder
        //---->february in none leap years gave a remainder of 0 but still calculated weekends between first and last day, this is one way to fix it
        $workingDays = $no_full_weeks * 5;
        if ($no_remaining_days > 0) {
            $workingDays += $no_remaining_days;
        }

        //We subtract the holidays
        foreach ($holidays as $holiday) {
            $time_stamp = strtotime($holiday);
            //If the holiday doesn't fall in weekend
            if ($startDate <= $time_stamp && $time_stamp <= $endDate && date("N", $time_stamp) != 6 && date("N", $time_stamp) != 7)
                $workingDays--;
        }

        return $workingDays;
    }
}
