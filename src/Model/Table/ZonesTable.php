<?php
namespace Store\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;
use Store\Model\Entity\Zone;

/**
 * Zones Model
 */
class ZonesTable extends Table
{

  /**
   * Initialize method
   *
   * @param array $config The configuration for the Table.
   * @return void
   */
  public function initialize(array $config)
  {
    $this->table( 'store_zones');
    $this->displayField( 'title');
    $this->primaryKey( 'id');

    // Behaviors
    $this->addBehavior( 'Timestamp');
    $this->addBehavior( 'Manager.Crudable');
    $this->addBehavior( 'Cofree.Saltable');

    // CRUD
    $this->crud
      ->addFields([
        'active' => __d( 'admin', 'Activo'),
        'title' => __d( 'admin', 'Nombre'),
        'postcodes' => [
          'type' => 'text',
          'label' => 'Códigos postales',
          'help' => 'Indica, separados por espacios, los códigos postales que pertenecen a esta zona.' 
        ],
      ])
      ->addIndex( 'index', [
        'fields' => [
          'title',
          'active',          
        ],
        'actionButtons' => ['create'],
        'saveButton' => false,
      ])
      ->setName( [
        'singular' => __d( 'admin', 'Zona'),
        'plural' => __d( 'admin', 'Zonas'),
      ])
      ->addView( 'create', [
        'columns' => [
          [
            'box' => [
              [
                'elements' => [
                  'title',
                  'active',
                  'postcodes',
                ]
              ]
            ]
          ],
        ],
        'actionButtons' => ['create', 'index'],
      ], ['update'])
      
      ;
  }

}
