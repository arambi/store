<?php
namespace Store\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * TaxRatings Model
 *
 * @method \Store\Model\Entity\TaxRating get($primaryKey, $options = [])
 * @method \Store\Model\Entity\TaxRating newEntity($data = null, array $options = [])
 * @method \Store\Model\Entity\TaxRating[] newEntities(array $data, array $options = [])
 * @method \Store\Model\Entity\TaxRating|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \Store\Model\Entity\TaxRating patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \Store\Model\Entity\TaxRating[] patchEntities($entities, array $data, array $options = [])
 * @method \Store\Model\Entity\TaxRating findOrCreate($search, callable $callback = null, $options = [])
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class TaxRatingsTable extends Table
{

/**
 * Initialize method
 *
 * @param array $config The configuration for the Table.
 * @return void
 */
public function initialize(array $config)
{
  parent::initialize($config);

  $this->setTable('store_tax_ratings');
  $this->setDisplayField('id');
  $this->setPrimaryKey('id');

  $this->addBehavior('Timestamp');
  
  // Behaviors
  $this->addBehavior( 'Manager.Crudable');

  $this->belongsTo( 'Countries', [
    'className' => 'Store.Countries',
    'foreignKey' => 'country_id',
  ]);

  // CRUD Config
  //Escribe aquí las asociaciones vinculadas con el model a la hora de editar (si las hubiera)
  $this->crud->associations(['Countries']);

  $this->crud
    ->addFields([
      'value' => __d( 'admin', 'Valor'),
      'country' => __d( 'admin', 'País'),
    ])
    ->addIndex( 'index', [
      'fields' => [
        'value',
        'country',
      ],
      'actionButtons' => ['create'],
      'saveButton' => false,
    ])
    ->setName( [
      'singular' => __d( 'admin', 'Zonas de impuestos'),
      'plural' => __d( 'admin', 'Zonas de impuestos'),
    ])
    ->addView( 'create', [
      'columns' => [
        [
          'cols' => 8,
          'box' => [
            [
              'elements' => [
                'value',
                'country',
              ]
            ]
          ]
        ]
      ],
      'actionButtons' => ['create', 'index']
    ], ['update'])
    ;
  
  }

  public function findFront( Query $query)
  {
    return $query
    ;
  }

}
