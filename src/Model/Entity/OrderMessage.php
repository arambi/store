<?php
namespace Store\Model\Entity;

use Cake\ORM\Entity;

/**
 * OrderMessage Entity.
 */
class OrderMessage extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * @var array
     */
    protected $_accessible = [
        'user_id' => true,
        'order_id' => true,
        'body' => true,
    ];
}
