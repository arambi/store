<?php
namespace Store\Model\Entity;

use Cake\ORM\Entity;

/**
 * MrwStatus Entity
 *
 * @property int $id
 * @property string|null $shipping_number
 * @property string|null $Estado
 * @property string|null $EstadoDescripcion
 * @property \Cake\I18n\Time|null $FechaEntrega
 * @property int|null $Intentos
 * @property int|null $NumAlbaran
 * @property int|null $PersonaEntrega
 * @property \Cake\I18n\Time|null $created
 * @property \Cake\I18n\Time|null $modified
 */
class MrwStatus extends Entity
{
    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'shipping_number' => true,
        'Estado' => true,
        'EstadoDescripcion' => true,
        'FechaEntrega' => true,
        'Intentos' => true,
        'NumAlbaran' => true,
        'PersonaEntrega' => true,
        'created' => true,
        'modified' => true,
    ];
}
