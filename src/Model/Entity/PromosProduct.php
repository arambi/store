<?php
namespace Store\Model\Entity;

use Cake\ORM\Entity;

/**
 * PromosProduct Entity.
 */
class PromosProduct extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * @var array
     */
    protected $_accessible = [
        'promo_id' => true,
        'product_id' => true,
        'id' => true,
    ];
}
