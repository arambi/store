<?php
namespace Store\Model\Entity;

use Manager\Model\Entity\CrudEntityTrait;
use Cake\ORM\Behavior\Translate\TranslateTrait;
use Cake\ORM\Entity;
use Cake\ORM\TableRegistry;

/**
 * PointsAdd Entity.
 *
 * @property int $id
 * @property string $title
 * @property string $data
 * @property string $subject
 * @property string $body
 * @property int $user_id
 * @property \Cake\I18n\Time $send_on
 * @property \Cake\I18n\Time $created
 * @property \Cake\I18n\Time $modified
 */
class PointsAdd extends Entity
{
  use CrudEntityTrait;
  use TranslateTrait;

  /**
   * Fields that can be mass assigned using newEntity() or patchEntity().
   *
   * Note that when '*' is set to true, this allows all unspecified fields to
   * be mass assigned. For security purposes, it is advised to set '*' to false
   * (or remove it), and explicitly make individual fields accessible as needed.
   *
   * @var array
   */
  protected $_accessible = [
      '*' => true,
      'id' => false,
  ];

  protected $_virtual = [
    'sends',
    'total',
  ];

  protected function _getSends()
  {
    if( empty( $this->id))
    {
      return null;
    }

    return TableRegistry::get( 'Store.PointsAddsSends')->find()
      ->where([
        'PointsAddsSends.send_id' => $this->id,
        'PointsAddsSends.send_on > 0'  
      ])
      ->count();
  }


   protected function _getTotal()
  {
    if( empty( $this->id))
    {
      return null;
    }
    
    return TableRegistry::get( 'Store.PointsAddsSends')->find()
      ->where([
        'PointsAddsSends.send_id' => $this->id
      ])
      ->count();
  }

}
