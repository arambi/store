<?php
namespace Store\Model\Entity;

use Manager\Model\Entity\CrudEntityTrait;
use Cake\ORM\Behavior\Translate\TranslateTrait;
use Slug\Model\Entity\SlugTrait;use Cake\ORM\Entity;

/**
 * Brand Entity.
 */
class Brand extends Entity
{
    use CrudEntityTrait;
    use TranslateTrait;

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * @var array
     */
    protected $_accessible = [
        '*' => true,
    ];
}
