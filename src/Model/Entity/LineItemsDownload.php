<?php
namespace Store\Model\Entity;

use Cake\ORM\Entity;

/**
 * LineItemsDownload Entity
 *
 * @property int $id
 * @property int|null $line_item_id
 * @property \Cake\I18n\Time|null $created
 *
 * @property \Store\Model\Entity\LineItem $line_item
 */
class LineItemsDownload extends Entity
{
    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'line_item_id' => true,
        'created' => true,
        'line_item' => true,
    ];
}
