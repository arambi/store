<?php

namespace Store\Model\Entity;

use Cake\ORM\Entity;
use Cake\Utility\Text;
use Cake\Routing\Router;
use Website\Lib\Website;
use Store\Model\Entity\PriceTrait;
use Manager\Model\Entity\CrudEntityTrait;

/**
 * InvoiceRefund Entity
 *
 * @property int $id
 * @property int|null $order_id
 * @property int|null $num
 * @property string|null $prefix
 * @property string|null $salt
 * @property \Cake\I18n\Time $created
 * @property \Cake\I18n\Time $modified
 */
class InvoiceRefund extends Entity
{
    use CrudEntityTrait;
    use PriceTrait;

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */

    protected $_virtual = [
        'invoice_num',
        'name',
        'total_human',
        'total_taxes_human',
        'invoice_url',
        'invoice_number',
        'invoice_date',
    ];

    protected $_accessible = [
        '*' => true,
        'invoice_refund_line_items',
    ];


    public function &get($property)
    {
        $value = parent::get($property);

        if ($value === null && !empty($this->order)) {
            return $this->order->get($property);
        }

        return $value;
    }

    protected function _getName()
    {
        if ($this->order) {
            if (!empty($this->order->adr_invoice_company)) {
                return $this->order->adr_invoice_company;
            } else {
                return $this->order->adr_invoice_firstname . ' ' . $this->order->adr_invoice_lastname;
            }
        }
    }

    protected function _getInvoiceUrl()
    {
        return Router::url([
            'prefix' => false,
            'plugin' => 'Store',
            'controller' => 'InvoiceRefunds',
            'action' => 'view',
            'filename' => Text::slug(@$this->order->adr_store_company . '-' . $this->num),
            '?' => [
                'salt' => $this->salt
            ],
            '_ext' => 'pdf',
        ], true);
    }

    protected function _getInvoiceNum()
    {
        $zeros = Website::get('settings.store_invoice_refund_zeros');

        if (is_numeric($this->num)) {
            return $this->prefix . sprintf('%0' . $zeros . 'd', $this->num);
        } else {
            return $this->prefix . $this->num;
        }
    }

    protected function _getInvoiceDate()
    {
        if (!is_object($this->created)) {
            return null;
        }

        return date(__d('app', 'd/m/Y'), $this->created->toUnixString());
    }

    protected function _getTotal()
    {
        $total = 0;

        foreach ((array)$this->invoice_refund_line_items as $item) {
            $total += ($item->price + $item->taxes) * $item->quantity;
        }

        $total += $this->shipping_handling;
        $total += $this->shipping_handling_tax;

        return $total;
    }

    protected function _getTotalHuman()
    {
        return $this->setOrderPrice($this->total);
    }

    protected function _getTotalTaxesHuman()
    {
        return $this->setOrderPrice($this->taxes);
    }

    protected function _getTaxes()
    {
        $total = 0;

        foreach ((array)$this->invoice_refund_line_items as $item) {
            $total += ($item->taxes) * $item->quantity;
        }

        $total += $this->shipping_handling_tax;
        return $total;
    }

    public function setOrderPrice($price)
    {
        return $this->setPrice($price, $this->currency);
    }
}
