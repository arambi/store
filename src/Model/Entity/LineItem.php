<?php

namespace Store\Model\Entity;

use Cake\ORM\Entity;
use Cake\Event\Event;
use Cake\Routing\Router;
use Website\Lib\Website;
use Cake\ORM\TableRegistry;
use Cake\Event\EventManager;
use Store\Config\StoreConfig;
use Cake\Collection\Collection;
use Store\Model\Entity\PriceTrait;
use Cake\Event\EventDispatcherTrait;
use Store\Cart\Cart;

/**
 * LineItem Entity.
 */
class LineItem extends Entity
{
    use PriceTrait;
    use EventDispatcherTrait;


    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * @var array
     */
    protected $_accessible = [
        'order_id' => true,
        'product_id' => true,
        'product_attribute_id' => true,
        'title' => true,
        'quantity' => true,
        'price' => true,
        'weight' => true,
        'reference' => true,
        'ean13' => true,
        'upc' => true,
        'tax_id' => true,
        'tax' => true,
        'tax_rate' => true,
        'discount' => true,
        'subtotal' => true,
        'is_gift' => true,
        'status' => true,
    ];

    protected $_virtual = [
        'subtotal',
        'subtotal_human',
        'price_human',
        'taxes_total',
        'taxes_total_human',
        'subtotal_no_taxes_human',
        'photo',
        'original_price_human',
        'price_without_discount_human',
        'price_without_discount_subtotal_human',
        'no_gift',
        'url',
        'full_title',
        'invoice_title',
        'description'
    ];

    public function __construct(array $properties = [], array $options = [])
    {
        parent::__construct($properties, $options);
        $event = new Event('Store.Model.Entity.LineItem.construct', $this);
        EventManager::instance()->dispatch($event);
    }

    protected function _getProduct($value)
    {
        if ($value !== null) {
            return $value;
        }

        $product = StoreConfig::getProductsModel()->find('products')
            ->where([
                StoreConfig::getProductsModel()->getAlias() . '.'. StoreConfig::getProductsModel()->getPrimaryKey() => $this->product_id
            ])
            ->first();
        
        return $product;
    }

    protected function _setPhoto()
    {
        return $this->getPhoto();
    }

    protected function _getPriceHuman()
    {
        if ($this->is_gift) {
            return '';
        }

        if (isset($this->price) && isset($this->tax_rate)) {
            return $this->setOrderPrice($this->realPrice($this->price, $this->tax_rate));
        }

        return $this->setOrderPrice($this->price);
    }

    protected function _getDiscountHuman()
    {
        if (isset($this->discount) && isset($this->tax_rate)) {
            return $this->setOrderPrice($this->realPrice($this->discount, $this->tax_rate));
        }

        return $this->setOrderPrice($this->discount);
    }

    protected function _getOriginalPriceHuman()
    {
        return $this->setOrderPrice($this->realPrice($this->original_price, $this->tax_rate));
    }

    protected function _getPriceWithoutDiscountHuman()
    {
        if (empty($this->discount)) {
            return;
        }

        return $this->setOrderPrice($this->realPrice($this->price + $this->discount, $this->tax_rate));
    }

    protected function _getPriceWithoutDiscountSubtotalHuman()
    {
        if (empty($this->discount)) {
            return;
        }

        return $this->setOrderPrice($this->realPrice(($this->price + $this->discount) * $this->quantity, $this->tax_rate));
    }

    protected function _getNoGift()
    {
        return !$this->is_gift;
    }


    /**
     * Devuelve el subtotal del item
     * 
     * @return float
     */
    protected function _getSubtotal()
    {
        return (round($this->price, 2) * $this->quantity);
    }


    protected function _getTotal()
    {
        return (round($this->realPrice($this->price, $this->tax_rate) * $this->quantity));
    }

    protected function _getSubtotalHuman()
    {
        if ($this->is_gift) {
            return __d('app', 'Regalo');
        }

        if (isset($this->price) && isset($this->tax_rate)) {
            return $this->setOrderPrice($this->realPrice($this->price, $this->tax_rate) * $this->quantity);
        }

        return $this->setOrderPrice($this->subtotal);
    }

    protected function _getSubtotalNoTaxesHuman()
    {
        if ($this->is_gift) {
            return __d('app', 'Regalo');
        }

        if (isset($this->price) && isset($this->tax_rate)) {
            return $this->setOrderPrice($this->subtotal * $this->quantity);
        }

        return $this->setOrderPrice($this->subtotal);
    }

    protected function _getPercentDiscount()
    {
        if ($this->discount == 0) {
            return 0;
        }

        return ($this->discount * 100) / $this->price;
    }

    /**
     * Devuelve el total de impuestos para una unidad
     * 
     * @return float
     */
    protected function _getTaxes()
    {
        return $this->__getTax();
    }

    private function __getTax()
    {
        if ($this->store_has_custom_taxes) {
            $return = $this->store_custom_taxes;
        } else {
            $total = $this->price + ($this->price * $this->tax_rate) / 100;
            $return = round($total, 2) - round($this->price, 2);
        }

        return $return;
    }
    /**
     * Devuelve el total de impuestos para todas las cantidades
     * 
     * @return float
     */
    protected function _getTaxesTotal()
    {
        return $this->__getTax() * $this->quantity;
    }

    protected function _getTaxesTotalHuman()
    {
        return $this->setOrderPrice($this->taxes_total);
    }

    /**
     * Devuelve el titulo total del item, tomando, si existe, los valores de los atributos
     * 
     * @return string
     */
    protected function _getFullTitle()
    {
        $itemTitleConfig = StoreConfig::getHook('itemTitle');

        if (is_callable($itemTitleConfig)) {
            return $itemTitleConfig($this);
        }

        if (empty($this->product_attribute)) {
            return @$this->product->title;
        }

        return @$this->product->title . ' - ' . $this->attributes_name;
    }

    /**
     * Devuelve el titulo total del item, tomando, si existe, los valores de los atributos
     * 
     * @return string
     */
    protected function _getInvoiceTitle()
    {
        $itemTitleConfig = StoreConfig::getHook('invoiceTitle');

        if (is_callable($itemTitleConfig)) {
            return $itemTitleConfig($this);
        }

        if (empty($this->product_attribute)) {
            return @$this->product->title;
        }

        return @$this->product->title . ' - ' . $this->attributes_name;
    }

    protected function _getDescription()
    {
        return StoreConfig::getHook('itemDescription', [$this]);
    }

    protected function _getTitle($title)
    {
        $return = '';

        if (!empty($title)) {
            return $title;
        } else {
            if (empty($this->product_attribute) && isset($this->product)) {
                $return = $this->product->title;
            } elseif (isset($this->product)) {
                $return = $this->product->title . ' - ' . $this->attributes_name;
            }
        }

        if (empty($return)) {
        }

        $this->set('title', $return);
        return $return;
    }

    protected function _getAttributesName()
    {
        $titles = [];

        foreach ($this->product_attribute->attributes as $attribute) {
            $titles[] = $attribute->title;
        }

        return implode(' - ', $titles);
    }

    protected function _getColorTitle()
    {
        if (empty($this->product_attribute) || !isset($this->product_attribute->attributes)) {
            return null;
        }

        foreach ($this->product_attribute->attributes as $attribute) {
            if (!empty($attribute->color)) {
                return $attribute->title;
            }
        }
    }

    public function getPhoto()
    {
        $attribute_id = false;

        if (!$this->product) {
            return false;
        }

        if (!method_exists($this->product, 'mainPhoto')) {
            return false;
        }

        if (property_exists($this, 'product_attribute')) {
            foreach ($this->product_attribute->attributes as $attribute) {
                if ($attribute->attribute_group->has_product_photo) {
                    $attribute_id = $attribute->id;
                }
            }
        }

        return $this->product->mainPhoto($attribute_id);
    }

    protected function _getPhoto()
    {
        return $this->getPhoto();
    }

    /**
     * Devuelve el valor de un item tomándolo del producto o del atributo
     * 
     * @param  string $prop
     * @return string
     */
    public function getItemValue($prop)
    {
        if (!empty($this->product_attribute) && !empty($this->product_attribute->$prop)) {
            return $this->product_attribute->$prop;
        }

        return $this->product->$prop;
    }

    public function setOrderPrice($price)
    {
        return $this->setPrice($price, $this->currency);
    }

    /**
     * Construye un item a partir de los datos del producto y product_attribute asociados
     * 
     * @return 
     */
    public function build($build_items_price = true)
    {
        $order = Cart::getOrder();

        if (!Cart::hasTaxes($order)) {
            $this->tax_rate = 0;
        } else {
            $this->tax_rate = $this->product->tax->value;
        }

        $this->weight = $this->getItemValue('store_weight');
        $this->reference = $this->getItemValue('store_reference');
        $this->ean13 = $this->getItemValue('store_ean13');
        $this->upc = $this->getItemValue('store_upc');
        $this->photo = $this->getPhoto();
        $this->store_custom_taxes = $this->product->store_custom_taxes;
        $this->store_has_custom_taxes = $this->product->store_has_custom_taxes;

        // El precio
        if (!empty($this->product_attribute_id) && is_array($this->product->product_attributes)) {
            $product_attribute = (new Collection($this->product->product_attributes))->firstMatch(['id' => $this->product_attribute_id]);
            
            if ($build_items_price) {
                $this->set('price', (!empty($product_attribute->price) ? $product_attribute->price : $this->product->price));
            }

            $this->set('original_price', $product_attribute->original_price);

            if ($product_attribute->discount) {
                $this->set('discount', $product_attribute->discount);
            } else {
                $this->set('discount', $this->product->discount);
            }
        } elseif (!empty($this->giftcard_price) || !empty($this->giftcard_price_text)) {
            if (!empty($this->giftcard_price_text)) {
                $price = (float)$this->giftcard_price_text;
                $this->set('giftcard_price', (float)$this->giftcard_price_text);
            } else {
                $price = $this->giftcard_price;
            }

            $price = $price / (1 + ($this->tax_rate / 100));
            $this->set('price', $price);
        } else {
            if ($build_items_price) {
                $this->set('price', $this->product->price);
                $this->set('discount', $this->product->discount);
                $this->set('original_price', $this->product->original_price);
            }
        }

        if ($this->is_gift) {
            $this->set('price', 0);
        }

        if (method_exists($this->product, 'noPortable')) {
            $this->set('no_portable', $this->product->noPortable());
        }

        $event = new Event('Store.Model.Entity.LineItem.build', $this);
        $this->getEventManager()->dispatch($event);
        return $this;
    }

    protected function _getUrl()
    {
        if (!$this->product) {
            return null;
        }

        return $this->product->url;
    }

    protected function _getGiftcardFullAddress()
    {
        $return = [
            $this->name,
        ];

        if (!empty($this->giftcard_phone)) {
            $return[] = __d('app', 'Tel.') . ' ' . $this->giftcard_phone;
        }

        if (!empty($this->giftcard_address)) {
            $return[] = $this->giftcard_address;
        }

        if (!empty($this->giftcard_postcode)) {
            $return[] = $this->giftcard_postcode;
        }

        if (!empty($this->giftcard_city)) {
            $return[] = $this->giftcard_city->title;
        }

        if (!empty($this->giftcard_state)) {
            $return[] = $this->giftcard_state->title;
        }

        if (!empty($this->giftcard_country)) {
            $return[] = $this->giftcard_country->title;
        }

        return implode(' - ', $return);
    }

    protected function _getGiftcardCountry($value)
    {
        if ($value !== null) {
            return $value;
        }

        if (empty($this->giftcard_country_id)) {
            return;
        }

        $table = TableRegistry::getTableLocator()->get('Store.Countries');

        $entity = $table->find()
            ->where([
                'id' => $this->giftcard_country_id
            ])
            ->first();

        $this->set('gift_country', $entity);
        return $entity;
    }

    protected function _getGiftcardState($value)
    {
        if ($value !== null) {
            return $value;
        }

        if (empty($this->giftcard_state_id)) {
            return;
        }

        $table = TableRegistry::getTableLocator()->get('Store.States');

        $entity = $table->find()
            ->where([
                'id' => $this->giftcard_state_id
            ])
            ->first();

        $this->set('giftcard_state', $entity);
        return $entity;
    }

    protected function _getStatusHuman()
    {
        if (empty($this->item_status)) {
            return;
        }

        $texts = [
            StoreConfig::ITEM_STATUS_REFUNDED_REQUEST => StoreConfig::itemStatusTitle(StoreConfig::ITEM_STATUS_REFUNDED_REQUEST),
            StoreConfig::ITEM_STATUS_REFUNDED_WORKING  => StoreConfig::itemStatusTitle(StoreConfig::ITEM_STATUS_REFUNDED_WORKING),
            StoreConfig::ITEM_STATUS_REFUNDED_SEND => StoreConfig::itemStatusTitle(StoreConfig::ITEM_STATUS_REFUNDED_SEND),
            StoreConfig::ITEM_STATUS_REFUNDED_RECEIVED => StoreConfig::itemStatusTitle(StoreConfig::ITEM_STATUS_REFUNDED_RECEIVED),
            StoreConfig::ITEM_STATUS_REFUNDED_ACCEPT => StoreConfig::itemStatusTitle(StoreConfig::ITEM_STATUS_REFUNDED_ACCEPT),
            StoreConfig::ITEM_STATUS_REFUNDED_CANCELED => StoreConfig::itemStatusTitle(StoreConfig::ITEM_STATUS_REFUNDED_CANCELED),
            StoreConfig::ITEM_STATUS_REFUNDED_REFUSED => StoreConfig::itemStatusTitle(StoreConfig::ITEM_STATUS_REFUNDED_REFUSED),
        ];

        if (array_key_exists($this->item_status, $texts)) {
            return $texts[$this->item_status];
        }
    }

    protected function _getItemStatus($value)
    {
        if ($value !== null) {
            return $value;
        }

        $refund_item = TableRegistry::getTableLocator()->get('Store.RefundsItems')
            ->findByLineItemId($this->id)
            ->contain([
                'Refunds'
            ])
            ->first();

        if (!$refund_item) {
            return null;
        }

        $this->set('item_status', $refund_item->status);
        return $refund_item->status;
    }

    protected function _getOrder($value)
    {
        if ($value !== null) {
            return $value;
        }

        if (!empty($this->order_id)) {
            $order = TableRegistry::getTableLocator()->get('Store.Orders')
                ->find()
                ->where([
                    'Orders.id' => $this->order_id,
                ])
                ->first();

            $this->set('order', $order);
            return $order;
        }
    }

    protected function _getTicketRefundItemUrl()
    {
        $url = StoreConfig::getHook('getTicketRefundItemUrl', [$this->id]);
        return $url;
    }
}
