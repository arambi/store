<?php
namespace Store\Model\Entity;

use Cake\ORM\Entity;

/**
 * PromosCategory Entity.
 */
class PromosCategory extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * @var array
     */
    protected $_accessible = [
        '*' => true,
        'id' => false,
    ];
}
