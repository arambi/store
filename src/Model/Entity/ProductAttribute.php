<?php

namespace Store\Model\Entity;

use Cake\ORM\Entity;
use Manager\Model\Entity\CrudEntityTrait;
use Store\Model\Entity\ProductTrait;
use Store\Model\Entity\PriceTrait;

/**
 * ProductAttribute Entity.
 */
class ProductAttribute extends Entity
{

    use CrudEntityTrait;
    use ProductTrait;
    use PriceTrait;

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * @var array
     */
    protected $_accessible = [
        '*' => true,
        'product_id' => true,
        'store_price' => true,
        'store_weight' => true,
        'minimal_quantity' => true,
        'store_reference' => true,
        'store_ean13' => true,
        'store_upc' => true,
        'photo_id' => true,
        'quantity' => true,
        'available_date' => true,
        'attributes' => true,
        'published' => true,
    ];

    protected $_virtual = [
        'price',
        'original_price',
        'price_human',
        'title',
        'photo',
        'attributes_title',
    ];

    protected function _getTitle()
    {
        if (empty($this->product)) {
            return null;
        }

        return $this->product->title . ' - ' . $this->getAttributesName();
    }

    protected function getAttributesName($withTitle = true)
    {
        $titles = [];

        foreach ((array)$this->attributes as $attribute) {
            if (isset($attribute->attribute_group)) {
                if ($withTitle) {
                    $titles[] = $attribute->attribute_group->title . ': ' . $attribute->title;
                } else {
                    $titles[] = $attribute->title;
                }
            }
        }

        return implode(', ', $titles);
    }

    protected function _getAttributesTitle()
    {
        $titles = $this->getAttributesName(false);
        return $titles;
    }

    protected function _getPhoto()
    {
        if (empty($this->product->photos)) {
            return;
        }

        $attribute_id = false;

        foreach ($this->attributes as $attribute) {
            if (isset($attribute->attribute_group)) {
                if ($attribute->attribute_group->has_product_photo) {
                    $attribute_id = $attribute->id;
                }
            }
        }

        if (!is_array($this->product->photos)) {
            return;
        }
        foreach ($this->product->photos as $photo) {
            if (isset($photo->attributes->$attribute_id) && $photo->attributes->$attribute_id) {
                return $photo->paths->square;
            }
        }
    }

}
