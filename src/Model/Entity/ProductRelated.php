<?php
namespace Store\Model\Entity;

use Cake\ORM\Entity;
use Manager\Model\Entity\CrudEntityTrait;
use Cake\ORM\Behavior\Translate\TranslateTrait;
use Slug\Model\Entity\SlugTrait;
use Store\Model\Entity\ProductTrait;
/**
 * ProductRelated Entity.
 */
class ProductRelated extends Entity
{
  use CrudEntityTrait;
  use TranslateTrait;
  use SlugTrait;
  use ProductTrait;

  /**
   * Fields that can be mass assigned using newEntity() or patchEntity().
   *
   * @var array
   */
  protected $_accessible = [
    'id' => true,
    'content_type' => true,
    'slugs' => true,
    'published' => true,
    'title' => true,
    'summary' => true,
    'photos' => true,
    'photo' => true,
    'body' => true,
    'salt' => true,
    'photo' => true,
    'product_attributes' => true,
    'categories_shops' => true,
    'content_categories' => true,
    'tags' => true,
    'tax' => true,
    'store_price' => true,
    'store_reference' => true,
    'store_ean13' => true,
    'store_upc' => true,
    'related_products' => true,
    'store_novelty' => true,
  ];

  protected $_virtual = [
    'price',
    'original_price'
  ];
}
