<?php
namespace Store\Model\Entity;

use Cake\ORM\Entity;
use Manager\Model\Entity\CrudEntityTrait;
use Slug\Model\Entity\SlugTrait;
use Cake\ORM\Behavior\Translate\TranslateTrait;


/**
 * CategoriesShop Entity.
 */
class CategoriesShop extends Entity
{
  use CrudEntityTrait;
  use SlugTrait;
  use TranslateTrait;

  /**
   * Fields that can be mass assigned using newEntity() or patchEntity().
   *
   * @var array
   */
  protected $_accessible = [
    '*' => true,
    'id' => true,
    'content_type' => true,
    'site_id' => true,
    'parent_id' => true,
    'slugs' => true,
    'photo' => true,
    'title' => true,
    'size' => true,
  ];
}
