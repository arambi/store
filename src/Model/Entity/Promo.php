<?php
namespace Store\Model\Entity;

use Manager\Model\Entity\CrudEntityTrait;
use Cake\ORM\Behavior\Translate\TranslateTrait;
use Slug\Model\Entity\SlugTrait;
use Cake\ORM\Entity;
use I18n\Model\Entity\DatesEntityTrait;

/**
 * Promo Entity.
 */
class Promo extends Entity
{
  use CrudEntityTrait;
  use TranslateTrait;
  use SlugTrait;
  use DatesEntityTrait;

  /**
   * Fields that can be mass assigned using newEntity() or patchEntity().
   *
   * @var array
   */
  protected $_accessible = [
      '*' => true,
      'id' => true,
      'groups' => true,
      'products' => true,
      'states' => true,
  ];

  protected $_dirty = [
    'products'
  ];


  public function isFinalized()
  {
    return (int)$this->finish_on->toUnixString() < strtotime( date( 'Y-m-d'));
  }
}
