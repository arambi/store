<?php
namespace Store\Model\Entity;

use Cake\ORM\Entity;

/**
 * ProductAttributeGroup Entity
 *
 * @property int $id
 * @property int $product_id
 * @property int $attribute_group_id
 *
 * @property \Store\Model\Entity\Product $product
 * @property \Store\Model\Entity\AttributeGroup $attribute_group
 */
class ProductAttributeGroup extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        '*' => true,
        'product_id' => true,
        'attribute_group_id' => true,
    ];
}
