<?php
namespace Store\Model\Entity;

use Manager\Model\Entity\CrudEntityTrait;
use Cake\ORM\Entity;

/**
 * Carrier Entity.
 */
class Carrier extends Entity
{
    use CrudEntityTrait;

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * @var array
     */
    protected $_accessible = [
        '*' => true,
    ];

    protected function _getTax($value)
    {
        return $this->getBelongsTo($value, 'tax');
    }
}
