<?php
namespace Store\Model\Entity;

use Cake\ORM\Entity;
use Manager\Model\Entity\CrudEntityTrait;

/**
 * PaymentMethod Entity.
 */
class PaymentMethod extends Entity
{   
    use CrudEntityTrait;

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * @var array
     */
    protected $_accessible = [
        '*' => true,
    ];
}
