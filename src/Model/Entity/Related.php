<?php
namespace Store\Model\Entity;

use Cake\ORM\Entity;

/**
 * Related Entity.
 */
class Related extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * @var array
     */
    protected $_accessible = [
        'product_id' => true,
        'related_id' => true,
    ];
}
