<?php
namespace Store\Model\Entity;

use Cake\ORM\Entity;

/**
 * CouponsOrder Entity
 *
 * @property int $id
 * @property int $coupon_id
 * @property int $user_id
 * @property int $order_id
 * @property \Cake\I18n\Time $created
 * @property \Cake\I18n\Time $modified
 *
 * @property \Store\Model\Entity\Coupon $coupon
 * @property \Store\Model\Entity\User $user
 * @property \Store\Model\Entity\Order $order
 */
class CouponsOrder extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'coupon_id' => true,
        'user_id' => true,
        'order_id' => true,
        'created' => true,
        'modified' => true,
        'coupon' => true,
        'user' => true,
        'order' => true
    ];
}
