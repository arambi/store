<?php

namespace Store\Model\Entity;

use Cake\ORM\Entity;
use Cake\ORM\Behavior\Translate\TranslateTrait;
use Cake\ORM\TableRegistry;
use Manager\Model\Entity\CrudEntityTrait;
use Slug\Model\Entity\SlugTrait;
use Store\Config\StoreConfig;
use Store\Currency\CurrencyCollection;
use Store\Model\Entity\PriceTrait;

/**
 * Product Entity.
 */
trait ProductTrait
{

    protected function _getHasOffer()
    {
        if (get_class($this) == 'Store\Model\Entity\ProductAttribute') {
            $key_offer = 'store_price_offer';
        } else {
            $key_offer = 'offer_price';
        }

        if (!empty($this->$key_offer)) {
            return true;
        }

        return false;
    }

    protected function _getCurrencyPrice()
    {
        $key = CurrencyCollection::getCurrency()->getProductKey();
        return $this->$key;
    }

    protected function _getPrice()
    {
        if (get_class($this) == 'Store\Model\Entity\ProductAttribute') {
            $key_offer = 'store_price_offer';
        } else {
            $key_offer = 'offer_price';
        }

        if (!empty($this->$key_offer)) {
            return $this->$key_offer;
        }

        return $this->currency_price;
    }

    protected function _getOriginalPrice()
    {
        if (isset($this->offer_price)) {
            return $this->currency_price;
        }

        return $this->currency_price;
    }

    protected function _getOriginalPriceHuman()
    {
        return $this->setPrice($this->realPrice($this->original_price, $this->tax));
    }

    /**
     * Devuelve un string con los precios del producto dado un id de atributo
     *
     * @param integer $attribute_id
     * @param string $format
     * @return array
     */
    public function pricesOfAttribute($attribute_id, $format = '%s - %s')
    {
        $prices = [];

        foreach ($this->product_attributes as $pa) {
            $count = collection($pa->attributes)
                ->match(['id' => $attribute_id]);

            if (iterator_count($count) > 0) {
                $prices[] = $pa->price_human;
            }
        }

        $prices = array_unique($prices);
        asort($prices);

        if (count($prices) == 1) {
            return $prices[0];
        } elseif (count($prices) > 1) {
            return vsprintf($format, [
                current($prices),
                end($prices)
            ]);
        }
    }

    /**
     * Aplica el descuento dado un porcentaje
     *
     * @param integer $value
     * @return void
     */
    public function applyDiscountPercent($value, $set = true, $set_percent = true)
    {
        $total = $this->currency_price;
        $discount = ($total * $value) / 100;
        $price = $total - $discount;
        
        if ($set) {
            $this->set([
                'discount' => $discount,
                'offer_price' => $price,
                'offer_percent' => $set_percent ? $value : 0
            ]);
        }

        return $price;
    }

    public function hasStock()
    {
        if ($this->combined) {
            $quantity = 0;

            foreach ((array)$this->product_attributes as $pa) {
                $quantity = $quantity + $pa->quantity;
            }
        } else {
            $quantity = $this->store_quantity;
        }

        return $this->without_stock || $quantity > 0;
    }

    protected function _getDiscountPrice()
    {
        if (empty($this->offer_price)) {
            return null;
        }

        return 100 - round(100 * $this->offer_price / $this->currency_price);
    }

    public function combinationsPrice()
    {
        $out = [];

        foreach ($this->product_attributes as $pa) {
            $combination = collection($pa->attributes)->extract('id')->toArray();
            $out[] = [
                'attributes' => $combination,
                'price' => !empty($pa->price)
                    ? $this->setPrice($this->realPrice($pa->price, $this->tax))
                    : $this->setPrice($this->realPrice($this->price, $this->tax)),
                'hasOffer' => !empty($pa->price) ? $pa->has_offer : $this->has_offer,
                'originalPrice' => !empty($pa->store_price)
                    ? $this->setPrice($this->realPrice($pa->store_price, $this->tax))
                    : $this->setPrice($this->realPrice($this->currency_price, $this->tax))
            ];
        }

        return $out;
    }

    protected function _getAttributeGroups($value)
    {
        if (!StoreConfig::getConfig('allGroupAttributesMandatory')) {
            return $value;
        }

        if (empty($this->product_attributes)) {
            return [];
        }

        $group_ids = [];
        $groups = [];

        foreach ($this->product_attributes as $pa) {
            foreach ($pa->attributes as $attribute) {
                if (!in_array($attribute->attribute_group_id, $group_ids)) {
                    $group_ids [] = $attribute->attribute_group_id;
                    $groups [] = $attribute->attribute_group;
                }
            }
        }

        return $groups;
    }

    public function combinationsAttributes()
    {
        if (empty($this->product_attributes)) {
            return [];
        }

        $ids = [];
        $group_ids = [];

        foreach ($this->product_attributes as $pa) {
            $_ids = collection($pa->attributes)->extract('id')->toArray();
            $_group_ids = collection($pa->attributes)->extract('attribute_group_id')->toArray();
            $ids = array_merge($ids, $_ids);
            $group_ids = array_merge($group_ids, $_group_ids);
        }

        $ids = array_values(array_unique($ids));
        $group_ids = array_values(array_unique($group_ids));
            
        $groups = TableRegistry::getTableLocator()->get('Store.AttributeGroups')->find()
            ->where([
                'AttributeGroups.id IN' => $group_ids,
            ])
            ->contain([
                'Attributes'
            ])
            ->formatResults(function ($result) {
                return $result->map(function ($row) {
                    $return = $row->extract([
                        'id',
                        'attributes'
                    ]);

                    $return['attributes'] = collection($return['attributes'])->extract('id')->toArray();
                    return $return;
                });
            })
            ->toArray();

        $out = [];

        foreach ($groups as $group) {
            foreach ($group['attributes'] as $attr_id) {
                if (!in_array($attr_id, $ids)) {
                    continue;
                }

                $_out = [
                    'id' => $attr_id,
                    'groups' => []
                ];

                foreach ($this->product_attributes as $pa) {
                    $_ids = collection($pa->attributes)->extract('id')->toArray();

                    if (in_array($attr_id, $_ids)) {
                        $other_ids = array_merge($_ids, [$attr_id]);

                        foreach ($groups as $_group) {
                            if ($_group['id'] != $group['id']) {
                                $group_attrs_ids = [];

                                foreach ($_group['attributes'] as $_attr_id) {
                                    if (in_array($_attr_id, $other_ids)) {
                                        $group_attrs_ids[] = $_attr_id;
                                    }
                                }

                                if (array_key_exists($_group['id'], $_out['groups'])) {
                                    $_out['groups'][$_group['id']] = [
                                        'id' => $_group['id'],
                                        'valids' => array_merge($_out['groups'][$_group['id']]['valids'], $group_attrs_ids)
                                    ];
                                } else {
                                    $_out['groups'][$_group['id']] = [
                                        'id' => $_group['id'],
                                        'valids' => $group_attrs_ids
                                    ];
                                }
                            }
                        }
                    }
                }

                $_out['groups'] = array_values($_out['groups']);

                $out[] = $_out;
            }
        }

        return $out;
    }
}
