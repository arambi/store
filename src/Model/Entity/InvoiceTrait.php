<?php

namespace Store\Model\Entity;

use Manager\Model\Entity\CrudEntityTrait;
use Cake\ORM\Entity;
use Website\Lib\Website;
use I18n\Model\Entity\DatesEntityTrait;


trait InvoiceTrait
{

    public function &get($property)
    {
        $value = parent::get($property);

        if ($value === null && !empty($this->order)) {
            return $this->order->get($property);
        }

        return $value;
    }

    protected function _getName()
    {
        if ($this->order) {
            if (!empty($this->order->adr_invoice_company)) {
                return $this->order->adr_invoice_company;
            } else {
                return $this->order->adr_invoice_firstname . ' ' . $this->order->adr_invoice_lastname;
            }
        }
    }

    protected function _getInvoiceNum()
    {
        $zeros = Website::get('settings.store_invoice_zeros');

        if (is_numeric($this->num)) {
            return sprintf('%0' . $zeros . 'd', $this->num);
        } else {
            return $this->num;
        }
    }

    protected function _getInvoiceFullNumber()
    {
        return $this->prefix . $this->invoice_num;
    }

    protected function _getInvoiceDate($value)
    {
        if (!is_object($this->created)) {
            return null;
        }

        return date(__d('app', 'd/m/Y'), $this->created->toUnixString());
    }
}
