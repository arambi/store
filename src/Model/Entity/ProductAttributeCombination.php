<?php
namespace Store\Model\Entity;

use Cake\ORM\Entity;

/**
 * ProductAttributeCombination Entity.
 */
class ProductAttributeCombination extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * @var array
     */
    protected $_accessible = [
        'id' => true,
        'attribute_id' => true,
        'product_attribute_id' => true,
    ];
}
