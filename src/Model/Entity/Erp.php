<?php
namespace Store\Model\Entity;

use Cake\ORM\Entity;

/**
 * Erp Entity.
 *
 * @property int $id
 * @property string $erp_id
 * @property \Store\Model\Entity\Erp $erp
 * @property int $store_id
 * @property \Store\Model\Entity\Store $store
 * @property string $model
 */
class Erp extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        '*' => true,
        'id' => false,
    ];
}
