<?php
namespace Store\Model\Entity;

use Cake\ORM\Entity;
use Manager\Model\Entity\CrudEntityTrait;

/**
 * Currency Entity.
 */
class Currency extends Entity
{

  use CrudEntityTrait;
  
  /**
   * Fields that can be mass assigned using newEntity() or patchEntity().
   *
   * @var array
   */
  protected $_accessible = [
    'title' => true,
    'iso_code' => true,
    'iso_code_num' => true,
    'active' => true,
    'sign' => true,
    'blank' => true,
    'format' => true,
    'decimals' => true,
    'conversion_rate' => true,
    'display_tax_label' => true,
    'deleted' => true,
  ];
}
