<?php
namespace Store\Model\Entity;

use Cake\ORM\Entity;
use Cake\ORM\Behavior\Translate\TranslateTrait;
use Manager\Model\Entity\CrudEntityTrait;
use Cake\ORM\TableRegistry;

/**
 * Attribute Entity.
 */
class Color extends Entity
{
  use CrudEntityTrait;

  /**
   * Fields that can be mass assigned using newEntity() or patchEntity().
   *
   * @var array
   */
  protected $_accessible = [
      'id' => true,
      'title' => true,
      'attribute_group_id' => true,
      'color' => true,
  ];


  protected function _getFullTitle()
  {
    if( is_object( $this->attribute_group))
    {
      return $this->attribute_group->title .' - '. $this->title;
    }
  }
}
