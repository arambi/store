<?php
namespace Store\Model\Entity;

use Cake\ORM\Entity;
use Cake\ORM\Behavior\Translate\TranslateTrait;
use Manager\Model\Entity\CrudEntityTrait;

/**
 * AttributeGroup Entity.
 */
class AttributeGroup extends Entity
{
  use CrudEntityTrait;
  use TranslateTrait;

  /**
   * Fields that can be mass assigned using newEntity() or patchEntity().
   *
   * @var array
   */
  protected $_accessible = [
      '*' => true,
      'attributes' => true
  ];

  protected function _getWebTitle()
  {
    if (!empty($this->store_title)) {
      return $this->store_title;
    }

    return $this->title;
  }
}
