<?php
namespace Store\Model\Entity;

use Cake\ORM\Entity;

/**
 * CarrierRuleZone Entity.
 */
class CarrierRuleZone extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * @var array
     */
    protected $_accessible = [
        'carrier_rule_id' => true,
        'zone_id' => true,
    ];
}
