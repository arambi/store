<?php

namespace Store\Model\Entity;

use Cake\I18n\Number;
use Website\Lib\Website;
use Store\Cart\Cart;
use Store\Currency\CurrencyCollection;

trait PriceTrait
{
    public function setPrice($value, $currency = null)
    {
        return CurrencyCollection::getCurrency($currency)->getFormat($value);
    }

    public function realPrice($price, $tax = false, $force_taxes = false)
    {
        if (Website::get('settings.store_price_with_taxes') || $force_taxes) {
            if (!$this->store_has_custom_taxes) {
                if ((is_object($tax) || is_numeric($tax))) {
                    $rate = is_object($tax) ? $tax->value : $tax;
                    $price = $price + ($price * $rate) / 100;
                }
            } else {
                $price = $price + $this->store_custom_taxes;
            }
        }

        return round($price, 2);
    }

    protected function _getPriceHuman()
    {
        return $this->setPrice($this->realPrice($this->price, $this->tax));
    }

    protected function _getPriceNoTaxHuman()
    {
        return $this->setPrice($this->realPrice($this->price));
    }

    /**
     * Devuelve el precio/precios de un producto y de sus product_attributes
     * 
     * @param  Shop\Model\Entity\Product    $product
     * @return string HTML
     */
    protected function _getPriceHtml()
    {
        $isView = false;

        $out = [];
        // Precio del producto
        $price = [];

        if (!empty($this->offer_price)) {
            $price[] = '<small style="text-decoration: line-through" class="shop-old-price">' . $this->original_price_human . '</small>';
        }

        $price[] = $this->price_human;

        $out[] = '<span>' . implode("\n", $price) . '</span>';

        if ($isView && !empty($this->product_attributes)) {
            foreach ($this->product_attributes as $pa) {
                $price = [];

                if (!empty($pa->offer_price)) {
                    $price[] = '<small style="text-decoration: line-through" class="shop-old-price">' . $pa->original_price_human . '</small>';
                }

                $price[] = $pa->price_human;
                $out[] = '<span>' . implode("\n", $price) . '</span>';
            }
        }

        return implode("\n", $out);
    }
}
