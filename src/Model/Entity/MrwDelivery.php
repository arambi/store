<?php
namespace Store\Model\Entity;

use Cake\ORM\Entity;

/**
 * MrwDeMrwDeliveryliery Entity
 *
 * @property int $id
 * @property int $order_id
 * @property string $request_number
 * @property string $shipping_number
 * @property string $message
 * @property string $state
 * @property string $ticket_file
 * @property \Cake\I18n\Time|null $created
 * @property \Cake\I18n\Time|null $modified
 *
 * @property \Store\Model\Entity\Order $order
 */
class MrwDelivery extends Entity
{
    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        '*' => true,
    ];
}
