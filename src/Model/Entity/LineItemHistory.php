<?php
namespace Store\Model\Entity;

use Cake\ORM\Entity;

/**
 * LineItemHistory Entity
 *
 * @property int $id
 * @property int|null $user_id
 * @property int|null $line_item_id
 * @property string|null $status
 * @property \Cake\I18n\Time|null $created
 *
 * @property \Store\Model\Entity\User $user
 * @property \Store\Model\Entity\LineItem $line_item
 */
class LineItemHistory extends Entity
{
    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'user_id' => true,
        'line_item_id' => true,
        'status' => true,
        'created' => true,
        'user' => true,
        'line_item' => true,
    ];
}
