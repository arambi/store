<?php

namespace Store\Model\Entity;

use Cake\ORM\Entity;
use Store\Config\StoreConfig;
use Manager\Model\Entity\CrudEntityTrait;

class Coupon extends Entity
{
    use CrudEntityTrait;


    protected $_accessible = [
        '*' => true
    ];

    private $order;

    public function setOrder($order)
    {
        $this->order = $order;
    }

    protected function _getMessage()
    {
        if (empty($this->products)) {
            return false;
        }

        $product_ids = collection($this->products)->extract('id')->toArray();
        $products_line_ids = collection($this->order->line_items)->extract('product_id')->toArray();

        if (count(array_intersect($products_line_ids, $product_ids)) == 0) {
            return __d('app', 'La promoción del código introducido corresponde a unos productos que no está en tu cesta de la compra');
        }

        return false;
    }

    public function discount($order)
    {
        if ($this->type == 'free_delivery') {
            return $this->applyFreeDelivery($order);
        }

        if (empty($this->products)) {

            if (StoreConfig::getConfig('couponStrategy') == StoreConfig::DISCOUNT_STRATEGY_WITHOUT_TAXES) {
                $discount_total = 0;

                foreach ($order->line_items as $item) {
                    $amount = $item->price;
                    $discount = ($amount * $this->value) / 100;
                    $item->set('discount', $discount);
                    $item->set('price', $item->price - $discount);
                    $discount_total += $discount * $item->quantity;
                }

                return $discount_total;
            }

            if (StoreConfig::getConfig('couponStrategy') == StoreConfig::DISCOUNT_STRATEGY_WITH_TAXES) {
                return $this->applyToOrder($order);
            }
        }

        return $this->applyToProducts($order);
    }

    private function applyFreeDelivery($order)
    {
        return $order->shipping_handling + $order->shipping_handling_tax;
    }

    private function applyToProducts($order)
    {
        $product_ids = collection($this->products)->extract('id')->toArray();

        $discount = 0;

        foreach ($order->line_items as $item) {
            if (in_array($item->product_id, $product_ids)) {
                if ($this->type == 'percent') {
                    $discount += (($item->subtotal + $item->taxes_total) * $this->value) / 100;
                } else {
                    $discount += $this->value;
                }
            }
        }

        return $discount;
    }

    private function applyToOrder($order)
    {
        if ($this->type == 'percent') {
            $amount = $order->subtotal + $order->line_items_taxes;
            $discount = ($amount * $this->value) / 100;
            return $discount;
        } else {
            return $this->value;
        }
    }
}
