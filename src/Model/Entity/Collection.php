<?php
namespace Store\Model\Entity;

use Manager\Model\Entity\CrudEntityTrait;
use Cake\ORM\Behavior\Translate\TranslateTrait;
use Slug\Model\Entity\SlugTrait;use Cake\ORM\Entity;

/**
 * Collection Entity.
 */
class Collection extends Entity
{
    use CrudEntityTrait;
    use TranslateTrait;
    use SlugTrait;

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     * Note that '*' is set to true, which allows all unspecified fields to be
     * mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        '*' => true,
        'slugs' => true,
    ];

    protected $_dirty = [
      'products_colors',
      'products'
    ];
}
