<?php

namespace Store\Model\Entity;

use Cake\ORM\Entity;
use Store\Model\Entity\PriceTrait;

/**
 * InvoiceRefundLineItem Entity
 *
 * @property int $id
 * @property int|null $invoice_refunds_id
 * @property int|null $line_item_id
 * @property int|null $quantity
 * @property float|null $price
 * @property float|null $tax_rate
 * @property \Cake\I18n\Time $created
 * @property \Cake\I18n\Time $modified
 *
 * @property \Store\Model\Entity\InvoiceRefund $invoice_refund
 * @property \Store\Model\Entity\LineItem $line_item
 */
class InvoiceRefundLineItem extends Entity
{
    use PriceTrait;

    protected $_accessible = [
        '*' => true,
    ];

    protected function _getSubtotal()
    {
        return ($this->price + $this->taxes) * $this->quantity;
    }

    protected function _getTaxes()
    {
        $total = $this->price + ($this->price * $this->tax_rate) / 100;
        $return = round($total, 2) - round($this->price, 2);
        return $return;
    }

    public function setOrderPrice($price)
    {
        return $this->setPrice($price, $this->currency);
    }
}
