<?php

namespace Store\Model\Entity;

use Cake\ORM\Entity;
use Cake\ORM\TableRegistry;
use Cofree\Lib\RemoteLocation;
use Manager\Model\Entity\CrudEntityTrait;
use Store\Model\Entity\PriceTrait;
use Cake\Routing\Router;
use ArrayObject;
use Cake\Event\EventDispatcherTrait;
use Cake\Event\Event;
use Cake\I18n\I18n;
use Cake\Utility\Text;
use I18n\Lib\Lang;
use Store\Config\StoreConfig;
use I18n\Model\Entity\DatesEntityTrait;
use Store\Cart\Cart;
use Website\Lib\Website;

/**
 * Order Entity.
 */
class Order extends Entity
{
    use CrudEntityTrait;
    use PriceTrait;
    use EventDispatcherTrait;
    use DatesEntityTrait;

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * @var array
     */
    protected $_accessible = [
        '*' => true,
        'user_id' => true,
        'carrier_id' => true,
        'express_delivery' => true,
        'payment_method_id' => true,
        'address_invoice_id' => true,
        'currency_id' => true,
        'session' => false,
        'salt' => false,
        'discount_price' => true,
        'points_discount' => true,
        'shipping_price' => true,
        'shipping_handling' => true,
        'shipping_handling_tax' => true,
        'shipping_handling_free' => true,
        'payment_price' => true,
        'payment_price_tax' => true,
        'subtotal' => true,
        'taxes' => true,
        'total' => true,
        'shipping_method' => true,
        'payment_method' => true,
        'status' => true,
        'comments' => true,
        'invoice_prefix' => true,
        'invoice_num' => true,
        'adr_store_company' => true,
        'adr_store_cif' => true,
        'adr_store_address' => true,
        'adr_store_postcode' => true,
        'adr_store_city' => true,
        'adr_store_state' => true,
        'adr_store_country' => true,
        'adr_store_phone' => true,
        'adr_invoice_company' => true,
        'adr_invoice_name' => true,
        'adr_invoice_type' => true,
        'adr_invoice_cif' => true,
        'adr_invoice_address' => true,
        'adr_invoice_postcode' => true,
        'adr_invoice_city' => true,
        'adr_invoice_state' => true,
        'adr_invoice_country' => true,
        'adr_delivery_company' => true,
        'adr_delivery_name' => true,
        'adr_delivery_type' => true,
        'adr_delivery_cif' => true,
        'adr_delivery_address' => true,
        'adr_delivery_postcode' => true,
        'adr_delivery_city' => true,
        'adr_delivery_state' => true,
        'adr_delivery_country' => true,
        'total_items' => true,
        'count_items' => true,
        'express_delivery' => true,
        'delivery_date' => true,
        'delivery_date_custom' => true,
        'delivery_date_input' => true,
        'delivery_days' => true,
        'has_invoice' => true,
        'order_date' => true,

        'delivery_date_next_day' => true,
        'grif_wrap' => true,
        'gift_wrapping_taxes' => true,
        'gift_wrapping_price' => true,
        'comments_gift' => true,
        'user_message' => true,
        'same_address' => true,
        'save_card' => true,
        'card_id' => true,
    ];


    protected $_virtual = [
        'total_items',
        'count_items',
        'status_human',
        'order_date_human',
        'order_date_human_day',
        'order_date_day',
        'line_taxes',
        'total_human',
        'total_human_no_tags',
        'subtotal_human',
        'shipping_handling_human',
        'payment_price_human',
        'discount_price_human',
        'delivery_date_next_day',
        'delivery_date_human',
        'delivery_date_next_day_human',
        'gift_wrapping_price_human',
        'total_without_taxes',
        'total_without_taxes_human',
        'gift_wrapping_taxes_rate',
        'points_discount_human',
        'points',
        'express_delivery_bool',
        'normal_delivery_bool',
        'is_carrier_free',
        'is_valid',
        'pretty_invoice_address',
        'pretty_delivery_address',
        'pretty_store_address',
        'invoice_url',
        'delivery_note_url',
        'has_invoice',
        'd_url',
        'order_url',
        'items_descriptions',
        'invoice_number',
        'invoice_type',
        'invoice_date',
        'has_express',
        'no_has_coupon',
        'coupon_discount_human',
        'coupon_discount',
        'promo_discount_human',
        'coupon_name',
        'total_taxes_human',
        'admin_custom_data',
        'locale_human'
    ];

    protected $_groupErrors = [
        'invoice' => [
            'adr_invoice_firstname',
            'adr_invoice_email',
            'adr_invoice_vat_number',
            'adr_invoice_email_confirmation',
            'adr_invoice_firstname',
            'adr_invoice_lastname',
            'adr_invoice_company',
            'adr_invoice_address',
            'adr_invoice_city',
            'adr_invoice_postcode',
            'adr_invoice_country_id',
            'adr_invoice_state_id',
            'adr_invoice_phone',
            'password',
            'password2',
        ],
        'delivery' => [
            'adr_delivery_firstname',
            'adr_delivery_lastname',
            'adr_delivery_company',
            'adr_delivery_address',
            'adr_delivery_city',
            'adr_delivery_postcode',
            'adr_delivery_country_id',
            'adr_delivery_state_id',
            'adr_delivery_phone',
        ]
    ];

    /**
     * Devuelve el peso total del pedido, haciendo la suma de todos los items
     * 
     * @return float
     */
    public function weight()
    {
        $total = 0;

        foreach ($this->line_items as $item) {
            $total += ($item->weight * $item->quantity);
        }

        return $total;
    }

    protected function _getWeight()
    {
        return $this->weight();
    }

    protected function _getForCart()
    {
        $address_fields = [
            'firstname',
            'lastname',
            'address1',
            'postcode',
            'city',
            'phone',
            'cif',
            'country_name',
            'state_name'
        ];

        $data = $this->extract([
            'salt',
            'subtotal',
            'subtotal_human',
            'total',
            'total_human',
            'total_items',
            'count_items',
            'has_items',
            'order_date_human',
            'order_date_day',
            'line_taxes',
            'shipping_handling_human',
            'shipping_handling_left_for_free',
            'shipping_handling_free_price',
            'payment_price_human',
            'payment_title',
            'discount_price_human',
            'delivery_date_next_day',
            'delivery_date',
            'delivery_date_human',
            'delivery_date_next_day_human',
            'is_carrier_free',
            'gift_wrapping_price_human',
            'total_taxes',
            'total_taxes_human',
            'total_without_taxes',
            'total_without_taxes_human',
            'gift_wrapping_taxes_rate',
            'points',
            'points_discount_human',
            'payment_id',
            'express_delivery',
            'carrier_rule_normal',
            'carrier_rule_express',
            'carrier_rule_id',
            'has_express',
            'express_delivery_bool',
            'normal_delivery_bool',
            'delivery_zone_id',
            'same_address',
            'want_account',
            'adr_delivery_firstname',
            'adr_delivery_lastname',
            'adr_delivery_company',
            'adr_delivery_name',
            'adr_delivery_type',
            'adr_delivery_vat_number',
            'adr_delivery_address',
            'adr_delivery_postcode',
            'adr_delivery_city',
            'adr_delivery_state',
            'adr_delivery_country',
            'adr_delivery_country_id',
            'adr_delivery_state_id',

            'adr_invoice_firstname',
            'adr_invoice_lastname',
            'adr_invoice_company',
            'adr_invoice_name',
            'adr_invoice_type',
            'adr_invoice_vat_number',
            'adr_invoice_address',
            'adr_invoice_postcode',
            'adr_invoice_city',
            'adr_invoice_state',
            'adr_invoice_country',
            'adr_invoice_country_id',
            'adr_invoice_state_id',

            'comments',
            'is_valid',
            'coupon_name',
            'no_has_coupon',
            'coupon_discount',
            'coupon_discount_human',
            'promo_discount',
            'promo_discount_human',
            'has_promo_discount',
            'coupon_message',

            'has_portable_products',
            'is_alculated',
            'is_shipping_free'
        ]);

        $extract_item_data = array_merge([
            'price',
            'full_title',
            'invoice_title',
            'quantity',
            'price_human',
            'subtotal_human',
            'description',
            'no_gift',
            'photo',
            'id',
            'url',
            'salt',
            'taxes',
            'is_giftcard',
            'giftcard_method',
            'ticket_refund_item_url',
            'no_portable',
            'price_without_discount_human',
            // 'giftcard_address',
            // 'giftcard_name',
            // 'giftcard_price',
            // 'giftcard_email',
            // 'giftcard_phone',
            // 'giftcard_state_id',
            // 'giftcard_country_id',
            // 'giftcard_postcode',
        ], StoreConfig::getConfig('itemFields'));

        foreach ($this->line_items as $item) {
            $_item = $item->extract($extract_item_data);

            if (method_exists($item->product, 'extractForCart')) {
                $_item['product'] = $item->product->extractForCart();
            }

            $data['line_items'][] = $_item;;
        }

        $data['carrier'] = $this->carrier->extract([
            'express_delivery',
            'express_delivery_days',
            'delivery_days',
        ]);

        $data['errors'] = $this->errors();
        $data['group_errors'] = $this->getGroupErrors();

        $data = new ArrayObject($data);

        $event = new Event('Store.Model.Entity.Order.forCart', $this, [
            'order' => $data,
        ]);

        $this->getEventManager()->dispatch($event);

        // Si el error de carrier es el único, se borra. Porque podría ser que la tarifa de envío no existe al no haber seleccionado el país o la provincia.
        if (isset($data['errors']) && isset($data['errors']['carrier_rule_id']) && count($data['errors']) > 1) {
            unset($data['errors']['carrier_rule_id']);
        }

        return $data;
    }

    protected function _getAdrDeliveryFirstname($value)
    {
        if ($value !== null) {
            return $value;
        }

        if (!empty($this->user->name)) {
            return $this->user->name;
        }
    }

    protected function _getOrderEmail($value)
    {
        if (!empty($this->adr_invoice_email)) {
            return $this->adr_invoice_email;
        }

        if (!empty($this->adr_delivery_email)) {
            return $this->adr_delivery_email;
        }

        if (!empty($this->user)) {
            return $this->user->email;
        }
    }

    protected function _getHasExpress($value)
    {
        if ($value !== null) {
            return $value;
        }

        $has = TableRegistry::getTableLocator()->get('Store.Carriers')->hasExpress($this);
        $this->set('has_express', $has);

        if (!$has) {
            $this->set('express_delivery', false);
        }
        return $has;
    }

    protected function _getPaymentTitle()
    {
        if ($this->payment_method) {
            return $this->payment_method->title;
        }
    }

    protected function _getCountryDelivery($value)
    {
        if (is_object($value)) {
            return $value;
        }

        if (!$value && !empty($this->adr_delivery_country_id)) {
            $country = TableRegistry::getTableLocator()->get('Store.Countries')->find()
                ->where([
                    'Countries.id' => $this->adr_delivery_country_id
                ])
                ->first();

            if ($country) {
                $this->set('country_delivery', $country);
                return $country;
            }
        }
    }

    protected function _getCountryInvoice($value)
    {
        if (is_object($value)) {
            return $value;
        }

        if (!$value && !empty($this->adr_invoice_country_id)) {
            $country = TableRegistry::getTableLocator()->get('Store.Countries')->find()
                ->where([
                    'Countries.id' => $this->adr_invoice_country_id
                ])
                ->first();

            if ($country) {
                $this->set('country_invoice', $country);
                return $country;
            }
        }
    }

    protected function _getStateInvoice($value)
    {
        if (is_object($value)) {
            return $value;
        }

        if (!$value && !empty($this->adr_invoice_state_id)) {
            $state = TableRegistry::getTableLocator()->get('Store.States')->find()
                ->where([
                    'States.id' => $this->adr_invoice_state_id
                ])
                ->first();

            if ($state) {
                $this->set('state_invoice', $state);
                return $state;
            }
        }
    }

    protected function _getStateDelivery($value)
    {
        if (is_object($value)) {
            return $value;
        }

        if (!$value && !empty($this->adr_delivery_state_id)) {
            $state = TableRegistry::getTableLocator()->get('Store.States')->find()
                ->where([
                    'States.id' => $this->adr_delivery_state_id
                ])
                ->first();

            if ($state) {
                $this->set('state_delivery', $state);
                return $state;
            }
        }
    }

    /**
     * Devuelve la zona de entrega
     * 
     * @return integer | null
     */
    protected function _getDeliveryZoneId()
    {
        $zone = new \stdClass;
        $zone->zone_id = null;

        $event = new Event('Store.Model.Entity.Order.gettingDeliveryZoneId', $this, [
            $zone,
        ]);
        $this->getEventManager()->dispatch($event);

        if ($zone->zone_id) {
            return $zone->zone_id;
        }

        if ($zone_id = $this->getZoneByPostcode()) {
            return $zone_id;
        }

        $state = $this->state_delivery;
        $key = 'adr_delivery_state_id';

        if (StoreConfig::getConfig('orderMainAddress') == 'invoice' && $this->same_address == 1) {
            $state =  $this->state_invoice;
            $key = 'adr_invoice_state_id';
        }

        if (
            is_object($state)
            && $state->id == $this->$key
            && !empty($state->zone_id)
        ) {
            return $state->zone_id;
        }

        $country = $this->country_delivery;

        if (StoreConfig::getConfig('orderMainAddress') == 'invoice' && $this->same_address == 1) {
            $country =  $this->country_invoice;
        }

        if (is_object($country) && !empty($country->zone_id)) {
            return $country->zone_id;
        }

        return null;
    }

    private function getZoneByPostcode()
    {
        $zones = TableRegistry::getTableLocator()->get('Store.Zones')->find();

        foreach ($zones as $zone) {
            if (in_array($this->adr_delivery_postcode, $zone->postcodes_array)) {
                return $zone->id;
            }
        }
    }

    protected function _getAdrInvoiceCountryTitle()
    {
        if ($this->country_delivery) {
            return $this->country_delivery->title;
        }
    }

    protected function _getAdrInvoiceStateTitle()
    {
        if ($this->country_delivery) {
            return $this->state_delivery->title;
        }
    }

    protected function _getAdrInvoiceCountryId($value)
    {
        if (StoreConfig::getConfig('orderMainAddress') == 'invoice') {
            return $value;
        }

        if ($this->same_address == 1) {
            return $this->adr_delivery_country_id;
        }

        return $value;
    }

    protected function _getAdrDeliveryCountryId($value)
    {
        // Event
        $event = new Event('Store.Model.Order.forceGetDeliveryCountry', $this, [
            'value' => $value,
        ]);
        $this->getEventManager()->dispatch($event);

        if ($event->result && $value === null) {
            return;
        }
        // Fin event

        if ($value === null) {
            if (env("SERVER_ADDR")) {
                $location = new RemoteLocation();

                if (!empty($location->country_code)) {
                    $country = TableRegistry::getTableLocator()->get('Store.Countries')->getByCode($location->country_code);

                    if ($country) {
                        $value = $country->id;
                    }
                }
            }
            if ($value === null) {
                $country = TableRegistry::getTableLocator()->get('Store.Countries')->getDefault();

                if ($country) {
                    $value = $country->id;
                }
            }
        }

        if (StoreConfig::getConfig('orderMainAddress') == 'delivery') {
            return $value;
        }

        if ($this->same_address == 1) {
            return $this->adr_invoice_country_id;
        }

        return $value;
    }

    protected function _getItemsDescriptions()
    {
        $return = new ArrayObject();

        if (!empty($this->line_items)) {
            foreach ($this->line_items as $item) {
                $return[] = $item->full_title;
            }

            $this->getEventManager()->dispatch(new Event('Store.Model.Entity.Order.itemsDescriptions', $this, [
                $return,
            ]));

            return Text::truncate(implode(' ; ', (array)$return), 100);
        }
    }

    protected function _getTotalItems()
    {
        if ($this->line_items) {
            return count($this->line_items);
        }
    }

    protected function _getCountItems()
    {
        $total = 0;

        if ($this->line_items) {
            foreach ($this->line_items as $item) {
                $total += $item->quantity;
            }
        }

        return $total;
    }

    protected function _getHasItems()
    {
        if ($this->line_items) {
            return count($this->line_items) > 0;
        }
    }

    protected function _getTotalWithoutTaxes()
    {
        $total =
            $this->subtotal +
            $this->shipping_handling +
            $this->points_discount +
            $this->gift_wrapping_price;


        if (StoreConfig::getConfig('couponStrategy') == StoreConfig::DISCOUNT_STRATEGY_WITHOUT_TAXES) {
            $total -= $this->coupon_discount;
        }

        return $total;
    }

    protected function _getTotalWithoutTaxesHuman()
    {
        return $this->setOrderPrice($this->total_without_taxes);
    }

    protected function _getTotalWithoutTaxesHumanNoTags()
    {
        return strip_tags($this->total_without_taxes_human);
    }


    public function setOrderPrice($price)
    {
        return $this->setPrice($price, $this->currency);
    }

    /**
     * Devuelve los diferentes porcentajes de impuestos aplicados a los items del pedido
     * Es decir, puede devolver un array como este
     * ``` 21 => 55
     *     16 => 12
     * ```
     *
     * Donde se indica el total de impuestos para lo aplicado al 21% y al 16%
     * 
     * @return array
     */
    protected function _getLineTaxes()
    {
        $taxes = $this->line_taxes_num;

        if (!empty($taxes)) {
            foreach ($taxes as $key => $value) {
                $taxes[$key] = $this->setOrderPrice($value);
            }

            return $taxes;
        }
    }

    protected function _getLineTaxesNum($value)
    {
        if ($value !== null) {
            return $value;
        }

        $taxes = [];

        if (!$this->has('line_items')) {
            return null;
        }

        foreach ($this->line_items as $item) {
            if (!$item->tax_rate) {
                continue;
            }

            if (isset($item->tax_rate) && !array_key_exists((int)$item->tax_rate, $taxes)) {
                $taxes[$item->tax_rate] = 0;
            }

            $taxes[$item->tax_rate] += $item->taxes_total;
        }

        // Impuestos de gastos de envio
        if (!empty($this->shipping_handling_tax)) {
            if (!isset($taxes[$this->shipping_handling_tax_rate])) {
                $taxes[$this->shipping_handling_tax_rate] = 0;
            }

            $taxes[$this->shipping_handling_tax_rate] += $this->shipping_handling_tax;
        }

        // Impuestos de gastos de pago
        if (!empty($this->payment_price_tax)) {
            if (!isset($taxes[$this->payment_price_tax])) {
                $taxes[$this->payment_price_tax] = 0;
            }

            $taxes[$this->payment_price_tax_rate] += $this->payment_price_tax;
        }

        // Impuestos de puntos de descuento
        if (!empty($this->points_discount_tax)) {
            if (!isset($taxes[$this->points_discount_tax_rate])) {
                $taxes[$this->points_discount_tax_rate] = 0;
            }

            $taxes[$this->points_discount_tax_rate] += $this->points_discount_tax;
        }

        // Impuestos de envoltorio para regalo
        if (!empty($this->gift_wrapping_taxes)) {
            if (!isset($taxes[$this->gift_wrapping_taxes_rate])) {
                $taxes[$this->gift_wrapping_taxes_rate] = 0;
            }

            $taxes[$this->gift_wrapping_taxes_rate] += $this->gift_wrapping_taxes;
        }

        $this->set('line_taxes_num', $taxes);

        return $taxes;
    }

    protected function _getTotalTaxes()
    {
        $total = 0;

        if (!empty($this->line_taxes_num)) {
            foreach ($this->line_taxes_num as $value => $_total) {
                $total += $_total;
            }

            return $total;
        }
    }

    protected function _getTotalTaxesHumanNoTags()
    {
        return strip_tags($this->total_taxes_human);
    }

    protected function _getTotalTaxesHuman()
    {
        if ($this->taxes !== null) {
            return $this->setOrderPrice($this->total_taxes);
        }

        return $this->setOrderPrice($this->total_taxes);
    }

    protected function _getLineItemsTaxes()
    {
        $total = 0;

        if (!$this->has('line_items')) {
            return null;
        }

        foreach ($this->line_items as $item) {
            $total += $item->taxes_total;
        }

        return $total;
    }

    protected function _getLineItemsTotals()
    {
        $total = 0;

        if (!$this->has('line_items')) {
            return null;
        }

        foreach ($this->line_items as $item) {
            $total += $item->subtotal + $item->taxes_total;
        }

        return $total;
    }

    protected function _getStatusHuman()
    {
        $table = $this->getTable();

        if (isset($table->statuses[$this->status])) {
            return $table->statuses[$this->status];
        }

        return null;
    }

    protected function _getTotalHuman()
    {
        return $this->setOrderPrice($this->total);
    }

    protected function _getTotalHumanNoTags()
    {
        return strip_tags($this->setOrderPrice($this->total));
    }

    protected function _getDiscountPriceHuman()
    {
        return $this->setOrderPrice($this->discount_price);
    }

    protected function _getPercentDiscount()
    {
        if ($this->discount_price == 0) {
            return 0;
        }

        return ($this->discount_price * 100) / $this->subtotal;
    }

    protected function _getSubtotalHuman()
    {
        if (is_array($this->line_items)) {
            $subtotal = 0;

            foreach ($this->line_items as $item) {
                if (isset($item->tax_rate)) {
                    if ($item->store_has_custom_taxes) {
                        $subtotal += ($item->price + $item->store_custom_taxes) * $item->quantity;
                    } else {
                        $subtotal += ($this->realPrice($item->price, $item->tax_rate)) * $item->quantity;
                    }
                } else {
                    $subtotal += $item->subtotal;
                }
            }
        } elseif ($this->subtotal) {
            $subtotal = $this->subtotal;
        }

        if (isset($subtotal)) {
            return $this->setOrderPrice($subtotal);
        }
    }

    protected function _getSubtotalNum()
    {
        if (is_array($this->line_items)) {
            $subtotal = 0;

            foreach ($this->line_items as $item) {
                if (isset($item->tax_rate)) {
                    $subtotal += ($this->realPrice($item->price, $item->tax_rate)) * $item->quantity;
                } else {
                    $subtotal += $item->subtotal;
                }
            }
        } elseif ($this->subtotal) {
            $subtotal = $this->subtotal;
        }

        if (isset($subtotal)) {
            return $subtotal;
        }
    }

    protected function _getGiftWrappingPriceHuman()
    {
        return $this->setOrderPrice($this->realPrice($this->gift_wrapping_price, $this->gift_wrapping_taxes_rate));
    }

    protected function _getIsShippingFree()
    {
        if (($this->carrier_rule_id === null && empty($this->status) && !$this->custom_shipping)) {
            return false;
        }

        if($this->shipping_handling == 0){
            return true;
        }

        return false;
    }

    protected function _getShippingHandlingHuman()
    {
        if (($this->carrier_rule_id === null && empty($this->status) && !$this->custom_shipping) || empty($this->shipping_handling)) {
            return '-';
        }

        return $this->setOrderPrice($this->realPrice($this->shipping_handling, $this->shipping_handling_tax_rate));
    }

    protected function _getShippingHandlingTaxHuman()
    {
        if (($this->carrier_rule_id === null && empty($this->status) && !$this->custom_shipping) || empty($this->shipping_handling_tax)) {
            return '-';
        }

        return $this->setOrderPrice($this->shipping_handling_tax);
    }

    protected function _getPaymentPriceHuman()
    {
        if (empty($this->payment_price)) {
            return null;
        }

        return $this->setOrderPrice($this->realPrice($this->payment_price, 0));
    }

    protected function _getPointsDiscountHuman()
    {
        return $this->setOrderPrice($this->realPrice($this->points_discount, $this->points_discount_tax_rate));
        return $this->setOrderPrice($this->points_discount + $this->points_discount_tax);
    }

    protected function _getPoints()
    {
        return $this->points_down ? $this->points_down->points : 0;
    }

    protected function _getOrderDateHuman()
    {
        if (!is_object($this->order_date)) {
            return null;
        }

        return date('d/m/Y H:i', $this->order_date->toUnixString());
    }

    protected function _getOrderDateHumanDay()
    {
        if (!is_object($this->order_date)) {
            return null;
        }

        return date('d/m/Y', $this->order_date->toUnixString());
    }


    protected function _getDeliveryDateNextDay()
    {
        if (empty($this->delivery_date)) {
            return null;
        }

        $delivery_date = $this->delivery_date;

        if (is_object($delivery_date)) {
            $delivery_date = date('Y-m-d', $delivery_date->toUnixString());
        }

        $date = date('Y-m-d', strtotime('+1 day', strtotime($delivery_date)));
        $dayweek = date('w', strtotime($date));

        if ($dayweek == 0) {
            $date = date('Y-m-d', strtotime('+1 day', strtotime($date)));
        }

        if ($dayweek == 6) {
            $date = date('Y-m-d', strtotime('+2 day', strtotime($date)));
        }
        return $date;
    }

    public function _getExpressDeliveryBool()
    {
        return $this->express_delivery;
    }

    public function _getNormalDeliveryBool()
    {
        return !$this->express_delivery;
    }


    public function _getDeliveryDateNextDayHuman()
    {
        return $this->datePostShort('delivery_date_next_day');
    }

    public function _getDeliveryDateHuman()
    {
        return $this->datePostShort('delivery_date');
    }

    public function _getAllTotals()
    {
        return [
            'subtotal_human' => $this->subtotal_human,
            'points_discount' => $this->points_discount,
            'points_discount_tax_rate' => $this->points_discount_tax_rate,
            'points_discount_tax' => $this->points_discount_tax,
            'discount_human' => $this->points_down->discount_human,
            'shipping_handling_human' => $this->shipping_handling_human,
            'gift_wrapping_price_human' => $this->gift_wrapping_price_human,
            'total_without_taxes_human' => $this->total_without_taxes_human,
            'total_without_taxes_human' => $this->total_without_taxes_human,
            'line_taxes' => $this->line_taxes,
            'total_human' => $this->total_human
        ];
    }

    protected function _getInvoiceUrl()
    {
        if (!$this->has_invoice) {
            return $this->delivery_note_url;
        }

        return Router::url([
            'prefix' => false,
            'plugin' => 'Store',
            'controller' => 'Orders',
            'action' => 'invoiceDownload',
            'filename' => Text::slug($this->adr_store_company . '-' . $this->invoice_num),
            '?' => [
                'salt' => $this->salt
            ],
            '_ext' => 'pdf',
        ], true);
    }

    protected function _getInvoiceUrlOriginal()
    {
        if (!$this->locale) {
            return $this->invoice_url;
        }

        $current_locale = I18n::getLocale();
        I18n::setLocale($this->locale);
        Router::reload();

        if (!$this->has_invoice) {
            return $this->delivery_note_url;
        }

        $url = Router::url([
            'prefix' => false,
            'plugin' => 'Store',
            'controller' => 'Orders',
            'action' => 'invoiceDownload',
            'filename' => Text::slug($this->adr_store_company . '-' . $this->invoice_num),
            '?' => [
                'salt' => $this->salt
            ],
            '_ext' => 'pdf',
        ], true);

        I18n::setLocale($current_locale);
        Router::reload();
        return $url;
    }

    protected function _getDeliveryNoteUrl()
    {
        return Router::url([
            'prefix' => false,
            'plugin' => 'Store',
            'controller' => 'Orders',
            'action' => 'deliveryNoteDownload',
            'filename' => Text::slug($this->adr_store_company . '-' . $this->id),
            '?' => [
                'salt' => $this->salt
            ],
            '_ext' => 'pdf',
        ], true);
    }

    protected function _getOrderUrl()
    {
        $custom = StoreConfig::getHook('orderUrl');

        if ($custom) {
            return $custom($this);
        }

        return Router::url([
            'prefix' => false,
            'plugin' => 'Store',
            'controller' => 'Users',
            'action' => 'order',
            'salt' => $this->salt,
        ]);
    }

    /**
     * Devuelve el número de factura o recibo
     *
     * @return string
     */
    protected function _getInvoiceNumber()
    {
        if (!empty($this->invoice) || !empty($this->invoice_id)) {
            return StoreConfig::getHook('invoicePrefix', [$this->invoice]);
        }
    }

    protected function _getInvoiceType()
    {
        if (!empty($this->invoice_id)) {
            return 'invoice';
        }

        if (!empty($this->receipt_id)) {
            return 'receipt';
        }
    }

    protected function _getHasInvoice()
    {
        return !empty($this->invoice);
    }

    protected function _getHasReceipt()
    {
        return !empty($this->receipt_id);
    }

    protected function _getInvoice($value)
    {
        if ($value !== null) {
            return $value;
        }

        $invoice = TableRegistry::getTableLocator()->get('Store.Invoices')->find()
            ->where([
                'order_id' => $this->id
            ])
            ->first();

        $this->set('invoice', $invoice);
        return $invoice;
    }

    protected function _getInvoiceDate()
    {
        if (!empty($this->invoice) || !empty($this->invoice_id)) {
            if (empty($this->invoice)) {
                $invoice = TableRegistry::getTableLocator()->get('Store.Invoices')->get($this->invoice_id);
                $this->set('invoice', $invoice);
            }

            return $this->invoice->datePostShort('created');
        }

        if (!empty($this->receipt) || !empty($this->receipt_id)) {
            if (empty($this->receipt)) {
                $receipt = TableRegistry::getTableLocator()->get('Store.Receipts')->get($this->receipt_id);
                $this->set('receipt', $receipt);
            }

            return $this->receipt->datePostShort('created');
        }
    }

    /**
     * Da el total del cupón de descuento
     *
     * @return void
     */
    protected function _getCouponDiscountHuman()
    {
        return $this->setOrderPrice($this->coupon_discount);
    }

    protected function _getPromoDiscountHuman()
    {
        return $this->setOrderPrice($this->promo_discount);
    }

    protected function _getHasPromoDiscount()
    {
        return !empty($this->promo_discount);
    }
    /**
     * Devuelve true si el pedido no tiene cupón de descuento
     *
     * @return boolean
     */
    public function _getNoHasCoupon()
    {
        return !$this->coupon_name;
    }

    /**
     * da el nombre del cupón de descuento
     *
     * @param string $value
     * @return void
     */
    protected function _getCouponName($value)
    {
        if ($value) {
            return $value;
        }

        $coupon = $this->order_coupon;

        if ($coupon) {
            $this->set('coupon_name', @$coupon->coupon->code);
            $this->set('coupon_id', @$coupon->coupon->id);
            return @$coupon->coupon->code;
        }

        return false;
    }

    protected function _getCouponMessage()
    {
        $coupon = $this->order_coupon;

        if (!$coupon || !$coupon->coupon->message) {
            return false;
        }

        return $coupon->coupon->message;
    }

    protected function _getOrderCoupon($value)
    {
        if ($value !== null) {
            return $value;
        }

        $coupon = TableRegistry::getTableLocator()->get('Store.CouponsOrders')->find()
            ->where([
                'CouponsOrders.order_id' => $this->id,
            ])
            ->contain('Coupons.Products')
            ->first();

        if ($coupon && $coupon->coupon) {
            $coupon->coupon->setOrder($this);
        }

        $this->set('order_coupon', $coupon);
        return $coupon;
    }



    public function mostInvoice()
    {
        return !empty($this->address_invoice->cif);
    }


    public function unsetErrors()
    {
        $this->_errors = [];
    }

    public function unsetError($key)
    {
        unset($this->_errors[$key]);
    }

    public function _getIsValid()
    {
        if (!empty($this->_errors)) {
            return false;
        }

        return true;
    }

    protected function _setUserId($value)
    {
        if ($this->user_id === null && $value) {
            $order = TableRegistry::getTableLocator()->get('Store.Orders')->getLastUserShopping($value);
            $user = TableRegistry::getTableLocator()->get('User.Users')->find()
                ->where([
                    'Users.id' => $value
                ])
                ->first();

            if ($order) {
                $this->setAddresses($user, $order);
                $this->set('country_invoice', $this->country_invoice);
                $this->set('state_invoice', $this->state_invoice);

                if (!$this->isEqualsAddresses($order)) {
                    $this->set('same_address', 0);
                } else {
                    $this->set('same_address', 1);
                }
            }
        }

        return $value;
    }

    public function isEqualsAddresses()
    {
        $fields = [
            'firstname',
            'lastname',
            'company',
            'address',
            'country_id',
            'state_id',
            'city',
            'phone',
            'postcode',
        ];

        $is = true;

        foreach ($fields as $field) {
            $prop1 = 'adr_invoice_' . $field;
            $prop2 = 'adr_delivery_' . $field;
            $value1 =  trim($this->get($prop1));
            $value2 =  trim($this->get($prop2));

            if ($value1 != $value2) {
                $is = false;
                break;
            }
        }

        return $is;
    }

    public function setAddresses($user, $order = null)
    {
        if (!$user) {
            return;
        }

        $fields = [
            'firstname',
            'lastname',
            'company',
            'address',
            'country_id',
            'state_id',
            'city',
            'phone',
            'postcode',
            'vat_number'
        ];

        foreach (['invoice', 'delivery'] as $type) {
            $address = TableRegistry::getTableLocator()->get('Store.Addresses')->find()
                ->where([
                    'Addresses.user_id' => $user->id,
                    'Addresses.type' => $type
                ])
                ->first();
            \Cake\Log\Log::debug($address);

            if ($address) {
                foreach ($fields as $field) {
                    $key = 'adr_' . $type . '_' . $field;
                    $this->set($key, $address->get($field));
                }
            } elseif ($order) {
                foreach ($fields as $field) {
                    $key = 'adr_' . $type . '_' . $field;
                    $this->set($key, $order->$key);
                }
            }
        }


        if ($user && empty($this->adr_invoice_email)) {
            $this->set('adr_invoice_email', $user->email);
        }
    }

    protected function _getPrettyDeliveryAddress()
    {
        if (
            empty($this->adr_delivery_firstname) &&
            empty($this->adr_delivery_lastname) &&
            empty($this->adr_delivery_address) &&
            empty($this->adr_delivery_city) &&
            empty($this->adr_delivery_phone)
        ) {
            return null;
        }

        $out = [
            $this->adr_delivery_firstname . ' ' . $this->adr_delivery_lastname,

        ];

        if (!empty($this->adr_delivery_company)) {
            $out[] = $this->adr_delivery_company;
        }

        $out[] = $this->adr_delivery_address;
        $out[] = $this->adr_delivery_postcode . ' ' . $this->adr_delivery_city;


        if (!empty($this->state_delivery)) {
            $state = @$this->state_delivery->title;

            if ($this->country_delivery && !empty($this->country_delivery->title)) {
                $state .= ' (' . @$this->country_delivery->title . ')';
            }

            $out[] = $state;
        }

        if (!empty($this->adr_delivery_phone)) {
            $out[] = __d('app', 'Tel.') . ' ' . $this->adr_delivery_phone;
        }

        if (!empty($this->adr_delivery_vat_number)) {
            $out[] = __d('app', 'NIF/CIF') . ': ' . $this->adr_invoice_vat_number;
        }

        return implode('<br>', $out);
    }

    protected function _getPrettyStoreAddress()
    {
        $out = [
            $this->adr_store_company,
            $this->adr_store_cif,
            $this->adr_store_address,
            $this->adr_store_postcode . ' ' . $this->adr_store_city,
            $this->adr_store_state . (isset($this->adr_store_country) ? ' (' . $this->adr_store_country . ')'  : ''),
            __d('app', 'Tel.') . ' ' . $this->adr_store_phone,
        ];

        $out = array_filter($out, function ($value) {
            return !empty($value);
        });

        return implode('<br>', $out);
    }

    protected function _getPrettyInvoiceAddress()
    {
        $out = [];

        if (!empty($this->adr_invoice_firstname) || !empty($this->adr_invoice_lastname)) {
            $out[] = $this->adr_invoice_firstname . ' ' . $this->adr_invoice_lastname;
        }

        if (!empty($this->adr_invoice_company)) {
            $out[] = $this->adr_invoice_company;
        }

        if (!empty($this->adr_invoice_address)) {
            $out[] = $this->adr_invoice_address;
        }

        if (!empty($this->adr_invoice_city)) {
            $out[] = $this->adr_invoice_postcode . ' ' . $this->adr_invoice_city;
        }

        if (!empty($this->state_invoice)) {
            $state = @$this->state_invoice->title;

            if ($this->country_invoice && !empty($this->country_invoice->title)) {
                $state .= ' (' . @$this->country_invoice->title . ')';
            }

            $out[] = $state;
        }

        if (!empty($this->adr_invoice_phone)) {
            $out[] = __d('app', 'Tel.') . ' ' . $this->adr_invoice_phone;
        }

        if (!empty($this->adr_invoice_email)) {
            $out[] = __d('app', 'Email.') . ' ' . $this->adr_invoice_email;
        }

        if (!empty($this->adr_invoice_vat_number)) {
            $out[] = __d('app', 'NIF/CIF') . ': ' . $this->adr_invoice_vat_number;
        }

        $out = array_filter($out, function ($value) {
            return !empty($value);
        });

        return implode('<br>', $out);
    }


    protected function _setAdrDeliveryCountryId($value)
    {
        if ($value === null) {
            return 6;
        }

        return $value;
    }

    protected function _getCarrier($value)
    {
        if ($value !== null) {
            return $value;
        }

        if (!empty($this->carrier_id)) {
            $carrier = TableRegistry::getTableLocator()->get('Store.Carriers')
                ->find()
                ->contain([
                    'Taxes'
                ])
                ->where([
                    'Carriers.id' => $this->carrier_id,
                ])
                ->first();

            $this->set('carrier', $carrier);
            return $carrier;
        }
    }

    protected function _getIsCarrierFree()
    {
        if ($this->carrier && $this->carrier->is_free) {
            return true;
        }

        return false;
    }

    public function getGroupErrors()
    {
        $return = [];

        foreach ($this->getErrors() as $field => $message) {
            foreach ($this->_groupErrors as $group => $fields) {
                if (in_array($field, $fields)) {
                    $return[$group][] = $field;
                }
            }
        }

        return $return;
    }

    protected function _getTotalForQuota()
    {
        $total = $this->subtotal + $this->points_discount;

        if (Website::get('settings.store_price_with_taxes')) {
            $total += $this->line_items_taxes;
        }


        if (StoreConfig::getConfig('couponDiscountApplyToCarrier')) {
            $total = $total - $this->coupon_discount;
        }

        $obj = new \stdClass();
        $obj->total = $total;

        $event = new Event('Store.Model.Entity.Order.totalForQuota', $this, [
            $obj,
        ]);

        $this->getEventManager()->dispatch($event);
        return $obj->total;
    }

    public function _getHasPortableProducts()
    {
        $has = false;

        foreach ((array)$this->line_items as $item) {
            if (!$item->is_giftcard && !$item->no_portable) {
                $has = true;
                break;
            }

            if ($item->is_giftcard && $item->giftcard_method == 'post' && !$item->no_portable) {
                $has = true;
                break;
            }
        }

        return $has;
    }

    protected function _getTicketDeliveryUrl()
    {
        $url = StoreConfig::getHook('getTicketDeliveryUrl', [$this->id]);
        return $url;
    }

    protected function _getAdminCustomData($value)
    {
        if ($value !== null) {
            return $value;
        }

        $this->set('admin_custom_data', '');

        $event = new Event('Store.Model.Entity.Order.adminCustomData', $this);
        $this->getEventManager()->dispatch($event);
        return $this->get('admin_custom_data');
    }

    protected function _getAdminCustomDelivery($value)
    {
        if ($value !== null) {
            return $value;
        }

        $this->set('admin_custom_delivery', false);
        $event = new Event('Store.Model.Entity.Order.adminCustomDelivery', $this);
        $this->getEventManager()->dispatch($event);
        return $this->get('admin_custom_delivery');
    }

    public function canRefundItem()
    {
        $days = Website::get('settings.store_days_limit_refund');

        if (!$days) {
            return false;
        }

        $status = TableRegistry::getTableLocator()->get('Store.OrderHistories')
            ->find()
            ->where([
                'status' => 'envoy',
                'order_id' => $this->id
            ])
            ->order([
                'created' => 'desc'
            ])
            ->first();

        if ($status) {
            $diff = time() - $status->created->toUnixString();
            $diff_days = $diff / 60 / 60 / 24;
            return $days > $diff_days;
        }

        return false;
    }

    protected function _getLocaleHuman()
    {
        if (!$this->locale) {
            return;
        }

        return Lang::getName($this->locale);
    }

    protected function _getUser($value)
    {
        return $this->getBelongsTo($value, 'user');
    }
}
