<?php
namespace Store\Model\Entity;

use Cake\ORM\Entity;
use I18n\Model\Entity\DatesEntityTrait;
use Manager\Model\Entity\CrudEntityTrait;

/**
 * Fav Entity.
 */
class Fav extends Entity
{
  use DatesEntityTrait;
  use CrudEntityTrait;
  

  /**
   * Fields that can be mass assigned using newEntity() or patchEntity().
   *
   * @var array
   */
  protected $_accessible = [
      '*' => true,
  ];

  protected $_virtual = [
    'title',
  ];

  protected function _getTitle()
  {
    if( $this->product)
    {
      return $this->product->title;
    }
  }

}
