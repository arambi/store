<?php
namespace Store\Model\Entity;

use Cake\ORM\Entity;

/**
 * CorreosPreregistro Entity
 *
 * @property int $id
 * @property string|null $sender_city_name
 * @property string|null $sender_street_name
 * @property string|null $sender_province_name
 * @property string|null $sender_street_number
 * @property string|null $sender_name
 * @property string|null $sender_postcode
 * @property string|null $sender_phone
 * @property string|null $sender_email
 * @property string|null $receiver_city_name
 * @property string|null $receiver_street_name
 * @property string|null $receiver_province_name
 * @property string|null $receiver_street_number
 * @property string|null $receiver_name
 * @property string|null $receiver_postcode
 * @property string|null $receiver_phone
 * @property string|null $receiver_email
 * @property \Cake\I18n\Time|null $created
 * @property \Cake\I18n\Time|null $modified
 */
class CorreosPreregistro extends Entity
{
    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        '*' => true,
    ];
}
