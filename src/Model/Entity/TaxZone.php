<?php
namespace Store\Model\Entity;

use Cake\ORM\Entity;

/**
 * TaxZone Entity.
 */
class TaxZone extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * @var array
     */
    protected $_accessible = [
        'tax_id' => true,
        'zone_id' => true,
    ];
}
