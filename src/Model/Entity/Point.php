<?php
namespace Store\Model\Entity;

use Manager\Model\Entity\CrudEntityTrait;use Cake\ORM\Entity;
use Store\Model\Entity\PriceTrait;

/**
 * Point Entity.
 */
class Point extends Entity
{
  use CrudEntityTrait;
  use PriceTrait;
  /**
   * Fields that can be mass assigned using newEntity() or patchEntity().
   *
   * @var array
   */
  protected $_accessible = [
      '*' => true,
      'id' => false,
  ];

  protected $_virtual = [
    'discount_human',
  ];



  protected function _getDiscountHuman()
  {
    return $this->setPrice( $this->discount);
  }

}
