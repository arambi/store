<?php
namespace Store\Model\Entity;

use Manager\Model\Entity\CrudEntityTrait;use Cake\ORM\Entity;

/**
 * TaxRating Entity
 *
 * @property int $id
 * @property float $value
 * @property int $tax_id
 * @property int $country_id
 * @property int $state_id
 * @property \Cake\I18n\Time $created
 * @property \Cake\I18n\Time $modified
 */
class TaxRating extends Entity
{
    use CrudEntityTrait;

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        '*' => true
    ];
}
