<?php
namespace Store\Model\Entity;

use Cake\ORM\Entity;
use Manager\Model\Entity\CrudEntityTrait;

/**
 * State Entity.
 */
class State extends Entity
{
  use CrudEntityTrait;

  /**
   * Fields that can be mass assigned using newEntity() or patchEntity().
   *
   * @var array
   */
  protected $_accessible = [
      '*' => true,
      'zone' => true,
      'country' => true,
  ];


  protected $_virtual = [
    'full_title'
  ];
}
