<?php
namespace Store\Model\Entity;

use Cake\ORM\Entity;
use Manager\Model\Entity\CrudEntityTrait;

/**
 * Zone Entity.
 */
class Zone extends Entity
{
  use CrudEntityTrait;
  
  /**
   * Fields that can be mass assigned using newEntity() or patchEntity().
   *
   * @var array
   */
  protected $_accessible = [
    '*' => true,
  ];

  protected function _getPostcodesArray()
  {
    $codes = array_map( function( $value){
      return trim( $value);
    }, explode("\n", $this->postcodes));

    $codes = array_filter( $codes, function( $value) {
      return !empty( $value);
    });

    return $codes;
  }
}
