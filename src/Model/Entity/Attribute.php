<?php
namespace Store\Model\Entity;

use Cake\ORM\Entity;
use Cake\ORM\Behavior\Translate\TranslateTrait;
use Manager\Model\Entity\CrudEntityTrait;
use Cake\ORM\TableRegistry;

/**
 * Attribute Entity.
 */
class Attribute extends Entity
{
  use CrudEntityTrait;
  use TranslateTrait;

  /**
   * Fields that can be mass assigned using newEntity() or patchEntity().
   *
   * @var array
   */
  protected $_accessible = [
      '*' => true,
      'position' => false
  ];

  protected static $_cached = [];

/**
 * Association Lazyload
 * Toma el attribute group
 * De esta manera no es necesario hacer contain hacia AttributeGroup
 * 
 * @return Entity
 */
  protected function _getAttributeGroup()
  {
    if( isset( static::$_cached [$this->attribute_group_id]))
    {
      return static::$_cached [$this->attribute_group_id];
    }

    $AttributeGroups = TableRegistry::get( 'Store.AttributeGroups');
    $group = $AttributeGroups->find()
      ->where( ['AttributeGroups.id' => $this->attribute_group_id])
      ->first();

    static::$_cached [$this->attribute_group_id] = $group;
    return $group;
  }

  protected function _getFullTitle()
  {
    if( is_object( $this->attribute_group))
    {
      return $this->attribute_group->title .' - '. $this->title;
    }
  }
}
