<?php
namespace Store\Model\Entity;

use Manager\Model\Entity\CrudEntityTrait;use Cake\ORM\Entity;

/**
 * CarrierRule Entity.
 */
class CarrierRule extends Entity
{
    use CrudEntityTrait;

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * @var array
     */
    protected $_accessible = [
        'active' => true,
        'type' => true,
        'carrier_id' => true,
        'price' => true,
        'min' => true,
        'max' => true,
        'salt' => true,
        'zones' => true,
        '*' => true,
    ];
}
