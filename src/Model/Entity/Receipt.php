<?php
namespace Store\Model\Entity;

use Manager\Model\Entity\CrudEntityTrait;
use Cake\ORM\Entity;
use Website\Lib\Website;
use I18n\Model\Entity\DatesEntityTrait;
use Store\Model\Entity\InvoiceTrait;

/**
 * Receipt Entity.
 */
class Receipt extends Entity
{
  use CrudEntityTrait;
  use DatesEntityTrait;
  use InvoiceTrait;

  /**
   * Fields that can be mass assigned using newEntity() or patchEntity().
   * Note that '*' is set to true, which allows all unspecified fields to be
   * mass assigned. For security purposes, it is advised to set '*' to false
   * (or remove), and explicitly make individual fields accessible as needed.
   *
   * @var array
   */
  protected $_accessible = [
      '*' => true,
      'id' => false,
  ];

  protected $_virtual = [
    'invoice_num',
    'name',
    'total_human',
    'invoice_url',
    'invoice_number',
    'invoice_date',
  ];



}
