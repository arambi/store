<?php
namespace Store\Model\Entity;

use Cake\ORM\Entity;

/**
 * CollectionsProductsProduct Entity.
 *
 * @property int $id
 * @property int $collection_id
 * @property \Store\Model\Entity\Collection $collection
 * @property int $product_id
 * @property \Store\Model\Entity\Product $product
 * @property \Cake\I18n\Time $created
 */
class CollectionsProductsProduct extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        '*' => true,
        'id' => false,
    ];
}
