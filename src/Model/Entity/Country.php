<?php
namespace Store\Model\Entity;

use Cake\ORM\Entity;
use Cake\ORM\Behavior\Translate\TranslateTrait;
use Manager\Model\Entity\CrudEntityTrait;

/**
 * Country Entity.
 */
class Country extends Entity
{
  use CrudEntityTrait;
  use TranslateTrait;

  /**
   * Fields that can be mass assigned using newEntity() or patchEntity().
   *
   * @var array
   */
  protected $_accessible = [
    '*' => true,
    'title' => true,
    'zone_id' => true,
    'currency_id' => true,
    'iso_code' => true,
    'call_prefix' => true,
    'active' => true,
    'contains_states' => true,
    'need_identification_number' => true,
    'need_zip_code' => true,
    'zip_code_format' => true,
    'display_tax_label' => true,
    'zone' => true,
    'country' => true,
    'states' => true,
  ];

  protected $_virtual = [
    'full_title'
  ];
}
