<?php
namespace Store\Model\Entity;

use Manager\Model\Entity\CrudEntityTrait;use Cake\ORM\Entity;
use Cake\ORM\TableRegistry;

/**
 * OrderUserAction Entity
 *
 * @property int $id
 * @property int|null $session_id
 * @property string|null $errors
 * @property string|null $post
 * @property string|null $response
 * @property string|null $url
 * @property string|null $action
 * @property \Cake\I18n\Time $created
 */
class OrderUserAction extends Entity
{
    use CrudEntityTrait;
    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        '*' => true,
    ];
}
