<?php
namespace Store\Model\Entity;

use Manager\Model\Entity\CrudEntityTrait;use Cake\ORM\Entity;

/**
 * Payment Entity.
 */
class Payment extends Entity
{
  use CrudEntityTrait;

  /**
   * Fields that can be mass assigned using newEntity() or patchEntity().
   * Note that '*' is set to true, which allows all unspecified fields to be
   * mass assigned. For security purposes, it is advised to set '*' to false
   * (or remove), and explicitly make individual fields accessible as needed.
   *
   * @var array
   */
  protected $_accessible = [
      '*' => true,
      'id' => false,
      'data' => true
  ];
}
