<?php
namespace Store\Model\Entity;

use Cake\ORM\Entity;

/**
 * PointsAddsSend Entity.
 *
 * @property int $id
 * @property int $send_id
 * @property \Store\Model\Entity\Send $send
 * @property \Cake\I18n\Time $user_id
 * @property \Store\Model\Entity\User $user
 * @property \Cake\I18n\Time $send_on
 * @property string $email
 * @property \Cake\I18n\Time $created
 */
class PointsAddsSend extends Entity
{

  /**
   * Fields that can be mass assigned using newEntity() or patchEntity().
   *
   * Note that when '*' is set to true, this allows all unspecified fields to
   * be mass assigned. For security purposes, it is advised to set '*' to false
   * (or remove it), and explicitly make individual fields accessible as needed.
   *
   * @var array
   */
  protected $_accessible = [
      '*' => true,
      'id' => false,
  ];
}
