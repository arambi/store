<?php
namespace Store\Model\Entity;

use Cake\ORM\Entity;
use Manager\Model\Entity\CrudEntityTrait;
use I18n\Model\Entity\DatesEntityTrait;

/**
 * OrderHistory Entity.
 */
class OrderHistory extends Entity
{
  use CrudEntityTrait;
  use DatesEntityTrait;
  
  /**
   * Fields that can be mass assigned using newEntity() or patchEntity().
   *
   * @var array
   */
  protected $_accessible = [
    'user_id' => true,
    'order_id' => true,
    'status' => true,
    'user' => true
  ];

  protected $_virtual = [
    'status_human'
  ];

  protected function _getStatusHuman()
  {
    $table = $this->getTable();

    if( isset( $table->Orders->statuses [$this->status]))
    {
      return $table->Orders->statuses [$this->status];
    }
    
    return null;
  }
}
