<?php
namespace Store\Model\Entity;

use Cake\ORM\TableRegistry;
use Manager\Model\Entity\CrudEntityTrait;
use Cake\ORM\Entity;
use Cake\Routing\Route\Route;
use Cake\Routing\Router;

/**
 * OrderUserSession Entity
 *
 * @property int $id
 * @property string|null $session_id
 * @property string|null $ip
 * @property string|null $browser
 * @property string|null $platform
 * @property int|null $user_id
 * @property \Cake\I18n\Time $created
 */
class OrderUserSession extends Entity
{
    use CrudEntityTrait;
    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        '*' => true,
    ];

    protected $_virtual = [
        'user',
        'order_last_status',
        'ip_map',
        'has_errors',
    ];

    protected function _getOrderLastStatus()
    {
        $orders = [];

        foreach ($this->order_user_actions as $action) {
            if (!empty($action->response['order_id'])) {
                $orders[$action->response['order_id']] = $action->action;
            }
        }

        $return = [];

        foreach( $orders as $order_id => $status) {
            $return [] = '<a target="_blank" href="/admin/#admin/store/orders/view/'. $order_id .'">'. $order_id .'</a>: '. $status;
        }

        return implode('<br>', $return);
    }


    protected function _getIpMap( $value)
    {
        return '<a target="_blank" href="http://geoiplookup.net/ip/'. $this->ip . '">' .$this->ip.'</a>';
    }

    protected function _getUser($value)
    {
        if ($value !== null) {
            return $value;
        }

        if (empty($this->user_id)) {
            return null;
        }

        $user = TableRegistry::get('User.Users')->find()
            ->where([
                'Users.id' => $this->user_id
            ])
            ->first();

        $user->set( 'access_link', Router::url([
            'plugin' => 'User',
            'controller' => 'Users',
            'action' => 'ses',
            $user->salt
        ], true));
        
        $this->set('user', $user);
        return $user;
    }

    protected function _getHasErrors()
    {
        $has = 'No';

        foreach ($this->order_user_actions as $action) {
            if (!empty($action->errors)) {
                $has = 'Sí';
                break;
            }
        }

        $class = $has == 'Sí' ? 'badge-danger' : 'badge-primary';
        return '<span class="badge ng-binding ng-scope '. $class .'">' . $has . '</span>';
    }

}
