<?php

namespace Store\Model\Entity;

use Manager\Model\Entity\CrudEntityTrait;
use Cake\ORM\Entity;
use Cake\ORM\TableRegistry;
use Store\Config\StoreConfig;

/**
 * Refund Entity
 *
 * @property int $id
 * @property int|null $order_id
 * @property string|null $status
 * @property \Cake\I18n\Time $created
 * @property \Cake\I18n\Time $modified
 */
class Refund extends Entity
{
    use CrudEntityTrait;
    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        '*' => true,
    ];

    protected $_virtual = [
        'total',
        'status_human',
    ];

    protected function _getOrder($value)
    {
        if ($value !== null) {
            return $value;
        }

        if (!empty($this->order_id)) {
            $order = TableRegistry::getTableLocator()->get('Store.Orders')
                ->find()
                ->where([
                    'Orders.id' => $this->order_id,
                ])
                ->first();

            $this->set('order', $order);
            return $order;
        }
    }

    protected function _getRefundsItems($value)
    {
        if ($value !== null) {
            return $value;
        }

        $items = TableRegistry::getTableLocator()->get('Store.RefundsItems')
            ->find()
            ->where([
                'RefundsItems.refund_id' => $this->id,
            ])
            ->contain([
                'LineItems'
            ])
            ->toArray();

        $this->set('refunds_items', $items);
        return $items;
    }

    public function getValidItems()
    {
        return array_filter($this->refunds_items, function ($item) {
            return !$item->refuse;
        });
    }

    public function getTotal()
    {
        $total = 0;

        foreach ($this->getValidItems() as $item) {
            $total += $item->line_item->quantity * $item->line_item->price;
        }

        return $total;
    }

    protected function _getTotal()
    {
        return $this->getTotal();
    }

    protected function _getStatusHuman()
    {
        if (!empty($this->status)) {
            return StoreConfig::refundStatusTitle($this->status);
        }
    }
}
