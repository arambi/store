<?php
namespace Store\Model\Entity;

use Cake\ORM\Entity;
use Manager\Model\Entity\CrudEntityTrait;

/**
 * Address Entity.
 */
class Address extends Entity
{ 
  use CrudEntityTrait;  

  /**
   * Fields that can be mass assigned using newEntity() or patchEntity().
   *
   * @var array
   */
  protected $_accessible = [
    '*' => true
  ];

/**
 * Devuelve el id de la zona para la dirección
 * 
 * @return integer | null
 */
  protected function _getZoneId()
  {
    if( !empty( $this->state->zone_id))
    {
      return $this->state->zone_id;
    }

    if( !empty( $this->country->zone_id))
    {
      return $this->country->zone_id;
    }

    return null;
  }

  protected function _getPretty()
  {
    return $this->address1 . ' - ' . $this->postcode . ' ' . $this->city . ' - ' . $this->state->title;
  }

  protected function _getCountryName()
  {
    return $this->country->title;
  }

  protected function _getStateName()
  {
    return $this->state->title;
  }
}
