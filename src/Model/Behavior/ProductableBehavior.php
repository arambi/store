<?php

namespace Store\Model\Behavior;

use ArrayObject;
use Cake\ORM\Query;
use Cake\ORM\Table;
use Cake\Event\Event;
use Cake\ORM\Behavior;
use Cake\Event\EventManager;
use Store\Config\StoreConfig;
use Cake\Datasource\EntityInterface;
use Cake\ORM\TableRegistry;

/**
 * Productable behavior
 */
class ProductableBehavior extends Behavior
{
    protected $_defaultConfig = [
        'implementedFinders' => [
            'products' => 'findProducts',
            'attributes' => 'findAttributes',
            'offers' => 'findOffers',
            'forInstagram' => 'findForInstagram',
        ],
    ];

    public function initialize(array $config)
    {
        $this->_table->crud->addJsFiles([
            '/store/js/directives.js',
        ]);

        $this->_table->belongsTo('Taxes', [
            'className' => 'Store.Taxes',
            'foreignKey' => 'store_tax_id',
        ]);


        $this->_table->crud->associations(['Taxes']);

        if (StoreConfig::getConfig('withAttributes')) {
            $this->setAttributesAssociations();

            $this->_table->crud->addAssociations(['AttributeGroups', 'ProductAttributes', 'Attributes']);

            $this->_table->crud->addFields([
                'product_attributes' => [
                    'label' => __d('admin', 'Combinaciones'),
                    'type' => 'hasMany'
                ],
                'attribute_groups' => [
                    'type' => 'checkboxes',
                    'label' => __d('admin', 'Tienen que tener al menos una de las siguiente opciones'),
                    'help' => __d('admin', 'Marca esta opción si el producto no se puede vender solo, sino con una combinación de opciones')
                ],
            ]);
        }

        $this->_table->crud->addFields([
            'store_quantity' => [
                'label' => __d('admin', 'Stock disponible'),
                'help' => __d('admin', 'Número de artículos disponibles'),
                'show' => '!content.without_stock'
            ],
            'store_price' => [
                'label' => __d('admin', 'Precio sin I.V.A.'),
                'template' => 'Store.fields.price',
                'help' => __d('admin', 'Introduce el precio con o sin impuestos. El precio es calculado automáticamente. Para introducir decimales, utiliza el punto')
            ],
            'store_weight' => [
                'label' => __d('admin', 'Peso (grs)'),
                'help' => __d('admin', 'Introduce el peso en gramos del producto')
            ],
            'store_reference' => [
                'label' => __d('admin', 'Código de referencia'),
                'help' => __d('admin', 'Referencia interna para este producto')
            ],
            'store_ean13' => [
                'label' => __d('admin', 'EAN-13 o código de barra JAN'),
                'help' => __d('admin', 'Este tipo de código de producto es específico de Europa y Japón, pero es ampliamente utilizado a nivel internacional. Es más utilizado que el código UPC: todos los productos con un EAN serán aceptados en Norteamérica.')
            ],
            'store_upc' => [
                'label' => __d('admin', 'Código de barra UPC'),
                'help' => __d('admin', 'Este tipo de código de producto es ampliamente usado en Estados Unidos, Canadá, Reino Unido, Australia, Nueva Zelanda y en otros países.')
            ],
            'tax' => [
                'label' => __d('admin', 'Tipo de impuestos'),
                'empty' => false
            ],
            'without_stock' => [
                'type' => 'boolean',
                'label' => __d('admin', 'Stock permanente'),
                'help' => __d('admin', 'Marca esta opción si el stock es permanente')
            ],
        ]);
        

        if (StoreConfig::getConfig('showPricesInEdition')) {
            $elements = [
                'without_stock',
                'store_quantity',
                'tax',
                'store_price',
                'store_weight',
            ];
    
            if (StoreConfig::getConfig('withAttributes')) {
                $elements[] = 'attribute_groups';
            }
    
            $this->_table->crud->addBoxToColumn(['create', 'update'], 'general', [
                'title' => __d('admin', 'Opciones tienda'),
                'elements' => $elements
            ]);
    
            // $this->_table->crud->addFieldToIndex( 'index', 'published');
    
            if (StoreConfig::getConfig('withAttributes')) {
                $this->_table->crud->addColumn(['create', 'update'], [
                    'key' => 'combinations',
                    'title' => __d('admin', 'Combinaciones'),
                    'cols' => 12,
                    'box' => [
                        [
                            'elements' => [
                                'product_attributes'
                            ]
                        ]
                    ]
                ]);
            }
        }

    }


    public function beforeSave(Event $event, EntityInterface $entity, ArrayObject $options)
    {
        if (!empty($entity->store_price_offer)) {
            $entity->set('store_price_order', $entity->store_price_offer);
        } else {
            $entity->set('store_price_order', $entity->store_price);
        }

        $this->setPricePVP($entity);
    }


    public function afterSave(Event $event, EntityInterface $entity)
    {
        $this->updateStock($entity->id);
        $this->updatePriceOfferGlobal($entity->id);
        \Cake\Log\Log::debug( 'Guardando '. $entity->id);
        // Event
        $event = new Event('Store.Model.Product.afterSave', $this, [
            'entity' => $entity,
        ]);

        EventManager::instance()->dispatch($event);
    }

    public function updatePriceOfferGlobal($id)
    {
        $query = $this->_table->find()
            ->where([
                $this->_table->getAlias() . '.' . $this->_table->getPrimaryKey() => $id
            ]);

        if (StoreConfig::getConfig('withAttributes')) {
            $query->contain([
                'ProductAttributes'
            ]);
        }

        $product = $query->first();

        $price = 0;

        if (!empty($product->store_price_offer)) {
            $price = $product->store_price_offer;
        }

        if (!empty($product->product_attributes)) {
            foreach ($product->product_attributes as $pa) {
                if (!empty($pa->store_price_offer) && $pa->store_price_offer < $price) {
                    $price = $pa->store_price_offer;
                }
            }
        }

        $this->_table->query()->update()
            ->set([
                'store_offer_price_global' => $price
            ])
            ->where([
                $this->_table->getPrimaryKey() => $id
            ])
            ->execute();
    }


    public function updateStock($id)
    {
        $query = $this->_table->find()
            ->where([
                $this->_table->getAlias() . '.' . $this->_table->getPrimaryKey() => $id
            ]);

        if (StoreConfig::getConfig('withAttributes')) {
            $query->contain([
                'ProductAttributes'
            ]);
        }

        $product = $query->first();

        $stock = 0;

        if (StoreConfig::getConfig('withAttributes') && !empty($product->product_attributes)) {
            foreach ($product->product_attributes as $pa) {
                $stock = $stock + $pa->quantity;
            }
        } else {
            $stock = $product->store_quantity;
        }


        $this->_table->query()->update()
            ->set([
                'quantity_total' => $stock
            ])
            ->where([
                $this->_table->getPrimaryKey() => $id
            ])
            ->execute();
    }

    public function setPricePVP($entity)
    {
        if (!empty($entity->store_tax_id)) {
            $tax = TableRegistry::getTableLocator()->get('Store.Taxes')->find()
                ->where([
                    'Taxes.id' => $entity->store_tax_id
                ])
                ->first();

            if ($tax) {
                $price = $entity->store_price_order;
                $price = $price + ($price * $tax->value) / 100;
                $entity->set('store_price_pvp', round($price, 2));
            }
        }
    }



    public function afterDelete(Event $event, EntityInterface $entity, ArrayObject $options)
    {
        // Event
        $event = new Event('Store.Model.Product.afterDelete', $this, [
            'entity' => $entity,
        ]);

        EventManager::instance()->dispatch($event);
    }

    public function findProducts(Query $query)
    {
        return $query
            ->contain([
                'Taxes'
            ])
            ->formatResults(function ($results) {
                return $this->_rowMapper($results);
            }, $query::PREPEND);
    }

    public function findAttributes(Query $query)
    {
        $this->_table->ProductAttributes->Attributes->belongsTo('AttributeGroups', [
            'className' => 'Store.AttributeGroups',
            'foreignKey' => 'attribute_group_id'
        ]);

        return $query
            ->contain([
                'Taxes',
                'AttributeGroups',
                'ProductAttributes' => [
                    'Attributes' => [
                        'AttributeGroups'
                    ]
                ]
            ])
            ->formatResults(function ($results) {
                return $results->map(function ($row) {
                    foreach ($row->product_attributes as $pa) {
                        $pa->set('tax', $row->tax);
                    }

                    return $row;
                });
            });
    }

    public function findOffers(Query $query)
    {
        return $query->where([
            $this->_table->getAlias() . '.store_offer_price_global > 0'
        ]);
    }


    public function setAttributesAssociations()
    {
        $this->_table->hasMany('ProductAttributes', [
            'className' => 'Store.ProductAttributes',
            'foreignKey' => 'product_id',
            'conditions' => [
                // 'ProductAttributes.store_price IS NOT NULL'
            ],
            'saveStrategy' => 'replace'
        ]);

        $this->_table->belongsToMany('AttributeGroups', [
            'joinTable' => 'store_product_attribute_groups',
            'through' => 'Store.ProductAttributeGroups',
            'foreignKey' => 'product_id',
            'targetForeignKey' => 'attribute_group_id',
            'className' => 'Store.AttributeGroups',
        ]);
    }

    protected function _rowMapper($results)
    {
        return $results->map(function ($row) {
            $event = new Event('Store.Model.Products.afterFind', $this, [$row]);
            EventManager::instance()->dispatch($event);

            // Precio de oferta
            if (!empty($row->store_price_offer) && (empty($row->offer_price) || $row->store_price_offer < $row->offer_price)) {
                $row->set('offer_price', $row->store_price_offer);
            }
            return $row;
        });
    }

    public function findForInstagram(Query $query, array $options)
    {
        $query->where([
            $this->_table->getAlias() .'.to_instagram' => true, 
        ]);

        if (isset($options['conditions'])) {
            $query->where($options['conditions']);
        }

        return $query;
    }

    public function getInstagramMap()
    {
        return [
            'id' => 'ig_id',
            'title' => 'ig_title',
            'description' => 'ig_description',
            'availability' => 'ig_availability',
            'condition' => 'ig_condition',
            'price' => 'ig_price',
            'link' => 'ig_link',
            'image_link' => 'ig_image_link',
            'brand' => 'ig_brand',
            'google_product_category' => 'ig_google_product_category',

        ];
    }

    public function getInstagramData($entity, $map)
    {
        $data = [];

        foreach ($map as $ig_key => $entity_key) {
            $data[] = $entity->get($entity_key);
        }

        return $data;
    }
}
