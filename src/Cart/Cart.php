<?php

namespace Store\Cart;

use Cake\Event\Event;
use Cake\Http\Session;
use Cake\ORM\TableRegistry;
use Store\Model\Entity\Order;
use Cofree\Lib\RemoteLocation;
use Cake\Controller\Component\AuthComponent;
use Cake\Event\EventManager;

class Cart
{
    /**
     * El order actual
     *
     * @var Store\Model\Entity\Order
     */
    protected static $_order;

    /**
     * La sessión
     *
     * @var Cake\Network\Session
     */
    protected static $_session;

    protected static $_authComponent;

    protected static $_stateDeliveryId = false;

    private static $__orderSessionKey = 'Store.orderSalt';

    /**
     * El country_id del usuario en cuestión legal
     * Generalmente viene por la dirección de facturación de los pedidos
     * Si no, la tomará de RemoteLocation
     *
     * @var integer
     */
    protected static $_invoiceCountryId;

    /**
     * Seta el order actual
     *
     * @param Store\Model\Entity\Order $order
     * @return void
     */
    public static function setOrder($order)
    {
        self::$_order = $order;

        if (!empty($order->adr_invoice_country_id)) {
            self::setInvoiceCountryId($order->adr_invoice_country_id);
        }

        if (!empty($order->adr_delivery_state_id)) {
            self::setStateDeliveryId($order->adr_delivery_state_id);
        }
    }

    /**
     * Toma el order actual
     *
     * @return Store\Model\Entity\Order
     */
    public static function getOrder()
    {
        return self::$_order;
    }

    public static function setSession(Session $session)
    {
        self::$_session = $session;
    }

    public static function getSession()
    {
        return self::$_session;
    }

    public function getOrderSalt()
    {
        if (!static::getSession()) {
            return;
        }

        $session = static::getSession();

        return $session->read(static::$__orderSessionKey);
    }

    public static function resetStateDeliveryId()
    {
        self::$_stateDeliveryId = false;
    }

    public static function setStateDeliveryId($id)
    {
        self::$_stateDeliveryId = $id;
    }

    public static function getStateDeliveryId()
    {
        if (self::$_stateDeliveryId !== false) {
            return self::$_stateDeliveryId;
        }

        if (!$salt = self::getOrderSalt()) {
            self::$_stateDeliveryId = null;
            return;
        }

        $order = TableRegistry::getTableLocator()->get('Store.Orders')->find()
            ->where([
                'Orders.salt' => $salt
            ])
            ->first();

        if ($order) {
            return self::$_stateDeliveryId = $order->adr_delivery_state_id;
        } else {
            self::$_stateDeliveryId = null;
        }
    }

    public static function setAuthComponent(AuthComponent $auth)
    {
        self::$_authComponent = $auth;
    }

    public static function getAuthComponent()
    {
        return self::$_authComponent;
    }


    /**
     * Setea el country_id de facturación
     *
     * @param integer $country_id
     * @return void
     */
    public static function setInvoiceCountryId($country_id)
    {
        if (self::$_session) {
            self::$_session->write('Cart.invoice_country_id', $country_id);
        }
    }


    /**
     * Recoge el country_id de facturación
     *
     * @param integer $country_id
     * @return void
     */
    public static function getInvoiceCountryId()
    {
        if (self::$_session && !self::$_session->check('Cart.invoice_country_id')) {
            $location = new RemoteLocation();

            if (property_exists($location, 'country_code')) {
                $country = TableRegistry::getTableLocator()->get('Store.Countries')->getByCode($location->country_code);
            } else {
                $country = TableRegistry::getTableLocator()->get('Store.Countries')->getDefault();
            }

            if ($country) {
                self::setInvoiceCountryId($country->id);
                return $country->id;
            }
        } elseif (self::$_session) {
            return self::$_session->read('Cart.invoice_country_id');
        }

        return false;
    }


    public static function taxRating($tax)
    {
        $country_id = self::getInvoiceCountryId();

        if (!$country_id) {
            return false;
        }

        $rating = TableRegistry::getTableLocator()->get('Store.TaxRatings')->find()
            ->where([
                'tax_id' => $tax->id,
                'country_id' => $country_id
            ])
            ->first();

        if ($rating) {
            return $rating->value;
        }

        return false;
    }

    public static function hasTaxes($order)
    {
        if ($order->country_delivery && $order->country_delivery->iso_code == 'ES') {
            if ($order->state_delivery && $order->state_delivery->no_taxes) {
                return false;
            }
            
            return true;
        }

        $country_no_taxes = ($order->country_delivery && $order->country_delivery->no_taxes) 
            || ($order->state_delivery && $order->state_delivery->no_taxes);

        $user_no_taxes = $order->user && $order->user->store_no_taxes;

        $hasTaxes = new \stdClass();
        $hasTaxes->has = true;

        $event = new Event('Store.Cart.hasTaxes', $order, [
            $order,
            $hasTaxes,
        ]);

        EventManager::instance()->dispatch($event);

        if (!$hasTaxes->has) {
            return false;
        }

        return !$country_no_taxes && !$user_no_taxes && $hasTaxes->has;

    }

}
