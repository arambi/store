<?php 

namespace Store\Cart;

use Cake\ORM\TableRegistry;
use Cake\ORM\Entity;
use Cake\Event\EventManager;
use Cake\Event\Event;
use Store\Config\StoreConfig;

/**
 * Genera un objeto que contiene las propiedades informativas sobre la añaduría de un item a la cesta de la compra
 */

class ItemDraft
{
/**
 * Listado de errores que suceden cuando se añade el item
 * @var array
 */
  protected $_errors = [];

/**
 * Indica si se ha añadido satisfactoriamente
 * @var boolean
 */
  protected $_success = true;

/**
 * El pedido
 * @var Store\Model\Entity\Order
 */
  protected $_order;

/**
 * Los datos que llegan de la petición
 * @var array
 */
  protected $_data;

/**
 * El producto a añadir
 * @var Store\Model\Entity\Product
 */
  protected $_product;

  protected $_product_attribute_id;

/**
 * La combinación de atributos del producto
 * @var Store\Model\Entity\ProductAttribute
 */
  protected $_attributes;

  protected $_extras;

/**
 * La cantidad a añadir
 * @var integer
 */
  protected $_quantity;

  protected $_existentQuantity = 0;

  protected $_message;

  protected $_defaults = [
    'quantity' => 1
  ];

  public function __construct( $data, $order)
  {
    $data = $data + $this->_defaults;
    $this->order( $order);
    $this->Products = StoreConfig::getProductsModel();
    $this->_data = $data;
    $this->quantity( $data ['quantity']);
    $this->_setProduct();
    $this->__verifyAttributeGroups();
    $this->_setAttributes();
    $this->_setExtras();
    $this->_setProductQuantity();
    $this->_verifyStock();

    // Event
    $event = new Event( 'Store.Cart.ItemDraft.afterConstruct', $this);
    EventManager::instance()->dispatch( $event);

  }

/**
 * Verifica la disponibilidad en stock del pedido
 * @return
 */
  protected function _verifyStock()
  {
    if( !$this->isValid())
    {
      return;
    }
    
    if( !empty( $this->_attributes) && $this->_attributes->published === false)
    {
      $this->addError( __d( 'app', 'No hay artículos disponibles para la combinación solicitada'));
      $this->setInvalid();
      return;
    }
    
    $event = new Event( 'Store.Cart.ItemDraft.verifyStock', $this);
    EventManager::instance()->dispatch( $event);


    if( $this->_product->without_stock)
    {
      return;
    }

    if( !empty( $this->_attributes))
    {
      if( $this->_attributes->quantity <= 0)
      {
        $this->addError( __d( 'app', 'No hay artículos disponibles para la combinación solicitada'));
        $this->setInvalid();
      }
      elseif( ($this->quantity() + $this->_existentQuantity) > $this->_attributes->quantity)
      {
        $this->addError( __d( 'app', 'Actualmente solo disponemos de {0} artículos para la combinación solicitada', [$this->_attributes->quantity]));
        $this->setInvalid();
      }
    }
    elseif( !empty( $this->_product))
    {
      if( $this->_product->store_quantity <= 0)
      {
        $this->addError( __d( 'app', 'No hay artículos disponibles para el producto solicitado'));
        $this->setInvalid();
      }
      elseif( $this->noHasProductQuantity())
      {
        $this->addError( __d( 'app', 'Actualmente solo disponemos de {0} artículos para el producto solicitado', [$this->_product->store_quantity]));
        $this->setInvalid();
      }
    }
  }

  public function existentQuantity()
  {
    return $this->_existentQuantity;
  }

  public function noHasProductQuantity()
  {
    if( !empty( $this->_attributes)) {
      return ($this->quantity() + $this->_existentQuantity) > $this->_attributes->quantity;
    } else {
      return ($this->quantity() + $this->_existentQuantity) > $this->_product->store_quantity;
    }
  }

  protected function _setProduct()
  {
    $query = $this->Products->find()->where([
      'Products.salt' => $this->_data ['salt']
    ]);

    if( StoreConfig::getConfig( 'withAttributes'))
    {
      $query->contain([
        'AttributeGroups',
        'ProductAttributes' => [
          'Attributes'
        ],
      ]);
    }

    $this->Products->setAttributesPrice( $query);
    
    $product = $query->first();

    if( !$product)
    {
      $this->addError( __d( 'app', 'El producto seleccionado no existe'));
      return;
    }

    $this->product( $product);
  }

  protected function _setProductQuantity()
  {
    $product = $this->product();

    if( !$product || !$this->order())
    {
      return;
    }

    foreach( $this->order()->line_items as $item)
    {
      // Se está añadiendo un producto_atributo que ya está en la cesta
      if( !empty( $this->_attributes) && !empty( $item->product_attribute)
          && $this->_attributes->id == $item->product_attribute->id
        )
      {
        $this->_existentQuantity = $item->quantity;
      }
      elseif( empty( $this->_attributes) && $product->id == $item->product->id)
      {
        $this->_existentQuantity = $item->quantity;
      }
    }
  }

  protected function _setAttributes()
  {
    \Cake\Log\Log::debug( 'Data Item Draft');
    \Cake\Log\Log::debug( $this->_data);
    if( !empty( $this->_data ['attributes']))
    {
      $product_attribute_id = $this->getProductAttribute();

      if( !$product_attribute_id)
      {
        if( $this->isValid())
        {
          $this->addError( __d( 'app', 'El producto no existe con las opciones seleccionadas'));
        }
      }
      else
      {
        $this->_product_attribute_id = $product_attribute_id;

        $attributes = $this->Products->ProductAttributes->find()->where([
            'ProductAttributes.id' => $product_attribute_id,
        ])->contain([
            'Attributes'
        ])->first(); 

        $this->attributes( $attributes);
      }
    }
  }


  protected function _setExtras()
  {
    if( !empty( $this->_data ['line_item']))
    {
      $this->_extras = $this->_data ['line_item'];
    }
  }

  private function __verifyAttributeGroups()
  {
    if( empty( $this->_product->attribute_groups))
    {
      return true;
    }


    if( empty( $this->_data ['attributes']))
    {
      $this->addError( __d( 'app', 'Es necesario seleccionar alguna de las opciones disponibles'));
      $this->setInvalid();
      return;
    }

    if( isset( $this->_data ['attributes']))
    {
      $had_group_ids = collection( $this->_data ['attributes'])->extract( 'group_id')->toArray();
    }
    else
    {
      $had_group_ids = [];
    }

    $group_ids = collection( $this->_product->attribute_groups)->extract( 'id')->toArray();
    $diff = array_diff( $group_ids, $had_group_ids);
    
    if( count( $diff) == 0)
    {
      return true;
    }

    $groups = TableRegistry::get( 'Store.AttributeGroups')->find()
      ->where([
        'AttributeGroups.id IN' => $diff
      ])
      ->extract( 'store_title')
      ->toArray();

    foreach( $groups as $group)
    {
      $this->addError( __d( 'app', 'Es necesario al menos seleccionar una opción de {0}', [$group]));
    }

    return false;
  }

/**
 * Comprueba que los atributos existen en el producto
 * Son os atributos que ha seleccionado el usuario al añadir un producto al carrito
 * 
 * @return boolean             [description]
 */
  private function getProductAttribute()
  {
    $attributes = $this->_data ['attributes'];

    foreach( $this->_product->product_attributes as $product_attribute)
    {
      $collection = collection( $product_attribute->attributes);
      $result = 0;

      foreach( $attributes as $attribute)
      {
        $has = $collection->firstMatch([
          'id' => $attribute ['attribute_id'],
          'attribute_group_id' => $attribute ['group_id']
        ]);

        if( $has)
        {
          $result++;
        }
      }

      if( $result == count( $product_attribute->attributes))
      {
        return $product_attribute->id;
      }
    }

    return false;
  }

  public function product( $product = false)
  {
    if( $product)
    {
      $this->_product = $product;
    }

    return $this->_product;
  }

  public function data()
  {
    return $this->_data;
  }

  public function quantity( $quantity = false)
  {
    if( $quantity)
    {
      $this->_quantity = $quantity;
    }

    return $this->_quantity;
  }

  public function attributes( $attributes = false)
  {
    if( $attributes)
    {
      $this->_attributes = $attributes;
    }

    return $this->_attributes;
  }
  
  public function extras()
  {
    return $this->_extras;  
  }

  public function productAttributesId()
  {
    if( !empty( $this->_attributes))
    {
      return $this->_attributes->id;
    }

    return null;
  }

  public function order( $order = false)
  {
    if( $order)
    {
      $this->_order = $order;
    }

    return $this->_order;
  }

  public function errors()
  {
    return !empty( $this->_errors) ? $this->_errors : false;
  }


  public function addError( $message)
  {
    $this->_errors [] = $message;
    $this->setInvalid();
  }

  public function setValid()
  {
    $this->_success = true;
  }

  public function setInvalid()
  {
    $this->_success = false;
  }

  public function isValid()
  {
    return $this->_success;
  }
}