<?php

namespace Store\Payment;

use Cake\Core\App;
use Cake\Http\ServerRequest;
use Cake\Network\Request;

class Checkout
{
    public static function proccess($order, $request)
    {
        if ($order->total == 0) {
            return new PaymentResponse([
                'redirect' => false,
                'success' => true,
                'status' => 'accepted',
                'finalize' => true
              ]);
        }

        if (empty($order->payment_method)) {
            die('no hay pago');
            return false;
        }

        $className = static::getClassName($order);
        if (!class_exists($className)) {
            die('no hay class');
            return false;
        }

        $payment = new $className($request);
        return $payment->checkout($order, $request);
    }

    public static function getClassName($order)
    {
        return 'Store\Payment\Processor\\' . $order->payment_method->processor . 'Payment';
    }

    public static function update($order, $request)
    {
        if ($order->payment_method) {
            $className = static::getClassName($order);
            $payment = new $className($request);

            if (method_exists($payment, 'update')) {
                $payment->update($order, $request);
            }
        }
    }
}
