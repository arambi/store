<?php 

namespace Store\Payment\Processor;

use Store\Payment\Payment;
use Store\Payment\PaymentInterface;
use Store\Payment\PaymentResponse;

class DraftPayment extends Payment implements PaymentInterface
{
  public static $name = [
    'spa' => 'Giro bancario',
    'eng' => 'Bank draft',
    'eus' => 'Bank draft',
  ];

  public function checkout( \Store\Model\Entity\Order $order, \Cake\Http\ServerRequest $request)
  {
    return new PaymentResponse([
      'redirect' => false,
      'success' => true,
      'status' => 'accepted',
      'finalize' => true
    ]);
  }

  public static function settings()
  {
    return [];
  }
}