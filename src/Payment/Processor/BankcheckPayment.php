<?php 

namespace Store\Payment\Processor;

use Store\Payment\Payment;
use Store\Payment\PaymentInterface;
use Store\Payment\PaymentResponse;

class BankcheckPayment extends Payment implements PaymentInterface
{
  public static $name = [
    'spa' => 'Ingreso en cuenta',
    'eng' => 'Bank check',
    'eus' => 'Bank check',
  ];

  public function checkout( \Store\Model\Entity\Order $order, \Cake\Http\ServerRequest $request)
  {
    return new PaymentResponse([
      'redirect' => false,
      'success' => true,
      'status' => 'pending',
      'finalize' => true
    ]);
  }

  public static function settings()
  {
    return [
      'account_number' => 'Número de cuenta',
    ];
  }
}