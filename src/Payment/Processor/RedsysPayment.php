<?php 

namespace Store\Payment\Processor;

use I18n\Lib\Lang;
use Omnipay\Omnipay;
use Cake\Event\Event;
use Cake\Core\Configure;
use Cake\Routing\Router;
use Store\Payment\Payment;
use Cake\ORM\TableRegistry;
use Section\Routing\RouteData;
use Store\Payment\PaymentResponse;
use Store\Payment\PaymentInterface;

class RedsysPayment extends Payment implements PaymentInterface
{
  public static $name = [
    'spa' => 'Redsys',
    'eng' => 'Redsys',
    'eus' => 'Redsys',
  ];

  public function checkout( \Store\Model\Entity\Order $order, \Cake\Http\ServerRequest $request)
  {
    \Cake\Log\Log::debug( 'RedysPayment: '. $order->id .' | Total: '. $order->total, ['scope' => ['payments']]);

    $Payments = TableRegistry::getTableLocator()->get( 'Payment.Payments');

    $entity = $Payments->newEntity([
      'order_id' => $order->id,
      'callback' => 'store',
      'amount' => $order->total,
      'model' => 'Store.Orders',
      'provider' => 'redsys',
      'request' => [
        'success_url' => RouteData::url([
          'plugin' => 'Store',
          'controller' => 'Orders',
          'action' => 'success'
        ], true) . '?cart=' . $order->salt,
        'error_url' => RouteData::url([
          'plugin' => 'Store',
          'controller' => 'Orders',
          'action' => 'error'
        ], true) . '?cart=' . $order->salt,
      ]
    ]);

    $event = new Event( 'Store.Payment.beforeSave', $this, [
      'payment' => $entity,
      'order' => $order,
    ]);

    $this->getEventManager()->dispatch( $event);
    $payment = $Payments->save( $entity);

    $Payments->query()->update()
        ->set([
            'transaction_id' => $entity->id
        ])
        ->where([
            'id' => $entity->id
        ])
        ->execute();

    return new PaymentResponse([
      'redirect' => '/'. Lang::current('iso2') . Router::url([
        'plugin' => 'Payment', 
        'controller' => 'Redsys', 
        'action' => 'index',
        'salt' => $payment->salt
      ]),
      'success' => true,
      'finalize' => false
    ]);
  }

  public static function settings()
  {
    
  }
}