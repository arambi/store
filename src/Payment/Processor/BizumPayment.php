<?php 

namespace Store\Payment\Processor;

use I18n\Lib\Lang;
use Omnipay\Omnipay;
use Cake\Event\Event;
use Cake\Core\Configure;
use Cake\Routing\Router;
use Store\Payment\Payment;
use Cake\ORM\TableRegistry;
use Section\Routing\RouteData;
use Store\Payment\PaymentResponse;
use Store\Payment\PaymentInterface;

class BizumPayment extends Payment implements PaymentInterface
{
  public static $name = [
    'spa' => 'Bizum',
    'eng' => 'Bizum',
    'eus' => 'Bizum',
  ];

  public function checkout( \Store\Model\Entity\Order $order, \Cake\Http\ServerRequest $request)
  {
    \Cake\Log\Log::debug( 'BizumPayment: '. $order->id .' | Total: '. $order->total, ['scope' => ['payments']]);

    $Payments = TableRegistry::getTableLocator()->get('Payment.Payments');

    $entity = $Payments->newEntity([
      'order_id' => $order->id,
      'callback' => 'store',
      'amount' => $order->total,
      'model' => 'Store.Orders',
      'provider' => 'bizum',
      'request' => [
        'success_url' => RouteData::url([
          'plugin' => 'Store',
          'controller' => 'Orders',
          'action' => 'success'
        ], true) . '?cart=' . $order->salt,
        'error_url' => RouteData::url([
          'plugin' => 'Store',
          'controller' => 'Orders',
          'action' => 'error'
        ], true) . '?cart=' . $order->salt,
      ]
    ]);

    $event = new Event( 'Store.Payment.beforeSave', $this, [
      'payment' => $entity,
      'order' => $order,
    ]);

    $this->getEventManager()->dispatch( $event);

    $payment = $Payments->save( $entity);


    return new PaymentResponse([
      'redirect' => '/'. Lang::current('iso2') . Router::url([
        'plugin' => 'Payment', 
        'controller' => 'Bizum', 
        'action' => 'index',
        'salt' => $payment->salt
      ]),
      'success' => true,
      'finalize' => false
    ]);
  }

  public static function settings()
  {
    
  }
}