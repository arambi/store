<?php

namespace Store\Payment\Processor;

use ArrayObject;
use Cake\Log\Log;
use I18n\Lib\Lang;
use Omnipay\Omnipay;
use Cake\Event\Event;
use Cake\Core\Configure;
use Cake\Routing\Router;
use Payment\Driver\Redsys;
use Store\Payment\Payment;
use Cake\ORM\TableRegistry;
use Cake\Event\EventManager;
use Section\Routing\RouteData;
use Store\Payment\PaymentResponse;
use Cake\Http\Client as HttpClient;
use Store\Payment\PaymentInterface;

class RedsysRestPayment extends Payment implements PaymentInterface
{
    public static $name = [
        'spa' => 'RedsysRest',
        'eng' => 'RedsysRest',
        'eus' => 'RedsysRest',
    ];

    public function checkout(\Store\Model\Entity\Order $order, \Cake\Http\ServerRequest $request)
    {
        $Payments = TableRegistry::getTableLocator()->get('Payment.Payments');
        $data_payment = [
            'order_id' => $order->id,
            'callback' => 'store',
            'amount' => $order->total,
            'card_salt' => $order->card_salt,
            'model' => 'Store.Orders',
            'provider' => 'redsysrest',
            'request' => [
                'success_url' => RouteData::url([
                    'plugin' => 'Store',
                    'controller' => 'Orders',
                    'action' => 'success'
                ], true) . '?cart=' . $order->salt,
                'error_url' => RouteData::url([
                    'plugin' => 'Store',
                    'controller' => 'Orders',
                    'action' => 'error'
                ], true) . '?cart=' . $order->salt,
            ]
        ];

        if ($order->save_card) {
            $data_payment['save_card'] = true;
        }

        $entity = $Payments->newEntity($data_payment);

        $event = new Event('Store.Payment.beforeSave', $this, [
            'payment' => $entity,
            'order' => $order,
        ]);

        $this->getEventManager()->dispatch($event);
        $payment = $Payments->save($entity);

        $account = $this->getAccount($payment);
        $obOrder = new \stdClass();
        $obOrder->order_id = $order->payment_order_id;
        $event = new Event('Payment.Controller.RedsysRest.getOrderId', $this, [
            $obOrder,
            $payment
        ]);

        EventManager::instance()->dispatch($event);

        $inicia_peticion = $this->iniciaPeticion($order, $payment, $account, $obOrder);
        
        if (isset($inicia_peticion['errorCode'])) {
            $this->markError($order, $inicia_peticion['errorCode']);
            return new PaymentResponse([
                'redirectToError' => false,
                'success' => false,
                'finalize' => false,
            ]);
        }

        $version = $this->getVersion($inicia_peticion);
        $data = [];

        $emv = $inicia_peticion['Ds_EMV3DS'];
        $emv['threeDSInfo'] = 'AuthenticationData';

        if ($version == '1') {
            $emv['protocolVersion'] = '1.0.2';
        } else if ($version == '2') {
            $emv['notificationURL'] = Router::url([
                'plugin' => 'Payment',
                'controller' => 'RedsysRest',
                'action' => 'validate',
                'salt' => $payment->salt,
            ], true);

            $emv['threeDSCompInd'] = 'Y';
            $emv['browserJavaEnabled'] = $order->metadata['browserJavaEnabled'];
            $emv['browserJavascriptEnabled'] = 'true';
            $emv['browserLanguage'] = $order->metadata['browserLanguage'];
            $emv['browserColorDepth'] = $order->metadata['browserColorDepth'];
            $emv['browserScreenHeight'] = $order->metadata['browserScreenHeight'];
            $emv['browserScreenWidth'] = $order->metadata['browserScreenWidth'];
            $emv['browserTZ'] = $order->metadata['browserTZ'];
        }

        $data ['DS_MERCHANT_EMV3DS'] = $emv;

        $response = $this->doRequest($this->getTrataUrl(), $payment, $obOrder, $order, $data, 'Trata Petición 1');
        
        if (isset($response['Ds_Response']) && $response['Ds_Response'] == '0000') {
            return new PaymentResponse([
                'status' => 'accepted',
                'success' => true,
                'finalize' => true,
            ]);
        }

        if (isset($response['errorCode'])) {
            $this->markError($order, $response['errorCode']);
            return new PaymentResponse([
                'redirectToError' => false,
                'success' => false,
                'finalize' => false
            ]);
        }

        $metadata = $order->metadata;
        $metadata['protocolVersion'] = $response['Ds_EMV3DS']['protocolVersion'];

        if ($version == '1') {

        } else {
            $metadata['creq'] = $response['Ds_EMV3DS']['creq'];
        }

        TableRegistry::getTableLocator()->get('Store.Orders')->query()->update()
            ->set([
                'metadata' => $metadata
            ])
            ->where([
                'id' => $order->id
            ])
            ->execute();

        return new PaymentResponse([
            'redirect' => '/' . Lang::current('iso2') . Router::url([
                'plugin' => 'Payment',
                'controller' => 'RedsysRest',
                'action' => 'form',
                'salt' => $payment->salt,
                'emv' => $response['Ds_EMV3DS'],
            ]),
            'success' => false,
            'finalize' => false
        ]);
    }

   


    private function doRequest($url, $payment, $obOrder, $order, &$data, $name)
    {
        $account = $this->getAccount($payment);
        
        $defaults = [
            'DS_MERCHANT_AMOUNT' => (string)(int)round($payment->amount * 100),
            'DS_MERCHANT_ORDER' => $obOrder->order_id,
            'DS_MERCHANT_MERCHANTCODE' => $account['MerchantCode'],
            'DS_MERCHANT_CURRENCY' => $account['Merchant_Currency'],
            'DS_MERCHANT_TRANSACTIONTYPE' => '0',
            'DS_MERCHANT_TERMINAL' => $account['Merchant_Terminal'],
        ];

        $data ['DS_MERCHANT_IDENTIFIER'] = $this->getIdentifier($obOrder);
        $this->setIdOper($data, $obOrder, $order);
        $data = array_merge($defaults, $data);

        $browserHeaders = apache_request_headers();
        $browserAcceptHeader = $browserHeaders['Accept'];
        $browserUserAgent = $browserHeaders['User-Agent'];

        $data['DS_MERCHANT_EMV3DS']['browserAcceptHeader'] = $browserAcceptHeader;
        $data['DS_MERCHANT_EMV3DS']['browserUserAgent'] = $browserUserAgent;

        \Cake\Log\Log::debug( "---- $name REQUEST ----", ['scope' => ['payments']]);
        \Cake\Log\Log::debug( $data, ['scope' => ['payments']]);
        $Redsys = new Redsys();

        foreach ($data as $key => $value) {
            $Redsys->setParameter($key, $value);
        }

        $params = $Redsys->createMerchantParameters();
        $signature = $Redsys->createMerchantSignature($this->clave($payment));
        $version = "HMAC_SHA256_V1";

        $_data = [
            'Ds_SignatureVersion' => $version,
            'Ds_MerchantParameters' => $params,
            'Ds_Signature' => $signature
        ];

        \Cake\Log\Log::debug( "---- $name REQUEST BASE64 ----", ['scope' => ['payments']]);
        \Cake\Log\Log::debug( $_data, ['scope' => ['payments']]);

        $http = new HttpClient();
        $response = $http->post($url, $_data);
        $code_response = json_decode($response->getBody()->getContents(), true);

        if (!isset($code_response['Ds_MerchantParameters'])) {
            return $code_response;
        }

        $data_response = $Redsys->decodeMerchantParameters($code_response['Ds_MerchantParameters']);
        $return = json_decode($data_response, true);
        \Cake\Log\Log::debug( "---- $name RESPONSE BASE64 ----", ['scope' => ['payments']]);
        \Cake\Log\Log::debug($code_response, ['scope' => ['payments']]);
        \Cake\Log\Log::debug( "---- $name RESPONSE ----", ['scope' => ['payments']]);
        \Cake\Log\Log::debug( $return, ['scope' => ['payments']]);
        return $return;
    }

    private function getVersion($response)
    {
        if (@$response['Ds_EMV3DS']['protocolVersion'] == 'NO_3DS_v2') {
            return '1';
        } else if(substr(@$response['Ds_EMV3DS']['protocolVersion'], 0, 1) == '2') {
            return '2';
        }
    }

    private function getTrataUrl()
    {
        if (Configure::read('Payment.env') == 'development') {
            return 'https://sis-t.redsys.es:25443/sis/rest/trataPeticionREST';
        } else {
            return 'https://sis.redsys.es/sis/rest/trataPeticionREST';
        }
    }

    private function getIniciaUrl()
    {
        if (Configure::read('Payment.env') == 'development') {
            return 'https://sis-t.redsys.es:25443/sis/rest/iniciaPeticionREST';
        } else {
            return 'https://sis.redsys.es/sis/rest/iniciaPeticionREST';
        }
    }

    private function iniciaPeticion($order, $payment, $account, $obOrder)
    {
        $data = [];
        $data ['DS_MERCHANT_EMV3DS'] = [
            'threeDSInfo' => 'CardData'
        ];
        
        $response = $this->doRequest($this->getIniciaUrl(), $payment, $obOrder, $order, $data, 'Inicia Petición');

        return $response;
    }

    private function setIdOper(&$data, $obOrder, $order)
    {
        $identifier = $this->getIdentifier($obOrder);
        if ($order->card_salt && $identifier == 'REQUIRED') {
            $data ['DS_MERCHANT_IDOPER'] = $order->card_salt;
        }
    }

    private function getIdentifier($obOrder)
    {
        if (!empty($obOrder->card_id)) {
            $card = TableRegistry::getTableLocator()->get('Payment.RedsysCards')->findById($obOrder->card_id)->first();

            if ($card) {
                return $card->card_id;
            }
        }

        return 'REQUIRED';
    }

    public static function settings()
    {
    }


    public function clave($payment)
    {
        $account = $this->getAccount($payment);
        return $account[Configure::read('Payment.env')]['clave'];
    }

    private function getAccount($payment)
    {
        if (empty($payment->provider_account)) {
            return Configure::read('Payment.sermepa.data');
        }

        return Configure::read('Payment.sermepa.' . $payment->provider_account);
    }

    private function markError($order, $error)
    {
        $message = $this->errorMessages($error);
        
        if ($message) {
            TableRegistry::getTableLocator()->get('Store.Orders')->query()->update()
                ->set([
                    'payment_error_code' => $message,
                ])
                ->where([
                    'id' => $order->id
                ])
                ->execute();
        }
    }


    private function errorMessages($code)
    {
        $messages = [
            'SIS0221' => __d( 'app', 'Es necesario indicar el código de seguridad (CVV2)'),
            'SIS0334' => __d( 'app', 'Ocurrió un problema con tu tarjeta'),
            'SIS0571' => __d( 'app', 'Operacion de autenticacion rechazada'),
            'SIS0502' => __d( 'app', 'Ocurrió un problema con tu tarjeta'),
        ];

        if (isset($messages[$code])) {
            return $messages[$code];
        } else {
            return  __d( 'app', 'Ocurrió un problema con tu tarjeta');
        }
    }
}
