<?php 

namespace Store\Payment\Processor;

use I18n\Lib\Lang;
use Omnipay\Omnipay;
use PayPal\Api\Item;
use PayPal\Api\Payer;
use PayPal\Api\Amount; 
use PayPal\Api\Details;
use Cake\Core\Configure;

use Cake\Routing\Router;
use PayPal\Api\ItemList;
use Store\Payment\Payment;
use Cake\ORM\TableRegistry;
use PayPal\Api\Transaction;
use PayPal\Rest\ApiContext;
use PayPal\Api\RedirectUrls; 
use Section\Routing\RouteData;
use Payment\Driver\PaypalTrait;

use Store\Payment\PaymentResponse;
use Store\Payment\PaymentInterface;
use PayPal\Api\Payment as PPayment; 
use PayPal\Auth\OAuthTokenCredential;

class PaypalPayment extends Payment implements PaymentInterface
{
  use PaypalTrait;

  public static $name = [
    'spa' => 'Paypal',
    'eng' => 'Paypal',
    'eus' => 'Paypal'
  ];

  public function checkout( \Store\Model\Entity\Order $order, \Cake\Http\ServerRequest $request)
  {
    // $payment = $this->getPayment();
    $payer = new Payer(); 
    $payer->setPaymentMethod("paypal");
    $itemList = new ItemList(); 

    $currency = $order->currency == 'usd' ? 'USD' : 'EUR';

    foreach( $order->line_items as $line_item)
    {
      $item = new Item(); 
      $item->setName( $line_item->full_title) 
        ->setCurrency($currency) 
        ->setQuantity( $line_item->quantity) 
        ->setPrice( number_format($line_item->price, 2, '.', ''))
      ;

      $itemList->addItem( $item);
    }

    $details = new Details(); 
    $details
      ->setShipping( number_format($order->shipping_handling + $order->shipping_handling_tax, 2, '.', ''))
      ->setTax( number_format($order->taxes, 2, '.', '')) 
      ->setSubtotal( number_format($order->subtotal, 2, '.', ''))
    ;

    if( !empty( $order->coupon_discount) || !empty( $order->promo_discount))
    {
      $details->setShippingDiscount( number_format( $order->coupon_discount + $order->promo_discount, 2, '.', ''));
    }

    $amount = new Amount(); 
    $amount->setCurrency($currency) 
      ->setTotal( number_format($order->total, 2, '.', '')) 
      ->setDetails( $details)
    ;

    $transaction_id = uniqid();

    $transaction = new Transaction(); 
    $transaction
      ->setAmount($amount) 
      ->setItemList($itemList) 
      ->setDescription( __d( 'app', 'Pago de {0}', [$order->adr_delivery_firstname])) 
      ->setInvoiceNumber( $transaction_id);
    
    $redirectUrls = new RedirectUrls();
    
    $url = '/' . Lang::current('iso2') . Router::url([
      'plugin' => 'Payment',
      'controller' => 'Paypal',
      'action' => 'proccess'
    ]);

    $redirectUrls
        ->setReturnUrl( Router::url( $url, true))
        ->setCancelUrl(Router::url([
          'plugin' => 'Store',
          'controller' => 'Orders',
          'action' => 'error'
        ], true));
  
    $payment = new PPayment();
    $payment->setIntent("sale")
        ->setPayer($payer)
        ->setRedirectUrls($redirectUrls)
        ->setTransactions(array($transaction));

    $payment->setId( $transaction_id);

    try {
        $payment->create( $this->apiContext());
    } catch (\Exception $ex) {
        \Cake\Log\Log::debug( $ex->getData());
    }

    return $this->createPayment( $order, $payment);
  }


  public function createPayment( $order, $payment)
  {
    $Payments = TableRegistry::get( 'Payment.Payments');
    \Cake\Log\Log::debug( 'Payment Paypal createPayment: '. $order->id .' | Total: '. $order->total, ['scope' => ['payments']]);
    $content = $Payments->save( $Payments->newEntity([
      'order_id' => $order->id,
      'callback' => 'store',
      'amount' => $order->total,
      'model' => 'Store.Orders',
      'provider' => 'paypal',
      'transaction_id' => $payment->getId(),
      'sent_at' => date( 'Y-m-d H:i:s'),
      'request' => [
        'success_url' => RouteData::url([
          'plugin' => 'Store',
          'controller' => 'Orders',
          'action' => 'success'
        ], true) . '?cart=' . $order->salt,
        'error_url' => RouteData::url([
          'plugin' => 'Store',
          'controller' => 'Orders',
          'action' => 'error'
        ], true) . '?cart=' . $order->salt,
      ]
    ]));

    return new PaymentResponse([
      'redirect' => $payment->getApprovalLink(),
      'success' => true,
      'finalize' => false
    ]);
  }

  public static function settings()
  {
    return [];
  }

  public function getKeys()
  {
    $env = Configure::read( 'App.payEnvironment') == 'development' ? 'development' : 'production';
    $settings = Configure::read( 'Payment.paypal')['data'][$env];

    if( !is_array( $settings) || empty( $settings ['id']) || empty( $settings ['secret']))
    {
      throw new \RuntimeException(
          'No está configurado Paypal'
      );
    }

    return [
      'id' => $settings ['id'],
      'secret' => $settings ['secret']
    ];
  }
}