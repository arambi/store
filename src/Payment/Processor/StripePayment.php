<?php 

namespace Store\Payment\Processor;

use Stripe\Charge;
use Stripe\Stripe;
use Omnipay\Omnipay;
use Stripe\Customer;
use Cake\Event\Event;
use Cake\Core\Configure;
use Store\Payment\Payment;
use Cake\ORM\TableRegistry;
use Store\Model\Entity\Order;
use Store\Payment\PaymentResponse;
use Store\Payment\PaymentInterface;

class StripePayment extends Payment implements PaymentInterface
{
  public static $name = [
    'spa' => 'Stripe',
    'eng' => 'Stripe',
    'eus' => 'Stripe',
  ];

  public function update( Order $order, $request)
  {
    $data = $request->getData();

    if( empty( $data ['stripeToken']))
    {
      return;
    }

    $StripeOrders = TableRegistry::get( 'Payment.CardStripeOrders');
    
    $guard = [
      'order_id' => $order->id,
      'model' => 'Store.Orders'
    ];

    $entity = $StripeOrders->find()
      ->where( $guard)
      ->first();

    if( !$entity)
    {
      $entity = $StripeOrders->newEntity( $guard);
    }

    $entity->set( 'token', $data ['stripeToken']);

    if( !empty( $data ['stripeSave']))
    {
      $entity->set( 'save', true);
    }

    $StripeOrders->save( $entity);
  }

  public function checkout( Order $order, \Cake\Http\ServerRequest $request)
  {
    $keys = $this->getKeys( $order);
    Stripe::setApiKey( $keys ['secret']);    

    $payment = new PaymentResponse();
    $StripeOrders = TableRegistry::get( 'Payment.CardStripeOrders');

    $stripe_order = $StripeOrders->find()
      ->where([
        'order_id' => $order->id
      ])
      ->first();

    if( !$stripe_order)
    {
      return $this->returnError();
    }

    try {
      $Payments = TableRegistry::get( 'Payment.Payments');

      $intent = \Stripe\PaymentIntent::retrieve( $stripe_order->intent_id);
      \Cake\Log\Log::debug( $intent);

      if( $intent->status == 'succeeded')
      {
        $payment->set([
          'success' => true,
          'status' => 'accepted',
          'finalize' => true,
          'data' => [
            'stripe_id' => $intent->id,
          ]
        ]);  

        $entity = $Payments->newEntity([
          'order_id' => $order->id,
          'callback' => 'store',
          'amount' => $order->total,
          'model' => 'Store.Orders',
          'provider' => 'stripe',
          'transaction_id' => $intent->id,
          'success' => true
        ]);

        \Cake\Log\Log::debug( $stripe_order);

        $event = new Event( 'Store.Payment.beforeSave', $this, [
          'payment' => $entity,
          'order' => $order,
        ]);
    
        $this->eventManager()->dispatch( $event);
    
        $Payments->save( $entity);
        \Cake\Log\Log::debug( $entity);

        if( $stripe_order->save)
        {
          $StripeCards = TableRegistry::get( 'Payment.StripeCards');
          $customer = $this->getStripeCustomer( $order);
          $stripe_payment = $this->createPayment( $customer, $stripe_order);

          $stripe_card = $StripeCards->newEntity([
            'user_id' => $order->user_id,
            'card_id' => $stripe_payment->id,
            'exp_year' => $stripe_payment->card->exp_year,
            'exp_month' => $stripe_payment->card->exp_month,
            'brand' => $stripe_payment->card->brand,
            'country' => $stripe_payment->card->country,
            'last4' => $stripe_payment->card->last4,
            'funding' => $stripe_payment->card->funding,
          ]);

          $StripeCards->save( $stripe_card);
          $this->saveCustomer( $order, $customer->id);
        }
      }
      else
      {
        $payment->set([
          'success' => false,
          'finalize' => true,
        ]);
      }
    } catch (\Throwable $th) {
      throw $th;
    }

    return $payment;
  }

  private function chargeOrder()
  {

  }

  private function typeOfToken( $token)
  {
    if( strpos( $token, 'cus_') !== false)
    {
      return 'customer';
    }

    if( strpos( $token, 'tok_') !== false)
    {
      return 'token';
    }
  }
  

  private function createPayment( $customer, $stripe_order)
  {
    $payment = \Stripe\PaymentMethod::create([
      'type' => 'card',
      'card' => [
        'token' => $stripe_order->token,
      ]
    ]);

    $payment->attach(['customer' => $customer->id]);
    return $payment;
  }

  private function createSource( $customer, $stripe_order)
  {
    \Cake\Log\Log::debug( 'Creando source '. $stripe_order->token);

    try {
      $card = Customer::createSource( $customer->id, [
        'source' => $stripe_order->token
      ]);

      \Cake\Log\Log::debug( $card);
  
      Customer::update( $customer->id, [
        'default_source' => $card->id
      ]);
  
      return $card;
    } catch (\Throwable $th) {
      \Cake\Log\Log::debug( $th);
      //throw $th;
    }

  }

  private function saveCustomer( $order, $customer_id)
  {
    TableRegistry::get( 'User.Users')->query()->update()
      ->where([
        'id' => $order->user_id,
      ])
      ->set([
        'stripe_customer_id' => $customer_id
      ])
      ->execute();
  }


  public function getStripeCustomer( $order)
  {
    if( !empty( $order->user->stripe_customer_id))
    {
      $customer = Customer::retrieve( $order->user->stripe_customer_id);
      return $customer;
    }

    $customer = Customer::create([
      // 'source' => $token,
      'name' => $order->user->name,
      'email' => $order->user->email,
      'phone' => $order->user->phone,
    ]);

    return $customer;
  }

  public function returnError()
  {
    return new PaymentResponse([
      'success' => false,
      'finalize' => false
    ]);
  }

  public static function settings()
  {
    return [
      'test_secret' => 'Test Secret',
      'test_public' => 'Test Public',
      'live_secret' => 'Live Secret',
      'live_public' => 'Live Public'
    ];
  }

  public function getKeys( $order)
  {
    $prefix = Configure::read( 'App.payEnvironment') == 'development' ? 'test_' : 'live_';

    $settings = $order->payment_method->settings;

    if( !is_object( $settings) || empty( $settings->{$prefix . 'public'}) || empty( $settings->{$prefix . 'secret'}))
    {
      throw new \RuntimeException(
          'No está configurado Stripe'
      );
    }

    return [
      'public' => $settings->{$prefix . 'public'},
      'secret' => $settings->{$prefix . 'secret'},
    ];
  }
}