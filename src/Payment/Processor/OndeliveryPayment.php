<?php 

namespace Store\Payment\Processor;

use Store\Payment\Payment;
use Store\Payment\PaymentInterface;
use Store\Payment\PaymentResponse;

class OndeliveryPayment extends Payment implements PaymentInterface
{
  public static $name = [
    'spa' => 'Contrareembolso',
    'eng' => 'On delivery',
    'eus' => 'On delivery'
  ];

  public function checkout( \Store\Model\Entity\Order $order, \Cake\Http\ServerRequest $request)
  {
    return new PaymentResponse([
      'redirect' => false,
      'success' => true,
      'status' => 'pending',
      'finalize' => true
    ]);
  }

  public static function settings()
  {
    return [];
  }
}