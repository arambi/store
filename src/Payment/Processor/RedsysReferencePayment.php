<?php

namespace Store\Payment\Processor;

use I18n\Lib\Lang;
use Omnipay\Omnipay;
use Cake\Event\Event;
use Cake\Core\Configure;
use Cake\Log\Log;
use Cake\Routing\Router;
use Store\Payment\Payment;
use Cake\ORM\TableRegistry;
use Section\Routing\RouteData;
use Store\Payment\PaymentResponse;
use Store\Payment\PaymentInterface;

class RedsysReferencePayment extends Payment implements PaymentInterface
{
    public static $name = [
        'spa' => 'RedsysReference',
        'eng' => 'RedsysReference',
        'eus' => 'RedsysReference',
    ];

    public function checkout(\Store\Model\Entity\Order $order, \Cake\Http\ServerRequest $request)
    {
        $Payments = TableRegistry::getTableLocator()->get('Payment.Payments');

        $data = [
            'order_id' => $order->id,
            'callback' => 'store',
            'amount' => $order->total,
            'model' => 'Store.Orders',
            'provider' => 'redsysreference',
            'request' => [
                'success_url' => RouteData::url([
                    'plugin' => 'Store',
                    'controller' => 'Orders',
                    'action' => 'success'
                ], true) . '?cart=' . $order->salt,
                'error_url' => RouteData::url([
                    'plugin' => 'Store',
                    'controller' => 'Orders',
                    'action' => 'error'
                ], true) . '?cart=' . $order->salt,
            ]
        ];

        if ($order->save_card) {
            $data['save_card'] = true;
        }

        $entity = $Payments->newEntity($data);

        $event = new Event('Store.Payment.beforeSave', $this, [
            'payment' => $entity,
            'order' => $order,
        ]);

        $this->getEventManager()->dispatch($event);
        $payment = $Payments->save($entity);

        $Payments->query()->update()
            ->set([
                'transaction_id' => $entity->id
            ])
            ->where([
                'id' => $entity->id
            ])
            ->execute();

        return new PaymentResponse([
            'redirect' => '/' . Lang::current('iso2') . Router::url([
                'plugin' => 'Payment',
                'controller' => 'RedsysReference',
                'action' => 'index',
                'salt' => $payment->salt
            ]),
            'success' => true,
            'finalize' => false
        ]);
    }

    public static function settings()
    {
    }
}
