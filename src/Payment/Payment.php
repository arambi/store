<?php 

namespace Store\Payment;

use Cake\Network\Request;
use Cake\Event\EventDispatcherTrait;


class Payment
{
  use EventDispatcherTrait;

  public $request;

  public function __construct( Request $request)
  {
    $this->request = $request;
  }  
}