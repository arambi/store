<?php 

namespace Store\Payment;

/**
 * Es la respuesta a un procesamiento de pago, de cualquiera de los métodos que existan:
 * 
 * 1. Método de pago con respuesta inmediate
 * 2. Método de pago con redirección a un sistema externo
 * 3. Método de pago que se realiza fuera de la tienda, p.e. contrareembolso o transferencia bancaria
 */

class PaymentResponse
{
  /**
   * Propiedades del objeto
   * @var array
   */
  protected $_properties = [
    'redirect',
    'success',
    'data',
    'finalize',
    'message_error',
    'order_id'
  ];

/**
 * Indica la redirección externa o interna
 * @var string
 */
  public $redirect = false;

/**
 * Indica que el procesamiento se ha efectuado correctamente
 * @var boolean
 */
  public $success = false;

/**
 * Datos de respuesta. Será distinto para cada sistema de pago
 * @var mixed
 */
  public $data;

/**
 * Indica que el proceso ha sido finalizado
 * @var boolean
 */
  public $finalize = false;

/**
 * Mensaje de error en caso de que el proceso haya salido mal
 * @var string
 */
  public $message_error;

/**
 * Mensaje de error en caso de que el proceso haya salido mal
 * @var string
 */
  public $redirectToError = false;

/**
 * El constructor puede setear los valores de las propiedades
 * 
 * @param array $config
 */
  public function __construct( $config = [])
  {
    $this->set( $config);
  }

/**
 * Setea las propiedades, bien con un array key => value o indicando (key, value)
 * 
 * @param mixed|array|string    $property
 * @param string                $value
 */
  public function set( $property, $value = null)
  {
    if( !is_array( $property) && $value)
    {
      $property = [$property => $value];
    }

    foreach( $property as $prop => $value)
    {
      $this->$prop = $value;
    }
  }

/**
 * Indica que un proceso se ha efectuado con éxito y ha finalizado
 * 
 * @return boolean
 */
  public function success()
  {
    return $this->success && $this->finalize;
  }

/**
 * Indica si el pago tiene una redirección
 * 
 * @return boolean
 */
  public function hasRedirect()
  {
    return !empty( $this->redirect);
  }

  public function redirectToError()
  {
    return $this->redirectToError;
  }

/**
 * Convierte en un array las propiedades del objeto
 * 
 * @return array
 */
  public function toArray()
  {
    $out = [];

    foreach( $this->_properties as $prop)
    {
      $out [$prop] = $this->$prop;
    }

    return $out;
  }
}