<?php

namespace Store\Payment;


interface PaymentInterface
{
  public function checkout( \Store\Model\Entity\Order $order, \Cake\Http\ServerRequest $request);
}