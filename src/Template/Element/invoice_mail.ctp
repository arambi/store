

<!-- order_address_plain -->
	<table class="email_table store__invoice" width="100%" border="0" cellspacing="0" cellpadding="0">
		<tbody>
			<tr>
				<td class="email_body tc">
					<!--[if (mso)|(IE)]><table width="650" border="0" cellspacing="0" cellpadding="0" align="center" style="vertical-align:top;width:650px;Margin:0 auto;"><tbody><tr><td style="line-height:0px;font-size:0px;mso-line-height-rule:exactly;"><![endif]-->
					<div class="email_container">
						<table class="content_section" width="100%" border="0" cellspacing="0" cellpadding="0">
							<tbody>
								<tr>
									<td class="content_cell tc">
										<!-- col-3x2 -->
										<div class="email_row email_row--align-left">
                      <!--[if (mso)|(IE)]><table width="600" border="0" cellspacing="0" cellpadding="0" align="center" style="vertical-align:top;width:600px;Margin:0 auto;"><tbody><tr><td valign="top" width="200" style="width:200px;line-height:0px;font-size:0px;mso-line-height-rule:exactly;"><![endif]-->
											<?php if( !empty( $order->pretty_delivery_address)): ?>
                        <div class="col_2 store__invoice__address">
                          <table class="column" width="100%" border="0" cellspacing="0" cellpadding="0">
                            <tbody>
                              <tr>
                                <td class="column_cell px tl">
                                <h5><?= __d( 'app', 'Dirección de envío') ?></h5>
                                <p><?= $order->pretty_delivery_address ?></p>
                                </td>
                              </tr>
                            </tbody>
                          </table>
                        </div>
											<?php endif ?>
                      <!--[if (mso)|(IE)]></td><td valign="top" width="200" style="width:200px;line-height:0px;font-size:0px;mso-line-height-rule:exactly;"><![endif]-->
                      <?php if( !empty( $order->pretty_invoice_address)): ?>
                        <div class="col_2 store__invoice__address">
                          <table class="column " width="100%" border="0" cellspacing="0" cellpadding="0">
                            <tbody>
                              <tr>
                                <td class="column_cell px tl">
                                  <h5><?= __d( 'app', 'Dirección de facturación') ?></h5>
                                  <p><?= $order->pretty_invoice_address ?></p>
                                </td>
                              </tr>
                            </tbody>
                          </table>
                        </div>
											<?php endif ?>
                      
                    <!--[if (mso)|(IE)]></td><td valign="top" width="200" style="width:200px;line-height:0px;font-size:0px;mso-line-height-rule:exactly;"><![endif]-->
											<div class="col_2 store__invoice__address">
												<table class="column" width="100%" border="0" cellspacing="0" cellpadding="0">
													<tbody>
														<tr>
															<td class="column_cell px tl">
                                <h5><?= __d( 'app', 'Pedido') ?></h5>
                                <?php if( !empty( $order->invoice_number)): ?>
                                  <p><strong><?= __d( 'app', 'Nº de factura') ?>: </strong><?= $order->invoice_number  ?></p>
                                <?php elseif( !empty( $order->receipt_number)): ?>
                                  <p><strong><?= __d( 'app', 'Nº de recibo') ?>: </strong><?= $order->receipt_number  ?></p>
                                <?php endif ?>

                                <p><strong><?= __d( 'app', 'Referencia') ?>: </strong><?= $order->id  ?></p>
                                
                                <?php if( !empty( $is_admin) && $order->payment_method->processor == 'redsys'): ?>
                                  <p><strong><?= __d( 'app', 'Referencia de pago con tarjeta') ?>: </strong><?= $order->payment->id  ?></p>
                                <?php endif ?>
                                
                                <p><strong><?= __d( 'app', 'Fecha') ?>: </strong><?= $order->order_date_human_day ?></p>
                                <br>

                                <h5><?= __d( 'app', 'Método de pago') ?></h5>
                                <p><?= $order->payment_method->title ?></p>
                                <?php if( $order->payment_method->processor == 'bankcheck' && !empty( $order->payment_method->settings->account_number)): ?>
                                  <h5><?= __d( 'app', 'Nº de cuenta') ?></h5>
                                  <p><?= $order->payment_method->settings->account_number ?></p>
                                <?php endif ?>

                                <?php if( $this->loadHelper( 'Section.Nav')->website( 'settings.store_show_delivery_days') && !empty( $order->delivery_date)): ?>                                
                                  <h5><?= __d( 'app', 'Entrega') ?></h5>
                                  <p><?= $order->delivery_date_human ?></p>
                                <?php endif ?>
                                
															</td>
														</tr>
													</tbody>
												</table>
											</div>
										<!--[if (mso)|(IE)]></td></tr></tbody></table><![endif]-->
										</div>
									</td>
								</tr>
							</tbody>
						</table>
					</div>
					<!--[if (mso)|(IE)]></td></tr></tbody></table><![endif]-->
				</td>
			</tr>
		</tbody>
	</table>


<!-- order_total_alt -->
<table class="email_table store__invoice" width="100%" border="0" cellspacing="0" cellpadding="0">
		<tbody>
			<tr>
				<td class="email_body tc">
					<!--[if (mso)|(IE)]><table width="650" border="0" cellspacing="0" cellpadding="0" align="center" style="vertical-align:top;width:650px;Margin:0 auto;"><tbody><tr><td style="line-height:0px;font-size:0px;mso-line-height-rule:exactly;"><![endif]-->
					<div class="email_container">
						<table class="content_section" width="100%" border="0" cellspacing="0" cellpadding="0">
							<tbody>
								<tr>
									<td class="content_cell pb">
                    <div class="email_row"> 
                      <!--[if (mso)|(IE)]><table width="600" border="0" cellspacing="0" cellpadding="0" align="center" style="vertical-align:top;width:600px;Margin:0 auto;"><tbody><tr><td width="300" style="width:300px;line-height:0px;font-size:0px;mso-line-height-rule:exactly;"><![endif]-->
											<div class="col_6 store__invoice__address">
												<table class="column" width="100%" border="0" cellspacing="0" cellpadding="0">
													<tbody>
														<tr>
															<td class="column_cell px tl">
                                <h2 class="store__invoice__heading"><?= __d( 'app', 'Pedido') ?></h2>
															</td>
														</tr>
													</tbody>
												</table>
                      </div>
                      <!--[if (mso)|(IE)]></td><td width="300" style="width:300px;line-height:0px;font-size:0px;mso-line-height-rule:exactly;"><![endif]-->
                    </div>
									</td>
								</tr>
							</tbody>
						</table>
					</div>
					<!--[if (mso)|(IE)]></td></tr></tbody></table><![endif]-->
				</td>
			</tr>
		</tbody>
	</table>




<!-- invoice_header -->
<table class="email_table hide store__invoice" width="100%" border="0" cellspacing="0" cellpadding="0">
<tbody>
  <tr>
    <td class="email_body tc">
      <!--[if (mso)|(IE)]><table width="650" border="0" cellspacing="0" cellpadding="0" align="center" style="vertical-align:top;width:650px;Margin:0 auto;"><tbody><tr><td style="line-height:0px;font-size:0px;mso-line-height-rule:exactly;"><![endif]-->
      <div class="email_container">
        <table class="content_section" width="100%" border="0" cellspacing="0" cellpadding="0">
          <tbody>
            <tr>
              <td class="content_cell pb">
                <!-- col-6 -->
                <div class="email_row tl store__invoice__lines__header">
                <!--[if (mso)|(IE)]><table width="600" border="0" cellspacing="0" cellpadding="0" align="center" style="vertical-align:top;width:600px;Margin:0 auto 0 0;"><tbody><tr><td valign="top" width="400" style="width:400px;line-height:0px;font-size:0px;mso-line-height-rule:exactly;"><![endif]-->
                  <div class="col_4">
                    <table class="column" width="100%" border="0" cellspacing="0" cellpadding="0">
                      <tbody>
                        <tr>
                          <td class="column_cell px pt_0 pb_0 tl">
                            <p class="small mb_0 tm"><?= __d( 'app', 'Producto') ?></p>
                          </td>
                        </tr>
                      </tbody>
                    </table>
                  </div>
                <!--[if (mso)|(IE)]></td><td width="100" style="width:100px;line-height:0px;font-size:0px;mso-line-height-rule:exactly;"><![endif]-->
                  <div class="col_1">
                    <table class="column" width="100%" border="0" cellspacing="0" cellpadding="0">
                      <tbody>
                        <tr>
                          <td class="column_cell px pt_0 pb_0 tc">
                            <p class="small mb_0 tm"><?= __d( 'app', 'Cantidad') ?></p>
                          </td>
                        </tr>
                      </tbody>
                    </table>
                  </div>
                <!--[if (mso)|(IE)]></td><td width="100" style="width:100px;line-height:0px;font-size:0px;mso-line-height-rule:exactly;"><![endif]-->
                  <div class="col_1">
                    <table class="column" width="100%" border="0" cellspacing="0" cellpadding="0">
                      <tbody>
                        <tr>
                          <td class="column_cell px pt_0 pb_0 tl">
                            <p class="small mb_0 tm tr"><?= __d( 'app', 'Precio') ?></p>
                          </td>
                        </tr>
                      </tbody>
                    </table>
                  </div>
                <!--[if (mso)|(IE)]></td></tr></tbody></table><![endif]-->
                </div>
              </td>
            </tr>
          </tbody>
        </table>
      </div>
      <!--[if (mso)|(IE)]></td></tr></tbody></table><![endif]-->
    </td>
  </tr>
</tbody>
</table>



      
<!-- invoice_row -->

<?php foreach( $order->line_items as $item): ?>
    <?= $this->element( 'Store.invoice_mail_item', ['item' => $item]) ?> 
<?php endforeach ?>


<!-- order_total_alt -->
<table class="email_table store__invoice store__invoice__total" width="100%" border="0" cellspacing="0" cellpadding="0">
		<tbody>
			<tr>
				<td class="email_body tc">
					<!--[if (mso)|(IE)]><table width="650" border="0" cellspacing="0" cellpadding="0" align="center" style="vertical-align:top;width:650px;Margin:0 auto;"><tbody><tr><td style="line-height:0px;font-size:0px;mso-line-height-rule:exactly;"><![endif]-->
					<div class="email_container">
						<table class="content_section" width="100%" border="0" cellspacing="0" cellpadding="0">
							<tbody>
								<tr>
									<td class="content_cell pb">
										<!-- col-6 -->
										<div class="email_row tr">
                      <!--[if (mso)|(IE)]><table width="600" border="0" cellspacing="0" cellpadding="0" align="center" style="vertical-align:top;width:600px;Margin:0 auto;"><tbody><tr><td width="500" style="text-align: right; width:500px;line-height:0px;font-size:0px;mso-line-height-rule:exactly;"><![endif]-->
                      <div class="col_5">
                        <table class="column" width="100%" border="0" cellspacing="0" cellpadding="0">
                          <tbody>
                            <tr>
                              <td class="column_cell px pt_0 pb_0" align="right" style="text-align: right">
                                <p><span class="store__invoice__total__subtotal store__invoice__total__label"><?= __d( 'app', 'Subtotal') ?></span></p>
                                <?php if( $order->coupon_discount > 0): ?>
                                  <p><span class="store__invoice__total__label"><?= __d( 'app', 'Cupón de descuento') ?> <?= $order->coupon_name ?></span></p>
                                <?php endif ?>

                                <?php if( !\Store\Config\StoreConfig::getConfig( 'notShowCarrier') && !empty( $order->shipping_handling)): ?>
                                  <p>
                                    <?php if( $order->express_delivery): ?>
                                      <span class="store__invoice__total__shipping store__invoice__total__label"><?= __d( 'app', 'Envío exprés') ?></span>
                                    <?php else: ?>
                                      <span class="store__invoice__total__shipping store__invoice__total__label"><?= __d( 'app', 'Gastos de envío') ?></span>
                                    <?php endif ?>
                                  </p>
                                <?php endif ?>
                                <p><span class="store__invoice__total__total store__invoice__total__label"><?= __d( 'app', 'Total') ?></span></p>
                              </td>
                            </tr>
                          </tbody>
                        </table>
                      </div>
                      <!--[if (mso)|(IE)]></td><td width="100" style="text-align: right; width:100px;line-height:0px;font-size:0px;mso-line-height-rule:exactly;"><![endif]-->
                      <div class="col_1">
                        <table class="column" width="100%" border="0" cellspacing="0" cellpadding="0">
                          <tbody>
                            <tr>
                              <td class="column_cell px pt_0 pb_0" align="right" style="text-align: right">
                                <p><span class="store__invoice__total__subtotal"><?= $order->subtotal_human ?></span></p>
                                <?php if( $order->coupon_discount > 0): ?>
                                  <p><span class="store__invoice__total__coupon-discount">- <?= $order->coupon_discount_human ?></span></p>
                                <?php endif ?>
                                <?php if( !\Store\Config\StoreConfig::getConfig( 'notShowCarrier') && !empty( $order->shipping_handling)): ?>
                                  <p><span class="store__invoice__total__shipping"><?= $order->shipping_handling_human ?></span></p>
                                <?php endif ?>
                                <p><span class="store__invoice__total__total"><?= $order->total_human ?></span></p>
                              </td>
                            </tr>
                          </tbody>
                        </table>
                      </div>
										<!--[if (mso)|(IE)]></td></tr></tbody></table><![endif]-->
										</div>
									</td>
								</tr>
							</tbody>
						</table>
					</div>
					<!--[if (mso)|(IE)]></td></tr></tbody></table><![endif]-->
				</td>
			</tr>
		</tbody>
	</table>


<?php if( !empty( $order->grif_wrap)): ?>
  
  <!-- order_total_alt -->
  <table class="email_table store__invoice" width="100%" border="0" cellspacing="0" cellpadding="0">
		<tbody>
			<tr>
				<td class="email_body tc">
					<!--[if (mso)|(IE)]><table width="650" border="0" cellspacing="0" cellpadding="0" align="center" style="vertical-align:top;width:650px;Margin:0 auto;"><tbody><tr><td style="line-height:0px;font-size:0px;mso-line-height-rule:exactly;"><![endif]-->
					<div class="email_container">
						<table class="content_section" width="100%" border="0" cellspacing="0" cellpadding="0">
							<tbody>
								<tr>
									<td class="content_cell pb">
										<!-- col-6 -->
										<div class="email_row tl">
                      <!--[if (mso)|(IE)]><table width="600" border="0" cellspacing="0" cellpadding="0" align="center" style="vertical-align:top;width:600px;Margin:0 auto;"><tbody><tr><td width="400" style="width:400px;line-height:0px;font-size:0px;mso-line-height-rule:exactly;"><![endif]-->
                      <div class="col_6">
                        <table class="column" width="100%" border="0" cellspacing="0" cellpadding="0">
                          <tbody>
                            <tr>
                              <td class="column_cell px pt_0 pb_0">
                                <p><strong><?= __d( 'app', 'Observaciones') ?></strong></p>
                                <p><?= __d( 'app', 'Envolver para regalo') ?></p>
                              </td>
                            </tr>
                          </tbody>
                        </table>
                      </div>
                      <!--[if (mso)|(IE)]></td></tr></tbody></table><![endif]-->
										</div>
									</td>
								</tr>
							</tbody>
						</table>
					</div>
					<!--[if (mso)|(IE)]></td></tr></tbody></table><![endif]-->
				</td>
			</tr>
		</tbody>
	</table>
<?php endif ?>
  



<?php if( !empty( $order->comments)): ?>
  <!-- order_total_alt -->
  <table class="email_table store__invoice" width="100%" border="0" cellspacing="0" cellpadding="0">
		<tbody>
			<tr>
				<td class="email_body tc">
					<!--[if (mso)|(IE)]><table width="650" border="0" cellspacing="0" cellpadding="0" align="center" style="vertical-align:top;width:650px;Margin:0 auto;"><tbody><tr><td style="line-height:0px;font-size:0px;mso-line-height-rule:exactly;"><![endif]-->
					<div class="email_container">
						<table class="content_section" width="100%" border="0" cellspacing="0" cellpadding="0">
							<tbody>
								<tr>
									<td class="content_cell pb">
										<!-- col-6 -->
										<div class="email_row tl">
                      <!--[if (mso)|(IE)]><table width="600" border="0" cellspacing="0" cellpadding="0" align="center" style="vertical-align:top;width:600px;Margin:0 auto;"><tbody><tr><td width="400" style="width:400px;line-height:0px;font-size:0px;mso-line-height-rule:exactly;"><![endif]-->
                      <div class="col_6">
                        <table class="column" width="100%" border="0" cellspacing="0" cellpadding="0">
                          <tbody>
                            <tr>
                              <td class="column_cell px pt_0 pb_0">
                                <p><strong><?= __d( 'app', 'Comentarios') ?></strong></p>
                                <p><?= $order->comments ?></p>
                              </td>
                            </tr>
                          </tbody>
                        </table>
                      </div>
                      <!--[if (mso)|(IE)]></td></tr></tbody></table><![endif]-->                      
										</div>
									</td>
								</tr>
							</tbody>
						</table>
					</div>
					<!--[if (mso)|(IE)]></td></tr></tbody></table><![endif]-->
				</td>
			</tr>
		</tbody>
	</table>
<?php endif ?>
  




<?php if( \Cake\Core\Configure::read( 'OrdersBottomEmail.'. $order->status)): ?>
  <!-- order_total_alt -->
  <table class="email_table store__invoice" width="100%" border="0" cellspacing="0" cellpadding="0">
		<tbody>
			<tr>
				<td class="email_body tc">
					<!--[if (mso)|(IE)]><table width="650" border="0" cellspacing="0" cellpadding="0" align="center" style="vertical-align:top;width:650px;Margin:0 auto;"><tbody><tr><td style="line-height:0px;font-size:0px;mso-line-height-rule:exactly;"><![endif]-->
					<div class="email_container">
						<table class="content_section" width="100%" border="0" cellspacing="0" cellpadding="0">
							<tbody>
								<tr>
									<td class="content_cell pb">
										<!-- col-6 -->
										<div class="email_row tl">
                      <!--[if (mso)|(IE)]><table width="600" border="0" cellspacing="0" cellpadding="0" align="center" style="vertical-align:top;width:600px;Margin:0 auto;"><tbody><tr><td width="400" style="width:400px;line-height:0px;font-size:0px;mso-line-height-rule:exactly;"><![endif]-->
                      <div class="col_6">
                        <table class="column" width="100%" border="0" cellspacing="0" cellpadding="0">
                          <tbody>
                            <tr>
                              <td class="column_cell px pt_0 pb_0">
                                <img width="500" style="width: 500px" src="<?= \Cake\Core\Configure::read( 'OrdersBottomEmail.'. $order->status) ?>" />
                              </td>
                            </tr>
                          </tbody>
                        </table>
                      </div>
										</div>
									</td>
								</tr>
							</tbody>
						</table>
					</div>
					<!--[if (mso)|(IE)]></td></tr></tbody></table><![endif]-->
				</td>
			</tr>
		</tbody>
	</table>
<?php endif ?>
  


