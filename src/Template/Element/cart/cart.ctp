<? if( empty( $order->line_items)): ?>
    <p><?= __d( 'app', 'No tienes actualmente productos añadidos a la cesta de la compra') ?></p>
    <? return ?>
<? endif ?>
<table class="table">
  <thead>
    <tr>
      <th><?= __d( 'app', 'Producto') ?></th>
      <th><?= __d( 'app', 'Descripción') ?></th>
      <th><?= __d( 'app', 'Precio unitario') ?></th>
      <th><?= __d( 'app', 'Cantidad') ?></th>
      <th>&nbsp;</th>
      <th><?= __d( 'app', 'Total') ?></th>
    </tr>
  </thead>
  <tbody>
    <? foreach( $order->line_items as $item): ?>
        <tr>
          <td><?= $this->Cart->itemImage( $item) ?></td>
          <td><?= $item->title ?></td>
          <td><?= $item->store_price ?></td>
          <td><?= $item->quantity ?></td>
          <td>Borrar</td>
          <td><?= $this->ShopPrice->currency( $item->subtotal) ?></td>
        </tr>
    <? endforeach ?>
    <tr>
      <td colspan="2">&nbsp;</td>
      <td colspan="3"><?= __d( 'app', 'Total productos') ?></td>
      <td><?= $this->ShopPrice->currency( $order->subtotal) ?></td>
    </tr>
    <tr>
      <td colspan="2">&nbsp;</td>
      <td colspan="3"><?= __d( 'app', 'Gastos de envío') ?></td>
      <td><?= $this->ShopPrice->currency( $order->shipping_handling) ?></td>
    </tr>

    <? foreach( $order->line_taxes as $tax_rate => $total): ?>
      <tr>
        <td colspan="2">&nbsp;</td>
        <td colspan="3"><?= $this->Cart->taxRate( $tax_rate) ?></td>
        <td><?= $this->ShopPrice->currency( $total) ?></td>
      </tr>
    <? endforeach ?>
    <tr>
      <td colspan="2">&nbsp;</td>
      <td colspan="3"><?= __d( 'app', 'Total') ?></td>
      <td><?= $this->ShopPrice->currency( $order->total) ?></td>
    </tr>
  </tbody>
</table>

<?= $this->Html->link( __d( 'app', 'Continuar'), $this->Nav->url([
    'plugin' => 'Store',
    'controller' => 'Orders',
    'action' => 'address'
])) ?>