<div class="checkout-payment-methods">
  <? if( $payments->count() == 1): ?>
      <?= $this->Form->input( 'Orders.payment_method_id', [
        'value' => $payments->first()->id,
        'type' => 'hidden',
      ]) ?>
      <div>
        <h4><?= __d( 'app', 'Método de pago') ?></h4>
        <p><?= $payments->first()->title ?></p>
      </div>
  <? else: ?>
      <input name="Orders[payment_method_id]" value="" type="hidden">
      <? foreach( $payments as $payment): ?>
        <div class="payment-<?= $payment->processor ?>">
          <input id="orders-payment-method-<?= $payment->id ?>" name="Orders[payment_method_id]" type="radio" value="<?= $payment->id ?>" <?= $this->Form->context()->val( 'payment_method_id') == $payment->id ? 'checked="checked"' : '' ?> /><label for="orders-payment-method-<?= $payment->id ?>"><?= $payment->title ?></label>
          <?php if( $payment->processor == 'bankcheck' && !empty( $payment->settings->account_number)): ?>
            <?= __d( 'app', 'Nº de cuenta') ?>: <?= $payment->settings->account_number ?>
          <?php endif ?>
        </div>
    <? endforeach ?>
  <? endif ?>
</div>
  