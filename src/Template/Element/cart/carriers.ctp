<h3><?= __d( 'app', 'Transporte') ?></h3>
<div class="checkout-carriers">
  <? if( $carriers->count() == 1): ?>
      <?= $this->Form->input( 'Orders.carrier_id', [
        'value' => $carriers->first()->id,
        'type' => 'hidden',
      ]) ?>
      <div>
        <h4><?= __d( 'app', 'Transporte') ?></h4>
        <p><?= $carriers->first()->title ?></p>
      </div>
  <? else: ?>
      <input name="Orders[carrier_id]" value="" type="hidden">
      <? foreach( $carriers as $carrier): ?>
        <div>
          <input id="orders-carrier-method-<?= $carrier->id ?>" name="Orders[carrier_id]" type="radio" value="<?= $carrier->id ?>" <?= $this->Form->context()->val( 'carrier_id') == $carrier->id ? 'checked="checked"' : '' ?> /><label for="orders-carrier-method-<?= $carrier->id ?>"><?= $carrier->title ?></label>
        </div>
    <? endforeach ?>
  <? endif ?>
</div>
  