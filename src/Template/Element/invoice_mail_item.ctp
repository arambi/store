<!-- invoice_row -->
<table class="email_table store__invoice store__invoice__lines__item" width="100%" border="0" cellspacing="0" cellpadding="0">
  <tbody>
    <tr>
      <td class="email_body tc">
        <!--[if (mso)|(IE)]><table width="650" border="0" cellspacing="0" cellpadding="0" align="center" style="vertical-align:top;width:650px;Margin:0 auto;"><tbody><tr><td style="line-height:0px;font-size:0px;mso-line-height-rule:exactly;"><![endif]-->
        <div class="email_container">
          <table class="content_section" width="100%" border="0" cellspacing="0" cellpadding="0">
            <tbody>
              <tr>
                <td class="content_cell pb">
                  <table class="hr_rl" align="center" width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tbody>
                      <tr>
                        <td class="hr_ep">&nbsp; </td>
                      </tr>
                    </tbody>
                  </table>
                  <!-- col-6 -->
                  <div class="email_row tl">
                  <!--[if (mso)|(IE)]><table width="600" border="0" cellspacing="0" cellpadding="0" align="center" style="vertical-align:top;width:600px;Margin:0 auto;"><tbody><tr><td valign="top" width="400" style="width:400px;line-height:0px;font-size:0px;mso-line-height-rule:exactly;"><![endif]-->
                    <div class="col_4">
                      <table class="column" width="100%" border="0" cellspacing="0" cellpadding="0">
                        <tbody>
                          <tr>
                            <td class="column_cell px pt_0 pb_0">
                              <p class="mt_0 mb_xxs"><?= $item->invoice_title ?></p>
                              <p class="small tm mb_0"><?= $item->description ?></p>
                            </td>
                          </tr>
                        </tbody>
                      </table>
                    </div>
                  <!--[if (mso)|(IE)]></td><td valign="top" width="100" style="width:100px;line-height:0px;font-size:0px;mso-line-height-rule:exactly;"><![endif]-->
                    <div class="col_1">
                      <table class="column" width="100%" border="0" cellspacing="0" cellpadding="0">
                        <tbody>
                          <tr>
                            <td class="column_cell px pt_0 pb_0 tc invoice_qty">
                              <p class="small mb_0"><?= $item->quantity ?></p>
                            </td>
                          </tr>
                        </tbody>
                      </table>
                    </div>
                  <!--[if (mso)|(IE)]></td><td valign="top" width="100" style="width:100px;line-height:0px;font-size:0px;mso-line-height-rule:exactly;"><![endif]-->
                    <div class="col_1">
                      <table class="column" width="100%" border="0" cellspacing="0" cellpadding="0">
                        <tbody>
                          <tr>
                            <td class="column_cell px pt_0 pb_0 tr invoice_price">
                              <p class="mb_0"><?= $item->price_human ?></p>
                            </td>
                          </tr>
                        </tbody>
                      </table>
                    </div>
                  <!--[if (mso)|(IE)]></td></tr></tbody></table><![endif]-->
                  </div>
                </td>
              </tr>
            </tbody>
          </table>
        </div>
        <!--[if (mso)|(IE)]></td></tr></tbody></table><![endif]-->
      </td>
    </tr>
  </tbody>
</table>
