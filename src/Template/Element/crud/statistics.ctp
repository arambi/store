  <div class="row wrapper wrapper-update border-bottom white-bg page-heading">
    <ng-include src="'header.html'"></ng-include>
  </div>
  <div class="animated fadeIn">
    <div class="row" ng-init="crudConfig = data.crudConfig">
      <ng-include src="'Manager/_warning.html'"></ng-include>
      
      <div class="ibox float-e-margins">
        <div  class="ibox-title">
          <h5><?= __d( 'admin', 'Parámetros') ?></h5>
        </div>
        <div class="ibox-content">
          <div class="row">
            <div class="col-md-9">
              <label class="control-label"><?= __d( 'admin', 'Desde') ?></label>
              <div class="dropdown btn-group">
                <a class="dropdown-toggle btn btn-outline btn-default btn-sm" data-toggle="dropdown">
                  {{( data.crudConfig.stats.form.date_start | date : 'd/M/yyyy')}}
                </a>

                <div class="dropdown-menu" role="menu" ng-click="$event.preventDefault();$event.stopPropagation()">
                  <div date-picker="data.crudConfig.stats.form.date_start" view="date" max-view="date" format="yyyy-mm-dd" min-view="date" max-date="new Date()"></div>
                </div>
              </div>
              <label class="control-label m-l"><?= __d( 'admin', 'Hasta') ?></label>
              <div class="dropdown btn-group">
                <a class="dropdown-toggle btn btn-outline btn-default btn-sm" data-toggle="dropdown">
                  {{( data.crudConfig.stats.form.date_end | date : 'd/M/yyyy')}}
                </a>

                <div class="dropdown-menu" role="menu" ng-click="$event.preventDefault();$event.stopPropagation()">
                  <div date-picker="data.crudConfig.stats.form.date_end" max-view="date" format="yyyy-mm-dd" min-view="date"></div>
                </div>
              </div>
            </div>
            <div class="col-md-3">
              <div class="form-group">
                <label class="control-label"><?= __d( 'admin', 'Grupos de usuario') ?></label>
                <select ng-model="data.crudConfig.stats.form.group_id" class="form-control" style="width:100%;">
                  <option style="display: none" value="">{{ field.empty }}</option>
                  <option ng-repeat="(id, title) in data.crudConfig.stats.groups" value="{{ id }}" ng-selected="data.crudConfig.stats.form.group_id == id">{{ title }}</option>
                </select>
              </div>
            </div>
          </div>
          <div class="row">
            <div class="col-md-6">
              <button class="btn btn-default" cf-stats-button><?= __d( 'admin', 'Calcular') ?></button>
            </div>
          </div>
        </div>
      </div>
          

      

      <div class="row">
        <div class="col-md-12">
          <div class="tabs-container">
            <div tabset>
              <div tab>
                <div tab-heading>
                  <?= __d( 'admin', 'Pedidos') ?>
                </div>
                <div class="panel-body">
                  <highchart config="data.crudConfig.stats.charts.orders"></highchart>
                </div>
              </div>
              <div tab>
                <div tab-heading>
                  <?= __d( 'admin', 'Volumen') ?>
                </div>
                <div class="panel-body">
                  <highchart config="data.crudConfig.stats.charts.totals"></highchart>
                </div>
              </div>
              <div tab>
                <div tab-heading>
                  <?= __d( 'admin', 'Carritos') ?>
                </div>
                <div class="panel-body">
                  <highchart config="data.crudConfig.stats.charts.carts"></highchart>
                </div>
              </div>
              <div tab>
                <div tab-heading>
                  <?= __d( 'admin', 'Productos vistos') ?>
                </div>
                <div class="panel-body">
                  <highchart config="data.crudConfig.stats.charts.views"></highchart>
                </div>
              </div>
              <div tab>
                <div tab-heading>
                  <?= __d( 'admin', 'Productos comprados') ?>
                </div>
                <div class="panel-body">
                  <highchart config="data.crudConfig.stats.charts.items"></highchart>
                </div>
              </div>

              <div tab>
                <div tab-heading>
                  <?= __d( 'admin', 'Top Productos') ?>
                </div>
                <div class="panel-body">
                  <div class="row">
                    <div class="col-md-4">
                      <div class="ibox float-e-margins">
                        <div class="ibox-title">
                          <h5><?= __d( 'admin', 'Ventas de productos') ?></h5>
                        </div>
                        <div class="ibox-content">
                          <table class="table table-striped">
                            <thead>
                              <tr>
                                <th><?= __d( 'admin', 'Foto') ?></th>
                                <th><?= __d( 'admin', 'Producto') ?></th>
                                <th><?= __d( 'admin', 'Número de ventas') ?></th>
                              </tr>
                            </thead>
                            <tbody>
                              <tr ng-repeat="item in data.crudConfig.stats.tables.products">
                                <td><img style="max-width: 50px" ng-src="{{ item.product.photos[0].paths.square }}"></td>
                                <td>{{ item.product.title }}</td>
                                <td>{{ item.number }}</td>
                              </tr>
                            </tbody>
                          </table>
                        </div>
                      </div>
                    </div>

                    <div class="col-md-4">
                      <div class="ibox float-e-margins">
                        <div class="ibox-title">
                          <h5><?= __d( 'admin', 'Ventas de productos/atributos') ?></h5>
                        </div>
                        <div class="ibox-content">
                          <table class="table table-striped">
                            <thead>
                              <tr>
                                <th><?= __d( 'admin', 'Foto') ?></th>
                                <th><?= __d( 'admin', 'Producto') ?></th>
                                <th><?= __d( 'admin', 'Número de ventas') ?></th>
                              </tr>
                            </thead>
                            <tbody>
                              <tr ng-repeat="item in data.crudConfig.stats.tables.product_attributes">
                                <td><img style="max-width: 50px" ng-src="{{ item.product.product.photos[0].paths.square }}"></td>
                                <td>{{ item.product.title }}</td>
                                <td>{{ item.number }}</td>
                              </tr>
                            </tbody>
                          </table>
                        </div>
                      </div>
                    </div>

                    <div class="col-md-4">
                      <div class="ibox float-e-margins">
                        <div class="ibox-title">
                          <h5><?= __d( 'admin', 'Productos más vistos') ?></h5>
                        </div>
                        <div class="ibox-content">
                          <table class="table table-striped">
                            <thead>
                              <tr>
                                <th><?= __d( 'admin', 'Foto') ?></th>
                                <th><?= __d( 'admin', 'Producto') ?></th>
                                <th><?= __d( 'admin', 'Número de vistas') ?></th>
                              </tr>
                            </thead>
                            <tbody>
                              <tr ng-repeat="item in data.crudConfig.stats.tables.product_views">
                                <td><img style="max-width: 50px" ng-src="{{ item.product.photos[0].paths.square }}"></td>
                                <td>{{ item.product.title }}</td>
                                <td>{{ item.number }}</td>
                              </tr>
                            </tbody>
                          </table>
                        </div>
                      </div>
                    </div>

                  </div>
                      
                </div>
              </div>

            </div>
          </div>
        </div>
          
      </div>
    </div>
  </div>


