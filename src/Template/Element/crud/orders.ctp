<div class="row wrapper wrapper-index border-bottom white-bg page-heading">
  <ng-include src="'header.html'"></ng-include>
</div>
<div class="wrapper animated fadeIn">
  <div class="row">
    <div class="col-lg-12">
      <div class="ibox float-e-margins">
        <div class="ibox-content">

          <div ng-if="data.contents.length == 0">
            <?= __d( 'admin', 'No ha resultados.') ?>
          </div>
          <table class="table table-striped table-bordered table-hover dataTables-example">
            <thead ng-show="data.contents.length > 0">
              <!-- Encabezados -->
              <tr>
                <th ng-repeat="field in data.crudConfig.view.fields | orderObjectBy:_index">
                  {{ field.label }}
                </th>
                <th></th>
              </tr>
              <!-- /Encabezados -->

              <?= $this->element( 'Manager.search') ?>

            </thead>
            <tbody>
              <tr ng-repeat="content in data.contents" class="gradeX">
                <td ng-repeat="field in data.crudConfig.view.fields | orderObjectBy:_index" class="{{ field.class }}">
                  <ng-include src="field.templateIndex + '.html'"></ng-include>
                </td>
                <td>
                  <a class="btn btn-success btn-bitbucket btn-xs" href="#{{ data.crudConfig.adminUrl }}view/{{ content.id }}"><i class="fa fa-pencil"></i></a>
                  <a class="btn btn-danger btn-bitbucket btn-xs" href confirm-delete cf-url="{{ content.urls.delete }}" cf-destroy="scope.data.contents.splice( scope.$index, 1)"><i class="fa fa-trash"></i></a>
                </td>
              </tr> 
            </tbody>
            <tfoot>
              <tr ng-show="data.contents.length > 10">
                <th ng-repeat="field in data.crudConfig.view.fields | orderObjectBy:_index">
                  {{ field.label }}
                </th>
              </tr>
            </tfoot>
          </table>

          <cf-paginator></cf-paginator>
        </div>
      </div>
    </div>
  </div>
</div>