<select ng-model="data.search[field.key]" type="text" class="form-control">
  <option ng-repeat="option in field.options" ng-selected="data.search.status == option.key" value="{{ option.key }}">{{ option.value }}</option>
</select>