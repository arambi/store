<div class="form-group"><label ng-help tooltip="{{ field.help }}" class="col-sm-2 control-label">{{ field.label }}</label>
  <div class="clearfix col-sm-10">
    <angucomplete-alt 
      autocomplete-data="{{ field.key }}"
      placeholder="<?= __d( 'admin', 'Introduce un texto') ?>"
      ng-model="content[field.key]"
      scope="content[field.key]"
      pause="400"
      selected-object="beforeSelect"
      remote-url="/admin/shop/products_colors/autocomplete.json?model={{ field.key }}&q="
      remote-url-data-field="results"
      title-field="title"
      image-field="photo_main.paths.square"
      minlength="2"
      clear-selected="true"
      input-class="form-control form-control-small"
      />
  </div>
  <div class="clearfix col-sm-10 col-sm-offset-2">
    <ul class="list-group clear-list">
      <li ng-repeat="_content in content[field.key]" class="list-group-item fist-item">
        <span class="pull-right"><a class="btn btn-danger btn-bitbucket btn-xs" href confirm-delete cf-destroy="scope.$parent.data.content[scope.$parent.field.key].splice( scope.$index, 1)"><i class="fa fa-trash"></i></a></span>
        <img style="width: 50px; margin-right: 20px" ng-src="{{ _content.photo_main.paths.square }}" /> {{ _content.title }}
      </li>
    </ul>
  </div>
</div>