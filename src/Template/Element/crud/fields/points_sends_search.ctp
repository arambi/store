<div ng-if="!field.translate" class="form-group" ng-class="field.error ? 'has-error' : false">
  <label class="col-lg-3 control-label"><?= __d( 'admin', 'Buscar usuarios') ?></label>
  <div class="col-lg-9">
    <input ng-model="data.custom.query" type="text" class="form-control" />
  </div>
</div>
<span cf-action-link="'/admin/shop/points_adds/search'" eval-return="scope.data.custom.users = result.users" params="{query: data.custom.query, id: data.content.id}" class="btn btn-primary btn-sm "><?= __d( 'admin', 'Buscar usuarios') ?></span>

<div class="table-responsive">
  <h4><?= __d( 'admin', 'Enviados {0} de {1}', ['{{ content.sends }}', '{{ content.total }}']) ?></h4>
  <table ng-if="data.custom.users" class="table table-striped table-bordered table-hover dataTables-example">
     <thead>
      <tr>
        <th><?= __d( 'admin', 'Nombre') ?></th>
        <th><?= __d( 'admin', 'Email') ?></th>
        <th><?= __d( 'admin', 'Enviado el') ?></th>
      </tr>
      <tr ng-repeat="user in data.custom.users">
        <td><a href="#/admin/shop/users/view/{{ user.user.id }}">{{ user.user.name }}</a></td>
        <td>{{ user.user.email }}</td>
        <td>{{ user.send_on | date:"dd-mm-yyyy HH:mm" }}</td>
      </tr>
    </thead>
  </table>
</div>