

<div class="form-group">
  <label ng-help tooltip="{{ field.help }}" class=" control-label">{{ field.label }}</label>
  <div ng-if="content.user">
    <a href="#/admin/store/users/view/{{ content.user.id }}">
      {{ content.user.name }} <{{ content.user.email }}> 
    </a>
    <br>{{ content.user.access_link}}</div>
</div>
