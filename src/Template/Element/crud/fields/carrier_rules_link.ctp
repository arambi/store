<div class="form-group">
    <label ng-help tooltip="{{ field.help }}" class="col-lg-3 control-label"></label>
    <a class="btn btn-primary" href="#admin/store/carrier_rules/index/{{ content.id }}" ng-model="content[field.key]" > <?= __d('admin', 'Reglas de pago') ?></a>
</div>