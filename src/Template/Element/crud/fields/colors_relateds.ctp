  <h4 class="m-t"><?= __d( 'admin', 'Relaciona los productos con los siguientes atributos') ?></h4>
  <div ng-repeat="color in content.colors">
    <h4>{{ color.title }}</h4>

    <p class=" clearfix">Productos</p>
    <div class="form-group">
      <div class="clearfix col-sm-10">
        <angucomplete-alt 
          shop-product-combi="products"
          placeholder="<?= __d(  'admin', 'Introduce un texto') ?>"
          scope="content.product_combi[color.id]"
          pause="400"
          selected-object="beforeSelectProducts"
          remote-url="{{ crudConfig.adminUrl }}autocomplete.json?model=products&q="
          remote-url-data-field="results"
          title-field="title"
          image-field="photo_main.paths.square"
          minlength="2"
          clear-selected="true"
          input-class="form-control form-control-small"
          />
      </div>

      <div class="clearfix col-sm-10">
        <ul class="list-group clear-list">
          <li ng-repeat="_content in content.product_combi[color.id].products" class="list-group-item fist-item">
            <span class="pull-right"><a class="btn btn-danger btn-bitbucket btn-xs" href confirm-delete cf-destroy="scope.$parent.data.content.product_combi[scope.color.id].products.splice( scope.$index, 1)"><i class="fa fa-trash"></i></a></span>
            <img style="width: 50px; margin-right: 20px" ng-src="{{ _content.photo_main.paths.square }}" /> {{ _content.title }}
          </li>
        </ul>
      </div>
    </div>


    <p class=" clearfix">Productos/colores</p>
    <div class="form-group">
      <div class="clearfix col-sm-10">
        <angucomplete-alt 
          shop-product-combi="products_colors"
          placeholder="<?= __d(  'admin', 'Introduce un texto') ?>"
          scope="content.product_combi[color.id]"
          pause="400"
          selected-object="beforeSelectColors"
          remote-url="{{ crudConfig.adminUrl }}autocomplete.json?model=products_colors&q="
          remote-url-data-field="results"
          title-field="title_color"
          image-field="color_photo.paths.square"
          minlength="2"
          clear-selected="true"
          input-class="form-control form-control-small"
          />
      </div>

      <div class="clearfix col-sm-10">
        <ul class="list-group clear-list">
          <li ng-repeat="_content in content.product_combi[color.id].products_colors" class="list-group-item fist-item">
            <span class="pull-right"><a class="btn btn-danger btn-bitbucket btn-xs" href confirm-delete cf-destroy="scope.$parent.data.content.product_combi[scope.color.id].products_colors.splice( scope.$index, 1)"><i class="fa fa-trash"></i></a></span>
            <img style="width: 50px; margin-right: 20px" ng-src="{{ _content.color_photo.paths.square }}" /> {{ _content.title_color }}
          </li>
        </ul>
      </div>
    </div>
  </div>  
