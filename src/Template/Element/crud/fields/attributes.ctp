<div class="row">
  <div class="col-md-4 col-sm-offset-2">
    <div class="row form-group">
      <label class="control-label">{{ field.label }}</label>
      <select ng-model="attribute" class="form-control m-b">
        <option ng-repeat="option in field.options" value="{{ $index }}">{{ option.title }}</option>
      </select>
    </div>
  </div>
  <div class="col-md-4  m-l">
    <div class="row form-group" ng-show="field.options[attribute].attributes.length > 0">
      <label class="control-label"><?= __d( 'admin', 'Opciones') ?></label>
      <select ng-model="option" class="form-control m-b">
        <option value="">-- <?= __d( 'admin', 'Selecciona una opción') ?> --</option>
        <option ng-repeat="option in field.options[attribute].attributes" value="{{ option }}">{{ option.title }}</option>
      </select>
      
    </div>
  </div>
</div>
<div class="row">
  <div class="col-md-2 m-b  col-sm-offset-2">
    <span shop-add-attribute ng-model="option" cf-target="content[field.key]" class="btn btn-default"><?= __d( 'admin', 'Añadir') ?></span>
  </div>
</div>
    

<div class="row">
  <div class="col-sm-8 col-sm-offset-2">
    <table class="table table-striped table-bordered table-hover dataTables-example">
      <thead>
        <tr>
          <th><?= __d( 'admin', 'Combinación/Opción') ?></th>
          <th><?= __d( 'admin', 'Acciones') ?></th>
        </tr>
      </thead>
      <tbody>
        <tr ng-repeat="option in content[field.key]">
          <td>{{ option.attribute_group.title }} - {{ option.title }}</td>
          <td><a class="btn btn-danger btn-bitbucket btn-xs" href confirm-delete cf-destroy="scope.content[scope.field.key].splice( scope.$index, 1)"><i class="fa fa-trash"></i></a></td>
        </tr>
      </tbody>
    </table>
  </div>
</div>
