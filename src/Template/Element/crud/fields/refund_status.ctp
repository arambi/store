<?php

use Store\Config\StoreConfig;
?>
<div class="form-group m-t-2">
    <label class="col-lg-3 control-label">Estado actual</label>
    <div class="col-lg-9">
        <h4 style="margin-top: 2px" ng-repeat="option in data.crudConfig.view.fields.status.options" ng-if="option.key == data.content.status">{{ option.value }}</h4>
    </div>
</div>