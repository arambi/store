
<div ng-if="data.content.id" class="form-group" ng-class="field.error ? 'has-error' : false">
  <label class="col-lg-3 control-label">Email para previsualización</label>
  <div class="col-lg-9">
    <input 
      ng-model="preview_email" 
      type="text" class="form-control">

    <span cf-action-link="'/admin/shop/points_adds/preview/{{ data.content.id }}'" eval-return="swal( result.text, '', result.type)" params="{email: preview_email}" class="btn btn-primary btn-sm m-t "><?= __d( 'admin', 'Previsualizar') ?></span>
  </div>
</div>