<?php

use Store\Config\StoreConfig;
?>
<div class="form-group m-t-2">
    <label class="col-lg-3 control-label"></label>
    <div class="col-lg-9">
        <button class="btn btn-primary btn-sm cf-save-button" ng-click="send( data.crudConfig.view.submit, data.content, false)">
            <i class="fa fa-check"></i>
            <span ng-if="data.content.status == '<?= StoreConfig::REFUND_STATUS_REVIEW ?>'">
                <?= __d('admin', 'Listo para procesar') ?>
            </span>
            <span ng-if="data.content.status == '<?= StoreConfig::REFUND_STATUS_RECEIVED ?>'">
                <?= __d('admin', 'Finalizar proceso de devolución') ?>
            </span>
        </button>
    </div>
</div>