<div ng-repeat="option in field.options">
  <div ng-if="content.processor == option.name">
    <div ng-repeat="(key, title) in option.keys">
      <div class="form-group">
        <label class="col-md-2 control-label">{{ title }}</label>
        <div class="col-md-10">
          <input 
            ng-model="data.content.settings[key]" 
            type="text" class="form-control">
        </div>
      </div>
    </div>
  </div>
</div>