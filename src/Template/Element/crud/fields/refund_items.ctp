<div class="form-group">
    <label class="col-lg-3 control-label"><?= __d('admin', 'Productos') ?></label>
    <div class="col-lg-9">
        <div class="form-group">
            <table class="table table-striped table-bordered table-hover dataTables-example" sortable-options="content[field.key]" config="field.crudConfig">
                <thead>
                    <tr>
                        <th>Producto</th>
                        <th>Cantidad</th>
                        <th>Precio</th>
                        <th>Rechazar</th>
                        <th>Estado</th>
                    </tr>
                </thead>
                <tbody>
                    <tr ng-repeat="(key, item) in content.refunds_items">
                        <td>{{ item.line_item.title }}</td>
                        <td>{{ item.quantity }}</td>
                        <td>{{ item.line_item.price }}</td>
                        <td>
                            <div ng-if="(data.content.status == '<?= \Store\Config\StoreConfig::REFUND_STATUS_REVIEW ?>'
                             || data.content.status == '<?= \Store\Config\StoreConfig::REFUND_STATUS_RECEIVED ?>')">
                                <div ng-if="item.status == '<?= \Store\Config\StoreConfig::ITEM_STATUS_REFUNDED_CANCELED ?>' && data.content.status == '<?= \Store\Config\StoreConfig::REFUND_STATUS_RECEIVED ?>'">
                                    <i ng-if="data.content.refunds_items[key].refuse" class="fa fa-check"></i>
                                </div>
                                <div ng-if="!(item.status == '<?= \Store\Config\StoreConfig::ITEM_STATUS_REFUNDED_CANCELED ?>' && data.content.status == '<?= \Store\Config\StoreConfig::REFUND_STATUS_RECEIVED ?>')">
                                    <input type="checkbox" ng-model="data.content.refunds_items[key].refuse">
                                </div>
                            </div>
                            <div ng-if="!((data.content.status == '<?= \Store\Config\StoreConfig::REFUND_STATUS_REVIEW ?>'
                                 || data.content.status == '<?= \Store\Config\StoreConfig::REFUND_STATUS_RECEIVED ?>'))">
                                <i ng-if="data.content.refunds_items[key].refuse" class="fa fa-check"></i>
                            </div>
                        </td>
                        <td>
                            <span ng-repeat="option in data.crudConfig.view.fields.refunds_items.crudConfig.view.fields.status.options" ng-if="option.key == item.status">{{ option.value }}</span>
                        </td>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>
</div>