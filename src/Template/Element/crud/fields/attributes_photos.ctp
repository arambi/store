<div ng-if="!field.translate" class="form-group" ng-class="field.error ? 'has-error' : false">
  <label ng-help tooltip="{{ field.help }}" class="col-sm-2 control-label">{{ field.label }}</label>
  <div class="col-sm-10">
    <div ng-repeat="photo in parentContent.photos" class="pull-left m-l">
      <label>
        <input type="radio" ng-model="content[field.key]" value="{{ photo.id }}" />
        <img ng-src="{{ photo.paths.thm }}" />
      </label>
    </div>
  </div>
</div>