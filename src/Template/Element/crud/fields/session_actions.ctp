


  <div class="table-responsive" style="max-width: calc( 100vw - 300px)">
    <table class="table table-striped table-bordered table-hover dataTables-example" config="field.crudConfig">
      <tr>
        <th>Fecha</th>
        <th>Acción</th>
        <th>Post</th>
        <th>Respuesta</th>
        <th>Errores</th>
      </tr>
      <tr ng-repeat="action in content.order_user_actions">
        <td>{{ action.created | date:'dd/MM/yyyy HH:mm' }}</td>
        <td>{{ action.action }}</td>
        <td><pre>{{ action.post | json }}</pre></td>
        <td><pre>{{ action.response | json }}</pre></td>
        <td><pre>{{ action.errors | json }}</pre></td>
      </tr>
    </table>
  </div>
