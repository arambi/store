<div>

  <input type="checkbox" class="js-switch" cf-switch="{color:'#1AB394'}" ng-model="photo.main"  /> <label><?= __d( 'admin', 'Principal') ?></label>
</div>

<div cf-attributes-unique>
  <label class="m-t"><?= __d( 'admin', 'Para los atributos') ?></label>
  <div  ng-repeat="product_attribute in arrayUnique(content.product_attributes)">
    <div ng-repeat="attribute in product_attribute.attributes">
      <div ng-if="attribute.attribute_group.has_product_photo == 1 && attribute.show">
        <input type="checkbox" class="js-switch" cf-switch="{color:'#1AB394'}" ng-model="photo.attributes[attribute.id]"  /> <label>{{ attribute.title }}</label>
      </div>
    </div>
  </div>  
</div>