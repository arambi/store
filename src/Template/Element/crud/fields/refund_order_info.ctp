<div class="form-group">
    <label class="col-lg-3 control-label">Pedido</label>
    <div class="col-lg-9">
        <div class="form-group">
            <div class="col-lg-4">
                <a target="_blank" href="/admin#/admin/store/orders/view/{{ content.order_id }}"><?= __d('admin', 'Ver pedido') ?></a>
            </div>
        </div>
        <div class="form-group">
            <div class="col-lg-4">
                <strong><?= __d('admin', 'Dirección de entrega') ?></strong>
                <div ng-bind-html="content.delivery_address"></div>
            </div>
            <div class="col-lg-4">
                <strong><?= __d('admin', 'Dirección de facturación') ?></strong>
                <div ng-bind-html="content.invoice_address"></div>
            </div>
        </div>
    </div>
</div>