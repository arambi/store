<div ng-if="!field.translate" class="form-group" ng-class="field.error ? 'has-error' : false">
  <label ng-help tooltip="{{ field.help }}" class="col-md-2 control-label">{{ field.label }}</label>
  <div class="col-md-10">
    <div>{{ content.user.name }} <{{ content.user.email }}> <a href="#/admin/shop/users/view/{{ content.user.id }}"> <?= __d( 'admin', 'Ver ficha') ?></a></div>
  </div>
</div>