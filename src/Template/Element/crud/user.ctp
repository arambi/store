  <div class="row wrapper wrapper-update border-bottom white-bg page-heading">
    <div class="col-lg-12 header-content">
      <h2><i class="{{ data.crudConfig.navigation.icon }}"></i> <?= __d( 'admin', '{{ data.content.name }} <small><{{ data.content.email }}></small>') ?>
      <button class="btn btn-primary btn-sm cf-save-button" 
          ng-if="data.crudConfig.view.saveButton" 
          ng-click="send( data.crudConfig.view.submit, data.content, false)">
          <i class="fa fa-check"></i> <?= __d( 'admin', 'Guardar') ?>
      </button>
      <span ng-if="data.crudConfig.view.actionButtons" class="btn btn-default btn-xs cf-action-button" ng-repeat="action in data.crudConfig.view.actionButtons"><a href="{{ action.url }}">{{ action.label }}</a></span>
      </h2>
    </div>
    <div class="col-lg-2"></div>
  </div>
  <div class="animated fadeIn">
    <div ng-init="crudConfig = data.crudConfig">
    <form cf-form class="form-horizontal"></form>
      <ng-include src="'Manager/_warning.html'"></ng-include>
      <div class="row">
        <div class="col-xs-6">
          <div class="ibox">

            <!-- Información -->
            <div class="ibox-title">
              <h5><i class="fa fa-user"></i> <?= __d( 'admin', 'Información') ?></h5>
            </div>
            <div class="ibox-content ibox-info">
              <div class="form-group">
                <label class="col-sm-5 text-right"><?= __d( 'admin', 'Nombre') ?></label>
                <div class="col-sm-7">
                  {{ data.content.name }}
                </div>
              </div>
              <div class="form-group">
                <label class="col-sm-5 text-right"><?= __d( 'admin', 'Email') ?></label>
                <div class="col-sm-7">
                  {{ data.content.email }}
                </div>
              </div>
              <div class="form-group" ng-if="data.content.age">
                <label class="col-sm-5 text-right"><?= __d( 'admin', 'Edad') ?></label>
                <div class="col-sm-7">
                  <?= __d( 'admin', '{{ data.content.age }} años') ?> (<?= __d( 'admin', 'fecha de nacimiento: {{ data.content.birthday | date }}') ?>)
                </div>
              </div>
              <div class="form-group">
                <label class="col-sm-5 text-right"><?= __d( 'admin', 'Fecha de registro') ?></label>
                <div class="col-sm-7">
                  {{ data.content.created | date }}
                </div>
              </div>
              <div class="form-group">
                <label class="col-sm-5 text-right"><?= __d( 'admin', 'Última visita') ?></label>
                <div class="col-sm-7">
                  {{ data.content.last_login | date }}
                </div>
              </div>

              <div class="form-group">
                <label class="col-sm-5 text-right"><?= __d( 'admin', 'Idioma') ?></label>
                <div class="col-sm-7">
                  {{ data.content.locale_human }}
                </div>
              </div>

              <div class="form-group">
                <label class="col-sm-5 text-right"><?= __d( 'admin', 'Estado') ?></label>
                <div class="col-sm-7">
                <select ng-model="data.content.status" class="form-control" style="width:100%;">
                  <option ng-init="content = data.content" ng-repeat="option in data.crudConfig.view.fields.status.options" value="{{ option.key }}" cf-bind-selected="data.crudConfig.view.fields.status.key">{{ option.value }}</option>
                </select>
                </div>
              </div>
              <div class="form-group">
                <label class="col-sm-5 text-right"><?= __d( 'admin', 'Grupo') ?></label>
                <div class="col-sm-7">
                  <select class="form-control" style="max-width:350px;"
                    ng-model="data.content.group" 
                    ng-options="item as item[data.crudConfig.view.fields.group.displayField] for item in data.crudConfig.view.fields.group.options track by item.id">
                    <option value="" ng-if="false"></option>
                  </select>
                </div>
              </div>
              <div class="form-group" ng-if="data.crudConfig.view.fields.payment_methods.options.length > 0">
                <label class="col-sm-5 text-right"><?= __d( 'admin', 'Métodos de pago') ?></label>
                <div class="col-sm-7">
                  <div class="row">
                    <div class="col-lg-12" ng-repeat="option in data.crudConfig.view.fields.payment_methods.options" cf-checkboxes="option" ng-model="data.content.payment_methods">
                      <label><input type="checkbox" /> {{ option.full_title }}</label>
                    </div>
                  </div>
                </div>
              </div>

              <crud-field ng-init="crudConfig = data.crudConfig; content = data.content" field="store_comments"></crud-field>
            </div>
            <!-- /informacón -->
          </div>
          <div class="ibox">
            <!-- Pedidos -->
            <div class="ibox-title">
              <h5><i class="fa fa-credit-card"></i> <?= __d( 'admin', 'Pedidos') ?></h5>
            </div>
            <div class="ibox-content">
              <div class="table-responsive">
                <table class="table table-striped">
                  <thead>
                    <tr>
                      <th>ID</th>
                      <th><?= __d( 'admin', 'Fecha') ?></th>
                      <th><?= __d( 'admin', 'Pago') ?></th>
                      <th><?= __d( 'admin', 'Estado') ?></th>
                      <th><?= __d( 'admin', 'Productos') ?></th>
                      <th><?= __d( 'admin', 'Total') ?></th>
                      <th> </th>
                    </tr>
                  </thead>
                  <tbody>
                    <tr ng-repeat="order in data.content.orders">
                      <td>{{ order.id }}</td>
                      <td>{{ order.order_date_human }}</td>
                      <td>{{ order.payment_method.title }}</td>
                      <td>{{ order.status_human }}</td>
                      <td>{{ order.total_items }}</td>
                      <td><span ng-bind-html="order.total_human"></span></td>
                      <td><a href="#/admin/store/orders/view/{{ order.id }}"><i class="fa fa-eye"></i></a></td>
                    </tr>
                  </tbody>
                </table>
              </div>
            </div>
            <!-- /Pedidos -->
          </div>
          <div class="ibox">
            <!-- Productos más vistos -->
            <div class="ibox-title">
              <h5><i class="fa fa-credit-card"></i> <?= __d( 'admin', 'Productos más vistos') ?></h5>
            </div>
            <div class="ibox-content">
              <div class="table-responsive">
                <table class="table table-striped">
                  <thead>
                    <tr>
                      <th><?= __d( 'admin', 'Producto') ?></th>
                      <th><?= __d( 'admin', 'Número') ?></th>
                      <th> </th>
                    </tr>
                  </thead>
                  <tbody>
                    <tr ng-repeat="product in data.content.product_views">
                      <td><a target="_blank" href="{{ product.product.weblink }}">{{ product.product.full_title }}</a></td>
                      <td>{{ product.total }}</td>
                    </tr>
                  </tbody>
                </table>
              </div>
            </div>
            <!-- /Productos más vistos -->
          </div>
        </div>

        
        <div class="col-xs-6">
          <!-- Direcciones -->
          <div class="ibox">
            <div class="ibox-title">
              <h5><i class="fa fa-user"></i> <?= __d( 'admin', 'Direcciones') ?></h5>
            </div>
            <div class="ibox-content">
              <div class="row">
                <div class="m-b">
                  <div class="tabs-container">
                    <tabset>
                      <tab heading="<?= __d( 'admin', 'Dirección de facturación') ?>">
                        <div class="panel-body" ng-init="data.content.address_invoice.type = 'invoice'">
                          <crud-field ng-init="crudConfig = data.crudConfig; content = data.content" field="address_invoice.firstname"></crud-field>
                          <crud-field ng-init="crudConfig = data.crudConfig; content = data.content" field="address_invoice.lastname"></crud-field>
                          <crud-field ng-init="crudConfig = data.crudConfig; content = data.content" field="address_invoice.company"></crud-field>
                          <crud-field ng-init="crudConfig = data.crudConfig; content = data.content" field="address_invoice.vat_number"></crud-field>
                          <crud-field ng-init="crudConfig = data.crudConfig; content = data.content" field="address_invoice.address"></crud-field>
                          <crud-field ng-init="crudConfig = data.crudConfig; content = data.content" field="address_invoice.postcode"></crud-field>
                          <crud-field ng-init="crudConfig = data.crudConfig; content = data.content" field="address_invoice.city"></crud-field>
                          <crud-field ng-init="crudConfig = data.crudConfig; content = data.content" field="address_invoice.state_id"></crud-field>
                          <crud-field ng-init="crudConfig = data.crudConfig; content = data.content" field="address_invoice.country_id"></crud-field>
                          <crud-field ng-init="crudConfig = data.crudConfig; content = data.content" field="address_invoice.email"></crud-field>
                        </div>
                      </tab>
                      <tab heading="<?= __d( 'admin', 'Dirección de envío') ?>">
                        <div class="panel-body" ng-init="data.content.address_delivery.type = 'delivery'">
                          <crud-field ng-init="crudConfig = data.crudConfig; content = data.content" field="address_delivery.firstname"></crud-field>
                          <crud-field ng-init="crudConfig = data.crudConfig; content = data.content" field="address_delivery.lastname"></crud-field>
                          <crud-field ng-init="crudConfig = data.crudConfig; content = data.content" field="address_delivery.company"></crud-field>
                          <crud-field ng-init="crudConfig = data.crudConfig; content = data.content" field="address_delivery.vat_number"></crud-field>
                          <crud-field ng-init="crudConfig = data.crudConfig; content = data.content" field="address_delivery.address"></crud-field>
                          <crud-field ng-init="crudConfig = data.crudConfig; content = data.content" field="address_delivery.postcode"></crud-field>
                          <crud-field ng-init="crudConfig = data.crudConfig; content = data.content" field="address_delivery.city"></crud-field>
                          <crud-field ng-init="crudConfig = data.crudConfig; content = data.content" field="address_delivery.state_id"></crud-field>
                          <crud-field ng-init="crudConfig = data.crudConfig; content = data.content" field="address_delivery.country_id"></crud-field>
                        </div>
                      </tab>
                    </tabset>
                  </div>
                </div>
              </div>
              
            </div>
          </div>
          <!-- /Cliente -->

          <!-- Puntos de descuento -->
          <?php if( \Store\Config\StoreConfig::getConfig('withPoints') && \Website\Lib\Website::get( 'settings.store_has_points')): ?>
            <div class="ibox">
              <div class="ibox-title">
                <h5><i class="fa fa-user"></i> <?= __d( 'admin', 'Puntos de descuento') ?> </h5>
                <span class="label label-primary">{{ data.content.total_points }}</span>
              </div>
              <div class="ibox-content">
                <h4><?= __d( 'admin', 'Puntos conseguidos') ?></h4>
                <div class="table-responsive">
                  <table class="table table-striped">
                    <thead>
                      <tr>
                        <th><?= __d( 'admin', 'Puntos') ?></th>
                        <th><?= __d( 'admin', 'Fecha') ?></th>
                        <th><?= __d( 'admin', 'Administrador') ?></th>
                        <th><?= __d( 'admin', 'Pedido') ?></th>
                      </tr>
                    </thead>
                    <tbody>
                      <tr ng-repeat="point in data.content.points_up">
                        <td>{{ point.points }}</td>
                        <td>{{ point.created | date }}</td>
                        <td>{{ point.administrator.name }}</td>
                        <td><a target="_blank" href="/admin/#/admin/store/orders/view/{{ point.order_id }}">{{ point.order_id }}</a></td>
                      </tr>
                    </tbody>
                  </table>
                </div>
  
                <h4><?= __d( 'admin', 'Puntos gastados') ?></h4>
                <div class="table-responsive">
                  <table class="table table-striped">
                    <thead>
                      <tr>
                        <th><?= __d( 'admin', 'Puntos') ?></th>
                        <th><?= __d( 'admin', 'Fecha') ?></th>
                        <th><?= __d( 'admin', 'Pedido') ?></th>
                        <th> </th>
                      </tr>
                    </thead>
                    <tbody>
                      <tr ng-repeat="point in data.content.points_down">
                        <td>{{ point.points }}</td>
                        <td>{{ point.created | date }}</td>
                        <td><a target="_blank" href="/admin/#/admin/store/orders/view/{{ point.order_id }}">{{ point.order_id }}</a></td>
                        <td></td>
                      </tr>
                    </tbody>
                  </table>
                </div>
  
                <input type="number" class="form-control" ng-model="points" style="width: 70px; display: inline; margin-right: 20px" />
                <span cf-action-link="'/admin/store/points/addup'" eval-return="if( result) scope.data.content.points_up = result.result" params="{salt: data.content.salt, points: points}" class="btn btn-primary btn-sm "><?= __d( 'admin', 'Añadir puntos') ?></span>
              </div>
            </div>
          <?php endif ?>
           <!-- /Puntos de descuento -->
        </div>
       
      </div>
      </form>
    </div>
  </div>