<?php

use Store\Config\StoreConfig; ?>
<div class="row wrapper wrapper-update border-bottom white-bg page-heading">
    <div class="col-lg-12 header-content">
        <h2><i class="{{ data.crudConfig.navigation.icon }}"></i> <?= __d('admin', 'Pedido nº {{ data.content.id }} de {{ data.content.adr_invoice_firstname }} {{ data.content.adr_invoice_lastname }} <small><{{ data.content.adr_invoice_email }}></small>') ?>
            <button class="btn btn-primary btn-sm cf-save-button" ng-if="data.crudConfig.view.saveButton" ng-click="send( data.crudConfig.view.submit, data.content, false)">
                <i class="fa fa-check"></i> <?= __d('admin', 'Guardar') ?>
            </button>
            <span ng-if="data.crudConfig.view.actionButtons" class="btn btn-default btn-xs cf-action-button" ng-repeat="action in data.crudConfig.view.actionButtons"><a href="{{ action.url }}">{{ action.label }}</a></span>
        </h2>
    </div>
    <div class="col-lg-2"></div>
</div>
<div class="animated fadeIn">
    <div ng-init="crudConfig = data.crudConfig">
        <ng-include src="'Manager/_warning.html'"></ng-include>

        <!-- Resumen -->
        <div class="ibox">
            <div class="ibox-content">
                <div class="row">
                    <div class="col-xs-6 col-sm-3">
                        <label><i class="fa fa-calendar"></i> <?= __d('admin', 'Fecha') ?></label>
                        <h2 ng-if="data.content.status">{{ data.content.order_date | date }}</h2>
                        <h2 ng-if="!data.content.status">{{ data.content.created | date }}</h2>
                    </div>
                    <div class="col-xs-6 col-sm-3">
                        <label><i class="fa fa-money"></i> <?= __d('admin', 'Total') ?></label>
                        <h2 ng-bind-html="data.content.total_human"></h2>
                    </div>
                    <div class="col-xs-6 col-sm-3">
                        <label><i class="fa fa-money"></i> <?= __d('admin', 'Estado') ?></label>
                        <h2>{{ data.content.status_human }}</h2>
                    </div>
                    <div class="col-xs-6 col-sm-3">
                        <label><i class="fa fa-money"></i> <?= __d('admin', 'Productos') ?></label>
                        <h2>{{ data.content.line_items.length }}</h2>
                    </div>
                </div>
            </div>
        </div>

        <!-- /Resumen -->

        <div class="row">
            <!-- Estado Pedido -->
            <div class="col-lg-8 col-md-12">
                <div class="ibox" ng-if="!data.content.status">
                    <div class="ibox-content">
                        <div class="alert alert-danger">Pedido no finalizado</div>
                    </div>
                </div>
                <div class="ibox" ng-if="!data.content.status">
                    <div class="ibox-content">
                        <span cf-action-link="'/admin/store/orders/finalize'" eval-return="window.location.reload()" params="{salt: data.content.salt}" class="btn btn-primary btn-sm "><?= __d('admin', 'Finalizar') ?></span>
                    </div>
                </div>
                <div class="ibox" ng-if="!data.content.status">
                    <div class="ibox-title">
                        <h5><i class="fa fa-credit-card"></i> <?= __d('admin', 'Intentos de pago') ?></h5>
                    </div>
                    <div class="ibox-content">
                        <table class="table table-striped table-bordered table-hover dataTables-example">
                            <thead>
                                <tr>
                                    <th>Sistema</th>
                                    <th>Respuesta</th>
                                    <th>Fecha</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr ng-repeat="payment in data.content.payments">
                                    <td>{{ payment.provider }}</td>
                                    <td>{{ payment.response_code }}</td>
                                    <td>{{ payment.created | date : 'dd/MM/yyyy HH:mm' }}</td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
                <div class="ibox" ng-if="data.content.status">
                    <div class="ibox-title">
                        <h5><i class="fa fa-credit-card"></i> <?= __d('admin', 'Pedido') ?></h5>
                    </div>
                    <div class="ibox-content">
                        <p ng-if="data.content.delivery_date"><label> <?= __d('admin', 'Fecha de entrega') ?></label>: {{ data.content.delivery_date | date }}</p>
                        <p ng-if="data.content.delivery_date_custom"><i class="fa fa-warning"></i> <?= __d('admin', 'El usuario ha pedido aplazamiento de la entrega') ?></p>
                        <p ng-if="data.content.payment_method.processor == 'redsys'"><label> <?= __d('admin', 'Referencia de pago con tarjeta') ?></label>: {{ data.content.payment.id }}</p>
                        <p ng-if="data.content.payment_method.processor == 'stripe'"><label> <?= __d('admin', 'Referencia de pago con tarjeta') ?></label>: {{ data.content.payment.transaction_id }}</p>
                        <p ng-if="data.content.has_invoice"><label> <?= __d('admin', 'Factura Nº') ?></label>: {{ data.content.invoice_number }}</p>
                        <p ng-if="data.content.has_receipt"><label> <?= __d('admin', 'Recibo Nº') ?></label>: {{ data.content.invoice_number }}</p>
                        <p ><label> <?= __d('admin', 'Idioma en el que hicieron el pedido') ?></label>: {{ data.content.locale_human }}</p>

                        <?php if (StoreConfig::getConfig('hasInvoiceNoTaxes')) : ?>
                            <div class="m-b" ng-if="data.content.status && data.content.shipping_handling_tax + data.content.taxes > 0">
                                <span promt="Estás seguro de quitar el IVA a la factura?" cf-action-link-promt="'/admin/store/orders/remove_taxes'" eval-return="window.location.reload()" params="{order_id: data.content.id}" class="btn btn-primary btn-sm "><?= __d('admin', 'Quitar el IVA de la factura') ?></span>
                            </div>
                        <?php endif ?>


                        <span ng-if="data.content.has_invoice" class="btn btn-primary btn-outline"><a style="color: #fff" target="_blank" href="{{ data.content.invoice_url }}"><?= __d('admin', 'Descargar factura') ?></a></span>
                        <span ng-if="data.content.has_invoice && data.content.invoice_url != data.content.invoice_url_original" class="btn btn-primary btn-outline"><a style="color: #fff" target="_blank" href="{{ data.content.invoice_url_original }}"><?= __d('admin', 'Descargar factura (en idioma del pedido)') ?></a></span>
                        <span class="btn btn-primary btn-outline"><a style="color: #fff" target="_blank" href="{{ data.content.delivery_note_url }}"><?= __d('admin', 'Descargar albarán') ?></a></span>
                        
                        <div style="margin-top: 15px" ng-bind-html="data.content.admin_custom_data"></div>

                        <?php if (StoreConfig::getConfig('adminEditCarrier')) : ?>
                        <div class="form-group">
                            <label class="col-lg-3 control-label">Transporte</label>
                            <div class="col-lg-9">
                                <select class="form-control" style="max-width:350px;" ng-model="data.content.carrier" ng-options="item as item[data.crudConfig.view.fields.carrier.displayField] for item in data.crudConfig.view.fields.carrier.options track by item.id">
                                    <option value="" ng-if="false"></option>
                                </select>
                            </div>
                        </div>
                        <?php endif ?>
                    </div>
                </div>
                <?php if (!StoreConfig::getConfig('dontChangeStatusFromAdmin')) : ?>
                    <div class="ibox" ng-if="data.content.status">
                        <div class="ibox-title">
                            <h5><i class="fa fa-credit-card"></i> <?= __d('admin', 'Estado del pedido') ?></h5>
                        </div>
                        <div class="ibox-content">
                            <!-- Statuses -->
                            <ul class="todo-list">
                                <li ng-repeat="history in data.content.order_histories"><span>{{ history.created | date }}</span> <strong>{{ history.status_human }}</strong></li>
                            </ul>
                            <!-- /Statuses -->
                            <select ng-model="changeStatus.status" class="form-control m-t" style="width: auto; display: inline;">
                                <option ng-repeat="(value, label) in data.statuses" value="{{ value }}">{{ label }}</option>
                            </select>
                            <span cf-action-link="'/admin/store/orders/change_status'" eval-return="scope.data.content = result.order; scope.data.content.order_histories = result.result; if( result.status_human)  scope.data.content.status_human = result.status_human, $timeout(function(){scope.$apply()})" params="{salt: data.content.salt, status: changeStatus.status}" class="btn btn-primary btn-sm "><?= __d('admin', 'Actualizar estado') ?></span>
                            <div class="row m-t">
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label><?= __d('admin', 'Comentarios del usuario') ?></label>
                                        <div>{{ data.content.comments }}</div>
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <crud-field ng-init="crudConfig = data.crudConfig; content = data.content" field="store_comments"></crud-field>
                                </div>
                            </div>

                        </div>
                    </div>
                <?php endif ?>

                <div class="ibox">
                    <div class="ibox-title">
                        <h5><i class="fa fa-cart"></i> <?= __d('admin', 'Productos') ?></h5>
                    </div>
                    <div class="ibox-content">
                        <!-- LineItems -->
                        <div class="row">
                            <div class="col-md-12">
                                <div class="table-responsive">
                                    <table class="table table-striped">
                                        <thead>
                                            <tr>
                                                <th></th>
                                                <th><?= __d('admin', 'Producto') ?></th>
                                                <th><?= __d('admin', 'Precio unitario') ?></th>
                                                <th><?= __d('admin', 'Cant.') ?></th>
                                                <th class="text-right"><?= __d('admin', 'Total') ?></th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr ng-repeat="item in data.content.line_items">
                                                <td></td>
                                                <td>{{ item.invoice_title }} <br><small ng-bind-html="item.description"></small></td>
                                                <td ng-bind-html="item.price_human"></td>
                                                <td>{{ item.quantity }}</td>
                                                <td class="text-right" ng-bind-html="item.subtotal_human"></td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                        <!-- /LineItems -->

                        <!-- Total -->
                        <div class="row">
                            <div class="col-md-6">

                            </div>
                            <div class="col-md-6">
                                <div class="table-responsive">
                                    <table class="table table-striped">
                                        <tbody>
                                            <tr>
                                                <td><?= __d('admin', 'Subtotal') ?></td>
                                                <td class="text-right" ng-bind-html="data.content.subtotal_human"></td>
                                            </tr>
                                            <tr ng-if="data.content.points_down.discount_human">
                                                <td><?= __d('admin', 'Puntos de descuento') ?> ({{ data.content.points_down.points }})</td>
                                                <td class="text-right" ng-cloak>- {{ data.content.points_down.discount_human }}</td>
                                            </tr>
                                            <tr ng-if="data.content.express_delivery">
                                                <td><?= __d('admin', 'Envío exprés') ?></td>
                                                <td class="text-right" ng-cloak ng-bind-html="data.content.shipping_handling_human"></td>
                                            </tr>
                                            <tr ng-if="data.content.no_has_coupon == false">
                                                <td><?= __d('admin', 'Cupón de descuento') ?> {{ data.content.coupon_name }}</td>
                                                <td ng-if="data.content.coupon_discount > 0" class="text-right" ng-cloak ng-bind-html="'- ' + data.content.coupon_discount_human"></td>
                                                <td ng-if="data.content.coupon_discount == 0" class="text-right"></td>
                                            </tr>
                                            <tr ng-if="data.content.promo_discount > 0">
                                                <td><?= __d('admin', 'Descuento') ?></td>
                                                <td class="text-right" ng-cloak ng-bind-html="'- ' + data.content.promo_discount_human"></td>
                                            </tr>
                                            <?php if (!StoreConfig::getConfig('notShowCarrier')) : ?>
                                                <tr ng-if="!data.content.express_delivery && !data.content.admin_custom_delivery">
                                                    <td><?= __d('admin', 'Envío estándar') ?></td>
                                                    <td class="text-right" ng-cloak ng-bind-html="data.content.shipping_handling_human"></td>
                                                </tr>
                                            <?php endif ?>
                                            <?= StoreConfig::getHook('orderViewAdminTotalsExtra', []) ?>
                                            <tr ng-if="data.content.grif_wrap">
                                                <td><?= __d('admin', 'EXTRA Regalo') ?></td>
                                                <td class="text-right" ng-cloak ng-bind-html="data.content.gift_wrapping_price_human"></td>
                                            </tr>
                                            <tr>
                                                <td><?= __d('admin', 'Impuestos') ?></td>
                                                <td class="text-right" ng-bind-html="data.content.total_taxes_human"></td>
                                            </tr>
                                            <tr ng-if="data.content.payment_price > 0">
                                                <td><?= __d('admin', 'Gastos de {0}', [
                                                        '{{ data.content.payment_method.title }}'
                                                    ]) ?></td>
                                                <td class="text-right" ng-cloak ng-bind-html="data.content.payment_price_human"></td>
                                            </tr>
                                            <tr>
                                                <td><?= __d('admin', 'Total') ?></td>
                                                <td class="text-right" ng-bind-html="data.content.total_human"></td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>

                        <div ng-if="data.content.comments">
                            <label>Observaciones</label>
                            <p>{{ data.content.comments }}</p>
                        </div>

                        <div ng-if="data.content.grif_wrap">
                            <label>ENVOLVER PARA REGALO</label>
                            <label>Mensaje del regalo</label>
                            <p>{{ data.content.user_message }}</p>

                            <label>Observaciones del regalo</label>
                            <p>{{ data.content.comments_gift }}</p>
                        </div>
                        <!-- /Total -->
                    </div>
                </div>


            </div>
            <!-- / Estado Pedido -->

            <div class="col-lg-4 col-md-12">
                <!-- Cliente -->
                <div class="ibox">
                    <div class="ibox-title">
                        <h5><i class="fa fa-user"></i> <?= __d('admin', 'Cliente') ?></h5>
                    </div>
                    <div class="ibox-content">
                        <div ng-if="data.content.user_id">
                            <label><?= __d('admin', 'Nombre') ?></label>
                            <p>{{ data.content.user.name }}</p>

                            <label><?= __d('admin', 'Correo electrónico') ?></label>
                            <p>{{ data.content.user.email }}</p>

                            <label><?= __d('admin', 'Fecha de registro') ?></label>
                            <p>{{ data.content.user.created | date }}</p>

                            <label><?= __d('admin', 'Grupo') ?></label>
                            <p>{{ data.content.user.group.name }}</p>
                            <div class="m-b"><a href="#/admin/store/users/view/{{ data.content.user.id }}"><i class="fa fa-eye"></i> <?= __d('admin', 'Ver ficha') ?></a></div>
                        </div>
                        <div ng-if="!data.content.user_id" class="m-b-2">
                            No registrado
                        </div>


                        <div class="tabs-container">
                            <tabset>
                                <tab heading="<?= __d('admin', 'Dirección de facturación') ?>">
                                    <div class="panel-body">
                                        <crud-field ng-init="crudConfig = data.crudConfig; content = data.content" field="adr_invoice_firstname"></crud-field>
                                        <?php if (!StoreConfig::getConfig('onlyFirstname')) : ?>
                                            <crud-field ng-init="crudConfig = data.crudConfig; content = data.content" field="adr_invoice_lastname"></crud-field>
                                        <?php endif ?>
                                        <crud-field ng-init="crudConfig = data.crudConfig; content = data.content" field="adr_invoice_company"></crud-field>
                                        <crud-field ng-init="crudConfig = data.crudConfig; content = data.content" field="adr_invoice_vat_number"></crud-field>
                                        <crud-field ng-init="crudConfig = data.crudConfig; content = data.content" field="adr_invoice_address"></crud-field>
                                        <crud-field ng-init="crudConfig = data.crudConfig; content = data.content" field="adr_invoice_postcode"></crud-field>
                                        <crud-field ng-init="crudConfig = data.crudConfig; content = data.content" field="adr_invoice_city"></crud-field>
                                        <crud-field ng-init="crudConfig = data.crudConfig; content = data.content" field="adr_invoice_state_id"></crud-field>
                                        <crud-field ng-init="crudConfig = data.crudConfig; content = data.content" field="adr_invoice_country_id"></crud-field>
                                        <crud-field ng-init="crudConfig = data.crudConfig; content = data.content" field="adr_invoice_email"></crud-field>
                                        <crud-field ng-init="crudConfig = data.crudConfig; content = data.content" field="adr_invoice_phone"></crud-field>
                                    </div>
                                </tab>
                                <?php if (!StoreConfig::getConfig('notShowCarrier')) : ?>
                                    <tab heading="<?= __d('admin', 'Dirección de envío') ?>">
                                        <div class="panel-body">
                                            <crud-field ng-init="crudConfig = data.crudConfig; content = data.content" field="adr_delivery_firstname"></crud-field>
                                            <?php if (!StoreConfig::getConfig('onlyFirstname')) : ?>
                                                <crud-field ng-init="crudConfig = data.crudConfig; content = data.content" field="adr_delivery_lastname"></crud-field>
                                            <?php endif ?>
                                            <crud-field ng-init="crudConfig = data.crudConfig; content = data.content" field="adr_delivery_company"></crud-field>
                                            <crud-field ng-init="crudConfig = data.crudConfig; content = data.content" field="adr_delivery_vat_number"></crud-field>
                                            <crud-field ng-init="crudConfig = data.crudConfig; content = data.content" field="adr_delivery_address"></crud-field>
                                            <crud-field ng-init="crudConfig = data.crudConfig; content = data.content" field="adr_delivery_postcode"></crud-field>
                                            <crud-field ng-init="crudConfig = data.crudConfig; content = data.content" field="adr_delivery_city"></crud-field>
                                            <crud-field ng-init="crudConfig = data.crudConfig; content = data.content" field="adr_delivery_state_id"></crud-field>
                                            <crud-field ng-init="crudConfig = data.crudConfig; content = data.content" field="adr_delivery_country_id"></crud-field>
                                            <crud-field ng-init="crudConfig = data.crudConfig; content = data.content" field="adr_delivery_phone"></crud-field>
                                        </div>
                                    </tab>
                                <?php endif ?>
                            </tabset>
                        </div>
                    </div>
                </div>
                <!-- /Cliente -->
                <!-- Forma de pago -->
                <div class="ibox">
                    <div class="ibox-title">
                        <h5><i class="fa fa-user"></i> <?= __d('admin', 'Forma de pago') ?></h5>
                    </div>
                    <div class="ibox-content">
                        {{ data.content.payment_method.title }}
                    </div>
                </div>
                <!-- /Forma de pago -->
            </div>


        </div>

        <div class="row">
            <div class="col-md-12">

            </div>
        </div>
    </div>
</div>