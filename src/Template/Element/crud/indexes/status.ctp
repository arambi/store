<span class="badge order-status" ng-class="{
  'badge-warning order-status-pending': content.status == 'pending',
  'badge-info order-status-accepted': content.status == 'accepted',
  'badge-info order-status-waiting_carrier': content.status == 'waiting_carrier',
  'badge-primary order-status-envoy': content.status == 'envoy',
  'badge-primary order-status-received': content.status == 'received',
  'badge-danger order-status-canceled': content.status == 'canceled',
  'badge-warning pp1 order-status-waiting_pay': content.status == 'waiting_pay',
  'badge-warning pp2 order-status-returned_pendent': content.status == 'returned_pendent',
  'badge-danger order-status-returned': content.status == 'returned'
}">{{ content.status_human }}</span>
