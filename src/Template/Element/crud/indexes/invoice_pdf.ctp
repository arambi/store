<a target="_blank" href="{{ content.invoice_url }}" class="btn btn-primary btn-outline btn-xs">
  <i class="fa fa-file-pdf-o"></i> <?= __d( 'admin', 'PDF') ?>
</a>

<a href="#/admin/store/orders/view/{{ content.order_id }}" class="btn btn-primary btn-outline btn-xs">
  <i class="fa fa-shopping-cart"></i> <?= __d( 'app', 'Pedido') ?>
</a>

<?php if( \Store\Config\StoreConfig::getConfig('hasInvoiceRefund')): ?>
  <span store-refund-invoice="content.order_id" ng-if="data.crudConfig.navigation.controller == 'Invoices'" class="btn btn-primary btn-outline btn-xs">
    <i class="fa fa-shopping-cart"></i> <?= __d( 'app', 'Crear factura de devolución') ?>
  </span>
<?php endif ?>