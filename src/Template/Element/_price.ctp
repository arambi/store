<div class="form-group" ng-class="field.error ? 'has-error' : false">
  <label ng-help tooltip="{{ field.help }}" class="col-lg-3 control-label">{{ field.label }}</label>
  <div class="col-lg-9">
    <input enter-numbers allow-negative="false" allow-decimal="true" decimal-upto="5" ng-model="content[field.key]" type="text" class="form-control">
    <span class="form-error" ng-if="field.error" ng-repeat="error in field.error">{{ error }}</span>
  </div>
</div>

<div class="form-group" ng-class="field.error ? 'has-error' : false">
  <label ng-help tooltip="{{ field.help }}" class="col-lg-3 control-label">{{ field.label }} <?= __d( 'admin', 'con I.V.A.') ?></label>
  <div class="col-lg-9">
    <input enter-numbers allow-negative="false" allow-decimal="true" decimal-upto="5" shop-price shop-price-ref="<?= $priceScope ?>" shop-price-tax="<?= $priceTaxScope ?>" price_with_taxes="price_with_taxes" ng-model="price_with_taxes" type="text" class="form-control">
  </div>
</div>