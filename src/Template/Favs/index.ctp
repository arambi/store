<div class="crumbs row" id="default-crumbs">

    <a id="nav-open" class="nav-button" href="#">
      <i class="fa fa-bars"></i><span class="none"><?= __d( 'app', 'Expandir menú') ?></span>
    </a>


</div><!-- .row -->

<div class="cart-container bg-greymed clearfix">

  <div class="row">
    
    <div class="cart-header clearfix">

      <h4 class="grid_5 alpha upper greymed"><?= __d( 'app', 'Mi lista de deseos') ?></h4>

      <div class="grid_7 text-right omega">
        <span class="greymed"><?= __d( 'app', 'Tu lista de deseos contiene {0} producto(s)', [count( $products)]) ?></span>
      </div>

    </div>


  </div><!-- .row -->

  <div class="row cart-table ">

    <? foreach( $products as $key => $product): ?>
      <div id="fav-<?= $product->id ?>" class="shop-product-list <?= ( ( $key + 1) %3 == 0) ? 'grid_4 omega' : 'grid_4' ?>">
        
        <?= $this->ShopProduct->deleteFav( $product, [
            'content' => '<i class="fa fa-remove"></i>',
            'class' => '',
            'ref' => '#fav-'. $product->id
        ]) ?>

        <div class="shop-image-ct">
          <div class="shop-image">
            <!-- Añadir enlace a esta imagen -->
            <a href="<?= $this->ShopProduct->url( $product->categories_shops [0], $product) ?>"><?= $this->ShopProduct->imageIndex( $product) ?></a> 
          </div>
        </div>

        <div class="shop-content-ct">

          <div class="shop-name">
            <?= $this->ShopProduct->link( $product->categories_shops [0], $product) ?>
          </div>

          <div class="shop-price">
            <?= $this->ShopProduct->price( $product) ?>
          </div>

        </div><!-- .shop-content-ct -->

      </div><!-- .product -->
    <? endforeach ?>

  </div><!-- .row -->

</div><!-- .cart-container -->



