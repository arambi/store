<?php

namespace Store\Correos;

use Exception;
use Cake\Core\Configure;
use Cake\ORM\TableRegistry;
use CorreosSdk\Factories\Address;
use CorreosSdk\Factories\Identification;
use CorreosSdk\CorreosConnector\CorreosConfig;
use CorreosSdk\Factories\SenderUnitedIdentity;
use Website\Lib\Website;

class CorreosReturn
{

    private $lineItemId;

    /**
     * @var \Store\Model\Entity\LineItem $lineItem
     */
    private $lineItem;

    /**
     * @var \Store\Model\Entity\Order $order
     */
    private $order;

    /**
     * @var \CorreosSdk\CorreosConnector\CorreosConfig $client
     */
    private $correosConfig;

    /**
     * @var array $config;
     */
    private $config;

    /**
     * @var array $data;
     */
    private $data;

    public function __construct($lineItemId)
    {
        $this->lineItemId = $lineItemId;
        $this->setLineItem();

        if (!Configure::read('Correos')) {
            throw new Exception("No está configurado Correos", 500);
        }

        $this->config = Configure::read('Correos');
        $this->setData();
    }

    private function setData()
    {
        $this->setSender();
        $this->setReceiver();
    }

    public function save()
    {
        $table = TableRegistry::getTableLocator()->get('Store.CorreosPreregistro');
        $entity = $table->newEntity($this->data);
        $table->save($entity);
    }

    public function setLineItem()
    {
        $this->lineItem = TableRegistry::getTableLocator()->get('Store.LineItems')
            ->find()
            ->contain('Orders')
            ->where([
                'LineItems.id' => $this->lineItemId,
            ])
            ->first();

        $this->order = $this->lineItem->order;
        $this->data['line_item_id'] = $this->lineItem->id;
        $this->data['order_id'] = $this->order->id;
    }

    public function setOrder(\Store\Model\Entity\Order $order)
    {
        $this->order = $order;
        return $this;
    }

    private function setSender()
    {
        $this->data['sender_city_name'] = $this->order->adr_delivery_city;
        $this->data['sender_street_name'] = $this->order->adr_delivery_address;
        $this->data['sender_province_name'] = $this->order->state_delivery->title;
        $this->data['sender_name'] = $this->order->adr_delivery_firstname .' '. $this->order->adr_delivery_lastname;
        $this->data['sender_postcode'] = $this->order->adr_delivery_postcode;
        $this->data['sender_phone'] = preg_replace('/\D+/', '', $this->order->adr_delivery_phone);
        $this->data['sender_email'] = $this->order->order_email;
        $this->data['sender_country'] = $this->order->country_delivery->iso_code;
    }

    private function setReceiver()
    {
        if (isset($this->config['receiver'])) {
            foreach ($this->config['receiver'] as $key => $value) {
                $this->data[$key] = $value;
            }
        } else {
            $this->data['receiver_city_name'] = $this->order->adr_store_city;
            $this->data['receiver_street_name'] = $this->order->adr_store_address;
            $this->data['receiver_province_name'] = $this->order->adr_store_state;
            $this->data['receiver_name'] = $this->order->adr_store_company;
            $this->data['receiver_postcode'] = $this->order->adr_store_postcode;
            $this->data['receiver_phone'] = preg_replace('/\D+/', '', $this->order->adr_store_phone);
            $this->data['receiver_email'] = Website::get('email');
        }
    }
}
