<?php

namespace Store\Mailer;

use Cake\Mailer\Mailer;
use Website\Lib\Website;
use Letter\Collector\LetterCollector;
use Letter\Mailer\LetterTrait;

/**
 * Correos electrónicos enviados para acciones generales de usuarios
 * Aquí estará solo aquellas que no sean particulares de otros plugins
 */

class ShopMailer extends Mailer
{
  use LetterTrait;

  public function user_add_points( $user, $points, $total_points)
  {
    $this->letter = LetterCollector::get( 'Store.addPoints', [
      'store_name' => Website::get( 'title'),
      'points' => $points,
      'name' => $user->name,
      'total_points' => $total_points
    ]);
    $this->_setLetterParams();

    $this->_email->from( Website::get( 'settings.users_reply_email'), Website::get( 'title'));
    $this->_email->to( $user->email);
  }

  public function user_add_points_order( $user, $points, $total_points)
  {
    $this->letter = LetterCollector::get( 'Store.addPointsOrder', [
      'store_name' => Website::get( 'title'),
      'points' => $points,
      'name' => $user->name,
      'total_points' => $total_points
    ]);
    $this->_setLetterParams();

    $this->_email->from( Website::get( 'settings.users_reply_email'), Website::get( 'title'));
    $this->_email->to( $user->email);
  }

}