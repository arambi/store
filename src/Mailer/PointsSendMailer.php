<?php
namespace Store\Mailer;

use Cake\Mailer\Mailer;
use Cake\ORM\TableRegistry;
use Website\Lib\Website;

/**
 * PointsSend mailer.
 */
class PointsSendMailer extends Mailer
{

  /**
   * Mailer's name.
   *
   * @var string
   */
  static public $name = 'PointsSend';

  public function sending( $send)
  {
    $this->PointsAddsSends = TableRegistry::get( 'Store.PointsAddsSends');

    $this->_email
      ->transport( 'default')
      ->subject( $send->points_add->subject)
      ->from( Website::get( 'settings.users_reply_email'), Website::get( 'title'))
      ->emailFormat('html')
      ->to( $send->email)
      ->template( 'Letter.view', Website::get( 'theme') .'.default')
    ;


    $body = str_replace( ['{name}', '{points}'], [$send->user->name, $send->points_add->data->points], $send->points_add->body);
    $this->_email
      ->set( 'body', $body);

    
    
    

  }
}
