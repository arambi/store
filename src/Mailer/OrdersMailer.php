<?php

namespace Store\Mailer;

use Cake\Mailer\Mailer;
use Website\Lib\Website;
use Letter\Collector\LetterCollector;
use Letter\Mailer\LetterTrait;
use Section\Routing\RouteData;
use Letter\Mailer\QueueMailer;
use Cake\Routing\Router;
use Store\Config\StoreConfig;

/**
 * Correos electrónicos enviados para acciones generales de usuarios
 * Aquí estará solo aquellas que no sean particulares de otros plugins
 */

class OrdersMailer extends QueueMailer
{
    use LetterTrait;


    /**
     * Envio de mail para el estado "Pendiente"
     * 
     * @param  Store\Model\Entity\Order  $order     El pedido
     * @param  User\Model\Entity\User   $user      El usuario
     */
    public function statusPending($order, $user, $email_to = false)
    {
        $this->status($order, $user, 'Store.orderStatusPending', [], $email_to);
    }


    /**
     * Envio de mail para el estado "Aceptado"
     * 
     * @param  Store\Model\Entity\Order  $order     El pedido
     * @param  User\Model\Entity\User   $user      El usuario
     */
    public function statusAccepted($order, $user, $email_to = false)
    {
        $this->status($order, $user, 'Store.orderStatusAccepted', [], $email_to);
    }


    /**
     * Envio de mail para el estado "Enviado"
     * 
     * @param  Store\Model\Entity\Order  $order     El pedido
     * @param  User\Model\Entity\User   $user      El usuario
     */
    public function statusEnvoy($order, $user, $email_to = false)
    {
        $this->status($order, $user, 'Store.orderStatusEnvoy', [], $email_to);
    }


    /**
     * Envio de mail para el estado "Cancelado"
     * 
     * @param  Store\Model\Entity\Order  $order     El pedido
     * @param  User\Model\Entity\User   $user      El usuario
     */
    public function statusCanceled($order, $user, $email_to = false)
    {
        $this->status($order, $user, 'Store.orderStatusCanceled', [], $email_to);
    }


    /**
     * Envio de mail para el estado "Recibido"
     * 
     * @param  Store\Model\Entity\Order  $order     El pedido
     * @param  User\Model\Entity\User   $user      El usuario
     */
    public function statusReceived($order, $user, $email_to = false)
    {
        $this->status($order, $user, 'Store.orderStatusReceived', [], $email_to);
    }


    /**
     * Envio de mail para el estado "Espera de confirmación bancaria"
     * 
     * @param  Store\Model\Entity\Order  $order     El pedido
     * @param  User\Model\Entity\User   $user      El usuario
     */
    public function statusWaitingPay($order, $user, $email_to = false)
    {
        $this->status($order, $user, 'Store.orderStatusWaitingPay', [], $email_to);
    }


    /**
     * Envio de mail para el estado "Pendiente de devolución"
     * 
     * @param  Store\Model\Entity\Order  $order     El pedido
     * @param  User\Model\Entity\User   $user      El usuario
     */
    public function statusReturnedPendent($order, $user, $email_to = false)
    {
        $this->status($order, $user, 'Store.orderStatusReturnedPendent', [], $email_to);
    }


    /**
     * Envio de mail para el estado "Devuelto"
     * 
     * @param  Store\Model\Entity\Order  $order     El pedido
     * @param  User\Model\Entity\User   $user      El usuario
     */
    public function statusReturned($order, $user, $email_to = false)
    {
        $this->status($order, $user, 'Store.orderStatusReturned', [], $email_to);
    }

    /**
     * Se encarga de enviar el mail para el estado dado
     * 
     * @param  Store\Model\Entity\Order  $order     El pedido
     * @param  User\Model\Entity\User   $user      El usuario
     * @param  string $key       El key de Letter
     * @param  array  $extraVars array con las variables que se añadirán a las que hay por defecto en status()
     */
    private function status($order, $user, $key, $extraVars = [], $email_to = false)
    {
        $view = new \Cake\View\View();
        $invoice = $view->element(StoreConfig::getConfig('invoiceEmailElement'), ['order' => $order, 'user' => $user]);
        $vars = [
            'web_name' => Website::get('title'),
            'name' => $user ? $user->name : $order->adr_invoice_firstname,
            'order_ref' => $order->id,
            'invoice_url' => Router::url($order->invoice_url, true),
            'order_url' => Router::url($order->order_url, true),
        ];

        $this->_email->set('invoice', $invoice);
        $vars = array_merge($vars, $extraVars);

        $this->letter = LetterCollector::get($key, $vars);
        $email_from = $email_admin = Website::get('settings.store_order_email_replay');

        if (empty($email_from)) {
            $email_from = Website::get('settings.users_reply_email');
        }

        $this->_setLetterParams();
        $email = $email_to ? $email_to : ($user ? $user->email : $order->adr_invoice_email);
        $email = trim($email);
        $this->_email->setFrom($email_from, Website::get('title'));
        $this->_email->setTo($email);
    }


    public function newOrder($order, $user, $email_admin)
    {
        $view = new \Cake\View\View();
        $invoice = $view->element(StoreConfig::getConfig('invoiceEmailElement'), [
            'order' => $order,
            'is_admin' => true
        ]);

        $vars = [
            'web_name' => Website::get('title'),
            'order_ref' => $order->id,
            'invoice_url' => Router::url($order->invoice_url, true),
            'order_url' => Router::url($order->order_url, true),
        ];

        $this->_email->set('invoice', $invoice);
        $this->_email->set('is_admin', true);

        $this->letter = LetterCollector::get('Store.newOrder', $vars);

        $this->_setLetterParams();
        $this->_email->setFrom(Website::get('settings.users_reply_email'), Website::get('title'));
        $this->_email->setTo($email_admin);
    }

    // Items
    private function itemStatus($refund, $email, $key)
    {
        $this->letter = LetterCollector::get($key, [
            'web_name' => Website::get('title'),
            'order_ref' => $refund->order_id,
            'order_url' => Router::url($refund->order->order_url, true),
            'items' => $this->getRefundItems($refund),
        ]);

        $this->_setLetterParams();
        $this->_email->setFrom(Website::get('settings.users_reply_email'), Website::get('title'));
        $this->_email->setTo($email);
    }

    public function refundRefundReview($refund, $email)
    {
        $this->itemStatus($refund, $email, 'Store.item_refunded_request');
    }

    public function refundRefundReviewAdmin($refund, $email)
    {
        $this->itemStatus($refund, $email, 'Store.item_refunded_request_admin');
    }

    public function refundRefundProccess($refund, $email)
    {
        $this->itemStatus($refund, $email, 'Store.item_refunded_working');
    }

    public function refundRefundProccessAdmin($refund, $email)
    {
        $this->itemStatus($refund, $email, 'Store.item_refunded_working_admin');
    }


    public function refundRefundSent($refund, $email)
    {
        $this->itemStatus($refund, $email, 'Store.item_refunded_send');
    }

    public function refundRefundSentAdmin($refund, $email)
    {
        $this->itemStatus($refund, $email, 'Store.item_refunded_send_admin');
    }

    public function refundRefundReceived($refund, $email)
    {
        $this->itemStatus($refund, $email, 'Store.item_refunded_received');
    }

    public function refundRefundReceivedAdmin($refund, $email)
    {
        $this->itemStatus($refund, $email, 'Store.item_refunded_received_admin');
    }

    public function refundRefundFinalized($refund, $email)
    {
        $this->itemStatus($refund, $email, 'Store.item_refunded_received');
    }

    public function refundRefundFinalizedAdmin($refund, $email)
    {
        $this->itemStatus($refund, $email, 'Store.item_refunded_received_admin');
    }

    public function refundRefundedInvoice($invoice, $refund, $email)
    {
        $this->letter = LetterCollector::get('Store.item_refunded_invoice', [
            'web_name' => Website::get('title'),
            'invoice_url' => $invoice->invoice_url,
        ]);

        $this->_setLetterParams();
        $this->_email->setFrom(Website::get('settings.users_reply_email'), Website::get('title'));
        $this->_email->setTo($email);
    }

    public function refundRefunded($invoice, $refund, $email)
    {
        $this->letter = LetterCollector::get('Store.item_refunded_invoice', [
            'web_name' => Website::get('title'),
            'invoice_url' => $invoice->invoice_url,
        ]);

        $this->_setLetterParams();
        $this->_email->setFrom(Website::get('settings.users_reply_email'), Website::get('title'));
        $this->_email->setTo($email);
    }

    public function itemRefundedInvoiceAdmin($invoice, $refund, $email)
    {
        $this->letter = LetterCollector::get('Store.item_refunded_invoice_admin', [
            'web_name' => Website::get('title'),
            'invoice_url' => $invoice->invoice_url,
        ]);

        $this->_setLetterParams();
        $this->_email->setFrom(Website::get('settings.users_reply_email'), Website::get('title'));
        $this->_email->setTo($email);
    }

    private function getRefundItems($content)
    {
        $style = 'border-bottom: 1px solid #e7e7e7; text-align: left; padding: 10px 10px';
        $t_quantity = __d( 'app', 'Cantidad');
        $t_products = __d( 'app', 'Producto');
        $t_price = __d( 'app', 'Precio');
        $t_status = __d( 'app', 'Estado');
        $body = <<<EOF
    <table bgcolor="#ffffff" style="font-size: 14px; width: 100%; "  cellpadding="0" cellspacing="0" border="0" align="center">
      <tr>
        <th style="$style">$t_products</th>
        <th style="$style">$t_quantity</th>
        <th style="$style">$t_price</th>
        <th style="$style">$t_status</th>
      </tr>
EOF;

        foreach ($content->refunds_items as $item) {
            $status = StoreConfig::itemStatusTitle($item->status);
            $body .= <<<EOF
            <tr>
                <td style="$style">{$item->line_item->title}</td>
                <td style="$style">{$item->quantity}</td>
                <td style="$style">{$item->line_item->price_human}</td>
                <td style="$style">{$status}</td>
            </tr>
EOF;
        }

        $body .= <<<EOF
    </table>
EOF;

        return $body;
    }

    public function giftcard($order, $item, $email)
    {
        $this->letter = LetterCollector::get('Store.giftcard', [
            'code' => $item->giftcard_code,
            'name_to' => $item->giftcard_name,
            'name_from' => $order->user ? $order->user->name : $order->adr_invoice_firstname,
            'pdf_button' => $this->giftPDFButton($item),
            'price' => $item->price_human
        ]);

        $this->_setLetterParams();
        $this->_email->setFrom(Website::get('settings.users_reply_email'));
        $this->_email->setTo($email);
    }

    public function stockZero($products, $email)
    {
        $this->letter = LetterCollector::get('Store.stock_zero', [
            'products' => implode('<br>', $products)
        ]);

        $this->_setLetterParams();
        $this->_email->setFrom(Website::get('settings.users_reply_email'));
        $this->_email->setTo($email);
    }

    private function giftPDFButton($item)
    {
        $url = Router::url([
            'plugin' => Website::get('theme'),
            'controller' => 'Products',
            'action' => 'giftcard',
            'salt' => $item->giftcard_hash
        ], true);

        $text = __d('app', 'Descargar PDF');

        $html = <<<EOF
        <table border="0" cellpadding="0" cellspacing="0" style="background-color:#e75952; border-radius:100px;">
            <tr>
                <td align="center" valign="middle" style="color:#FFFFFF; font-size:16px; font-weight:bold; letter-spacing:-.5px; line-height:150%; padding-top:15px; padding-right:30px; padding-bottom:15px; padding-left:30px;">
                <a href="$url" target="_blank" style="color:#FFFFFF; text-decoration:none;">$text</a>
                </td>
            </tr>
        </table>

EOF;
        return $html;
    }
}
