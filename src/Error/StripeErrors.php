<?php
namespace Store\Error;

use Cake\Core\Configure;
use I18n\Lib\Lang;

/**
* incorrect_number  The card number is incorrect.
* invalid_number  The card number is not a valid credit card number.
* invalid_expiry_month  The card's expiration month is invalid.
* invalid_expiry_year   The card's expiration year is invalid.
* invalid_cvc   The card's security code is invalid.
* expired_card  The card has expired.
* incorrect_cvc   The card's security code is incorrect.
* incorrect_zip   The card's zip code failed validation.
* card_declined   The card was declined.
* missing   There is no card on a customer that is being charged.
* processing_error  An error occurred while processing the card.
*/
class StripeErrors
{
  
  static $errors = array(
      'incorrect_number' => array(
          'eng' => 'The card number is incorrect',
          'spa' => 'El número de tarjeta es incorrecto',
          'fre' => 'Le numéro de carte est incorrect',
      ),
      'invalid_number' => array(
          'eng' => 'The card number is not a valid credit card number',
          'spa' => 'El número de tarjeta no es un número válido',
          'fre' => "Le numéro de carte n'est pas un numéro de carte de crédit en cours de validité",
      ),
      'invalid_expiry_month' => array(
          'eng' => "The card's expiration month is invalid",
          'spa' => 'El mes de vencimiento de la tarjeta no es válido',
          'eng' => "Expiration le mois de la carte n'est pas valide",
      ),
      'invalid_expiry_year' => array(
          'eng' => "The card's expiration year is invalid",
          'spa' => "El año de vencimiento de la tarjeta no es válido",
          'fre' => "L'année d'expiration de la carte n'est pas valide",
      ),
      'invalid_cvc' => array(
          'eng' => "The card's security code is invalid",
          'spa' => "El código de seguridad de la tarjeta no es válido",
          'fre' => "Le code de sécurité de la carte n'est pas valide",
      ),
      'expired_card' => array(
          'eng' => "The card has expired",
          'spa' => "La tarjeta ha caducado",
          'fre' => "La carte est expirée",
      ),
      'incorrect_cvc' => array(
          'eng' => "The card's security code is incorrect",
          'spa' => "El código de seguridad es incorrecto",
          'fre' => "Le code de sécurité de la carte est incorrecte",
      ),
      'incorrect_zip' => array(
          'eng' => "The card's zip code failed validation",
          'spa' => "El código postal de la tarjeta no pasó la validación",
          'fre' => "Le code postal de la carte échoué à la validation",
      ),
      'card_declined' => array(
          'eng' => "The card was declined",
          'spa' => "La tarjeta ha sido rechazada",
          'fre' => "La carte a été refusée",
      ),
      'missing' => array(
          'eng' => "There is no card on a customer that is being charged",
          'spa' => "No hay ninguna tarjeta de un cliente que se esté usando",
          'fre' => "Il n'y a aucune carte sur un client qui est en charge",
      ),
      'processing_error' => array(
          'eng' => "An error occurred while processing the card",
          'spa' => "Se ha producido un error al procesar la tarjeta",
          'fre' => "Une erreur s'est produite lors du traitement de la carte",
      ),
      'invalid_request_error' => array(
          'eng' => "An error occurred while processing the card",
          'spa' => "Se ha producido un error al procesar la tarjeta",
          'fre' => "Une erreur s'est produite lors du traitement de la carte",
      ),
      'undefined' => array(
          'eng' => "An error occurred while processing the card",
          'spa' => "Se ha producido un error al procesar la tarjeta",
          'fre' => "Une erreur s'est produite lors du traitement de la carte",
      )
  );
  
  
  public function getError( $error)
  {
    if( isset( $error ['error']['code']))
    {
      $key = $error ['error']['code'];
    }
    elseif( isset( $error ['error']['type']))
    {
      $key = $error ['error']['type'];
    }
    else
    {
      $key = $error;
    }
    
    $errors = self::$errors;
    $locale = Lang::current( 'iso3') ? Lang::current( 'iso3') : 'eng';
    if( isset( $errors [$key][$locale]))
    {
      return $errors [$key][$locale];
    }
    else
    {
      return $errors [$key]['eng'];
    }
  }
}


