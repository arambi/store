<?php
namespace Store\Test\Fixture;

use Cake\TestSuite\Fixture\TestFixture;

/**
 * PromosFixture
 *
 */
class PromosFixture extends TestFixture
{

    /**
     * Table name
     *
     * @var string
     */
    public $table = 'store_promos';

    /**
     * Fields
     *
     * @var array
     */
    // @codingStandardsIgnoreStart
    public $fields = [
        'id' => ['type' => 'integer', 'length' => 11, 'unsigned' => false, 'null' => false, 'default' => null, 'comment' => '', 'autoIncrement' => true, 'precision' => null],
        'title' => ['type' => 'string', 'length' => 255, 'null' => false, 'default' => null, 'comment' => '', 'precision' => null, 'fixed' => null],
        'start_on' => ['type' => 'date', 'length' => null, 'null' => false, 'default' => null, 'comment' => '', 'precision' => null],
        'finish_on' => ['type' => 'date', 'length' => null, 'null' => false, 'default' => null, 'comment' => '', 'precision' => null],
        'active' => ['type' => 'boolean', 'length' => null, 'null' => false, 'default' => '0', 'comment' => '', 'precision' => null],
        'value' => ['type' => 'float', 'length' => null, 'precision' => null, 'unsigned' => false, 'null' => true, 'default' => null, 'comment' => ''],
        'promo_type' => ['type' => 'text', 'length' => null, 'null' => false,  'comment' => '', 'precision' => null],
        'discount_type' => ['type' => 'text', 'length' => null, 'null' => false,  'comment' => '', 'precision' => null],
        'salt' => ['type' => 'string', 'length' => 255, 'null' => true, 'default' => null, 'comment' => '', 'precision' => null, 'fixed' => null],
        'created' => ['type' => 'datetime', 'length' => null, 'null' => false, 'default' => null, 'comment' => '', 'precision' => null],
        'modified' => ['type' => 'datetime', 'length' => null, 'null' => false, 'default' => null, 'comment' => '', 'precision' => null],
        '_indexes' => [
            'active' => ['type' => 'index', 'columns' => ['active'], 'length' => []],
            'start_on' => ['type' => 'index', 'columns' => ['start_on'], 'length' => []],
            'finish_on' => ['type' => 'index', 'columns' => ['finish_on'], 'length' => []],
        ],
        '_constraints' => [
            'primary' => ['type' => 'primary', 'columns' => ['id'], 'length' => []],
        ],
        '_options' => [
            'engine' => 'InnoDB',
            'collation' => 'utf8_general_ci'
        ],
    ];
    // @codingStandardsIgnoreEnd

    /**
     * Records
     *
     * @var array
     */
    public $records = [
      [
        'id' => 1,
        'title' => 'Verano',
        'start_on' => '2015-08-15',
        'finish_on' => '2015-09-15',
        'active' => 1,
        'value' => 5,
        'promo_type' => 'categories',
        'discount_type' => 'fixed',
        'salt' => 'Lorem ipsum dolor sit amet',
        'created' => '2015-08-15 08:37:59',
        'modified' => '2015-08-15 08:37:59'
      ],
      [
        'id' => 2,
        'title' => 'Invierno',
        'start_on' => '2015-08-15',
        'finish_on' => '2015-09-15',
        'active' => 1,
        'value' => 5,
        'promo_type' => 'products',
        'discount_type' => 'percent',
        'salt' => 'Lorem ipsum dolor sit amet',
        'created' => '2015-08-15 08:37:59',
        'modified' => '2015-08-15 08:37:59'
      ],
      [
        'id' => 3,
        'title' => 'Otoño',
        'start_on' => '2015-08-15',
        'finish_on' => '2015-09-15',
        'active' => 1,
        'value' => 10,
        'promo_type' => 'global',
        'discount_type' => 'fixed',
        'salt' => 'Lorem ipsum dolor sit amet',
        'created' => '2015-08-15 08:37:59',
        'modified' => '2015-08-15 08:37:59'
      ],
    ];
}
