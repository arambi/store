<?php
namespace Store\Test\Fixture;

use Cake\TestSuite\Fixture\TestFixture;

/**
 * AttributesFixture
 *
 */
class AttributesFixture extends TestFixture
{

    /**
     * Table name
     *
     * @var string
     */
    public $table = 'store_attributes';

    /**
     * Fields
     *
     * @var array
     */
    // @codingStandardsIgnoreStart
    public $fields = [
        'id' => ['type' => 'integer', 'length' => 11, 'unsigned' => false, 'null' => false, 'default' => null, 'comment' => '', 'autoIncrement' => true, 'precision' => null],
        'title' => ['type' => 'string', 'length' => 255, 'null' => true, 'default' => null, 'collate' => 'utf8_general_ci', 'comment' => '', 'precision' => null, 'fixed' => null],
        'attribute_group_id' => ['type' => 'integer', 'length' => 11, 'unsigned' => false, 'null' => true, 'default' => null, 'comment' => '', 'precision' => null, 'autoIncrement' => null],
        'color' => ['type' => 'string', 'length' => 32, 'null' => true, 'default' => null, 'collate' => 'utf8_general_ci', 'comment' => '', 'precision' => null, 'fixed' => null],
        'salt' => ['type' => 'string', 'length' => 255, 'null' => true, 'default' => null, 'collate' => 'utf8_general_ci', 'comment' => '', 'precision' => null, 'fixed' => null],
        'created' => ['type' => 'datetime', 'length' => null, 'null' => false, 'default' => null, 'comment' => '', 'precision' => null],
        'modified' => ['type' => 'datetime', 'length' => null, 'null' => false, 'default' => null, 'comment' => '', 'precision' => null],
        '_indexes' => [
            'attribute_group_id' => ['type' => 'index', 'columns' => ['attribute_group_id'], 'length' => []],
        ],
        '_constraints' => [
            'primary' => ['type' => 'primary', 'columns' => ['id'], 'length' => []],
        ],
        '_options' => [
            'engine' => 'InnoDB',
            'collation' => 'utf8_general_ci'
        ],
    ];
    // @codingStandardsIgnoreEnd

    /**
     * Records
     *
     * @var array
     */
    public $records = [
        [
            'id' => 1,
            'title' => null,
            'attribute_group_id' => 1,
            'color' => '#e82525',
            'salt' => '520de92309550ce90e7f16a529ac6346765c7ccf',
            'created' => '2017-09-22 13:28:07',
            'modified' => '2017-09-25 10:21:26'
        ],
        [
            'id' => 2,
            'title' => null,
            'attribute_group_id' => 1,
            'color' => '#3a30ab',
            'salt' => '8891d25106fc13492562e210ee411fa02578496f',
            'created' => '2017-09-22 13:28:18',
            'modified' => '2017-09-25 10:21:26'
        ],
        [
            'id' => 3,
            'title' => null,
            'attribute_group_id' => 1,
            'color' => '#15854f',
            'salt' => 'ac9afee8ada65b2ef31f99c8c3344f9ab413d315',
            'created' => '2017-09-22 13:28:26',
            'modified' => '2017-09-25 10:21:26'
        ],
        [
            'id' => 4,
            'title' => null,
            'attribute_group_id' => 1,
            'color' => '#b173f0',
            'salt' => 'a56da94b23d484d1f22b3a797e79642deb58a71a',
            'created' => '2017-09-22 13:28:38',
            'modified' => '2017-09-22 19:22:18'
        ],
        [
            'id' => 5,
            'title' => null,
            'attribute_group_id' => 2,
            'color' => '',
            'salt' => 'd35e3776c6a04762c0703d6e8423a13591f984c5',
            'created' => '2017-09-22 14:06:58',
            'modified' => '2017-09-25 10:21:26'
        ],
    ];
}
