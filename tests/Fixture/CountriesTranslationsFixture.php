<?php
namespace Store\Test\Fixture;

use Cake\TestSuite\Fixture\TestFixture;

/**
 * CountriesTranslationsFixture
 *
 */
class CountriesTranslationsFixture extends TestFixture
{

    /**
     * Table name
     *
     * @var string
     */
    public $table = 'store_countries_translations';

    /**
     * Fields
     *
     * @var array
     */
    // @codingStandardsIgnoreStart
    public $fields = [
        'id' => ['type' => 'integer', 'length' => 11, 'unsigned' => false, 'null' => false, 'default' => null, 'comment' => '', 'precision' => null, 'autoIncrement' => null],
        'locale' => ['type' => 'string', 'length' => 5, 'null' => false, 'default' => '', 'collate' => 'utf8_general_ci', 'comment' => '', 'precision' => null, 'fixed' => null],
        'title' => ['type' => 'string', 'length' => 255, 'null' => true, 'default' => null, 'collate' => 'utf8_general_ci', 'comment' => '', 'precision' => null, 'fixed' => null],
        '_constraints' => [
            'primary' => ['type' => 'primary', 'columns' => ['id', 'locale'], 'length' => []],
        ],
        '_options' => [
            'engine' => 'InnoDB',
            'collation' => 'utf8_general_ci'
        ],
    ];
    // @codingStandardsIgnoreEnd

    /**
     * Records
     *
     * @var array
     */
    public $records = [
        [
            'id' => 1,
            'locale' => 'baq',
            'title' => 'Germany'
        ],
        [
            'id' => 1,
            'locale' => 'eng',
            'title' => 'Germany'
        ],
        [
            'id' => 1,
            'locale' => 'fre',
            'title' => 'Germany'
        ],
        [
            'id' => 1,
            'locale' => 'spa',
            'title' => 'Alemania'
        ],
        [
            'id' => 2,
            'locale' => 'baq',
            'title' => 'Austria'
        ],
        [
            'id' => 2,
            'locale' => 'eng',
            'title' => 'Austria'
        ],
        [
            'id' => 2,
            'locale' => 'fre',
            'title' => 'Austria'
        ],
        [
            'id' => 2,
            'locale' => 'spa',
            'title' => 'Austria'
        ],
        [
            'id' => 3,
            'locale' => 'baq',
            'title' => 'Belgium'
        ],
        [
            'id' => 3,
            'locale' => 'eng',
            'title' => 'Belgium'
        ],
        [
            'id' => 3,
            'locale' => 'fre',
            'title' => 'Belgium'
        ],
        [
            'id' => 3,
            'locale' => 'spa',
            'title' => 'Bélgica'
        ],
        [
            'id' => 4,
            'locale' => 'baq',
            'title' => 'Canada'
        ],
        [
            'id' => 4,
            'locale' => 'eng',
            'title' => 'Canada'
        ],
        [
            'id' => 4,
            'locale' => 'fre',
            'title' => 'Canada'
        ],
        [
            'id' => 4,
            'locale' => 'spa',
            'title' => 'Canadá'
        ],
        [
            'id' => 5,
            'locale' => 'baq',
            'title' => 'China'
        ],
        [
            'id' => 5,
            'locale' => 'eng',
            'title' => 'China'
        ],
        [
            'id' => 5,
            'locale' => 'fre',
            'title' => 'China'
        ],
        [
            'id' => 5,
            'locale' => 'spa',
            'title' => 'Porcelana'
        ],
        [
            'id' => 6,
            'locale' => 'baq',
            'title' => 'Spain'
        ],
        [
            'id' => 6,
            'locale' => 'eng',
            'title' => 'Spain'
        ],
        [
            'id' => 6,
            'locale' => 'fre',
            'title' => 'Spain'
        ],
        [
            'id' => 6,
            'locale' => 'spa',
            'title' => 'España'
        ],
        [
            'id' => 7,
            'locale' => 'baq',
            'title' => 'Finland'
        ],
        [
            'id' => 7,
            'locale' => 'eng',
            'title' => 'Finland'
        ],
        [
            'id' => 7,
            'locale' => 'fre',
            'title' => 'Finland'
        ],
        [
            'id' => 7,
            'locale' => 'spa',
            'title' => 'Finlandia'
        ],
        [
            'id' => 8,
            'locale' => 'baq',
            'title' => 'France'
        ],
        [
            'id' => 8,
            'locale' => 'eng',
            'title' => 'France'
        ],
        [
            'id' => 8,
            'locale' => 'fre',
            'title' => 'France'
        ],
        [
            'id' => 8,
            'locale' => 'spa',
            'title' => 'Francia'
        ],
        [
            'id' => 9,
            'locale' => 'baq',
            'title' => 'Greece'
        ],
        [
            'id' => 9,
            'locale' => 'eng',
            'title' => 'Greece'
        ],
        [
            'id' => 9,
            'locale' => 'fre',
            'title' => 'Greece'
        ],
        [
            'id' => 9,
            'locale' => 'spa',
            'title' => 'Grecia'
        ],
        [
            'id' => 10,
            'locale' => 'baq',
            'title' => 'Italy'
        ],
        [
            'id' => 10,
            'locale' => 'eng',
            'title' => 'Italy'
        ],
        [
            'id' => 10,
            'locale' => 'fre',
            'title' => 'Italy'
        ],
        [
            'id' => 10,
            'locale' => 'spa',
            'title' => 'Italia'
        ],
        [
            'id' => 11,
            'locale' => 'baq',
            'title' => 'Japan'
        ],
        [
            'id' => 11,
            'locale' => 'eng',
            'title' => 'Japan'
        ],
        [
            'id' => 11,
            'locale' => 'fre',
            'title' => 'Japan'
        ],
        [
            'id' => 11,
            'locale' => 'spa',
            'title' => 'Japón'
        ],
        [
            'id' => 12,
            'locale' => 'baq',
            'title' => 'Luxemburg'
        ],
        [
            'id' => 12,
            'locale' => 'eng',
            'title' => 'Luxemburg'
        ],
        [
            'id' => 12,
            'locale' => 'fre',
            'title' => 'Luxemburg'
        ],
        [
            'id' => 12,
            'locale' => 'spa',
            'title' => 'Luxemburgo'
        ],
        [
            'id' => 13,
            'locale' => 'baq',
            'title' => 'Netherlands'
        ],
        [
            'id' => 13,
            'locale' => 'eng',
            'title' => 'Netherlands'
        ],
        [
            'id' => 13,
            'locale' => 'fre',
            'title' => 'Netherlands'
        ],
        [
            'id' => 13,
            'locale' => 'spa',
            'title' => 'Países Bajos'
        ],
    ];
}
