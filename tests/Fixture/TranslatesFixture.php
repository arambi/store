<?php
namespace Store\Test\Fixture;

use Cake\TestSuite\Fixture\TestFixture;

/**
 * ArticlesFixture
 *
 */
class TranslatesFixture extends TestFixture {

/**
 * table property
 *
 * @var string
 */
  public $table = 'i18n';

/**
 * fields property
 *
 * @var array
 */
  public $fields = array(
    'id' => ['type' => 'integer'],
    'locale' => ['type' => 'string', 'length' => 6, 'null' => false],
    'model' => ['type' => 'string', 'null' => false],
    'foreign_key' => ['type' => 'integer', 'null' => false],
    'field' => ['type' => 'string', 'null' => false],
    'content' => ['type' => 'text'],
    '_constraints' => ['primary' => ['type' => 'primary', 'columns' => ['id']]],

  );

/**
 * records property
 *
 * @var array
 */
  public $records = array(
    array('locale' => 'spa', 'model' => 'Attributes', 'foreign_key' => 1, 'field' => 'title', 'content' => 'Blanco'),
    array('locale' => 'eng', 'model' => 'Attributes', 'foreign_key' => 1, 'field' => 'title', 'content' => 'White'),

    array('locale' => 'spa', 'model' => 'Attributes', 'foreign_key' => 2, 'field' => 'title', 'content' => 'Azul'),
    array('locale' => 'eng', 'model' => 'Attributes', 'foreign_key' => 2, 'field' => 'title', 'content' => 'Blue'),
    
    array('locale' => 'spa', 'model' => 'Attributes', 'foreign_key' => 3, 'field' => 'title', 'content' => 'Pequeño'),
    array('locale' => 'eng', 'model' => 'Attributes', 'foreign_key' => 3, 'field' => 'title', 'content' => 'Small'),
    
    array('locale' => 'spa', 'model' => 'Attributes', 'foreign_key' => 4, 'field' => 'title', 'content' => 'Grande'),
    array('locale' => 'eng', 'model' => 'Attributes', 'foreign_key' => 4, 'field' => 'title', 'content' => 'Big'),
    
    array('locale' => 'spa', 'model' => 'AttributeGroups', 'foreign_key' => 1, 'field' => 'title', 'content' => 'Color'),
    array('locale' => 'eng', 'model' => 'AttributeGroups', 'foreign_key' => 1, 'field' => 'title', 'content' => 'Color'),
    
    array('locale' => 'spa', 'model' => 'AttributeGroups', 'foreign_key' => 2, 'field' => 'title', 'content' => 'Tamaño'),
    array('locale' => 'eng', 'model' => 'AttributeGroups', 'foreign_key' => 2, 'field' => 'title', 'content' => 'Size'),

    array('locale' => 'spa', 'model' => 'Countries', 'foreign_key' => 1, 'field' => 'title', 'content' => 'España'),
    array('locale' => 'eng', 'model' => 'Countries', 'foreign_key' => 1, 'field' => 'title', 'content' => 'Spain'),

    array('locale' => 'spa', 'model' => 'Countries', 'foreign_key' => 2, 'field' => 'title', 'content' => 'Francia'),
    array('locale' => 'eng', 'model' => 'Countries', 'foreign_key' => 2, 'field' => 'title', 'content' => 'France'),

    array('locale' => 'spa', 'model' => 'Products', 'foreign_key' => 1, 'field' => 'title', 'content' => 'Polo manga corta'),
    array('locale' => 'eng', 'model' => 'Products', 'foreign_key' => 1, 'field' => 'title', 'content' => 'Polo manga corta'),
    array('locale' => 'spa', 'model' => 'Products', 'foreign_key' => 1, 'field' => 'body', 'content' => 'Lorem ipsum dolor sit amet, aliquet feugiat.'),
    array('locale' => 'eng', 'model' => 'Products', 'foreign_key' => 1, 'field' => 'body', 'content' => 'Lorem ipsum dolor sit amet, aliquet feugiat.'),

    array('locale' => 'spa', 'model' => 'Products', 'foreign_key' => 2, 'field' => 'title', 'content' => 'Chaleco negro'),
    array('locale' => 'eng', 'model' => 'Products', 'foreign_key' => 2, 'field' => 'title', 'content' => 'Chaleco negro'),
    array('locale' => 'spa', 'model' => 'Products', 'foreign_key' => 2, 'field' => 'body', 'content' => 'Lorem ipsum dolor sit amet, aliquet feugiat.'),
    array('locale' => 'eng', 'model' => 'Products', 'foreign_key' => 2, 'field' => 'body', 'content' => 'Lorem ipsum dolor sit amet, aliquet feugiat.'),




  );
}