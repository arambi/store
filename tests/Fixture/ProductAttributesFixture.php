<?php
namespace Store\Test\Fixture;

use Cake\TestSuite\Fixture\TestFixture;

/**
 * ProductAttributesFixture
 *
 */
class ProductAttributesFixture extends TestFixture
{

    /**
     * Table name
     *
     * @var string
     */
    public $table = 'store_product_attributes';

    /**
     * Fields
     *
     * @var array
     */
    // @codingStandardsIgnoreStart
    public $fields = [
        'id' => ['type' => 'integer', 'length' => 11, 'unsigned' => false, 'null' => false, 'default' => null, 'comment' => '', 'autoIncrement' => true, 'precision' => null],
        'product_id' => ['type' => 'integer', 'length' => 11, 'unsigned' => false, 'null' => true, 'default' => null, 'comment' => '', 'precision' => null, 'autoIncrement' => null],
        'store_price' => ['type' => 'float', 'length' => 20, 'precision' => 6, 'unsigned' => false, 'null' => true, 'default' => null, 'comment' => ''],
        'store_weight' => ['type' => 'float', 'length' => null, 'precision' => null, 'unsigned' => false, 'null' => true, 'default' => null, 'comment' => ''],
        'minimal_quantity' => ['type' => 'integer', 'length' => 11, 'unsigned' => false, 'null' => true, 'default' => null, 'comment' => '', 'precision' => null, 'autoIncrement' => null],
        'store_reference' => ['type' => 'string', 'length' => 32, 'null' => true, 'default' => null, 'collate' => 'utf8_general_ci', 'comment' => '', 'precision' => null, 'fixed' => null],
        'store_ean13' => ['type' => 'string', 'length' => 13, 'null' => true, 'default' => null, 'collate' => 'utf8_general_ci', 'comment' => '', 'precision' => null, 'fixed' => null],
        'store_upc' => ['type' => 'string', 'length' => 12, 'null' => true, 'default' => null, 'collate' => 'utf8_general_ci', 'comment' => '', 'precision' => null, 'fixed' => null],
        'photo_id' => ['type' => 'string', 'length' => 12, 'null' => true, 'default' => null, 'collate' => 'utf8_general_ci', 'comment' => '', 'precision' => null, 'fixed' => null],
        'quantity' => ['type' => 'integer', 'length' => 10, 'unsigned' => false, 'null' => true, 'default' => null, 'comment' => '', 'precision' => null, 'autoIncrement' => null],
        'available_date' => ['type' => 'date', 'length' => null, 'null' => true, 'default' => null, 'comment' => '', 'precision' => null],
        'published' => ['type' => 'boolean', 'length' => null, 'null' => false, 'default' => '1', 'comment' => '', 'precision' => null],
        '_indexes' => [
            'published' => ['type' => 'index', 'columns' => ['published'], 'length' => []],
            'product_id' => ['type' => 'index', 'columns' => ['product_id'], 'length' => []],
            'store_reference' => ['type' => 'index', 'columns' => ['store_reference'], 'length' => []],
        ],
        '_constraints' => [
            'primary' => ['type' => 'primary', 'columns' => ['id'], 'length' => []],
        ],
        '_options' => [
            'engine' => 'InnoDB',
            'collation' => 'utf8_general_ci'
        ],
    ];
    // @codingStandardsIgnoreEnd

    /**
     * Records
     *
     * @var array
     */
    public $records = [
        [
            'id' => 1,
            'product_id' => 1,
            'store_price' => 40,
            'store_weight' => null,
            'minimal_quantity' => null,
            'store_reference' => '',
            'store_ean13' => '',
            'store_upc' => '',
            'photo_id' => '',
            'quantity' => 5,
            'available_date' => null,
            'published' => true
        ],
        [
            'id' => 2,
            'product_id' => 1,
            'store_price' => 40,
            'store_weight' => null,
            'minimal_quantity' => null,
            'store_reference' => '',
            'store_ean13' => '',
            'store_upc' => '',
            'photo_id' => '',
            'quantity' => 5,
            'available_date' => null,
            'published' => true
        ],
        [
            'id' => 3,
            'product_id' => 1,
            'store_price' => 40,
            'store_weight' => null,
            'minimal_quantity' => null,
            'store_reference' => '',
            'store_ean13' => '',
            'store_upc' => '',
            'photo_id' => '',
            'quantity' => 9996,
            'available_date' => null,
            'published' => true
        ],
        [
            'id' => 4,
            'product_id' => 1,
            'store_price' => 49,
            'store_weight' => null,
            'minimal_quantity' => null,
            'store_reference' => '',
            'store_ean13' => '',
            'store_upc' => '',
            'photo_id' => '',
            'quantity' => 5,
            'available_date' => null,
            'published' => true
        ],
        [
            'id' => 5,
            'product_id' => 1,
            'store_price' => 49,
            'store_weight' => null,
            'minimal_quantity' => null,
            'store_reference' => '',
            'store_ean13' => '',
            'store_upc' => '',
            'photo_id' => '',
            'quantity' => 5,
            'available_date' => null,
            'published' => true
        ],
    ];
}
