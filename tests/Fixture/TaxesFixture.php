<?php
namespace Store\Test\Fixture;

use Cake\TestSuite\Fixture\TestFixture;

/**
 * TaxesFixture
 *
 */
class TaxesFixture extends TestFixture
{

    /**
     * Table name
     *
     * @var string
     */
    public $table = 'store_taxes';

    /**
     * Fields
     *
     * @var array
     */
    // @codingStandardsIgnoreStart
    public $fields = [
        'id' => ['type' => 'integer', 'length' => 11, 'unsigned' => false, 'null' => false, 'default' => null, 'comment' => '', 'autoIncrement' => true, 'precision' => null],
        'title' => ['type' => 'string', 'length' => 255, 'null' => false, 'default' => null, 'comment' => '', 'precision' => null, 'fixed' => null],
        'value' => ['type' => 'float', 'length' => null, 'precision' => null, 'unsigned' => false, 'null' => true, 'default' => null, 'comment' => ''],
        'multizone' => ['type' => 'boolean', 'length' => null, 'null' => false, 'default' => '0', 'comment' => '', 'precision' => null],
        'created' => ['type' => 'datetime', 'length' => null, 'null' => false, 'default' => null, 'comment' => '', 'precision' => null],
        'modified' => ['type' => 'datetime', 'length' => null, 'null' => false, 'default' => null, 'comment' => '', 'precision' => null],
        '_indexes' => [
            'multizone' => ['type' => 'index', 'columns' => ['multizone'], 'length' => []],
        ],
        '_constraints' => [
            'primary' => ['type' => 'primary', 'columns' => ['id'], 'length' => []],
        ],
        '_options' => [
'engine' => 'InnoDB', 'collation' => 'utf8_general_ci'
        ],
    ];
    // @codingStandardsIgnoreEnd

    /**
     * Records
     *
     * @var array
     */
    public $records = [
        [
            'id' => 1,
            'title' => 'IVA 21%',
            'value' => 21,
            'multizone' => 0,
            'created' => '2015-02-26 14:02:13',
            'modified' => '2015-02-26 14:02:13'
        ],
        [
            'id' => 2,
            'title' => 'IVA ES 16%',
            'value' => 16,
            'multizone' => 0,
            'created' => '2015-02-26 14:02:13',
            'modified' => '2015-02-26 14:02:13'
        ],
        [
            'id' => 3,
            'title' => 'IVA FR 20%',
            'value' => 20,
            'multizone' => 0,
            'created' => '2015-02-26 14:02:13',
            'modified' => '2015-02-26 14:02:13'
        ],
    ];
}
