<?php
namespace Store\Test\Fixture;

use Cake\TestSuite\Fixture\TestFixture;

/**
 * ProductsFixture
 *
 */
class ProductsFixture extends TestFixture
{

    /**
     * Table name
     *
     * @var string
     */
    public $table = 'products';

    /**
     * Fields
     *
     * @var array
     */
    // @codingStandardsIgnoreStart
    public $fields = [
        'id' => ['type' => 'integer', 'length' => 11, 'unsigned' => false, 'null' => false, 'default' => null, 'comment' => '', 'autoIncrement' => true, 'precision' => null],
        'content_type' => ['type' => 'string', 'length' => 16, 'null' => false, 'default' => null, 'collate' => 'utf8_general_ci', 'comment' => '', 'precision' => null, 'fixed' => null],
        'site_id' => ['type' => 'integer', 'length' => 4, 'unsigned' => false, 'null' => true, 'default' => null, 'comment' => '', 'precision' => null, 'autoIncrement' => null],
        'parent_id' => ['type' => 'integer', 'length' => 11, 'unsigned' => false, 'null' => true, 'default' => null, 'comment' => '', 'precision' => null, 'autoIncrement' => null],
        'subtype' => ['type' => 'string', 'length' => 32, 'null' => false, 'default' => null, 'collate' => 'utf8_general_ci', 'comment' => '', 'precision' => null, 'fixed' => null],
        'category_id' => ['type' => 'integer', 'length' => 11, 'unsigned' => false, 'null' => true, 'default' => null, 'comment' => '', 'precision' => null, 'autoIncrement' => null],
        'published' => ['type' => 'boolean', 'length' => null, 'null' => true, 'default' => '0', 'comment' => '', 'precision' => null],
        'published_at' => ['type' => 'date', 'length' => null, 'null' => true, 'default' => null, 'comment' => '', 'precision' => null],
        'title' => ['type' => 'string', 'length' => 255, 'null' => true, 'default' => null, 'collate' => 'utf8_general_ci', 'comment' => '', 'precision' => null, 'fixed' => null],
        'antetitle' => ['type' => 'string', 'length' => 255, 'null' => true, 'default' => null, 'collate' => 'utf8_general_ci', 'comment' => '', 'precision' => null, 'fixed' => null],
        'subtitle' => ['type' => 'string', 'length' => 255, 'null' => true, 'default' => null, 'collate' => 'utf8_general_ci', 'comment' => '', 'precision' => null, 'fixed' => null],
        'summary' => ['type' => 'text', 'length' => null, 'null' => true, 'default' => null, 'collate' => 'utf8_general_ci', 'comment' => '', 'precision' => null],
        'body' => ['type' => 'text', 'length' => null, 'null' => true, 'default' => null, 'collate' => 'utf8_general_ci', 'comment' => '', 'precision' => null],
        'url' => ['type' => 'string', 'length' => 255, 'null' => true, 'default' => null, 'collate' => 'utf8_general_ci', 'comment' => '', 'precision' => null, 'fixed' => null],
        'row' => ['type' => 'integer', 'length' => 3, 'unsigned' => false, 'null' => true, 'default' => null, 'comment' => '', 'precision' => null, 'autoIncrement' => null],
        'cols' => ['type' => 'integer', 'length' => 2, 'unsigned' => false, 'null' => true, 'default' => null, 'comment' => '', 'precision' => null, 'autoIncrement' => null],
        'position' => ['type' => 'integer', 'length' => 6, 'unsigned' => false, 'null' => true, 'default' => null, 'comment' => '', 'precision' => null, 'autoIncrement' => null],
        'settings' => ['type' => 'text', 'length' => null, 'null' => true, 'default' => null, 'collate' => 'utf8_general_ci', 'comment' => '', 'precision' => null],
        'salt' => ['type' => 'string', 'length' => 255, 'null' => true, 'default' => null, 'collate' => 'utf8_general_ci', 'comment' => '', 'precision' => null, 'fixed' => null],
        'photo' => ['type' => 'text', 'length' => null, 'null' => true, 'default' => null, 'collate' => 'utf8_general_ci', 'comment' => '', 'precision' => null],
        'photos' => ['type' => 'text', 'length' => null, 'null' => true, 'default' => null, 'collate' => 'utf8_general_ci', 'comment' => '', 'precision' => null],
        'docs' => ['type' => 'text', 'length' => null, 'null' => true, 'default' => null, 'collate' => 'utf8_general_ci', 'comment' => '', 'precision' => null],
        'created' => ['type' => 'datetime', 'length' => null, 'null' => false, 'default' => null, 'comment' => '', 'precision' => null],
        'modified' => ['type' => 'datetime', 'length' => null, 'null' => false, 'default' => null, 'comment' => '', 'precision' => null],
        'draft' => ['type' => 'boolean', 'length' => null, 'null' => true, 'default' => '0', 'comment' => '', 'precision' => null],
        'special' => ['type' => 'boolean', 'length' => null, 'null' => true, 'default' => '0', 'comment' => '', 'precision' => null],
        'comment_count' => ['type' => 'integer', 'length' => 11, 'unsigned' => false, 'null' => true, 'default' => '0', 'comment' => '', 'precision' => null, 'autoIncrement' => null],
        'view_count' => ['type' => 'integer', 'length' => 11, 'unsigned' => false, 'null' => false, 'default' => '0', 'comment' => '', 'precision' => null, 'autoIncrement' => null],
        'legacy_id' => ['type' => 'integer', 'length' => 11, 'unsigned' => false, 'null' => true, 'default' => null, 'comment' => '', 'precision' => null, 'autoIncrement' => null],
        'store_brand_id' => ['type' => 'integer', 'length' => 11, 'unsigned' => false, 'null' => true, 'default' => null, 'comment' => '', 'precision' => null, 'autoIncrement' => null],
        'store_tax_id' => ['type' => 'integer', 'length' => 11, 'unsigned' => false, 'null' => true, 'default' => null, 'comment' => '', 'precision' => null, 'autoIncrement' => null],
        'store_price' => ['type' => 'float', 'length' => 20, 'precision' => 6, 'unsigned' => false, 'null' => true, 'default' => '0.000000', 'comment' => ''],
        'store_weight' => ['type' => 'float', 'length' => null, 'precision' => null, 'unsigned' => false, 'null' => true, 'default' => null, 'comment' => ''],
        'store_minimal_quantity' => ['type' => 'integer', 'length' => 11, 'unsigned' => false, 'null' => true, 'default' => null, 'comment' => '', 'precision' => null, 'autoIncrement' => null],
        'store_reference' => ['type' => 'string', 'length' => 32, 'null' => true, 'default' => null, 'collate' => 'utf8_general_ci', 'comment' => '', 'precision' => null, 'fixed' => null],
        'store_ean13' => ['type' => 'string', 'length' => 13, 'null' => true, 'default' => null, 'collate' => 'utf8_general_ci', 'comment' => '', 'precision' => null, 'fixed' => null],
        'store_upc' => ['type' => 'string', 'length' => 12, 'null' => true, 'default' => null, 'collate' => 'utf8_general_ci', 'comment' => '', 'precision' => null, 'fixed' => null],
        'store_quantity' => ['type' => 'integer', 'length' => 10, 'unsigned' => false, 'null' => true, 'default' => null, 'comment' => '', 'precision' => null, 'autoIncrement' => null],
        'store_available_date' => ['type' => 'date', 'length' => null, 'null' => true, 'default' => null, 'comment' => '', 'precision' => null],
        'store_novelty' => ['type' => 'boolean', 'length' => null, 'null' => true, 'default' => null, 'comment' => '', 'precision' => null],
        'delay_days' => ['type' => 'integer', 'length' => 11, 'unsigned' => false, 'null' => true, 'default' => '0', 'comment' => '', 'precision' => null, 'autoIncrement' => null],
        'only_with_attributes' => ['type' => 'boolean', 'length' => null, 'null' => false, 'default' => '0', 'comment' => '', 'precision' => null],
        'without_stock' => ['type' => 'boolean', 'length' => null, 'null' => false, 'default' => '0', 'comment' => '', 'precision' => null],
        '_indexes' => [
            'content_type' => ['type' => 'index', 'columns' => ['content_type'], 'length' => []],
            'site_id' => ['type' => 'index', 'columns' => ['site_id'], 'length' => []],
            'category_id' => ['type' => 'index', 'columns' => ['category_id'], 'length' => []],
            'parent_id' => ['type' => 'index', 'columns' => ['parent_id'], 'length' => []],
            'published_at' => ['type' => 'index', 'columns' => ['published_at'], 'length' => []],
            'row' => ['type' => 'index', 'columns' => ['row'], 'length' => []],
            'position' => ['type' => 'index', 'columns' => ['position'], 'length' => []],
            'special' => ['type' => 'index', 'columns' => ['special'], 'length' => []],
            'comment_count' => ['type' => 'index', 'columns' => ['comment_count'], 'length' => []],
            'view_count' => ['type' => 'index', 'columns' => ['view_count'], 'length' => []],
            'legacy_id' => ['type' => 'index', 'columns' => ['legacy_id'], 'length' => []],
            'store_brand_id' => ['type' => 'index', 'columns' => ['store_brand_id'], 'length' => []],
            'store_tax_id' => ['type' => 'index', 'columns' => ['store_tax_id'], 'length' => []],
            'store_price' => ['type' => 'index', 'columns' => ['store_price'], 'length' => []],
            'store_quantity' => ['type' => 'index', 'columns' => ['store_quantity'], 'length' => []],
        ],
        '_constraints' => [
            'primary' => ['type' => 'primary', 'columns' => ['id'], 'length' => []],
        ],
        '_options' => [
            'engine' => 'InnoDB',
            'collation' => 'utf8_general_ci'
        ],
    ];
    // @codingStandardsIgnoreEnd

    /**
     * Records
     *
     * @var array
     */
    public $records = [
        [
            'id' => 1,
            'content_type' => 'Products',
            'title' => 'Título de producto',
            'antetitle' => 'Antetítulo de producto',
            'salt' => '874e3daa82e9cdc037a28c3160d28c7ca8528b6e',
            'photo' => null,
            'photos' => '[{"_id":"o_1bql0qghu1u1c1026tir1i4v4dn22","isUploading":false,"percent":100,"id":347,"content_type":"product","filename":"90-1-FONDO-BLANCO-1.jpg","path":"2017\\/09","original_filename":"90-1-FONDO-BLANCO.jpg","filesize":646561,"extension":"jpg","mimetype":"image\\/jpeg","duration":null,"created":"2017-09-22T14:45:55+00:00","modified":"2017-09-22T14:45:57+00:00","paths":{"thm":"\\/files\\/2017\\/09\\/thm_90-1-FONDO-BLANCO-1.jpg","hor":"\\/files\\/2017\\/09\\/hor_90-1-FONDO-BLANCO-1.jpg","square":"\\/files\\/2017\\/09\\/square_90-1-FONDO-BLANCO-1.jpg","big":"\\/files\\/2017\\/09\\/big_90-1-FONDO-BLANCO-1.jpg","org":"\\/files\\/2017\\/09\\/90-1-FONDO-BLANCO-1.jpg"},"title":"90-1-FONDO-BLANCO-1","alt":null},{"_id":"o_1bql0qghuvvlrj51opl38m1fuq23","isUploading":false,"percent":100,"id":348,"content_type":"product","filename":"80-1-FONDO-BLANCO-1.jpg","path":"2017\\/09","original_filename":"80-1-FONDO-BLANCO.jpg","filesize":615542,"extension":"jpg","mimetype":"image\\/jpeg","duration":null,"created":"2017-09-22T14:45:58+00:00","modified":"2017-09-22T14:45:59+00:00","paths":{"thm":"\\/files\\/2017\\/09\\/thm_80-1-FONDO-BLANCO-1.jpg","hor":"\\/files\\/2017\\/09\\/hor_80-1-FONDO-BLANCO-1.jpg","square":"\\/files\\/2017\\/09\\/square_80-1-FONDO-BLANCO-1.jpg","big":"\\/files\\/2017\\/09\\/big_80-1-FONDO-BLANCO-1.jpg","org":"\\/files\\/2017\\/09\\/80-1-FONDO-BLANCO-1.jpg"},"title":"80-1-FONDO-BLANCO-1","alt":null},{"_id":"o_1bql0qghu1qebglc1bbvvnl16vf24","isUploading":false,"percent":100,"id":349,"content_type":"product","filename":"cream-spray-994x1500-1.jpg","path":"2017\\/09","original_filename":"cream-spray-994x1500.jpg","filesize":294612,"extension":"jpg","mimetype":"image\\/jpeg","duration":null,"created":"2017-09-22T14:46:01+00:00","modified":"2017-09-22T14:46:02+00:00","paths":{"thm":"\\/files\\/2017\\/09\\/thm_cream-spray-994x1500-1.jpg","hor":"\\/files\\/2017\\/09\\/hor_cream-spray-994x1500-1.jpg","square":"\\/files\\/2017\\/09\\/square_cream-spray-994x1500-1.jpg","big":"\\/files\\/2017\\/09\\/big_cream-spray-994x1500-1.jpg","org":"\\/files\\/2017\\/09\\/cream-spray-994x1500-1.jpg"},"title":"cream-spray-994x1500-1","alt":null}]',
            'created' => '2017-09-22 13:58:42',
            'modified' => '2017-09-25 10:21:26',
            'store_brand_id' => null,
            'store_tax_id' => 1,
            'store_price' => 41.240001999999997,
            'store_weight' => 50,
            'store_minimal_quantity' => null,
            'store_reference' => null,
            'store_ean13' => null,
            'store_upc' => null,
            'store_quantity' => 10,
            'store_available_date' => null,
            'store_novelty' => null,
            'delay_days' => 0,
            'only_with_attributes' => true,
            'without_stock' => false,
            'legacy_id' => 99
        ],

        [
            'id' => 2,
            'content_type' => 'Products',
            'title' => 'Título de producto 2',
            'antetitle' => 'Antetítulo de producto 2',
            'salt' => '874e3daa82e9cdc037a28c1160d28c7ca8528b6e',
            'photo' => null,
            'photos' => '[{"_id":"o_1bql0qghu1u1c1026tir1i4v4dn22","isUploading":false,"percent":100,"id":347,"content_type":"product","filename":"90-1-FONDO-BLANCO-1.jpg","path":"2017\\/09","original_filename":"90-1-FONDO-BLANCO.jpg","filesize":646561,"extension":"jpg","mimetype":"image\\/jpeg","duration":null,"created":"2017-09-22T14:45:55+00:00","modified":"2017-09-22T14:45:57+00:00","paths":{"thm":"\\/files\\/2017\\/09\\/thm_90-1-FONDO-BLANCO-1.jpg","hor":"\\/files\\/2017\\/09\\/hor_90-1-FONDO-BLANCO-1.jpg","square":"\\/files\\/2017\\/09\\/square_90-1-FONDO-BLANCO-1.jpg","big":"\\/files\\/2017\\/09\\/big_90-1-FONDO-BLANCO-1.jpg","org":"\\/files\\/2017\\/09\\/90-1-FONDO-BLANCO-1.jpg"},"title":"90-1-FONDO-BLANCO-1","alt":null},{"_id":"o_1bql0qghuvvlrj51opl38m1fuq23","isUploading":false,"percent":100,"id":348,"content_type":"product","filename":"80-1-FONDO-BLANCO-1.jpg","path":"2017\\/09","original_filename":"80-1-FONDO-BLANCO.jpg","filesize":615542,"extension":"jpg","mimetype":"image\\/jpeg","duration":null,"created":"2017-09-22T14:45:58+00:00","modified":"2017-09-22T14:45:59+00:00","paths":{"thm":"\\/files\\/2017\\/09\\/thm_80-1-FONDO-BLANCO-1.jpg","hor":"\\/files\\/2017\\/09\\/hor_80-1-FONDO-BLANCO-1.jpg","square":"\\/files\\/2017\\/09\\/square_80-1-FONDO-BLANCO-1.jpg","big":"\\/files\\/2017\\/09\\/big_80-1-FONDO-BLANCO-1.jpg","org":"\\/files\\/2017\\/09\\/80-1-FONDO-BLANCO-1.jpg"},"title":"80-1-FONDO-BLANCO-1","alt":null},{"_id":"o_1bql0qghu1qebglc1bbvvnl16vf24","isUploading":false,"percent":100,"id":349,"content_type":"product","filename":"cream-spray-994x1500-1.jpg","path":"2017\\/09","original_filename":"cream-spray-994x1500.jpg","filesize":294612,"extension":"jpg","mimetype":"image\\/jpeg","duration":null,"created":"2017-09-22T14:46:01+00:00","modified":"2017-09-22T14:46:02+00:00","paths":{"thm":"\\/files\\/2017\\/09\\/thm_cream-spray-994x1500-1.jpg","hor":"\\/files\\/2017\\/09\\/hor_cream-spray-994x1500-1.jpg","square":"\\/files\\/2017\\/09\\/square_cream-spray-994x1500-1.jpg","big":"\\/files\\/2017\\/09\\/big_cream-spray-994x1500-1.jpg","org":"\\/files\\/2017\\/09\\/cream-spray-994x1500-1.jpg"},"title":"cream-spray-994x1500-1","alt":null}]',
            'created' => '2017-09-22 13:58:42',
            'modified' => '2017-09-25 10:21:26',
            'store_brand_id' => null,
            'store_tax_id' => 1,
            'store_price' => 41.240001999999997,
            'store_weight' => 50,
            'store_minimal_quantity' => null,
            'store_reference' => null,
            'store_ean13' => null,
            'store_upc' => null,
            'store_quantity' => 10,
            'store_available_date' => null,
            'store_novelty' => null,
            'delay_days' => 0,
            'only_with_attributes' => false,
            'without_stock' => false
        ],
        
    ];
    
}
