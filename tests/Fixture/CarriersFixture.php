<?php
namespace Store\Test\Fixture;

use Cake\TestSuite\Fixture\TestFixture;

/**
 * CarriersFixture
 *
 */
class CarriersFixture extends TestFixture
{

    /**
     * Table name
     *
     * @var string
     */
    public $table = 'store_carriers';

    /**
     * Fields
     *
     * @var array
     */
    // @codingStandardsIgnoreStart
    public $fields = [
        'id' => ['type' => 'integer', 'length' => 11, 'unsigned' => false, 'null' => false, 'default' => null, 'comment' => '', 'autoIncrement' => true, 'precision' => null],
        'title' => ['type' => 'string', 'length' => 255, 'null' => true, 'default' => null, 'collate' => 'utf8_general_ci', 'comment' => '', 'precision' => null, 'fixed' => null],
        'active' => ['type' => 'boolean', 'length' => null, 'null' => false, 'default' => '0', 'comment' => '', 'precision' => null],
        'by_default' => ['type' => 'boolean', 'length' => null, 'null' => false, 'default' => '0', 'comment' => '', 'precision' => null],
        'tax_id' => ['type' => 'integer', 'length' => 11, 'unsigned' => false, 'null' => true, 'default' => null, 'comment' => '', 'precision' => null, 'autoIncrement' => null],
        'price' => ['type' => 'float', 'length' => 20, 'precision' => 6, 'unsigned' => false, 'null' => true, 'default' => '0.000000', 'comment' => ''],
        'hour' => ['type' => 'string', 'length' => null, 'null' => false, 'default' => 'fixed', 'collate' => 'utf8_general_ci', 'comment' => '', 'precision' => null, 'fixed' => null],
        'insurance' => ['type' => 'boolean', 'length' => null, 'null' => false, 'default' => '0', 'comment' => '', 'precision' => null],
        'insurance_type' => ['type' => 'string', 'length' => null, 'null' => false, 'default' => 'fixed', 'collate' => 'utf8_general_ci', 'comment' => '', 'precision' => null, 'fixed' => null],
        'insurance_min' => ['type' => 'float', 'length' => null, 'precision' => null, 'unsigned' => false, 'null' => true, 'default' => null, 'comment' => ''],
        'express_delivery' => ['type' => 'boolean', 'length' => null, 'null' => false, 'default' => '0', 'comment' => '', 'precision' => null],
        'express_delivery_type' => ['type' => 'string', 'length' => null, 'null' => false, 'default' => 'fixed', 'collate' => 'utf8_general_ci', 'comment' => '', 'precision' => null, 'fixed' => null],
        'express_delivery_value' => ['type' => 'float', 'length' => null, 'precision' => null, 'unsigned' => false, 'null' => true, 'default' => null, 'comment' => ''],
        'express_delivery_days' => ['type' => 'integer', 'length' => 11, 'unsigned' => false, 'null' => true, 'default' => null, 'comment' => '', 'precision' => null, 'autoIncrement' => null],
        'delivery_days' => ['type' => 'integer', 'length' => 11, 'unsigned' => false, 'null' => true, 'default' => null, 'comment' => '', 'precision' => null, 'autoIncrement' => null],
        'delivery_days_type' => ['type' => 'string', 'length' => null, 'null' => false, 'default' => 'business_days', 'collate' => 'utf8_general_ci', 'comment' => '', 'precision' => null, 'fixed' => null],
        'salt' => ['type' => 'string', 'length' => 255, 'null' => true, 'default' => null, 'collate' => 'utf8_general_ci', 'comment' => '', 'precision' => null, 'fixed' => null],
        '_indexes' => [
            'active' => ['type' => 'index', 'columns' => ['active'], 'length' => []],
            'tax_id' => ['type' => 'index', 'columns' => ['tax_id'], 'length' => []],
            'by_default' => ['type' => 'index', 'columns' => ['by_default'], 'length' => []],
        ],
        '_constraints' => [
            'primary' => ['type' => 'primary', 'columns' => ['id'], 'length' => []],
        ],
        '_options' => [
            'engine' => 'InnoDB',
            'collation' => 'utf8_general_ci'
        ],
    ];
    // @codingStandardsIgnoreEnd

    /**
     * Records
     *
     * @var array
     */
    public $records = [
        [
            'id' => 1,
            'title' => 'Normal',
            'active' => 1,
            'by_default' => 1,
            'tax_id' => 1,
            'price' => 0,
            'hour' => '19:30',
            'insurance' => 0,
            'insurance_type' => '',
            'insurance_min' => 10,
            'express_delivery' => 1,
            'express_delivery_type' => 'Lorem ipsum dolor sit amet',
            'express_delivery_value' => 1,
            'express_delivery_days' => 1,
            'delivery_days' => 1,
            'delivery_days_type' => 'business_days',
            'salt' => '21334234'
        ],
    ];
}
