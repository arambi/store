<?php
namespace Store\Test\Fixture;

use Cake\TestSuite\Fixture\TestFixture;

/**
 * AttributeGroupsFixture
 *
 */
class AttributeGroupsFixture extends TestFixture
{

    /**
     * Table name
     *
     * @var string
     */
    public $table = 'store_attribute_groups';

    /**
     * Fields
     *
     * @var array
     */
    // @codingStandardsIgnoreStart
    public $fields = [
        'id' => ['type' => 'integer', 'length' => 11, 'unsigned' => false, 'null' => false, 'default' => null, 'comment' => '', 'autoIncrement' => true, 'precision' => null],
        'title' => ['type' => 'string', 'length' => 255, 'null' => true, 'default' => null, 'collate' => 'utf8_general_ci', 'comment' => '', 'precision' => null, 'fixed' => null],
        'store_title' => ['type' => 'string', 'length' => 255, 'null' => true, 'default' => null, 'collate' => 'utf8_general_ci', 'comment' => '', 'precision' => null, 'fixed' => null],
        'is_color_group' => ['type' => 'boolean', 'length' => null, 'null' => false, 'default' => '0', 'comment' => '', 'precision' => null],
        'has_product_photo' => ['type' => 'boolean', 'length' => null, 'null' => false, 'default' => '0', 'comment' => '', 'precision' => null],
        'color' => ['type' => 'string', 'length' => 32, 'null' => true, 'default' => null, 'collate' => 'utf8_general_ci', 'comment' => '', 'precision' => null, 'fixed' => null],
        'group_type' => ['type' => 'string', 'length' => null, 'null' => false, 'default' => 'select', 'collate' => 'utf8_general_ci', 'comment' => '', 'precision' => null, 'fixed' => null],
        'salt' => ['type' => 'string', 'length' => 255, 'null' => true, 'default' => null, 'collate' => 'utf8_general_ci', 'comment' => '', 'precision' => null, 'fixed' => null],
        'created' => ['type' => 'datetime', 'length' => null, 'null' => false, 'default' => null, 'comment' => '', 'precision' => null],
        'modified' => ['type' => 'datetime', 'length' => null, 'null' => false, 'default' => null, 'comment' => '', 'precision' => null],
        'cname' => ['type' => 'string', 'length' => 32, 'null' => true, 'default' => null, 'collate' => 'utf8_general_ci', 'comment' => '', 'precision' => null, 'fixed' => null],
        '_indexes' => [
            'is_color_group' => ['type' => 'index', 'columns' => ['is_color_group'], 'length' => []],
        ],
        '_constraints' => [
            'primary' => ['type' => 'primary', 'columns' => ['id'], 'length' => []],
        ],
        '_options' => [
            'engine' => 'InnoDB',
            'collation' => 'utf8_general_ci'
        ],
    ];
    // @codingStandardsIgnoreEnd

    /**
     * Records
     *
     * @var array
     */
    public $records = [
        [
            'id' => 1,
            'title' => null,
            'store_title' => null,
            'is_color_group' => false,
            'has_product_photo' => false,
            'color' => '',
            'group_type' => 'color',
            'salt' => 'ccd639bb584fd9fa9737bca7f82fce6b02192733',
            'created' => '2017-09-22 13:27:53',
            'modified' => '2017-09-24 20:01:52',
            'cname' => 'color'
        ],
        [
            'id' => 2,
            'title' => null,
            'store_title' => null,
            'is_color_group' => false,
            'has_product_photo' => false,
            'color' => '',
            'group_type' => 'radio',
            'salt' => 'dc24d38c2a7a8e470665b6c1be8f2e7fe0811c84',
            'created' => '2017-09-22 14:06:51',
            'modified' => '2017-09-24 20:01:52',
            'cname' => 'size'
        ],
    ];
}
