<?php
namespace Store\Test\Fixture;

use Cake\TestSuite\Fixture\TestFixture;

/**
 * ZonesFixture
 *
 */
class ZonesFixture extends TestFixture
{

    /**
     * Table name
     *
     * @var string
     */
    public $table = 'store_zones';

    /**
     * Fields
     *
     * @var array
     */
    // @codingStandardsIgnoreStart
    public $fields = [
        'id' => ['type' => 'integer', 'length' => 11, 'unsigned' => false, 'null' => false, 'default' => null, 'comment' => '', 'autoIncrement' => true, 'precision' => null],
        'title' => ['type' => 'string', 'length' => 255, 'null' => false, 'default' => null, 'comment' => '', 'precision' => null, 'fixed' => null],
        'active' => ['type' => 'boolean', 'length' => null, 'null' => false, 'default' => '0', 'comment' => '', 'precision' => null],
        'created' => ['type' => 'datetime', 'length' => null, 'null' => false, 'default' => null, 'comment' => '', 'precision' => null],
        'modified' => ['type' => 'datetime', 'length' => null, 'null' => false, 'default' => null, 'comment' => '', 'precision' => null],
        '_constraints' => [
            'primary' => ['type' => 'primary', 'columns' => ['id'], 'length' => []],
        ],
        '_options' => [
'engine' => 'InnoDB', 'collation' => 'utf8_general_ci'
        ],
    ];
    // @codingStandardsIgnoreEnd

    /**
     * Records
     *
     * @var array
     */
    public $records = [
        [
            'id' => 1,
            'title' => 'España',
            'active' => 1,
            'created' => '2015-02-25 20:10:59',
            'modified' => '2015-02-25 20:10:59'
        ],
        [
            'id' => 2,
            'title' => 'Francia',
            'active' => 1,
            'created' => '2015-02-25 20:10:59',
            'modified' => '2015-02-25 20:10:59'
        ],
        [
            'id' => 3,
            'title' => 'Islas Canarias',
            'active' => 1,
            'created' => '2015-02-25 20:10:59',
            'modified' => '2015-02-25 20:10:59'
        ],
    ];
}
