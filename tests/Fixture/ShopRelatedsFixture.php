<?php
namespace Store\Test\Fixture;

use Cake\TestSuite\Fixture\TestFixture;

/**
 * ShopRelatedsFixture
 *
 */
class ShopRelatedsFixture extends TestFixture
{

    /**
     * Fields
     *
     * @var array
     */
    // @codingStandardsIgnoreStart
    public $fields = [
        'id' => ['type' => 'integer', 'length' => 11, 'unsigned' => false, 'null' => false, 'default' => null, 'comment' => '', 'autoIncrement' => true, 'precision' => null],
        'product_id' => ['type' => 'integer', 'length' => 11, 'unsigned' => false, 'null' => true, 'default' => null, 'comment' => '', 'precision' => null, 'autoIncrement' => null],
        'related_id' => ['type' => 'integer', 'length' => 11, 'unsigned' => false, 'null' => true, 'default' => null, 'comment' => '', 'precision' => null, 'autoIncrement' => null],
        '_indexes' => [
            'product_id' => ['type' => 'index', 'columns' => ['product_id'], 'length' => []],
            'related_id' => ['type' => 'index', 'columns' => ['related_id'], 'length' => []],
        ],
        '_constraints' => [
            'primary' => ['type' => 'primary', 'columns' => ['id'], 'length' => []],
        ],
        '_options' => [
            'engine' => 'InnoDB',
            'collation' => 'utf8_general_ci'
        ],
    ];
    // @codingStandardsIgnoreEnd

    /**
     * Records
     *
     * @var array
     */
    public $records = [
        [
            'id' => 1,
            'product_id' => 1,
            'related_id' => 1
        ],
        [
            'id' => 2,
            'product_id' => 2,
            'related_id' => 2
        ],
        [
            'id' => 3,
            'product_id' => 3,
            'related_id' => 3
        ],
        [
            'id' => 4,
            'product_id' => 4,
            'related_id' => 4
        ],
        [
            'id' => 5,
            'product_id' => 5,
            'related_id' => 5
        ],
        [
            'id' => 6,
            'product_id' => 6,
            'related_id' => 6
        ],
        [
            'id' => 7,
            'product_id' => 7,
            'related_id' => 7
        ],
        [
            'id' => 8,
            'product_id' => 8,
            'related_id' => 8
        ],
        [
            'id' => 9,
            'product_id' => 9,
            'related_id' => 9
        ],
        [
            'id' => 10,
            'product_id' => 10,
            'related_id' => 10
        ],
    ];
}
