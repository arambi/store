<?php
namespace Store\Test\Fixture;

use Cake\TestSuite\Fixture\TestFixture;

/**
 * InvoiceRefundLineItemsFixture
 */
class InvoiceRefundLineItemsFixture extends TestFixture
{
    /**
     * Table name
     *
     * @var string
     */
    public $table = 'store_invoice_refund_line_items';
    /**
     * Fields
     *
     * @var array
     */
    // @codingStandardsIgnoreStart
    public $fields = [
        'id' => ['type' => 'integer', 'length' => 11, 'unsigned' => false, 'null' => false, 'default' => null, 'comment' => '', 'autoIncrement' => true, 'precision' => null],
        'invoice_refunds_id' => ['type' => 'integer', 'length' => 11, 'unsigned' => false, 'null' => true, 'default' => null, 'comment' => '', 'precision' => null, 'autoIncrement' => null],
        'line_item_id' => ['type' => 'integer', 'length' => 11, 'unsigned' => false, 'null' => true, 'default' => null, 'comment' => '', 'precision' => null, 'autoIncrement' => null],
        'quantity' => ['type' => 'integer', 'length' => 11, 'unsigned' => false, 'null' => true, 'default' => null, 'comment' => '', 'precision' => null, 'autoIncrement' => null],
        'price' => ['type' => 'float', 'length' => 20, 'precision' => 3, 'unsigned' => false, 'null' => true, 'default' => '0.000', 'comment' => ''],
        'tax_rate' => ['type' => 'float', 'length' => 20, 'precision' => 3, 'unsigned' => false, 'null' => true, 'default' => '0.000', 'comment' => ''],
        'created' => ['type' => 'datetime', 'length' => null, 'null' => false, 'default' => null, 'comment' => '', 'precision' => null],
        'modified' => ['type' => 'datetime', 'length' => null, 'null' => false, 'default' => null, 'comment' => '', 'precision' => null],
        '_indexes' => [
            'invoice_refunds_id' => ['type' => 'index', 'columns' => ['invoice_refunds_id'], 'length' => []],
            'line_item_id' => ['type' => 'index', 'columns' => ['line_item_id'], 'length' => []],
        ],
        '_constraints' => [
            'primary' => ['type' => 'primary', 'columns' => ['id'], 'length' => []],
        ],
        '_options' => [
            'engine' => 'InnoDB',
            'collation' => 'utf8mb4_unicode_ci'
        ],
    ];
    // @codingStandardsIgnoreEnd
    /**
     * Init method
     *
     * @return void
     */
    public function init()
    {
        $this->records = [
            [
                'id' => 1,
                'invoice_refunds_id' => 1,
                'line_item_id' => 1,
                'quantity' => 1,
                'price' => 1,
                'tax_rate' => 1,
                'created' => '2021-01-28 09:18:58',
                'modified' => '2021-01-28 09:18:58',
            ],
        ];
        parent::init();
    }
}
