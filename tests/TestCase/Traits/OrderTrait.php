<?php
namespace Store\Test\TestCase\Traits;

use Cake\Datasource\ConnectionManager;
use Cake\Controller\ComponentRegistry;
use Cake\TestSuite\TestCase;
use Store\Controller\Component\CartComponent;
use Cake\Controller\Controller;
use Cake\Network\Request;
use Cake\Network\Session;
use Cake\ORM\TableRegistry;
use Cake\Event\EventManager;
use Store\Config\StoreConfig;
use Store\Test\Table\ProductsTable;
use Website\Lib\Website;
use Cake\Cache\Cache;
use Cake\Core\Plugin;

trait OrderTrait
{
  public $fixtures = [
    'plugin.store.users',
    'plugin.store.groups',
    'plugin.store.groups_translations',
    'plugin.store.orders',
    'plugin.store.order_histories',
    'plugin.store.order_messages',
    'plugin.store.payment_methods',
    'plugin.store.payment_methods_translations',
    'plugin.store.line_items',
    'plugin.store.carriers',
    'plugin.store.carrier_rules',
    'plugin.store.carrier_rule_zones',
    'plugin.store.carrier_festives',
    'plugin.store.currencies',
    'plugin.store.coupons',
    'plugin.store.coupons_orders',
    'plugin.store.order_messages',
    'plugin.store.order_histories',
    'plugin.store.products',
    'plugin.store.product_attributes',
    'plugin.store.attributes',
    'plugin.store.attributes_translations',
    'plugin.store.attribute_groups_translations',
    'plugin.store.product_attribute_groups',
    'plugin.store.product_attribute_combinations',
    'plugin.store.attribute_groups',
    'plugin.store.taxes',
    'plugin.store.taxes_translations',
    'plugin.store.countries',
    'plugin.store.countries_translations',
    'plugin.store.states',
    'plugin.store.invoices',
    'plugin.store.receipts',
    'plugin.store.zones',
    'plugin.store.points',
    'plugin.store.tax_zones',
    'plugin.store.tax_ratings',
    'plugin.store.translates',
    'plugin.store.slugs',
    'plugin.store.addresses',
    'plugin.store.letters',
    'plugin.store.letters_translations',
    'plugin.store.sections',
    'plugin.store.sections_translations',
    'plugin.store.layouts',
    'plugin.store.sites',
    'plugin.store.sites_translations',
    'plugin.store.rows',
    'plugin.store.columns',
    'plugin.store.drafts',
    'plugin.store.email_queue',
  ];

  public function setSetup()
  {
    Cache::clearAll();
 
    StoreConfig::setConfig([
        'withAttributes' => property_exists( $this, 'methodsWithAttributes') && in_array( $this->getName(), $this->methodsWithAttributes),
    ]);
    
    $this->connection = ConnectionManager::get('test');
		$this->Products = new ProductsTable([
			'alias' => 'Products',
			'table' => 'products',
			'connection' => $this->connection
    ]);
    
    StoreConfig::setProductsModel( function(){
      return $this->Products;
    }, true, true);

    $this->Controller = new Controller( new Request(['session' => new Session()]));
    $registry = new ComponentRegistry( $this->Controller);
    $this->Cart = new CartComponent( $registry);
    $this->Controller->loadComponent( 'Auth');
    $this->Orders = TableRegistry::get( 'Store.Orders');
    $this->LineItems = TableRegistry::get( 'Store.LineItems');
    $this->Users = TableRegistry::get( 'User.Users');
    $this->Invoices = TableRegistry::get( 'Store.Invoices');
    $this->Receipts = TableRegistry::get( 'Store.Receipts');
    $this->storeConfig();    
    $this->storeSettings();    
    TableRegistry::get( 'Letter.Letters')->saveLetters();
  }

  public function setTearDown()
  {
    unset( $this->Cart);
    unset( $this->Controller);
    unset( $this->Orders);
    unset( $this->LineItems);
    unset( $this->ProductAttributes);
    unset( $this->Attributes);
    unset( $this->Users);
    unset( $this->Products);
    unset( $this->Invoices);
    unset( $this->Receipts);
  }

  public function storeConfig()
  {    
    StoreConfig::setHook([
      'invoicePrefix' => function( $invoice){
        return 'I'. date( 'Y'). $invoice->invoice_num;
      },
      'receiptPrefix' => function( $invoice){
        return 'R'. date( 'Y'). $invoice->invoice_num;
      },
      'itemDescription' => function( $item){
        return null;
      },
      'validateCheckout' => false
    ]);
  
    StoreConfig::setConfig([
        'withAttributes' => false,
        'itemFields' => [

        ],
        'orderDefaults' => [
          'same_address' => 0
        ],
    ]);
  }

  public function storeSettings( array $data = [])
  {
    $data = array_merge( [
      'store_name' => 'My Store',
      'store_cif' => '00000000F',
      'store_address' => 'Green Avenue',
      'store_postcode' => '000001',
      'store_city' => 'Pamplona',
      'store_state' => 'Navarra',
      'store_country' => 'España',
      'store_price_with_taxes' => true,
      'store_has_points' => false,
      'store_points_down_value' => '1',
      'store_points_up_value' => '.01',
      'store_points_charge_days' => '1',
      'store_invoice_prefix' => 'I-',
      'store_invoice_zeros' => 8,
      'store_invoice_first' => 1,
      'store_receipt_prefix' => 'R-',
      'store_receipt_zeros' => 8,
      'store_receipt_first' => 1,
      'store_invoice_when_status' => 'accepted',
      'store_gift_wrapping_price' => 20,
      'store_gift_wrapping_tax_id' => 1,
      'store_mandatory_fields_firstname' => true,
      'store_mandatory_fields_lastname' => true,
      'store_mandatory_fields_address' => true,
      'store_mandatory_fields_phone' => true,
      'store_mandatory_fields_postcode' => true,
      'store_mandatory_fields_vat_number' => true,
      'store_mandatory_fields_email_confirmation' => false,
    ], $data);
    
    $site = TableRegistry::get( 'Website.Sites')->find()->first();
    $settings = $site->settings;
    $settings = array_merge( (array)$settings, $data);
    $site->set( 'settings', (object)$settings);
    TableRegistry::get( 'Website.Sites')->save( $site);
    Website::set( $site);
  }

/**
 * Pone la tienda con atributos
 *
 * @return void
 */
  public function storeWithAttributes()
  {
    StoreConfig::setConfig([
      'withAttributes' => true,
    ]);

    $this->Products->setAttributesAssociations();
    StoreConfig::setProductsModel( function(){
      return $this->Products;
    }, true, true);
  }

/**
 * Añade un producto a la cesta
 *
 * @return void
 */
  public function addItem( $product_data = [])
  {
    $product = $this->Products->find()->first();

    foreach( $product_data as $key => $value)
    {
      $product->set( $key, $value);
    }

    $this->Products->save( $product);

    $data = [
      'salt' => $product->salt,
      'attributes' => [],
      'line_item' => [],
    ];
    
    // Añade un producto
    return $this->Cart->addItem( $data);
  }

  public function getOrder( $order_id)
  {
    $order = $this->Orders
    ->find( 'order')
    ->where([
      'Orders.id' => $order_id
    ])
    ->first();

    return $order;
  }

  public function finalizeOrder( $status, $invoice = false, $data = [])
  {
    $this->addItem();
    $this->setInvoiceAddress();
    $order = $this->Cart->order();
    
    if( !empty( $data))
    {
      foreach( $data as $key => $value)
      {
        $this->Orders
          ->query()
          ->update()
          ->set( $key, $value)
          ->where(['id' => $order->id])
          ->execute();
        
        $order = $this->Cart->order();
      }
    }

    if( $invoice)
    {
      $this->setOrderInvoice( $order);
    }

    $this->Orders->finalize( $this->Cart->order(), $status);
    
    $order = $this->Orders
      ->find( 'order')
      ->where([
        'Orders.id' => $order->id
      ])
      ->first();
    
    return $order;
  }

  public function setOrderInvoice( $order)
  {
    $this->Orders->query()->update()
      ->set( 'has_invoice', true)
      ->where(['id', $order->id])
      ->execute();
  }

/**
 * Añade un producto con atributos a la cesta
 *
 * @return void
 */
  public function addItemWithAttributes()
  {
    $this->storeWithAttributes();
    $product = $this->Products->find( 'attributes')
      ->first();
    
    $attributes = [];
    
    foreach( $product->product_attributes [0]->attributes as $attr)
    {
      $attributes [] = [
        'group_id' => $attr->attribute_group_id,
        'attribute_id' => $attr->id,
      ];
    }

    $data = [
      'salt' => $product->salt,
      'attributes' => $attributes,
      'line_item' => [],
    ];

    // Añade un producto
    return $this->Cart->addItem( $data);
  }

  public function setInvoiceAddress( $data = [])
  {
    $default = [
      'adr_invoice_email' => 'anton@arambis.com',
      'adr_delivery_firstname' => 'Anton',
      'adr_delivery_lastname' => 'Arambi',
      'adr_delivery_vat_number' => '111111P',
      'adr_delivery_country_id' => 1,
      'adr_delivery_state_id' => 1,
      'adr_delivery_postcode' => '31009',
      'adr_delivery_city' => 'Pamplona',
      'adr_delivery_phone' => '948 25 25 25',
      'adr_delivery_address' => 'San Juan 4',
      'same_address' => 1,
    ];

    $data = array_merge( $default, $data);

    $order = $this->Cart->order();
    $this->Orders->patchEntity( $order, $data);
  }

  

  public function login()
  {
    $user = $this->Users->find()->first();
    $this->Cart->Auth->setUser( $user->toArray());
  }

  public function pretty( $order)
  {
    $this->out( '================================================');
    foreach( $order->line_items as $item)
    {
      $this->out( $item->full_title ."\t". $item->price . ' x '. $item->quantity .' = '. $item->subtotal);
    }

    $this->out( '-------------------');
    $this->out( "Subtotal \t" . $order->subtotal);

    foreach( $order->line_taxes as $tax_rate => $total)
    {
      $this->out( $tax_rate . "% IVA\t" . $total);
    }

    $this->out( "Gastos de envío \t" . $order->shipping_handling);
    $this->out( $order->shipping_handling_tax_rate . '% ' . "IVA Gastos de envío \t" . $order->shipping_handling_tax);
    $this->out( "Total \t" . $order->total);
    $this->out( '================================================');
  }

  public function out( $string)
  {
    echo "\n" . $string;
  }

  public function getProductData( $id)
  {
    $product = $this->Products->find()->where([
      'Products.id' => $id,
    ])->contain([
      'ProductAttributes' => [
        'Attributes'
      ]
    ])->first();

    $product_attribute = $product->product_attributes [0];

    $attributes = [];

    foreach( $product_attribute->attributes as $attribute)
    {
      $attributes [$attribute->attribute_group_id] = $attribute->id;
    }

    return [
      'salt' => $product->salt,
      'quantity' => 1,
      'attributes' => $attributes
    ];
  }

  public function getOrderWithProduct( $id = 1)
  {
    $this->Cart->addItem( $this->getProductData( $id));
    return $this->Cart->order();
  }



}