<?php
namespace Store\Test\TestCase\View\Helper;

use Cake\TestSuite\TestCase;
use Cake\View\View;
use Store\View\Helper\FavHelper;

/**
 * Store\View\Helper\FavHelper Test Case
 */
class FavHelperTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \Store\View\Helper\FavHelper
     */
    public $Fav;

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $view = new View();
        $this->Fav = new FavHelper($view);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->Fav);

        parent::tearDown();
    }

    /**
     * Test initial setup
     *
     * @return void
     */
    public function testInitialization()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
