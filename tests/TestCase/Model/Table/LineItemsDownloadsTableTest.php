<?php
namespace Store\Test\TestCase\Model\Table;

use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;
use Store\Model\Table\LineItemsDownloadsTable;

/**
 * Store\Model\Table\LineItemsDownloadsTable Test Case
 */
class LineItemsDownloadsTableTest extends TestCase
{
    /**
     * Test subject
     *
     * @var \Store\Model\Table\LineItemsDownloadsTable
     */
    public $LineItemsDownloads;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'plugin.Store.LineItemsDownloads',
        'plugin.Store.LineItems',
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::getTableLocator()->exists('LineItemsDownloads') ? [] : ['className' => LineItemsDownloadsTable::class];
        $this->LineItemsDownloads = TableRegistry::getTableLocator()->get('LineItemsDownloads', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->LineItemsDownloads);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
