<?php
namespace Store\Test\TestCase\Model\Table;

use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;
use Store\Model\Table\CouponsTable;
use Manager\TestSuite\CrudTestCase;
use Store\Test\TestCase\Traits\OrderTrait;


/**
 * Store\Model\Table\CouponsTable Test Case
 */
class CouponsTableTest extends CrudTestCase
{
  use OrderTrait;
  
  /**
   * Test subject
   *
   * @var \Store\Model\Table\CouponsTable
   */
  public $Coupons;

  /**
   * Fixtures
   *
   * @var array
   */


  /**
   * setUp method
   *
   * @return void
   */
  public function setUp()
  {
    parent::setUp();
    $this->Coupons = TableRegistry::get( 'Store.Coupons');
    $this->setSetup();
  }

  /**
   * tearDown method
   *
   * @return void
   */
  public function tearDown()
  {
    $this->setTearDown();
    parent::tearDown();
  }

  public function createCouponValid()
  {
    $data = [
      'title' => 'Cupón de descuento',
      'code' => 'MOSCU',
      'type' => 'percent',
      'value' => 10,
      'active' => true,
      'start_on' => date( 'Y-m-d', strtotime( '-2 day')),
      'finish_on' => date( 'Y-m-d', strtotime( '+2 day')),
    ];

    return $this->Coupons->save( $this->Coupons->newEntity( $data));
  }

  public function testAddCoupon()
  {
    $coupon = $this->createCouponValid();

    $this->addItem([
      'store_price' => 100
    ]);
    $this->setInvoiceAddress();
    $order = $this->Cart->order();
    $this->setOrderInvoice( $order);
    $result = $this->Coupons->addToOrder( $coupon->code, $order->id);
    $this->assertTrue( array_key_exists( 'success', $result));
    $order = $this->Cart->order( true);
    $this->Orders->finalize( $this->Cart->order(), 'accepted');
    $order = $this->getOrder( $order->id);
    $this->assertEquals( 108.9, $order->total);
  }


  public function testAddCouponUsed()
  {
    $coupon = $this->createCouponValid();

    $this->addItem();
    $this->setInvoiceAddress();
    $order = $this->Cart->order();
    $this->setOrderInvoice( $order);
    $result = $this->Coupons->addToOrder( $coupon->code, $order->id);
    $result = $this->Coupons->addToOrder( $coupon->code, $order->id);
    $this->assertTrue( array_key_exists( 'error', $result));
  }
  
  public function testAddCouponUsedForUser()
  {
    $coupon = $this->createCouponValid();
    $this->addItem();
    $this->setInvoiceAddress();
    $order = $this->Cart->order();
    $this->setOrderInvoice( $order);
    $result = $this->Coupons->addToOrder( $coupon->code, $order->id, 1);
    $this->Orders->finalize( $this->Cart->order(), 'accepted');
    
    $this->Cart->clearSession();

    $this->addItem();
    $this->setInvoiceAddress();
    $order = $this->Cart->order();
    $result = $this->Coupons->addToOrder( $coupon->code, $order->id, 1);
    $this->assertTrue( array_key_exists( 'error', $result));
  }

  public function testAddCouponUsedForUserNoFinalized()
  {
    $coupon = $this->createCouponValid();
    $this->addItem();
    $this->setInvoiceAddress();
    $order = $this->Cart->order();
    $this->setOrderInvoice( $order);
    $result = $this->Coupons->addToOrder( $coupon->code, $order->id, 1);

    $this->Cart->clearSession();

    $this->addItem();
    $this->setInvoiceAddress();
    $order = $this->Cart->order();
    $result = $this->Coupons->addToOrder( $coupon->code, $order->id, 1);
    $this->assertTrue( array_key_exists( 'success', $result));
  }

}
