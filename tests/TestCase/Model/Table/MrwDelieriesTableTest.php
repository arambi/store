<?php
namespace Store\Test\TestCase\Model\Table;

use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;
use Store\Model\Table\MrwDelieriesTable;

/**
 * Store\Model\Table\MrwDelieriesTable Test Case
 */
class MrwDelieriesTableTest extends TestCase
{
    /**
     * Test subject
     *
     * @var \Store\Model\Table\MrwDelieriesTable
     */
    public $MrwDelieries;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'plugin.Store.MrwDelieries',
        'plugin.Store.Orders',
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::getTableLocator()->exists('MrwDelieries') ? [] : ['className' => MrwDelieriesTable::class];
        $this->MrwDelieries = TableRegistry::getTableLocator()->get('MrwDelieries', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->MrwDelieries);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
