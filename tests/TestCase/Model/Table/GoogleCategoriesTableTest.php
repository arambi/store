<?php
namespace Store\Test\TestCase\Model\Table;

use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;
use Store\Model\Table\GoogleCategoriesTable;

/**
 * Store\Model\Table\GoogleCategoriesTable Test Case
 */
class GoogleCategoriesTableTest extends TestCase
{
    /**
     * Test subject
     *
     * @var \Store\Model\Table\GoogleCategoriesTable
     */
    public $GoogleCategories;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'plugin.Store.GoogleCategories',
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::getTableLocator()->exists('GoogleCategories') ? [] : ['className' => GoogleCategoriesTable::class];
        $this->GoogleCategories = TableRegistry::getTableLocator()->get('GoogleCategories', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->GoogleCategories);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
