<?php
namespace Store\Test\TestCase\Model\Table;

use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;
use Store\Model\Table\RefundsTable;
use Manager\TestSuite\CrudTestCase;


/**
 * Store\Model\Table\RefundsTable Test Case
 */
class RefundsTableTest extends CrudTestCase
{
    /**
     * Test subject
     *
     * @var \Store\Model\Table\RefundsTable
     */
    public $Refunds;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'plugin.Store.Refunds',
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::getTableLocator()->exists('Refunds') ? [] : ['className' => RefundsTable::class];
        $this->Refunds = TableRegistry::getTableLocator()->get('Refunds', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->Refunds);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
          
      public function testConfig()
      {
        $this->assertCrudDataEdit( 'update', $this->Refunds);
        $this->assertCrudDataIndex( 'index', $this->Refunds);
      }
}
