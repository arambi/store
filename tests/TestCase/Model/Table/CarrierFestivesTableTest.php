<?php
namespace Store\Test\TestCase\Model\Table;

use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;
use Store\Model\Table\CarrierFestivesTable;
use Manager\TestSuite\CrudTestCase;


/**
 * Store\Model\Table\CarrierFestivesTable Test Case
 */
class CarrierFestivesTableTest extends CrudTestCase
{

    /**
     * Test subject
     *
     * @var \Store\Model\Table\CarrierFestivesTable
     */
    public $CarrierFestives;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'plugin.store.carrier_festives'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('CarrierFestives') ? [] : ['className' => CarrierFestivesTable::class];
        $this->CarrierFestives = TableRegistry::get('CarrierFestives', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->CarrierFestives);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
          
      public function testConfig()
      {
        $this->assertCrudDataEdit( 'update', $this->CarrierFestives);
        $this->assertCrudDataIndex( 'index', $this->CarrierFestives);
      }
}
