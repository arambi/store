<?php
namespace Store\Test\TestCase\Model\Table;

use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;
use Store\Model\Table\OrderUserActionsTable;
use Manager\TestSuite\CrudTestCase;


/**
 * Store\Model\Table\OrderUserActionsTable Test Case
 */
class OrderUserActionsTableTest extends CrudTestCase
{
    /**
     * Test subject
     *
     * @var \Store\Model\Table\OrderUserActionsTable
     */
    public $OrderUserActions;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'plugin.Store.OrderUserActions',
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::getTableLocator()->exists('OrderUserActions') ? [] : ['className' => OrderUserActionsTable::class];
        $this->OrderUserActions = TableRegistry::getTableLocator()->get('OrderUserActions', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->OrderUserActions);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
          
      public function testConfig()
      {
        $this->assertCrudDataEdit( 'update', $this->OrderUserActions);
        $this->assertCrudDataIndex( 'index', $this->OrderUserActions);
      }
}
