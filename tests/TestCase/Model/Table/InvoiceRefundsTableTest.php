<?php
namespace Store\Test\TestCase\Model\Table;

use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;
use Store\Model\Table\InvoiceRefundsTable;
use Manager\TestSuite\CrudTestCase;


/**
 * Store\Model\Table\InvoiceRefundsTable Test Case
 */
class InvoiceRefundsTableTest extends CrudTestCase
{
    /**
     * Test subject
     *
     * @var \Store\Model\Table\InvoiceRefundsTable
     */
    public $InvoiceRefunds;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'plugin.Store.InvoiceRefunds',
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::getTableLocator()->exists('InvoiceRefunds') ? [] : ['className' => InvoiceRefundsTable::class];
        $this->InvoiceRefunds = TableRegistry::getTableLocator()->get('InvoiceRefunds', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->InvoiceRefunds);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
          
      public function testConfig()
      {
        $this->assertCrudDataEdit( 'update', $this->InvoiceRefunds);
        $this->assertCrudDataIndex( 'index', $this->InvoiceRefunds);
      }
}
