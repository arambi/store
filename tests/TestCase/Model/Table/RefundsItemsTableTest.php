<?php
namespace Store\Test\TestCase\Model\Table;

use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;
use Store\Model\Table\RefundsItemsTable;
use Manager\TestSuite\CrudTestCase;


/**
 * Store\Model\Table\RefundsItemsTable Test Case
 */
class RefundsItemsTableTest extends CrudTestCase
{
    /**
     * Test subject
     *
     * @var \Store\Model\Table\RefundsItemsTable
     */
    public $RefundsItems;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'plugin.Store.RefundsItems',
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::getTableLocator()->exists('RefundsItems') ? [] : ['className' => RefundsItemsTable::class];
        $this->RefundsItems = TableRegistry::getTableLocator()->get('RefundsItems', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->RefundsItems);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
          
      public function testConfig()
      {
        $this->assertCrudDataEdit( 'update', $this->RefundsItems);
        $this->assertCrudDataIndex( 'index', $this->RefundsItems);
      }
}
