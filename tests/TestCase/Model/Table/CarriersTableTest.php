<?php
namespace Store\Test\TestCase\Model\Table;

use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;
use Store\Model\Table\CarriersTable;
use Store\Test\TestCase\Traits\OrderTrait;

/**
 * Store\Model\Table\CarriersTable Test Case
 */
class CarriersTableTest extends TestCase
{
  use OrderTrait;
  
  public function setUp()
  {
    parent::setUp();
    $this->Carriers = TableRegistry::get( 'Store.Carriers');
    $this->Countries = TableRegistry::get( 'Store.Countries');
    $this->Zones = TableRegistry::get( 'Store.Zones');
    $this->setSetup();
  }

  /**
   * tearDown method
   *
   * @return void
   */
  public function tearDown()
  {
    $this->setTearDown();
    parent::tearDown();
  }

  public function prepareData( $data = [])
  {
    $this->Carriers->deleteAll([]);
    $default = [
      'title' => 'Normal',
      'active' => 1,
      'by_default' => 1,
      'hour' => '19:30',
      'tax_id' => 1,
      'delivery_days_type' => 'business_days',
      'carrier_rules' => [
        [
          'active' => 1,
          'type' => 'weight',
          'price' => 20,
          'min' => 0,
          'max' => 100,
          'delivery_type' => 'normal',
          'deadline' => 3,
          'by_default' => 1,
          'zones' => [
            $this->Zones->find()->first()
          ]
        ],
        [
          'active' => 1,
          'type' => 'weight',
          'price' => 40,
          'min' => 101,
          'max' => 200,
          'delivery_type' => 'normal',
          'deadline' => 3,
          'by_default' => 0,
          'zones' => [
            $this->Zones->find()->first()
          ]
        ],
        [
          'active' => 1,
          'type' => 'price',
          'price' => 0,
          'min' => 100,
          'max' => 0,
          'delivery_type' => 'normal',
          'deadline' => 3,
          'by_default' => 0,
          'zones' => [
            $this->Zones->find()->first()
          ]
        ]
      ]
    ];

    $data = array_merge( $default, $data);
    $this->Carriers->save( $this->Carriers->newEntity( $data));
  }

  public function prepareProductWeight( $weight, $price)
  {
    $this->addItem([
      'store_weight' => $weight,
      'store_price' => $price,
    ]);
    
    $this->setInvoiceAddress([
      'delivery_zone_id' => $this->Zones->find()->first()->id
    ]);
    $this->Orders->saveOrder( $this->Cart->order());
    $order = $this->Cart->order( true);
    $this->Orders->build( $order);
    return $order;
  }

  public function testSetQuota()
  {
    $this->prepareData();
    $order = $this->prepareProductWeight( 50, 30);
    $this->assertEquals( 20, $order->shipping_handling);

    $this->Cart->clearSession();

    $this->prepareData();
    $order = $this->prepareProductWeight( 180, 30);
    $this->assertEquals( 40, $order->shipping_handling);

    $this->Cart->clearSession();
    
    $this->prepareData();
    $order = $this->prepareProductWeight( 180, 130);
    $this->assertEquals( 0, $order->shipping_handling);

    $this->Cart->clearSession();
    
    $this->prepareData();
    $order = $this->prepareProductWeight( 180, 130);
    $this->assertEquals( 0, $order->shipping_handling);
  }

  public function testDeliveryDays()
  {
    // Con fin de semana festivo
    $this->prepareData();
    $order = $this->prepareProductWeight( 20, 20);
    $this->assertEquals( 3, $order->delivery_days);
    $days = $order->delivery_days;

    if( time() > strtotime( $order->carrier->hour))
    {
      $days++;
    }

    if( in_array( date( 'N', strtotime( $order->delivery_date_start)), [3, 4, 5, 6]))
    {
      $days += 1;
    }


    $this->assertEquals( date( 'Y-m-d', strtotime( ' +' . $days .' day')), $order->delivery_date);

    // $this->Cart->clearSession();

    // // Sin fin de semana festivo
    // $this->prepareData([
    //   'delivery_days_type' => 'business_days',
    // ]);

    // $order = $this->prepareProductWeight( 20, 20);
    // $days = $order->delivery_days;
    
    // if( time() > strtotime( $order->carrier->hour))
    // {
    //   $days++;
    // }
    
    // $this->assertEquals( date( 'Y-m-d', strtotime( ' +' . $days .' day')), $order->delivery_date);
    
    // $this->Cart->clearSession();

    // // Con un día festivo
    // $this->prepareData([
    //   'delivery_days_type' => 'all_days',
    //   'carrier_festives' => [
    //     [
    //       'day' => date( 'Y-m-d', strtotime( '+1 day'))
    //     ]
    //   ]
    // ]);

    // $order = $this->prepareProductWeight( 20, 20);
    // $days = $order->delivery_days + 1;

    // if( time() > strtotime( $order->carrier->hour))
    // {
    //   $days++;
    // }
    
    // $this->assertEquals( date( 'Y-m-d', strtotime( ' +' . $days .' day')), $order->delivery_date);
    
    // $this->Cart->clearSession();
    
    // // Con un día festivo pero más adelante
    // $this->prepareData([
    //   'delivery_days_type' => 'all_days',
    //   'carrier_festives' => [
    //     [
    //       'day' => date( 'Y-m-d', strtotime( '+4 day'))
    //     ]
    //   ]
    // ]);

    // $order = $this->prepareProductWeight( 20, 20);
    // $days = $order->delivery_days;
    
    // if( time() > strtotime( $order->carrier->hour))
    // {
    //   $days++;
    // }

    // $this->assertEquals( date( 'Y-m-d', strtotime( ' +' . $days .' day')), $order->delivery_date);
    
  }

  public function testGetFreeRulePrice()
  {
    $this->prepareData();
    $this->prepareProductWeight( 50, 20);
    $price = $this->Carriers->getFreeRulePrice( $this->Cart->order( true));
    $this->assertEquals( 100, $price);
  }

  public function testGetFreeRulePriceNull()
  {
    $this->prepareData([
      'carrier_rules' => [
        [
          'active' => 1,
          'type' => 'weight',
          'price' => 20,
          'min' => 0,
          'max' => 100,
          'delivery_type' => 'normal',
          'deadline' => 3,
          'by_default' => 1,
          'zones' => [
            $this->Zones->find()->first()
          ]
        ],
      ]
    ]);

    $this->prepareProductWeight( 50, 20);
    $price = $this->Carriers->getFreeRulePrice( $this->Cart->order( true));
    $this->assertEquals( null, $price);
  }


  public function testSetForFree()
  {
    $this->prepareData();
    $this->prepareProductWeight( 50, 20);
    $price = $this->Carriers->getFreeRulePrice( $this->Cart->order( true));
    $order = $this->Cart->order( true);
    $this->assertEquals( 100, $order->shipping_handling_free_price);
    $this->assertEquals( 80, $order->shipping_handling_left_for_free);
  }

  public function testSetForFree2()
  {
    $this->prepareData();
    $this->prepareProductWeight( 50, 120);
    $price = $this->Carriers->getFreeRulePrice( $this->Cart->order( true));
    $order = $this->Cart->order( true);
    $this->assertEquals( 100, $order->shipping_handling_free_price);
    $this->assertEquals( null, $order->shipping_handling_left_for_free);
  }

  public function testSetForFree3()
  {
    $this->prepareData();
    $this->prepareProductWeight( 50, 120);
    $price = $this->Carriers->getFreeRulePrice( $this->Cart->order( true));
    $order = $this->Cart->order( true);
    $this->assertEquals( 100, $order->shipping_handling_free_price);
    $this->assertEquals( 0, $order->shipping_handling);
  }
  
}
