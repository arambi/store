<?php
namespace Store\Test\TestCase\Model\Table;

use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;
use Store\Model\Table\OrderUserSessionsTable;
use Manager\TestSuite\CrudTestCase;


/**
 * Store\Model\Table\OrderUserSessionsTable Test Case
 */
class OrderUserSessionsTableTest extends CrudTestCase
{
    /**
     * Test subject
     *
     * @var \Store\Model\Table\OrderUserSessionsTable
     */
    public $OrderUserSessions;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'plugin.Store.OrderUserSessions',
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::getTableLocator()->exists('OrderUserSessions') ? [] : ['className' => OrderUserSessionsTable::class];
        $this->OrderUserSessions = TableRegistry::getTableLocator()->get('OrderUserSessions', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->OrderUserSessions);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
          
      public function testConfig()
      {
        $this->assertCrudDataEdit( 'update', $this->OrderUserSessions);
        $this->assertCrudDataIndex( 'index', $this->OrderUserSessions);
      }
}
