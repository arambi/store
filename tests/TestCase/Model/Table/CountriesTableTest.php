<?php
namespace Store\Test\TestCase\Model\Table;

use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;
use Store\Model\Table\CountriesTable;
use Store\Test\TestCase\Traits\OrderTrait;


/**
 * Store\Model\Table\CountriesTable Test Case
 */
class CountriesTableTest extends TestCase
{
  use OrderTrait;
  
  /**
   * setUp method
   *
   * @return void
   */
  public function setUp()
  {
    parent::setUp();
    $this->setSetup();
  }

  /**
   * tearDown method
   *
   * @return void
   */
  public function tearDown()
  {
    $this->setTearDown();
    parent::tearDown();
  }

  /**
   * Test getFromRest method
   *
   * @return void
   */
  public function testGetFromRest()
  {
    $Countries = TableRegistry::get( 'Store.Countries');
    $Countries->getFromRest();

    $country = $Countries->getByCode( 'ES');
    $country->set( 'active', true);
    $Countries->save( $country);
    $this->assertEquals( 52, $Countries->States->find()->where(['country_id' => $country->id])->count());
  }
}
