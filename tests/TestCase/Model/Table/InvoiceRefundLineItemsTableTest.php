<?php
namespace Store\Test\TestCase\Model\Table;

use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;
use Store\Model\Table\InvoiceRefundLineItemsTable;

/**
 * Store\Model\Table\InvoiceRefundLineItemsTable Test Case
 */
class InvoiceRefundLineItemsTableTest extends TestCase
{
    /**
     * Test subject
     *
     * @var \Store\Model\Table\InvoiceRefundLineItemsTable
     */
    public $InvoiceRefundLineItems;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'plugin.Store.InvoiceRefundLineItems',
        'plugin.Store.InvoiceRefunds',
        'plugin.Store.LineItems',
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::getTableLocator()->exists('InvoiceRefundLineItems') ? [] : ['className' => InvoiceRefundLineItemsTable::class];
        $this->InvoiceRefundLineItems = TableRegistry::getTableLocator()->get('InvoiceRefundLineItems', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->InvoiceRefundLineItems);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
