<?php
namespace Store\Test\TestCase\Model\Table;

use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;
use Store\Model\Table\RefundsHistoriesTable;

/**
 * Store\Model\Table\RefundsHistoriesTable Test Case
 */
class RefundsHistoriesTableTest extends TestCase
{
    /**
     * Test subject
     *
     * @var \Store\Model\Table\RefundsHistoriesTable
     */
    public $RefundsHistories;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'plugin.Store.RefundsHistories',
        'plugin.Store.Users',
        'plugin.Store.Refunds',
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::getTableLocator()->exists('RefundsHistories') ? [] : ['className' => RefundsHistoriesTable::class];
        $this->RefundsHistories = TableRegistry::getTableLocator()->get('RefundsHistories', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->RefundsHistories);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
