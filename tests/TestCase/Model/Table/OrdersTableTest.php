<?php
namespace Store\Test\TestCase\Model\Table;

use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;
use Store\Model\Table\OrdersTable;
use Store\Test\TestCase\Traits\OrderTrait;

/**
 * Store\Model\Table\OrdersTable Test Case
 */
class OrdersTableTest extends TestCase
{
  use OrderTrait;

/**
   * setUp method
   *
   * @return void
   */
  public function setUp()
  {
    parent::setUp();
    $this->setSetup();
  }

  /**
   * tearDown method
   *
   * @return void
   */
  public function tearDown()
  {
    $this->setTearDown();
    parent::tearDown();
  }

  /**
   * Test finalize method
   *
   * @return void
   */
  public function testFinalize()
  {
    $this->addItem();
    $this->setInvoiceAddress();
    $order = $this->Cart->order();
    $this->Orders->patchEntity( $order, $order->toArray(), ['validate' => 'checkout']);
    $this->Orders->finalize( $this->Cart->order(), 'accepted');
    
    $order = $this->Orders
      ->find( 'order')
      ->where([
        'Orders.id' => $order->id
      ])
      ->first();
  }

  /**
   * Test createInvoice method
   *
   * @return void
   */
  public function testCreateInvoice()
  {
    $this->addItem();
    $this->setInvoiceAddress();
    $order = $this->Cart->order();
    $this->Orders->patchEntity( $order, $order->toArray(), ['validate' => 'checkout']);
    $this->Orders->finalize( $this->Cart->order(), 'accepted');
    
    $order = $this->Orders
      ->find( 'order')
      ->where([
        'Orders.id' => $order->id
      ])
      ->first();
    
    $this->assertEquals( 1, $order->receipt_id);

    $receipt = TableRegistry::get( 'Store.Receipts')->find()
      ->where([
        'Receipts.order_id' => $order->id
      ])
      ->first();
    
    $this->assertEquals( $order->id, $receipt->order_id);
  }

  public function testCreateInvoiceInvoice()
  {
    $this->addItem();
    $this->setInvoiceAddress();
    $order = $this->Cart->order();
    $this->Orders->query()->update()
      ->set( 'has_invoice', true)
      ->where(['id', $order->id])
      ->execute();

    $this->Orders->finalize( $this->Cart->order(), 'accepted');
    
    $order = $this->Orders
      ->find( 'order')
      ->where([
        'Orders.id' => $order->id
      ])
      ->first();
    
    $this->assertEquals( 1, $order->invoice_id);
    $this->Orders->addStatus( $order->salt, 'envoy');
    $this->assertEquals( 1, $order->invoice_id);
  }

}
