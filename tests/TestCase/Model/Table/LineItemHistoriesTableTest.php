<?php
namespace Store\Test\TestCase\Model\Table;

use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;
use Store\Model\Table\LineItemHistoriesTable;

/**
 * Store\Model\Table\LineItemHistoriesTable Test Case
 */
class LineItemHistoriesTableTest extends TestCase
{
    /**
     * Test subject
     *
     * @var \Store\Model\Table\LineItemHistoriesTable
     */
    public $LineItemHistories;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'plugin.Store.LineItemHistories',
        'plugin.Store.Users',
        'plugin.Store.LineItems',
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::getTableLocator()->exists('LineItemHistories') ? [] : ['className' => LineItemHistoriesTable::class];
        $this->LineItemHistories = TableRegistry::getTableLocator()->get('LineItemHistories', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->LineItemHistories);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
