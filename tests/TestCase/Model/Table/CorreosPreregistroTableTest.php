<?php
namespace Store\Test\TestCase\Model\Table;

use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;
use Store\Model\Table\CorreosPreregistroTable;

/**
 * Store\Model\Table\CorreosPreregistroTable Test Case
 */
class CorreosPreregistroTableTest extends TestCase
{
    /**
     * Test subject
     *
     * @var \Store\Model\Table\CorreosPreregistroTable
     */
    public $CorreosPreregistro;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'plugin.Store.CorreosPreregistro',
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::getTableLocator()->exists('CorreosPreregistro') ? [] : ['className' => CorreosPreregistroTable::class];
        $this->CorreosPreregistro = TableRegistry::getTableLocator()->get('CorreosPreregistro', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->CorreosPreregistro);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
