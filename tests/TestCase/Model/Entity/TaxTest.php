<?php
namespace Store\Test\TestCase\Model\Entity;

use Cake\TestSuite\TestCase;
use Store\Model\Entity\Tax;
use Store\Test\TestCase\Traits\OrderTrait;

/**
 * Store\Model\Entity\Tax Test Case
 */
class TaxTest extends TestCase
{
  use OrderTrait;

  /**
   * setUp method
   *
   * @return void
   */
  public function setUp()
  {
    parent::setUp();
    $this->setSetup();
  }

  /**
   * tearDown method
   *
   * @return void
   */
  public function tearDown()
  {
    $this->setTearDown();
    parent::tearDown();
  }

  public function testGetValue()
  {
    $product = $this->Products->find()->contain( 'Taxes')->first();
  }
}
