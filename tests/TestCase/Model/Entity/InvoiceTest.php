<?php
namespace Store\Test\TestCase\Model\Entity;

use Cake\TestSuite\TestCase;
use Store\Model\Entity\Invoice;
use Store\Test\TestCase\Traits\OrderTrait;
use Website\Lib\Website;


/**
 * Store\Model\Entity\Invoice Test Case
 */
class InvoiceTest extends TestCase
{

  use OrderTrait;
  
  /**
   * setUp method
   *
   * @return void
   */
  public function setUp()
  {
    parent::setUp();
    $this->setSetup();
  }

  /**
   * tearDown method
   *
   * @return void
   */
  public function tearDown()
  {
    $this->setTearDown();
    parent::tearDown();
  }

  private function __invoiceOrder( $address = [])
  {
    $this->addItem();
    $this->setInvoiceAddress( $address);
    $order = $this->Cart->order();
    $this->setOrderInvoice( $order);
    $this->Orders->finalize( $this->Cart->order(), Website::get( 'settings.store_invoice_when_status'));
    $order = $this->getOrder( $order->id);
    $invoice = $this->Invoices->find()
      ->where([
        'order_id' => $order->id
      ])
      ->contain('Orders')
      ->first();
    
    return [
      'order' => $order,
      'invoice' => $invoice
    ];
  }

  /**
   * Test _getName method
   *
   * @return void
   */
  public function testGetName()
  {
    $data = $this->__invoiceOrder([
      'adr_delivery_firstname' => 'Anton',
      'adr_delivery_lastname' => 'Arambi',
    ]);
    
    $this->assertEquals( 'Anton Arambi', $data ['invoice']->name);
  }

  public function testGetNameCompany()
  {
    $data = $this->__invoiceOrder([
      'adr_delivery_company' => 'Arambis S.L.',
    ]);

    $this->assertEquals( 'Arambis S.L.', $data ['invoice']->name);
  }

  public function testGetTotal()
  {
    $data = $this->__invoiceOrder();   
    $this->assertEquals( $data ['order']->total_human, $data ['invoice']->total_human);
  }

  public function testGetInvoiceUrl()
  {
    $data = $this->__invoiceOrder();   
    $this->assertEquals( $data ['order']->invoice_url, $data ['invoice']->invoice_url);
  }

  public function testGetInvoiceNumber()
  {
    $data = $this->__invoiceOrder();   
    $this->assertEquals( $data ['order']->invoice_number, $data ['invoice']->invoice_number);
  }

  public function testGetInvoiceDate()
  {
    $data = $this->__invoiceOrder();   
    $this->assertEquals( $data ['order']->invoice_date, $data ['invoice']->invoice_date);
  }

  
}
