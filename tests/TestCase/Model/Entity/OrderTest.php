<?php
namespace Store\Test\TestCase\Model\Entity;

use Cake\TestSuite\TestCase;
use Store\Model\Entity\Order;
use Store\Test\TestCase\Traits\OrderTrait;
use Store\Config\StoreConfig;
use Website\Lib\Website;
use Cake\ORM\TableRegistry;

/**
 * Store\Model\Entity\Order Test Case
 */
class OrderTest extends TestCase
{
  use OrderTrait;

  /**
   * setUp method
   *
   * @return void
   */
  public function setUp()
  {
    parent::setUp();
    $this->setSetup();
  }

  /**
   * tearDown method
   *
   * @return void
   */
  public function tearDown()
  {
    $this->setTearDown();
    parent::tearDown();
  }

  public function testGetDeliveryCountryId()
  {
    $this->addItem();
    $order = $this->Cart->order( true);
    $this->assertEquals( 6, $order->adr_delivery_country_id);
    $this->assertEquals( 9, $order->delivery_zone_id);

  }

  
  public function testGetInvoiceNumber()
  {
    StoreConfig::setHook( 'invoicePrefix', function( $invoice){
      return 'W'. date( 'Y'). $invoice->invoice_num;
    });

    $order = $this->finalizeOrder( 'accepted', true);
    $zeros = Website::get( 'settings.store_invoice_zeros');
    
    $num = sprintf('%0'. $zeros .'d', 1);
    $this->assertEquals( 'W'. date( 'Y'). $num, $order->invoice_number);

    $this->Cart->clearSession();

    $order = $this->finalizeOrder( 'accepted', true);
    $num = sprintf('%0'. $zeros .'d', 2);
    $this->assertEquals( 'W'. date( 'Y'). $num, $order->invoice_number);
  }

  public function testGetReceiptNumber()
  {
    StoreConfig::setHook( 'receiptPrefix', function( $invoice){
      return 'R'. date( 'Y'). $invoice->invoice_num;
    });

    $order = $this->finalizeOrder( 'accepted', false);
    $zeros = Website::get( 'settings.store_invoice_zeros');
    
    $num = sprintf('%0'. $zeros .'d', 1);
    $this->assertEquals( 'R'. date( 'Y'). $num, $order->invoice_number);

    $this->Cart->clearSession();

    $order = $this->finalizeOrder( 'accepted', false);
    $num = sprintf('%0'. $zeros .'d', 2);
    $this->assertEquals( 'R'. date( 'Y'). $num, $order->invoice_number);
  }

  public function testGetInvoiceType()
  {
    $order = $this->finalizeOrder( 'accepted', true);
    $this->assertEquals( 'invoice', $order->invoice_type);

    $this->Cart->clearSession();

    $order = $this->finalizeOrder( 'accepted', false);
    $this->assertEquals( 'receipt', $order->invoice_type);
  }  

  public function testGetInvoiceDate()
  {
    $order = $this->finalizeOrder( 'accepted', true);
    $date = strftime( Website::get( 'date_post_short'), $order->invoice->created->toUnixString());
    $this->assertEquals( $date, $order->invoice_date);
  }

  public function testGetInvoiceDate2()
  {
    $order = $this->finalizeOrder( 'accepted', false);
    $date = strftime( Website::get( 'date_post_short'), $order->receipt->created->toUnixString());
    $this->assertEquals( $date, $order->invoice_date);
  }
}
