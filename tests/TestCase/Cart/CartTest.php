<?php
namespace Store\Test\TestCase\Controller\Component;

use Cake\Datasource\ConnectionManager;
use Cake\Controller\ComponentRegistry;
use Cake\TestSuite\TestCase;
use Cake\ORM\Table;
use Cake\ORM\Entity;
use Store\Controller\Component\CartComponent;
use Cake\Controller\Controller;
use Cake\Network\Request;
use Cake\Network\Session;
use Cake\ORM\TableRegistry;
use Cake\Event\EventManager;
use Store\Cart\ItemDraft;
use Store\Cart\Cart;
use Store\Traits\ProductsTableTrait;
use Manager\Model\Entity\CrudEntityTrait;
use Store\Config\StoreConfig;
use Store\Test\TestCase\Traits\OrderTrait;


class CartTest extends TestCase
{
  use OrderTrait;

  /**
   * setUp method
   *
   * @return void
   */
  public function setUp()
  {
    parent::setUp();
    $this->TaxRatings = TableRegistry::get( 'Store.TaxRatings');
    $this->Countries = TableRegistry::get( 'Store.Countries');
    $this->States = TableRegistry::get( 'Store.States');
    $this->Taxes = TableRegistry::get( 'Store.Taxes');
    $this->setSetup();
  }

  /**
   * tearDown method
   *
   * @return void
   */
  public function tearDown()
  {
    $this->setTearDown();
    unset( $this->Ratings);
    parent::tearDown();
  }
  
  public function saveTaxRating( $data)
  {
    return $this->TaxRatings->save( $this->TaxRatings->newEntity( $data));
  }
  
  public function testSetOrder()
  {
    $order = $this->Orders->find( 'order')->first();
    Cart::setOrder( $order);
    $this->assertEquals( $order->id, Cart::getOrder()->id);
    $this->assertEquals( $order->adr_invoice_country_id, Cart::getInvoiceCountryId());
  }
  
  public function testGetAuthComponent()
  {
    $auth = Cart::getAuthComponent();
    $this->assertInstanceOf( \Cake\Controller\Component\AuthComponent::class, $auth);
  }

  public function testGetInvoiceCountryId()
  {
    $country_id = Cart::getInvoiceCountryId();
    $this->assertTrue( (bool)$country_id);
  }

  
  public function testTaxRating()
  {
    $tax = $this->Taxes->find()->first();
    
    // No hay nada
    $value = Cart::taxRating( $tax);
    $this->assertFalse( $value);

    // Hay un rating para el país actual del usuario
    $country_id = Cart::getInvoiceCountryId();

    $this->saveTaxRating([
      'country_id' => $country_id,
      'value' => 12,
      'tax_id' => $tax->id
    ]);

    $value = Cart::taxRating( $tax);    
    $this->assertEquals( 12, $value);
    
    $country = $this->Countries->find()
      ->where([
        'iso_code' => 'FR'
      ])
      ->first();
    
    // para generar los states
    $country->set( 'active', true);
    $this->Countries->save( $country);
    $state = $this->States->find()
      ->where([
        'country_id' => $country->id
      ])
      ->first();
    

    // El usuario hace un pedido con un país distinto al anterior
    $order = $this->finalizeOrder( 'accepted', false, [
      'adr_invoice_country_id' => $country->id,
      'adr_invoice_state_id' => $state->id,
    ]);

    $country_id = Cart::getInvoiceCountryId();

    $this->saveTaxRating([
      'country_id' => $country_id,
      'value' => 6,
      'tax_id' => $tax->id
    ]);
    
    $value = Cart::taxRating( $tax);    
    $this->assertEquals( 6, $value);
  }
}
