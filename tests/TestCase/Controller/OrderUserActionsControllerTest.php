<?php
namespace Store\Test\TestCase\Controller;

use Cake\TestSuite\IntegrationTestTrait;
use Cake\TestSuite\TestCase;
use Store\Controller\OrderUserActionsController;

/**
 * Store\Controller\OrderUserActionsController Test Case
 *
 * @uses \Store\Controller\OrderUserActionsController
 */
class OrderUserActionsControllerTest extends TestCase
{
    use IntegrationTestTrait;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'plugin.Store.OrderUserActions',
    ];

    /**
     * Test initial setup
     *
     * @return void
     */
    public function testInitialization()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
