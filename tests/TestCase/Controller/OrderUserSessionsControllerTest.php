<?php
namespace Store\Test\TestCase\Controller;

use Cake\TestSuite\IntegrationTestTrait;
use Cake\TestSuite\TestCase;
use Store\Controller\OrderUserSessionsController;

/**
 * Store\Controller\OrderUserSessionsController Test Case
 *
 * @uses \Store\Controller\OrderUserSessionsController
 */
class OrderUserSessionsControllerTest extends TestCase
{
    use IntegrationTestTrait;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'plugin.Store.OrderUserSessions',
    ];

    /**
     * Test initial setup
     *
     * @return void
     */
    public function testInitialization()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
