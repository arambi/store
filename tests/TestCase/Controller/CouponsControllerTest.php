<?php
namespace Store\Test\TestCase\Controller;

use Cake\TestSuite\IntegrationTestCase;
use Store\Controller\CouponsController;

/**
 * Store\Controller\CouponsController Test Case
 */
class CouponsControllerTest extends IntegrationTestCase
{

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'plugin.store.coupons'
    ];

    /**
     * Test initial setup
     *
     * @return void
     */
    public function testInitialization()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
