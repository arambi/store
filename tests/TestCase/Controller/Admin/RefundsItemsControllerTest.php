<?php
namespace Store\Test\TestCase\Controller\Admin;

use Cake\TestSuite\IntegrationTestTrait;
use Cake\TestSuite\TestCase;
use Store\Controller\Admin\RefundsItemsController;

/**
 * Store\Controller\Admin\RefundsItemsController Test Case
 *
 * @uses \Store\Controller\Admin\RefundsItemsController
 */
class RefundsItemsControllerTest extends TestCase
{
    use IntegrationTestTrait;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'plugin.Store.RefundsItems',
    ];

    /**
     * Test initial setup
     *
     * @return void
     */
    public function testInitialization()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
