<?php
namespace Store\Test\TestCase\Controller\Admin;

use Cake\TestSuite\IntegrationTestTrait;
use Cake\TestSuite\TestCase;
use Store\Controller\Admin\InvoiceRefundsController;

/**
 * Store\Controller\Admin\InvoiceRefundsController Test Case
 *
 * @uses \Store\Controller\Admin\InvoiceRefundsController
 */
class InvoiceRefundsControllerTest extends TestCase
{
    use IntegrationTestTrait;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'plugin.Store.InvoiceRefunds',
    ];

    /**
     * Test initial setup
     *
     * @return void
     */
    public function testInitialization()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
