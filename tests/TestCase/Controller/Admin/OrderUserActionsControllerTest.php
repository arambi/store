<?php
namespace Store\Test\TestCase\Controller\Admin;

use Cake\TestSuite\IntegrationTestTrait;
use Cake\TestSuite\TestCase;
use Store\Controller\Admin\OrderUserActionsController;

/**
 * Store\Controller\Admin\OrderUserActionsController Test Case
 *
 * @uses \Store\Controller\Admin\OrderUserActionsController
 */
class OrderUserActionsControllerTest extends TestCase
{
    use IntegrationTestTrait;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'plugin.Store.OrderUserActions',
    ];

    /**
     * Test initial setup
     *
     * @return void
     */
    public function testInitialization()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
