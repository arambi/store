<?php
namespace Store\Test\TestCase\Controller;

use Cake\TestSuite\IntegrationTestCase;
use Store\Controller\CarrierFestivesController;

/**
 * Store\Controller\CarrierFestivesController Test Case
 */
class CarrierFestivesControllerTest extends IntegrationTestCase
{

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'plugin.store.carrier_festives'
    ];

    /**
     * Test initial setup
     *
     * @return void
     */
    public function testInitialization()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
