<?php
namespace Store\Test\TestCase\Controller\Component;

use Cake\Controller\ComponentRegistry;
use Cake\TestSuite\TestCase;
use Store\Controller\Component\CartComponent;
use Store\Test\TestCase\Traits\OrderTrait;

/**
 * Store\Controller\Component\CartComponent Test Case
 */
class CartComponentTest extends TestCase
{
  use OrderTrait;
  
  public $methodsWithAttributes = [
    'testAddItemAttributes',
    'testItemQuantity',
    'testDeleteItem',
    // 'testAddItem'
  ];

  /**
   * setUp method
   *
   * @return void
   */
  public function setUp()
  {
    parent::setUp();
    $this->setSetup();
  }

  /**
   * tearDown method
   *
   * @return void
   */
  public function tearDown()
  {
    $this->setTearDown();
    parent::tearDown();
  }

  

  /**
   * Test addItem method
   *
   * @return void
   */
  public function testAddItem()
  {
    $product = $this->Products->find()->first();
    $data = [
      'salt' => $product->salt,
      'attributes' => [],
      'line_item' => [],
    ];
    
    // Añade un producto
    $result = $this->Cart->addItem( $data);
    $this->assertEquals( $product->id, $result ['item']['product_id']);
    $this->assertEquals( 1, $result ['item']['quantity']);
    $this->assertEquals( 1, count( $result ['order']['line_items']));
    
    // Añade el mismo producto
    $result = $this->Cart->addItem( $data);
    $this->assertEquals( 2, $result ['item']['quantity']);

    // Añade el mismo producto con cantidad 2
    $data ['quantity'] = 2;
    $result = $this->Cart->addItem( $data);
    $this->assertEquals( 4, $result ['item']['quantity']);

    $data ['quantity'] = 7;
    $result = $this->Cart->addItem( $data);
    $this->assertEquals( 'Actualmente solo disponemos de 10 artículos para el producto solicitado', $result ['errors'][0]);
  }

  public function testAddItemAttributes()
  {
    $this->storeWithAttributes();
    $product = $this->Products->find( 'attributes')
      ->first();
    
    $attributes = [];
    
    foreach( $product->product_attributes [0]->attributes as $attr)
    {
      $attributes [] = [
        'group_id' => $attr->attribute_group_id,
        'attribute_id' => $attr->id,
      ];
    }

    $data = [
      'salt' => $product->salt,
      'attributes' => $attributes,
      'line_item' => [],
    ];

    // Añade un producto
    $result = $this->Cart->addItem( $data);
    $this->assertEquals( $product->id, $result ['item']['product_id']);
    $this->assertEquals( $product->product_attributes [0]->id, $result ['item']['product_attribute_id']);
    $this->assertEquals( 1, $result ['item']['quantity']);
    
    // Añade el mismo producto
    $result = $this->Cart->addItem( $data);
    $this->assertEquals( 2, $result ['item']['quantity']);

    // Añade el mismo producto con cantidad 2
    $data ['quantity'] = 2;
    $result = $this->Cart->addItem( $data);
    $this->assertEquals( 4, $result ['item']['quantity']);

    $data ['attributes'][0]['attribute_id'] = 90;
    $result = $this->Cart->addItem( $data);
    $this->assertEquals( 'El producto no existe con las opciones seleccionadas', $result ['errors'][0]);
  }


/**
 * Añade producto sin atributos en una tienda con atributos
 *
 * @return void
 */
  public function testAddItemAttributes2()
  {
    $this->storeWithAttributes();
    $product = $this->Products->find()
      ->where([
        'Products.id' => 2
      ])
      ->first();
    
    $attributes = [];

    $data = [
      'salt' => $product->salt,
      'attributes' => $attributes,
      'line_item' => [],
    ];

    // Añade un producto
    $result = $this->Cart->addItem( $data);
    $this->assertEquals( $product->id, $result ['item']['product_id']);
    $this->assertEquals( 1, $result ['item']['quantity']);
    
  }
  
  

  /**
   * Test itemQuantity method
   *
   * @return void
   */
  public function testItemQuantity()
  {
    $this->storeWithAttributes();
    $result = $this->addItemWithAttributes();
    $order = $this->Cart->order();

    // Añade un item más
    $this->assertEquals( 1, count( $order->line_items));
    $order = $this->Cart->itemQuantity( $order->line_items [0]->id, 'add');
    $this->assertEquals( 2, $order->line_items [0]->quantity);    

    // Quita un item
    $order = $this->Cart->itemQuantity( $order->line_items [0]->id, 'remove');
    $this->assertEquals( 1, $order->line_items [0]->quantity);    
    
    // Añade dos items
    $order = $this->Cart->itemQuantity( $order->line_items [0]->id, 'add', 2);
    $this->assertEquals( 3, $order->line_items [0]->quantity);    

    // Añade más items de los que hay
    $result = $this->Cart->itemQuantity( $order->line_items [0]->id, 'add', 200);
    $this->assertEquals( 'Actualmente solo disponemos de 5 artículos para la combinación solicitada', $result ['errors'][0]);
  }

/**
 * Test deleteItem method
 *
 * @return void
 */
  public function testDeleteItem()
  {
    $this->storeWithAttributes();
    $result = $this->addItemWithAttributes();
    $order = $this->Cart->order();

    $this->assertEquals( 1, count( $order->line_items));
    $this->Cart->deleteItem( $order->line_items [0]->id);
    $order = $this->Cart->order();
    $this->assertEquals( 0, count( $order->line_items));    
  }

}
