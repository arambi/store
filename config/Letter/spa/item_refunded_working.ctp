Hola, 
El producto {title} del pedido <a href="{order_url}">{order_ref}</a> está en proceso de devolución a la tienda.

Puedes revisar tu pedido de desde Listado de pedidos de tu cuenta de cliente o en el siguiente <a href="{order_url}">enlace</a>.
