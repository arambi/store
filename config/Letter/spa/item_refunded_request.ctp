Hola,

Se ha recibido una solicitud de cancelación de pedido {web_name} con la referencia de pedido {order_ref}.

El producto es:
{title}

Puedes revisar tu pedido de desde Listado de pedidos de tu cuenta de cliente o en el siguiente <a href="{order_url}">enlace</a>.

