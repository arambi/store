<h2>Pedido aceptado</h2>

Hola {name},

Tu pedido en {web_name} con la referencia {order_ref} ha sido aceptado

Puedes revisar tu pedido de desde Listado de pedidos de tu cuenta de cliente o en el siguiente <a href="{order_url}">enlace</a>.
También puedes descargarte el <a href="{invoice_url}">PDF</a>.

Gracias por confiar en {web_name}.