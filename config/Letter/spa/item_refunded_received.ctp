Hola, 

Se ha recibido en {webname} el producto {title} del pedido <a href="{order_url}">{order_ref}</a> para su devolución.

Puedes revisar tu pedido de desde Listado de pedidos de tu cuenta de cliente o en el siguiente <a href="{order_url}">enlace</a>.
