Hola,

La solicitud de devolución de {title} del pedido <a href="{order_url}">{order_ref}</a> en {web_name} ha sido cancelada.
Puedes revisar tu pedido de desde Listado de pedidos de tu cuenta de cliente o en el siguiente <a href="{order_url}">enlace</a>.
