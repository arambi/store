Hola {name}, 

Tu pedido con la referencia {order_ref} de {web_name} está pendiente de confirmación bancaria. 

Puedes revisar tu pedido de desde Listado de pedidos de tu cuenta de cliente o en el siguiente <a href="{order_url}">enlace</a>.
También puedes descargarte el <a href="{invoice_url}">PDF</a>.

Gracias por confiar en {web_name}.
