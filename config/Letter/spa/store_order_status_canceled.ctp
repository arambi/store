
<h2>Pedido cancelado</h2>

Tu pedido con la referencia {order_ref} de {web_name} ha sido cancelado. 

Puedes revisar tu pedido de desde Listado de pedidos de tu cuenta de cliente o en el siguiente <a href="{order_url}">enlace</a>.
También puedes descargarte el <a href="{invoice_url}">PDF</a>.

Gracias por confiar en {web_name}.
