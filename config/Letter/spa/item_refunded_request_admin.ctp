Hola,

Se ha recibido una solicitud de cancelación de pedido {web_name} con la referencia de pedido {order_ref}.

El producto es:
{title}

Puedes ir al administrador del web para gestionar su devolución.

