<?php
use Migrations\AbstractMigration;

class CorreosTrack extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     * @return void
     */
    public function change()
    {
        $this->table('store_orders')
            ->addColumn('correos_track_number', 'string', ['null' => true, 'default' => null, 'limit' => 64])
            ->addIndex('correos_track_number')
            ->update();
    }
}
