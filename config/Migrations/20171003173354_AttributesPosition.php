<?php
use Migrations\AbstractMigration;

class AttributesPosition extends AbstractMigration
{
  /**
   * Change Method.
   *
   * More information on this method is available here:
   * http://docs.phinx.org/en/latest/migrations.html#the-change-method
   * @return void
   */
  public function change()
  {
    $attributes = $this->table( 'store_attributes');
    $attributes
      ->addColumn( 'position', 'integer', ['limit' => 2, 'null' => true, 'default' => null])
      ->addIndex( 'position')
      ->update();
  }
}
