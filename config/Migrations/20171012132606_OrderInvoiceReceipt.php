<?php
use Migrations\AbstractMigration;

class OrderInvoiceReceipt extends AbstractMigration
{
  /**
   * Change Method.
   *
   * More information on this method is available here:
   * http://docs.phinx.org/en/latest/migrations.html#the-change-method  
   * @return void
   */
  public function change()
  {
    $orders = $this->table( 'store_orders');
    $orders 
      ->addColumn( 'invoice_id', 'integer', ['null' => true, 'default' => null])
      ->addColumn( 'receipt_id', 'integer', ['null' => true, 'default' => null])
      ->update();
  }
}
