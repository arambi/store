<?php
use Migrations\AbstractMigration;

class InvoicesNumInteger extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     * @return void
     */
    public function change()
    {
        $this->table('store_invoices')
            ->changeColumn('num', 'integer', ['null' => true, 'default' => null])
            ->update();
    }
}
