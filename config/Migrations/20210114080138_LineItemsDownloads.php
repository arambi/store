<?php
use Migrations\AbstractMigration;

class LineItemsDownloads extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     * @return void
     */
    public function change()
    {
        $this->table('store_line_items_downloads')
            ->addColumn('line_item_id', 'integer', ['default' => null, 'null' => true])
            ->addColumn('created', 'datetime', ['default' => null, 'null' => true])
            ->create();
    }
}
