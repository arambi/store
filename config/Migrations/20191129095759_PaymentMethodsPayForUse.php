<?php
use Migrations\AbstractMigration;

class PaymentMethodsPayForUse extends AbstractMigration
{
    /**
   * Change Method.
   *
   * More information on this method is available here:
   * http://docs.phinx.org/en/latest/migrations.html#the-change-method
   * @return void
   */
  public function change()
  {
    $payment_methods = $this->table( 'store_payment_methods');
    $payment_methods
      ->changeColumn( 'pay_for_used', 'boolean', ['null' => true, 'default' => 0])
      ->addColumn( 'cost_type', 'string', ['null' => true, 'default' => NULL, 'limit' => 50])
      ->update();
  }
}
