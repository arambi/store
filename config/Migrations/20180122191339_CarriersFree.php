<?php
use Migrations\AbstractMigration;

class CarriersFree extends AbstractMigration
{
  /**
   * Change Method.
   *
   * More information on this method is available here:
   * http://docs.phinx.org/en/latest/migrations.html#the-change-method
   * @return void
   */
  public function change()
  {
    $carriers = $this->table( 'store_carriers');
    $carriers
      ->addColumn( 'is_free', 'boolean', ['null' => false, 'default' => 0])
      ->update();
  }
}
