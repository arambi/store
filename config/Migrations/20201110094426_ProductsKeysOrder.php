<?php
use Store\Config\StoreConfig;
use Migrations\AbstractMigration;

class ProductsKeysOrder extends AbstractMigration
{

    public function up()
    {
        $productsTable = StoreConfig::getProductsModel();
        $table = $this->table($productsTable->getTable());

        if (!$table->hasColumn('store_price_order')) {
            $table
                ->addColumn( 'store_price_order', 'float', ['precision' => 20, 'scale' => 6, 'null' => true, 'default' => 0])
                ->addIndex('store_price_order')
                ->update();
        }

        if (!$table->hasColumn('store_sales_count')) {
            $table
                ->addColumn( 'store_sales_count', 'integer', ['null' => true, 'default' => 0])
                ->update();
        }
    }

    public function down()
    {
        $productsTable = StoreConfig::getProductsModel();
        $table = $this->table($productsTable->getTable());

        if ($table->hasColumn('store_price_order')) {
            $table
                ->removeColumn('store_price_order')
                ->removeIndex('store_price_order')
                ->removeIndex('store_sales_count')
                ->update();
        }
    }
}
