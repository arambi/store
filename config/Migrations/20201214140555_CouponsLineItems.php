<?php

use Migrations\AbstractMigration;

class CouponsLineItems extends AbstractMigration
{

    public function change()
    {
        $this->table('store_coupons')
            ->addColumn('is_giftcard', 'boolean', ['null' => false, 'default' => 0])
            ->addIndex(['is_giftcard'])
            ->update();
    }
}
