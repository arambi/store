<?php

use Phinx\Migration\AbstractMigration;

class StoreInit extends AbstractMigration
{
  public function up()
  {
    // Atributos
    $attribute_groups = $this->table( 'store_attribute_groups');
    $attribute_groups
      ->addColumn( 'title', 'string', ['null' => true, 'default' => null, 'limit' => 255])
      ->addColumn( 'store_title', 'string', ['null' => true, 'default' => null, 'limit' => 255])
      ->addColumn( 'is_color_group', 'boolean', ['null' => false, 'default' => 0])
      ->addColumn( 'has_product_photo', 'boolean', ['null' => false, 'default' => 0])
      ->addColumn( 'color', 'string', ['null' => true, 'default' => NULL, 'limit' => 32])
      ->addColumn( 'group_type', 'enum', ['values' => ['select', 'radio', 'color'], 'null' => false, 'default' => 'select'])
      ->addColumn( 'salt', 'string', ['null' => true, 'default' => null, 'limit' => 255])


      ->addColumn( 'created', 'datetime', ['default' => null])
      ->addColumn( 'modified', 'datetime', ['default' => null])
      ->addIndex( ['is_color_group'])
      ->save();

    $attribute_groups_translations = $this->table( 'store_attribute_groups_translations', ['id' => false, 'primary_key' => ['id', 'locale']]);
    $attribute_groups_translations
      ->addColumn( 'id', 'integer', ['null' => false])
      ->addColumn( 'locale', 'string', ['null' => true, 'default' => null, 'limit' => 5])
      ->addColumn( 'title', 'string', ['null' => true, 'default' => null])
      ->addColumn( 'store_title', 'string', ['null' => true, 'default' => null])
      ->save();  

    $attributes = $this->table( 'store_attributes');
    $attributes
      ->addColumn( 'title', 'string', ['null' => true, 'limit' => 255])
      ->addColumn( 'attribute_group_id', 'integer', ['null' => true, 'default' => null])
      ->addColumn( 'color', 'string', ['null' => true, 'default' => NULL, 'limit' => 32])
      ->addColumn( 'salt', 'string', ['null' => true, 'default' => null, 'limit' => 255])


      ->addColumn( 'created', 'datetime', ['default' => null])
      ->addColumn( 'modified', 'datetime', ['default' => null])
      ->addIndex( ['attribute_group_id'])
      ->save();

    $attributes_translations = $this->table( 'store_attributes_translations', ['id' => false, 'primary_key' => ['id', 'locale']]);
    $attributes_translations
      ->addColumn( 'id', 'integer', ['null' => false])
      ->addColumn( 'locale', 'string', ['null' => true, 'default' => null, 'limit' => 5])
      ->addColumn( 'title', 'string', ['null' => true, 'default' => null])
      ->save();  

    $product_attributes = $this->table( 'store_product_attributes');
    $product_attributes
      ->addColumn( 'product_id', 'integer', ['null' => true, 'default' => null])
      ->addColumn( 'store_price', 'float', ['precision' => 20, 'scale' => 6, 'null' => true, 'default' => null])
      ->addColumn( 'store_weight', 'float', ['null' => true, 'default' => NULL])
      ->addColumn( 'minimal_quantity', 'integer', ['null' => true, 'default' => NULL])
      ->addColumn( 'store_reference', 'string', ['null' => true, 'default' => NULL, 'limit' => 32])
      ->addColumn( 'store_ean13', 'string', ['null' => true, 'default' => NULL, 'limit' => 13])
      ->addColumn( 'store_upc', 'string', ['null' => true, 'default' => NULL, 'limit' => 12])
      ->addColumn( 'photo_id', 'string', ['null' => true, 'default' => NULL, 'limit' => 12])
      ->addColumn( 'quantity', 'integer', ['null' => true, 'default' => NULL, 'limit' => 10])
      ->addColumn( 'available_date', 'date', ['null' => true, 'default' => null])
      ->addColumn( 'published', 'boolean', ['null' => false, 'default' => 1])
      ->addIndex( ['published'])
      ->addIndex( ['product_id'])
      ->addIndex( ['store_reference'])
      ->save();

    $product_attribute_combinations = $this->table( 'store_product_attribute_combinations');
    $product_attribute_combinations
      ->addColumn( 'attribute_id', 'integer', ['null' => true, 'default' => null])
      ->addColumn( 'product_attribute_id', 'integer', ['null' => true, 'default' => null])
      ->addColumn( 'salt', 'string', ['null' => true, 'default' => null, 'limit' => 255])


      ->addIndex( ['attribute_id'])
      ->addIndex( ['product_attribute_id'])
      ->save();

    $relateds = $this->table( 'store_relateds');
    $relateds
      ->addColumn( 'product_id', 'integer', ['null' => true, 'default' => null])
      ->addColumn( 'related_id', 'integer', ['null' => true, 'default' => null])
      ->addIndex( ['product_id'])
      ->addIndex( ['related_id'])
      ->save();

    $brands = $this->table( 'store_brands');
    $brands
      ->addColumn( 'title', 'string', ['null' => true, 'default' => null, 'limit' => 255])
      ->addColumn( 'active', 'boolean', ['null' => false, 'default' => 0])
      ->addColumn( 'logo', 'text', ['default' => NULL, 'null' => true])
      ->addColumn( 'salt', 'string', ['null' => true, 'default' => null, 'limit' => 255])
      ->addColumn( 'body', 'text', ['default' => NULL, 'null' => true])
      ->addColumn( 'published', 'boolean', ['null' => true, 'default' => 1])
      ->addColumn( 'slug', 'string', ['null' => true, 'default' => null])
      ->addIndex( ['salt'])
      ->addIndex( ['published'])
      ->addIndex( ['slug'])
      ->save();

    $brands_translations = $this->table( 'store_brands_translations', ['id' => false, 'primary_key' => ['id', 'locale']]);
    $brands_translations
      ->addColumn( 'id', 'integer', ['null' => false])
      ->addColumn( 'locale', 'string', ['null' => true, 'default' => null, 'limit' => 5])
      ->addColumn( 'body', 'text', ['null' => true, 'default' => null])
      ->save();  

    // Monedas
    $currencies = $this->table( 'store_currencies');
    $currencies
      ->addColumn( 'title', 'string', ['null' => true, 'default' => null, 'limit' => 255])
      ->addColumn( 'iso_code', 'string', ['null' => true, 'default' => null, 'limit' => 3])
      ->addColumn( 'iso_code_num', 'string', ['null' => true, 'default' => null, 'limit' => 3])
      ->addColumn( 'active', 'boolean', ['null' => false, 'default' => 0])
      ->addColumn( 'sign', 'string', ['null' => true, 'default' => null, 'limit' => 8])
      ->addColumn( 'blank', 'boolean', ['null' => false, 'default' => 0])
      ->addColumn( 'format', 'string', ['null' => false, 'default' => null, 'limit' => 20])
      ->addColumn( 'decimals', 'boolean', ['null' => false, 'default' => 0])
      ->addColumn( 'conversion_rate', 'decimal', ['null' => true, 'default' => null])
      ->addColumn( 'display_tax_label', 'boolean', ['null' => false, 'default' => 0])
      ->addColumn( 'salt', 'string', ['null' => true, 'default' => null, 'limit' => 255])
      ->addColumn( 'deleted', 'boolean', ['null' => false, 'default' => 0])
      ->addColumn( 'created', 'datetime', ['default' => null])
      ->addColumn( 'modified', 'datetime', ['default' => null])
      ->addIndex( ['iso_code'])
      ->addIndex( ['active'])
      ->save();


    // Zonas
    $zones = $this->table( 'store_zones');
    $zones
      ->addColumn( 'title', 'string', ['null' => true, 'default' => null, 'limit' => 255])
      ->addColumn( 'active', 'boolean', ['null' => false, 'default' => 0])
      ->addColumn( 'salt', 'string', ['null' => true, 'default' => null, 'limit' => 255])
      ->addColumn( 'created', 'datetime', ['default' => null])
      ->addColumn( 'modified', 'datetime', ['default' => null])
      ->save();


    // Países
    $countries = $this->table( 'store_countries');
    $countries
      ->addColumn( 'title', 'string', ['null' => true, 'default' => null, 'limit' => 255])
      ->addColumn( 'zone_id', 'integer', ['null' => true, 'default' => null])
      ->addColumn( 'currency_id', 'integer', ['null' => true, 'default' => null])
      ->addColumn( 'iso_code', 'string', ['null' => true, 'default' => null, 'limit' => 3])
      ->addColumn( 'call_prefix', 'integer', ['null' => true, 'default' => null, 'limit' => 3])
      ->addColumn( 'active', 'boolean', ['null' => false, 'default' => 0])
      ->addColumn( 'contains_states', 'boolean', ['null' => false, 'default' => 0])
      ->addColumn( 'need_identification_number', 'boolean', ['null' => false, 'default' => 0])
      ->addColumn( 'need_zip_code', 'boolean', ['null' => true, 'default' => null, 'limit' => 3])
      ->addColumn( 'zip_code_format', 'string', ['null' => false, 'default' => 0])
      ->addColumn( 'display_tax_label', 'boolean', ['null' => false, 'default' => 0])
      ->addColumn( 'salt', 'string', ['null' => true, 'default' => null, 'limit' => 255])
      ->addColumn( 'created', 'datetime', ['default' => null])
      ->addColumn( 'modified', 'datetime', ['default' => null])
      ->addIndex( ['active'])
      ->addIndex( ['zone_id'])
      ->addIndex( ['currency_id'])
      ->addIndex( ['iso_code'])
      ->save();

    $countries_translations = $this->table( 'store_countries_translations', ['id' => false, 'primary_key' => ['id', 'locale']]);
    $countries_translations
      ->addColumn( 'id', 'integer', ['null' => false])
      ->addColumn( 'locale', 'string', ['null' => true, 'default' => null, 'limit' => 5])
      ->addColumn( 'title', 'string', ['null' => true, 'default' => null])
      ->save();    

    // Provincias
    $states = $this->table( 'store_states');
    $states
      ->addColumn( 'title', 'string', ['null' => true, 'default' => null, 'limit' => 255])
      ->addColumn( 'country_id', 'integer', ['null' => true, 'default' => null])
      ->addColumn( 'zone_id', 'integer', ['null' => true, 'default' => null])
      ->addColumn( 'iso_code', 'string', ['null' => true, 'default' => null, 'limit' => 3])
      ->addColumn( 'active', 'boolean', ['null' => false, 'default' => 0])
      ->addColumn( 'salt', 'string', ['null' => true, 'default' => null, 'limit' => 255])
      ->addColumn( 'created', 'datetime', ['default' => null])
      ->addColumn( 'modified', 'datetime', ['default' => null])
      ->addIndex( ['zone_id'])
      ->addIndex( ['country_id'])
      ->addIndex( ['iso_code'])
      ->save();

    // Impuestos
    $taxes = $this->table( 'store_taxes');
    $taxes
      ->addColumn( 'title', 'string', ['null' => true, 'default' => null, 'limit' => 255])
      ->addColumn( 'value', 'float', ['precision' => 20, 'scale' => 6, 'null' => false, 'default' => 0])
      ->addColumn( 'multizone', 'boolean', ['null' => false, 'default' => 0])
      ->addColumn( 'salt', 'string', ['null' => true, 'default' => null, 'limit' => 255])
      ->addColumn( 'active', 'boolean', ['null' => false, 'default' => 0])
      ->addColumn( 'created', 'datetime', ['default' => null])
      ->addColumn( 'modified', 'datetime', ['default' => null])
      ->addIndex( ['multizone'])
      ->save();

    $taxes_translations = $this->table( 'store_taxes_translations', ['id' => false, 'primary_key' => ['id', 'locale']]);
    $taxes_translations
      ->addColumn( 'id', 'integer', ['null' => false])
      ->addColumn( 'locale', 'string', ['null' => true, 'default' => null, 'limit' => 5])
      ->addColumn( 'title', 'string', ['null' => true, 'default' => null])
      ->save();   

    // Zonas
    $tax_zones = $this->table( 'store_tax_zones');
    $tax_zones
      ->addColumn( 'tax_id', 'integer', ['null' => true, 'default' => null])
      ->addColumn( 'zone_id', 'integer', ['null' => true, 'default' => null])
      ->addColumn( 'created', 'datetime', ['default' => null])
      ->addIndex( ['tax_id'])
      ->addIndex( ['zone_id'])
      ->save();

    // Envios
    $carriers = $this->table( 'store_carriers');
    $carriers
      ->addColumn( 'title', 'string', ['null' => true, 'default' => null, 'limit' => 255])
      ->addColumn( 'active', 'boolean', ['null' => false, 'default' => 0])
      ->addColumn( 'by_default', 'boolean', ['null' => false, 'default' => 0])
      ->addColumn( 'tax_id', 'integer', ['null' => true, 'default' => null])
      ->addColumn( 'price', 'float', ['precision' => 20, 'scale' => 6, 'null' => true, 'default' => 0])
      ->addColumn( 'insurance', 'boolean', ['null' => false, 'default' => 0])
      ->addColumn( 'insurance_type', 'enum', ['values' => ['fixed', 'percent'], 'null' => false, 'default' => 'fixed'])
      ->addColumn( 'insurance_min', 'float', ['null' => true, 'default' => NULL])
      ->addColumn( 'express_delivery', 'boolean', ['null' => false, 'default' => 0])
      ->addColumn( 'express_delivery_type', 'enum', ['values' => ['fixed', 'percent'], 'null' => false, 'default' => 'fixed'])
      ->addColumn( 'express_delivery_value', 'float', ['null' => true, 'default' => NULL])
      ->addColumn( 'express_delivery_days', 'integer', ['null' => true, 'default' => NULL])
      ->addColumn( 'delivery_days', 'integer', ['null' => true, 'default' => NULL])
      ->addColumn( 'delivery_days_type', 'enum', ['values' => ['business_days', 'all_days'], 'null' => false, 'default' => 'business_days'])
      ->addColumn( 'salt', 'string', ['null' => true, 'default' => null, 'limit' => 255])
      ->addIndex( ['active'])
      ->addIndex( ['tax_id'])
      ->addIndex( ['by_default'])
      ->save();

    $carrier_rules = $this->table( 'store_carrier_rules');
    $carrier_rules
      ->addColumn( 'active', 'boolean', ['null' => false, 'default' => 0])
      ->addColumn( 'type', 'enum', ['values' => ['weight', 'price'], 'null' => false, 'default' => 'price'])
      ->addColumn( 'carrier_id', 'integer', ['null' => true, 'default' => null])
      ->addColumn( 'price', 'float', ['precision' => 20, 'scale' => 6, 'null' => false, 'default' => 0])
      ->addColumn( 'min', 'float', ['precision' => 20, 'scale' => 6, 'null' => false, 'default' => 0])
      ->addColumn( 'max', 'float', ['precision' => 20, 'scale' => 6, 'null' => false, 'default' => 0])
      ->addColumn( 'salt', 'string', ['null' => true, 'default' => null, 'limit' => 255])
      ->addColumn( 'delivery_type', 'enum', ['values' => ['normal', 'express'], 'null' => false, 'default' => 'normal'])
      ->addColumn( 'deadline', 'integer', ['null' => true, 'default' => null])
      ->addColumn( 'by_default', 'boolean', ['null' => false, 'default' => 0])
      ->addIndex( ['active'])
      ->addIndex( ['carrier_id'])
      ->addIndex( ['type'])
      ->save();

    $carrier_rules_zones = $this->table( 'store_carrier_rule_zones');
    $carrier_rules_zones
      ->addColumn( 'carrier_rule_id', 'integer', ['null' => true, 'default' => null])
      ->addColumn( 'zone_id', 'integer', ['null' => true, 'default' => null])
      ->addIndex( ['carrier_rule_id'])
      ->addIndex( ['zone_id'])
      ->save();

    // Métodos de pago
    $payment_methods = $this->table( 'store_payment_methods');
    $payment_methods
      ->addColumn( 'title', 'string', ['null' => true, 'default' => null, 'limit' => 255])
      ->addColumn( 'active', 'boolean', ['null' => false, 'default' => 0])
      ->addColumn( 'processor', 'string', ['null' => true, 'default' => NULL, 'limit' => 50])
      ->addColumn( 'tax_id', 'integer', ['null' => true, 'default' => null])
      ->addColumn( 'pay_for_used', 'boolean', ['null' => true, 'default' => null])
      ->addColumn( 'price', 'float', ['precision' => 20, 'scale' => 6, 'null' => true, 'default' => null])
      ->addColumn( 'price_max', 'float', ['precision' => 20, 'scale' => 6, 'null' => true, 'default' => null])
      ->addColumn( 'price_min', 'float', ['precision' => 20, 'scale' => 6, 'null' => true, 'default' => null])
      ->addColumn( 'salt', 'string', ['null' => true, 'default' => null, 'limit' => 255])
      ->addColumn( 'settings', 'text', ['null' => true, 'default' => null])
      ->addColumn( 'position', 'integer', ['null' => true, 'default' => null])
      ->addIndex( ['position'])
      ->addIndex( ['active'])
      ->addIndex( ['tax_id'])
      ->save();

    $addresses = $this->table( 'store_addresses');
    $addresses
      ->addColumn( 'user_id', 'integer', ['null' => true, 'default' => null])
      ->addColumn( 'country_id', 'integer', ['null' => true, 'default' => null])
      ->addColumn( 'state_id', 'integer', ['null' => true, 'default' => null])
      ->addColumn( 'alias', 'string', ['null' => true, 'default' => null, 'limit' => 32])
      ->addColumn( 'company', 'string', ['null' => true, 'default' => null, 'limit' => 64])
      ->addColumn( 'firstname', 'string', ['null' => true, 'default' => null, 'limit' => 32])
      ->addColumn( 'lastname', 'string', ['null' => true, 'default' => null, 'limit' => 32])
      ->addColumn( 'address1', 'string', ['null' => true, 'default' => null, 'limit' => 128])
      ->addColumn( 'address2', 'string', ['null' => true, 'default' => null, 'limit' => 128])
      ->addColumn( 'postcode', 'string', ['null' => true, 'default' => null, 'limit' => 12])
      ->addColumn( 'city', 'string', ['null' => true, 'default' => null, 'limit' => 64])
      ->addColumn( 'phone', 'string', ['null' => true, 'default' => null, 'limit' => 32])
      ->addColumn( 'phone_mobile', 'string', ['null' => true, 'default' => null, 'limit' => 32])
      ->addColumn( 'cif', 'string', ['null' => true, 'default' => null, 'limit' => 32])
      ->addColumn( 'salt', 'string', ['null' => true, 'default' => null, 'limit' => 255])
      ->addColumn( 'deleted', 'boolean', ['null' => false, 'default' => 0])
      ->addColumn( 'created', 'datetime', ['default' => null])
      ->addColumn( 'modified', 'datetime', ['default' => null])
      ->addIndex( ['user_id'])
      ->addIndex( ['country_id'])
      ->addIndex( ['state_id'])
      ->save();


    $orders = $this->table( 'store_orders');
    $orders
      ->addColumn( 'user_id', 'integer', ['null' => true, 'default' => null])
      ->addColumn( 'carrier_id', 'integer', ['null' => true, 'default' => null])
      ->addColumn( 'express_delivery', 'boolean', ['null' => false, 'default' => 0])
      ->addColumn( 'payment_method_id', 'integer', ['null' => true, 'default' => null])
      ->addColumn( 'address_delivery_id', 'integer', ['null' => true, 'default' => null])
      ->addColumn( 'address_invoice_id', 'integer', ['null' => true, 'default' => null])
      ->addColumn( 'currency_id', 'integer', ['null' => true, 'default' => null])
      ->addColumn( 'session', 'string', ['null' => true, 'default' => null, 'limit' => 128])
      ->addColumn( 'salt', 'string', ['null' => true, 'default' => null, 'limit' => 255])

      // Precios
      ->addColumn( 'discount_price', 'float', ['precision' => 20, 'scale' => 6, 'null' => false, 'default' => 0])
      ->addColumn( 'points_discount', 'float', ['precision' => 20, 'scale' => 6, 'null' => false, 'default' => 0])
      ->addColumn( 'points_discount_tax_rate', 'float', ['precision' => 20, 'scale' => 6, 'null' => false, 'default' => 0])
      ->addColumn( 'points_discount_tax', 'float', ['precision' => 20, 'scale' => 6, 'null' => false, 'default' => 0])
      ->addColumn( 'shipping_price', 'float', ['precision' => 20, 'scale' => 6, 'null' => false, 'default' => 0])
      ->addColumn( 'shipping_handling', 'float', ['precision' => 20, 'scale' => 6, 'null' => false, 'default' => 0])
      ->addColumn( 'shipping_handling_tax_rate', 'float', ['precision' => 20, 'scale' => 6, 'null' => false, 'default' => 0])
      ->addColumn( 'shipping_handling_tax', 'float', ['precision' => 20, 'scale' => 6, 'null' => false, 'default' => 0])
      ->addColumn( 'shipping_handling_free', 'float', ['precision' => 20, 'null' => false, 'default' => 0])
      ->addColumn( 'payment_price', 'boolean', ['precision' => 20, 'null' => false, 'default' => 0])
      ->addColumn( 'payment_price_tax_rate', 'float', ['precision' => 20, 'null' => false, 'default' => 0])
      ->addColumn( 'payment_price_tax', 'float', ['precision' => 20, 'null' => false, 'default' => 0])
      ->addColumn( 'grif_wrap', 'boolean', ['null' => true, 'default' => null])
      ->addColumn( 'gift_wrapping_price', 'float', ['precision' => 20, 'null' => false, 'default' => 0])
      ->addColumn( 'gift_wrapping_taxes', 'float', ['precision' => 20, 'null' => false, 'default' => 0])
      ->addColumn( 'subtotal', 'float', ['precision' => 20, 'null' => false, 'default' => 0])
      ->addColumn( 'taxes', 'float', ['precision' => 20, 'null' => false, 'default' => 0])
      ->addColumn( 'total', 'float', ['precision' => 20, 'null' => false, 'default' => 0])

      // Métodos de pago y envio
      ->addColumn( 'shipping_method', 'string', ['null' => true, 'default' => null, 'limit' => 64])
      ->addColumn( 'payment_method', 'string', ['null' => true, 'default' => null, 'limit' => 64])
      ->addColumn( 'payment_data', 'text', ['null' => true, 'default' => null])
      ->addColumn( 'delivery_date', 'date', ['default' => null])
      ->addColumn( 'delivery_date_input', 'date', ['default' => null])
      ->addColumn( 'delivery_date_custom', 'boolean', ['null' => true, 'default' => null])
      
      ->addColumn( 'status', 'string', ['null' => true, 'default' => null, 'limit' => 255])
      ->addColumn( 'comments', 'text', ['null' => true, 'default' => null])

      // Dirección de la tienda
      ->addColumn( 'adr_store_company', 'string', ['null' => true, 'default' => null, 'limit' => 64])
      ->addColumn( 'adr_store_cif', 'string', ['null' => true, 'default' => null, 'limit' => 64])
      ->addColumn( 'adr_store_address' , 'string', ['null' => true, 'default' => null, 'limit' => 64])
      ->addColumn( 'adr_store_postcode', 'string', ['null' => true, 'default' => null, 'limit' => 64])
      ->addColumn( 'adr_store_city', 'string', ['null' => true, 'default' => null, 'limit' => 64])
      ->addColumn( 'adr_store_state', 'string', ['null' => true, 'default' => null, 'limit' => 64])
      ->addColumn( 'adr_store_country', 'string', ['null' => true, 'default' => null, 'limit' => 64])
      ->addColumn( 'adr_store_phone', 'string', ['null' => true, 'default' => null, 'limit' => 64])

      // Dirección de facturación
      ->addColumn( 'adr_invoice_firstname', 'string', ['null' => true, 'default' => null, 'limit' => 64])
      ->addColumn( 'adr_invoice_lastname', 'string', ['null' => true, 'default' => null, 'limit' => 64])
      ->addColumn( 'adr_invoice_email', 'string', ['null' => true, 'default' => null, 'limit' => 64])
      ->addColumn( 'adr_invoice_phone', 'string', ['null' => true, 'default' => null, 'limit' => 64])
      ->addColumn( 'adr_invoice_company', 'string', ['null' => true, 'default' => null, 'limit' => 64])
      ->addColumn( 'adr_invoice_name', 'string', ['null' => true, 'default' => null, 'limit' => 64])
      ->addColumn( 'adr_invoice_type', 'string', ['null' => true, 'default' => null, 'limit' => 64])
      ->addColumn( 'adr_invoice_vat_number', 'string', ['null' => true, 'default' => null, 'limit' => 64])
      ->addColumn( 'adr_invoice_address', 'string', ['null' => true, 'default' => null, 'limit' => 64])
      ->addColumn( 'adr_invoice_postcode', 'string', ['null' => true, 'default' => null, 'limit' => 64])
      ->addColumn( 'adr_invoice_city', 'string', ['null' => true, 'default' => null, 'limit' => 64])
      ->addColumn( 'adr_invoice_state', 'string', ['null' => true, 'default' => null, 'limit' => 64])
      ->addColumn( 'adr_invoice_country', 'string', ['null' => true, 'default' => null, 'limit' => 64])
      ->addColumn( 'adr_invoice_country_id', 'integer', ['null' => true, 'default' => null])
      ->addColumn( 'adr_invoice_state_id', 'integer', ['null' => true, 'default' => null])

      // Dirección de entrega
      ->addColumn( 'adr_delivery_firstname', 'string', ['null' => true, 'default' => null, 'limit' => 64])
      ->addColumn( 'adr_delivery_lastname', 'string', ['null' => true, 'default' => null, 'limit' => 64])
      ->addColumn( 'adr_delivery_company', 'string', ['null' => true, 'default' => null, 'limit' => 64])
      ->addColumn( 'adr_delivery_phone', 'string', ['null' => true, 'default' => null, 'limit' => 64])
      ->addColumn( 'adr_delivery_name', 'string', ['null' => true, 'default' => null, 'limit' => 64])
      ->addColumn( 'adr_delivery_type', 'string', ['null' => true, 'default' => null, 'limit' => 64])
      ->addColumn( 'adr_delivery_vat_number', 'string', ['null' => true, 'default' => null, 'limit' => 64])
      ->addColumn( 'adr_delivery_address', 'string', ['null' => true, 'default' => null, 'limit' => 64])
      ->addColumn( 'adr_delivery_postcode', 'string', ['null' => true, 'default' => null, 'limit' => 64])
      ->addColumn( 'adr_delivery_city', 'string', ['null' => true, 'default' => null, 'limit' => 64])
      ->addColumn( 'adr_delivery_state', 'string', ['null' => true, 'default' => null, 'limit' => 64])
      ->addColumn( 'adr_delivery_country', 'string', ['null' => true, 'default' => null, 'limit' => 64])
      ->addColumn( 'adr_delivery_country_id', 'integer', ['null' => true, 'default' => null])
      ->addColumn( 'adr_delivery_state_id', 'integer', ['null' => true, 'default' => null])
      
      ->addColumn( 'order_date', 'datetime', ['default' => null])
      ->addColumn( 'has_invoice', 'boolean', ['null' => false, 'default' => 0])
      
      ->addColumn( 'user_message', 'text', ['null' => true, 'default' => null])
      ->addColumn( 'delivery_days', 'integer', ['null' => true, 'default' => null])
      ->addColumn( 'comments_gift', 'text', ['null' => true, 'default' => null])
      ->addColumn( 'same_address', 'boolean', ['null' => false, 'default' => 0])

      ->addColumn( 'created', 'datetime', ['default' => null])
      ->addColumn( 'modified', 'datetime', ['default' => null])

      // Registro de usuarios
      ->addColumn( 'want_account', 'boolean', ['null' => false, 'default' => 0])
      ->addColumn( 'password', 'string', ['null' => true, 'default' => null])
      ->addColumn( 'password2', 'string', ['null' => true, 'default' => null])
      
      ->addIndex( ['user_id'])
      ->addIndex( ['carrier_id'])
      ->addIndex( ['payment_method_id'])
      ->addIndex( ['address_delivery_id'])
      ->addIndex( ['address_invoice_id'])
      ->addIndex( ['currency_id'])

      ->save();



      // Recibos
      $receipts = $this->table( 'store_receipts');
      $receipts
        ->addColumn( 'order_id', 'integer', ['null' => true, 'default' => null])
        ->addColumn( 'num', 'string', ['null' => true, 'default' => null])
        ->addColumn( 'prefix', 'string', ['null' => true, 'default' => null, 'limit' => 8])
        ->addColumn( 'salt', 'string', ['null' => true, 'default' => null, 'limit' => 255])
        ->addColumn( 'created', 'datetime', ['default' => null])
        ->addColumn( 'modified', 'datetime', ['default' => null])
        ->addIndex( ['order_id'])
        ->addIndex( ['num'])
        ->save();



      // Facturas
      $invoices = $this->table( 'store_invoices');
      $invoices
        ->addColumn( 'order_id', 'integer', ['null' => true, 'default' => null])
        ->addColumn( 'num', 'string', ['null' => true, 'default' => null])
        ->addColumn( 'prefix', 'string', ['null' => true, 'default' => null, 'limit' => 8])
        ->addColumn( 'salt', 'string', ['null' => true, 'default' => null, 'limit' => 255])
        ->addColumn( 'created', 'datetime', ['default' => null])
        ->addColumn( 'modified', 'datetime', ['default' => null])
        ->addIndex( ['order_id'])
        ->addIndex( ['num'])
        ->save();



      // Pagos
      $payments = $this->table( 'store_payments');
      $payments
        ->addColumn( 'order_id', 'integer', ['null' => true, 'default' => null])
        ->addColumn( 'payment_method_id', 'integer', ['null' => true, 'default' => null])
        ->addColumn( 'success', 'boolean', ['null' => false, 'default' => 0])
        ->addColumn( 'finalize', 'boolean', ['null' => false, 'default' => 0])
        ->addColumn( 'data', 'text', ['null' => true, 'default' => null])
        ->addColumn( 'message_error', 'string', ['null' => true, 'default' => null, 'limit' => 255])
        ->addColumn( 'salt', 'string', ['null' => true, 'default' => null, 'limit' => 255])
        ->addColumn( 'created', 'datetime', ['default' => null])
        ->addColumn( 'modified', 'datetime', ['default' => null])
        ->addIndex( ['order_id'])
        ->addIndex( ['payment_method_id'])
        ->addIndex( ['salt'])
        ->save();



      $line_items = $this->table( 'store_line_items');
      $line_items
        ->addColumn( 'order_id', 'integer', ['null' => true, 'default' => null])
        ->addColumn( 'product_id', 'integer', ['null' => true, 'default' => null])
        ->addColumn( 'product_attribute_id', 'integer', ['null' => true, 'default' => null])
        ->addColumn( 'quantity', 'integer', ['null' => true, 'default' => null])
        ->addColumn( 'title', 'string', ['null' => true, 'default' => NULL, 'limit' => 255])
        ->addColumn( 'weight', 'float', ['null' => true, 'default' => NULL])
        ->addColumn( 'reference', 'string', ['null' => true, 'default' => NULL, 'limit' => 32])
        ->addColumn( 'ean13', 'string', ['null' => true, 'default' => NULL, 'limit' => 13])
        ->addColumn( 'upc', 'string', ['null' => true, 'default' => NULL, 'limit' => 12])
        ->addColumn( 'price', 'float', ['precision' => 20, 'scale' => 6, 'null' => false, 'default' => 0])
        ->addColumn( 'tax_id', 'integer', ['null' => true, 'default' => null])
        ->addColumn( 'tax_rate', 'float', ['precision' => 20, 'scale' => 6, 'null' => false, 'default' => 0])
        ->addColumn( 'discount', 'float', ['precision' => 20, 'scale' => 6, 'null' => false, 'default' => 0])
        ->addColumn( 'photo', 'text', ['default' => NULL, 'null' => true])
        ->addColumn( 'salt', 'string', ['null' => true, 'default' => null, 'limit' => 255])


        ->addIndex( ['order_id'])
        ->addIndex( ['product_id'])
        ->addIndex( ['product_attribute_id'])
        ->addIndex( ['tax_id'])
        ->save();


      $order_histories = $this->table( 'store_order_histories');
      $order_histories
        ->addColumn( 'user_id', 'integer', ['null' => true, 'default' => null])
        ->addColumn( 'order_id', 'integer', ['null' => true, 'default' => null])
        ->addColumn( 'status', 'string', ['null' => true, 'default' => null, 'limit' => 16])
        ->addColumn( 'salt', 'string', ['null' => true, 'default' => null, 'limit' => 255])
        ->addColumn( 'created', 'datetime', ['default' => null])
        ->addIndex( ['user_id'])
        ->addIndex( ['order_id'])
        ->save();




      $order_messages = $this->table( 'store_order_messages');
      $order_messages
        ->addColumn( 'user_id', 'integer', ['null' => true, 'default' => null])
        ->addColumn( 'order_id', 'integer', ['null' => true, 'default' => null])
        ->addColumn( 'body', 'text', ['null' => true, 'default' => null])
        ->addColumn( 'salt', 'string', ['null' => true, 'default' => null, 'limit' => 255])
        ->addColumn( 'created', 'datetime', ['default' => null])
        ->addIndex( ['user_id'])
        ->addIndex( ['order_id'])
        ->save();



      $i18n = $this->table( 'store_i18n');
      $i18n
          ->addColumn( 'locale', 'string', ['limit' => 5])
          ->addColumn( 'model', 'string', ['limit' => 64, 'null' => false])
          ->addColumn( 'foreign_key', 'string', ['limit' => 36, 'null' => false])
          ->addColumn( 'field', 'string', ['limit' => 64, 'null' => false])
          ->addColumn( 'content', 'text', ['default' => null, 'null' => true])
          ->addColumn( 'created', 'datetime', array('default' => null))
          ->addColumn( 'modified', 'datetime', array('default' => null))
          ->addIndex( ['locale', 'model', 'foreign_key', 'field'], ['unique' => true])
          ->addIndex( ['locale', 'model', 'foreign_key'], ['unique' => false])
          ->addIndex( ['locale', 'model'], ['unique' => false])
          ->addIndex( ['model', 'foreign_key', 'field'], ['unique' => false])
          ->addIndex( ['model', 'foreign_key'], ['unique' => false])
          ->save();



      $collections = $this->table( 'store_collections');
      $collections
        ->addColumn( 'title', 'string', ['null' => true, 'default' => null, 'limit' => 255])
        ->addColumn( 'photo', 'text', ['default' => null, 'null' => true])
        ->addColumn( 'start_on', 'date', ['null' => true, 'default' => null])
        ->addColumn( 'finish_on', 'date', ['null' => true, 'default' => null])
        ->addColumn( 'date_restrict', 'boolean', ['null' => false, 'default' => 0])
        ->addColumn( 'body', 'text', ['null' => true, 'default' => null])
        ->addColumn( 'salt', 'string', ['null' => true, 'default' => null, 'limit' => 255])
        ->addColumn( 'created', 'datetime', ['default' => null])
        ->addColumn( 'modified', 'datetime', ['default' => null])
        ->save();

      $collections_translations = $this->table( 'store_collections_translations', ['id' => false, 'primary_key' => ['id', 'locale']]);
      $collections_translations
        ->addColumn( 'id', 'integer', ['null' => false])
        ->addColumn( 'locale', 'string', ['null' => true, 'default' => null, 'limit' => 5])
        ->addColumn( 'title', 'string', ['null' => true, 'default' => null])
        ->addColumn( 'body', 'text', ['null' => true, 'default' => null])
        ->save();  

      $collections_products = $this->table( 'store_collections_products');
      $collections_products
        ->addColumn( 'collection_id', 'integer', ['null' => true, 'default' => null])
        ->addColumn( 'product_id', 'integer', ['null' => true, 'default' => null])
        ->addColumn( 'created', 'datetime', ['default' => null])
        ->addIndex( ['collection_id'])
        ->addIndex( ['product_id'])
        ->save();



      // Guía de tallas
      $sizes = $this->table( 'store_sizes');
      $sizes
        ->addColumn( 'title', 'string', ['null' => true, 'default' => null, 'limit' => 255])
        ->save();



      // Devoluciones
      $returneds = $this->table( 'store_returneds');
      $returneds
        ->addColumn( 'user_id', 'integer', ['null' => false])
        ->addColumn( 'ordernumber', 'string', ['null' => true, 'default' => null])
        ->addColumn( 'reason', 'string', ['null' => true, 'default' => null])
        ->addColumn( 'comments', 'text', ['null' => true, 'default' => null])
        ->addColumn( 'created', 'datetime', ['default' => null])
        ->addColumn( 'modified', 'datetime', ['default' => null])
        ->addIndex( ['user_id'])
        ->save();



      // ERP
      $erp = $this->table( 'store_erp');
      $erp
        ->addColumn( 'erp_id', 'string', ['limit' => 64, 'null' => false])
        ->addColumn( 'store_id', 'integer', ['null' => false])
        ->addColumn( 'model', 'string', ['limit' => 64, 'null' => false])
        ->addIndex( ['erp_id'])
        ->addIndex( ['store_id'])
        ->addIndex( ['model'])
        ->save();



      $product_views = $this->table( 'store_product_views');
      $product_views
        ->addColumn( 'user_id', 'integer', ['null' => true, 'default' => null])
        ->addColumn( 'session_id', 'string', ['limit' => 64, 'null' => true, 'default' => null])
        ->addColumn( 'product_id', 'integer', ['null' => true, 'default' => null])
        ->addColumn( 'created', 'datetime', ['default' => null])
        ->addIndex( ['user_id'])
        ->addIndex( ['session_id'])
        ->addIndex( ['product_id'])
        ->save();



      $store_favs = $this->table( 'store_favs');
      $store_favs
        ->addColumn( 'user_id', 'integer', ['null' => true, 'default' => null])
        ->addColumn( 'product_id', 'integer', ['null' => true, 'default' => null])
        ->addColumn( 'salt', 'string', ['null' => true, 'default' => null, 'limit' => 64])
        ->addColumn( 'created', 'datetime', ['default' => null])
        ->addIndex( ['user_id'])
        ->addIndex( ['product_id'])
        ->save();



      $promos = $this->table( 'store_promos');
      $promos
        ->addColumn( 'title', 'string', ['null' => true, 'default' => null, 'limit' => 255])
        ->addColumn( 'start_on', 'date', ['default' => null])
        ->addColumn( 'finish_on', 'date', ['default' => null])
        ->addColumn( 'active', 'boolean', ['null' => false, 'default' => 0])
        ->addColumn( 'value', 'float', ['null' => true, 'default' => NULL])
        ->addColumn( 'percent', 'float', ['null' => true, 'default' => NULL])
        ->addColumn( 'promo_type', 'enum', ['values' => ['global', 'categories', 'products'], 'null' => false, 'default' => 'global'])
        ->addColumn( 'discount_type', 'enum', ['values' => ['fixed', 'percent'], 'null' => false, 'default' => 'fixed'])
        ->addColumn( 'salt', 'string', ['null' => true, 'default' => null, 'limit' => 255])
        ->addColumn( 'created', 'datetime', ['default' => null])
        ->addColumn( 'modified', 'datetime', ['default' => null])
        ->addIndex( ['active'])
        ->addIndex( ['start_on'])
        ->addIndex( ['finish_on'])
        ->save();

      $promos_translations = $this->table( 'store_promos_translations', ['id' => false, 'primary_key' => ['id', 'locale']]);
      $promos_translations
        ->addColumn( 'id', 'integer', ['null' => false])
        ->addColumn( 'locale', 'string', ['null' => true, 'default' => null, 'limit' => 5])
        ->addColumn( 'title', 'string', ['null' => true, 'default' => null])
        ->save(); 

      $promos_categories = $this->table( 'store_promos_categories');

      $promos_categories
        ->addColumn( 'promo_id', 'integer', ['null' => true, 'default' => null])
        ->addColumn( 'category_id', 'integer', ['null' => true, 'default' => null])
        ->addIndex( ['promo_id'])
        ->addIndex( ['category_id'])
        ->save();



      $promos_states = $this->table( 'store_promos_states');

      $promos_states
        ->addColumn( 'promo_id', 'integer', ['null' => true, 'default' => null])
        ->addColumn( 'state_id', 'integer', ['null' => true, 'default' => null])
        ->addIndex( ['promo_id'])
        ->addIndex( ['state_id'])
        ->save();



      $promos_countries = $this->table( 'store_promos_countries');

      $promos_countries
        ->addColumn( 'promo_id', 'integer', ['null' => true, 'default' => null])
        ->addColumn( 'country_id', 'integer', ['null' => true, 'default' => null])
        ->addIndex( ['promo_id'])
        ->addIndex( ['country_id'])
        ->save();



      $promos_products = $this->table( 'store_promos_products');
      $promos_products
        ->addColumn( 'promo_id', 'integer', ['null' => true, 'default' => null])
        ->addColumn( 'product_id', 'integer', ['null' => true, 'default' => null])
        ->addIndex( ['promo_id'])
        ->addIndex( ['product_id'])
        ->save();


      $promos_groups = $this->table( 'store_promos_groups');
      $promos_groups
        ->addColumn( 'promo_id', 'integer', ['null' => true, 'default' => null])
        ->addColumn( 'group_id', 'integer', ['null' => true, 'default' => null])
        ->addIndex( ['promo_id'])
        ->addIndex( ['group_id'])
        ->save();




      $discounts = $this->table( 'store_discounts');
      $discounts
        ->addColumn( 'line_item_id', 'integer', ['null' => true, 'default' => null])
        ->addColumn( 'promo_id', 'integer', ['null' => true, 'default' => null])
        ->addColumn( 'amount', 'float', ['null' => true, 'default' => NULL])
        ->addColumn( 'percent', 'float', ['null' => true, 'default' => NULL])
        ->addColumn( 'salt', 'string', ['null' => true, 'default' => null, 'limit' => 255])
        ->addColumn( 'created', 'datetime', ['default' => null])
        ->addColumn( 'modified', 'datetime', ['default' => null])
        ->addIndex( ['line_item_id'])
        ->addIndex( ['promo_id'])
        ->save();



      $points = $this->table( 'store_points');
      $points
        ->addColumn( 'user_id', 'integer', ['null' => true, 'default' => null])
        ->addColumn( 'administrator_id', 'integer', ['null' => true, 'default' => null])
        ->addColumn( 'order_id', 'integer', ['null' => true, 'default' => null])
        ->addColumn( 'action', 'enum', ['values' => ['up', 'down'], 'null' => false, 'default' => 'up'])
        ->addColumn( 'points', 'integer', ['null' => true, 'default' => null])
        ->addColumn( 'discount', 'float', ['null' => true, 'default' => null])
        ->addColumn( 'salt', 'string', ['null' => true, 'default' => null, 'limit' => 64])
        ->addColumn( 'created', 'datetime', ['default' => null])
        ->addColumn( 'modified', 'datetime', ['default' => null])
        ->addIndex( ['user_id'])
        ->addIndex( ['order_id'])
        ->addIndex( ['administrator_id'])
        ->save();

      $points_adds = $this->table( 'store_points_adds');
      $points_adds
        ->addColumn( 'title', 'string', ['null' => false, 'limit' => 255])
        ->addColumn( 'data', 'text', ['default' => null, 'null' => true])
        ->addColumn( 'subject', 'string', ['default' => null, 'null' => true])
        ->addColumn( 'body', 'text', ['default' => null, 'null' => true])
        ->addColumn( 'user_id', 'integer', ['null' => true, 'default' => null])
        ->addColumn( 'send_on', 'datetime', ['default' => null])
        ->addColumn( 'created', 'datetime', ['default' => null])
        ->addColumn( 'modified', 'datetime', ['default' => null])
        ->addColumn( 'active', 'boolean', [ 'default' => 0, 'null' => true])
        ->addColumn( 'finish_on', 'datetime', ['default' => null])
        ->addIndex( ['finish_on'])
        ->addIndex( ['active'])
        ->save();

      
      $points_adds_translations = $this->table( 'store_points_adds_translations', ['id' => false, 'primary_key' => ['id', 'locale']]);
      $points_adds_translations
        ->addColumn( 'id', 'integer', ['null' => false])
        ->addColumn( 'locale', 'string', ['null' => true, 'default' => null, 'limit' => 5])
        ->addColumn( 'title', 'string', ['null' => true, 'default' => null])
        ->addColumn( 'subject', 'string', ['null' => true, 'default' => null])
        ->addColumn( 'body', 'text', ['null' => true, 'default' => null])
        ->save(); 

      $points_adds_sends = $this->table( 'store_points_adds_sends');
      $points_adds_sends
        ->addColumn( 'send_id', 'integer', ['null' => true, 'default' => null])
        ->addColumn( 'user_id', 'integer', ['default' => null, 'null' => true])
        ->addColumn( 'send_on', 'datetime', ['default' => null, 'null' => true])
        ->addColumn( 'email', 'string', ['default' => null, 'null' => true])
        ->addColumn( 'created', 'datetime', ['default' => null])
        ->save();
  }

  public function _down()
  {
    $this->dropTable( 'store_attribute_groups');
    $this->dropTable( 'store_attribute_groups_translations');
    $this->dropTable( 'store_attributes');
    $this->dropTable( 'store_attributes_translations');
    $this->dropTable( 'store_product_attributes');
    $this->dropTable( 'store_product_attribute_combinations');
    $this->dropTable( 'store_relateds');
    $this->dropTable( 'store_brands');
    $this->dropTable( 'store_brands_translations');
    $this->dropTable( 'store_currencies');
    $this->dropTable( 'store_zones');
    $this->dropTable( 'store_countries');
    $this->dropTable( 'store_countries_translations');
    $this->dropTable( 'store_states');
    $this->dropTable( 'store_taxes');
    $this->dropTable( 'store_taxes_translations');
    $this->dropTable( 'store_tax_zones');
    $this->dropTable( 'store_carriers');
    $this->dropTable( 'store_carrier_rules');
    $this->dropTable( 'store_carrier_rule_zones');
    $this->dropTable( 'store_payment_methods');
    $this->dropTable( 'store_addresses');
    $this->dropTable( 'store_orders');
    $this->dropTable( 'store_receipts');
    $this->dropTable( 'store_invoices');
    $this->dropTable( 'store_payments');
    $this->dropTable( 'store_line_items');
    $this->dropTable( 'store_order_histories');
    $this->dropTable( 'store_order_messages');
    $this->dropTable( 'store_i18n');
    $this->dropTable( 'store_collections');
    $this->dropTable( 'store_collections_translations');
    $this->dropTable( 'store_collections_products');
    $this->dropTable( 'store_sizes');
    $this->dropTable( 'store_returneds');
    $this->dropTable( 'store_erp');
    $this->dropTable( 'store_product_views');
    $this->dropTable( 'store_favs');
    $this->dropTable( 'store_promos');
    $this->dropTable( 'store_promos_translations');
    $this->dropTable( 'store_promos_categories');
    $this->dropTable( 'store_promos_states');
    $this->dropTable( 'store_promos_countries');
    $this->dropTable( 'store_promos_products');
    $this->dropTable( 'store_promos_groups');
    $this->dropTable( 'store_discounts');
    $this->dropTable( 'store_points');
    $this->dropTable( 'store_points_adds');
    $this->dropTable( 'store_points_adds_translations');
    $this->dropTable( 'store_points_adds_sends');
  }
}
