<?php
use Migrations\AbstractMigration;

class PaymentMethodsTranslate2 extends AbstractMigration
{

  public function change()
  {
    $payment_methods = $this->table( 'store_payment_methods');
    $payment_methods
      ->addColumn( 'subtitle', 'string', ['null' => true, 'default' => null])
      ->update();
    
    $payment_methods_translations = $this->table( 'store_payment_methods_translations');
    $payment_methods_translations
      ->addColumn( 'title', 'string', ['null' => true, 'default' => null])
      ->addColumn( 'subtitle', 'string', ['null' => true, 'default' => null])
      ->update(); 
  }
}
