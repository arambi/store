<?php
use Migrations\AbstractMigration;

class RemoveOrdersColumms extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     * @return void
     */
    public function change()
    {
        $this->table('store_orders')
            ->removeColumn('address_delivery_id')
            ->removeColumn('address_invoice_id')
            ->update();
    }
}
