<?php
use Migrations\AbstractMigration;

class GiftCard extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     * @return void
     */
    public function change()
    {
        $line_items = $this->table( 'store_line_items');
        
        if ($line_items->hasColumn('giftcard_code')) {
            return;
        }

        $line_items
            ->addColumn( 'giftcard_code', 'string', ['null' => true, 'default' => null])
            ->addColumn( 'giftcard_method', 'string', ['null' => true, 'default' => null])
            ->addColumn( 'giftcard_address', 'string', ['null' => true, 'default' => null])
            ->addColumn( 'giftcard_city', 'string', ['null' => true, 'default' => null])
            ->addColumn( 'giftcard_name', 'string', ['null' => true, 'default' => null])
            ->addColumn( 'giftcard_email', 'string', ['null' => true, 'default' => null])
            ->addColumn( 'giftcard_phone', 'string', ['null' => true, 'default' => null])
            ->addColumn( 'giftcard_postcode', 'string', ['null' => true, 'default' => null])
            ->addColumn( 'giftcard_country_id', 'integer', ['null' => true, 'default' => null])
            ->addColumn( 'giftcard_state_id', 'integer', ['null' => true, 'default' => null])
            ->addColumn( 'giftcard_price', 'float', ['precision' => 20, 'scale' => 6, 'null' => true, 'default' => null])
            ->addColumn( 'is_giftcard', 'boolean', ['null' => false, 'default' => 0])
            ->update();
    }
}
