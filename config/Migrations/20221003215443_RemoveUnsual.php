<?php
use Migrations\AbstractMigration;

class RemoveUnsual extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     * @return void
     */
    public function change()
    {
        $this->table('store_orders')
            ->removeColumn('adr_invoice_state')
            ->removeColumn('adr_invoice_country')
            ->removeColumn('adr_delivery_state')
            ->removeColumn('adr_delivery_country')
            ->update();
    }
}
