<?php
use Migrations\AbstractMigration;

class CorreosTrack3 extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     * @return void
     */
    public function change()
    {
        $this->table('store_line_items')
            ->addColumn('correos_track_number', 'string', ['null' => true, 'default' => null, 'limit' => 64])
            ->addColumn('correos_track_data', 'text', ['null' => true, 'default' => null])
            ->addIndex('correos_track_number')
            ->update();
    }
}
