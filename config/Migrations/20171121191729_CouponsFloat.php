<?php
use Migrations\AbstractMigration;

class CouponsFloat extends AbstractMigration
{
  /**
   * Change Method.
   *
   * More information on this method is available here:
   * http://docs.phinx.org/en/latest/migrations.html#the-change-method
   * @return void
   */
  public function change()
  {
    $coupons = $this->table( 'store_coupons');
    $coupons
      ->changeColumn( 'value', 'float', ['null' => true, 'default' => null])
      ->update();

  }
}
