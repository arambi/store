<?php

use Migrations\AbstractMigration;

class InvoiceRefunds extends AbstractMigration
{

    public function change()
    {
        $invoices = $this->table('store_invoice_refunds');
        $invoices
            ->addColumn('order_id', 'integer', ['null' => true, 'default' => null])
            ->addColumn('num', 'integer', ['null' => true, 'default' => null])
            ->addColumn('prefix', 'string', ['null' => true, 'default' => null, 'limit' => 8])
            ->addColumn('salt', 'string', ['null' => true, 'default' => null, 'limit' => 255])
            ->addColumn('invoice_date', 'date', ['null' => true, 'default' => null])
            ->addColumn('shipping_handling', 'float', ['precision' => 20, 'scale' => 3, 'null' => true, 'default' => 0])
            ->addColumn('shipping_handling_tax_rate', 'float', ['precision' => 20, 'scale' => 3, 'null' => true, 'default' => 0])
            ->addColumn('shipping_handling_tax', 'float', ['precision' => 20, 'scale' => 3, 'null' => true, 'default' => 0])
            ->addColumn('created', 'datetime', ['default' => null])
            ->addColumn('modified', 'datetime', ['default' => null])
            ->addIndex(['order_id'])
            ->addIndex(['num'])
            ->create();

        $invoices = $this->table('store_invoice_refund_line_items');
        $invoices
            ->addColumn('invoice_refund_id', 'integer', ['null' => true, 'default' => null])
            ->addColumn('line_item_id', 'integer', ['null' => true, 'default' => null])
            ->addColumn('quantity', 'integer', ['null' => true, 'default' => null])
            ->addColumn('price', 'float', ['precision' => 20, 'scale' => 3, 'null' => true, 'default' => 0])
            ->addColumn('tax_rate', 'float', ['precision' => 20, 'scale' => 3, 'null' => true, 'default' => 0])
            ->addColumn('created', 'datetime', ['default' => null])
            ->addColumn('modified', 'datetime', ['default' => null])
            ->addIndex(['invoice_refund_id'])
            ->addIndex(['line_item_id'])
            ->create();
    }
}
