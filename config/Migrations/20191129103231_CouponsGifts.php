<?php
use Migrations\AbstractMigration;

class CouponsGifts extends AbstractMigration
{
  /**
   * Change Method.
   *
   * More information on this method is available here:
   * http://docs.phinx.org/en/latest/migrations.html#the-change-method
   * @return void
   */
  public function change()
  {
    $coupons = $this->table( 'store_coupons');
    $coupons
      ->addColumn( 'has_gifts', 'boolean', [ 'null' => false, 'default' => 0])
      ->update();

    $contents_gifts = $this->table( 'store_coupons_gifts');
    $contents_gifts
      ->addColumn( 'coupon_id', 'integer', [ 'null' => true, 'default' => null])
      ->addColumn( 'product_id', 'integer', [ 'null' => true, 'default' => null])
      ->addIndex( ['coupon_id', 'product_id'])
      ->addIndex( 'coupon_id')
      ->addIndex( 'product_id')
      ->create();
  }
}
