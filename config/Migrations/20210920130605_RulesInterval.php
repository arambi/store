<?php

use Migrations\AbstractMigration;

class RulesInterval extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     * @return void
     */
    public function change()
    {
        $this->table('store_carrier_rules')
            ->addColumn('is_interval', 'boolean', ['null' => false, 'default' => 0])
            ->addColumn('interval', 'integer', ['null' => true, 'default' => null])
            ->addColumn('from', 'integer', ['null' => true, 'default' => null])
            ->changeColumn('min', 'float', ['precision' => 20, 'scale' => 6, 'null' => true, 'default' => true])
            ->changeColumn('max', 'float', ['precision' => 20, 'scale' => 6, 'null' => true, 'default' => true])
            ->addIndex(['is_interval'])
            ->update();
    }
}
