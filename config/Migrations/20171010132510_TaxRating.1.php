<?php
use Migrations\AbstractMigration;

class TaxRating extends AbstractMigration
{
  /**
   * Change Method.
   *
   * More information on this method is available here:
   * http://docs.phinx.org/en/latest/migrations.html#the-change-method
   * @return void
   */
  public function change()
  {
    $tax_ratings = $this->table( 'store_tax_ratings');
    $tax_ratings
      ->addColumn( 'value', 'float', ['precision' => 20, 'scale' => 6, 'null' => false, 'default' => 0])
      ->addColumn( 'tax_id', 'integer', ['limit' => 3, 'null' => false, 'default' => null])
      ->addColumn( 'country_id', 'integer', ['limit' => 3,'null' => false, 'default' => null])
      ->addColumn( 'state_id', 'integer', ['limit' => 3,'null' => false, 'default' => null])
      ->addColumn( 'created', 'datetime', ['default' => null])
      ->addColumn( 'modified', 'datetime', ['default' => null])
      ->addIndex( ['tax_id', 'country_id'])
      ->addIndex( ['tax_id', 'state_id'])
      ->addIndex( ['tax_id'])
      ->addIndex( ['country_id'])
      ->addIndex( ['state_id'])
      ->create();
  }
}
