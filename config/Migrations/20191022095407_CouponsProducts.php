<?php
use Migrations\AbstractMigration;

class CouponsProducts extends AbstractMigration
{
  /**
   * Change Method.
   *
   * More information on this method is available here:
   * http://docs.phinx.org/en/latest/migrations.html#the-change-method
   * @return void
   */
  public function change()
  {
    $contents_products = $this->table( 'store_coupons_products');
    $contents_products
      ->addColumn( 'coupon_id', 'integer', [ 'null' => true, 'default' => null])
      ->addColumn( 'product_id', 'integer', [ 'null' => true, 'default' => null])
      ->addIndex( ['coupon_id', 'product_id'])
      ->addIndex( 'coupon_id')
      ->addIndex( 'product_id')
      ->create();
  }
}
