<?php

use Migrations\AbstractMigration;

class RulesInterval2 extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     * @return void
     */
    public function change()
    {
        $this->table('store_carrier_rules')
            ->addColumn('price_from', 'float', ['precision' => 20, 'scale' => 6, 'null' => true, 'default' => null])
            ->addColumn('price_interval', 'float', ['precision' => 20, 'scale' => 6, 'null' => true, 'default' => null])
            ->changeColumn('price', 'float', ['precision' => 20, 'scale' => 6, 'null' => true, 'default' => null])
            ->update();
    }
}
