<?php
use Migrations\AbstractMigration;

class CarrierRulesReduction extends AbstractMigration
{
  /**
   * Change Method.
   *
   * More information on this method is available here:
   * http://docs.phinx.org/en/latest/migrations.html#the-change-method
   * @return void
   */
  public function change()
  {
    $carrier_rules = $this->table( 'store_carrier_rules');
    $carrier_rules
      ->addColumn( 'is_reduction', 'boolean', ['null' => false, 'default' => 0])
      ->addIndex( ['is_reduction'])
      ->update();
  }
}
