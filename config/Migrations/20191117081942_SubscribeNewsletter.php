<?php
use Migrations\AbstractMigration;

class SubscribeNewsletter extends AbstractMigration
{
  /**
   * Change Method.
   *
   * More information on this method is available here:
   * http://docs.phinx.org/en/latest/migrations.html#the-change-method
   * @return void
   */
  public function change()
  {
    $orders = $this->table( 'store_orders');

    if( !$orders->hasColumn( 'subscribe_newsletter'))
    {
      $orders
        ->addColumn( 'subscribe_newsletter', 'boolean', ['null' => false, 'default' => 0])
        ->update();
    }
  }
}
