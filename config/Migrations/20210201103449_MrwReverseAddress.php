<?php
use Migrations\AbstractMigration;

class MrwReverseAddress extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     * @return void
     */
    public function change()
    {
        $this->table('store_mrw_deliveries')
            ->addColumn('reverse_address', 'boolean', ['null' => false, 'default' => null])
            ->update();
    }
}
