<?php
use Migrations\AbstractMigration;

class CouponsPriceMin extends AbstractMigration
{
  /**
   * Change Method.
   *
   * More information on this method is available here:
   * http://docs.phinx.org/en/latest/migrations.html#the-change-method
   * @return void
   */
  public function change()
  {
    $coupons = $this->table( 'store_coupons');
    $coupons
      ->addColumn( 'with_price_min', 'boolean', [ 'null' => false, 'default' => 0])
      ->addColumn( 'price_min', 'float', ['precision' => 20, 'scale' => 6, 'null' => true, 'default' => null])
      ->update();
  }
}
