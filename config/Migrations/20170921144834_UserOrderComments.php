<?php
use Migrations\AbstractMigration;

class UserOrderComments extends AbstractMigration
{
  /**
   * Change Method.
   *
   * More information on this method is available here:
   * http://docs.phinx.org/en/latest/migrations.html#the-change-method
   * @return void
   */
  public function change()
  {
    $users = $this->table( 'users');
    $users
      ->addColumn( 'store_comments', 'text', ['null' => true, 'default' => null])
      ->save();
    
    $orders = $this->table( 'store_orders');
    $orders
      ->addColumn( 'store_comments', 'text', ['null' => true, 'default' => null])
      ->save();
  }
}
