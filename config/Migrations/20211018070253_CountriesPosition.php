<?php
use Migrations\AbstractMigration;

class CountriesPosition extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     * @return void
     */
    public function change()
    {
        $this->table('store_countries')
            ->addColumn('with_position', 'boolean', ['null' => false, 'default' => 0])
            ->addColumn('position', 'integer', ['null' => true, 'default' => null, 'limit' => 3])
            ->addIndex('with_position')
            ->addIndex('position')
            ->update();
    }
}
