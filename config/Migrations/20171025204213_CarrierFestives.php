<?php
use Migrations\AbstractMigration;

class CarrierFestives extends AbstractMigration
{
  /**
   * Change Method.
   *
   * More information on this method is available here:
   * http://docs.phinx.org/en/latest/migrations.html#the-change-method
   * @return void
   */
  public function change()
  {
    $carrier_festives = $this->table( 'store_carrier_festives');
    $carrier_festives
      ->addColumn( 'carrier_id', 'integer', ['null' => true, 'default' => null])
      ->addColumn( 'day', 'date', ['null' => true, 'default' => null])
      ->addIndex( ['carrier_id'])
      ->create();
  }
}
