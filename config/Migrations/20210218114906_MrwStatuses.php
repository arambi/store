<?php
use Migrations\AbstractMigration;

class MrwStatuses extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     * @return void
     */
    public function change()
    {
        $this->table('store_mrw_statuses')
            ->addColumn('shipping_number', 'string', ['limit' => 36, 'null' => true, 'default' => null])
            ->addColumn('Estado', 'string', ['limit' => 4, 'null' => true, 'default' => null])
            ->addColumn('EstadoDescripcion', 'string', ['limit' => 36, 'null' => true, 'default' => null])
            ->addColumn('FechaEntrega', 'datetime', ['null' => true, 'default' => null])
            ->addColumn('Intentos', 'integer', ['null' => true, 'default' => null])
            ->addColumn('NumAlbaran', 'integer', ['limit' => 36, 'null' => true, 'default' => null])
            ->addColumn('PersonaEntrega', 'integer', ['limit' => 64, 'null' => true, 'default' => null])
            ->addColumn('created', 'datetime', ['null' => true, 'default' => null])
            ->addColumn('modified', 'datetime', ['null' => true, 'default' => null])
            ->addIndex('shipping_number')
            ->create();

        $this->table('store_mrw_deliveries')
            ->addColumn('Estado', 'string', ['limit' => 4, 'null' => true, 'default' => null])
            ->update();

    }
}
