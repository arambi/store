<?php
use Migrations\AbstractMigration;

class AttributesLegacies extends AbstractMigration
{
  /**
   * Change Method.
   *
   * More information on this method is available here:
   * http://docs.phinx.org/en/latest/migrations.html#the-change-method
   * @return void
   */
  public function change()
  {
    $attributes = $this->table( 'store_attributes');
    $attributes
      ->addColumn( 'legacy_id', 'string', ['null' => true, 'default' => null, 'limit' => 32])
      ->addIndex( 'legacy_id')
      ->update();

    $product_attributes = $this->table( 'store_product_attributes');
    $product_attributes
      ->addColumn( 'legacy_id', 'string', ['null' => true, 'default' => null, 'limit' => 32])
      ->addIndex( 'legacy_id')
      ->update();
  }
}
