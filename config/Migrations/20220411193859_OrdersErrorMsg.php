<?php
use Migrations\AbstractMigration;

class OrdersErrorMsg extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     * @return void
     */
    public function change()
    {
        $this->table('store_orders')
            ->addColumn('payment_error_code', 'string', ['null' => true, 'default' => null])
            ->update();

    }
}
