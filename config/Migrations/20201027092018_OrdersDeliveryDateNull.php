<?php

use Migrations\AbstractMigration;

class OrdersDeliveryDateNull extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     * @return void
     */
    public function up()
    {
        $orders = $this->table('store_orders');
        $orders->changeColumn('delivery_date', 'date', ['null' => true, 'default' => null])
            ->update();
    }

    public function down()
    {
        
    }
}
