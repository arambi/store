<?php
use Migrations\AbstractMigration;

class CouponsStates extends AbstractMigration
{
  /**
   * Change Method.
   *
   * More information on this method is available here:
   * http://docs.phinx.org/en/latest/migrations.html#the-change-method
   * @return void
   */
  public function change()
  {
    $coupons_states = $this->table( 'store_coupons_states');
    $coupons_states
      ->addColumn( 'coupon_id', 'integer', [ 'null' => true, 'default' => null])
      ->addColumn( 'state_id', 'integer', [ 'null' => true, 'default' => null])
      ->addIndex(['coupon_id', 'state_id'])
      ->create();

    $coupons = $this->table( 'store_coupons');
    $coupons
      ->addColumn( 'states_exclude', 'boolean', [ 'null' => false, 'default' => 0])
      ->update();
  }
}
