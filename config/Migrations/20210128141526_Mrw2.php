<?php
use Migrations\AbstractMigration;

class Mrw2 extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     * @return void
     */
    public function change()
    {
        $this->table('store_mrw_deliveries')
            ->removeColumn('order_id')
            ->addColumn('content_id', 'integer', ['null' => false])
            ->addColumn('model', 'string', ['limit' => 32, 'null' => true, 'default' => null])
            ->addIndex(['content_id'])
            ->addIndex(['model'])
            ->update();
    }
}
