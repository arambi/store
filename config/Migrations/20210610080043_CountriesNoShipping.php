<?php
use Migrations\AbstractMigration;

class CountriesNoShipping extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     * @return void
     */
    public function change()
    {
        $this->table('store_countries')
            ->addColumn('active_no_carrier', 'boolean', ['null' => false, 'default' => 0])
            ->addIndex('active_no_carrier')
            ->update();
    }
}
