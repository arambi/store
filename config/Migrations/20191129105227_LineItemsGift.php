<?php
use Migrations\AbstractMigration;

class LineItemsGift extends AbstractMigration
{

  public function change()
  {
    $line_items = $this->table( 'store_line_items');
    $line_items
      ->addColumn( 'is_gift', 'boolean', ['null' => false, 'default' => 0])
      ->addColumn( 'created', 'datetime', ['default' => null])
      ->addColumn( 'modified', 'datetime', ['default' => null])
      ->update();
  }
}