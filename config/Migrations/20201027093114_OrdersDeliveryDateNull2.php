<?php

use Migrations\AbstractMigration;

class OrdersDeliveryDateNull2 extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     * @return void
     */
    public function up()
    {
        $orders = $this->table('store_orders');
        $orders->changeColumn('order_date', 'datetime', ['null' => true, 'default' => null])
            ->update();
    }

    public function down()
    {
        
    }
}
