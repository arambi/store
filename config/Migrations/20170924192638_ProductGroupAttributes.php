<?php
use Migrations\AbstractMigration;

class ProductGroupAttributes extends AbstractMigration
{
  /**
   * Change Method.
   *
   * More information on this method is available here:
   * http://docs.phinx.org/en/latest/migrations.html#the-change-method
   * @return void
   */
  public function change()
  {
    $product_attribute_groups = $this->table( 'store_product_attribute_groups');
    $product_attribute_groups
      ->addColumn( 'product_id', 'integer', ['null' => true, 'default' => null])
      ->addColumn( 'attribute_group_id', 'integer', ['null' => true, 'default' => null])
      ->addIndex( ['product_id', 'attribute_group_id'])
      ->addIndex( ['product_id'])
      ->addIndex( ['attribute_group_id'])
      ->create();
  }
}
