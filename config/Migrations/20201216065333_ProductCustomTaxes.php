<?php
use Store\Config\StoreConfig;
use Migrations\AbstractMigration;

class ProductCustomTaxes extends AbstractMigration
{
    public function up()
    {
        $productsTable = StoreConfig::getProductsModel();
        $table = $this->table($productsTable->getTable());

        $table
            ->addColumn( 'store_custom_taxes', 'float', ['precision' => 20, 'scale' => 3, 'null' => true, 'default' => 0])
            ->addColumn( 'store_has_custom_taxes', 'boolean', ['null' => false, 'default' => 0])
            ->update();
        
        $this->table('store_line_items')
            ->addColumn( 'store_custom_taxes', 'float', ['precision' => 20, 'scale' => 3, 'null' => true, 'default' => 0])
            ->addColumn( 'store_has_custom_taxes', 'boolean', ['null' => false, 'default' => 0])
            ->update();
        ;
    }

    public function down()
    {
        $productsTable = StoreConfig::getProductsModel();
        $table = $this->table($productsTable->getTable());

        if ($table->hasColumn('store_custom_taxes')) {
            $table
                ->removeColumn('store_custom_taxes')
                ->removeColumn('store_has_custom_taxes')
                ->update();
        }

        $this->table('store_line_items')
            ->removeColumn( 'store_custom_taxes')
            ->removeColumn( 'store_has_custom_taxes')
            ->update();
    }
}
