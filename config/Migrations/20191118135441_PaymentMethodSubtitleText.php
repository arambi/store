<?php
use Migrations\AbstractMigration;

class PaymentMethodSubtitleText extends AbstractMigration
{
  /**
   * Change Method.
   *
   * More information on this method is available here:
   * http://docs.phinx.org/en/latest/migrations.html#the-change-method
   * @return void
   */
  public function change()
  {
    $payment_methods = $this->table( 'store_payment_methods');
    $payment_methods
      ->changeColumn( 'subtitle', 'text', ['null' => true, 'default' => null])
      ->update();
    
    $payment_methods_translations = $this->table( 'store_payment_methods_translations');
    $payment_methods_translations
      ->changeColumn( 'subtitle', 'text', ['null' => true, 'default' => null])
      ->update(); 
  }
}
