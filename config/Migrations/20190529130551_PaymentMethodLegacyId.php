<?php
use Migrations\AbstractMigration;

class PaymentMethodLegacyId extends AbstractMigration
{
  /**
   * Change Method.
   *
   * More information on this method is available here:
   * http://docs.phinx.org/en/latest/migrations.html#the-change-method
   * @return void
   */
  public function change()
  {
    $payment_methods = $this->table( 'store_payment_methods');
    $payment_methods
      ->addColumn( 'legacy_id', 'string', ['null' => true, 'default' => null, 'limit' => 50])
      ->addIndex( ['legacy_id'])
      ->update();
  }
}
