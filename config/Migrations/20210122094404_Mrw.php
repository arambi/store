<?php
use Migrations\AbstractMigration;

class Mrw extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     * @return void
     */
    public function change()
    {
        $this->table('store_mrw_deliveries')
            ->addColumn('order_id', 'integer', ['null' => false])
            ->addColumn('proccesed', 'boolean', ['null' => false, 'default' => 0])
            ->addColumn('error', 'boolean', ['null' => false, 'default' => 0])
            ->addColumn('response', 'text', ['null' => true, 'default' => null])
            ->addColumn('request_number', 'string', ['limit' => 32, 'null' => true, 'default' => null])
            ->addColumn('shipping_number', 'string', ['limit' => 32, 'null' => true, 'default' => null])
            ->addColumn('message', 'text', ['null' => true, 'default' => null])
            ->addColumn('state', 'string', ['limit' => 1, 'null' => true, 'default' => null])
            ->addColumn('ticket_file', 'string', ['limit' => 32, 'null' => true, 'default' => null])
            ->addColumn('created', 'datetime', ['null' => true, 'default' => null])
            ->addColumn('modified', 'datetime', ['null' => true, 'default' => null])
            ->create();
    }
}
