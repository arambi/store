<?php

use Store\Config\StoreConfig;
use Migrations\AbstractMigration;

class ProductsStockTotal extends AbstractMigration
{

    public function up()
    {
        $productsTable = StoreConfig::getProductsModel();
        $table = $this->table($productsTable->getTable());

        $table
            ->addColumn('quantity_total', 'float', ['null' => true, 'default' => 0])
            ->addIndex('quantity_total')
            ->update();
    }

    public function down()
    {
        $productsTable = StoreConfig::getProductsModel();
        $table = $this->table($productsTable->getTable());

        if ($table->hasColumn('quantity_total')) {
            $table
                ->removeColumn('quantity_total')
                ->removeIndex('quantity_total')
                ->update();
        }
    }
}
