<?php
use Migrations\AbstractMigration;

class GiftcardFromName extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     * @return void
     */
    public function change()
    {
        $line_items = $this->table( 'store_line_items');
        $line_items
            ->addColumn( 'giftcard_from_name', 'string', ['null' => true, 'default' => null])
            ->update();
    }
}
