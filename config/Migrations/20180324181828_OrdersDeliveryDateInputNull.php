<?php
use Migrations\AbstractMigration;

class OrdersDeliveryDateInputNull extends AbstractMigration
{
  /**
   * Change Method.
   *
   * More information on this method is available here:
   * http://docs.phinx.org/en/latest/migrations.html#the-change-method
   * @return void
   */
  public function change()
  {
    $orders = $this->table( 'store_orders');
    $orders->changeColumn( 'delivery_date_input', 'date', ['null' => true, 'default' => null])
      ->update();
      
  }
}
