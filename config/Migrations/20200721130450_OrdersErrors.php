<?php
use Migrations\AbstractMigration;

class OrdersErrors extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     * @return void
     */
    public function change()
    {
        $this->table( 'store_order_user_sessions')
            ->addColumn( 'session_id', 'string', ['null' => true, 'default' => null, 'limit' => 32])
            ->addColumn( 'ip', 'string', ['null' => true, 'default' => null, 'limit' => 32])
            ->addColumn( 'browser', 'string', ['null' => true, 'default' => null, 'limit' => 32])
            ->addColumn( 'platform', 'string', ['null' => true, 'default' => null, 'limit' => 16])
            ->addColumn( 'user_id', 'integer', ['null' => true, 'default' => null])
            ->addColumn( 'created', 'datetime', ['default' => null])
            ->addIndex( 'user_id')
            ->addIndex( 'session_id')
            ->addIndex( 'ip')
            ->addIndex( 'created')
            ->create();

        $this->table( 'store_order_user_actions')
            ->addColumn( 'session_id', 'integer', ['null' => true, 'default' => null])
            ->addColumn( 'errors', 'text', ['null' => true, 'default' => null])
            ->addColumn( 'post', 'text', ['null' => true, 'default' => null])
            ->addColumn( 'response', 'text', ['null' => true, 'default' => null])
            ->addColumn( 'url', 'string', ['null' => true, 'default' => null])
            ->addColumn( 'action', 'string', ['null' => true, 'default' => null, 'limit' => 32])
            ->addColumn( 'created', 'datetime', ['default' => null])
            ->addIndex( 'action')
            ->addIndex( 'session_id')
            ->addIndex( 'created')
            ->create();
    }
}
