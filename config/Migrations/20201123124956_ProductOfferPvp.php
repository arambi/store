<?php

use Store\Config\StoreConfig;
use Migrations\AbstractMigration;

class ProductOfferPvp extends AbstractMigration
{
    public function up()
    {
        $productsTable = StoreConfig::getProductsModel();
        $table = $this->table($productsTable->getTable());

        $table
            ->addColumn('store_offer_price_global', 'float', ['precision' => 20, 'scale' => 6, 'null' => true, 'default' => 0])
            ->addIndex('store_offer_price_global')
            ->update();
    }

    public function down()
    {
        $productsTable = StoreConfig::getProductsModel();
        $table = $this->table($productsTable->getTable());

        if ($table->hasColumn('store_offer_price_global')) {
            $table
                ->removeColumn('store_offer_price_global')
                ->removeIndex('store_offer_price_global')
                ->update();
        }
    }
}
