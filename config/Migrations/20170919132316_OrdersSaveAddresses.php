<?php
use Migrations\AbstractMigration;

class OrdersSaveAddresses extends AbstractMigration
{
  /**
   * Change Method.
   *
   * More information on this method is available here:
   * http://docs.phinx.org/en/latest/migrations.html#the-change-method
   * @return void
   */
  public function change()
  {
    $orders = $this->table( 'store_orders');
    $orders
      ->addColumn( 'save_adr_delivery', 'boolean', ['null' => false, 'default' => 0])
      ->addColumn( 'save_adr_invoice', 'boolean', ['null' => false, 'default' => 0])
      ->save();
  }
}
