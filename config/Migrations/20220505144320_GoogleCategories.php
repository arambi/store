<?php

use Migrations\AbstractMigration;

class GoogleCategories extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     * @return void
     */
    public function change()
    {
        $this->table('store_google_categories', ['id' => false, 'primary_key' => ['id']])
            ->addColumn('id', 'char', ['default' => null, 'limit' => 10, 'null' => false])
            ->addColumn('title', 'string', ['null' => true, 'default' => null])
            ->addColumn('path', 'string', ['null' => true, 'default' => null])
            ->addIndex(['title'])
            ->addIndex(['path'])
            ->create();
    }
}
