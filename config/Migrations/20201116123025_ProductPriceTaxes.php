<?php

use Store\Config\StoreConfig;
use Migrations\AbstractMigration;

class ProductPriceTaxes extends AbstractMigration
{

    public function up()
    {
        $productsTable = StoreConfig::getProductsModel();
        $table = $this->table($productsTable->getTable());

        $table
            ->addColumn( 'store_price_pvp', 'float', ['precision' => 20, 'scale' => 6, 'null' => true, 'default' => 0])
            ->addIndex('store_price_pvp')
            ->update();

        $table
            ->addIndex('store_price_order')
            ->update();
    }

    public function down()
    {
        $productsTable = StoreConfig::getProductsModel();
        $table = $this->table($productsTable->getTable());

        if ($table->hasColumn('store_price_pvp')) {
            $table
                ->removeColumn('store_price_pvp')
                ->removeIndex('store_price_pvp')
                ->removeIndex('store_price_order')
                ->update();
        }
    }
}
