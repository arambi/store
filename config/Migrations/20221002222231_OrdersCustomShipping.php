<?php
use Migrations\AbstractMigration;

class OrdersCustomShipping extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     * @return void
     */
    public function change()
    {
        $this->table('store_orders')
            ->addColumn('custom_shipping', 'boolean', ['null' => false, 'default' => 0])
            ->update();
    }
}
