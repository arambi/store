<?php
use Migrations\AbstractMigration;

class CarrierRulesLegacy extends AbstractMigration
{
  /**
   * Change Method.
   *
   * More information on this method is available here:
   * http://docs.phinx.org/en/latest/migrations.html#the-change-method
   * @return void
   */
  public function change()
  {
    $carrier_rules = $this->table( 'store_carrier_rules');
    $carrier_rules
      ->addColumn( 'legacy_id', 'integer', ['null' => true, 'default' => null])
      ->addIndex( ['legacy_id'])
      ->update();
  }
}
