<?php
use Migrations\AbstractMigration;

class StatesLegacyId extends AbstractMigration
{
  public function change()
  {
    $states = $this->table( 'store_states');

    if( !$states->hasColumn( 'legacy_id'))
    {
      $states
        ->addColumn( 'legacy_id', 'string', ['null' => true, 'default' => null, 'limit' => 32])
        ->addIndex( 'legacy_id')
        ->update();
    }

    $countries = $this->table( 'store_countries');

    if( !$countries->hasColumn( 'legacy_id'))
    {
      $countries
        ->addColumn( 'legacy_id', 'string', ['null' => true, 'default' => null, 'limit' => 32])
        ->addIndex( 'legacy_id')
        ->update();
    }


  }
}
