<?php
use Migrations\AbstractMigration;

class StatesCp extends AbstractMigration
{
  /**
   * Change Method.
   *
   * More information on this method is available here:
   * http://docs.phinx.org/en/latest/migrations.html#the-change-method
   * @return void
   */
  public function change()
  {
    $this->table( 'store_states')
      ->addColumn( 'postcode', 'string', ['null' => true, 'default' => null, 'limit' => 4])
      ->addIndex( 'postcode')
      ->update();
  }
}
