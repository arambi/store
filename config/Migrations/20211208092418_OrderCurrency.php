<?php
use Migrations\AbstractMigration;

class OrderCurrency extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     * @return void
     */
    public function change()
    {
        $this->table('store_orders')
            ->addColumn('currency', 'string', ['null' => true, 'default' => null, 'limit' => 3])
            ->update();

        $this->table('store_line_items')
            ->addColumn('currency', 'string', ['null' => true, 'default' => null, 'limit' => 3])
            ->update();

        $this->table('store_invoice_refund_line_items')
            ->addColumn('currency', 'string', ['null' => true, 'default' => null, 'limit' => 3])
            ->update();

        $this->table('store_invoice_refunds')
            ->addColumn('currency', 'string', ['null' => true, 'default' => null, 'limit' => 3])
            ->update();

        
        foreach ([
            'store_orders',
            'store_line_items',
            'store_invoice_refund_line_items',
            'store_invoice_refunds',
        ] as $table) {
            $this->execute("UPDATE $table SET currency = 'eur'");
        }
    }
}
