<?php

use Migrations\AbstractMigration;
use Store\Config\StoreConfig;

class StorePricesOffer extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     * @return void
     */
    public function up()
    {
        $productsTable = StoreConfig::getProductsModel();
        $table = $this->table($productsTable->getTable());

        if (!$table->hasColumn('store_price_offer')) {
            $table
                ->addColumn( 'store_price_offer', 'float', ['precision' => 20, 'scale' => 6, 'null' => true, 'default' => 0])
                ->update();
        }

        $table = $this->table('store_product_attributes');

        if (!$table->hasColumn('store_price_offer')) {
            $table
                ->addColumn( 'store_price_offer', 'float', ['precision' => 20, 'scale' => 6, 'null' => true, 'default' => 0])
                ->update();
        }
    }

    public function down()
    {
        $productsTable = StoreConfig::getProductsModel();
        $table = $this->table($productsTable->getTable());

        if ($table->hasColumn('store_price_offer')) {
            $table
                ->removeColumn( 'store_price_offer')
                ->update();
        }

        $table = $this->table('store_product_attributes');

        if ($table->hasColumn('store_price_offer')) {
            $table
                ->removeColumn( 'store_price_offer')
                ->update();
        }
    }
}
