<?php
use Migrations\AbstractMigration;

class CarriersInsuranceChange extends AbstractMigration
{
  /**
   * Change Method.
   *
   * More information on this method is available here:
   * http://docs.phinx.org/en/latest/migrations.html#the-change-method
   * @return void
   */
  public function change()
  {
    $this->table( 'store_carriers')
      ->removeColumn( 'insurance_min')
      ->addColumn( 'insurance_value', 'float', ['null' => true, 'default' => NULL])
      ->update();
  }
}
