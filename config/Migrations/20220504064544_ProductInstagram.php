<?php
use Store\Config\StoreConfig;
use Migrations\AbstractMigration;

class ProductInstagram extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     * @return void
     */
    public function change()
    {
        $productsTable = StoreConfig::getProductsModel();
        $table = $this->table($productsTable->getTable());

        $table
            ->addColumn('to_instagram', 'boolean', ['null' => false, 'default' => 0])
            ->addIndex('to_instagram')
            ->update();
    }
}
