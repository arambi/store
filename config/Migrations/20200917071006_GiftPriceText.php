<?php
use Migrations\AbstractMigration;

class GiftPriceText extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     * @return void
     */
    public function up()
    {
        $line_items = $this->table( 'store_line_items');

        if ($line_items->hasColumn('giftcard_price_text')) {
            return;
        }

        $line_items
            ->addColumn( 'giftcard_price_text', 'integer', ['null' => true, 'default' => null])
            ->addColumn( 'giftcard_dedicatoria', 'text', ['null' => true, 'default' => null])
            ->addColumn( 'giftcard_hash', 'string', ['null' => true, 'default' => null])
            ->update();
    }
}
