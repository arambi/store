<?php
use Migrations\AbstractMigration;

class OrderOrderRule extends AbstractMigration
{
  /**
   * Change Method.
   *
   * More information on this method is available here:
   * http://docs.phinx.org/en/latest/migrations.html#the-change-method
   * @return void
   */
  public function change()
  {
    $orders = $this->table( 'store_orders');
    $orders
      ->addColumn( 'carrier_rule_id', 'integer', ['null' => true, 'default' => NULL])
      ->update();
  }
}
