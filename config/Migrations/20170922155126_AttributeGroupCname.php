<?php
use Migrations\AbstractMigration;

class AttributeGroupCname extends AbstractMigration
{

  public function change()
  {
    $attribute_groups = $this->table( 'store_attribute_groups');
    $attribute_groups
      ->addColumn( 'cname', 'string', ['null' => true, 'default' => null, 'limit' => 32])
      ->update();
  }
}
