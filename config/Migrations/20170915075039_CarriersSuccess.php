<?php
use Migrations\AbstractMigration;

class CarriersSuccess extends AbstractMigration
{

	public function change()
	{
		$carriers = $this->table( 'store_payment_methods', ['id' => false, 'primary_key' => ['id', 'locale']]);
		$carriers
			->addColumn( 'for_users', 'boolean', ['null' => true, 'default' => 0])
			->addColumn( 'title_success', 'string', ['null' => true, 'default' => null])
			->addColumn( 'text_success', 'text', ['null' => true, 'default' => null])
			->save(); 

		$carriers_translations = $this->table( 'store_payment_methods_translations', ['id' => false, 'primary_key' => ['id', 'locale']]);
		$carriers_translations
			->addColumn( 'id', 'integer', ['null' => false])
			->addColumn( 'locale', 'string', ['null' => true, 'default' => null, 'limit' => 5])
			->addColumn( 'title_success', 'string', ['null' => true, 'default' => null])
			->addColumn( 'text_success', 'text', ['null' => true, 'default' => null])
			->create(); 
	}
}
