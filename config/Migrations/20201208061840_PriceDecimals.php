<?php

use Store\Config\StoreConfig;
use Migrations\AbstractMigration;

class PriceDecimals extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     * @return void
     */
    public function change()
    {
        $productsTable = StoreConfig::getProductsModel();
        $table = $this->table($productsTable->getTable());

        $table
            ->changeColumn('store_offer_price_global', 'float', ['precision' => 20, 'scale' => 3, 'null' => true, 'default' => 0])
            ->changeColumn('store_price_pvp', 'float', ['precision' => 20, 'scale' => 3, 'null' => true, 'default' => 0])
            ->changeColumn('store_price_order', 'float', ['precision' => 20, 'scale' => 3, 'null' => true, 'default' => 0])
            ->changeColumn('store_price_offer', 'float', ['precision' => 20, 'scale' => 3, 'null' => true, 'default' => 0])
            ->changeColumn('store_price', 'float', ['precision' => 20, 'scale' => 3, 'null' => true, 'default' => 0])
            ->update();

        $this->table('store_product_attributes')
            ->changeColumn('store_price_offer', 'float', ['precision' => 20, 'scale' => 3, 'null' => true, 'default' => 0])
            ->changeColumn('store_price', 'float', ['precision' => 20, 'scale' => 3, 'null' => true, 'default' => 0])
            ->update();

        $this->table('store_orders')
            ->changeColumn('points_discount', 'float', ['precision' => 20, 'scale' => 3, 'null' => true, 'default' => 0])
            ->changeColumn('points_discount_tax_rate', 'float', ['precision' => 20, 'scale' => 3, 'null' => true, 'default' => 0])
            ->changeColumn('points_discount_tax', 'float', ['precision' => 20, 'scale' => 3, 'null' => true, 'default' => 0])
            ->changeColumn('shipping_price', 'float', ['precision' => 20, 'scale' => 3, 'null' => true, 'default' => 0])
            ->changeColumn('shipping_handling', 'float', ['precision' => 20, 'scale' => 3, 'null' => true, 'default' => 0])
            ->changeColumn('shipping_handling_tax_rate', 'float', ['precision' => 20, 'scale' => 3, 'null' => true, 'default' => 0])
            ->changeColumn('shipping_handling_tax', 'float', ['precision' => 20, 'scale' => 3, 'null' => true, 'default' => 0])
            ->changeColumn('coupon_discount', 'float', ['precision' => 20, 'scale' => 3, 'null' => true, 'default' => 0])
            ->changeColumn('promo_discount', 'float', ['precision' => 20, 'scale' => 3, 'null' => true, 'default' => 0])
            ->update();
    }
}
