<?php
use Migrations\AbstractMigration;

class Coupons extends AbstractMigration
{
  /**
   * Change Method.
   *
   * More information on this method is available here:
   * http://docs.phinx.org/en/latest/migrations.html#the-change-method
   * @return void
   */
  public function change()
  {
    $coupons = $this->table( 'store_coupons');
    $coupons
      ->addColumn( 'title', 'string', ['null' => true, 'default' => null, 'limit' => 255])
      ->addColumn( 'code', 'string', ['null' => true, 'default' => null, 'limit' => 32])
      ->addColumn( 'type', 'string', ['null' => true, 'default' => null, 'limit' => 16])
      ->addColumn( 'value', 'integer', ['null' => true, 'default' => null, 'limit' => 3])
      ->addColumn( 'active', 'boolean', ['null' => false, 'default' => 0])
      ->addColumn( 'start_on', 'date', ['null' => true, 'default' => null])
      ->addColumn( 'finish_on', 'date', ['null' => true, 'default' => null])
      ->addColumn( 'created', 'datetime', ['default' => null])
      ->addColumn( 'modified', 'datetime', ['default' => null])
      ->addIndex( ['code'])
      ->addIndex( ['active'])
      ->addIndex( ['active', 'start_on', 'finish_on'])
      ->addIndex( ['start_on', 'finish_on'])
      ->addIndex( ['start_on'])
      ->addIndex( ['finish_on'])
      ->create();

    $coupons_orders = $this->table( 'store_coupons_orders');
    $coupons_orders
      ->addColumn( 'coupon_id', 'integer', ['null' => true, 'default' => null, 'limit' => 3])
      ->addColumn( 'user_id', 'integer', ['null' => true, 'default' => null, 'limit' => 3])
      ->addColumn( 'order_id', 'integer', ['null' => true, 'default' => null, 'limit' => 3])
      ->addColumn( 'created', 'datetime', ['default' => null])
      ->addColumn( 'modified', 'datetime', ['default' => null])
      ->addIndex( ['coupon_id'])
      ->addIndex( ['user_id'])
      ->addIndex( ['order_id'])
      ->create();

    $orders = $this->table( 'store_orders');
    $orders
      ->addColumn( 'coupon_discount', 'float', ['precision' => 20, 'scale' => 6, 'null' => false, 'default' => 0])
      ->update();
  }
}
