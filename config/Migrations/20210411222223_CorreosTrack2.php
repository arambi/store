<?php
use Migrations\AbstractMigration;

class CorreosTrack2 extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     * @return void
     */
    public function change()
    {
        $this->table('store_orders')
            ->addColumn('correos_track_data', 'text', ['null' => true, 'default' => null])
            ->update();
    }
}
