<?php
use Migrations\AbstractMigration;

class PaymentPriceChange extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     * @return void
     */
    public function change()
    {
        $this->table( 'store_orders')
            ->changeColumn( 'payment_price', 'float', ['precision' => 20, 'null' => false, 'default' => 0])
            ->update();
    }
}
