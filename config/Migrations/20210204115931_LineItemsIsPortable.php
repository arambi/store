<?php
use Migrations\AbstractMigration;

class LineItemsIsPortable extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     * @return void
     */
    public function change()
    {
        $line_items = $this->table( 'store_line_items');
        $line_items
            ->addColumn( 'no_portable', 'boolean', ['null' => false, 'default' => 0])
            ->update();
    }
}
