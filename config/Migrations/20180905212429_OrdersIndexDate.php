<?php
use Migrations\AbstractMigration;

class OrdersIndexDate extends AbstractMigration
{

  public function change()
  {
    $orders = $this->table( 'store_orders');
    $orders
      ->addIndex( 'order_date')
      ->update();
  }
}
