<?php
use Migrations\AbstractMigration;

class UsersPayments extends AbstractMigration
{
  /**
   * Change Method.
   *
   * More information on this method is available here:
   * http://docs.phinx.org/en/latest/migrations.html#the-change-method
   * @return void
   */
  public function change()
  {
    $users_payment_methods = $this->table( 'store_users_payment_methods');
    $users_payment_methods
      ->addColumn( 'user_id', 'integer', ['null' => true, 'default' => null])
      ->addColumn( 'payment_method_id', 'integer', ['null' => true, 'default' => null])
      ->addIndex( ['user_id', 'payment_method_id'])
      ->addIndex( ['user_id'])
      ->addIndex( ['payment_method_id'])
      ->save();
  }
}
