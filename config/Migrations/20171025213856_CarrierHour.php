<?php
use Migrations\AbstractMigration;

class CarrierHour extends AbstractMigration
{
  /**
   * Change Method.
   *
   * More information on this method is available here:
   * http://docs.phinx.org/en/latest/migrations.html#the-change-method
   * @return void
   */
  public function change()
  {

    $carriers = $this->table( 'store_carriers');
    $carriers
      ->addColumn( 'hour', 'string', ['null' => true, 'default' => NULL, 'limit' => 5])
      ->update();

    $orders = $this->table( 'store_orders');
    $orders
      ->addColumn( 'delivery_date_start', 'date', ['null' => true, 'default' => NULL])
      ->update();
      
  }
}
