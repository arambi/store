<?php

use Migrations\AbstractMigration;

class Correos extends AbstractMigration
{
    public function change()
    {
        $this->table('store_correos_preregistro')
            ->addColumn('sended', 'boolean', ['null' => false, 'default' => 0])
            ->addColumn('sended_at', 'datetime', ['null' => true, 'default' => null])
            ->addColumn('order_id', 'integer', ['null' => true, 'default' => null])
            ->addColumn('line_item_id', 'integer', ['null' => true, 'default' => null])
            ->addColumn('fecha_respuesta', 'datetime', ['null' => true, 'default' => null])
            ->addColumn('cod_expedicion', 'string', ['limit' => 64, 'null' => true, 'default' => null])
            ->addColumn('referencia_expedicion', 'string', ['limit' => 64, 'null' => true, 'default' => null])
            ->addColumn('resultado', 'integer', ['limit' => 3, 'null' => true, 'default' => null])
            ->addColumn('cod_envio', 'string', ['limit' => 64, 'null' => true, 'default' => null])
            ->addColumn('cod_manifiesto', 'string', ['limit' => 64, 'null' => true, 'default' => null])
            ->addColumn('sender_city_name', 'string', ['limit' => 64, 'null' => true, 'default' => null])
            ->addColumn('sender_street_name', 'string', ['limit' => 64, 'null' => true, 'default' => null])
            ->addColumn('sender_province_name', 'string', ['limit' => 64, 'null' => true, 'default' => null])
            ->addColumn('sender_street_number', 'string', ['limit' => 64, 'null' => true, 'default' => null])
            ->addColumn('sender_name', 'string', ['limit' => 64, 'null' => true, 'default' => null])
            ->addColumn('sender_postcode', 'string', ['limit' => 64, 'null' => true, 'default' => null])
            ->addColumn('sender_phone', 'string', ['limit' => 64, 'null' => true, 'default' => null])
            ->addColumn('sender_email', 'string', ['limit' => 64, 'null' => true, 'default' => null])
            ->addColumn('sender_country', 'string', ['limit' => 64, 'null' => true, 'default' => null])
            ->addColumn('sender_zip', 'string', ['limit' => 64, 'null' => true, 'default' => null])
            ->addColumn('receiver_city_name', 'string', ['limit' => 64, 'null' => true, 'default' => null])
            ->addColumn('receiver_street_name', 'string', ['limit' => 64, 'null' => true, 'default' => null])
            ->addColumn('receiver_province_name', 'string', ['limit' => 64, 'null' => true, 'default' => null])
            ->addColumn('receiver_street_number', 'string', ['limit' => 64, 'null' => true, 'default' => null])
            ->addColumn('receiver_name', 'string', ['limit' => 64, 'null' => true, 'default' => null])
            ->addColumn('receiver_postcode', 'string', ['limit' => 64, 'null' => true, 'default' => null])
            ->addColumn('receiver_phone', 'string', ['limit' => 64, 'null' => true, 'default' => null])
            ->addColumn('receiver_email', 'string', ['limit' => 64, 'null' => true, 'default' => null])
            ->addColumn('receiver_country', 'string', ['limit' => 64, 'null' => true, 'default' => null])
            ->addColumn('receiver_zip', 'string', ['limit' => 64, 'null' => true, 'default' => null])
            ->addColumn('created', 'datetime', ['null' => true, 'default' => null])
            ->addColumn('modified', 'datetime', ['null' => true, 'default' => null])
            ->create();
    }
}
