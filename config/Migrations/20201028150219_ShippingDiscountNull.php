<?php

use Migrations\AbstractMigration;

class ShippingDiscountNull extends AbstractMigration
{
    public function up()
    {
        $this->table('store_orders')
            ->changeColumn('shipping_handling', 'float', ['precision' => 20, 'scale' => 6, 'null' => true, 'default' => 0])
            ->update();

        $this->table('store_line_items')
            ->changeColumn('discount', 'float', ['precision' => 20, 'scale' => 6, 'null' => true, 'default' => 0])
            ->update();
    }

    public function down()
    {
    }
}
