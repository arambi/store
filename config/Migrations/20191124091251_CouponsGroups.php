<?php
use Migrations\AbstractMigration;

class CouponsGroups extends AbstractMigration
{
  /**
   * Change Method.
   *
   * More information on this method is available here:
   * http://docs.phinx.org/en/latest/migrations.html#the-change-method
   * @return void
   */
  public function change()
  {
    $coupons_groups = $this->table( 'store_coupons_groups');
    $coupons_groups
      ->addColumn( 'coupon_id', 'integer', [ 'null' => true, 'default' => null])
      ->addColumn( 'group_id', 'integer', [ 'null' => true, 'default' => null])
      ->addIndex(['coupon_id', 'group_id'])
      ->create();
  }
}
