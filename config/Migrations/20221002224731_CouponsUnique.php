<?php

use Migrations\AbstractMigration;

class CouponsUnique extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     * @return void
     */
    public function change()
    {
         $this->table('store_coupons')
            ->addColumn('unique', 'boolean', ['null' => false, 'default' => 0])
            ->update();
    }
}
