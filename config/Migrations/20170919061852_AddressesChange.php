<?php
use Migrations\AbstractMigration;

class AddressesChange extends AbstractMigration
{
  /**
   * Change Method.
   *
   * More information on this method is available here:
   * http://docs.phinx.org/en/latest/migrations.html#the-change-method
   * @return void
   */
  public function change()
  {
    $addresses = $this->table( 'store_addresses');
    $addresses
      ->removeColumn( 'alias')
      ->removeColumn( 'cif')
      ->removeColumn( 'address1')
      ->removeColumn( 'address2')
      ->removeColumn( 'phone_mobile')
      ->addColumn( 'type', 'string', ['null' => true, 'default' => null, 'limit' => 32])
      ->addColumn( 'email', 'string', ['null' => true, 'default' => null])
      ->addColumn( 'address', 'string', ['null' => true, 'default' => null])
      ->addColumn( 'vat_number', 'string', ['null' => true, 'default' => null])
      ->save();
  }
}
