<?php

use Migrations\AbstractMigration;

class Refunds extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     * @return void
     */
    public function change()
    {
        $this->table('store_refunds')
            ->addColumn('order_id', 'integer', ['null' => true, 'default' => null])
            ->addColumn('status', 'string', ['limit' => 32, 'null' => true, 'default' => null])
            ->addColumn('created', 'datetime', ['default' => null])
            ->addColumn('modified', 'datetime', ['default' => null])
            ->create();

        $this->table('store_refunds_items')
            ->addColumn('refund_id', 'integer', ['null' => true, 'default' => null])
            ->addColumn('line_item_id', 'integer', ['null' => true, 'default' => null])
            ->addColumn('status', 'string', ['null' => true, 'default' => null, 'limit' => 32])
            ->addColumn('refuse', 'boolean', ['null' => false, 'default' => 0])
            ->addColumn('quantity', 'integer', ['null' => true, 'default' => null])
            ->addColumn('created', 'datetime', ['default' => null])
            ->addColumn('modified', 'datetime', ['default' => null])
            ->create();

        $this->table('store_refunds_histories')
            ->addColumn('user_id', 'integer', ['null' => true, 'default' => null])
            ->addColumn('refund_id', 'integer', ['null' => true, 'default' => null])
            ->addColumn('status', 'string', ['null' => true, 'default' => null, 'limit' => 32])
            ->addColumn('created', 'datetime', ['null' => true, 'default' => null])
            ->addIndex('status')
            ->addIndex('user_id')
            ->addIndex('refund_id')
            ->create();
    }
}
