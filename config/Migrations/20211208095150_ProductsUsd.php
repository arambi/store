<?php

use Store\Config\StoreConfig;
use Migrations\AbstractMigration;

class ProductsUsd extends AbstractMigration
{

    public function up()
    {
        $productsTable = StoreConfig::getProductsModel();
        $table = $this->table($productsTable->getTable());

        $table
            ->addColumn('store_price_usd', 'float', ['precision' => 20, 'scale' => 6, 'null' => true, 'default' => 0])
            ->addIndex('store_price_usd')
            ->update();
    }

    public function down()
    {
        $productsTable = StoreConfig::getProductsModel();
        $table = $this->table($productsTable->getTable());

        if ($table->hasColumn('store_price_usd')) {
            $table
                ->removeColumn('store_price_usd')
                ->removeIndex('store_price_usd')
                ->update();
        }
    }
}
