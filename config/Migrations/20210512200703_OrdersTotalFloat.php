<?php
use Migrations\AbstractMigration;

class OrdersTotalFloat extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     * @return void
     */
    public function change()
    {
        $this->table('store_orders')
            ->changeColumn('total', 'float', ['precision' => 20, 'scale' => 2, 'null' => true, 'default' => 0])
            ->changeColumn('subtotal', 'float', ['precision' => 20, 'scale' => 2, 'null' => true, 'default' => 0])
            ->changeColumn('taxes', 'float', ['precision' => 20, 'scale' => 2, 'null' => true, 'default' => 0])
            ->update();
    }
}
