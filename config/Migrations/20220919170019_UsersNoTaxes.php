<?php
use Migrations\AbstractMigration;

class UsersNoTaxes extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     * @return void
     */
    public function change()
    {
        $this->table('users')
            ->addColumn('store_no_taxes', 'boolean', ['null' => false, 'default' => 0])
            ->update();
    }
}
