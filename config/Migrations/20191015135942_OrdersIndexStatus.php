<?php
use Migrations\AbstractMigration;

class OrdersIndexStatus extends AbstractMigration
{

  public function change()
  {
    $orders = $this->table( 'store_orders');
    $orders
      ->addIndex( 'status')
      ->addIndex( ['status', 'order_date'])
      ->update();
  }
}
