<?php
use Migrations\AbstractMigration;

class InvoicesRefundsConcept extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     * @return void
     */
    public function change()
    {
        $this->table('store_invoice_refund_line_items')
            ->addColumn('concept', 'string', ['null' => true, 'default' => null])
            ->update();
    }
}
