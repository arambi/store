<?php
use Migrations\AbstractMigration;

class PromoPricesMin extends AbstractMigration
{

  public function change()
  {
    $promos = $this->table( 'store_promos');
    $promos
      ->addColumn( 'with_price_min', 'boolean', [ 'null' => false, 'default' => 0])
      ->addColumn( 'price_min', 'float', ['precision' => 20, 'scale' => 6, 'null' => true, 'default' => null])
      ->changeColumn( 'promo_type', 'enum', ['values' => ['global', 'categories', 'products', 'cart', 'free_delivery'], 'null' => false, 'default' => 'global'])
      ->update();

    $orders = $this->table( 'store_orders');
    $orders
      ->addColumn( 'promo_id', 'integer', [ 'null' => true, 'default' => null])
      ->addColumn( 'promo_type', 'string', [ 'limit' => 50, 'null' => true, 'default' => null])
      ->addColumn( 'promo_discount', 'float', ['precision' => 20, 'scale' => 6, 'null' => true, 'default' => null])
      ->update();
  }
}
