<?php
use Migrations\AbstractMigration;

class OrdersPaymentOrderIdt extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     * @return void
     */
    public function change()
    {
        $this->table('store_orders')
            ->addColumn('payment_order_id', 'string', ['null' => true, 'default' => null])
            ->update();
    }
}
