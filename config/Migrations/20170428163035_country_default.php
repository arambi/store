<?php

use Phinx\Migration\AbstractMigration;

class CountryDefault extends AbstractMigration
{

  public function change()
  {
    $countries = $this->table( 'store_countries');
    $countries
      ->addColumn( 'by_default', 'boolean', ['null' => false, 'default' => 0])
      ->save();
  }
}
