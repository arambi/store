<?php

use Migrations\AbstractMigration;

class ReturnedsItems extends AbstractMigration
{
    public function change()
    {
        $line_items = $this->table( 'store_line_items');

        if ($line_items->hasColumn('status')) {
            return;
        }

        $this->table('store_line_items')
            ->addColumn('status', 'string', ['null' => true, 'default' => null, 'limit' => 32])
            ->addIndex('status')
            ->update();

        $this->table('store_line_items_histories')
            ->addColumn('user_id', 'integer', ['null' => true, 'default' => null])
            ->addColumn('line_item_id', 'integer', ['null' => true, 'default' => null])
            ->addColumn('status', 'string', ['null' => true, 'default' => null, 'limit' => 32])
            ->addColumn('created', 'datetime', ['null' => true, 'default' => null])
            ->addIndex('status')
            ->addIndex('user_id')
            ->addIndex('line_item_id')
            ->create();
    }
}
