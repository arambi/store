<?php

use Letter\Collector\LetterCollector;


LetterCollector::add('Store.addPoints', [
    'title' => 'Tienda - Puntos añadidos por el admin',
    'subject' => [
        'spa' => 'Hemos añadido {points} a tu cuenta de {store_name}'
    ],
    'file' => 'store_add_points',
    'vars' => [
        'name' => 'El nombre del usuario',
        'points' => 'El número de puntos',
        'store_name' => 'Nombre de la tienda',
        'total_points' => 'El total de punto de que dispone en la actualidad'
    ]
]);

LetterCollector::add('Store.addPointsOrder', [
    'title' => 'Tienda - Puntos añadidos en el pedido',
    'subject' => [
        'spa' => 'Tienes {points} puntos más en {store_name}'
    ],
    'file' => 'store_add_points_order',
    'vars' => [
        'name' => 'El nombre del usuario',
        'points' => 'El número de puntos',
        'store_name' => 'Nombre de la tienda',
        'total_points' => 'El total de punto de que dispone en la actualidad'
    ]
]);


LetterCollector::add('Store.orderStatusPending', [
    'title' => 'Tienda - Estado de pedido - Pendiente',
    'subject' => [
        'spa' => 'Tu pedido en {web_name} está tramitándose'
    ],
    'file' => 'store_order_status_pending',
    'vars' => [
        'name' => 'El nombre del usuario',
        'order_ref' => 'El número de referencia del pedido',
        'web_name' => 'Nombre de la tienda',
        'order_url' => 'Enlace al pedido',
        'invoice_url' => 'Enlace a la factura en PDF',
    ]
]);


LetterCollector::add('Store.orderStatusAccepted', [
    'title' => 'Tienda - Estado de pedido - Aceptado',
    'subject' => [
        'spa' => 'Tu pedido en {web_name} está aceptado'
    ],
    'file' => 'store_order_status_accepted',
    'vars' => [
        'name' => 'El nombre del usuario',
        'order_ref' => 'El número de referencia del pedido',
        'web_name' => 'Nombre de la tienda',
        'order_url' => 'Enlace al pedido',
        'invoice_url' => 'Enlace a la factura en PDF',
    ]
]);

LetterCollector::add('Store.orderStatusEnvoy', [
    'title' => 'Tienda - Estado de pedido - Enviado',
    'subject' => [
        'spa' => 'Tu pedido en {web_name} ha sido enviado'
    ],
    'file' => 'store_order_status_envoy',
    'vars' => [
        'name' => 'El nombre del usuario',
        'order_ref' => 'El número de referencia del pedido',
        'web_name' => 'Nombre de la tienda',
        'order_url' => 'Enlace al pedido',
        'invoice_url' => 'Enlace a la factura en PDF',
    ]
]);

LetterCollector::add('Store.orderStatusCanceled', [
    'title' => 'Tienda - Estado de pedido - Cancelado',
    'subject' => [
        'spa' => 'Tu pedido en {web_name} ha sido cancelado'
    ],
    'file' => 'store_order_status_canceled',
    'vars' => [
        'name' => 'El nombre del usuario',
        'order_ref' => 'El número de referencia del pedido',
        'web_name' => 'Nombre de la tienda',
        'order_url' => 'Enlace al pedido',
        'invoice_url' => 'Enlace a la factura en PDF',
    ]
]);

LetterCollector::add('Store.orderStatusWaitingPay', [
    'title' => 'Tienda - Estado de pedido - Esperando confirmación bancaria',
    'subject' => [
        'spa' => 'Tu pedido en {web_name} está a la espera de confirmacion bancaria'
    ],
    'file' => 'store_order_status_waiting_pay',
    'vars' => [
        'name' => 'El nombre del usuario',
        'order_ref' => 'El número de referencia del pedido',
        'web_name' => 'Nombre de la tienda',
        'order_url' => 'Enlace al pedido',
        'invoice_url' => 'Enlace a la factura en PDF',
    ]
]);


LetterCollector::add('Store.orderStatusReturnedPendent', [
    'title' => 'Tienda - Estado de pedido - Pendiente de devolución',
    'subject' => [
        'spa' => 'Tu pedido en {web_name} está pendiente de confirmación de devolución'
    ],
    'file' => 'store_order_status_returned_pendent',
    'vars' => [
        'name' => 'El nombre del usuario',
        'order_ref' => 'El número de referencia del pedido',
        'web_name' => 'Nombre de la tienda',
        'order_url' => 'Enlace al pedido',
        'invoice_url' => 'Enlace a la factura en PDF',
    ]
]);

LetterCollector::add('Store.orderStatusReceived', [
    'title' => 'Tienda - Estado de pedido - Entregado',
    'subject' => [
        'spa' => 'Tu pedido en {web_name} ha sido entregado'
    ],
    'file' => 'store_order_status_received',
    'vars' => [
        'name' => 'El nombre del usuario',
        'order_ref' => 'El número de referencia del pedido',
        'web_name' => 'Nombre de la tienda',
        'order_url' => 'Enlace al pedido',
        'invoice_url' => 'Enlace a la factura en PDF',
    ]
]);

LetterCollector::add('Store.orderStatusReturned', [
    'title' => 'Tienda - Estado de pedido - Devuelto',
    'subject' => [
        'spa' => 'Tu pedido en {web_name} ha sido devuelto'
    ],
    'file' => 'store_order_status_returned',
    'vars' => [
        'name' => 'El nombre del usuario',
        'order_ref' => 'El número de referencia del pedido',
        'web_name' => 'Nombre de la tienda',
        'order_url' => 'Enlace al pedido',
        'invoice_url' => 'Enlace a la factura en PDF',
    ]
]);


LetterCollector::add('Store.newOrder', [
    'title' => 'Tienda - Recibido nuevo pedido',
    'subject' => [
        'spa' => 'Recibido un nuevo pedido en {web_name}'
    ],
    'file' => 'store_new_order',
    'vars' => [
        'order_ref' => 'El número de referencia del pedido',
        'invoice' => 'El albarán del pedido',
        'web_name' => 'Nombre de la tienda',
        'order_url' => 'Enlace al pedido',
        'invoice_url' => 'Enlace a la factura en PDF',
    ]
]);


// item_refunded_request
LetterCollector::add('Store.item_refunded_request', [
    'title' => 'Petición de devolución',
    'subject' => [
        'spa' => 'La petición de devolución del producto {title} se ha enviado a {web_name}'
    ],
    'file' => 'item_refunded_request',
    'vars' => [
        'web_name' => 'Nombre de la tienda',
        'title' => 'El nombre del producto',
        'order_ref' => 'El número de referencia del pedido',
        'order_url' => 'Enlace al pedido',
    ]
]);

LetterCollector::add('Store.item_refunded_request_admin', [
    'title' => 'Petición de devolución (admin)',
    'subject' => [
        'spa' => 'Se ha recibido una petición de devolución del producto {title} en {web_name}'
    ],
    'file' => 'item_refunded_request_admin',
    'vars' => [
        'web_name' => 'Nombre de la tienda',
        'title' => 'El nombre del producto',
        'order_ref' => 'El número de referencia del pedido',
        'order_url' => 'Enlace al pedido',
    ]
]);

// item_refunded_working
LetterCollector::add('Store.item_refunded_working', [
    'title' => 'Producto en proceso de devolución',
    'subject' => [
        'spa' => 'El producto {title} está en proceso de devolución a la tienda'
    ],
    'file' => 'item_refunded_working',
    'vars' => [
        'web_name' => 'Nombre de la tienda',
        'title' => 'El nombre del producto',
        'order_ref' => 'El número de referencia del pedido',
        'order_url' => 'Enlace al pedido',
    ]
]);

// item_refunded_working_admin
LetterCollector::add('Store.item_refunded_working_admin', [
    'title' => 'Producto en proceso de devolución (admin)',
    'subject' => [
        'spa' => 'El producto {title} está en proceso de devolución a la tienda'
    ],
    'file' => 'item_refunded_working_admin',
    'vars' => [
        'web_name' => 'Nombre de la tienda',
        'title' => 'El nombre del producto',
        'order_ref' => 'El número de referencia del pedido',
        'order_url' => 'Enlace al pedido',
    ]
]);


// item_refunded_send
LetterCollector::add('Store.item_refunded_send', [
    'title' => 'Envío del producto en devolución',
    'subject' => [
        'spa' => 'El producto {title} ha sido enviado a {web_name} para su devolución'
    ],
    'file' => 'item_refunded_send',
    'vars' => [
        'web_name' => 'Nombre de la tienda',
        'title' => 'El nombre del producto',
        'order_ref' => 'El número de referencia del pedido',
        'order_url' => 'Enlace al pedido',
    ]
]);

LetterCollector::add('Store.item_refunded_send_admin', [
    'title' => 'Envío del producto en devolución (admin)',
    'subject' => [
        'spa' => 'El producto {title} ha sido enviado a {web_name} para su devolución'
    ],
    'file' => 'item_refunded_send_admin',
    'vars' => [
        'web_name' => 'Nombre de la tienda',
        'title' => 'El nombre del producto',
        'order_ref' => 'El número de referencia del pedido',
        'order_url' => 'Enlace al pedido',
    ]
]);

// item_refunded_received
LetterCollector::add('Store.item_refunded_received', [
    'title' => 'El producto en devolución ha llegado a la tienda',
    'subject' => [
        'spa' => 'Se ha recibido en {webname} el producto {title} para su devolución'
    ],
    'file' => 'item_refunded_received',
    'vars' => [
        'web_name' => 'Nombre de la tienda',
        'title' => 'El nombre del producto',
        'order_ref' => 'El número de referencia del pedido',
        'order_url' => 'Enlace al pedido',
    ]
]);

LetterCollector::add('Store.item_refunded_received_admin', [
    'title' => 'El producto en devolución ha llegado a la tienda (admin)',
    'subject' => [
        'spa' => 'Se ha recibido en {webname} el producto {title} para su devolución'
    ],
    'file' => 'item_refunded_received_admin',
    'vars' => [
        'web_name' => 'Nombre de la tienda',
        'title' => 'El nombre del producto',
        'order_ref' => 'El número de referencia del pedido',
        'order_url' => 'Enlace al pedido',
    ]
]);

// item_refunded_accept
LetterCollector::add('Store.item_refunded_accept', [
    'title' => 'Solicitud devolución de producto aceptada',
    'subject' => [
        'spa' => 'La solicitud de la devolución del producto {title} en {web_name} ha sido aceptada'
    ],
    'file' => 'item_refunded_accept',
    'vars' => [
        'web_name' => 'Nombre de la tienda',
        'title' => 'El nombre del producto',
        'order_ref' => 'El número de referencia del pedido',
        'order_url' => 'Enlace al pedido',
    ]
]);

// item_refunded_accept_admin
LetterCollector::add('Store.item_refunded_accept_admin', [
    'title' => 'Solicitud devolución de producto aceptada (admin)',
    'subject' => [
        'spa' => 'La solicitud de la devolución del producto {title} en {web_name} ha sido aceptada'
    ],
    'file' => 'item_refunded_accept_admin',
    'vars' => [
        'web_name' => 'Nombre de la tienda',
        'title' => 'El nombre del producto',
        'order_ref' => 'El número de referencia del pedido',
        'order_url' => 'Enlace al pedido',
    ]
]);

// item_refunded_canceled
LetterCollector::add('Store.item_refunded_canceled', [
    'title' => 'Cancelación de solicitud de devolución de producto',
    'subject' => [
        'spa' => 'La solicitud de devolución de {title} en {web_name} ha sido cancelada'
    ],
    'file' => 'item_refunded_canceled',
    'vars' => [
        'web_name' => 'Nombre de la tienda',
        'title' => 'El nombre del producto',
        'order_ref' => 'El número de referencia del pedido',
        'order_url' => 'Enlace al pedido',
    ]
]);

// item_refunded_canceled_admin
LetterCollector::add('Store.item_refunded_canceled_admin', [
    'title' => 'Cancelación de solicitud de devolución de producto (admin)',
    'subject' => [
        'spa' => 'La solicitud de devolución de {title} en {web_name} ha sido cancelada'
    ],
    'file' => 'item_refunded_canceled_admin',
    'vars' => [
        'web_name' => 'Nombre de la tienda',
        'title' => 'El nombre del producto',
        'order_ref' => 'El número de referencia del pedido',
        'order_url' => 'Enlace al pedido',
    ]
]);

// item_refunded_invoice
LetterCollector::add('Store.item_refunded_invoice', [
    'title' => 'Envío de factura de devolución de producto',
    'subject' => [
        'spa' => 'Se ha generado una factura de devolución en {web_name}'
    ],
    'file' => 'item_refunded_invoice',
    'vars' => [
        'web_name' => 'Nombre de la tienda',
        'title' => 'El nombre del producto',
        'invoice_url' => 'Enlace al pedido',
    ]
]);

LetterCollector::add('Store.item_refunded_invoice_admin', [
    'title' => 'Envío de factura de devolución de producto (admin)',
    'subject' => [
        'spa' => 'Se ha generado una factura de devolución en {web_name}'
    ],
    'file' => 'item_refunded_invoice_admin',
    'vars' => [
        'web_name' => 'Nombre de la tienda',
        'title' => 'El nombre del producto',
        'invoice_url' => 'Enlace al pedido',
    ]
]);


LetterCollector::add( 'Store.giftcard', [
  'title' => 'Tarjeta regalo',
  'subject' => [
    'spa' => 'Te han enviado un regalo'
  ],
  'file' => 'giftcard',
  'vars' => [
    'name_to' => 'El nombre del que recibe el regalo',
    'name_from' => 'El nombre de quien manda el regalo',
    'code' => 'El código del cupón',
    'pdf_button' => 'El enlace al PDF',
    'price' => 'El precio'
  ]
]);


LetterCollector::add('Store.stock_zero', [
	'title' => 'Productos con stock a cero',
	'subject' => [
		'spa' => 'Productos con stock a cero'
	],
	'file' => 'store_stock_zero',
	'vars' => [
		'products' => 'Listado de productos',
	]
]);