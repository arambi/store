<?php

use User\Auth\Access;
use Cake\Event\EventManager;
use Block\Lib\BlocksRegistry;
use Store\Config\StoreConfig;
use Store\Event\ShopListener;
use Store\Event\OrdersListener;
use Store\Event\MonitorListener;
use Store\Event\MailchimpListener;
use Section\Action\ActionCollection;
use Manager\Navigation\NavigationCollection;

// Event Listeners
EventManager::instance()->attach(new OrdersListener());
EventManager::instance()->attach(new ShopListener());
EventManager::instance()->attach(new MonitorListener());

if (StoreConfig::getConfig('mailchimpMarketing')) {
  EventManager::instance()->attach(new MailchimpListener());
}

require 'letters.php';

ActionCollection::set('store_orders_cart', [
  'label' => __d('admin', 'Carro de la compra'),
  'plugin' => 'Store',
  'controller' => 'Orders',
  'action' => 'cart',
  'icon' => 'fa fa-shopping-cart',
  'autocreate' => true,
  'defaults' => [
    'title' => 'Carro de la compra',
    'menu' => 'none',
  ]
]);

ActionCollection::set('store_orders_express', [
  'label' => __d('admin', 'Compra del pedido'),
  'plugin' => 'Store',
  'controller' => 'Orders',
  'action' => 'express',
  'icon' => 'fa fa-shop',
  'autocreate' => true,
  'defaults' => [
    'title' => 'Compra del pedido',
    'menu' => 'none',
  ]
]);


ActionCollection::set('store_orders_delete', [
  'label' => __d('admin', 'Cancelación del pedido'),
  'plugin' => 'Store',
  'controller' => 'Orders',
  'action' => 'delete',
  'icon' => 'fa fa-shop',
  'autocreate' => true,
  'wildcard' => true,
  'defaults' => [
    'title' => 'Cancelación del pedido',
    'menu' => 'none',
  ]
]);


ActionCollection::set('store_orders_address', [
  'label' => __d('admin', 'Direcciones de envio'),
  'plugin' => 'Store',
  'controller' => 'Orders',
  'action' => 'address',
  'icon' => 'fa fa-shop',
]);

ActionCollection::set('store_orders_checkout', [
  'label' => __d('admin', 'Confirmación del pedido'),
  'plugin' => 'Store',
  'controller' => 'Orders',
  'action' => 'checkout',
  'icon' => 'fa fa-shop',
  'autocreate' => true,
  'defaults' => [
    'title' => 'Confirmación del pedido',
    'menu' => 'none',
  ]
]);

ActionCollection::set('store_orders_payment', [
  'label' => __d('admin', 'Pago del pedido'),
  'plugin' => 'Store',
  'controller' => 'Orders',
  'action' => 'payment',
  'icon' => 'fa fa-shop',
  'autocreate' => true,
  'defaults' => [
    'title' => 'Pago del pedido',
    'menu' => 'none',
  ]
]);



ActionCollection::set('store_orders_invoice', [
  'label' => __d('admin', 'Factura de pedido'),
  'plugin' => 'Store',
  'controller' => 'Orders',
  'action' => 'invoices',
  '_ext' => 'pdf',
  'icon' => 'fa fa-shop',
  'autocreate' => true,
  'defaults' => [
    'title' => 'Factura de pedido',
    'menu' => 'none',
  ],
  'actions' => [
    'invoice' => '/:salt',
  ]
]);

ActionCollection::set('store_orders_success', [
  'label' => __d('admin', 'Pago correcto'),
  'plugin' => 'Store',
  'controller' => 'Orders',
  'action' => 'success',
  'icon' => 'fa fa-shop',
  'wildcard' => true,
  'autocreate' => true,
  'defaults' => [
    'title' => 'Pago correcto',
    'menu' => 'none',
  ]
]);

ActionCollection::set('store_orders_error', [
  'label' => __d('admin', 'Pago con error'),
  'plugin' => 'Store',
  'controller' => 'Orders',
  'action' => 'error',
  'icon' => 'fa fa-shop',
  'autocreate' => true,
  'defaults' => [
    'title' => 'Pago con error',
    'menu' => 'none',
  ]
]);


ActionCollection::set('store_users_index', [
  'label' => __d('admin', 'Mi cuenta'),
  'plugin' => 'Store',
  'controller' => 'Users',
  'action' => 'index',
  'icon' => 'fa fa-user',
  'autocreate' => true,
  'defaults' => [
    'title' => 'Mi perfil',
    'menu' => 'none',
  ]
]);

ActionCollection::set('store_users_addresses', [
  'label' => __d('admin', 'Mis direcciones'),
  'plugin' => 'Store',
  'controller' => 'Users',
  'action' => 'addresses',
  'icon' => 'fa fa-user',
  'autocreate' => true,
  'wildcard' => true,
  'defaults' => [
    'title' => 'Mis direcciones',
    'menu' => 'none',
  ]
]);


ActionCollection::set('store_users_returns', [
  'label' => __d('admin', 'Solicitud de devolución'),
  'plugin' => 'Store',
  'controller' => 'Users',
  'action' => 'returns',
  'icon' => 'fa fa-user',
]);

ActionCollection::set('store_users_returns_form', [
  'label' => __d('admin', 'Solicitud de devolución (formulario)'),
  'plugin' => 'Store',
  'controller' => 'Users',
  'action' => 'returns_form',
  'icon' => 'fa fa-user',
]);

ActionCollection::set('store_users_favs', [
  'label' => __d('admin', 'Mis favoritos'),
  'plugin' => 'Store',
  'controller' => 'Favs',
  'action' => 'index',
  'icon' => 'fa fa-user',
  'autocreate' => true,
  'defaults' => [
    'title' => 'Mis favoritos',
    'menu' => 'none',
  ]
]);

ActionCollection::set('store_users_orders', [
  'label' => __d('admin', 'Mis pedidos'),
  'plugin' => 'Store',
  'controller' => 'Users',
  'action' => 'orders',
  'icon' => 'fa fa-user',
  'autocreate' => true,
  'defaults' => [
    'title' => 'Mis pedidos',
    'menu' => 'none',
  ],
  'actions' => [
    'order' => '/:salt',
  ]
]);

ActionCollection::set('store_orders_view', [
  'label' => __d('admin', 'Ver pedido'),
  'plugin' => 'Store',
  'controller' => 'Orders',
  'action' => 'views',
  'icon' => 'fa fa-shop',
  'autocreate' => true,
  'defaults' => [
    'title' => 'Ver pedido',
    'menu' => 'none',
  ],
  'actions' => [
    'view' => '/:salt',
  ]
]);


ActionCollection::set('store_users_edit', [
  'label' => __d('admin', 'Editar datos de perfil'),
  'plugin' => 'Store',
  'controller' => 'Users',
  'action' => 'edit',
  'icon' => 'fa fa-user',
  'autocreate' => true,
  'defaults' => [
    'title' => 'Editar datos de perfil',
    'menu' => 'none',
  ]
]);

ActionCollection::set('store_collections', [
  'label' => __d('admin', 'Colecciones de productos'),
  'plugin' => 'Store',
  'controller' => 'Collections',
  'action' => 'index',
  'icon' => 'fa fa-tree',
  'defaults' => [
    'title' => 'Colecciones',
    'menu' => 'none',
  ],
  'actions' => [
    'view' => '/:slug',
  ]
]);

ActionCollection::set('store_promos', [
  'label' => __d('admin', 'Promociones de productos'),
  'plugin' => 'Store',
  'controller' => 'Promos',
  'action' => 'index',
  'defaults' => [
    'title' => 'Promociones',
    'menu' => 'none',
  ],
  'actions' => [
    'view' => '/:slug',
  ]
]);


ActionCollection::set('store_returneds', [
  'label' => 'Devoluciones',
  'plugin' => 'Store',
  'controller' => 'Returneds',
  'action' => 'add',
  'icon' => 'fa fa-square',
  'defaults' => [
    'title' => 'Formulario de devolución',
    'menu' => 'none',
  ],
]);


// CONFIGURACIÓN DE LOS MENUS DE ADMINISTRACIÓN
NavigationCollection::add([
  'name' => 'Tienda',
  'icon' => 'fa fa-shopping-cart',
  'url' => false,
  'key' => 'shop'
]);


NavigationCollection::add([
  'parent' => 'shop',
  'name' => 'Categorías',
  'parentName' => 'Tienda',
  'plugin' => 'Store',
  'controller' => 'CategoriesShops',
  'action' => 'index',
  'icon' => 'fa fa-book',
]);

NavigationCollection::add([
  'parent' => 'shop',
  'name' => 'Pedidos',
  'parentName' => 'Tienda',
  'plugin' => 'Store',
  'controller' => 'Orders',
  'action' => 'index',
  'icon' => 'fa fa-list-alt',
]);
NavigationCollection::add([
  'parent' => 'shop',
  'name' => 'Pedidos no finalizados',
  'parentName' => 'Tienda',
  'plugin' => 'Store',
  'controller' => 'Orders',
  'action' => 'unfinish',
  'icon' => 'fa fa-list-alt',
]);
NavigationCollection::add( [
  'parent' => 'shop',
  'name' => 'Carritos abandonados',
  'parentName' => 'Tienda',
  'plugin' => 'Store',
  'controller' => 'Orders',
  'action' => 'abandoned',
  'icon' => 'fa fa-list-alt',
]);

NavigationCollection::add([
  'parent' => 'shop',
  'name' => 'Devoluciones de productos',
  'parentName' => 'Tienda',
  'plugin' => 'Store',
  'controller' => 'Refunds',
  'action' => 'index',
  'icon' => 'fa fa-square',
]);

NavigationCollection::add([
  'parent' => 'shop',
  'name' => 'Facturas',
  'parentName' => 'Tienda',
  'plugin' => 'Store',
  'controller' => 'Invoices',
  'action' => 'index',
  'icon' => 'fa fa-sticky-note-o',
]);

NavigationCollection::add([
  'parent' => 'shop',
  'name' => 'Facturas de devolución',
  'parentName' => 'Tienda',
  'plugin' => 'Store',
  'controller' => 'InvoiceRefunds',
  'action' => 'index',
  'icon' => 'fa fa-square',
]);


// NavigationCollection::add( [
//   'parent' => 'shop',
//   'name' => 'Estadísticas',
//   'parentName' => 'Tienda',
//   'plugin' => 'Store',
//   'controller' => 'Orders',
//   'action' => 'statistics',
//   'icon' => 'fa fa-area-chart',
// ]);


NavigationCollection::add([
  'parent' => 'shop',
  'name' => 'Usuarios',
  'parentName' => 'Tienda',
  'plugin' => 'Store',
  'controller' => 'Users',
  'action' => 'index',
  'icon' => 'fa fa-user',
]);

NavigationCollection::add([
  'parent' => 'shop',
  'name' => 'Atributos de producto',
  'parentName' => 'Tienda',
  'plugin' => 'Store',
  'controller' => 'AttributeGroups',
  'action' => 'index',
  'icon' => 'fa fa-book',
]);

NavigationCollection::add([
  'parent' => 'shop',
  'name' => 'Impuestos',
  'parentName' => 'Tienda',
  'plugin' => 'Store',
  'controller' => 'Taxes',
  'action' => 'index',
  'icon' => 'fa fa-book',
]);

NavigationCollection::add([
  'parent' => 'shop',
  'name' => 'Países',
  'parentName' => 'Tienda',
  'plugin' => 'Store',
  'controller' => 'Countries',
  'action' => 'index',
  'icon' => 'fa fa-globe',
]);

NavigationCollection::add([
  'parent' => 'shop',
  'name' => 'Zonas',
  'parentName' => 'Tienda',
  'plugin' => 'Store',
  'controller' => 'Zones',
  'action' => 'index',
  'icon' => 'fa fa-book',
]);


NavigationCollection::add([
  'parent' => 'shop',
  'name' => 'Transporte',
  'parentName' => 'Tienda',
  'plugin' => 'Store',
  'controller' => 'Carriers',
  'action' => 'index',
  'icon' => 'fa fa-truck',
]);

NavigationCollection::add([
  'parent' => 'shop',
  'name' => 'Métodos de pago',
  'parentName' => 'Tienda',
  'plugin' => 'Store',
  'controller' => 'PaymentMethods',
  'action' => 'index',
  'icon' => 'fa fa-credit-card',
]);

NavigationCollection::add([
  'parent' => 'shop',
  'name' => 'Promociones',
  'parentName' => 'Tienda',
  'plugin' => 'Store',
  'controller' => 'Promos',
  'action' => 'index',
  'icon' => 'fa fa-credit-card',
]);

NavigationCollection::add([
  'parent' => 'shop',
  'name' => 'Anadir puntos',
  'parentName' => 'Tienda',
  'plugin' => 'Store',
  'controller' => 'PointsAdds',
  'action' => 'index',
  'icon' => 'fa fa-square',
]);


// NavigationCollection::add( [
//   'parent' => 'shop',
//   'name' => 'Colecciones',
//   'parentName' => 'Tienda',
//   'plugin' => 'Store',
//   'controller' => 'Collections',
//   'action' => 'index',
//   'icon' => 'fa fa-square',
// ]);


// NavigationCollection::add( [
//   'parent' => 'shop',
//   'name' => 'Tallas',
//   'parentName' => 'Tienda',
//   'plugin' => 'Store',
//   'controller' => 'Sizes',
//   'action' => 'index',
//   'icon' => 'fa fa-square',
// ]);



// NavigationCollection::add( [
//   'parent' => 'shop',
//   'name' => 'Devoluciones',
//   'parentName' => 'Tienda',
//   'plugin' => 'Store',
//   'controller' => 'Returneds',
//   'action' => 'index',
//   'icon' => 'fa fa-square',
// ]);



Access::add('store_refunds', [
  'name' => 'Devoluciones de productos',
  'options' => [
    'edit' => [
      'name' => 'Edición',
      'nodes' => [
        [
          'prefix' => 'admin',
          'plugin' => 'Store',
          'controller' => 'Refunds',
          'action' => 'index',
        ],
        [
          'prefix' => 'admin',
          'plugin' => 'Store',
          'controller' => 'Refunds',
          'action' => 'add',
        ],
        [
          'prefix' => 'admin',
          'plugin' => 'Store',
          'controller' => 'Refunds',
          'action' => 'create',
        ],
        [
          'prefix' => 'admin',
          'plugin' => 'Store',
          'controller' => 'Refunds',
          'action' => 'update',
        ],
        [
          'prefix' => 'admin',
          'plugin' => 'Store',
          'controller' => 'Refunds',
          'action' => 'delete',
        ],
        [
          'prefix' => 'admin',
          'plugin' => 'Store',
          'controller' => 'Refunds',
          'action' => 'sortable',
        ],
        [
          'prefix' => 'admin',
          'plugin' => 'Store',
          'controller' => 'Refunds',
          'action' => 'field',
        ]
      ]
    ]
  ]
]);





Access::add('shop', [
  'name' => __d('admin', 'Tienda'),
  'options' => [
    'products' => [
      'name' => __d('admin', 'Productos'),
      'nodes' => [

        // Products
        [
          'prefix' => 'admin',
          'plugin' => 'Store',
          'controller' => 'products',
          'action' => 'index',
        ],
        [
          'prefix' => 'admin',
          'plugin' => 'Store',
          'controller' => 'products',
          'action' => 'create',
        ],
        [
          'prefix' => 'admin',
          'plugin' => 'Store',
          'controller' => 'products',
          'action' => 'update',
        ],
        [
          'prefix' => 'admin',
          'plugin' => 'Store',
          'controller' => 'products',
          'action' => 'delete',
        ],
        [
          'prefix' => 'admin',
          'plugin' => 'Store',
          'controller' => 'products',
          'action' => 'autocomplete',
        ],
        [
          'prefix' => 'admin',
          'plugin' => 'Store',
          'controller' => 'ProductRelateds',
          'action' => 'autocomplete',
        ],


        // Categories
        [
          'prefix' => 'admin',
          'plugin' => 'Store',
          'controller' => 'CategoriesShops',
          'action' => 'index',
        ],
        [
          'prefix' => 'admin',
          'plugin' => 'Store',
          'controller' => 'CategoriesShops',
          'action' => 'create',
        ],
        [
          'prefix' => 'admin',
          'plugin' => 'Store',
          'controller' => 'CategoriesShops',
          'action' => 'update',
        ],
        [
          'prefix' => 'admin',
          'plugin' => 'Store',
          'controller' => 'CategoriesShops',
          'action' => 'delete',
        ],
        [
          'prefix' => 'admin',
          'plugin' => 'Store',
          'controller' => 'CategoriesShops',
          'action' => 'autocomplete',
        ],
        [
          'prefix' => 'admin',
          'plugin' => 'Store',
          'controller' => 'CategoriesShops',
          'action' => 'order',
        ],

        // AttributeGroups
        [
          'prefix' => 'admin',
          'plugin' => 'Store',
          'controller' => 'AttributeGroups',
          'action' => 'index',
        ],
        [
          'prefix' => 'admin',
          'plugin' => 'Store',
          'controller' => 'AttributeGroups',
          'action' => 'create',
        ],
        [
          'prefix' => 'admin',
          'plugin' => 'Store',
          'controller' => 'AttributeGroups',
          'action' => 'update',
        ],
        [
          'prefix' => 'admin',
          'plugin' => 'Store',
          'controller' => 'AttributeGroups',
          'action' => 'delete',
        ],

        // Attributes
        [
          'prefix' => 'admin',
          'plugin' => 'Store',
          'controller' => 'Attributes',
          'action' => 'index',
        ],
        [
          'prefix' => 'admin',
          'plugin' => 'Store',
          'controller' => 'Attributes',
          'action' => 'create',
        ],
        [
          'prefix' => 'admin',
          'plugin' => 'Store',
          'controller' => 'Attributes',
          'action' => 'update',
        ],
        [
          'prefix' => 'admin',
          'plugin' => 'Store',
          'controller' => 'Attributes',
          'action' => 'delete',
        ],
        [
          'prefix' => 'admin',
          'plugin' => 'Store',
          'controller' => 'Attributes',
          'action' => 'sortable',
        ],
        // ProductAttributes
        [
          'prefix' => 'admin',
          'plugin' => 'Store',
          'controller' => 'ProductAttributes',
          'action' => 'index',
        ],
        [
          'prefix' => 'admin',
          'plugin' => 'Store',
          'controller' => 'ProductAttributes',
          'action' => 'create',
        ],
        [
          'prefix' => 'admin',
          'plugin' => 'Store',
          'controller' => 'ProductAttributes',
          'action' => 'update',
        ],
        [
          'prefix' => 'admin',
          'plugin' => 'Store',
          'controller' => 'ProductAttributes',
          'action' => 'delete',
        ],
        [
          'prefix' => 'admin',
          'plugin' => 'Store',
          'controller' => 'ProductAttributes',
          'action' => 'autocomplete',
        ],
      ],
    ],
    'orders' => [
      'name' => __d('admin', 'Pedidos'),
      'nodes' => [
        [
          'prefix' => 'admin',
          'plugin' => 'Store',
          'controller' => 'orders',
          'action' => 'index',
        ],
        [
          'prefix' => 'admin',
          'plugin' => 'Store',
          'controller' => 'orders',
          'action' => 'create',
        ],
        [
          'prefix' => 'admin',
          'plugin' => 'Store',
          'controller' => 'orders',
          'action' => 'update',
        ],
        [
          'prefix' => 'admin',
          'plugin' => 'Store',
          'controller' => 'orders',
          'action' => 'view',
        ],
        [
          'prefix' => 'admin',
          'plugin' => 'Store',
          'controller' => 'orders',
          'action' => 'unfinish',
        ],
        [
          'prefix' => 'admin',
          'plugin' => 'Store',
          'controller' => 'orders',
          'action' => 'abandoned',
        ],
        [
          'prefix' => 'admin',
          'plugin' => 'Store',
          'controller' => 'orders',
          'action' => 'finalize',
        ],
        [
          'prefix' => 'admin',
          'plugin' => 'Store',
          'controller' => 'orders',
          'action' => 'cart',
        ],
        [
          'prefix' => 'admin',
          'plugin' => 'Store',
          'controller' => 'orders',
          'action' => 'abandoned_cart',
        ],
        [
          'prefix' => 'admin',
          'plugin' => 'Store',
          'controller' => 'orders',
          'action' => 'invoice',
        ],
        [
          'prefix' => 'admin',
          'plugin' => 'Store',
          'controller' => 'orders',
          'action' => 'states',
        ],
        [
          'prefix' => 'admin',
          'plugin' => 'Store',
          'controller' => 'orders',
          'action' => 'delete',
        ],
        [
          'prefix' => 'admin',
          'plugin' => 'Store',
          'controller' => 'orders',
          'action' => 'change_status',
        ],
        [
          'prefix' => 'admin',
          'plugin' => 'Store',
          'controller' => 'orders',
          'action' => 'remove_taxes',
        ],
        [
          'prefix' => 'admin',
          'plugin' => 'Store',
          'controller' => 'Users',
          'action' => 'index',
        ],
        [
          'prefix' => 'admin',
          'plugin' => 'Store',
          'controller' => 'Users',
          'action' => 'view',
        ],
        [
          'prefix' => 'admin',
          'plugin' => 'Store',
          'controller' => 'Points',
          'action' => 'addup',
        ],
        [
          'prefix' => 'admin',
          'plugin' => 'Store',
          'controller' => 'Points',
          'action' => 'add',
        ],
      ],
    ],

    'statistics' => [
      'name' => __d('admin', 'Estadísticas'),
      'nodes' => [
        [
          'prefix' => 'admin',
          'plugin' => 'Store',
          'controller' => 'orders',
          'action' => 'statistics',
        ],
      ],
    ],
    'manager' => [
      'name' => __d('admin', 'Configuraciones'),
      'nodes' => [
        // Carriers
        [
          'prefix' => 'admin',
          'plugin' => 'Store',
          'controller' => 'carriers',
          'action' => 'index',
        ],
        [
          'prefix' => 'admin',
          'plugin' => 'Store',
          'controller' => 'carriers',
          'action' => 'create',
        ],
        [
          'prefix' => 'admin',
          'plugin' => 'Store',
          'controller' => 'carriers',
          'action' => 'update',
        ],
        [
          'prefix' => 'admin',
          'plugin' => 'Store',
          'controller' => 'carriers',
          'action' => 'delete',
        ],
        [
          'prefix' => 'admin',
          'plugin' => 'Store',
          'controller' => 'CarrierRules',
          'action' => 'index',
        ],
        [
          'prefix' => 'admin',
          'plugin' => 'Store',
          'controller' => 'CarrierRules',
          'action' => 'add',
        ],
        [
          'prefix' => 'admin',
          'plugin' => 'Store',
          'controller' => 'CarrierRules',
          'action' => 'create',
        ],
        [
          'prefix' => 'admin',
          'plugin' => 'Store',
          'controller' => 'CarrierRules',
          'action' => 'update',
        ],
        [
          'prefix' => 'admin',
          'plugin' => 'Store',
          'controller' => 'CarrierRules',
          'action' => 'delete',
        ],
        [
          'prefix' => 'admin',
          'plugin' => 'Store',
          'controller' => 'CarrierRules',
          'action' => 'field',
        ],
        [
          'prefix' => 'admin',
          'plugin' => 'Store',
          'controller' => 'CarrierRules',
          'action' => 'bulk',
        ],

        // Countries
        [
          'prefix' => 'admin',
          'plugin' => 'Store',
          'controller' => 'countries',
          'action' => 'index',
        ],
        [
          'prefix' => 'admin',
          'plugin' => 'Store',
          'controller' => 'countries',
          'action' => 'create',
        ],
        [
          'prefix' => 'admin',
          'plugin' => 'Store',
          'controller' => 'countries',
          'action' => 'update',
        ],
        [
          'prefix' => 'admin',
          'plugin' => 'Store',
          'controller' => 'countries',
          'action' => 'delete',
        ],
        [
          'prefix' => 'admin',
          'plugin' => 'Store',
          'controller' => 'countries',
          'action' => 'field',
        ],
        // Currencies
        [
          'prefix' => 'admin',
          'plugin' => 'Store',
          'controller' => 'currencies',
          'action' => 'index',
        ],
        [
          'prefix' => 'admin',
          'plugin' => 'Store',
          'controller' => 'currencies',
          'action' => 'create',
        ],
        [
          'prefix' => 'admin',
          'plugin' => 'Store',
          'controller' => 'currencies',
          'action' => 'update',
        ],
        [
          'prefix' => 'admin',
          'plugin' => 'Store',
          'controller' => 'currencies',
          'action' => 'delete',
        ],

        // Payments
        [
          'prefix' => 'admin',
          'plugin' => 'Store',
          'controller' => 'payment_methods',
          'action' => 'index',
        ],
        [
          'prefix' => 'admin',
          'plugin' => 'Store',
          'controller' => 'payment_methods',
          'action' => 'create',
        ],
        [
          'prefix' => 'admin',
          'plugin' => 'Store',
          'controller' => 'payment_methods',
          'action' => 'update',
        ],
        [
          'prefix' => 'admin',
          'plugin' => 'Store',
          'controller' => 'payment_methods',
          'action' => 'delete',
        ],
        [
          'prefix' => 'admin',
          'plugin' => 'Store',
          'controller' => 'payment_methods',
          'action' => 'field',
        ],
        [
          'prefix' => 'admin',
          'plugin' => 'Store',
          'controller' => 'payment_methods',
          'action' => 'sortable',
        ],
        // States
        [
          'prefix' => 'admin',
          'plugin' => 'Store',
          'controller' => 'states',
          'action' => 'index',
        ],
        [
          'prefix' => 'admin',
          'plugin' => 'Store',
          'controller' => 'states',
          'action' => 'create',
        ],
        [
          'prefix' => 'admin',
          'plugin' => 'Store',
          'controller' => 'states',
          'action' => 'update',
        ],
        [
          'prefix' => 'admin',
          'plugin' => 'Store',
          'controller' => 'states',
          'action' => 'delete',
        ],
        [
          'prefix' => 'admin',
          'plugin' => 'Store',
          'controller' => 'states',
          'action' => 'field',
        ],
        [
          'prefix' => 'admin',
          'plugin' => 'Store',
          'controller' => 'states',
          'action' => 'autocomplete',
        ],

        // Taxes
        [
          'prefix' => 'admin',
          'plugin' => 'Store',
          'controller' => 'taxes',
          'action' => 'index',
        ],
        [
          'prefix' => 'admin',
          'plugin' => 'Store',
          'controller' => 'taxes',
          'action' => 'create',
        ],
        [
          'prefix' => 'admin',
          'plugin' => 'Store',
          'controller' => 'taxes',
          'action' => 'update',
        ],
        [
          'prefix' => 'admin',
          'plugin' => 'Store',
          'controller' => 'taxes',
          'action' => 'delete',
        ],

        // Zones
        [
          'prefix' => 'admin',
          'plugin' => 'Store',
          'controller' => 'zones',
          'action' => 'index',
        ],
        [
          'prefix' => 'admin',
          'plugin' => 'Store',
          'controller' => 'zones',
          'action' => 'create',
        ],
        [
          'prefix' => 'admin',
          'plugin' => 'Store',
          'controller' => 'zones',
          'action' => 'update',
        ],
        [
          'prefix' => 'admin',
          'plugin' => 'Store',
          'controller' => 'zones',
          'action' => 'delete',
        ],
      ],
    ]
  ]
]);






Access::add('brands', [
  'name' => 'Marcas',
  'options' => [
    'edit' => [
      'name' => 'Edición',
      'nodes' => [
        [
          'prefix' => 'admin',
          'plugin' => 'Store',
          'controller' => 'Brands',
          'action' => 'index',
        ],
        [
          'prefix' => 'admin',
          'plugin' => 'Store',
          'controller' => 'Brands',
          'action' => 'add',
        ],
        [
          'prefix' => 'admin',
          'plugin' => 'Store',
          'controller' => 'Brands',
          'action' => 'create',
        ],
        [
          'prefix' => 'admin',
          'plugin' => 'Store',
          'controller' => 'Brands',
          'action' => 'update',
        ],
        [
          'prefix' => 'admin',
          'plugin' => 'Store',
          'controller' => 'Brands',
          'action' => 'delete',
        ]
      ]
    ]
  ]
]);


Access::add('shopper', [
  'name' => 'Cliente de tienda',
  'options' => [
    'edit' => [
      'name' => 'Edición',
      'nodes' => [
        [
          'plugin' => 'Store',
          'controller' => 'Orders',
          'action' => 'address',
        ],
        [
          'plugin' => 'Store',
          'controller' => 'Orders',
          'action' => 'checkout',
        ],
        [
          'plugin' => 'Store',
          'controller' => 'Orders',
          'action' => 'payment',
        ],
        [
          'plugin' => 'Store',
          'controller' => 'Orders',
          'action' => 'success',
        ],
        [
          'plugin' => 'Store',
          'controller' => 'Orders',
          'action' => 'view',
        ],
        [
          'plugin' => 'Store',
          'controller' => 'Orders',
          'action' => 'invoice',
        ],
        [
          'plugin' => 'Store',
          'controller' => 'Orders',
          'action' => 'error',
        ],
        [
          'plugin' => 'Store',
          'controller' => 'Orders',
          'action' => 'addpoints',
        ],
        [
          'plugin' => 'Store',
          'controller' => 'Addresses',
          'action' => 'add',
        ],
        [
          'plugin' => 'Store',
          'controller' => 'Addresses',
          'action' => 'edit',
        ],
        [
          'plugin' => 'Store',
          'controller' => 'Addresses',
          'action' => 'delete',
        ],
        [
          'plugin' => 'Store',
          'controller' => 'Users',
          'action' => 'index',
        ],
        [
          'plugin' => 'Store',
          'controller' => 'Users',
          'action' => 'edit',
        ],
        [
          'plugin' => 'Store',
          'controller' => 'Users',
          'action' => 'favs',
        ],
        [
          'plugin' => 'Store',
          'controller' => 'Users',
          'action' => 'orders',
        ],
        [
          'plugin' => 'Store',
          'controller' => 'Users',
          'action' => 'returns',
        ],
        [
          'plugin' => 'Store',
          'controller' => 'Users',
          'action' => 'returns_form',
        ],
        [
          'plugin' => 'Store',
          'controller' => 'Users',
          'action' => 'addresses',
        ],
        [
          'plugin' => 'Store',
          'controller' => 'Users',
          'action' => 'order',
        ],
        [
          'plugin' => 'Store',
          'controller' => 'Favs',
          'action' => 'index',
        ],
        [
          'plugin' => 'Store',
          'controller' => 'Favs',
          'action' => 'add',
        ],
        [
          'plugin' => 'Store',
          'controller' => 'Favs',
          'action' => 'delete',
        ],
        [
          'plugin' => 'Store',
          'controller' => 'Returneds',
          'action' => 'add',
        ],
        [
          'plugin' => 'Store',
          'controller' => 'Returneds',
          'action' => 'success',
        ],
        [
          'plugin' => 'Store',
          'controller' => 'Payments',
          'action' => 'index',
        ],
      ]
    ]
  ]
]);


Access::add('promos', [
  'name' => 'Promociones',
  'options' => [
    'edit' => [
      'name' => 'Edición',
      'nodes' => [
        [
          'prefix' => 'admin',
          'plugin' => 'Store',
          'controller' => 'Promos',
          'action' => 'index',
        ],
        [
          'prefix' => 'admin',
          'plugin' => 'Store',
          'controller' => 'Promos',
          'action' => 'add',
        ],
        [
          'prefix' => 'admin',
          'plugin' => 'Store',
          'controller' => 'Promos',
          'action' => 'create',
        ],
        [
          'prefix' => 'admin',
          'plugin' => 'Store',
          'controller' => 'Promos',
          'action' => 'update',
        ],
        [
          'prefix' => 'admin',
          'plugin' => 'Store',
          'controller' => 'Promos',
          'action' => 'delete',
        ],
        [
          'prefix' => 'admin',
          'plugin' => 'Store',
          'controller' => 'Promos',
          'action' => 'field',
        ],

      ]
    ]
  ]
]);


Access::add('favs', [
  'name' => 'Favoritos',
  'options' => [
    'edit' => [
      'name' => 'Edición',
      'nodes' => [
        [
          'prefix' => 'admin',
          'plugin' => 'Store',
          'controller' => 'Favs',
          'action' => 'index',
        ],
        [
          'prefix' => 'admin',
          'plugin' => 'Store',
          'controller' => 'Favs',
          'action' => 'update',
        ],
      ]
    ]
  ]
]);


Access::add('points', [
  'name' => 'Puntos de descuento',
  'options' => [
    'edit' => [
      'name' => 'Edición',
      'nodes' => [
        [
          'prefix' => 'admin',
          'plugin' => 'Store',
          'controller' => 'Points',
          'action' => 'index',
        ],
        [
          'prefix' => 'admin',
          'plugin' => 'Store',
          'controller' => 'Points',
          'action' => 'add',
        ],
        [
          'prefix' => 'admin',
          'plugin' => 'Store',
          'controller' => 'Points',
          'action' => 'create',
        ],
        [
          'prefix' => 'admin',
          'plugin' => 'Store',
          'controller' => 'Points',
          'action' => 'update',
        ],
        [
          'prefix' => 'admin',
          'plugin' => 'Store',
          'controller' => 'Points',
          'action' => 'delete',
        ]
      ]
    ]
  ]
]);

Access::add('medals', [
  'name' => 'Medallas',
  'options' => [
    'edit' => [
      'name' => 'Edición',
      'nodes' => [
        [
          'prefix' => 'admin',
          'plugin' => 'Store',
          'controller' => 'Medals',
          'action' => 'index',
        ],
        [
          'prefix' => 'admin',
          'plugin' => 'Store',
          'controller' => 'Medals',
          'action' => 'add',
        ],
        [
          'prefix' => 'admin',
          'plugin' => 'Store',
          'controller' => 'Medals',
          'action' => 'create',
        ],
        [
          'prefix' => 'admin',
          'plugin' => 'Store',
          'controller' => 'Medals',
          'action' => 'update',
        ],
        [
          'prefix' => 'admin',
          'plugin' => 'Store',
          'controller' => 'Medals',
          'action' => 'delete',
        ]
      ]
    ]
  ]
]);






Access::add('collections', [
  'name' => 'Colecciones',
  'options' => [
    'edit' => [
      'name' => 'Edición',
      'nodes' => [
        [
          'prefix' => 'admin',
          'plugin' => 'Store',
          'controller' => 'Collections',
          'action' => 'index',
        ],
        [
          'prefix' => 'admin',
          'plugin' => 'Store',
          'controller' => 'Collections',
          'action' => 'add',
        ],
        [
          'prefix' => 'admin',
          'plugin' => 'Store',
          'controller' => 'Collections',
          'action' => 'create',
        ],
        [
          'prefix' => 'admin',
          'plugin' => 'Store',
          'controller' => 'Collections',
          'action' => 'update',
        ],
        [
          'prefix' => 'admin',
          'plugin' => 'Store',
          'controller' => 'Collections',
          'action' => 'delete',
        ]
      ]
    ]
  ]
]);






Access::add('productscolors', [
  'name' => 'Productos y colores',
  'options' => [
    'edit' => [
      'name' => 'Edición',
      'nodes' => [
        [
          'prefix' => 'admin',
          'plugin' => 'Store',
          'controller' => 'ProductsColors',
          'action' => 'index',
        ],
        [
          'prefix' => 'admin',
          'plugin' => 'Store',
          'controller' => 'ProductsColors',
          'action' => 'add',
        ],
        [
          'prefix' => 'admin',
          'plugin' => 'Store',
          'controller' => 'ProductsColors',
          'action' => 'create',
        ],
        [
          'prefix' => 'admin',
          'plugin' => 'Store',
          'controller' => 'ProductsColors',
          'action' => 'update',
        ],
        [
          'prefix' => 'admin',
          'plugin' => 'Store',
          'controller' => 'ProductsColors',
          'action' => 'delete',
        ],
        [
          'prefix' => 'admin',
          'plugin' => 'Store',
          'controller' => 'ProductsColors',
          'action' => 'autocomplete',
        ]
      ]
    ]
  ]
]);





Access::add('payments', [
  'name' => 'Pagos',
  'options' => [
    'edit' => [
      'name' => 'Edición',
      'nodes' => [
        [
          'prefix' => 'admin',
          'plugin' => 'Store',
          'controller' => 'Payments',
          'action' => 'index',
        ],
        [
          'prefix' => 'admin',
          'plugin' => 'Store',
          'controller' => 'Payments',
          'action' => 'add',
        ],
        [
          'prefix' => 'admin',
          'plugin' => 'Store',
          'controller' => 'Payments',
          'action' => 'create',
        ],
        [
          'prefix' => 'admin',
          'plugin' => 'Store',
          'controller' => 'Payments',
          'action' => 'update',
        ],
        [
          'prefix' => 'admin',
          'plugin' => 'Store',
          'controller' => 'Payments',
          'action' => 'delete',
        ]
      ]
    ]
  ]
]);




Access::add('invoices', [
  'name' => 'Facturas',
  'options' => [
    'edit' => [
      'name' => 'Edición',
      'nodes' => [
        [
          'prefix' => 'admin',
          'plugin' => 'Store',
          'controller' => 'Invoices',
          'action' => 'index',
        ],
        [
          'prefix' => 'admin',
          'plugin' => 'Store',
          'controller' => 'Invoices',
          'action' => 'add',
        ],
        [
          'prefix' => 'admin',
          'plugin' => 'Store',
          'controller' => 'Invoices',
          'action' => 'create',
        ],
        [
          'prefix' => 'admin',
          'plugin' => 'Store',
          'controller' => 'Invoices',
          'action' => 'update',
        ],
        [
          'prefix' => 'admin',
          'plugin' => 'Store',
          'controller' => 'Invoices',
          'action' => 'negative',
        ],
        [
          'prefix' => 'admin',
          'plugin' => 'Store',
          'controller' => 'Invoices',
          'action' => 'delete',
        ]
      ]
    ]
  ]
]);


Access::add('receipts', [
  'name' => 'Recibos',
  'options' => [
    'edit' => [
      'name' => 'Edición',
      'nodes' => [
        [
          'prefix' => 'admin',
          'plugin' => 'Store',
          'controller' => 'Receipts',
          'action' => 'index',
        ],
        [
          'prefix' => 'admin',
          'plugin' => 'Store',
          'controller' => 'Receipts',
          'action' => 'add',
        ],
        [
          'prefix' => 'admin',
          'plugin' => 'Store',
          'controller' => 'Receipts',
          'action' => 'create',
        ],
        [
          'prefix' => 'admin',
          'plugin' => 'Store',
          'controller' => 'Receipts',
          'action' => 'update',
        ],
        [
          'prefix' => 'admin',
          'plugin' => 'Store',
          'controller' => 'Receipts',
          'action' => 'delete',
        ]
      ]
    ]
  ]
]);











Access::add('sizes', [
  'name' => 'Tallas',
  'options' => [
    'edit' => [
      'name' => 'Edición',
      'nodes' => [
        [
          'prefix' => 'admin',
          'plugin' => 'Store',
          'controller' => 'Sizes',
          'action' => 'index',
        ],
        [
          'prefix' => 'admin',
          'plugin' => 'Store',
          'controller' => 'Sizes',
          'action' => 'add',
        ],
        [
          'prefix' => 'admin',
          'plugin' => 'Store',
          'controller' => 'Sizes',
          'action' => 'create',
        ],
        [
          'prefix' => 'admin',
          'plugin' => 'Store',
          'controller' => 'Sizes',
          'action' => 'update',
        ],
        [
          'prefix' => 'admin',
          'plugin' => 'Store',
          'controller' => 'Sizes',
          'action' => 'delete',
        ]
      ]
    ]
  ]
]);






Access::add('returneds', [
  'name' => 'Devoluciones',
  'options' => [
    'edit' => [
      'name' => 'Edición',
      'nodes' => [
        [
          'prefix' => 'admin',
          'plugin' => 'Store',
          'controller' => 'Returneds',
          'action' => 'index',
        ],
        [
          'prefix' => 'admin',
          'plugin' => 'Store',
          'controller' => 'Returneds',
          'action' => 'add',
        ],
        [
          'prefix' => 'admin',
          'plugin' => 'Store',
          'controller' => 'Returneds',
          'action' => 'create',
        ],
        [
          'prefix' => 'admin',
          'plugin' => 'Store',
          'controller' => 'Returneds',
          'action' => 'update',
        ],
        [
          'prefix' => 'admin',
          'plugin' => 'Store',
          'controller' => 'Returneds',
          'action' => 'delete',
        ]
      ]
    ]
  ]
]);






Access::add('pointsadds', [
  'name' => 'Anadir puntos',
  'options' => [
    'edit' => [
      'name' => 'Edición',
      'nodes' => [
        [
          'prefix' => 'admin',
          'plugin' => 'Store',
          'controller' => 'PointsAdds',
          'action' => 'index',
        ],
        [
          'prefix' => 'admin',
          'plugin' => 'Store',
          'controller' => 'PointsAdds',
          'action' => 'add',
        ],
        [
          'prefix' => 'admin',
          'plugin' => 'Store',
          'controller' => 'PointsAdds',
          'action' => 'create',
        ],
        [
          'prefix' => 'admin',
          'plugin' => 'Store',
          'controller' => 'PointsAdds',
          'action' => 'update',
        ],
        [
          'prefix' => 'admin',
          'plugin' => 'Store',
          'controller' => 'PointsAdds',
          'action' => 'delete',
        ],
        [
          'prefix' => 'admin',
          'plugin' => 'Store',
          'controller' => 'PointsAdds',
          'action' => 'search',
        ],
        [
          'prefix' => 'admin',
          'plugin' => 'Store',
          'controller' => 'PointsAdds',
          'action' => 'preview',
        ]
      ]
    ]
  ]
]);









// Bloque Novedades
BlocksRegistry::add('novelties', [
  'key' => 'novelties',
  'title' => __d('admin', 'Novedades'),
  'icon' => 'fa fa-newspaper-o',
  'afterAddTarget' => 'parent',
  'inline' => false,
  'unique' => true,
  'deletable' => true,
  'className' => 'Store\\Model\\Block\\NoveltiesBlock',
  'cell' => 'Store.Novelties::display',
  'blockView' => 'Store/blocks/novelties'
]);




Access::add('taxratings', [
  'name' => 'Zonas de impuestos',
  'options' => [
    'edit' => [
      'name' => 'Edición',
      'nodes' => [
        [
          'prefix' => 'admin',
          'plugin' => 'Store',
          'controller' => 'TaxRatings',
          'action' => 'index',
        ],
        [
          'prefix' => 'admin',
          'plugin' => 'Store',
          'controller' => 'TaxRatings',
          'action' => 'add',
        ],
        [
          'prefix' => 'admin',
          'plugin' => 'Store',
          'controller' => 'TaxRatings',
          'action' => 'create',
        ],
        [
          'prefix' => 'admin',
          'plugin' => 'Store',
          'controller' => 'TaxRatings',
          'action' => 'update',
        ],
        [
          'prefix' => 'admin',
          'plugin' => 'Store',
          'controller' => 'TaxRatings',
          'action' => 'delete',
        ],
        [
          'prefix' => 'admin',
          'plugin' => 'Store',
          'controller' => 'TaxRatings',
          'action' => 'sortable',
        ],
        [
          'prefix' => 'admin',
          'plugin' => 'Store',
          'controller' => 'TaxRatings',
          'action' => 'field',
        ]
      ]
    ]
  ]
]);



// NavigationCollection::add( [
//   'name' => 'Zonas de impuestos',
//   'parentName' => 'Tienda',
//   'plugin' => 'Store',
//   'controller' => 'TaxRatings',
//   'action' => 'index',
//   'icon' => 'fa fa-square',
// ]);



Access::add('carrierfestives', [
  'name' => 'Festivos',
  'options' => [
    'edit' => [
      'name' => 'Edición',
      'nodes' => [
        [
          'prefix' => 'admin',
          'plugin' => 'Store',
          'controller' => 'CarrierFestives',
          'action' => 'index',
        ],
        [
          'prefix' => 'admin',
          'plugin' => 'Store',
          'controller' => 'CarrierFestives',
          'action' => 'add',
        ],
        [
          'prefix' => 'admin',
          'plugin' => 'Store',
          'controller' => 'CarrierFestives',
          'action' => 'create',
        ],
        [
          'prefix' => 'admin',
          'plugin' => 'Store',
          'controller' => 'CarrierFestives',
          'action' => 'update',
        ],
        [
          'prefix' => 'admin',
          'plugin' => 'Store',
          'controller' => 'CarrierFestives',
          'action' => 'delete',
        ],
        [
          'prefix' => 'admin',
          'plugin' => 'Store',
          'controller' => 'CarrierFestives',
          'action' => 'sortable',
        ],
        [
          'prefix' => 'admin',
          'plugin' => 'Store',
          'controller' => 'CarrierFestives',
          'action' => 'field',
        ]
      ]
    ]
  ]
]);






Access::add('coupons', [
  'name' => 'Cupones de descuento',
  'options' => [
    'edit' => [
      'name' => 'Edición',
      'nodes' => [
        [
          'prefix' => 'admin',
          'plugin' => 'Store',
          'controller' => 'Coupons',
          'action' => 'index',
        ],
        [
          'prefix' => 'admin',
          'plugin' => 'Store',
          'controller' => 'Coupons',
          'action' => 'add',
        ],
        [
          'prefix' => 'admin',
          'plugin' => 'Store',
          'controller' => 'Coupons',
          'action' => 'create',
        ],
        [
          'prefix' => 'admin',
          'plugin' => 'Store',
          'controller' => 'Coupons',
          'action' => 'update',
        ],
        [
          'prefix' => 'admin',
          'plugin' => 'Store',
          'controller' => 'Coupons',
          'action' => 'delete',
        ],
        [
          'prefix' => 'admin',
          'plugin' => 'Store',
          'controller' => 'Coupons',
          'action' => 'sortable',
        ],
        [
          'prefix' => 'admin',
          'plugin' => 'Store',
          'controller' => 'Coupons',
          'action' => 'field',
        ]
      ]
    ]
  ]
]);



NavigationCollection::add([
  'parent' => 'shop',
  'name' => 'Cupones de descuento',
  'parentName' => 'Tienda',
  'plugin' => 'Store',
  'controller' => 'Coupons',
  'action' => 'index',
  'icon' => 'fa fa-square',
]);






Access::add('orderusersessions', [
  'name' => 'Sesiones de compra',
  'options' => [
    'edit' => [
      'name' => 'Edición',
      'nodes' => [
        [
          'prefix' => 'admin',
          'plugin' => 'Store',
          'controller' => 'OrderUserSessions',
          'action' => 'index',
        ],
        [
          'prefix' => 'admin',
          'plugin' => 'Store',
          'controller' => 'OrderUserSessions',
          'action' => 'add',
        ],
        [
          'prefix' => 'admin',
          'plugin' => 'Store',
          'controller' => 'OrderUserSessions',
          'action' => 'create',
        ],
        [
          'prefix' => 'admin',
          'plugin' => 'Store',
          'controller' => 'OrderUserSessions',
          'action' => 'update',
        ],
        [
          'prefix' => 'admin',
          'plugin' => 'Store',
          'controller' => 'OrderUserSessions',
          'action' => 'delete',
        ],
        [
          'prefix' => 'admin',
          'plugin' => 'Store',
          'controller' => 'OrderUserSessions',
          'action' => 'sortable',
        ],
        [
          'prefix' => 'admin',
          'plugin' => 'Store',
          'controller' => 'OrderUserSessions',
          'action' => 'field',
        ]
      ]
    ]
  ]
]);



NavigationCollection::add([
  'parent' => 'shop',
  'name' => 'Sesiones de compra',
  'parentName' => 'Tienda',
  'plugin' => 'Store',
  'controller' => 'OrderUserSessions',
  'action' => 'index',
  'icon' => 'fa fa-square',
]);



Access::add('orderuseractions', [
  'name' => 'Acciones de usuario',
  'options' => [
    'edit' => [
      'name' => 'Edición',
      'nodes' => [
        [
          'prefix' => 'admin',
          'plugin' => 'Store',
          'controller' => 'OrderUserActions',
          'action' => 'index',
        ],
        [
          'prefix' => 'admin',
          'plugin' => 'Store',
          'controller' => 'OrderUserActions',
          'action' => 'add',
        ],
        [
          'prefix' => 'admin',
          'plugin' => 'Store',
          'controller' => 'OrderUserActions',
          'action' => 'create',
        ],
        [
          'prefix' => 'admin',
          'plugin' => 'Store',
          'controller' => 'OrderUserActions',
          'action' => 'update',
        ],
        [
          'prefix' => 'admin',
          'plugin' => 'Store',
          'controller' => 'OrderUserActions',
          'action' => 'delete',
        ],
        [
          'prefix' => 'admin',
          'plugin' => 'Store',
          'controller' => 'OrderUserActions',
          'action' => 'sortable',
        ],
        [
          'prefix' => 'admin',
          'plugin' => 'Store',
          'controller' => 'OrderUserActions',
          'action' => 'field',
        ]
      ]
    ]
  ]
]);






Access::add('invoicerefunds', [
  'name' => 'Facturas de devolución',
  'options' => [
    'edit' => [
      'name' => 'Edición',
      'nodes' => [
        [
          'prefix' => 'admin',
          'plugin' => 'Store',
          'controller' => 'InvoiceRefunds',
          'action' => 'index',
        ],
        [
          'prefix' => 'admin',
          'plugin' => 'Store',
          'controller' => 'InvoiceRefunds',
          'action' => 'add',
        ],
        [
          'prefix' => 'admin',
          'plugin' => 'Store',
          'controller' => 'InvoiceRefunds',
          'action' => 'create',
        ],
        [
          'prefix' => 'admin',
          'plugin' => 'Store',
          'controller' => 'InvoiceRefunds',
          'action' => 'update',
        ],
        [
          'prefix' => 'admin',
          'plugin' => 'Store',
          'controller' => 'InvoiceRefunds',
          'action' => 'delete',
        ],
        [
          'prefix' => 'admin',
          'plugin' => 'Store',
          'controller' => 'InvoiceRefunds',
          'action' => 'sortable',
        ],
        [
          'prefix' => 'admin',
          'plugin' => 'Store',
          'controller' => 'InvoiceRefunds',
          'action' => 'field',
        ]
      ]
    ]
  ]
]);







Access::add('refunds', [
  'name' => 'Devoluciones',
  'options' => [
    'edit' => [
      'name' => 'Edición',
      'nodes' => [
        [
          'prefix' => 'admin',
          'plugin' => 'Store',
          'controller' => 'Refunds',
          'action' => 'index',
        ],
        [
          'prefix' => 'admin',
          'plugin' => 'Store',
          'controller' => 'Refunds',
          'action' => 'add',
        ],
        [
          'prefix' => 'admin',
          'plugin' => 'Store',
          'controller' => 'Refunds',
          'action' => 'create',
        ],
        [
          'prefix' => 'admin',
          'plugin' => 'Store',
          'controller' => 'Refunds',
          'action' => 'update',
        ],
        [
          'prefix' => 'admin',
          'plugin' => 'Store',
          'controller' => 'Refunds',
          'action' => 'delete',
        ],
        [
          'prefix' => 'admin',
          'plugin' => 'Store',
          'controller' => 'Refunds',
          'action' => 'sortable',
        ],
        [
          'prefix' => 'admin',
          'plugin' => 'Store',
          'controller' => 'Refunds',
          'action' => 'field',
        ]
      ]
    ]
  ]
]);



NavigationCollection::add([
  'name' => 'Devoluciones',
  'parentName' => 'Devoluciones',
  'plugin' => 'Store',
  'controller' => 'Refunds',
  'action' => 'index',
  'icon' => 'fa fa-square',
]);



Access::add('refundsitems', [
  'name' => 'Productos de devolución',
  'options' => [
    'edit' => [
      'name' => 'Edición',
      'nodes' => [
        [
          'prefix' => 'admin',
          'plugin' => 'Store',
          'controller' => 'RefundsItems',
          'action' => 'index',
        ],
        [
          'prefix' => 'admin',
          'plugin' => 'Store',
          'controller' => 'RefundsItems',
          'action' => 'add',
        ],
        [
          'prefix' => 'admin',
          'plugin' => 'Store',
          'controller' => 'RefundsItems',
          'action' => 'create',
        ],
        [
          'prefix' => 'admin',
          'plugin' => 'Store',
          'controller' => 'RefundsItems',
          'action' => 'update',
        ],
        [
          'prefix' => 'admin',
          'plugin' => 'Store',
          'controller' => 'RefundsItems',
          'action' => 'delete',
        ],
        [
          'prefix' => 'admin',
          'plugin' => 'Store',
          'controller' => 'RefundsItems',
          'action' => 'sortable',
        ],
        [
          'prefix' => 'admin',
          'plugin' => 'Store',
          'controller' => 'RefundsItems',
          'action' => 'field',
        ]
      ]
    ]
  ]
]);

Access::add('google_categories', [
  'name' => 'Categorías de Google',
  'options' => [
    'edit' => [
      'name' => 'Edición',
      'nodes' => [
        [
          'prefix' => 'admin',
          'plugin' => 'Store',
          'controller' => 'GoogleCategories',
          'action' => 'index',
        ],
        [
          'prefix' => 'admin',
          'plugin' => 'Store',
          'controller' => 'GoogleCategories',
          'action' => 'autocomplete',
        ],
      ]
    ]
  ]
]);



NavigationCollection::add([
  'name' => 'Productos de devolución',
  'parentName' => 'Productos de devolución',
  'plugin' => 'Store',
  'controller' => 'RefundsItems',
  'action' => 'index',
  'icon' => 'fa fa-square',
]);
