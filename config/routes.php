<?php

use Cake\Core\Configure;
use Cake\Routing\Router;
use Cake\Routing\RouteBuilder;
use Cake\Routing\Route\DashedRoute;


Router::connect('/invoice-refund/:filename', [
    'plugin' => 'Store',
    'controller' => 'InvoiceRefunds',
    'action' => 'view',
]);

Router::connect('/invoice/:filename', [
    'plugin' => 'Store',
    'controller' => 'Orders',
    'action' => 'invoiceDownload',
]);

Router::connect('/delivery-note/:filename', [
    'plugin' => 'Store',
    'controller' => 'Orders',
    'action' => 'deliveryNoteDownload',
]);

Router::connect('/order-view/:salt', [
    'plugin' => 'Store',
    'controller' => 'Orders',
    'action' => 'view',
]);


if (Configure::version() > '3.8.6') {
    Router::plugin(
        'Store',
        [
            'path' => '/{lang}/store'
        ],
        function (RouteBuilder $routes) {
            $routes->fallbacks(DashedRoute::class);
        }
    );
} else {
    Router::plugin(
        'Store',
        [
            'path' => '/:lang/store'
        ],
        function (RouteBuilder $routes) {
            $routes->fallbacks(DashedRoute::class);
        }
    );
}


Router::plugin(
    'Store',
    [
        'path' => '/store'
    ],
    function (RouteBuilder $routes) {
        $routes->fallbacks(DashedRoute::class);
    }
);

